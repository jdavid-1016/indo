
   <link href="assets/css/pages/error.css" rel="stylesheet" type="text/css"/>
   <link href="assets/css/custom.css" rel="stylesheet" type="text/css"/>
   <!-- END THEME STYLES -->
   <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="page-header-fixed">
  
   
   <div class="page-container">
      
      <!-- BEGIN PAGE -->
      <div class="page-content">
         <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->         
         
         <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
                     
         <!-- BEGIN PAGE HEADER-->
         <div class="row">
            <div class="col-md-12">
               <!-- BEGIN PAGE TITLE & BREADCRUMB-->
               <h3 class="page-title">
                  500 Page Option 1 <small>500 Error de permisos</small>
               </h3>
               <ul class="page-breadcrumb breadcrumb">
                  <li>
                     <i class="icon-home"></i>
                     <a href="index.html">Home</a> 
                     <i class="icon-angle-right"></i>
                  </li>
                  <li>
                     <a href="#">Pages</a>
                     <i class="icon-angle-right"></i>
                  </li>
                  <li><a href="#">500 Page Option 1</a></li>
               </ul>
               <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
         </div>
         <!-- END PAGE HEADER-->
         <!-- BEGIN PAGE CONTENT-->          
         <div class="row">
            <div class="col-md-12 page-500">
               <div class=" number">
                  500
               </div>
               <div class=" details">
                  <h3>Oops!  No cuenta con los permisos suficientes.</h3>
                  <p>
                     solo los usuarios Administradores cuentan con permisos para ver este contenido.<br />
                     Pongase en contacto con el administrador del sitio<br /><br />
                  </p>
               </div>
            </div>
         </div>
         <!-- END PAGE CONTENT-->      
      </div>
      <!-- BEGIN PAGE -->     
   </div>
   <!-- END CONTAINER -->
   <!-- BEGIN FOOTER -->
   