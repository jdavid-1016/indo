<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
   <style type="text/css">
   </style>
</head>
<body>
	
<table width="800" cellspacing="0" cellpadding="0" align="center" style="border:3px solid; font-family: helvetica; border-color:#58ACFA #58ACFA; border-style:solid;">
	<tr>
		<td>
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt; ">
				<tr>
					<td width="800">
						<!-- <img src="http://indoamericana.edu.co/airmail/Diplomado/images/header.jpg" width="800"  style="margin:0;padding:0;"> -->
                        <img src="http://indoamericana.edu.co/airmail/sgi/header_reporte_diario_sgi.jpg" width="800"  style="margin:0;padding:0;">
					</td>
				</tr>
			</table>
			<table width="760" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:11pt; color:#3d3d3d; line-height:18px;">
                <br>
                <tr><td><strong> Hola {{$user->name}}  {{$user->last_name}}, a continuación te presentamos un resumen diario de lo que ha pasado en el SGI.</strong></td></tr>
                <br>
                
				<tr>
					<td>
						<div class="row">
                     <div class="page-content">
                     @if(count($supports) != 0 )
                        <h2 style="color:#58ACFA">Soportes Pendientes por calificar</h2>
                        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th >Ticket </th>
                                <th >Solicitó</th>
                                <th >Prioridad</th>
                                <th >Solicitud</th>
                                <th >Estado</th>
                                <th >Creado</th>
                                <th >Comentario</th>
                            </tr>
                        <!-- </table>
                        <table style="font-size: 14px;font-weight: 600;" width="100%" > -->
                            <tbody>
                            @foreach($supports as $support)
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        {{'HD' . str_pad($support->id, 6, "0", STR_PAD_LEFT);}}
                                    </td>
                                    <td class="td_center">
                                        {{$support->Users->name}} {{$support->Users->last_name}}
                                    </td>
                                    <td class="td_center">
                                        <span class="badge {{$support['class_priority']}}">{{$support['user_priority']}}</span>
                                    </td>
                                    <td class="td_center">
                                        {{$support->message}}
                                    </td>
                                    <td class="td_center">
                                        <span class="label label-sm label-info">{{$support->Supports->SupportStatus->name;}}</span>
                                    </td>
                                    <td class="td_center">
                                        {{$support['created_at']}}
                                    </td>
                                    <td class="td_center">
                                        {{$support['observation']}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                        @if(count($supports2) != 0 )
                        <h2 style="color:#58ACFA" >Soportes Asignados</h2>
                        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                
                                <th>Ticket </th>
                                <th>Prioridad</th>
                                <th>Solicitud</th>
                                <th>Estado</th>
                                <th>Responsable</th>
                                <th>Creado</th>
                                <th>Comentario</th>
                            </tr>
                        <!-- </table>
                        <table class="table top-blue" width="100%"> -->
                            <tbody>
                            @foreach($supports2 as $support)
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        {{'HD' . str_pad($support['id'], 6, "0", STR_PAD_LEFT);}}
                                    </td>
                                    <td class="td_center">
                                        <span class="badge {{$support['class_priority']}}">{{$support['user_priority']}}</span>
                                    </td>
                                    <td class="td_center">
                                        {{$support['message'] }}
                                    </td>
                                    <td class="td_center">
                                        <span class="label label-sm {{$support['label']}} ">{{$support->Supports->SupportStatus->name}}</span>
                                    </td>
                                    <td class="td_center">
                                        {{$support->Users->name}} {{$support->Supports->Users->last_name}}
                                    </td>
                                    <td class="td_center">
                                        {{$support['created_at']}}
                                    </td>
                                    <td class="td_center">
                                        {{$support['observation']}}
                                    </td>
                                 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                        @if(count($tareas) != 0 )
                        <h2 style="color:#58ACFA">Actividades Asignadas</h2>
                        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th>Id</th>
                                <th>Actividades</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Cateoria</th>
                                <th>Periodicidad</th>
                                <th>Progreso</th>
                                <th>Inicio</th>
                            </tr>
                        <!-- </table>
                        <table class="table top-blue"> -->
                            <tbody>
                            @foreach($tareas as $tarea)
                                <tr style=" padding: 9px;" align="center">
                                    
                                        <td  class="td_center">
                                            
                                            {{'TR' . str_pad($tarea->id, 6, "0", STR_PAD_LEFT);}}

                                        </td>
                                        <td class="tarea">
                                            <span class="tarea-titulo ">{{$tarea->tasks}}</span>
                                            <!-- <span class="tarea-cuerpo comment more ">{{$tarea->description}}</span> -->
                                        </td>
                                        <td class="">
                                            <!-- <a href="#" class=" nameimg" title="">
                                              <img alt="" style="border-radius:50px;" width="50" height="50" src="{{$tarea->img_min}}"> -->
                                                {{$tarea->name}} {{$tarea->last_name}}
                                            <!-- </a> -->
                                        </td>
                                        <td class="td_center">
                                            {{$tarea->status}}
                                        </td>
                                        <td class="td_center">
                                            {{$tarea->category}}
                                        </td>
                                        <td class="td_center">{{$tarea->periodicity}}</td>
                                        <td>
                                            <span>{{$tarea->progress}} %</span>
                                        </td>
                                        <td class="td_center">{{$tarea->start_date}}</td>
                                        
                                    
                                 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif
                        @if(count($tareas_ingresadas) != 0 )
                        <h2 style="color:#58ACFA">Actividades Creadas</h2>
                        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th>Id</th>
                                <th>Actividades</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                                <th>Cateoria</th>
                                <th>Periodicidad</th>
                                <th>Progreso</th>
                                <th>Inicio</th>
                            </tr>
                        <!-- </table>
                        <table class="table top-blue"> -->
                            <tbody>
                            @foreach($tareas_ingresadas as $tarea_in)
                                <tr style=" padding: 9px;" align="center">
                                    
                                        <td  class="td_center">
                                            {{'TR' . str_pad($tarea_in->id, 6, "0", STR_PAD_LEFT);}}
                                        </td>
                                        <td class="tarea">
                                            <span class="tarea-titulo ">{{$tarea_in->tasks}}</span>
                                            <!-- <span class="tarea-cuerpo comment more ">{{$tarea_in->description}}</span> -->
                                        </td>
                                        <td class="">
                                            <!-- <a href="#" class=" nameimg" title="">
                                              <img alt="" style="border-radius:50px;" width="50" height="50" src="{{$tarea_in->img_min}}"> -->
                                                {{$tarea_in->name}} {{$tarea_in->last_name}}
                                            <!-- </a> -->
                                        </td>
                                        <td class="td_center">
                                            {{$tarea_in->status}}
                                        </td>
                                        <td class="td_center">
                                            {{$tarea_in->category}}
                                        </td>
                                        <td class="td_center">{{$tarea_in->periodicity}}</td>
                                        <td>
                                            <span>{{$tarea_in->progress}} %</span>
                                        </td>
                                        <td class="td_center">{{$tarea_in->start_date}}</td>
                                        
                                    
                                 
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        @endif


                     </div>
                  </div>
					</td>
				</tr>
			</table>
            <br>
            <hr>
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt; background-color:#58ACFA;">
				<tr>
					<td></td>
					<td style="color:#fff;">
						<p align="center" style="font-size:8pt">
							<strong style="font-size:9pt;">Contacto:</strong> <br>
							Corporación Educativa Indoamericana - Area de sistemas - 3239750 ext 2006 - <a href="mailto:dzarate@indoamericana.edu.co">dzarate@indoamericana.edu.co</a> - Casa Piper
						</p>
					</td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>	
</table>	
</body>
</html>