<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
   <style type="text/css">
   </style>
</head>
<body>
	
<table width="800" cellspacing="0" cellpadding="0" align="center" style="border:3px solid; font-family: helvetica; border-color:#58ACFA #58ACFA; border-style:solid;">
	<tr>
		<td>
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt; ">
				<tr>
					<td width="800">
						<!-- <img src="http://indoamericana.edu.co/airmail/Diplomado/images/header.jpg" width="800"  style="margin:0;padding:0;"> -->
                        <img src="http://indoamericana.edu.co/airmail/sgi/header_reporte_semanal_sgi.jpg" width="800"  style="margin:0;padding:0;">
					</td>
				</tr>
			</table>
			<table width="760" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:11pt; color:#3d3d3d; line-height:18px;">
                <br>
                <tr><td><strong> Hola {{$user->name}}  {{$user->last_name}}, a continuación te presentamos un resumen semanal de lo que ha pasado en el SGI.</strong></td></tr>
                <br>
                
				<tr>
					<td>
						<div class="row">
                     <div class="page-content">
                        
                        <h2 style="" align="center">Intranet</h2>
                        <table width="800" border="1" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th >Descipción </th>
                                <th >Valor</th>
                            </tr>
                        <!-- </table>
                        <table style="font-size: 14px;font-weight: 600;" width="100%" > -->
                            <tbody>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Cantidad de Ingresos en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">{{count($ingresos)}}</h4>
                                    </td>
                                    
                                </tr>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Cantidad de Archivos vistos en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">{{count($archivos)}}</h4>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>

                        <br>
                        
                        <h2 style="" align="center">Help-Desk</h2>
                        <table width="800" border="1" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th >Descipción </th>
                                <th >Valor</th>
                            </tr>
                        <!-- </table>
                        <table style="font-size: 14px;font-weight: 600;" width="100%" > -->
                            <tbody>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Cantidad de Soportes atendidos en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">{{count($soportes_atendidos)}}</h4>
                                    </td>
                                    
                                </tr>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Cantidad de Soportes Ingresados en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">{{count($soportes_ingresados)}}</h4>
                                    </td>
                                    
                                </tr>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Promedio de calificaciones de Soportes en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">
                                        @if(count($calificaciones) == 0)
                                            N/A
                                        @else
                                            {{round( $calificaciones[0]->dato, 2)}}
                                        @endif
                                        </h4>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        
                        <h2 style="" align="center">Actividades</h2>
                        <table width="800" border="1" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th >Descipción </th>
                                <th >Valor</th>
                            </tr>
                        <!-- </table>
                        <table style="font-size: 14px;font-weight: 600;" width="100%" > -->
                            <tbody>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Cantidad de Actividades atendidas en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">{{count($actividades_atendidas)}}</h4>
                                    </td>
                                    
                                </tr>
                                <tr style=" padding: 9px;" align="center">
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">Cantidad de Actividades Ingresadas en la última semana.</h4>
                                    </td>
                                    <td class="td_center">
                                        <h4 style="color:#58ACFA">{{count($actividades_ingresadas)}}</h4>
                                    </td>
                                    
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                  </div>
					</td>
				</tr>
			</table>
            <br>
            
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt; background-color:#58ACFA;">
				<tr>
					<td></td>
					<td style="color:#fff;">
						<p align="center" style="font-size:8pt">
							<strong style="font-size:9pt;">Contacto:</strong> <br>
							Corporación Educativa Indoamericana - Area de sistemas - 3239750 ext 2006 - <a href="mailto:dzarate@indoamericana.edu.co">dzarate@indoamericana.edu.co</a> - Casa Piper
						</p>
					</td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>	
</table>	
</body>
</html>