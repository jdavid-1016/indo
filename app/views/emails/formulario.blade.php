<br>
<div class="row">
   <div class="page-content">      
      <h2>Formulario de Registro</h2>
      <table class="table top-blue">
         Hola {{ $name }},
         
         para registrarse como aspirante ingrese al siguiente link:
         
         {{ $link }}
         
         Gracias.
      </table>
   </div>
</div>