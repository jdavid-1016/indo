<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Editar Post
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Editar Post</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <!-- <li>
                    <a href="javascript:;">Aspirantes</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Gestion Emails</a>
                </li> -->
            </ul>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
        {{Form::open( array('url'=>'blog/guardareditarblog', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}
        
        <input type="hidden" id="id_post" name="id_post" required value="{{ $blog->id }}"/>
        
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="col-md-3 control-label">Titulo:</label>
                    <div class="col-md-12">
                        <input type="text" class="form-control" id="titulo" name="titulo" required value="{{ $blog->title }}"/>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2 control-label">Descripcion Corta:</label>
                    <div class="col-md-10">
                        <textarea class="ckeditor form-control" id="desc" name="desc" rows="6">{{ $blog->description }}</textarea>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2 control-label">Articulo:</label>
                    <div class="col-md-10">
                        <textarea class="ckeditor form-control" id="articulo" name="articulo" rows="6">{{ $blog->article }}</textarea>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row" style="display:none">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="col-md-2 control-label">Publico:</label>
                    <div class="col-md-10">
                        <select class="form-control" id="public" name="public">
                            <option value="0" <?php if($blog->public==0){ echo "selected"; } ?> >No</option>
                            <option value="1" <?php if($blog->public==1){ echo "selected"; } ?> >Si</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Imagen Previa*</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="../{{ $blog->image_1 }}" alt="">
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                <input type="file" class="default form-control" name="imagen_previa" id="imagen_previa" size="20">
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                            <div class="help-block">
                               Medidas (390 X 292), Extenciones (.jpg, .png)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-md-6">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Imagen Post*</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="../{{ $blog->image_2 }}" alt="">
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                <input type="file" class="default form-control" name="imagen_post" id="imagen_post" size="20">
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                            <div class="help-block">
                                Medidas (946 X 381), Extenciones (.jpg, .png)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <br>
        <div class="form-actions fluid">
            <div class="col-md-1 col-md-12">
                <button type="submit" class="btn btn-info btn-block">Enviar</button>
            </div>
        </div>

        {{Form::close()}}
        </div>
        <div class="col-md-6">
            
            <div class="scroller" style="height:1130px" data-rail-color="red" data-handle-color="black">
                    <table class="table top-blue" data-target="soporte/callshopping/">                        
                                     <thead>
                                       <tr>
                                          <th>Usuario</th>
                                          <th>Requerido</th>
                                          <th>Visto</th>
                                          <th>Fecha</th>
                                       </tr>
                                    </thead>
                           <tbody>
                           @foreach($usuarios as $usuario)
                              <tr class="@if($usuario->status==0) red @else green @endif" style="" id="" >
                                 <td class="td_center">
                                    {{$usuario->name}} {{$usuario->name2}} {{$usuario->last_name}} {{$usuario->last_name2}}
                                 </td>
                                 <td>
                                   @if($usuario->required==0) No @else Si @endif
                                 </td>
                                 <td class="td_center">
                                    @if($usuario->status==0) No @else Si @endif
                                 </td>
                                 <td class="td_center">
                                    {{$usuario->updated_at}}
                                 </td>
                              </tr>
                           @endforeach
                          </tbody>
                    </table>
                </div>
            
        </div>
    </div>    
</div>
