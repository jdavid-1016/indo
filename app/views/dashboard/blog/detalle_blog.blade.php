<link href="../assets/css/pages/blog.css" rel="stylesheet" type="text/css"/>
<div class="page-content">

    <!-- BEGIN PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
            <h3 class="page-title">
                Blog
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="#">Blog</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Detalle Post</a>
                </li>
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
        </div>
    </div>
    <!-- END PAGE HEADER-->
    <!-- BEGIN PAGE CONTENT-->
    <div class="row">
        <div class="col-md-12 blog-page">
            <div class="row">
                @foreach($blog as $blog)
                <div class="col-md-9 article-block">
                    <h3>{{ $blog->title }}</h3>
                    <div class="blog-tag-data">
                        <img src="../{{ $blog->image_2 }}" class="img-responsive" width="946px" height="381px" alt="">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="list-inline blog-tags">
                                    <li>
                                        <i class="icon-tags"></i>
                                        @foreach($tags as $tag)
                                        <a href="tag?tag={{ $tag->tag }}">{{$tag->tag }}</a>
                                        @endforeach
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-6 blog-tag-data-inner">
                                <ul class="list-inline">
                                    <li><i class="icon-calendar"></i> <a href="#"><?php echo date("\n F jS, Y", strtotime($blog->created_at)); ?></a></li>
                                    <li><i class="icon-comments"></i> <a href="#">{{ count($comentarios) }} Comentarios</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!--end news-tag-data-->
                    <div>
                        <p>{{ $blog->article }}</p>
                    </div>                    
                    <hr>
                     <div class="row">
                        <div class="col-md-2">
                            <img alt="" src="../assets/img/button-face.png" class="media-object">
                        </div>
                        <div class="col-md-2">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-text="Visita el Post en el blog Indoamericana" data-size="large">Twittear</a>
                            <script>!function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location)?'http':'https'; if (!d.getElementById(id)){js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); }}(document, 'script', 'twitter-wjs');</script>
                        </div>
                        <div class="col-md-2">
                            <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
                            <script src="https://apis.google.com/js/platform.js" async defer>
                                {lang: 'es'}
                            </script>

                            <!-- Inserta esta etiqueta donde quieras que aparezca Botón Compartir. -->
                            <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                        </div>
                    </div>
                     <hr>
                    <!--end media-->
                    <div id="comentarios_post">
                        @foreach($comentarios as $comentario)
                        <div class="media">
                            <a href="#" class="pull-left">
                                <img alt="" src="../{{ $comentario->Users->img_min }}" class="media-object">
                            </a>
                            <div class="media-body">
                                <h4 class="media-heading">{{ $comentario->Users->name }} {{ $comentario->Users->last_name }}<span><?php echo date("\n F jS, Y", strtotime($comentario->created_at)); ?></span></h4>
                                <p>{{ $comentario->comment }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <!--end media-->
                    <hr>
                    <div class="post-comment">
                        <h3>Deja un Comentario</h3>                        
                        <div class="form-group">
                            <label class="control-label">Mensaje<span class="required">*</span></label>
                            <textarea class="col-md-10 form-control" rows="8" id="comentario_user" name="comentario_user"></textarea>
                        </div>
                        <button class="margin-top-20 btn btn-info" type="button" onclick="enviarComentarioPost({{ $blog->id }});">Enviar Comentario</button>
                    </div>
                </div>
                @endforeach
                <!--end col-md-9-->
                <div class="col-md-3 blog-sidebar">
                    <h3>Top Entradas</h3>
                    <div class="top-news">
                        <?php
                        foreach ($tops as $top) {
                            $tags = BlogTags::where('blogs_id', $top->id)->get();
                            ?>
                            <a href="detallepost?id={{ $top->id }}" class="btn btn-success">
                                <span>{{ mb_strcut($top->title, 0, 32, "UTF-8") }}...</span>
                                <em>Posteado en: <?php echo date("\n F jS, Y", strtotime($top->created_at)); ?></em>
                                <em>
                                    <i class="icon-tags"></i>
                                    @foreach($tags as $tag)
                                    {{ $tag->tag }},
                                    @endforeach
                                </em>
                                <i class="icon-briefcase top-news-icon"></i>
                            </a>
<?php } ?>
                    </div>
                    <div class="space20"></div>
                    <h3>Blog Tags</h3>
                    <ul class="list-inline sidebar-tags">
                        @foreach($tags_general as $tag)
                        <li><a href="tag?tag={{ $tag->tag }}"><i class="icon-tags"></i>{{ $tag->tag }}</a></li>
                        @endforeach
                    </ul>
                    <div class="space20"></div>
                </div>
                <!--end col-md-3-->
            </div>
        </div>
    </div>
    <!-- END PAGE CONTENT-->
</div>
<!-- END PAGE -->    
</div>
<!-- END CONTAINER -->
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
<script>
            function enviarComentarioPost(id){

            var comentario_user = $("#comentario_user").val();
                    var html = $.ajax({
                    type: "GET",
                            url: "../blog/guardarcomentario",
                            data: { comentario_user:comentario_user, id:id },
                            async: false
                    }).responseText;
                    if (html != 1) {
            toastr.success('Su comentario ha sido enviado correctamente', 'Nuevo Comentario');
                    $("#comentarios_post").append(html);
            } else{
            toastr.error('No ha sido posible guardar tu comentario', 'Error');
            }
            }
</script>