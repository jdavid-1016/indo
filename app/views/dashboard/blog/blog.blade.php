<link href="../assets/css/pages/blog.css" rel="stylesheet" type="text/css"/>
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Blog
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="informacion">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Blog</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
        </div>
    </div>
    
     <div class="row">
            <div class="col-md-12 blog-page">
               <div class="row">
                  <div class="col-md-9 col-sm-8 article-block">
                     <h1>Latest Blog</h1>
                     <?php foreach ($blogs as $blog) { 
                         $tags = BlogTags::where('blogs_id', $blog->id)->get();
                         $comentarios = BlogComments::where('blogs_id', $blog->id)->get();
                         ?>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="../<?php echo $blog->image_1; ?>" alt="" width="390px" height="292px" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#"><?php echo date("\n F jS, Y", strtotime($blog->created_at)); ?></a></li>
                              <li><i class="icon-comments"></i> <a href="#">{{ count($comentarios) }} Comentarios</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i>
                                 @foreach($tags as $tag)
                                 <a href="tag?tag={{ $tag->tag }}">{{ $tag->tag }}</a>
                                 @endforeach
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="detallepost?id={{ $blog->id }}">{{ $blog->title }}</a></h3>
                           <p>{{ $blog->description }}</p>
                           <a class="btn btn-info" href="detallepost?id={{ $blog->id }}">
                           Leer mas 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                           @if($blog->users_id == Auth::user()->id)
                           <a class="btn btn-info" href="editarblog?id={{ $blog->id }}">
                           Editar
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                           @endif
                        </div>
                     </div>
                     <?php } ?>                     
                  </div>
                  <!--end col-md-9-->
                  <div class="col-md-3 col-sm-4 blog-sidebar">
                     <h3>Top Entradas</h3>
                     <div class="top-news">
                         <?php foreach ($tops as $top) { 
                         $tags = BlogTags::where('blogs_id', $top->id)->get();
                         ?>
                        <a href="detallepost?id={{ $top->id }}" class="btn btn-success">
                        <span>{{ mb_strcut($top->title, 0, 32, "UTF-8") }}...</span>
                        <em>Posteado en: <?php echo date("\n F jS, Y", strtotime($top->created_at)); ?></em>
                        <em>
                        <i class="icon-tags"></i>
                        @foreach($tags as $tag)
                         {{ $tag->tag }},
                        @endforeach
                        </em>
                        <i class="icon-briefcase top-news-icon"></i>
                        </a>
                         <?php } ?>
                     </div>
                     <div class="space20"></div>                     
                     <h3>Blog Tags</h3>
                     <ul class="list-inline sidebar-tags">
                         @foreach($tags_general as $tag)
                         <li><a href="tag?tag={{ $tag->tag }}"><i class="icon-tags"></i>{{ $tag->tag }}</a></li>
                        @endforeach
                     </ul>
                     <div class="space20"></div>
                     
                  </div>
                  <!--end col-md-3-->
               </div>
               <div class="pagination">
                {{$pag->appends(array("tag" => Input::get('tag')))->links()}}
              </div>
            </div>
         </div>        

</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">

    <!-- /.modal-dialog -->
</div>