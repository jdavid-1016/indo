<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
    <div class="modal-content">
        <form class="form" action="#" ng-submit="adminSupport()">

            <input type="hidden" ng-model="support_id" value="" id="support_id">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Histórico de solicitudes de compras</h4>
            </div>
            <div class="modal-body form-body">

                <div class="table-responsive">

                    <table class="table table-bordered table-hover top-blue" >
                        <thead>
                            <tr>

                                <th>id</th>
                                <th>Descripción</th>
                                <th>Categoria</th>
                                <th>Referencia</th>
                                <th>Cantidad</th>
                                <th>Valor</th>
                                <th>Estado</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach($shopping_details as $details)

                            <tr class="@if($details->shopping_details_statuses_id == 2)green @endif">

                                <td>{{$details - > id}}</td>
                                <td>{{$details - > description}}</td>
                                <td>{{$details - > ShoppingCategories - > name}}</td>
                                <td>{{$details - > reference}}</td>
                                <td>{{$details - > quantity}}</td>
                                <td>$ {{$details - > value}}</td>
                                <td>{{$details - > ShoppingDetailsStatuses - > name}}</td>

                            </tr>

                            @endforeach
                        </tbody>

                    </table>
                    <div class="note note-success">
                        <a href="#" class="pull-left">
                            <img alt="" style="border-radius:50px;" width="50%"  src="../{{$details - > Shoppings - > Users - > img_min}}">
                        </a>

                        <span class="text-success">Justificación {{$details - > Shoppings - > Users - > name}} {{$details - > Shoppings - > Users - > last_name}}:</span>

                        <span class="pull-right"></span>
                        <p>
                            {{$details - > Shoppings - > justification}}
                        </p>    
                    </div>

                    <div class="row col-md-12">
                        <hr>
                        <h3 style="text-align:center">Cotizaciones </h3>
                        <hr>

                        <?php
                        $cuotation = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                        ?>            

                        <?php
                        $cuotations = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                        ?>
                        <table class="table table-bordered table-hover top-blue" id="">
                            <thead>
                                <tr>

                                    <th>Nombre Proveedor</th>
                                    <th>Nit</th>
                                    <th>Valor Cotización</th>
                                    <th>Observación</th>
                                    <th>Cotizacion</th>

                                </tr>
                            </thead>
                            <tbody id="archivos_subidos">
                                @foreach($cuotations as $cuotation)

                                <tr id="cuotation_{{$cuotation - > id}}" class="@if($cuotation->status == 1)green @endif" >

                                    <td>{{$cuotation - > Providers - > provider}}</td>
                                    <td>{{$cuotation - > Providers - > nit}}</td>
                                    <td>$ {{$cuotation - > valor}}</td>
                                    <td>{{$cuotation - > observacion}}</td>
                                    <td><a class="btn btn-default" id="" href="../{{$cuotation - > file}}" target="_blanc"><i class="icon-paper-clip"></i></a></td>


                                </tr>

                                @endforeach
                            </tbody>


                        </table>

                        <?php $traces = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->get(); ?>
                        @foreach($traces as $trace)

                        <div class="note note-success">
                            <a href="#" class="pull-left">
                                <img alt="" style="border-radius:50px; padding:5px" width="50px" src="../{{$trace - > Users - > img_min}}">
                            </a>

                            <span class="text-success">Commentario {{$trace - > Users - > name}} {{$trace - > Users - > last_name}}:</span>

                            <span class="pull-right">{{$trace - > created_at}}</span>
                            <p>
                                {{$trace - > comment}}
                            </p>    
                        </div>

                        @endforeach

                        <div class="note">
                            Comentario:
                            <textarea id="comment_aproved" placeholder="Comentario para el usuario" class="form-control"></textarea><br>
                            <input type="text" placeholder="Fecha Aprox. de entrega (yyyy-mm-dd)" id="fecha_entrega" class="form-control fecha_required">}
                            
                            <input type="file" name="archivo" id="archivo" />
                        </div>

                    </div>
                </div>
            </div>   
            <div class="modal-footer">
                <button type="button" class="btn btn-success" onclick="terminar_proceso({{$shopping_details[0] - > shoppings_id}})">Enviar</button>
                <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
            </div>

        </form>
    </div>
</div>

<script src="/indo/public/js/upload.js"></script>
<script type="text/javascript">
                            $("#fecha_entrega").datepicker({ dateFormat: "yy-mm-dd" });
                            function terminar_proceso(id) {
                            var comment_aproved = $("#comment_aproved").val();
                            var fecha_entrega = $("#fecha_entrega").val();
                                    if (comment_aproved != "") {
                            if (fecha_entrega != "") {


                            $("#archivo").upload('subirfactura', 
                            {
                                    id_shopping:id,
                                    comment_aproved: comment_aproved,
                                    fecha_entrega: fecha_entrega
                                    
                            },
                                    function(respuesta) {
                                    //Subida finalizada.
                                    $("#barra_de_progreso_" + id).val(0);
                                            if (respuesta === 1) {
                                    mostrarRespuesta('El archivo NO se ha podido subir.', false, id);
                                    } else {
                                    mostrarRespuesta(respuesta, true, id);
                                            $("#nombre_archivo_" + id + ", #archivo_" + id).val('');
                                    }
                                    $("#respuesta").html(respuesta);
                                        alert("hola")
                                            //mostrarArchivos(id);
                                    }, function(progreso, valor) {
                                    //Barra de progreso.
                                    $("#barra_de_progreso_" + id).val(valor);
                                    });
                                    
//                                    $.ajax({
//                                    type: "GET",
//                                            url: 'terminarproceso',
//                                            data:{id_shopping:id, comment_aproved:comment_aproved, fecha_entrega:fecha_entrega},
//                                            success: function(respuesta) {
//                                            if (respuesta) {
//                                            toastr.success('Se actualizo correctamente la solicitud', 'Actualización');
//                                                    $("#close_modal").click();
//                                                    var type_event = ["pendingshopping", respuesta[0]["name"], respuesta[0]["last_name"], respuesta[0]["img_min"], respuesta[0]["id"], 6, id];
//                                                    send(type_event);
//                                            }
//                                            }
//                                    });
                                    
                            } else{
                            toastr.error('El campo Fecha Aprox. de entrega no puede estar vacio', 'Error');
                            }
                            } else{
                            toastr.error('El campo Comentario no puede estar vacio', 'Error');
                            }
                            }
</script>