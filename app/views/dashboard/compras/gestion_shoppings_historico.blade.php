<script language="JavaScript" src="../assets/busc/jquery-1.5.1.min.js"></script>
<script language="JavaScript" src="../assets/busc/jquery.watermarkinput.js"></script>
<div class="modal-dialog  modal-wide " ng-controller="help_deskCtrl" id="responsible">
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Histórico de solicitudes de compras</h4>
      </div>
      <div class="modal-body form-body">   
          
         <div class="table-responsive">
         
            <table class="table table-bordered table-hover top-blue" >
               <thead>
                  <tr>
                     
                     <th>id</th>
                     <th>Descripción</th>
                     <th>Categoria</th>
                     <th>Referencia</th>
                     <th>Cantidad</th>
                     <th>Valor</th>
                     <th>Estado</th>
                     
                     
                  </tr>
               </thead>
               <tbody>
                  @foreach($shopping_details as $details)
                  
                  <tr class="@if($details->shopping_details_statuses_id == 2)green @endif">
    
                     <td>{{$details->id}}</td>
                     <td>{{$details->description}}</td>
                     <td>{{$details->ShoppingCategories->name}}</td>
                     <td>{{$details->reference}}</td>
                     <td>{{$details->quantity}}</td>
                     <td>$ {{$details->value}}</td>
                     <td>{{$details->ShoppingDetailsStatuses->name}}</td>
                     
                     
                  </tr>
                  
                  @endforeach
               </tbody>

               
            </table>
            <div class="col-md-6">
              <div class="note note-success">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px;" width="50%"  src="../{{$details->Shoppings->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Justificación {{$details->Shoppings->Users->name}} {{$details->Shoppings->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$time}}</span>
                  <p>
                      {{$details->Shoppings->justification}}
                  </p>    
              </div>
            </div>
            <?php $traces2 = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status', 2)->get(); ?>
            <div class="col-md-6">
              <div class="note note-info">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px; padding:5px" width="50px"  src="../{{$traces2[0]->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Commentario {{$traces2[0]->Users->name}} {{$traces2[0]->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$traces2[0]->created_at}}</span>
                  <p>
                      {{$traces2[0]->comment}}
                  </p>    
              </div>
            </div>
            <div class="row col-md-12">
            <hr>
            <h3 style="text-align:center">Cotizaciones </h3>
            <hr>
            
            
                <?php 
                $cuotation = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                
                ?>
                
                                                                                            
            
            <?php 
                $cuotations = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
            ?>
                <table class="table table-bordered table-hover top-blue" id="">
                   <thead>
                        <tr>
                            <th>Cod.</th>
                            <th>Nombre Proveedor</th>
                            <th>Nit</th>
                            <th>Valor Cotización</th>
                            <th>Observación</th>
                            <th>Cotizacion</th>
                           
                        </tr>
                   </thead>
                   <tbody id="archivos_subidos">
                      @foreach($cuotations as $cuotation)
                      
                      <tr id="cuotation_{{$cuotation->id}}" class="@if($cuotation->status == 1)green @endif" >
                         <td>COT{{str_pad($cuotation->id, 6, "0", STR_PAD_LEFT)}}</td>
                         <td>{{$cuotation->Providers->provider}}</td>
                         <td>{{$cuotation->Providers->nit}}</td>
                         <td>$ {{$cuotation->valor}}</td>
                         <td>{{$cuotation->observacion}}</td>
                         <td><a class="btn btn-default" style="@if($cuotation->file == '') display:none @endif " id="" href="../{{$cuotation->file}}" target="_blanc"><i class="icon-paper-clip"></i></a></td>
                         
                         
                      </tr>
                      
                      @endforeach
                   </tbody>
                </table>

                   <?php $traces = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status', '<>', 2)->get(); ?>
                   @foreach($traces as $trace)
                   
                   <div class="note note-success">
                       <a href="#" class="pull-left">
                           <img alt="" style="border-radius:50px; padding:5px" width="50px"  src="../{{$trace->Users->img_min}}">
                       </a>
                       
                       <span class="text-success">Commentario {{$trace->Users->name}} {{$trace->Users->last_name}}:</span>
                       
                       <span class="pull-right">{{$trace->created_at}}</span>
                       <p>
                           {{$trace->comment}}
                       </p>    
                   </div>
                   
                   @endforeach
                @if($shopping_details[0]->Shoppings->comment == "")
                @else
                <div class="row col-md-12">
                   <div class="note note-warning">
                       <h4 class="block">Comentario:</h4>
                       <p>
                           {{$shopping_details[0]->Shoppings->comment}}
                       </p>
                   </div>
                  
                </div>
                @endif
                @if($shopping_details[0]->Shoppings->comment_aproved == "")
                @else
                <div class="row col-md-12">
                   <div class="note note-warning">
                       <h4 class="block">Comentario:</h4>
                       <span class="pull-right">Fecha estimada de entrega: {{$shopping_details[0]->Shoppings->date_delivery}}</span>
                       <p>
                           {{$shopping_details[0]->Shoppings->comment_aproved}}
                       </p>
                   </div>
                  
                </div>
                @endif
                @if($shopping_details[0]->Shoppings->comment_refused == "")
                @else
                <div class="row col-md-12">
                   <div class="note note-warning">
                       <h4 class="block">Comentario:</h4>
                       <p>
                           {{$shopping_details[0]->Shoppings->comment_refused}}
                       </p>
                   </div>
                  
                </div>
                @endif
                             
            </div>
         </div>
      </div>   
      <div class="modal-footer">                     
                     
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>
<script type="text/javascript">
    
   function terminar_proceso(id) {
       var comment_aproved = $("#comment_aproved").val();
       if (comment_aproved != "") {

           $.ajax({
               type: "GET",
               url: 'terminarproceso',
               data:{id_shopping:id, comment_aproved:comment_aproved} ,    
               success: function(respuesta) {
                   if (respuesta) {
                       toastr.success('Se Enviaron las cotizaciones correctamente', 'cotizaciones');
                   }
               }
           });
       }else{
        toastr.error('El campo Comentario no puede estar vacio', 'Error');
       }
   }
    
</script>