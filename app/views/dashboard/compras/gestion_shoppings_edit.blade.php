<style type="text/css">
#displayprov /*estilos para la caja principal en donde se puestran los resultados de la busqueda en forma de lista*/
{
width:400px;
display:none;
overflow:hidden;
z-index:10;
border: solid 1px #666;
}
.display_box /*estilos para cada caja unitaria de cada usuario que se muestra*/
{
padding:2px;
padding-left:6px; 
font-size:15px;
height:30px;
text-decoration:none;
color:#3b5999; 
}

.display_box:hover /*estilos para cada caja unitaria de cada usuario que se muestra. cuando el mause se pocisiona sobre el area*/
{
background: #7f93bc;
color: #FFF;
}
/* Easy Tooltip */
</style>
<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Gestion de solicitudes de compras</h4>
      </div>
      <div class="modal-body form-body">   
          
            <div class="table-responsive">
            
                <table class="table table-bordered table-hover table-striped top-blue table-responsive" >
                   <thead>
                      <tr>

                         <th>id</th>
                         <th>Descripción</th>
                         <th>Categoria</th>
                         <th>Referencia</th>
                         <th>Cantidad</th>
                         <th>Valor</th>

                      </tr>
                   </thead>
                   <tbody>
                      @foreach($shopping_details as $details)
                      
                      <tr>
        
                         <td>{{$details->id}}</td>
                         <td>{{$details->description}}</td>
                         <td>{{$details->ShoppingCategories->name}}</td>
                         <td>{{$details->reference}}</td>
                         <td>{{$details->quantity}}</td>
                         <td>$ {{$details->value}}</td>
                         
                      </tr>
                      
                      @endforeach
                   </tbody>
                   
                </table>
            </div>
            <div class="col-md-6">
              <div class="note note-success">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px;" width="50%"  src="../{{$details->Shoppings->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Justificación {{$details->Shoppings->Users->name}} {{$details->Shoppings->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$time}}</span>
                  <p>
                      {{$details->Shoppings->justification}}
                  </p>    
              </div>
            </div>
            <?php $traces2 = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status',2)->get(); ?>
            <div class="col-md-6">
              <div class="note note-info">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px;" width="50%"  src="../{{$traces2[0]->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Commentario {{$traces2[0]->Users->name}} {{$traces2[0]->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$traces2[0]->created_at}}</span>
                  <p>
                      {{$traces2[0]->comment}}
                  </p>    
              </div>
            </div>
            
            
                    <hr>
                    <h3 style="text-align:center">Cotizaciones </h3>
                    <hr>
            
            <div class="row">    
                <div class="col-md-9">
                    <h4>Subir Nueva Cotización: </h4>
                    <?php 
                    $cuotation = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                    
                    ?>
                    <input type="hidden" class="form-control" name="id_shopping" id="id_shopping" value="{{$details->id}}" />
                    <input type="text" class="form-control " name="nit" id="nit"  value="" onfocus="focusFunctionNit()" onblur="blurFunctionNit()" placeholder="Nit"/><br>
                    <div id="displaynit"></div>
                    <input type="hidden" name="id_proveedor" id="id_proveedor" value=""/>
                    
                    <input type="text" class="form-control " name="nombre_pro" id="nombre_pro" value="" onfocus="focusFunction()" onblur="blurFunction()" placeholder="Nombre Proveedor" /><br>
                    <div id="displayprov"></div>
                    <input type="text" class="form-control  money" name="valor_cot" id="valor_cot" value="" placeholder="Valor Cotización"/><br>
                        
                    <textarea class="form-control " placeholder="Observación" id="observacion"></textarea>
                    <br>
                    <input type="file" name="archivo" id="archivo" />
                       <br>
                       <h4>Cotizacion Recomendada: <input type="checkbox" name="recomendado" id="recomendado"></h4>
                       <br>
                    <input type="button" id="boton_subir" value="Subir" class="btn btn-success" onclick="subirArchivos({{$shopping_details['0']->shoppings_id}})" /><br>
                    <div id="respuesta" class="alert">  </div>

                </div>
             </div>
                
                <div class="col-md-12">

                    <?php
                        $cuotations = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                    ?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover top-blue table-responsive" id="" style="width:100%">                            
                           <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre Proveedor</th>
                                    <th>Nit</th>
                                    <th>Valor Cotización</th>
                                    <th>Observación</th>
                                    <th>Cotizacion</th>
                                    <th>Opciones</th>
                                </tr>
                           </thead>
                           <tbody id="archivos_subidos">
                              @foreach($cuotations as $cuotation)
                              
                              <tr id="cuotation_{{$cuotation->id}}" class="<?php if($cuotation->recommended == 1){ echo "green"; }?>">
                                 <td>COT{{str_pad($cuotation->id, 6, "0", STR_PAD_LEFT)}}</td>
                                 <td>{{$cuotation->Providers->provider}}</td>
                                 <td>{{$cuotation->Providers->nit}}</td>
                                 <td>$ {{$cuotation->valor}}</td>
                                 <td>{{$cuotation->observacion}}</td>
                                 <td><a class="btn btn-default" style="@if($cuotation->file == '') display:none @endif " id="" href="../{{$cuotation->file}}" target="_blanc"><i class="icon-paper-clip"></i></a></td>
                                 <td><a class="btn btn-danger" id="{{$cuotation->id}}" onclick='delete_cuotation($(this).attr("id"));return false;'><i class="icon-trash"></i></a></td>
                                 
                              </tr>
                              
                              @endforeach
                           </tbody>
                        </table>
                    </div>
                    <?php $traces = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status', '<>',2)->get(); ?>
                    @foreach($traces as $trace)
                    
                    <div class="note note-success">
                        <a href="#" class="pull-left">
                            <img alt="" style="border-radius:50px;" width="50%"  src="../{{$trace->Users->img_min}}">
                        </a>
                        
                        <span class="text-success">Commentario {{$trace->Users->name}} {{$trace->Users->last_name}}:</span>
                        
                        <span class="pull-right">{{$trace->created_at}}</span>
                        <p>
                            {{$trace->comment}}
                        </p>    
                    </div>
                    
                    @endforeach
                    @if($details->Shoppings->comment_refused == "")
                    @else
                    <div class="row col-md-12">
                       <div class="note note-warning">
                           <h4 class="block">Comentario:</h4>
                           <p>
                               {{$details->Shoppings->comment_refused}}
                           </p>
                       </div>
                      
                    </div>
                    @endif
                    <hr>
                    <div id="comentario" style="display:">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <textarea rows="2" class="form-control" id="comentario_refuse"></textarea>
                        </div>
                     </div>
                </div>
            
        </div>   
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" unico="{{$details->shoppings_id}}" onclick="terminar_proceso({{$shopping_details['0']->shoppings_id}}, 5)" id="rechazarsol" >Rechazar</button>
            <button type="button" class="btn btn-success" onclick="terminar_proceso({{$shopping_details['0']->shoppings_id}}, 3)">Terminar Proceso</button>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
        </div>
   
    </form>
    </div>
</div>

<script src="/indo/public/js/upload.js"></script>
<script type="text/javascript">
    function rechazarSol(){
        $('#comentario').toggle('slow');
        var unico = $('#rechazarsol').attr("unico");
        $('#rechazarsol').attr("onclick", "terminar_proceso("+unico+", 5)");        
    }
    function mostrarRespuesta(mensaje, ok, id){
       $("#respuesta_"+id).removeClass('alert-success').removeClass('alert-danger').html(mensaje);
       if(ok){
           $("#respuesta_"+id).addClass('alert-success');
       }else{
           $("#respuesta_"+id).addClass('alert-danger');
       }
    }
   function subirArchivos(id) {
       
        if($("#recomendado").is(':checked')) {
            var recomendado = 1;
        } else {
            var recomendado = 0;
        }
        var archivo = $("#archivo").val();

        if (archivo == "") {

            var id_shopping=    id;
            var id_proveedor=   $("#id_proveedor").val();
            var valor_cot=      $("#valor_cot").val();
            var recomendado=    recomendado;
            var observacion=    $("#observacion").val();

            if (id_proveedor == "") {
                toastr.error('error campo de proveedor esta vacio', 'cotizaciones');
                return;
            }
            if (valor_cot == "") {
                toastr.error('error campo de valor cot vacio', 'cotizaciones');
                return;
            }
            if (observacion == "") {
                toastr.error('error campo de observacion vacio', 'cotizaciones');
                return;
            }

            toastr.info('Subio una cotizacion sin archivo de soporte', 'cotizaciones');

            $.ajax({
                type: "POST",
                url: 'subricotizacion',
                data:{
                    id_shopping:id,
                    id_proveedor:id_proveedor,
                    valor_cot:valor_cot,
                    recomendado:recomendado,
                    archivo:1,
                    observacion:observacion

                } ,
                success: function(respuesta) {
                       //Subida finalizada.
                       $("#barra_de_progreso_"+id).val(0);
                       if (respuesta === 1) {
                           mostrarRespuesta('El archivo NO se ha podido subir.', false, id);
                       } else {
                           mostrarRespuesta(respuesta, true, id);
                           $("#nombre_archivo_"+id+", #archivo_"+id).val('');
                       }
                       $("#respuesta").html(respuesta);
                       mostrarArchivos(id);
                    }
            });
        
            return
            
        }

        $("#archivo").upload('subricotizacion',
        {
            id_shopping:id,
            id_proveedor:   $("#id_proveedor").val(),
            valor_cot:      $("#valor_cot").val(),
            recomendado:    recomendado,
            observacion:    $("#observacion").val(),
            archivo:        0
        },
        function(respuesta) {
           //Subida finalizada.
           $("#barra_de_progreso_"+id).val(0);
           if (respuesta === 1) {
               mostrarRespuesta('El archivo NO se ha podido subir.', false, id);
           } else {
               mostrarRespuesta(respuesta, true, id);
               $("#nombre_archivo_"+id+", #archivo_"+id).val('');
           }
           $("#respuesta").html(respuesta);
           mostrarArchivos(id);
        }, function(progreso, valor) {
           //Barra de progreso.
           $("#barra_de_progreso_"+id).val(valor);
        });
   }

   function mostrarArchivos(id) {
       $.ajax({
           type: "GET",
           url: 'mostrararchivos',
           data:{id_quotation:id} ,
           success: function(respuesta) {
               if (respuesta) {
                   var html = '';
                   for (var i = 0; i < respuesta.length; i++) {
                       if (respuesta[i] != undefined) {
                           html += '<div class="row"> <span class="col-lg-2"> ' + respuesta[i] + ' </span> <div class="col-lg-2"> <a class="eliminar_archivo btn btn-danger" href="javascript:void(0);"> Eliminar </a> </div> </div> <hr />';
                       }
                   }
                   $("#archivos_subidos").html(respuesta);
                   $("#nombre_pro").val(''); 
                   $("#nit").val('');
                   $("#valor_cot").val('');
                   $("#observacion").val('');
                   $("#archivo").val('');
                   $("#recomendado").attr('checked', false);
               }
           }
       });
   }
   function terminar_proceso(id, status) {
        var comentario_general = $("#comentario_refuse").val()
        if (comentario_general != "") {

           $.ajax({
               type: "GET",
               url: 'terminarproceso',
               data:{id_shopping:id, comentario_general:comentario_general, status:status} ,    
               success: function(respuesta) {
                   if (respuesta) {
                       toastr.success('Se Enviaron las cotizaciones correctamente', 'cotizaciones');
                       $("#close_modal").click();
                       
                       var type_event = ["adminshopping", respuesta[0]["name"],respuesta[0]["last_name"],respuesta[0]["img_min"], respuesta[0]["id"], status, id];
                       send(type_event);
                   }
               }
           });
        }else{
          toastr.error('El Campo comentario general esta vacio', 'Error');
        }

   }
   function delete_cuotation(id) {
       $.ajax({
           type: "GET",
           url: 'deletecuotation',
           data:{cuotation:id} ,    
           success: function(respuesta) {
               if (respuesta) {
                   toastr.success('Se Elimino la cotización correctamente', 'cotizaciones');
                   $("#cuotation_"+id).css("display", "none");
               }
           }
       });
   }
   function funcion2(hola){
      var hola = hola;

     
      $('#ocult'+hola).toggle('slow');
      mostrarArchivos(hola);
   } 
   
   function agregarNit(nit, nombre, id){
     
      $('#id_proveedor').val(id);
      $('#nit').val(nit);
      $('#nombre_pro').val(nombre);
      $("#displayprov").hide();
      
   }
   
   function agregarProv(nit, nombre, id){
     
      $('#id_proveedor').val(id);
      $('#nit').val(nit);
      $('#nombre_pro').val(nombre);
      $("#displayprov").hide();
      $("#displaynit").hide();
      
   }

// Focus = Changes the background color of input to yellow
function focusFunction() {
    $("#displayprov").hide('slow');
}

function blurFunction() {
    $("#displayprov").show('slow');
}

function focusFunctionNit() {
    $("#displaynit").hide('slow');
}

function blurFunctionNit() {
    $("#displaynit").show('slow');
}

$("#nombre_pro").keyup(function() //se crea la funcioin keyup
{
    var texto = $("#nombre_pro").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displayprov").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarprov",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displayprov").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

$("#nit").keyup(function() //se crea la funcioin keyup
{
    var texto = $("#nit").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displaynit").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarnit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displaynit").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});
</script>

<style type="text/css">
    
    #mensaje{
      border:dashed 1px red;
      background-color:#FFC6C7;
      color: #000000;
      padding: 10px;
      text-align: left;
      margin: 10px auto; 
      display: none;//Al cargar el documento el contenido del mensaje debe estar oculto
    }
  </style>