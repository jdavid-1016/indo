
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Administrar compras</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="administrar" data-toggle="tab">Solicitudes</a></li>
                  <li class=""><a href="administrarhist" >Histórico</a></li>
                  <li class=""><a href="aprobarpagos" >Aprobar Pagos</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_shoppings">
                     <div>
                        <form class="form-inline" action="administrar" method="get">
                           <div class="search-region">
                              <div class="form-group">
                                 <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                              </div>
                              <div class="form-group">
                                 <select class="form-control input-medium" name="applicant">
                                 <option value="">Solicitó</option> 
                              @foreach($applicants as $applicant)
                              @if($applicant->id == Input::get('applicant'))
                                 <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                              @else
                                 <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                              @endif  
                              @endforeach
                                 
                                 
                                 </select>
                              </div>
                              <!-- <div class="form-group">
                                 <select class="form-control input-medium" name="responsible">
                                 <option value="">Responsable</option>   
                                 
                                    <option value="" selected=""></option>
                                 
                                 
                                 </select>
                              </div> -->
                              <!-- <div class="form-group">
                                 <select class="form-control input-small" name="status">
                                    <option value="">Estado</option>
                                    <option  value="2">cerrado</option>
                                    <option  value="3">rechazado</option>
                                 </select>
                              </div> -->
                              <!-- <div class="form-group">
                                 <select class="form-control input-small" name="priority">
                                    <option value="">Prioridad</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                 </select>
                              </div> -->
                              <div class="form-group">
                                 <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                              </div>    
                           </div>
                        </form> 
                     </div>
                        
                        @include('dashboard.compras.table_admin_compras')
                        <div class="pagination">
                          {{$pag->appends(array("code" => Input::get('code'),"applicant" => Input::get('applicant'),"text" => Input::get('text'),"status" => Input::get('status'),"priority" => Input::get('priority')))->links()}}
                        </div>
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="wide" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>
<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });    
  });
</script>
   <script type="text/javascript">
   function realizarProceso(valorCaja1){
      var parametros = {
         "valorCaja1": valorCaja1
      };
      $.ajax({
         data: parametros,
         url:  'gestionajax',
         type: 'post',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   </script>
   <script type="text/javascript">
   function cargaHistorico(){
     
      $.ajax({
         data: null,
         url:  'adminHistorico',
         type: 'get',

         success: function(response){
               $("#historico").html(response);
         }
      });
   }
   
   </script>