
<div class="page-content" id="adminhelpdesk">
   <div class="row">
       <div class="col-md-12">
           <h3 class="page-title">
               Aprobar Solicitudes de compras
           </h3>
           <ul class="page-breadcrumb breadcrumb">
               <li>
                   <i class="icon-home"></i>
                   <a href="javascript:;">Página Principal</a> 
                   <i class="icon-angle-right"></i>
               </li>
               <li>
                   <a href="javascript:;">Compras</a>
                   <i class="icon-angle-right"></i> 
               </li>
               <li>
                   <a href="javascript:;">Mis Solicitudes</a> 
               </li>
           </ul>
       </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         
         <div class="table-responsive">
            <div class="tabbable tabbable-custom">  

            
               <ul class="nav nav-tabs">
                   
                   <li class="active"><a href="aprobar">Mis solicitudes</a></li>
                   <li class=""><a href="aprobarhist">Histórico</a></li>
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_aprobar_shoppings">
                      <!-- <div>
                         <form class="form-inline" action="admin_history" method="get">
                            <div class="search-region">
                               <div class="form-group">
                                  <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-medium" name="applicant">
                                  <option value="">Solicitó</option> 
                              
                                  <option value="" selected=""></option>
                               
                                  
                             
                                  
                                  
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-medium" name="responsible">
                                  <option value="">Responsable</option>   
                                  
                                     <option value="" selected=""></option>
                                  
                                  
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-small" name="status">
                                     <option value="">Estado</option>
                                     <option  value="2">cerrado</option>
                                     <option  value="3">rechazado</option>
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-small" name="priority">
                                     <option value="">Prioridad</option>
                                     <option value="1">1</option>
                                     <option value="2">2</option>
                                     <option value="3">3</option>
                                     <option value="4">4</option>
                                     <option value="5">5</option>
                                  </select>
                               </div>
                               <div class="form-group">
                                  <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                               </div>    
                            </div>
                         </form> 
                      </div> -->
                                                
                         @include('dashboard.compras.table_aprobar_compras')
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>

