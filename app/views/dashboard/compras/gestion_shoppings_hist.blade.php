<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Histórico de solicitudes de compras</h4>
      </div>
      <div class="modal-body form-body">
          
         <div class="table-responsive">
         
            <table class="table table-bordered table-hover top-blue" >
               <thead>
                  <tr>
                     
                     <th>id</th>
                     <th>Descripción</th>
                     <th>Categoria</th>
                     <th>Referencia</th>
                     <th>Cantidad</th>
                     <th>Valor</th>
                     <th>Estado</th>
                     
                  </tr>
               </thead>
               <tbody>
                  @foreach($shopping_details as $details)
                  
                  <tr class="<?php if($details->shopping_details_statuses_id == 2){ echo "green"; }?>">
    
                     <td>{{$details->id}}</td>
                     <td>{{$details->description}}</td>
                     <td>{{$details->ShoppingCategories->name}}</td>
                     <td>{{$details->reference}}</td>
                     <td>{{$details->quantity}}</td>
                     <td>$ {{$details->value}}</td>
                     <td>{{$details->ShoppingDetailsStatuses->name}}</td>

                  </tr>
                  
                  @endforeach
               </tbody>

            </table>
            <div class="col-md-6">
              <div class="note note-success">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px;" width="50%"  src="../{{$details->Shoppings->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Justificación {{$details->Shoppings->Users->name}} {{$details->Shoppings->Users->last_name}}:</span>
                  
                  <span class="pull-right"></span>
                  <p>
                      {{$details->Shoppings->justification}}
                  </p>
              </div>
            </div>

            <?php 
                   $traces2 = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status', 2)->get(); 
                   
            ?>
            <div class="col-md-6">
              <div class="note note-info">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px; padding:5px" width="50px" src="../{{$traces2[0]->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Commentario {{$traces2[0]->Users->name}} {{$traces2[0]->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$traces2[0]->created_at}}</span>
                  <p>
                      {{$traces2[0]->comment}}
                  </p>    
              </div>
            </div>
            
            <div class="row col-md-12">
            <hr>
            <h3 style="text-align:center">Cotizaciones </h3>
            <hr>
            
                <?php
                $cuotation = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                
                ?>
            
            <?php
                $cuotations = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
            ?>
                <table class="table table-bordered table-hover top-blue" id="">
                   <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre Proveedor</th>
                            <th>Nit</th>
                            <th>Valor Cotización</th>
                            <th>Observación</th>
                            <th>Cotizacion</th>
                           
                        </tr>
                   </thead>
                   <tbody id="archivos_subidos">
                      @foreach($cuotations as $cuotation)
                      
                      <tr id="cuotation_{{$cuotation->id}}" class=" <?php if($cuotation->status == 1){ echo "green"; } ?>" >
                         <td>COT{{str_pad($cuotation->id, 6, "0", STR_PAD_LEFT)}}</td>
                         <td>{{$cuotation->Providers->provider}}</td>
                         <td>{{$cuotation->Providers->nit}}</td>
                         <td>$ {{$cuotation->valor}}</td>
                         <td>{{$cuotation->observacion}}</td>
                         <td><a class="btn btn-default" style="@if($cuotation->file == '') display:none @endif " id="" href="../{{$cuotation->file}}" target="_blanc"><i class="icon-paper-clip"></i></a></td>
                         
                         
                      </tr>
                      
                      @endforeach
                   </tbody>

                   
                </table>
            
               <?php 
                      $traces = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status','<>', 2)->get(); 
                      $concepts = PaymentsMethods::get();
               ?>
               @foreach($traces as $trace)
               
               <div class="note note-success">
                   <a href="#" class="pull-left">
                       <img alt="" style="border-radius:50px; padding:5px" width="50px" src="../{{$trace->Users->img_min}}">
                   </a>
                   
                   <span class="text-success">Commentario {{$trace->Users->name}} {{$trace->Users->last_name}}:</span>
                   
                   <span class="pull-right">{{$trace->created_at}}</span>
                   <p>
                       {{$trace->comment}}
                   </p>    
               </div>
               
               @endforeach
                <div class="row">
                   <div class="col-md-6">
                      <div class="form-group has-success">
                         <label class="control-label">Comentario para el Usuario</label>
                         <div class="input-group">
                            <span class="input-group-addon"><i class="icon-time"></i></span>
                            
                            <textarea id="comment_aproved" placeholder="Comentario para el usuario" class="form-control"></textarea><br>
                         </div> 
                      </div>
                   </div>
                   <div class="col-md-6">
                      <div class="form-group has-success">
                         <label class="control-label">Fecha Aprox. de entrega</label>
                         <div class="input-group">
                            <span class="input-group-addon"><i class="icon-time"></i></span>
                            
                            <input type="text" placeholder="Fecha Aprox. de entrega (yyyy-mm-dd)" id="fecha_entrega" class="form-control fecha_required"><br>
                         </div> 
                      </div>
                   </div>
                </div>
                   
               <div class="note">
                @foreach($cuotations as $cuotation)
                    @if($cuotation->status == 1)
                         
                        <div class="alert alert-info">
                        <h4 class="block">Factura de Cotizacion <span class="label label-success">COT{{str_pad($cuotation->id, 6, "0", STR_PAD_LEFT)}} {{$cuotation->Providers->provider}}</span></h4>
                           <div class="col-md-6">
                              <div class="form-group has-success">
                                 <label class="control-label">Precio Final</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-time"></i></span>
                                    <input type="text" placeholder="Precio Final" id="precio_final_{{$cuotation->id}}" class="form-control money" value="{{$cuotation->valor}}"><br>
                                 </div> 
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group has-success">
                                 <label class="control-label">N° Factura</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-time"></i></span>
                                    <input type="text" placeholder="# De Factura" id="no_factura_{{$cuotation->id}}" class="form-control" value=""><br>
                                 </div> 
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group has-success">
                                 <label class="control-label">Fecha Pago</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-time"></i></span>
                                    
                                    <input type="text" placeholder="Fecha de Pago" id="fecha_pago{{$cuotation->id}}" class="form-control fecha_required fecha_pago_date" value=""><br>
                                 </div> 
                              </div>
                           </div>
                           <div class="col-md-6">
                              <div class="form-group has-success">
                                 <label class="control-label">Forma de Pago</label>
                                 <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-time"></i></span>
                                    <select id="forma_pago{{$cuotation->id}}" name="forma_pago" class="form-control">
                                              
                                      @foreach($concepts as $concept)
                                        @if($cuotation->Providers->payments_methods_id == $concept->id)
                                          <option value="{{$concept->id}}" selected="">{{$concept->name}}</option>
                                        @else
                                          <option value="{{$concept->id}}">{{$concept->name}}</option>
                                        @endif
                                      @endforeach
                                    </select>
                                 </div> 
                              </div>
                           </div>
                              
                                  
                                <input type="hidden" placeholder="Forma de Pago" id="provider_id{{$cuotation->id}}" class="form-control" value="{{$cuotation->providers_id}}"><br>
                            
                            
                            
                            
                                <input type="file" name="archivo" id="archivo_{{$cuotation->id}}" />
                            
                        </div>
                    @endif 
                    
                @endforeach
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
      
         <button type="button" class="btn btn-success" onclick="terminar_proceso({{$shopping_details[0]->shoppings_id}})">Enviar</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>
<script type="text/javascript">
    $("#fecha_entrega").datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
    $(".fecha_pago_date").datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
    
    function terminar_proceso(id) {
      var comment_aproved = $("#comment_aproved").val();
      var fecha_entrega = $("#fecha_entrega").val();
                            
        if (comment_aproved != "") {
            if (fecha_entrega != "") {
                
                @foreach($cuotations as $cuotation)
                @if($cuotation->status == 1)
                    
                    if($("#archivo_{{$cuotation->id}}").val()==""){
                       toastr.error('Debe subir un archivo', 'Error');
                       return;
                    }
                    if($("#precio_final_{{$cuotation->id}}").val()==""){
                       toastr.error('El campo de precio final esta vacio', 'Error');
                       return;
                    }
                    if($("#no_factura_{{$cuotation->id}}").val()==""){
                       toastr.error('El campo de N° factura esta vacio', 'Error');
                       return;
                    }
                    if($("#fecha_pago{{$cuotation->id}}").val()==""){
                       toastr.error('El campo Fecha de Pago esta vacio', 'Error');
                       return;
                    }
                    if($("#forma_pago{{$cuotation->id}}").val()==""){
                       toastr.error('El campo Forma de Pago esta vacio', 'Error');
                       return;
                    }
                    
                
                @endif
                @endforeach
                
              @foreach($cuotations as $cuotation)
                @if($cuotation->status == 1)
                    
                    var precio_final_{{$cuotation->id}} = $("#precio_final_{{$cuotation->id}}").val();
                    var no_factura_{{$cuotation->id}} = $("#no_factura_{{$cuotation->id}}").val();
                    var fecha_pago{{$cuotation->id}} = $("#fecha_pago{{$cuotation->id}}").val();
                    var forma_pago{{$cuotation->id}} = $("#forma_pago{{$cuotation->id}}").val();
                    var provider_id{{$cuotation->id}} = $("#provider_id{{$cuotation->id}}").val();
                    

                    if (precio_final_{{$cuotation->id}} != "") {

                        $("#archivo_{{$cuotation->id}}").upload('subirfactura', 
                        {
                            id_shopping:id,
                            comment_aproved: comment_aproved,
                            fecha_entrega: fecha_entrega,
                            precio_final: precio_final_{{$cuotation->id}},
                            no_factura: no_factura_{{$cuotation->id}},
                            fecha_pago: fecha_pago{{$cuotation->id}},
                            forma_pago: forma_pago{{$cuotation->id}},
                            provider_id:provider_id{{$cuotation->id}},
                            id_cuotation: {{$cuotation->id}}

                        },
                        function(respuesta) {
                            //Subida finalizada.
                            $("#barra_de_progreso_" + id).val(0);

                            if (respuesta === 1) {
                                    mostrarRespuesta('El archivo NO se ha podido subir.', false, id);
                            } else {
                                                                
                                //mostrarRespuesta(respuesta, true, id);                                    
                                toastr.success('Se actualizo correctamente la solicitud', 'Actualización');
                                $("#close_modal").click();
                                var type_event = ["pendingshopping", respuesta[0]["name"], respuesta[0]["last_name"], respuesta[0]["img_min"], respuesta[0]["id"], 6, id];
                                send(type_event);
                            }
                                                        
                        }, function(progreso, valor) {
                            //Barra de progreso.
                            $("#barra_de_progreso_" + id).val(valor);
                        });
                                                        
                                            
                    } else{
                        toastr.error('El campo Precio Final del proveedor {{$cuotation->Providers->provider}} no puede estar vacio.', 'Error');
                    }

                @endif
              @endforeach
            } else{
                toastr.error('El campo Fecha Aprox. de entrega no puede estar vacio', 'Error');
            }
        } else{
                toastr.error('El campo Comentario no puede estar vacio', 'Error');
        }
    }
</script>

