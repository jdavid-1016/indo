<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Aprobar Solicitudes de Compra</h4>
      </div>
      <div class="modal-body form-body">   
          
         <div class="table-responsive">
         
            <table class="table table-bordered table-hover table-striped top-blue" >
               <thead>
                  <tr>
                     
                     <th>id</th>
                     <th>Descripción</th>
                     <th>Categoria</th>
                     <th>Referencia</th>
                     <th>Cantidad</th>
                     <th>Valor</th>
                     <th>Estado</th>
                     
                     
                  </tr>
               </thead>
               <tbody>
                  @foreach($shopping_details as $details)
                  
                  <tr>
    
                     <td>{{$details->id}}</td>
                     <td>{{$details->description}}</td>
                     <td>{{$details->ShoppingCategories->name}}</td>
                     <td>{{$details->reference}}</td>
                     <td>{{$details->quantity}}</td>
                     <td>$ {{$details->value}}</td>
                     <td>{{$details->ShoppingDetailsStatuses->name}}</td>
                     
                     
                  </tr>
                  
                  @endforeach
               </tbody>
               
            </table>
            <div class="note note-success">
                <a href="#" class="pull-left">
                    <img alt="" style="border-radius:50px;" width="50%"  src="../{{$details->Shoppings->Users->img_min}}">
                </a>
                
                <span class="text-success">Justificación {{$details->Shoppings->Users->name}} {{$details->Shoppings->Users->last_name}}:</span>
                
                <span class="pull-right">{{$time}}</span>
                <p>
                    {{$details->Shoppings->justification}}
                </p>    
            </div>
            
             <hr>
             <div id="comentario" >
                <span>Observación:</span>
                <textarea class="form-control" placeholder="" id="comentario_refuse"></textarea>
              </div>
            
         </div>
      </div>   
      <div class="modal-footer">
        <button type="button" class="btn btn-success" id="aprobarsol" onclick="terminar_proceso({{$details->shoppings_id}}, 2)">Aprobar</button>
        <button type="button" class="btn btn-danger" onclick="terminar_proceso({{$details->shoppings_id}}, 5)" id="rechazarsol" >Rechazar</button>
        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>

<script type="text/javascript">

function terminar_proceso(id, estado) {
    
    var comentario = $('#comentario_refuse').val();
    
    if(comentario==""){
       toastr.error('Debe escribir una observacion', 'Error');
       return;
    }
    
       $.ajax({
           type: "GET",
           url: 'aprobarsol',
           data:{id_shopping:id, estado:estado, comentario:comentario} ,
           success: function(respuesta) {
               if (respuesta) {
                   toastr.success('Se Actualizo  la solicitud de compra correctamente', 'Actualización');
                   $("#close_modal").click();
                        var type_event = ["approvshopping", respuesta[0]["name"],respuesta[0]["last_name"],respuesta[0]["img_min"], respuesta[0]["id"], estado, id];
                        send(type_event);
                        
               }
           }
       });
   }
</script>