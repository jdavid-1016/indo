  <div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Mis Solicitudes de Compras</h4>
      </div>
      <div class="modal-body form-body">   
          
         <div class="table-responsive">
         
            <table class="table table-bordered table-hover table-striped top-blue" >
               <thead>
                  <tr>
                     
                     <th>id</th>
                     <th>Descripción</th>
                     <th>Categoria</th>
                     <th>Referencia</th>
                     <th>Cantidad</th>
                     <th>Valor</th>
                     <th>Estado</th>
                     
                     
                  </tr>
               </thead>
               <tbody>
                  @foreach($shopping_details as $details)
                  
                  <tr>
    
                     <td>{{$details->id}}</td>
                     <td>{{$details->description}}</td>
                     <td>{{$details->ShoppingCategories->name}}</td>
                     <td>{{$details->reference}}</td>
                     <td>{{$details->quantity}}</td>
                     <td>$ {{$details->value}}</td>
                     <td>{{$details->ShoppingDetailsStatuses->name}}</td>
                     
                     
                  </tr>
                  
                  @endforeach
               </tbody>

               
            </table>
            
                
            <div class="note note-success">
                <a href="#" class="pull-left">
                    <img alt="" style="border-radius:50px;padding:5px" width="50px"  src="../{{$details->Shoppings->Users->img_min}}">
                </a>
                
                <span class="text-success">Justificación {{$details->Shoppings->Users->name}} {{$details->Shoppings->Users->last_name}}:</span>
                
                <span class="pull-right">{{$time}}</span>
                <p>
                    {{$details->Shoppings->justification}}
                </p>    
            </div>



            <div class="row col-md-12">
            <hr>
            <h3 style="text-align:center">Proveedores </h3>
            <hr>
            
            
                <?php 
                $cuotation = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                
                ?>
                
                  
                  
                
                
            
            
            
            <?php 
                $cuotations = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
            ?>
                <table class="table table-bordered table-hover top-blue" id="">
                   <thead>
                        <tr>
                            <th>Cod.</th>
                            <th>Nombre Proveedor</th>
                            <th>Nit</th>
                            <th>Valor Cotización</th>
                            <th>Observación</th>
                            <th>Cotizacion</th>
                           
                        </tr>
                   </thead>
                   <tbody id="archivos_subidos">
                      @foreach($cuotations as $cuotation)
                      
                      <tr id="cuotation_{{$cuotation->id}}" class="@if($cuotation->status == 1)green @endif" >
                         <td>COT{{str_pad($cuotation->id, 6, "0", STR_PAD_LEFT)}}</td>
                         <td>{{$cuotation->Providers->provider}}</td>
                         <td>{{$cuotation->Providers->nit}}</td>
                         <td>$ {{$cuotation->valor}}</td>
                         <td>{{$cuotation->observacion}}</td>
                         <td><a class="btn btn-default" style="@if($cuotation->file == '') display:none @endif " id="" href="../{{$cuotation->file}}" target="_blanc"><i class="icon-paper-clip"></i></a></td>
                         
                         
                      </tr>
                      
                      @endforeach
                   </tbody>
                </table>
              </div>
            <br>
            <?php $traces = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->get(); ?>
            
            
            <div class="note note-warning">
                
                
                <span class="text-info">{{$details->Shoppings->ShoppingStatuses->name}}:</span>
                <span class="pull-right">{{$details->Shoppings->updated_at}}</span>
                
                <p>
                    {{$details->Shoppings->ShoppingStatuses->description}}
                </p>    
            </div>
            
            
            @if($shopping_details[0]->Shoppings->comment_aproved == "")
            @else
            <div class="row col-md-12">
               <div class="note note-warning">
                   <h4 class="block">Comentario:</h4>
                   <span class="pull-right">Fecha estimada de entrega: {{$shopping_details[0]->Shoppings->date_delivery}}</span>
                   <p>
                       {{$shopping_details[0]->Shoppings->comment_aproved}}
                   </p>
               </div>
              
            </div>
            @endif
            @if($shopping_details[0]->Shoppings->comment == "")
            @else
            <div class="row col-md-12">
               <div class="note note-warning">
                   <h4 class="block">Comentario:</h4>
                   <p>
                       {{$shopping_details[0]->Shoppings->comment}}
                   </p>
               </div>
              
            </div>
            @endif
            @if($shopping_details[0]->Shoppings->comment_refused == "")
            @else
            <div class="row col-md-12">
               <div class="note note-warning">
                   <h4 class="block">Comentario:</h4>
                   <p>
                       {{$shopping_details[0]->Shoppings->comment_refused}}
                   </p>
               </div>
              
            </div>
            @endif  
         </div>
      </div>   
      <div class="modal-footer">
      
         
      
      
      
      
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>
<script type="text/javascript">
    function mostrarRespuesta(mensaje, ok, id){
       $("#respuesta_"+id).removeClass('alert-success').removeClass('alert-danger').html(mensaje);
       if(ok){
           $("#respuesta_"+id).addClass('alert-success');
       }else{
           $("#respuesta_"+id).addClass('alert-danger');
       }
    }
   function subirArchivos(id) {
       $("#archivo").upload('subricotizacion',
       {
            id_shopping:id,
            nombre_pro: $("#nombre_pro").val(),
            nit: $("#nit").val(),
            valor_cot: $("#valor_cot").val(),
            observacion: $("#observacion").val()
       },
       function(respuesta) {
           //Subida finalizada.
           $("#barra_de_progreso_"+id).val(0);
           if (respuesta === 1) {
               mostrarRespuesta('El archivo NO se ha podido subir.', false, id);
           } else {
               mostrarRespuesta(respuesta, true, id);
               $("#nombre_archivo_"+id+", #archivo_"+id).val('');
           }
           $("#respuesta").html(respuesta);
           mostrarArchivos(id);
       }, function(progreso, valor) {
           //Barra de progreso.
           $("#barra_de_progreso_"+id).val(valor);
       });
   }

   function mostrarArchivos(id) {
       $.ajax({
           type: "GET",
           url: 'mostrararchivos',
           data:{id_quotation:id} ,    
           success: function(respuesta) {
               if (respuesta) {
                   var html = '';
                   for (var i = 0; i < respuesta.length; i++) {
                       if (respuesta[i] != undefined) {
                           html += '<div class="row"> <span class="col-lg-2"> ' + respuesta[i] + ' </span> <div class="col-lg-2"> <a class="eliminar_archivo btn btn-danger" href="javascript:void(0);"> Eliminar </a> </div> </div> <hr />';
                       }
                   }
                   $("#archivos_subidos").html(respuesta);
                   $("#nombre_pro").val('');
                   $("#nit").val('');
                   $("#valor_cot").val('');
                   $("#observacion").val('');
                   $("#archivo").val('');
               }
           }
       });
   }
   function terminar_proceso(id) {
       $.ajax({
           type: "GET",
           url: 'terminarproceso',
           data:{id_shopping:id} ,    
           success: function(respuesta) {
               if (respuesta) {
                   toastr.success('Se Enviaron las cotizaciones correctamente', 'cotizaciones');
               }
           }
       });
   }
   function delete_cuotation(id) {
       $.ajax({
           type: "GET",
           url: 'deletecuotation',
           data:{cuotation:id} ,    
           success: function(respuesta) {
               if (respuesta) {
                   toastr.success('Se Elimino la cotización correctamente', 'cotizaciones');
                   $("#cuotation_"+id).css("display", "none");
               }
           }
       });
   }
   function funcion2(hola){
      var hola = hola;

     
      $('#ocult'+hola).toggle('slow');
      mostrarArchivos(hola);
   } 
</script>

<style type="text/css">
    
    #mensaje{
      border:dashed 1px red;
      background-color:#FFC6C7;
      color: #000000;
      padding: 10px;
      text-align: left;
      margin: 10px auto; 
      display: none;//Al cargar el documento el contenido del mensaje debe estar oculto
    }
  </style>