
<div class="page-content" id="adminhelpdesk">
   <div class="row">
       <div class="col-md-12">
           <h3 class="page-title">
               Mis Solicitudes de compras
           </h3>
           <ul class="page-breadcrumb breadcrumb">
               <li>
                   <i class="icon-home"></i>
                   <a href="javascript:;">Página Principal</a> 
                   <i class="icon-angle-right"></i>
               </li>
               <li>
                   <a href="javascript:;">Compras</a>
                   <i class="icon-angle-right"></i> 
               </li>
               <li>
                   <a href="javascript:;">Mis Solicitudes</a> 
               </li>
           </ul>
       </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         
         <div class="table-responsive">
            <div class="tabbable tabbable-custom">  

            
               <ul class="nav nav-tabs">
                   <li class=""><a href="solicitudes">Nueva solicitud</a></li>
                   <li class="active"><a href="misolicitudescompras">Mis solicitudes</a></li>
                   <li class=""><a href="historicocompras">Histórico</a></li>
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_supports">
                      <!-- <div>
                         <form class="form-inline" action="admin_history" method="get">
                            <div class="search-region">
                               <div class="form-group">
                                  <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-medium" name="applicant">
                                  <option value="">Solicitó</option> 
                              
                                  <option value="" selected=""></option>
                               
                                  
                             
                                  
                                  
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-medium" name="responsible">
                                  <option value="">Responsable</option>   
                                  
                                     <option value="" selected=""></option>
                                  
                                  
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-small" name="status">
                                     <option value="">Estado</option>
                                     <option  value="2">cerrado</option>
                                     <option  value="3">rechazado</option>
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-small" name="priority">
                                     <option value="">Prioridad</option>
                                     <option value="1">1</option>
                                     <option value="2">2</option>
                                     <option value="3">3</option>
                                     <option value="4">4</option>
                                     <option value="5">5</option>
                                  </select>
                               </div>
                               <div class="form-group">
                                  <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                               </div>    
                            </div>
                         </form> 
                      </div> -->
                        
                        <table class="table top-blue" data-target="soporte/callshopping/">
                           <thead>
                              <tr>
                                 <th></th>
                                 <th>
                                    Id 
                                 </th>
                                 <th>Editar</th>
                                 <th>Solicitó</th>
                                 
                                 <th>Justificacion</th>
                                 <th>Estado</th>
                                 <th>Proceso</th>
                                 <th>Creado</th>
                                 <th class="td_center"><i class="icon-time"></i></th>
                                 
                                 <!--<th class="td_center"><i class="icon-time"></i></th>-->
                              </tr>
                           </thead>
                           <tbody>
                           @foreach($shoppings as $shopping)
                              <tr class="{{$shopping['row_color']}}" style="" id="" >
                                 <td>
                                    <i class="" id="" onclick="funcion2($(this).attr('id'));return false;"></i>
                                 </td>
                                 <td class="td_center">
                                    {{$shopping['id']}}
                                 </td>
                                 <td class="td_center">
                                    <a class=" btn btn-default" style="" data-target="#ajax" id="{{$shopping['hid']}}" data-toggle="modal" onclick='cargarDetalleCompra2($(this).attr("id"));return false;'><i class="icon-edit"></i></a>
                                 </td>
                                 <td class="td_center">
                                    {{$shopping['user']}}
                                 </td>
                                 
                                 <td class="td_center">
                                 <div class="comment more">
                                    {{$shopping['justification']}}
                                    
                                 </div>
                                 </td>
                                 <td class="td_center">
                                    <span class="label label-sm  {{$shopping['label']}} informacion" title="{{$shopping['description'] }}">{{$shopping['state'] }}</span>
                                 </td>
                                 <td class="td_center">
                                 {{$shopping['profil']}}
                                    
                                 </td>
                                 <td class="td_center">
                                    {{$shopping['created_at']}}
                                 </td>
                                 <td class="td_center">
                                    <div class="alert alert-warning">
                                       {{$shopping['tiempo']}}
                                       
                                    </div>
                                 </td>
                                 
                              </tr>
                           @endforeach
                           </tbody>
                        </table>

                        <div class="pagination">
                        </div>
                       
                        
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>
<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });    
  });
</script>
   
   <script type="text/javascript">
   
   function cargarDetalleCompra2(id){
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'misolicitudescomprasdetalle',
         
         type: 'get',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }

  
   </script>