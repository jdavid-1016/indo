<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Mis Solicitudes de compras
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Compras</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Mis Solicitudes</a> 
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="solicitudes">Nueva solicitud</a></li>
                        <li class=""><a href="misolicitudescompras">Mis solicitudes</a></li>
                        <li class=""><a href="historicocompras">Histórico</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                            <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                        </div>
                        <div class="tab-pane active" id="admin_suppotrs">

                            <form class="form-horizontal">
                                <div class="form-body">

                                    <div class="form-group">


                                        <!-- BEGIN PAGE CONTENT-->
                                        <div class="row">
                                            <div class="col-md-12">
                                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                                <div class="portlet">
                                                    <div class="portlet-title">
                                                        <div class="caption"><i class="icon-cogs"></i>Items</div>
                                                        <div class="actions">
                                                            <button type="button" onclick="NuevoItem()" class="btn btn-success"><i class="icon-plus"></i> Agregar</button>
                                                        </div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <div class="table-responsive">
                                                            <table class="table table-striped table-bordered table-hover top-blue">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Fecha en que se requiere</th>
                                                                        <th>Categoria</th>
                                                                        <th>Descripcion</th>
                                                                        <th>Cantidad</th>
                                                                        <th>Referencia</th>
                                                                        <th>Precio</th>
                                                                        <th>Acciones</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="bodytable">                                                                    
                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- END EXAMPLE TABLE PORTLET-->
                                            </div>
                                        </div>

                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-1 control-label">Justificacion de la compra</label>
                                        <div class="col-md-5">
                                            <textarea name="justificacion" id="justificacion" class="form-control" rows="5"></textarea>
                                        </div>
                                    </div> 
                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-1 col-md-11">
                                        <button type="button" onclick="GuardarNuevaCompra()" class="btn btn-success">Enviar</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="modal fade" id="nuevoitem" tabindex="-1" role="basic" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Nuevo Item</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form id="formcompras">
                                            <div class="form-group">
                                                <label class="control-label">Fecha</label>
                                                <div class="input-group">
                                                    <input name="fecha2" id="fecha2" class="form-control fecha_required" type="text" value="">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Descripcion</label>
                                                <div class="input-group">
                                                    <textarea class="form-control" id="descripcion" name="descripcion" rows="3" ></textarea>
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Cantidad</label>
                                                <div class="input-group">
                                                    <input name="cantidad" id="cantidad" class="form-control number" type="text" value="">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Referencia</label>
                                                <div class="input-group">
                                                    <input name="referencia" id="referencia" class="form-control" type="text" value="">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Precio</label>
                                                <div class="input-group">
                                                    <input name="precio" id="precio" class="form-control money" type="text" value="">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                </div>
                                            </div>
                                        </form>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-info" onclick="GuardarItem()">Crear</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="scriptcalendar">
    <script>
        $.ajax({
             type: "GET",
             url: "../compras/categorias",
             data: { cont:"1"}
             })
             .done(function( data ) {
             categorias = data;
         });
        $(function() {
            $(".fecha_required").datepicker({ dateFormat: "yy-mm-dd" });
        });
    </script>
</div>