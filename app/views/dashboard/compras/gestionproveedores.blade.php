<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Gestion Proveedores
            </h3>            
        </div>
    </div>
    <div class="portlet-body">
        <ul  class="nav nav-tabs">
            <li class=""><a href="gestion">Solicitudes</a></li>
            <li class=""><a href="gestionhist" >Histórico</a></li>
            <li class="active"><a href="gestionprov" >Proveedores</a></li>

        </ul>

        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <ul class="page-breadcrumb breadcrumb">
                        <h4><b>Formulario para nuevo proveedor</b></h4>
                    </ul>
                </div>
                <form class="form-horizontal" id="form">
                    <div class="form-body">
                        <div class="scroller" style="height:300px" data-always-visible="1" data-rail-visible1="1">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">NIT*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="nit" onfocus="focusFunctionNitp()" onblur="blurFunctionNitp()" required="">
                                            <div id="displaynitp"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Sucursal</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="branch_office" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa sucursal</label>
                                        <div class="col-md-8">                                            
                                            <select type="text" class="form-control" id="branch_office_usa" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Proveedor*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="provider" onfocus="focusFunctionp()" onblur="blurFunctionp()" required="">
                                            <div id="displayprovp"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Contacto*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="contact" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Direccion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="address" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 1*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone1" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 2</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone2" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 3</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone3" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 4</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone4" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">FAX</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="fax" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Apartado Aereo</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="air_section" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="email" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Genero</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="genders_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Masculino</option>
                                                <option value="3">Femenino</option>
                                                <option value="4">Empresa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nacimiento</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="birth" required="" placeholder="aaaa-mm-dd">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Digito de verificacion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="digit_of_verification" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">ID. Tributaria*</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="tax_ids_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">NIT</option>
                                                <option value="3">Cedula</option>
                                                <option value="4">RUC</option>
                                                <option value="5">Codigo identificacion</option>
                                                <option value="6">Pasaporte</option>
                                                <option value="7">Cedula extranjera</option>
                                                <option value="8">Tarjeta de identidad</option>
                                                <option value="9">Registro civil</option>
                                                <option value="10">Tarjeta extranjera</option>
                                                <option value="11">Numero identificacion personal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo del pais</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="country_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo de la ciudad</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="city_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Clas. tributaria*</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="tax_classifications_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Gran contribuyente</option>
                                                <option value="3">Empresa del estado</option>
                                                <option value="4">Regimen comun</option>
                                                <option value="5">Regimen simplificado</option>
                                                <option value="6">Regimen simplificado no residente pais</option>
                                                <option value="7">No residente pais</option>
                                                <option value="8">no responsable IVA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Actv. economica</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="economic_activity" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo persona juridica</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="legal_person_types_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Natural</option>
                                                <option value="3">Juridica</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Observaciones</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="observations" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa agente retenedor</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="withholding_agent" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Es beneficiario RETEIVA</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="beneficiary_reteiva" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Es agente RETEICA</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="agent_reteica" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Es declarante</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="declarant" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">                                                                                                            
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa retencion</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="uses_retention" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Estado</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="status" required="">
                                                <option value="0">Desactivo</option>
                                                <option value="1" selected>Activo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa Razon Social</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="uses_social_reason" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo EAN</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="ean_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Primer nombre</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="first_name" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Segundo nombre</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="middle_name" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Primer apellido</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="last_name" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Segundo apellido</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="last_name2" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Forma de pago</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="method_of_payment" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Calificacion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="rating" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">% de descuento</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="discount_percentage" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Periodo de pago</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="payment_period" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Dias optimista</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="optimistic_days" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Dias pesimista</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="pessimistic_days" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">ID. extranjera</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="foreign_identification" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo identificacion fiscal</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="tax_identification_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo empresa</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="company_type" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Autoriza reportar entidades crediticias</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="authorizes_lenders_report" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tarifa diferencial RETEIVA compras</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="differential_rate_reteiva_shopping" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tarifa diferencial RETEIVA ventas</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="differential_rate_reteiva_sales" required="">
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Metodo de pago*</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="payments_methods_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Cheque</option>
                                                <option value="3">Transferencia</option>
                                                <option value="4">Efectivo</option>
                                                <option value="5">T. credito</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Banco</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="bank" required="" value="0">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo de cuenta</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="account_type" required="">
                                                <option value="0">No aplica</option>
                                                <option value="Ahorros">Ahorros</option>
                                                <option value="Corriente">Corriente</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">No. de cuenta</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="account_number" required="" value="0">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>                                
                        <div class="form-actions fluid">
                            <div class="col-md-1 col-md-11">
                                <a class="btn btn-success" onclick="guardarprov()">Guardar</a>
                            </div>
                        </div>                        
                    </div>
                </form>
            </div>

            <div class="col-md-6">
                <div class="col-md-12">
                    <ul class="page-breadcrumb breadcrumb">
                        <h4><b>Importar desde excel</b></h4>
                    </ul>
                </div>
                <form class="form-horizontal" id="formarcprov" action="guardararcprov" method="POST" enctype="multipart/form-data">
                    <div class="form-body">
                        <br><br>

                        <div class="form-group">
                            <label class="col-md-2 control-label">Archivo</label>
                            <div class="col-md-3">
                                <input type="file" id="archivo" name="archivo" required/>
                            </div>
                        </div>
                        <br>
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-1 col-md-11">
                            <a class="btn btn-success" onclick="guardarprovarc()">Subir Archivo</a>
                            <a class="btn btn-info" onclick="exportarprov()">Exportar</a>
                        </div>
                    </div>

                </form>

            </div>
        </div>



    </div>

    <script>
        function guardarprovarc() {            
                  $("#formarcprov").submit();
        }

        function exportarprov() {
            window.open("exportarprov");
        }
    </script>
    <script type="text/javascript">    
   

</script>