  
<table class="table top-blue" data-target="soporte/callshopping/">
   <thead>
      <tr>
         <th></th>
         <th>
            Id 
         </th>
         <th>Editar</th>
         <th>Solicitó</th>
         <th>Aprobó</th>
         <th>Proceso</th>
         
         <th>Justificacion</th>
         <th>Estado</th>
         <th>Creado</th>
         <th class="td_center"><i class="icon-time"></i></th>
         
         <!--<th class="td_center"><i class="icon-time"></i></th>-->
      </tr>
   </thead>
   <tbody>
   @foreach($shoppings as $shopping)
      <tr class="{{$shopping['row_color']}}" style="" id="" >
         <td>
            <i class="" id="" onclick="funcion2($(this).attr('id'));return false;"></i>
         </td>
         <td class="td_center">
            {{$shopping['id']}}
         </td>
         <td class="td_center">
            <a class=" btn btn-default" style="" data-target="#ajax" id="{{$shopping['hid']}}" data-toggle="modal" onclick='@if($shopping["state"] == "Aprobada")cargarDetalleCompra2($(this).attr("id"));return false; @elseif($shopping["state"] == "Pendiente Aprobacion") cargarDetalleCompra2($(this).attr("id"));return false; @else cargarDetalleCompra($(this).attr("id"));return false; @endif '><i class="icon-edit"></i></a>
         </td>
         <td class="td_center">
            {{$shopping['user']}}
         </td>
         <td class="td_center">
            {{$shopping['user2']}}
         </td>
         <td class="td_center">
            {{$shopping['profil']}}
            
         </td>
         
         <td class="td_center">
         <div class="comment more">
            {{$shopping['justification']}}
            
         </div>
         </td>
         <td class="td_center">
             <span class="label label-sm  {{$shopping['label']}} informacion" title="{{$shopping['description'] }}">{{$shopping['state'] }}</span>
         </td>
         <td class="td_center">
            {{$shopping['created_at']}}
         </td>
         <td class="td_center">
            <div class="alert alert-warning">
               {{$shopping['tiempo']}}
               
            </div>
         </td>
         
         
         
      </tr>
   @endforeach
   </tbody>
</table>

<div class="pagination">
</div>
<script type="text/javascript">
function cargarDetalleCompra(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  'detalle2',
      type: 'get',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
</script>
<script type="text/javascript">
function cargarComments(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  'cargarcomments',
      type: 'get',
      cache: false,

      success: function(response){
            $("#chat_messages").html(response);
                    var cont = $('#chats');
                    var list = $('.chats', cont);
                    $('.scroller', cont).slimScroll({
                    scrollTo: list.height()
            });
      }
   });
}
</script>
<script type="text/javascript">
 function enviarmsj() {
     event.preventDefault();
     var cont = $('#chats');
     var list = $('.chats', cont);
     var mensaje = $("#mensaje_nuevo").val();
     var id = $("#sopping_comversation").val();
     var id_user = {{Auth::user()->id}};
     
     mensaje = str_replace(":)", "<img src= ../assets/img/emoticons/biggrin.gif>", mensaje);
     mensaje = str_replace(":(", "<img src= ../assets/img/emoticons/crybaby.gif>", mensaje);
     $.ajax({
     type: "GET",
             url: "nuevomsj",
             data: { id: id, mensaje: mensaje, id_user_chat : id_user}
     })
     .done(function(data) {
         $("#id_chat").val(data[3]);
     if (data != 0){
       $("#comment_vacio").css('display', 'none');
       $("#comment_vacio2").css('display', 'none');
     var nuevomsj = '<li class="out">\n\
                     <img class="avatar img-responsive" alt="" src="../{{Auth::user()->img_min}} " />\n\
                     <div class="message"><span class="arrow"></span><a href="#" class="name">{{Auth::user()->name}} {{Auth::user()->last_name}}</a><span class="datetime"> at '+data[2]+'</span>\n\
                     <span class="body">' + mensaje + '</span></div></li>';
     
         $(".chats").append(nuevomsj);
         $("#mensaje_nuevo").val("");
         $('.scroller', cont).slimScroll({
         scrollTo: list.height()

     });

         if(id==0){
             $(".chats").attr("id", "chat_int_"+data[3]);
             var nuevomsj2 = '<li id="chat_'+data[3]+'" onclick="conversacion('+data[3]+')">\n\
                                     <a data-toggle="tab" href="#tab_1-1"><i><img class="avatar img-responsive" alt="" src="' + data[4] + '" /></i>' + data[5] + '</a>\n\
                                     </li>';

             $(".ver-inline-menu").prepend(nuevomsj2);
             
             setTimeout(function(){
                 $("#chat_"+data[3]).click();
                 $("#chat_"+data[3]).addClass("active");
             }, 500);
             
             var type_event = ["nuevaconv", data[0], data[1], $("#img_min_chat").val(), $("#nombre_chat").val()];
         }else{        
         var type_event = ["nuevochat", data[0], data[1], $("#img_min_chat").val(), $("#nombre_chat").val()];
         }
         send( type_event);
     } else{
         alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
     }
     });
 }
 function str_replace(search, replace, subject, count) {
   //  discuss at: http://phpjs.org/functions/str_replace/
   // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   // improved by: Gabriel Paderni
   // improved by: Philip Peterson
   // improved by: Simon Willison (http://simonwillison.net)
   // improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   // improved by: Onno Marsman
   // improved by: Brett Zamir (http://brett-zamir.me)
   //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
   // bugfixed by: Anton Ongson
   // bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
   // bugfixed by: Oleg Eremeev
   //    input by: Onno Marsman
   //    input by: Brett Zamir (http://brett-zamir.me)
   //    input by: Oleg Eremeev
   //        note: The count parameter must be passed as a string in order
   //        note: to find a global variable in which the result will be given
   //   example 1: str_replace(' ', '.', 'Kevin van Zonneveld');
   //   returns 1: 'Kevin.van.Zonneveld'
   //   example 2: str_replace(['{name}', 'l'], ['hello', 'm'], '{name}, lars');
   //   returns 2: 'hemmo, mars'

   var i = 0,
     j = 0,
     temp = '',
     repl = '',
     sl = 0,
     fl = 0,
     f = [].concat(search),
     r = [].concat(replace),
     s = subject,
     ra = Object.prototype.toString.call(r) === '[object Array]',
     sa = Object.prototype.toString.call(s) === '[object Array]';
   s = [].concat(s);
   if (count) {
     this.window[count] = 0;
   }

   for (i = 0, sl = s.length; i < sl; i++) {
     if (s[i] === '') {
       continue;
     }
     for (j = 0, fl = f.length; j < fl; j++) {
       temp = s[i] + '';
       repl = ra ? (r[j] !== undefined ? r[j] : '') : r[0];
       s[i] = (temp)
         .split(f[j])
         .join(repl);
       if (count && s[i] !== temp) {
         this.window[count] += (temp.length - s[i].length) / f[j].length;
       }
     }
   }
   return sa ? s : s[0];
 }
</script>
<div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog  modal-wide " ng-controller="help_deskCtrl" id="responsible">
      <div class="col-md-8">
          <div class="tab-content">
              <div id="tab_1-1" class="tab-pane active" >
                  <div class="portlet" id="nuevo_chat">
                      <div class="portlet-title">
                          <div class="caption"><i class="icon-comments"></i>Conversacion</div>
                      </div>
                      <div class="portlet-body" id="chats">
                          <div class="scroller" style="height: 500px;" data-always-visible="10" data-rail-visible1="10" id="chat_messages">
                                  <h2 style="text-align: center;">Seleccione una conversacion</h2>
                                  <img width="50px" src="../assets/img/cargando.gif" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px;">
                          </div>                                            
                          <div class="chat-form">
                              <div class="input-cont">
                                  <input class="form-control" id="mensaje_nuevo" type="text" value="" placeholder="Escribe tu mensaje aquí..." />
                              </div>
                              <div class="btn-cont" onclick="enviarmsj(@if(count($shoppings) != 0){{$shopping['cid']}}@else @endif)"> 
                                  <span class="arrow"></span>
                                  <a class="btn btn-primary icn-only"><i class="icon-ok icon-white"></i></a>
                              </div>
                          </div>

                      </div>
                  </div>                                    


              </div>

          </div>
      </div>
   </div>
</div>