<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Administrar solicitudes de compras</h4>
      </div>
      <div class="modal-body form-body">   
          
         <div class="table-responsive">
         
            <table class="table table-bordered table-hover table-striped top-blue table-responsive">
               <thead>
                  <tr>
                     <th>id</th>
                     <th>Descripción</th>
                     <th>Referencia</th>
                     <th>Cantidad</th>
                     <th>Valor</th>
                     <th>Estado</th>
                  </tr>
               </thead>
               <tbody>
                    <?php 
                        $total = 0;
                    ?>
                    @foreach($shopping_details as $details)
                    <?php 
                        
                        $total  = $total + str_replace(".", "", $details->value);
                    ?>
                    <tr>
                        <td>{{$details->id}}</td>
                        <td>{{$details->description}}</td>
                        <td>{{$details->reference}}</td>
                        <td>{{$details->quantity}}</td>
                        <td>$ {{$details->value}}</td>
                        <td>
                            <input type="checkbox" class="check" id="status_{{$details->id}}" value="1">Aprobar
                        </td>
                    </tr>
                    @endforeach
                    <thead>
                    <tr>
                        <th colspan="4">Total Compra</th>
                        
                        <th>$ {{ number_format($total, 0, ',', '.');}}</th>
                        <th><input type="checkbox" value="1" id="approve_all" onclick="OnChangeCheckbox (this)">Aprobar Todos</th>
                    
                    </tr>
                    </thead>
                </tbody>
                
            </table>
            <div class="col-md-6">
              <div class="note note-success">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px;" width="50%"  src="../{{$details->Shoppings->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Justificación {{$details->Shoppings->Users->name}} {{$details->Shoppings->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$time}}</span>
                  <p>
                      {{$details->Shoppings->justification}}
                  </p>    
              </div>
            </div>
            <?php $traces2 = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status', 2)->get(); ?>
            <div class="col-md-6">
              <div class="note note-info">
                  <a href="#" class="pull-left">
                      <img alt="" style="border-radius:50px;" width="50%"  src="../{{$traces2[0]->Users->img_min}}">
                  </a>
                  
                  <span class="text-success">Commentario {{$traces2[0]->Users->name}} {{$traces2[0]->Users->last_name}}:</span>
                  
                  <span class="pull-right">{{$traces2[0]->created_at}}</span>
                  <p>
                      {{$traces2[0]->comment}}
                  </p>    
              </div>
            </div>




            <input type="hidden" value="{{$details->shoppings_id}}" id="id_shopping">

            <div class="row col-md-12">
            <hr>
            <h3 style="text-align:center">Proveedores </h3>
            <hr>

                <?php
                    $cuotations = ShoppingQuotation::where("shoppings_id", $shopping_details['0']->shoppings_id)->get();
                ?>
                <table class="table table-bordered table-hover top-blue table-responsive" id="">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Nombre Proveedor</th>
                            <th>Nit</th>
                            <th>Valor Cotización</th>
                            <th>Observación</th>
                            <th>Cotizacion</th>
                            <th><i class="icon-ok"></i></th>
                           
                        </tr>
                    </thead>
                    <tbody id="archivos_subidos">
                        @foreach($cuotations as $cuotation)
                      
                        <tr id="cuotation_{{$cuotation->id}}" class="<?php if($cuotation->recommended == 1){ echo "green"; }?>">
                            <td>COT{{str_pad($cuotation->id, 6, "0", STR_PAD_LEFT)}}</td>
                            <td>{{$cuotation->Providers->provider}}</td>
                            <td>{{$cuotation->Providers->nit}}</td>
                            <td>$ {{$cuotation->valor}}</td>
                            <td>{{$cuotation->observacion}}</td>
                            <td><a class="btn btn-default" style="@if($cuotation->file == '') display:none @endif " id="" href="../{{$cuotation->file}}" target="_blanc"><i class="icon-paper-clip"></i></a></td>
                            <td><input type="checkbox" name="aprobar" id="status_cot_{{$cuotation->id}}" value="1" class=""></td>
                         
                        </tr>
                      
                        @endforeach
                           
                        <?php $traces = ShoppingTraces::where("shoppings_id", $shopping_details['0']->shoppings_id)->where('status', '<>', 2)->get(); ?>
                    </tbody>

                   
                </table>
                        @foreach($traces as $trace)
                        
                        <div class="note note-success">
                            <a href="#" class="pull-left">
                                <img alt="" style="border-radius:50px;" width="50%"  src="../{{$trace->Users->img_min}}">
                            </a>
                            
                            <span class="text-success">Commentario {{$trace->Users->name}} {{$trace->Users->last_name}}:</span>
                            
                            <span class="pull-right">{{$trace->created_at}}</span>
                            <p>
                                {{$trace->comment}}
                            </p>    
                        </div>
                        
                        @endforeach
                <hr>
                <span>Observación:</span>
                <textarea class="form-control" placeholder="" id="comment"></textarea>
            
            
              
            </div>
         </div>
      </div>   
      <div class="modal-footer">
            <button type="button" class="btn btn-danger" onclick="gestion_compras_rechazar()" >Rechazar Solicitud</button>
            <button type="button" class="btn btn-success" onclick="gestion_compras()" >Aceptar Solicitud</button>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>
   </div>
</div>

<script type="text/javascript">
    
    function OnChangeCheckbox (checkbox) {
            if (checkbox.checked) {
                $(".check").attr('checked', true);
            }
            else {
                $(".check").attr('checked', false);
            }
    }

   function gestion_compras(){ 
       var estado_gen = 5;
       var validacion_items = 0;
       var validacion_proveedores = 0;
       var validacion_general = 0;
       var estados = new Array();
       var ids = new Array();
       var i = 0;
      @foreach($shopping_details as $details)
       
       ids[i] = {{$details->id}};
       
       if($("#status_{{$details->id}}").is(':checked')) {  
            estados[i] = 2; 
            validacion_items=1;
        } else {
            estados[i] = 3; 
        }

       if (estados[i] == "") {
         toastr.error('No ha sido posible actualizar la solicitud de compra', 'Error');         
       }else{
           i++;
       }

      @endforeach
      
        var estados_cot = new Array();
        var ids_cot = new Array();
        var i_cot = 0;

      @foreach($cuotations as $cuotation)
        ids_cot[i_cot] = {{$cuotation->id}};

        if($("#status_cot_{{$cuotation->id}}").is(':checked')) {
             estados_cot[i_cot] = 1;
             validacion_proveedores = 1;
         } else {
             estados_cot[i_cot] = 0;
         }
         
        i_cot++;

      @endforeach

      var comment = $("#comment").val();
      var id_shopping = $("#id_shopping").val();
      i=0;
      
      if(validacion_items==1){
          if(comment!=""){
              if(validacion_proveedores==0){
                  var r = confirm("¿ Desea continuar sin elegir un proveedor ?");
                    if (r == false) {
                        return;
                    }
              }
              validacion_general = 1;
              estado_gen=4;
          }else{
            toastr.error('Debe escribir una observacion', 'Error');
          }
      }else if(validacion_items==0 ){
          toastr.error('Para aceptar la solicitud debe seleccionar al menos un item', 'Error');
      }
           
           if(validacion_general==1){
                $.ajax({
                type: "post",
                url: "gestion",
                data: { id_details: ids, status: estados, estado_gen: estado_gen, id_cot:ids_cot, status_cot:estados_cot, comment:comment, id_shopping:id_shopping}
                })
                .done(function( data ) {
                    if(data=="2"){
                      toastr.error('No ha sido posible actualizar la solicitud', 'Error');
                    }else{
                        toastr.success('Se actualizo la solicitud correctamente', 'Actualizacion');
                        $("#close_modal").click();
                        
                       var type_event = ["gestshopping", data[0]["name"],data[0]["last_name"],data[0]["img_min"], data[0]["id"], estado_gen, id_shopping];
                       send(type_event);
                    }
                });
           }
         
   }
   
      function gestion_compras_rechazar(){
       var estado_gen = 5;
       var validacion_items = 0;
       var validacion_proveedores = 0;
       var validacion_general = 0;
       var estados = new Array();
       var ids = new Array();
       var i = 0;
      @foreach($shopping_details as $details)
       
       ids[i] = {{$details->id}};
       
       if($("#status_{{$details->id}}").is(':checked')) {
            estados[i] = 3;
        } else {
            estados[i] = 3;
        }

       if (estados[i] == "") {
         toastr.error('No ha sido posible actualizar la solicitud de compra', 'Error');
       }else{
           i++;
       }

      @endforeach
      
        var estados_cot = new Array();
        var ids_cot = new Array();
        var i_cot = 0;

      @foreach($cuotations as $cuotation)
        ids_cot[i_cot] = {{$cuotation->id}};

        if($("#status_cot_{{$cuotation->id}}").is(':checked')) {
             estados_cot[i_cot] = 0;
         } else {
             estados_cot[i_cot] = 0;
         }
         
        i_cot++;

      @endforeach

      var comment = $("#comment").val();
      var id_shopping = $("#id_shopping").val();
      i=0;
      
      if(comment!=""){
              validacion_general = 1;
          }else{
            toastr.error('Debe escribir una observacion para rechazar la solicitud', 'Error');
          }
           
           if(validacion_general==1){
                $.ajax({
                type: "post",
                url: "gestion",
                data: { id_details: ids, status: estados, estado_gen: estado_gen, id_cot:ids_cot, status_cot:estados_cot, comment:comment, id_shopping:id_shopping}
                })
                .done(function( data ) {
                    if(data=="2"){
                      toastr.error('No ha sido posible actualizar la solicitud', 'Error');
                    }else{                        
                        toastr.success('Se actualizo la solicitud correctamente', 'Actualizacion');
                        $("#close_modal").click();
                        
                       var type_event = ["gestshopping", data[0]["name"],data[0]["last_name"],data[0]["img_min"], data[0]["id"], estado_gen, id_shopping];
                       send(type_event);
                    }              
                });
           }
         
   }
</script>