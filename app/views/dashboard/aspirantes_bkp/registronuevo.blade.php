<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Indoamericana</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="MobileOptimized" content="320">
        <link href="{{URL::to("logo.ico")}}" rel="shortcut icon" >
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
        <link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/select2/select2_conquer.css")}}" />
        <link rel="stylesheet" href="{{  URL::to("assets/plugins/data-tables/DT_bootstrap.css")}}" />

        <link rel="stylesheet" href="../js/tragator/fm.tagator.jquery.css"/>


        <!-- END THEME STYLES -->   
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->   
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->  
                <a class="navbar-brand" href="">
                    <img src="{{URL::to("assets/img/logo.png")}}" alt="logo" style="margin-top: -15px"class="img-responsive" />
                </a>
                <!--<form class="search-form search-form-header" role="form" action="index.html" >
                   <div class="input-icon right">
                      <i class="icon-search"></i>
                      <input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
                   </div>
                </form>-->
                <h2 style="color: #ffffff">Formulario De Registro Aspirantes</h2>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <img src="{{URL::to("assets/img/menu-toggler.png")}}" alt="" />
                </a> 
                <!-- END RESPONSIVE MENU TOGGLER -->

                <!-- BEGIN TOP NAVIGATION MENU -->        

                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>

        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <!----------------------------------------------------------------------------------HEAD---------------------------------------------------------------------------------------->
        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <hr><hr>
        <div class="">    
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="tabbable tabbable-custom">
                            <div class="tab-content">

                                <div class="row">
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="page-breadcrumb breadcrumb">
                                                    <h4></h4>
                                                </ul>
                                            </div>
                                        </div>

                                        <form class="form-horizontal" id="form">
                    <div class="form-body">
                        
                            <div class="row">
                                
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nombre*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="name" name="name" value="" required="">
                                            <div id="displaynitp"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="email" id="email" value="" required="">
                                        </div>
                                    </div>                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="telefono" name="telefono" value="" required="">
                                            <div id="displayprovp"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Ciudad*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="ciudad" name="ciudad" value="" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Programa</label>
                                        <div class="col-md-8">                                                                 
                                        <select name="programs" id="programs" class="form-control">
                                            @foreach($programas as $pro)
                                            <option value="{{ $pro->id }}">{{ $pro->name }}</option>
                                            @endforeach
                                        </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Direccion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="address" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 1*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone1" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 2</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone2" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 3</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone3" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Telefono 4</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="phone4" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">FAX</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="fax" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Apartado Aereo</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="air_section" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Email</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="email" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Genero</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="genders_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Masculino</option>
                                                <option value="3">Femenino</option>
                                                <option value="4">Empresa</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nacimiento</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="birth" required="" placeholder="aaaa-mm-dd">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Digito de verificacion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="digit_of_verification" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">ID. Tributaria*</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="tax_ids_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">NIT</option>
                                                <option value="3">Cedula</option>
                                                <option value="4">RUC</option>
                                                <option value="5">Codigo identificacion</option>
                                                <option value="6">Pasaporte</option>
                                                <option value="7">Cedula extranjera</option>
                                                <option value="8">Tarjeta de identidad</option>
                                                <option value="9">Registro civil</option>
                                                <option value="10">Tarjeta extranjera</option>
                                                <option value="11">Numero identificacion personal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo del pais</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="country_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo de la ciudad</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="city_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Clas. tributaria*</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="tax_classifications_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Gran contribuyente</option>
                                                <option value="3">Empresa del estado</option>
                                                <option value="4">Regimen comun</option>
                                                <option value="5">Regimen simplificado</option>
                                                <option value="6">Regimen simplificado no residente pais</option>
                                                <option value="7">No residente pais</option>
                                                <option value="8">no responsable IVA</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Actv. economica</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="economic_activity" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo persona juridica</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="legal_person_types_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Natural</option>
                                                <option value="3">Juridica</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Observaciones</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="observations" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa agente retenedor</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="withholding_agent" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Es beneficiario RETEIVA</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="beneficiary_reteiva" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>                                            
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Es agente RETEICA</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="agent_reteica" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Es declarante</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="declarant" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="col-md-6">                                                                                                            
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa retencion</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="uses_retention" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Estado</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="status" required="">
                                                <option value="0">Desactivo</option>
                                                <option value="1" selected>Activo</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Usa Razon Social</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="uses_social_reason" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo EAN</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="ean_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Primer nombre</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="first_name" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Segundo nombre</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="middle_name" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Primer apellido</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="last_name" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Segundo apellido</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="last_name2" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Forma de pago</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="method_of_payment" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Calificacion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="rating" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">% de descuento</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="discount_percentage" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Periodo de pago</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="payment_period" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Dias optimista</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="optimistic_days" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Dias pesimista</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="pessimistic_days" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">ID. extranjera</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="foreign_identification" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo identificacion fiscal</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="tax_identification_code" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo empresa</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="company_type" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Autoriza reportar entidades crediticias</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="authorizes_lenders_report" required="">
                                                <option value="0">Falso</option>
                                                <option value="1">Verdadero</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tarifa diferencial RETEIVA compras</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="differential_rate_reteiva_shopping" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tarifa diferencial RETEIVA ventas</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="differential_rate_reteiva_sales" required="">
                                        </div>
                                    </div> 
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Metodo de pago*</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="payments_methods_id" required="">
                                                <option value="1">Ninguno</option>
                                                <option value="2">Cheque</option>
                                                <option value="3">Transferencia</option>
                                                <option value="4">Efectivo</option>
                                                <option value="5">T. credito</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Banco</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="bank" required="" value="0">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Tipo de cuenta</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="account_type" required="">
                                                <option value="0">No aplica</option>
                                                <option value="Ahorros">Ahorros</option>
                                                <option value="Corriente">Corriente</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">No. de cuenta</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="account_number" required="" value="0">
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                                                       
                        <div class="form-actions fluid">
                            <div class="col-md-1 col-md-11">
                                <a class="btn btn-success" onclick="guardarprov()">Guardar</a>
                            </div>
                        </div>                        
                    </div>
                </form>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>







        <!-- BEGIN FOOTER -->

        <div class="footer">
            <div class="footer-inner">
                2015 &copy; SGI Indoamericana.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="icon-angle-up"></i>
                </span>
            </div>
        </div>
        <div id="sound">
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <![endif]-->

        <script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>

        <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
        <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
        <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
        // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
        <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
        <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->   
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
        <!-- END PAGE LEVEL SCRIPTS -->     
        <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
        <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
        <script src="{{ URL::to("js/app.js")}}"></script>
        <script src="{{ URL::to("js/upload.js")}}"></script>
        <script src="{{ URL::to("js/validar.js")}}"></script>
        <script src="{{ URL::to("js/compras.js")}}"></script>
        <script src="{{ URL::to("js/continuada.js")}}"></script>
        <script src="{{ URL::to("js/aspirantes.js")}}"></script>
        <script src="{{ URL::to("js/angular.min.js")}}"></script>
        <script src="{{ URL::to("js/documentacion.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
        <script src="{{ URL::to("js/jQuery_Mask/src/jquery.mask.js")}}"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

        <script src="../js/tragator/fm.tagator.jquery.js"></script>

        <script>
jQuery(document).ready(function () {
    App.init(); // initlayout and core plugins
    Portfolio.init();
    TableEditable.init();
    Index.init();
    Index.initJQVMAP(); // init index page's custom scripts
    Index.initCalendar(); // init index page's custom scripts
    Index.initCharts(); // init index page's custom scripts
    Index.initChat();
    Index.initMiniCharts();
    Index.initPeityElements();
    Index.initKnowElements();
    Index.initDashboardDaterange();
    Tasks.initDashboardWidget();
    UITree.init();
    UIToastr.init();
    FormComponents.init();
});
        </script>   