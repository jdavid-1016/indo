        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script
            <script src="../Highcharts-4.1.4/js/highcharts.js"></script>
   <script src="../Highcharts-4.1.4/js/modules/exporting.js"></script>

<div class="col-md-4 col-sm-12">
                    <div id="container2" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                </div>        
<script type="text/javascript">
    $(function () {
        Highcharts.setOptions({
            colors: ['#058DC7']
        });

        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Seguimientos por usuario'
            },
            subtitle: {
                text: 'Código: <a href="http://192.168.0.18/indo/public/aspirantes/prospectos">Indoamericana</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            series: [{
                name: 'Population',
                data: [
                @foreach($seguimientos_grafica as $seg)
                    ['{{$seg->name}} {{$seg->last_name}}', '80'],
                @endforeach
                ],
                
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    });
</script>