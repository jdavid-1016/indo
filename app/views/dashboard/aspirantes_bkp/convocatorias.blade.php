<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Convocatorias</div>

            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="prospectos" data-toggle="tab">Conovocatorias</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_prospects">
                        <div>
                        </div>
                        @include('dashboard.aspirantes.tabla_convocatorias')
                     </div>
                     <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>

<div class="modal fade" id="cargamasiva" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Carga Masiva De Resultados</h4>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-6">
                 
            </div>
                        <div class="form-group has-success">
                       <label class="control-label">Excel Resultados</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                      <input type="file" class="default form-control" name="file_resultados" id="file_resultados" size="20">
                       </div> 
                    </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" onclick="SubirExcelRes()">Subir</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
    });
    
    Tipped.create('.nameimg', {
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
  });
</script>

<script src="/indo/public/js/upload.js"></script>

<script>
function SubirExcelRes(){
    
    $("#file_resultados").upload('cargamasivares', 
        {
        },
        function(respuesta){
            
            if (respuesta === 2) {
                toastr.error('No ha sido posible leer el archivo', 'Error');
            } else {
                $(".close").click();
                toastr.success('Se ingresaron correctamente los registros', 'Carga Masiva');
                
            }
        });

}
</script>