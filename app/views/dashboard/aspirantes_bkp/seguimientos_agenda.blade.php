<div class="modal-dialog modal-wide" >
    <div class="modal-content">
    <form class="form" action="#" ng-submit="adminSupport()">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Eventos en la Institucion</h4>
        </div>
        <div class="modal-body form-body">
            <!--  -->
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group" id="frm_comentario">
                     
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_seguimientos">
                              Seguimientos
                              </a>
                           </h4>
                        </div>
                        <div id="collapse_seguimientos" class="panel-collapse " style="height: auto;">
                            <table class="table top-blue">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Usuario</th>
                                        <th>Comentario</th>
                                        <th>Fecha</th>
                                        <th>Doc</th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    @foreach($detalle_agendas as $detalle_agenda)
                                    <tr>
                                        <td>
                                            <a href="#" class="pull-left">
                                                <img alt="" style="border-radius:50px;" width="50%"  src="../{{$detalle_agenda->Users->img_min}}">
                                            </a>
                                        </td>
                                        <td>{{$detalle_agenda->Users->name}} {{$detalle_agenda->Users->last_name}}</td>
                                        <td>{{$detalle_agenda->observation}}</td>
                                        <td>{{$detalle_agenda->created_at}}</td>
                                        <td><a class="pull-right" href="../{{$detalle_agenda->file}}" target="_blank" class="btn btn-info"><i class="icon-file-alt"></i></a></td>
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                               
                        </div>
                     </div>

                  </div>
               </div>
            </div>
            <div class="row">
                
                <div class="col-md-12">
                    <div class="form-group has-success"> 
                       <label class="control-label">Nuevo Seguimiento</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <textarea  class="form-control" placeholder="Seguimiento" data-required="true" name="name_n" id="observaciones_seg" ></textarea>
                       </div> 
                    </div>
                    <div class="col-md-6">
               <div class="form-group has-success">
                  <label class="control-label">Campaña</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select name="campaign_carga" id="campaign_carga" class="form-control">
                         @foreach($campanas as $cam)
                         <option  value="{{ $cam->id }}">{{ $cam->campaign }}</option>
                         @endforeach
                     </select>
                  </div> 
               </div>   
            </div> 
                    <div class="form-group has-success"> 
                       <label class="control-label">Excel Prospectos</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                      <input type="file" class="default form-control" name="file_estudiantes" id="file_estudiantes" size="20">
                       </div> 
                    </div>
                </div>                
            </div> 

            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-info" onclick="Descargarfomatoasp()">Formato</button>
            <a class="btn btn-success" onclick="ingresar_seguimiento({{$id_agenda}})"><i class="icon-edit"></i> Ingresar Seguimiento</a>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_metodo">Cerrar</button>
        </div>
  
    </form>   
    </div>
</div>

<script src="/indo/public/js/upload.js"></script>   

<script>
    function Descargarfomatoasp(){
       window.open('http://localhost/indo/public/assets/files/aspirantes.xls','','width=600,height=400,left=50,top=50,toolbar=yes');
       
   }
   
function ingresar_seguimiento(id){

    var f = new Date();
    

    var observaciones = $('#observaciones_seg').val();
    var campaign_carga = $('#campaign_carga').val();
    
    if (observaciones == "") {
        toastr.error('El campo Seguimiento esta vacio', 'Error');
        return;
    }
    
    
    $("#file_estudiantes").upload('ingresarseguimientos', 
        {
            observaciones: observaciones,
            campaign_carga: campaign_carga,
            id: id
        },
        function(respuesta){
            
            if (respuesta === 2) {                                
                toastr.error('No ha sido posible Ingresar El Seguimiento', 'Error');
            } else {
                toastr.success('Se ingreso correctamente el nuevo seguimiento', 'Nuevo Seguimiento');
                var texto = '<div class="note note-success">\n\
            <a href="#" class="pull-left">\n\
            <img alt="" style="border-radius:50px;" width="50%"  src="../{{Auth::user()->img_min}}"></a>\n\
            <span class="text-success">Commentario {{Auth::user()->name}} {{Auth::user()->last_name}}:</span>\n\
            <span class="pull-right">'+f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate() +" "+ f.getHours() +":"+f.getMinutes()+":"+ f.getSeconds()+'</span>\n\
            <p>'+observaciones+'<a href="../'+respuesta+'" target="_blank" class="btn btn-info"><i class="icon-file-alt"></i></a></p></div>';
                $('#observaciones_seg').val("");
                $("#collapse_seguimientos").append(texto);
                
            }
        });
    
//    var html = $.ajax({
//        type: "GET",
//        url: "ingresarseguimientos",
//        data: {observaciones:observaciones, id:id},
//        async: false
//    }).responseText;
//
//    if (parseInt(html) == 1) {
//
//        toastr.success('Se ingreso correctamente el nuevo seguimiento', 'Nuevo Seguimiento');
//        var texto = '<div class="alert alert-success">'+f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate() +" "+ f.getHours() +":"+f.getMinutes()+":"+ f.getSeconds()+  ' <br><span class="text-primary">Comentario:</span> '+observaciones+'. </div>';
//        $('#observaciones_seg').val("");
//        $("#collapse_seguimientos").append(texto);
//        
//        
//    } else {
//        toastr.error('No ha sido posible Ingresar El Seguimiento', 'Error');
//    }



}
</script>