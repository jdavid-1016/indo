
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Directorio de Colegios</div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="directorio">Directorio</a></li>
                  <li class=""><a href="feriascolegios">Ferias de Colegios</a></li>
                  <!-- <li class=""><a href="directorioferias">Proximas Ferias</a></li> -->
                  <li class=""><a href="calendario">Calendario</a></li>
                  
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  <a href="#ajax2" data-toggle="modal" class="btn btn-info  pull-right">Ingresar Contacto</a>                          
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_directorio">
                        
                        @include('dashboard.aspirantes.tabla_directorio')
                        
                     </div>
                    <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog " >
                            <div class="modal-content">
                            <form class="form" action="#" ng-submit="adminSupport()">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Ingreso nuevo contacto</h4>
                                </div>
                                <div class="modal-body form-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Nombre Institución</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="nombre" value="">
                                               </div> 
                                            </div> 
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Nombre Contacto</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="contacto" value="">
                                               </div> 
                                            </div> 
                                        </div>   
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Dirección</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="direccion" value="">
                                               </div> 
                                            </div> 
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Teléfono</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="telefono" value="">
                                               </div> 
                                            </div> 
                                        </div>   
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Email</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="email" value="">
                                               </div> 
                                            </div> 
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Fecha Feria</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control fecha_required" placeholder="" data-required="true" name="name_n" id="feria" value="">
                                               </div> 
                                            </div> 
                                        </div>   
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Rector</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="rector" value="">
                                               </div> 
                                            </div> 
                                        </div> 
                                        <div class="col-md-6">
                                            <div class="form-group"> 
                                               <label class="control-label">Observaciones</label>
                                               <div class="input-group">
                                                  <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                  <textarea  class="form-control" placeholder="" data-required="true" name="name_n" id="observaciones" value=""></textarea>
                                               </div> 
                                            </div> 
                                        </div>   
                                    </div>

                                    
                                </div>
                                <div class="modal-footer">
                                    <a class="btn btn-success" onclick="crear_agenda()"><i class="icon-ok"></i> Crear</a>
                                    <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_metodo">Cerrar</button>
                                </div>
                          
                            </form>   
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="ajax3" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                    </div>
                    <div class="modal fade" id="ajax1" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                    </div>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>
<script type="text/javascript">
   function recargarTabla(){
       
       var html = $.ajax({
                     type: "GET",
                     url: "directorio",
                     cache: false,
                     data: {nombre: "hola"},
                     async: false
                    }).responseText;
                                        
        $('#table_admin_directorio').html(html);
       
   }

</script>