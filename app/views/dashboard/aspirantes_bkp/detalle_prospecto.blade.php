<div class="modal-dialog modal-wide">
    <div class="modal-content">
    <form class="form" action="#" >
    @foreach($aspirantes as $aspirante)
        <input type="hidden" value="" id="prospect_id">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Prospecto</h4>
        </div>      
        <div class="modal-body form-body">
          
            <div class="row">
                <div class="col-md-6">
                   <div class="form-group"> 
                      <label class="control-label">Nombre</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         <input type="text" class="form-control" placeholder="" data-required="true" name="name" id="name" value="{{$aspirante->name}}">
                      </div> 
                   </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <label class="control-label">Email</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         <input type="text" class="form-control" placeholder="" data-required="true" name="email" id="email" value="{{$aspirante->email}}">
                      </div> 
                   </div>
                </div>      
            </div>
            <div class="row">
                <div class="col-md-6">
                   <div class="form-group">
                      <label class="control-label">Telefono</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         <input type="text" class="form-control" placeholder="" data-required="true" name="phone" id="phone" value="{{$aspirante->phone}}">
                            
                         </select>
                      </div>
                   </div>
                </div>
                 
                <div class="col-md-6">
                   <div class="form-group">
                      <label class="control-label">Telefono 2</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         <input type="text" class="form-control" placeholder="" data-required="true" name="phone2" id="phone2" value="{{$aspirante->phone2}}">
                            
                         </select>
                      </div>
                   </div>
                </div>                        
                
            </div>
          
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Ciudad</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <input type="text" class="form-control" placeholder="" data-required="true" name="city" id="city" value="{{$aspirante->city}}">
                        </div>
                    </div>
                </div>
                  
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label">Documento</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <input type="text" class="form-control" placeholder="" data-required="true" name="document" id="document" value="{{$aspirante->document}}">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="frm_comentario">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_5">
                                        Programar Seguimiento
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_5" class="panel-collapse collapse" style="height: auto;">
                                
                                    <div class="panel-body">                            
                                        <div class="row">
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Fecha</label>
                                                  <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control fecha_required_next" placeholder="" data-required="true" name="next_fecha" id="next_fecha" value="">
                                                  </div>
                                              </div>
                                          </div>
                  
                                          <div class="col-md-6">
                                              <div class="form-group">
                                                  <label class="control-label">Observacion</label>
                                                  <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <textarea rows="2" class="form-control" name="" id="next_observation"></textarea>
                                                  </div>
                                              </div>
                                          </div>
                                        </div>
                                        <button type="button" class="btn btn-info editar_observacion" id="" onclick="guardarNextDate({{$aspirante->id}})"><i id="icon" class="icon-edit"></i>Guardar</button>
                                      
                                    </div>
                                
                            </div>
                        </div> 
                    </div>
                </div>      
            </div>
            <div class="row">
               <div class="col-md-6">
                  <div class="form-group has-success">
                     <label class="control-label">Estado</label>
                     <div class="input-group">
                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                        <select name="status" id="status" class="form-control">
                            @foreach($estados as $est)
                            <option <?php if($aspirante->aspirants_status_prospects_id==$est->id){ echo "selected"; } ?> value="{{ $est->id }}">{{ $est->status }}</option>
                            @endforeach
                        </select>
                        
                     </div> 
                  </div>
               </div>
               
               <div class="col-md-6">
                  <div class="form-group has-success"> 
                     <label class="control-label">Programa</label>
                     <div class="input-group">
                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                        <select name="programs" id="programs" class="form-control">
                            @foreach($programas as $pro)
                            <option <?php if($aspirante->aspirants_programs_id==$pro->id){ echo "selected"; } ?> value="{{ $pro->id }}">{{ $pro->name }}</option>
                            @endforeach
                        </select>
                     </div> 
                  </div>   
               </div> 
             
            </div>
            
            <div class="row" id="observacion_editar" style="">
                         
                <div class="col-md-12">
                    <div class="form-group has-success">
                        <label class="control-label">Comentario</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <textarea rows="2" class="form-control" name="" id="observation_edit"></textarea>
                        </div>
                    </div>
                </div>      
             
              
                <div class="col-md-12">
                    <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(1, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo1</button>
                    <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(2, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo2</button>
                    <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(3, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo3</button>
                    <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(4, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo4</button>
                    <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(5, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo5</button>
                </div>
            </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-success rechazar_solicitud" id="" onclick="enviarNuevoAspirante({{$aspirante->id}})"><i id="icon" class="icon-ok"></i>Enviar Formulario</button>
            <button type="button" class="btn btn-info editar_observacion" id="" onclick="guardarCambiosAspirantes({{$aspirante->id}})"><i id="icon" class="icon-edit"></i>Guardar</button>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
        </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group" id="frm_comentario">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
                                        Seguimientos
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_4" class="panel-collapse " style="height: auto;">
                                
                                    <div class="panel-body">                            
                                        <table class="table top-blue" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th>Usuario</th>
                                                    <th>Estado</th>
                                                    <th>Comentario</th>
                                                    <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($traces as $trace)
                                              
                                                <tr style="text-align:center;">
                                                    <td><img alt="" style="border-radius:50px;" width="50" height="50"  src="../{{$trace->Users->img_min}}"></td>
                                                    <td><strong>{{$trace->Users->name}} {{$trace->Users->last_name}}</strong></td>
                                                    <td>({{$trace->AspirantsStatusProspects->status}})</td>
                                                    <td>{{$trace->coment}}</td>
                                                    <td>{{$trace->created_at}}</td>
                                                </tr>
                                       
                                                @endforeach
                                            </tbody>
                                        </table>
                                      
                                    </div>
                                
                            </div>
                        </div> 
                    </div>
                </div>      
            </div>
            
            
        
            

        </div>
    
    @endforeach
   </form>
   </div>
</div>
<script>
    $( ".fecha_required_next" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
 </script>   