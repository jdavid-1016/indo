<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Gestion de Emails
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Aspirantes</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Gestion Emails</a> 
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="page-breadcrumb breadcrumb">
                <h4><b>Enviar un nuevo Email</b></h4>
            </ul>
        </div>
    </div>
    <div class="row">
        {{Form::open( array('url'=>'aspirantes/enviaremail', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-1 control-label">Para:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="para" name="para" required/>
                    </div>
                </div>
            </div>
            </div><br>
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-1 control-label">Asunto:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="asunto" name="asunto" required/>
                    </div>
                </div>
                </div>
            </div><br>
        <div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Mensaje:</label>
                    <div class="col-md-6">
                        <textarea class="ckeditor form-control" id="mensaje" name="mensaje" rows="6"></textarea>
                    </div>
                </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-md-1 col-md-6">
                    <button type="submit" class="btn btn-info btn-block">Enviar</button>
                </div>
            </div>            

        {{Form::close()}}
    </div>
</div>