<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Indoamericana</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="MobileOptimized" content="320">
        <link href="{{URL::to("logo.ico")}}" rel="shortcut icon" >
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
        <link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/select2/select2_conquer.css")}}" />
        <link rel="stylesheet" href="{{  URL::to("assets/plugins/data-tables/DT_bootstrap.css")}}" />

        <link rel="stylesheet" href="../js/tragator/fm.tagator.jquery.css"/>


        <!-- END THEME STYLES -->   
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->   
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->  
                <a class="navbar-brand" href="">
                    <img src="{{URL::to("assets/img/logo.png")}}" alt="logo" style="margin-top: -15px"class="img-responsive" />
                </a>
                <!--<form class="search-form search-form-header" role="form" action="index.html" >
                   <div class="input-icon right">
                      <i class="icon-search"></i>
                      <input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
                   </div>
                </form>-->
                <h2 style="color: #ffffff">Formulario De Registro Aspirantes</h2>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <img src="{{URL::to("assets/img/menu-toggler.png")}}" alt="" />
                </a> 
                <!-- END RESPONSIVE MENU TOGGLER -->

                <!-- BEGIN TOP NAVIGATION MENU -->        

                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>

        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <!----------------------------------------------------------------------------------HEAD---------------------------------------------------------------------------------------->
        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <hr><hr>
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="tabbable tabbable-custom">
                            <div class="tab-content">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="page-breadcrumb breadcrumb">
                                                    <h4></h4>
                                                </ul>
                                            </div>
                                        </div>

                                        <form class="form-horizontal" id="form">
                                            <div class="form-body">
                                                <div class="row">                            
                                                    <div class="col-md-12 note note-warning">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                            </div>
                                                            <div class="col-md-2">                
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label class="control-label">Imagen</label>
                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                                            </div>
                                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                            <div>
                                                                                <span class="btn btn-default btn-file">
                                                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                                    <input type="file" class="default form-control" name="imagen_cpu" id="imagen_cpu" size="20">
                                                                                </span>
                                                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                                                                                <div class="help-block">
                                                                                    Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Programa Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="programs" id="programs" class="form-control">
                                                                            @foreach($programas as $pro)
                                                                            <option <?php if ($aspirante[0]->aspirants_programs_id == $pro->id) {
                                                                                                echo "selected";
                                                                                            } ?> value="{{ $pro->id }}">{{ $pro->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Periodo Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="period" id="period" class="form-control">
                                                                            <option value="Segundo Semestre 2015">Segundo Semestre 2015</option>
                                                                            <option value="Primer Semestre 2016">Primer Semestre 2016</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Tipo de Aspirante</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="type_aspirant" id="type_aspirant" class="form-control">
                                                                            <option value="Nuevo">Nuevo</option>
                                                                            <option value="Homologante Externo">Homologante Externo</option>
                                                                            <option value="Reintegro">Reintegro</option>
                                                                        </select>
                                                                    </div>
                                                                </div>                            

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Jornada</label>
                                                                    <div class="col-md-4">
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline">
                                                                                <div class="radio" id="uniform-optionsRadios25"><input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked></div> Mañana
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                                <div class="radio" id="uniform-optionsRadios26"><input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" ></div> Tarde
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                                <div class="radio" id="uniform-optionsRadios27"><input type="radio" name="optionsRadios" id="optionsRadios27" value="option3" disabled></div> Noche
                                                                            </label>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Personales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name" name="name" type="text" class="form-control campos_cpu" value="{{$name[0]}}" placeholder="Primer Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="middle_name" name="middle_name" type="text" class="form-control campos_cpu" value="<?php if (isset($name[1])) { ?>{{$name[1]}} <?php } ?>" placeholder="Segundo Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name" name="last_name" type="text" class="form-control campos_cpu" value="<?php if (isset($name[2])) { ?>{{$name[2]}} <?php } ?>" placeholder="Primer Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name2" name="last_name2" type="text" class="form-control campos_cpu" value="<?php if (isset($name[3])) { ?>{{$name[3]}} <?php } ?>" placeholder="Segundo Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="type_document" id="type_document" class="form-control">
                                                                            <option value="0">Tipo de documento</option>
                                                                            @foreach($typedoc as $type)
                                                                            <option value="{{ $type->id }}">{{ $type->type }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="document" name="document" type="text" class="form-control campos_cpu" value="{{$aspirante[0]->document}}" placeholder="N° Doc Identidad">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="nivel_formacion" id="nivel_formacion" class="form-control">
                                                                            <option value="0">Nivel de formacion</option>
                                                                            @foreach($nivelf as $nivel)
                                                                            <option value="{{ $nivel->id }}">{{ $nivel->training }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="libreta_militar" id="libreta_militar" class="form-control">
                                                                            <option value="0">Libreta Militar</option>
                                                                            <option value="1">Si</option>
                                                                            <option value="2">No</option>
                                                                        </select>
                                                                        
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Fecha Nacimiento">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="estado_civil" id="estado_civil" class="form-control">
                                                                            <option value="0">Estado Civil</option>
                                                                            @foreach($estadocivil as $estcivil)
                                                                            <option value="{{ $estcivil->id }}">{{ $estcivil->marital_status }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="genero" id="genero" class="form-control">
                                                                            <option value="0">Genero</option>
                                                                            @foreach($genero as $gen)
                                                                            <option value="{{ $gen->id }}">{{ $gen->aspirants_gender }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="grupo_sanguineo" id="grupo_sanguineo" class="form-control">
                                                                            <option value="0">Grupo Sanguineo</option>
                                                                            <option value="O+">O+</option>
                                                                            <option value="O-">O-</option>
                                                                            <option value="A+">A+</option>
                                                                            <option value="A-">A-</option>
                                                                            <option value="AB+">AB+</option>
                                                                            <option value="AB-">AB-</option>
                                                                            <option value="B+">B-</option>
                                                                            <option value="B-">B+</option>
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nacionalidad">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección de Residencia">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="localidad" id="localidad" class="form-control">
                                                                            <option value="0">Localidad</option>
                                                                            @foreach($localidades as $loc)
                                                                            <option value="{{ $loc->id }}">{{ $loc->location }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ciudad de Residencia" value="{{$aspirante[0]->city}}">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Celular" value="{{$aspirante[0]->phone2}}">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Email" value="{{$aspirante[0]->email}}">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="lugar_nacimiento" id="lugar_nacimiento" class="form-control">
                                                                            <option value="0">Lugar de Nacimiento</option>
                                                                            @foreach($lugarnac as $lugar)
                                                                            <option value="{{ $lugar->id }}">{{ $lugar->birth_place }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="cpu_serial" class="form-control campos_cpu" rows="3" placeholder="Observaciones"></textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>                                    



                                                    </div>
                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Laborales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Empresa Donde Trabaja">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Cargo Actual">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4 class="block">Datos Familiares</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nombre Padre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nombre Madre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nombre Acudiente">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Celular">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Email">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3> &nbsp &nbsp</h3>





                                                    </div>
                                                </div>
                                                <div class="row">


                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Estudios Realizados</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="*Bachillerato: Nombre de la Institucion:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Carrera Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Otros:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="cpu_serial" name="cpu_serial" class="form-control campos_cpu" placeholder="Que lo motivo a estudiar en la CORPORACIÓN EDUCATIVA INDOAMERICANA:"></textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Fuentes De Infomación</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="*Fuentes de Información:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="*Especifique:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-actions fluid">
                                                    <div class="col-md-1 col-md-11">
                                                        <a class="btn btn-success" onclick="guardarprov()">Guardar</a>
                                                    </div>
                                                </div>                        
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>







        <!-- BEGIN FOOTER -->

        <div class="footer">
            <div class="footer-inner">
                2015 &copy; SGI Indoamericana.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="icon-angle-up"></i>
                </span>
            </div>
        </div>
        <div id="sound">
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <![endif]-->

        <script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>

        <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
        <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
        <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
        // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
        <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
        <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->   
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
        <!-- END PAGE LEVEL SCRIPTS -->     
        <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
        <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
        <script src="{{ URL::to("js/app.js")}}"></script>
        <script src="{{ URL::to("js/upload.js")}}"></script>
        <script src="{{ URL::to("js/validar.js")}}"></script>
        <script src="{{ URL::to("js/compras.js")}}"></script>
        <script src="{{ URL::to("js/continuada.js")}}"></script>
        <script src="{{ URL::to("js/aspirantes.js")}}"></script>
        <script src="{{ URL::to("js/angular.min.js")}}"></script>
        <script src="{{ URL::to("js/documentacion.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
        <script src="{{ URL::to("js/jQuery_Mask/src/jquery.mask.js")}}"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

        <script src="../js/tragator/fm.tagator.jquery.js"></script>

        <script>
                                                            jQuery(document).ready(function () {
                                                                App.init(); // initlayout and core plugins
                                                                Portfolio.init();
                                                                TableEditable.init();
                                                                Index.init();
                                                                Index.initJQVMAP(); // init index page's custom scripts
                                                                Index.initCalendar(); // init index page's custom scripts
                                                                Index.initCharts(); // init index page's custom scripts
                                                                Index.initChat();
                                                                Index.initMiniCharts();
                                                                Index.initPeityElements();
                                                                Index.initKnowElements();
                                                                Index.initDashboardDaterange();
                                                                Tasks.initDashboardWidget();
                                                                UITree.init();
                                                                UIToastr.init();
                                                                FormComponents.init();
                                                            });
        </script>   