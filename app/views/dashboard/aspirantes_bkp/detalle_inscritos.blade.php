<div class="modal-dialog modal-wide">
    <div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Inscrito</h4>
    </div> 
    <div class="portlet">
        <div class="portlet-title">
           <div class="caption"><i class="icon-reorder"></i>Prospectos</div>
                <div class="actions">
                    <div class="btn-group" data-toggle="buttons">
                        
                    </div>
                </div>
        </div>
        <div class="portlet-body">
           <ul  class="nav nav-tabs">
              <li class="active"><a href="#prospectos" data-toggle="tab">Seguimientos</a></li>
              <li class=""><a href="#inscritos" data-toggle="tab">Información Personal</a></li>
              <li class=""><a href="#radicados" data-toggle="tab">Radicación de Documentos</a></li>
              <li class=""><a href="#examen" data-toggle="tab">Resultados de Admisión</a></li>
              <!--<li class=""><a href="#examen" data-toggle="tab">Homologación Externa</a></li>-->
              <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
              
           </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade active in" id="prospectos">
                <div class="table-responsive" id="table_admin_supports">
                     <form class="form" action="#" >
                         <input type="hidden" value="{{$id_documentos[0]->id}}" id="id_informacion_form">
                     @foreach($aspirantes as $aspirante)
                              
                         <div class="modal-body form-body">
                           
                             <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group"> 
                                       <label class="control-label col-md-3  col-md-3">Nombre</label>
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input type="text" class="form-control" readonly="" placeholder="" data-required="true" name="name" id="name" value="{{$aspirante->name}}">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-3">Email</label>
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input type="text" class="form-control" readonly="" placeholder="" data-required="true" name="email" id="email" value="{{$aspirante->email}}">
                                       </div> 
                                    </div>
                                 </div>      
                             </div>
                             <div class="row">
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-3">Telefono</label>
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input type="text" class="form-control" readonly="" placeholder="" data-required="true" name="phone" id="phone" value="{{$aspirante->phone}}">
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>
                                  
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <label class="control-label col-md-3">Telefono 2</label>
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input type="text" class="form-control" readonly="" placeholder="" data-required="true" name="phone2" id="phone2" value="{{$aspirante->phone2}}">
                                             
                                          </select>
                                       </div>
                                    </div>
                                 </div>                        
                                 
                             </div>
                           
                             <div class="row">
                                 <div class="col-md-6">
                                     <div class="form-group">
                                         <label class="control-label col-md-3">Ciudad</label>
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input type="text" class="form-control" readonly="" placeholder="" data-required="true" name="city" id="city" value="{{$aspirante->city}}">
                                         </div>
                                     </div>
                                 </div>
                                   
                                 <div class="col-md-6">
                                     <div class="form-group">
                                         <label class="control-label col-md-3">Documento</label>
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input type="text" class="form-control" readonly="" placeholder="" data-required="true" name="document" id="document" value="{{$aspirante->document}}">
                                         </div>
                                     </div>
                                 </div>
                             </div>
                             
                             <div class="row">
                                 <div class="col-md-12">
                                     <div class="form-group" id="frm_comentario">
                                         <div class="panel panel-default">
                                             <div class="panel-heading">
                                                 <h4 class="panel-title">
                                                     <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_5">
                                                         Programar Seguimiento
                                                     </a>
                                                 </h4>
                                             </div>
                                             <div id="collapse_5" class="panel-collapse collapse" style="height: auto;">
                                                 
                                                     <div class="panel-body">                            
                                                         <div class="row">
                                                             <div class="col-md-6">
                                                                 <div class="form-group">
                                                                     <label class="control-label">Fecha</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input type="text" class="form-control fecha_required_next" placeholder="" data-required="true" name="next_fecha" id="next_fecha" value="">
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                               
                                                             <div class="col-md-6">
                                                                 <div class="form-group">
                                                                     <label class="control-label">Observacion</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <textarea rows="2" class="form-control" name="" id="next_observation"></textarea>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <button type="button" class="btn btn-info editar_observacion" id="" onclick="guardarNextDate({{$aspirante->id}})"><i id="icon" class="icon-edit"></i>Guardar</button>
                                                       
                                                     </div>
                                                 
                                             </div>
                                         </div> 
                                     </div>
                                 </div>      
                             </div>
                             <div class="row">
                                <div class="col-md-6">
                                   <div class="form-group has-success">
                                      <label class="control-label">Estado</label>
                                      <div class="input-group">
                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                         <select name="status" id="status" class="form-control">
                                             @foreach($estados as $est)
                                             <option <?php if($aspirante->aspirants_status_prospects_id==$est->id){ echo "selected"; } ?> value="{{ $est->id }}">{{ $est->status }}</option>
                                             @endforeach
                                         </select>
                                         
                                      </div> 
                                   </div>
                                </div>
                                
                                <div class="col-md-6">
                                   <div class="form-group has-success"> 
                                      <label class="control-label">Programa</label>
                                      <div class="input-group">
                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                         <select name="programs" id="programs" class="form-control">
                                             @foreach($programas as $pro)
                                             <option <?php if($aspirante->aspirants_programs_id==$pro->id){ echo "selected"; } ?> value="{{ $pro->id }}">{{ $pro->name }}</option>
                                             @endforeach
                                         </select>
                                      </div> 
                                   </div>   
                                </div> 
                              
                             </div>
                             
                             <div class="row" id="observacion_editar" style="">
                                          
                                 <div class="col-md-12">
                                     <div class="form-group has-success">
                                         <label class="control-label">Comentario</label>
                                         <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <textarea rows="2" class="form-control" name="" id="observation_edit"></textarea>
                                         </div>
                                     </div>
                                 </div>      
                            
                               
                                 <div class="col-md-12">
                                     <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(1, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo1</button>
                                     <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(2, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo2</button>
                                     <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(3, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo3</button>
                                     <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(4, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo4</button>
                                     <button type="button" class="btn btn-info" id="" onclick="enviaremailnuevo(5, {{$aspirante->id}})"><i id="icon" class="icon-envelope-alt"></i> Correo5</button>
                                 </div>
                             </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-success rechazar_solicitud" id="" onclick="enviarNuevoAspirante({{$aspirante->id}})"><i id="icon" class="icon-ok"></i>Enviar Formulario</button>
                                <button type="button" class="btn btn-info editar_observacion" id="" onclick="guardarCambiosAspirantes({{$aspirante->id}})"><i id="icon" class="icon-edit"></i>Guardar</button>
                                <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                            </div>
                             <div class="row">
                                 <div class="col-md-12">
                                     <div class="form-group" id="frm_comentario">
                                         <div class="panel panel-default">
                                             <div class="panel-heading">
                                                 <h4 class="panel-title">
                                                     <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
                                                         Seguimientos
                                                     </a>
                                                 </h4>
                                             </div>
                                             <div id="collapse_4" class="panel-collapse " style="height: auto;">
                                                 
                                                     <div class="panel-body">                            
                                                         <table class="table top-blue" data-target="soporte/callSupport/">
                                                             <thead>
                                                                 <tr>
                                                                     <th></th>
                                                                     <th>Usuario</th>
                                                                     <th>Estado</th>
                                                                     <th>Comentario</th>
                                                                     <th>Fecha</th>
                                                                 </tr>
                                                             </thead>
                                                             <tbody>
                                                                 @foreach($traces as $trace)
                                                               
                                                                 <tr style="text-align:center;">
                                                                     <td><img alt="" style="border-radius:50px;" width="50" height="50"  src="../{{$trace->Users->img_min}}"></td>
                                                                     <td><strong>{{$trace->Users->name}} {{$trace->Users->last_name}}</strong></td>
                                                                     <td>({{$trace->AspirantsStatusProspects->status}})</td>
                                                                     <td>{{$trace->coment}}</td>
                                                                     <td>{{$trace->created_at}}</td>
                                                                 </tr>
                                                        
                                                                 @endforeach
                                                             </tbody>
                                                         </table>
                                                       
                                                     </div>
                                                 
                                             </div>
                                         </div> 
                                     </div>
                                 </div>      
                             </div>                                                      

                         </div>
                     
                     @endforeach
                    </form>    

                </div>
            </div>
            <div class="tab-pane fade" id="inscritos">
                <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="tabbable tabbable-custom">
                <div class="tab-content">
                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <ul class="page-breadcrumb breadcrumb">
                                                    <h4></h4>
                                                </ul>
                                            </div>
                                        </div>

                                        <form class="form-horizontal" id="form">
                                            <div class="form-body">
                                                <div class="row">                            
                                                    <div class="col-md-12 note note-warning">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                            </div>
                                                            <div class="col-md-2">                
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label class="control-label">Imagen</label>
                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                                <img src="../{{$inscrito[0]->image}}" alt="">
                                                                            </div>
                                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                            <div>
                                                                                <span class="btn btn-default btn-file">
                                                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                                    <input type="file" class="default form-control" name="imagen_asp" id="imagen_asp" size="20">
                                                                                </span>
                                                                                <div class="help-block">
                                                                                    Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Programa Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="programs" id="programs" class="form-control">
                                                                         @foreach($programas as $prog)
                                                                         <option <?php if($inscrito[0]->aspirants_programs_id==$prog->id){ echo "selected"; } ?> value="{{ $prog->id }}">{{ $prog->name }}</option>
                                                                         @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Periodo Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="period" id="period" class="form-control">
                                                                            @foreach($periodos as $per)
                                                                            <option <?php if($inscrito[0]->aspirants_periods_id==$per->id){ echo "selected"; } ?> value="{{ $per->id }}">{{ $per->period }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Tipo de Aspirante</label>
                                                                    <div class="col-md-4">
                                                                        <select name="type_aspirant" id="type_aspirant" class="form-control">
                                                                            <option value="Nuevo">Nuevo</option>
                                                                            <option value="Homologante Externo">Homologante Externo</option>
                                                                            <option value="Reintegro">Reintegro</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Jornada</label>
                                                                    <div class="col-md-6">
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline">
                                                                                <div id="uniform-optionsRadios25"><input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked></div> Mañana
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                                <div  id="uniform-optionsRadios26"><input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" ></div> Tarde
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                                <div  id="uniform-optionsRadios27"><input type="radio" name="optionsRadios" id="optionsRadios27" value="option3" disabled></div> Noche
                                                                            </label>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Personales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="first_name" name="first_name" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->first_name}}" placeholder="Primer Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="middle_name" name="middle_name" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->middle_name}}" placeholder="Segundo Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name" name="last_name" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->last_name}}" placeholder="Primer Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name2" name="last_name2" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->last_name2}}" placeholder="Segundo Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="type_document" id="type_document" class="form-control">
                                                                            <option value="0">Tipo de documento</option>
                                                                            @foreach($tipos_documentos as $tipo)
                                                                            <option value="{{ $tipo->id }}" <?php if($inscrito[0]->aspirants_type_documents_id==$tipo->id){ echo "selected"; } ?>>{{ $tipo->type }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="document" name="document" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->document}}" placeholder="N° Doc Identidad">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="training" id="training" class="form-control">
                                                                            <option value="0">Nivel de formacion</option>
                                                                            @foreach($formacion as $for)
                                                                            <option value="{{ $for->id }}" <?php if($inscrito[0]->aspirants_trainings_id==$for->id){ echo "selected"; } ?>>{{ $for->training }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="military_card" id="military_card" class="form-control">
                                                                            <option value="0">Libreta Militar</option>
                                                                            <option value="1" <?php if($inscrito[0]->military_card==1){ echo "selected"; } ?>>Si (Libreta Militar)</option>
                                                                            <option value="2" <?php if($inscrito[0]->military_card==2){ echo "selected"; } ?>>No (Libreta Militar)</option>
                                                                        </select>
                                                                        
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="birthday" name="birthday" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->birthday}}" placeholder="Fecha Nacimiento">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="marital_status" id="marital_status" class="form-control">
                                                                            <option value="0">Estado Civil</option>
                                                                            @foreach($estado_civil as $estado)
                                                                            <option value="{{ $estado->id }}" <?php if($inscrito[0]->aspirants_marital_statuses_id==$estado->id){ echo "selected"; } ?>>{{ $estado->marital_status }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="gender" id="gender" class="form-control">
                                                                            <option value="0">Genero</option>
                                                                            @foreach($generos as $genero)
                                                                            <option value="{{ $genero->id }}" <?php if($inscrito[0]->aspirants_genders_id==$genero->id){ echo "selected"; } ?>>{{ $genero->aspirants_gender }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="blood_group" id="blood_group" class="form-control">
                                                                            <option value="0">Grupo Sanguineo</option>
                                                                            <option value="O+" <?php if($inscrito[0]->blood_group=="O+"){ echo "selected"; } ?>>O+</option>
                                                                            <option value="O-" <?php if($inscrito[0]->blood_group=="O-"){ echo "selected"; } ?>>O-</option>
                                                                            <option value="A+" <?php if($inscrito[0]->blood_group=="A+"){ echo "selected"; } ?>>A+</option>
                                                                            <option value="A-" <?php if($inscrito[0]->blood_group=="A-"){ echo "selected"; } ?>>A-</option>
                                                                            <option value="AB+" <?php if($inscrito[0]->blood_group=="AB+"){ echo "selected"; } ?>>AB+</option>
                                                                            <option value="AB-" <?php if($inscrito[0]->blood_group=="AB-"){ echo "selected"; } ?>>AB-</option>
                                                                            <option value="B+" <?php if($inscrito[0]->blood_group=="B+"){ echo "selected"; } ?>>B-</option>
                                                                            <option value="B-" <?php if($inscrito[0]->blood_group=="B-"){ echo "selected"; } ?>>B+</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nacionalidad">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address" name="address" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->address}}" placeholder="Dirección de Residencia">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="location" id="location" class="form-control">
                                                                            <option value="0">Locacion</option>
                                                                            @foreach($locations as $location)
                                                                            <option value="{{ $location->id }}" <?php if($inscrito[0]->aspirants_locations_id==$location->id){ echo "selected"; } ?>>{{ $location->location }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="neighborhood" name="neighborhood" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->neighborhood}}" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ciudad de Residencia" value="">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone" name="phone" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->phone}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cell_phone" name="cell_phone" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->cell_phone}}" placeholder="Celular" value="">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="email" name="email" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->email}}" placeholder="Email" value="">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Lugar de Nacimiento">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="observations" class="form-control campos_cpu" rows="3" placeholder="Observaciones">{{$inscrito[0]->observations}}</textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Laborales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="company" name="company" type="text" class="form-control campos_cpu" value="{{$work[0]->company}}" placeholder="Empresa Donde Trabaja">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation" name="occupation" type="text" class="form-control campos_cpu" value="{{$work[0]->occupation}}" placeholder="Cargo Actual">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="work_phone" name="work_phone" type="text" class="form-control campos_cpu" value="{{$work[0]->phone}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="work_address" name="work_address" type="text" class="form-control campos_cpu" value="{{$work[0]->address}}" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4 class="block">Datos Familiares</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name_father" name="name_father" type="text" class="form-control campos_cpu" value="{{$family[0]->name_father}}" placeholder="Nombre Padre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation_father" name="occupation_father" type="text" class="form-control campos_cpu" value="{{$family[0]->occupation_father}}" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address_father" name="address_father" type="text" class="form-control campos_cpu" value="{{$family[0]->address_father}}" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone_father" name="phone_father" type="text" class="form-control campos_cpu" value="{{$family[0]->phone_father}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name_mother" name="name_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->name_mother}}" placeholder="Nombre Madre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation_mother" name="occupation_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->occupation_mother}}" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address_mother" name="address_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->address_mother}}" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone_mother" name="phone_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->phone_mother}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name_keeper" name="name_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->name_keeper}}" placeholder="Nombre Acudiente">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation_keeper" name="occupation_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->occupation_keeper}}" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address_keeper" name="address_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->address_keeper}}" placeholder="Dirección">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone_keeper" name="phone_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->phone_keeper}}" placeholder="Teléfono">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cellphone_keeper" name="cellphone_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->cellphone_keeper}}" placeholder="Celular">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="email_keeper" name="email_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->email_keeper}}" placeholder="Email">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="neighborhood_keeper" name="neighborhood_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->neighborhood}}" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3> &nbsp &nbsp</h3>

                                                    </div>
                                                </div>
                                                <div class="row">


                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Estudios Realizados</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="school" name="school" type="text" class="form-control campos_cpu" value="{{$studies[0]->school}}" placeholder="*Bachillerato: Nombre de la Institucion:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="university" name="university" type="text" class="form-control campos_cpu" value="{{$studies[0]->university}}" placeholder="Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="career" name="career" type="text" class="form-control campos_cpu" value="{{$studies[0]->career}}" placeholder="Carrera Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="others" name="others" type="text" class="form-control campos_cpu" value="{{$studies[0]->others}}" placeholder="Otros:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="motivation" name="motivation" class="form-control campos_cpu"  placeholder="Que lo motivo a estudiar en la CORPORACIÓN EDUCATIVA INDOAMERICANA:">{{$studies[0]->motivation}}</textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Fuentes De Infomación</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="source" id="source" class="form-control">
                                                                            <option value="0">Fuente de informacion</option>
                                                                            @foreach($fuentes as $fuente)
                                                                            <option value="{{ $fuente->id }}" <?php if($inscrito[0]->aspirants_sources_id==$fuente->id){ echo "selected"; } ?>>{{ $fuente->source }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="source_details" name="source_details" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->source_details}}" placeholder="*Especifique:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-actions fluid">
                                                    <div class="col-md-1 col-md-11">
                                                        <a class="btn btn-success" onclick="guardareditasp()">Guardar</a>
                                                    </div>
                                                </div>                        
                                            </div>
                                        </form>

                                    </div>
                                </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
            </div>
            <div class="tab-pane fade" id="radicados">
                <div class="table-responsive" id="table_admin_supports">
                    
                    <div id="openreq" class="portlet-tab-content" style="display: block;">
                         <form name="form1" method="post" action="editar_aspirante_perfil_comercial.php">   
                          <table class="table top-blue">
                            <thead>
                              <tr>
                                <th>Radicado</th>
                                <th>Documentos</th>
                                <th>Fecha de Compromiso</th>
                              </tr>
                            </thead>
                            <tbody class="center">
                                <?php
                                for($i=0;count($documentos)>$i;$i++){
                                ?>
                                <tr>
                                    <td><input name="formulario" type="checkbox" id="documentos_chk_{{$documentos[$i]->id}}" <?php if($documentosasp[$i]->status==2){ echo "checked"; } ?>></td>
                                    <td>{{$documentos[$i]->document}}</td>
                                    <td><input name="fecha_formulario" type="text"  class="form-control fecha_required_next"  id="documentos_{{$documentos[$i]->id}}" value="{{$documentosasp[$i]->date_commitment}}" class="hasDatepicker"></td>
                                </tr>
                                <?php } ?>
                                <tr>
                                    <td colspan="3">
                                        <input name="guardar" type="button" class="btn btn-info" id="guardar" value="Guardar Radicación" onclick="validar_formulario_documentos()">
                                        <input name="radicacion_documentos" type="hidden" id="radicacion_documentos" value="1">
                                        <input name="registroaspirante" type="hidden" id="registroaspirante" value="10158">
                                        @if($citaciones)
                                        <a target="_blank" href="imprimircitacion?id={{$inscrito[0]->id}}"><input name="imprimir" type="button" class="btn btn-info" id="vercit" value="Ver Citacion"></a>
                                        <input onclick="botoneliminarcit({{$inscrito[0]->id}})" name="imprimir" type="button" class="btn btn-danger" id="eliminarcit" value="Eliminar Citacion">
                                        <a target="_blank" onclick="botonimprimircit()" href="imprimircitacion?id={{$inscrito[0]->id}}"><input style="display:none" name="imprimir" type="button" class="btn btn-info" id="imprimircit" value="Imprimir Citacion"></a>
                                        @else
                                        <a target="_blank" href="imprimircitacion?id={{$inscrito[0]->id}}"><input style="display:none" name="imprimir" type="button" class="btn btn-info" id="vercit" value="Ver Citacion"></a>
                                        <input onclick="botoneliminarcit({{$inscrito[0]->id}})" style="display:none" name="imprimir" type="button" class="btn btn-danger" id="eliminarcit" value="Eliminar Citacion">
                                        <a target="_blank" onclick="botonimprimircit()" href="imprimircitacion?id={{$inscrito[0]->id}}"><input name="imprimir" type="button" class="btn btn-info" id="imprimircit" value="Imprimir Citacion"></a>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                          </table>

                        </form>

                          <p>&nbsp;</p>
                        </div>    
                            
                </div>
            </div>
            <div class="tab-pane fade" id="examen">
                
                
                <div class="table-responsive" id="table_admin_supports">
                    
                    <div id="respreq3" class="portlet-tab-content" style="display: block;">
                         <form name="form1" method="post" action="editar_aspirante_perfil_comercial.php">   
                          <table class="table top-blue">
                            <thead>
                              <tr>
                            <th class="align-left"><div align="center">CONCEPTO</div></th>
                            <th class="align-left"><div align="center">FECHA</div></th>
                            <th class="align-left"><div align="center">CONCEPTO PUNTAJE</div></th>
                          </tr>
                            </thead>
                           <tbody class="center">
                                
                                <tr>
                            <td>Examen de conocimiento generales</td>
                            <td><div align="center">
                                <input name="date_knowledge" type="text" class="form-control fecha_required_next" id="date_knowledge" value="{{$resultados[0]->date_knowledge}}">
                            </div></td>
                            <td><div align="center">
                                <input name="score_knowledge" type="text" class="form-control" id="score_knowledge" value="{{$resultados[0]->score_knowledge}}">
                            </div></td>
                          </tr>
                          <tr>
                            <td>Entrevista</td>
                            <td><div align="center">
                              <input name="date_interview" type="text" class="form-control fecha_required_next" id="date_interview" value="{{$resultados[0]->date_interview}}" class="hasDatepicker">
                            </div></td>
                            <td><div align="center">
                              <input name="score_interview" type="text" class="form-control" id="score_interview" value="{{$resultados[0]->score_interview}}">
                            </div></td>
                            </tr>
                          <tr>
                            <td>Resultados</td>
                            <td><div align="center">
                              <select name="aspirants_status_results_id" id="aspirants_status_results_id" class="form-control">
                                @foreach($estados_resultados as $estados)
                                <option value="{{$estados->id}}" <?php if($resultados[0]->aspirants_status_results_id == $estados->id){ echo "selected"; }?>>{{$estados->status}}</option>
                                @endforeach
                              </select>
                            </div></td>
                            <td><div align="center">
                              <input name="score_status" type="text" class="form-control" id="score_status" value="{{$resultados[0]->score_status}}">
                            </div></td>
                            </tr>
                          <tr>
                            <td>Descuento</td>
                            <td><div align="center">
                              <input name="discount" type="text" class="form-control" id="discount" value="{{$resultados[0]->discount}}" size="6">
                            %</div></td>
                            <td><div align="center">
                              <input name="score_discount" type="text" class="form-control" id="score_discount" value="{{$resultados[0]->score_discount}}">
                            </div></td>
                          </tr>

                          <tr>
                            <td>Observaciones</td>
                            <td><div align="center">
                                <textarea name="observations" class="form-control" id="observations" cols="20" rows="8">{{$resultados[0]->observations}}</textarea></div></td>
                            <td></td>
                          </tr>
                          <tr>
                                    <td colspan="3">
                                        <input name="guardar_resultados" type="button" class="btn btn-info" id="guardar_resultados" value="Guardar Resultados" onclick="validar_formulario_resultados()">
                                    </td>
                                </tr>
                            </tbody>
                          </table>

                        </form>
                          <p>&nbsp;</p>
                        </div>
                            
                </div>

            </div>
        </div>

    </div>
    
   </div>
</div>
<script>
    $( ".fecha_required_next" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
 </script>
 <script type="text/javascript">

 function validar_formulario_documentos(){
    
     var estado_gen = 5;
     var validacion_items = 0;
     var validacion_proveedores = 0;
     var validacion_general = 0;
     var estados = new Array();
     var ids = new Array();
     var fechas = new Array();
     var i = 0;
     var id_informacion = $('#id_informacion_form').val();
    @foreach($documentos as $documento)
     
        ids[i] = {{$documento->id}};
         
        if( $("#documentos_chk_{{$documento->id}}").is(':checked')) {  
            estados[i] = 2; 
            
            fechas[i] = "";
        } else {

            if ( $('#documentos_{{$documento->id}}').val() == "0000-00-00") {
                toastr.error('Debe Seleccionar Una fecha de entrega para el campo {{$documento->document}}', 'Error');
                return;
            }
            fechas[i] = $('#documentos_{{$documento->id}}').val();
            estados[i] = 3;
        }

        i++;

    @endforeach

    $.ajax({
    type: "post",
    url: "guardardocumentos",
        data: { id_details: ids, status: estados, fechas: fechas, id_informacion:id_informacion}
    })
    .done(function( data ) {
        if(data=="2"){
          toastr.error('No ha sido posible actualizar la solicitud', 'Error');
        }else{
            toastr.success('Se actualizo la solicitud Informacion', 'Actualizacion');
            $("#close_modal").click();
            
           
        }
    });
   

 }
 
 function botoneliminarcit(id){

    $.ajax({
    type: "get",
    url: "eliminarcitacion",
        data: { id: id}
    })
    .done(function( data ) {
        toastr.success('Se elimino la citacion correctamente', 'Eliminacion Citacion');
        $('#vercit').hide();
        $('#eliminarcit').hide();
        $('#imprimircit').show();
        
    });

 }
 
  function botonimprimircit(){

    $('#vercit').show();
    $('#eliminarcit').show();
    $('#imprimircit').hide();

 }
 
  function validar_formulario_resultados(){
      
    var id_informacion = $('#id_informacion_form').val();
    var date_knowledge = $('#date_knowledge').val();
    var score_knowledge = $('#score_knowledge').val();
    var date_interview = $('#date_interview').val();
    var score_interview = $('#score_interview').val();
    var aspirants_status_results_id = $('#aspirants_status_results_id').val();
    var score_status = $('#score_status').val();
    var discount = $('#discount').val();
    var score_discount = $('#score_discount').val();
    var observations = $('#observations').val();
    
    $.ajax({
    type: "get",
    url: "guardarresultados",
        data: { date_knowledge: date_knowledge, score_knowledge: score_knowledge, date_interview: date_interview, score_interview: score_interview, aspirants_status_results_id: aspirants_status_results_id, score_status:score_status, discount:discount, score_discount:score_discount, observations:observations, id_informacion:id_informacion}
    })
    .done(function( data ) {
        if(data=="2"){
          toastr.error('No ha sido posible actualizar los datos', 'Error');
        }else{
            toastr.success('Se actualizaron los datos correctamente', 'Actualizacion');

        }
    });

 }
 
 function guardareditasp(){
     
     toastr.success('Se actualizaron los datos correctamente', 'Actualizacion');
      
    var first_name = $('#first_name').val();
    var middle_name = $('#middle_name').val();
    var last_name = $('#last_name').val();
    var last_name2 = $('#last_name2').val();
    var aspirants_type_documents_id = $('#aspirants_type_documents_id').val();
    var document = $('#document').val();
    var issued = $('#issued').val();
    var aspirants_programs_id = $('#aspirants_programs_id').val();
    var aspirants_periods_id = $('#aspirants_periods_id').val();
    var aspirants_times_id = $('#aspirants_times_id').val();
    var aspirants_trainings_id = $('#aspirants_trainings_id').val();
    var military_card = $('#military_card').val();
    var birthday = $('#birthday').val();
    var aspirants_marital_statuses_id = $('#aspirants_marital_statuses_id').val();
    var aspirants_genders_id = $('#aspirants_genders_id').val();
    var blood_group = $('#blood_group').val();
    var nationality = $('#nationality').val();
    var address = $('#address').val();
    var aspirants_locations_id = $('#aspirants_locations_id').val();
    var neighborhood = $('#neighborhood').val();
    var aspirants_cities_id = $('#aspirants_cities_id').val();
    var phone = $('#phone').val();
    var cell_phone = $('#cell_phone').val();
    var email = $('#email').val();
    var aspirants_birth_places_id = $('#aspirants_birth_places_id').val();
    var observations = $('#observations').val();
    var height = $('#height').val();
    var image = $('#image').val();
    var aspirants_sources_id = $('#aspirants_sources_id').val();
    var source_details = $('#source_details').val();
    var aspirants_prospects_id = $('#aspirants_prospects_id').val();
    var aspirants_status_managements_id = $('#aspirants_status_managements_id').val();
    var name_father = $('#name_father').val();
    var occupation_father = $('#occupation_father').val();
    var address_father = $('#address_father').val();
    var phone_father = $('#phone_father').val();
    var name_mother = $('#name_mother').val();
    var occupation_mother = $('#occupation_mother').val();
    var address_mother = $('#address_mother').val();
    var phone_mother = $('#phone_mother').val();
    var name_keeper = $('#name_keeper').val();
    var occupation_keeper = $('#occupation_keeper').val();
    var address_keeper = $('#address_keeper').val();
    var phone_keeper = $('#phone_keeper').val();
    var cellphone_keeper = $('#cellphone_keeper').val();
    var email_keeper = $('#email_keeper').val();
    var neighborhood_keeper = $('#neighborhood_keeper').val();
    
    var school = $('#school').val();
    var university = $('#university').val();
    var career = $('#career').val();
    var others = $('#others').val();
    var motivation = $('#motivation').val();
    
    var company = $('#company').val();
    var occupation = $('#occupation').val();
    var phone = $('#phone').val();
    var address = $('#address').val();
    
    $.ajax({
    type: "get",
    url: "updatedatospros",
        data: { first_name: first_name, middle_name: middle_name, last_name: last_name, last_name2: last_name2, aspirants_type_documents_id: aspirants_type_documents_id, 
            document: document, issued: issued, aspirants_programs_id: aspirants_programs_id, aspirants_periods_id: aspirants_periods_id, aspirants_times_id: aspirants_times_id, 
            aspirants_trainings_id: aspirants_trainings_id, military_card: military_card, birthday: birthday, aspirants_marital_statuses_id: aspirants_marital_statuses_id,
    aspirants_genders_id: aspirants_genders_id, blood_group: blood_group, nationality: nationality, address: address, aspirants_locations_id: aspirants_locations_id,
    neighborhood: neighborhood, aspirants_cities_id: aspirants_cities_id, phone: phone, cell_phone: cell_phone, email: email, aspirants_birth_places_id: aspirants_birth_places_id,
    observations: observations, height: height, aspirants_sources_id: aspirants_sources_id, source_details: source_details, aspirants_prospects_id: aspirants_prospects_id,
    aspirants_status_managements_id: aspirants_status_managements_id, name_father: name_father, occupation_father: occupation_father, address_father: address_father,
    phone_father: phone_father, name_mother: name_mother, occupation_mother: occupation_mother, address_mother: address_mother, phone_mother: phone_mother, name_keeper: name_keeper,
    occupation_keeper: occupation_keeper, address_keeper: address_keeper, phone_keeper: phone_keeper, cellphone_keeper: cellphone_keeper, email_keeper: email_keeper,
    neighborhood_keeper: neighborhood_keeper, school: school, university: university, career: career, others: others, motivation: motivation, company: company, occupation: occupation,
    phone: phone, address:address}
    })
    .done(function( data ) {
        if(data=="2"){
          toastr.error('No ha sido posible actualizar los datos', 'Error');
        }else{
            toastr.success('Se actualizaron los datos correctamente', 'Actualizacion');

        }
    });

 }
 </script>