<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>A date range picker for Bootstrap</title>
        <!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="all" href="{{ URL::to("assets/calendario/daterangepicker-bs3.css")}}" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ URL::to("assets/calendario/moment.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/calendario/daterangepicker.js")}}"></script>
    </head>
    <body>	
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Estadísticas
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="javascript:;">Página Principal</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="javascript:;">Aspirantes</a>
                            <i class="icon-angle-right"></i> 
                        </li>
                        <li>
                            <a href="javascript:;">Estadísticas</a> 
                        </li>
                        <li class="pull-right">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                        </li>
                    </ul>                            				

                    <script type="text/javascript">
                        $(document).ready(function () {

                            var cb = function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                                var fecha_inicio = start.format('YYYY-M-D');
                                var fecha_fin = end.format('YYYY-M-D');                                

//                                $.ajax({
//                                    type: "GET",
//                                    url: "changedate",
//                                    data: {fecha_inicio: fecha_inicio, fecha_fin: fecha_fin},
//                                })
//                                        .done(function (data) {
//                                            alert(data);
//                                        });

                                document.getElementById("grafica").src = "changedateseguimientos?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica2").src = "changedateprogramas?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica3").src = "changedatecampanas?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica4").src = "changedatefuentes?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica5").src = "changedateestados?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica6").src = "changedatejornadas?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica7").src = "changedategeneros?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica8").src = "changedateciudad?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica9").src = "changedatelocalidad?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";

                                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                            }

                            var optionSet1 = {
                                startDate: moment().subtract(29, 'days'),
                                endDate: moment(),
                                minDate: '01/01/2012',
                                maxDate: '12/31/2015',
                                dateLimit: {days: 60},
                                showDropdowns: true,
                                showWeekNumbers: true,
                                timePicker: false,
                                timePickerIncrement: 1,
                                timePicker12Hour: true,
                                ranges: {
                                    'Hoy': [moment(), moment()],
                                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                                    'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                                    'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                },
                                opens: 'left',
                                buttonClasses: ['btn btn-default'],
                                applyClass: 'btn-small btn-primary',
                                cancelClass: 'btn-small',
                                format: 'MM/DD/YYYY',
                                separator: ' to ',
                                locale: {
                                    applyLabel: 'Aplicar',
                                    cancelLabel: 'Clear',
                                    fromLabel: 'From',
                                    toLabel: 'To',
                                    customRangeLabel: 'Personalizada',
                                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                    firstDay: 1
                                }
                            };

                            var optionSet2 = {
                                startDate: moment().subtract(7, 'days'),
                                endDate: moment(),
                                opens: 'left',
                                ranges: {
                                    'Today': [moment(), moment()],
                                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                }
                            };

                            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

                            $('#reportrange').daterangepicker(optionSet1, cb);

                            $('#reportrange').on('show.daterangepicker', function () {
                                console.log("show event fired");
                            });
                            $('#reportrange').on('hide.daterangepicker', function () {
                                console.log("hide event fired");
                            });
                            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                                console.log("apply event fired, start/end dates are "
                                        + picker.startDate.format('MMMM D, YYYY')
                                        + " to "
                                        + picker.endDate.format('MMMM D, YYYY')
                                        );
                            });
                            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                                console.log("cancel event fired");
                            });

                            $('#options1').click(function () {
                                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
                            });

                            $('#options2').click(function () {
                                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
                            });

                            $('#destroy').click(function () {
                                $('#reportrange').data('daterangepicker').remove();
                            });

                        });
                    </script>

                </div>
            </div>
            <div class="row" id="graficas_aspirantes">
                
                <div class="col-md-4 col-sm-12">
                    <div id="container2" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Seguimientos por usuario</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(1)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica" width="100%" height="500px" src="graphicseguimientos">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Programas de mas interes</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(2)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica2" width="100%" height="500px" src="graphicprogramas">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Campañas con mas prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(3)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica3" width="100%" height="500px" src="graphiccampanas">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Fuentes de informacion con mas prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(4)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica4" width="100%" height="500px" src="graphicfuentes">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Estados de los prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(5)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica5" width="100%" height="500px" src="graphicestados">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Jornadas con mas prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(6)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica6" width="100%" height="500px" src="graphicjornadas">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Generos con mas prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(7)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica7" width="100%" height="500px" src="graphicgeneros">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Ciudades con mas prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(8)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica8" width="100%" height="500px" src="graphicciudad">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Localidades con mas prospectos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportarAspirantes(9)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica9" width="100%" height="500px" src="graphiclocalidad">

                            </div>   
                        </div>
                    </div>
                </div>
                  
                
                
                
                 <script type="text/javascript">
    function graficoasp1(arregloasp) {
        
        Highcharts.setOptions({
            colors: ['#058DC7']
        });

        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Seguimientos por usuario'
            },
            subtitle: {
                text: 'Código: <a href="http://192.168.0.18/indo/public/aspirantes/prospectos">Indoamericana</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            series: [{
                name: 'Population',
                data: arregloasp,
                
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    }
    
    $(document).ready(function () {
     var arregloasp = new Array();
     @foreach($seguimientos_grafica as $seg)
     arregloasp.push(new Array('{{ $seg->name }} {{ $seg->last_name }}', {{ $seg->dato }}));
     @endforeach
    graficoasp1(arregloasp);
    });            
     
     function hola(){
         
         var arregloasp1 = new Array();     
     arregloasp1.push(new Array('Jose Betancur', 22));
     arregloasp1.push(new Array('John Tuta', 15));
     graficoasp1(arregloasp1);
         
     }
    
    setInterval(hola, 5000);
</script>
                
                
                                
            </div>
        </div>
    </div>      
    
   