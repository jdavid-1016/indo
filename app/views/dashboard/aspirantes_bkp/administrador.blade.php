
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Administrador</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class="active"><a href="#tab_1_1" data-toggle="tab">Metodos</a></li>
                        <li class=""><a href="#tab_1_2" data-toggle="tab">Campañas</a></li>
                        <li class=""><a href="#tab_1_3" data-toggle="tab">Fuentes</a></li>
                        <li class=""><a href="#tab_1_4" data-toggle="tab">Estados</a></li>
                        <li class=""><a href="#tab_1_5" data-toggle="tab">Documentos</a></li>
                        <li class=""><a href="#tab_1_6" data-toggle="tab">Convocatorias</a></li>
                        <li class=""><a href="#tab_1_7" data-toggle="tab">Salones</a></li>
                        <li class=""><a href="#tab_1_8" data-toggle="tab">Asignar Salones</a></li>

                    </ul>
                    <div  class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_1">
                            <div class="table-responsive" id="">
                                
                                <div class="row">
                                    <div class="col-md-9">                         
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Método</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($metodos as $metodo)
                                                 <tr style="text-align:center;" id="admin_metodo_{{$metodo->id}}">
                                                    
                                                    <td>{{$metodo->id}}</td>
                                                    <td>{{$metodo->inscription_method}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('1', '{{$metodo->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Método</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="item_metodo" value="">
                                           </div> 
                                        </div>
                                        <a class="btn btn-success" onclick="crear_metodo()"><i class="icon-ok"></i> Crear</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_2">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9">                            
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Campaña</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($campanas as $campana)
                                                 <tr style="text-align:center;" id="admin_campana_{{$campana->id}}">
                                                    
                                                    <td>{{$campana->id}}</td>
                                                    <td>{{$campana->campaign}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('2', '{{$campana->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Campaña</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="item_campania" value="">
                                           </div> 
                                        </div> 
                                        <a class="btn btn-success" onclick="crear_campania()"><i class="icon-ok"></i> Crear</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_3">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9"> 
                                                          
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Fuentes</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($fuentes as $fuente)
                                                 <tr style="text-align:center;" id="admin_fuente_{{$fuente->id}}">
                                                    
                                                    <td>{{$fuente->id}}</td>
                                                    <td>{{$fuente->source}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('3', '{{$fuente->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Fuente</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="item_fuente" value="">
                                           </div> 
                                        </div>
                                        <a class="btn btn-success" onclick="crear_fuente()"><i class="icon-ok"></i> Crear</a> 
                                    </div>  
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_4">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9">
                                                          
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Estados</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($estados as $estado)
                                                 <tr style="text-align:center;" id="admin_estado_{{$estado->id}}">
                                                    
                                                    <td>{{$estado->id}}</td>
                                                    <td>{{$estado->status}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('4', '{{$estado->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Estado</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="item_estado" value="">
                                           </div> 
                                        </div> 
                                        <a class="btn btn-success" onclick="crear_estado()"><i class="icon-ok"></i> Crear</a>
                                    </div>  
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_5">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9">
                                                          
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Documentos</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($documentos as $documento)
                                                 <tr style="text-align:center;" id="admin_documento_{{$documento->id}}">
                                                    
                                                    <td>{{$documento->id}}</td>
                                                    <td>{{$documento->document}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('5', '{{$documento->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Documento</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="item_documento" value="">
                                           </div> 
                                        </div> 
                                        <a class="btn btn-success" onclick="crear_documento()"><i class="icon-ok"></i> Crear</a>
                                    </div>  
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_6">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9">
                                                          
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Convocatorias</th>
                                                    <th>Periodo</th>
                                                    <th>Fecha</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($convocatorias as $convocatoria)
                                                 <tr style="text-align:center;" id="admin_convocatoria_{{$convocatoria->id}}">
                                                    
                                                    <td>{{$convocatoria->id}}</td>
                                                    <td>{{$convocatoria->convocation}}</td>
                                                    <td>{{$convocatoria->AspirantsPeriods->period}}</td>
                                                    <td>{{$convocatoria->date}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('6', '{{$convocatoria->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Convocatoria</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="Nombre" data-required="true" name="convocation" id="convocation" value="">
                                           </div> <br>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <select class="form-control" placeholder="Periodo" data-required="true" name="period_c" id="period_c">
                                                  <option value="0">Periodo</option>
                                                  @foreach($periodos as $periodo)
                                                  <option value="{{ $periodo->id }}">{{ $periodo->period }}</option>
                                                  @endforeach
                                              </select>
                                           </div><br>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control fecha_required" placeholder="Fecha" data-required="true" name="date_convocation" id="date_convocation" value="">
                                           </div>
                                        </div>
                                        <a class="btn btn-success" onclick="crear_convocatoria()"><i class="icon-ok"></i> Crear</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_7">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9">
                                                          
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Salones</th>
                                                    <th>Cupo</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody>
                                              @foreach($salones as $salon)
                                                 <tr style="text-align:center;" id="admin_salon_{{$salon->id}}">
                                                    
                                                    <td>{{$salon->id}}</td>
                                                    <td>{{$salon->classroom}}</td>
                                                    <td>{{$salon->quota}}</td>
                                                    
                                                    <td><a class="btn btn-danger" onclick="removeAdmin('7', '{{$salon->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Salon</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="Salon" data-required="true" name="classroom" id="classroom" value="">
                                           </div> <br>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="Cupo" data-required="true" name="quota" id="quota" value="">
                                           </div>
                                        </div>
                                        <a class="btn btn-success" onclick="crear_salon()"><i class="icon-ok"></i> Crear</a>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="tab-pane fade" id="tab_1_8">
                            <div class="table-responsive" id="">
                                <div class="row">
                                    <div class="col-md-9">
                                                          
                                        <table class="table top-blue table-bordered table-hover" data-target="soporte/callSupport/">
                                            <thead>
                                                <tr>
                                                    <th>id</th>
                                                    <th>Convocatoria</th>
                                                    <th>Salon</th>
                                                    <th>Tipo</th>
                                                    <th>Fecha</th>
                                                    <th>Hora</th>
                                                    <th>Operaciones</th>
                                                </tr>
                                            </thead>
                                           <tbody id="table-asignaciones">
                                              @foreach($asignaciones as $asig)
                                                 <tr style="text-align:center;" id="admin_asig_{{$asig->id}}">
                                                    
                                                    <td>{{$asig->id}}</td>
                                                    <td>{{$asig->convocation}}</td>
                                                    <td>{{$asig->classroom}}</td>
                                                    <td>{{$asig->type}}</td>
                                                    <td>{{$asig->date}}</td>
                                                    <td>{{$asig->time}}</td>
                                                    
                                                    <td>
                                                        <a class=" btn btn-default" data-target="#ajax1" id="{{$asig->id}}" data-toggle="modal" onclick='cargar_programas_asignacion($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a>
                                                        <a class="btn btn-danger" onclick="removeAdmin('8', '{{$asig->id}}');"><i class="icon-remove"></i></a></td>
                                                </tr>
                                              @endforeach
                                           </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Asignacion</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <select class="form-control" placeholder="Convocatoria" data-required="true" name="convocation_a" id="convocation_a">
                                                  <option value="0">Convocatoria</option>
                                                  @foreach($convocatorias as $convocatoria)
                                                  <option value="{{ $convocatoria->id }}">{{ $convocatoria->convocation }}</option>
                                                  @endforeach
                                              </select>
                                           </div> <br>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <select class="form-control" placeholder="Salon" data-required="true" name="classroom_a" id="classroom_a">
                                                  <option value="0">Salon</option>
                                                  @foreach($salones as $salon)
                                                  <option value="{{ $salon->id }}">{{ $salon->classroom }}</option>
                                                  @endforeach
                                              </select>
                                           </div><br>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <select class="form-control" placeholder="Tipo" data-required="true" name="type_a" id="type_a">
                                                  <option value="0">Tipo</option>
                                                  @foreach($tipos as $tipo)
                                                  <option value="{{ $tipo->id }}">{{ $tipo->type }}</option>
                                                  @endforeach
                                              </select>
                                           </div><br>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="Hora" data-required="true" name="time_a" id="time_a" value="">
                                           </div>
                                        </div>
                                        <a class="btn btn-success" onclick="crear_asignacion()"><i class="icon-ok"></i> Crear</a>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="modal fade" id="ajax1" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                    </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>