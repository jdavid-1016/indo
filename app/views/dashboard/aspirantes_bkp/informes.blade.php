<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>A date range picker for Bootstrap</title>
        <!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="all" href="{{ URL::to("assets/calendario/daterangepicker-bs3.css")}}" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ URL::to("assets/calendario/moment.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/calendario/daterangepicker.js")}}"></script>
    </head>
    <body>
<div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Informes
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="javascript:;">Página Principal</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="javascript:;">Aspirantes</a>
                            <i class="icon-angle-right"></i> 
                        </li>
                        <li>
                            <a href="javascript:;">Informes</a> 
                        </li>
                        <li class="pull-right">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                        </li>
                        <li class="pull-right">
                                   <select name="status_i" id="status_i" class="form-control">
                                       <option value="0">Estado</option>
                                       @foreach($estados as $est)
                                          <option  value="{{$est->id}}">{{$est->status}}</option>
                                       @endforeach
                                      </select>
                            </li>
                        
                    </ul>
                    
                    
                    <script type="text/javascript">
                        $(document).ready(function () {

                            var cb = function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                                var fecha_inicio = start.format('YYYY-M-D');
                                var fecha_fin = end.format('YYYY-M-D');
                                //alert(fecha_inicio);
                                //alert(fecha_fin);

//                                $.ajax({
//                                    type: "GET",
//                                    url: "changedate",
//                                    data: {fecha_inicio: fecha_inicio, fecha_fin: fecha_fin},
//                                })
//                                        .done(function (data) {
//                                            alert(data);
//                                        });

                                

                                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                            }

                            var optionSet1 = {
                                startDate: moment().subtract(29, 'days'),
                                endDate: moment(),
                                minDate: '01/01/2012',
                                maxDate: '12/31/2015',
                                dateLimit: {days: 900},
                                showDropdowns: true,
                                showWeekNumbers: true,
                                timePicker: false,
                                timePickerIncrement: 1,
                                timePicker12Hour: true,
                                ranges: {
                                    'Hoy': [moment(), moment()],
                                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                                    'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                                    'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                },
                                opens: 'left',
                                buttonClasses: ['btn btn-default'],
                                applyClass: 'btn-small btn-primary',
                                cancelClass: 'btn-small',
                                format: 'MM/DD/YYYY',
                                separator: ' to ',
                                locale: {
                                    applyLabel: 'Aplicar',
                                    cancelLabel: 'Clear',
                                    fromLabel: 'From',
                                    toLabel: 'To',
                                    customRangeLabel: 'Personalizada',
                                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                    firstDay: 1
                                }
                            };

                            var optionSet2 = {
                                startDate: moment().subtract(7, 'days'),
                                endDate: moment(),
                                opens: 'left',
                                ranges: {
                                    'Today': [moment(), moment()],
                                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                }
                            };

                            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

                            $('#reportrange').daterangepicker(optionSet1, cb);

                            $('#reportrange').on('show.daterangepicker', function () {
                                console.log("show event fired");
                            });
                            $('#reportrange').on('hide.daterangepicker', function () {
                                console.log("hide event fired");
                            });
                            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                                console.log("apply event fired, start/end dates are "
                                        + picker.startDate.format('MMMM D, YYYY')
                                        + " to "
                                        + picker.endDate.format('MMMM D, YYYY')
                                        );
                            });
                            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                                console.log("cancel event fired");
                            });

                            $('#options1').click(function () {
                                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
                            });

                            $('#options2').click(function () {
                                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
                            });

                            $('#destroy').click(function () {
                                $('#reportrange').data('daterangepicker').remove();
                            });

                        });
                    </script>
                    

                </div>
            </div>
            <div class="row">
                <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-group"></i>Informes Usuarios</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                           <thead>
                              <tr>
                                 <th><i class="icon-bar-chart"></i> Informe</th>
                                 <th class="hidden-xs"><i class="icon-question-sign"></i> Descripcion</th>
                                 <th><i class="icon-download-alt"></i> Descarga</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Seguimientos</a>
                                 </td>
                                 <td class="hidden-xs">Numero de seguimientos por usuario.</td>
                                 <td><a onclick="exportarinformesasp(1)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
<!--                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Usuarios</a>
                                 </td>
                                 <td class="hidden-xs">Breve descripcion del informe</td>
                                 <td><a href="generarpdfasp?id=2" target="_blank" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>-->
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>

            </div>
            <div class="row">
                <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-male"></i>Informes Prospectos</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                     </div>
                  </div>
                  <div class="portlet-body">
                     <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover">
                           <thead>
                              <tr>
                                 <th><i class="icon-bar-chart"></i> Informe</th>
                                 <th class="hidden-xs"><i class="icon-question-sign"></i> Descripcion</th>
                                 <th><i class="icon-download-alt"></i> Descarga</th>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Programas</a>
                                 </td>
                                 <td class="hidden-xs">Programas de mas interes.</td>
                                 <td><a onclick="exportarinformesasp(3)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Campañas</a>
                                 </td>
                                 <td class="hidden-xs">Campañas con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(4)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Estados</a>
                                 </td>
                                 <td class="hidden-xs">Estados de los aspirantes.</td>
                                 <td><a onclick="exportarinformesasp(5)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Fuentes de informacion</a>
                                 </td>
                                 <td class="hidden-xs">Fuentes de informacion con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(6)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Jornadas</a>
                                 </td>
                                 <td class="hidden-xs">Jornadas con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(7)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Edad</a>
                                 </td>
                                 <td class="hidden-xs">Rango de edades con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(8)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Genero</a>
                                 </td>
                                 <td class="hidden-xs">Generos con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(9)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Ciudad</a>
                                 </td>
                                 <td class="hidden-xs">Ciudades con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(10)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                              <tr>
                                 <td class="highlight">
                                    <div class="success"></div>
                                    <a href="#">Localidad</a>
                                 </td>
                                 <td class="hidden-xs">Localidades con mas prospectos.</td>
                                 <td><a onclick="exportarinformesasp(11)" class="btn btn-default btn-xs purple"><i class="icon-edit"></i>Descargar</a></td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>

            </div>
        </div>  

<script>
    
    function exportarinformesasp(id) {
        var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
        var fecha_fin = $('input:text[name=daterangepicker_end]').val();
        var estado = $('#status_i').val();
        
        var dir = "generarpdfasp?inicio=" + fecha_inicio + "&fin=" + fecha_fin+ "&id=" + id+ "&estado=" + estado;
        
        window.open(dir);
    }

</script>