<div class="modal-dialog modal-wide" >
    <div class="modal-content">
    <form class="form" action="#" ng-submit="adminSupport()">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Ferias de en la Institucion</h4>
        </div>
        <div class="modal-body form-body">
            <!--  -->
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group" id="frm_comentario">
                     
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_seguimientos">
                              Ferias
                              </a>
                           </h4>
                        </div>
                        <div id="collapse_seguimientos" class="panel-collapse " style="height: auto;">
                            <table class="table top-blue">
                                <thead>
                                    <tr>
                                        <th>Comentario</th>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    @foreach($ferias_agenda as $ferias_agenda)
                                    <tr>
                                        <td>{{$ferias_agenda->observacion}}</td>
                                        <td>{{$ferias_agenda->date_fair}}</td>
                                        <td>{{$ferias_agenda->date_fair_end}}</td>
                                        
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                               
                        </div>
                     </div> 

                  </div>
               </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Fecha Inicio</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control fecha_required_next"  id="fecha_ini" value="">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Fecha Fin</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control fecha_required_next" id="fecha_fin" value="">
                       </div> 
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Observación</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <textarea class="form-control fecha_required_asp"  id="Observacion_feria" value=""> </textarea>
                       </div> 
                    </div> 
                </div> 
                
            </div>

            
        </div>
        <div class="modal-footer">
            <!-- <button type="button" class="btn btn-info" onclick="Descargarfomatoasp()">Formato</button> -->
            <a class="btn btn-success" onclick="ingresar_feria({{$id_agenda}})"><i class="icon-edit"></i> Ingresar Seguimiento</a>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_metodo">Cerrar</button>
        </div>
  
    </form>   
    </div>
</div>

<script src="/indo/public/js/upload.js"></script>

<script>
    
$( ".fecha_required_next" ).datepicker({ dateFormat: "dd/mm/yy", minDate: '+0d' });
    
function ingresar_feria(id){

    var fecha_ini = $("#fecha_ini").val();
    var fecha_fin = $("#fecha_fin").val();
    var Observacion_feria = $("#Observacion_feria").val();
    
    if(fecha_ini == ""){
        toastr.error('El campo fecha_ini esta vacio', 'Error');
        $("#fecha_ini").focus();
        return;
    }
    if(fecha_fin == ""){
        toastr.error('El campo fecha_fin esta vacio', 'Error');
        $("#fecha_fin").focus();
        return;
    }
    if(Observacion_feria == ""){
        toastr.error('El campo Observacion_feria esta vacio', 'Error');
        $("#Observacion_feria").focus();
        return;
    }
    
    
    
   var html = $.ajax({
       type: "GET",
       url: "ingresarferias",
       data: {fecha_ini:fecha_ini,fecha_fin:fecha_fin, Observacion_feria:Observacion_feria, id:id},
       async: false
   }).responseText;

   if (parseInt(html) == 1) {

       toastr.success('Se ingreso correctamente la nueva feria', 'Nueva Feria');
       cargar_seguimientos_agenda(id);
       
       
   } else {
       toastr.error('No ha sido posible Ingresar la nueva feria', 'Error');
   }



}
</script>