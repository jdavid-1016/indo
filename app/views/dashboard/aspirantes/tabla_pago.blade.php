
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         
         <th>id</th>
         <th>Nombre</th>
         <th>Email</th>
         <th>Telefono</th>
         <th>Ciudad</th>
         <th>Seguimientos</th>
         <th>Usuario</th>
         <th>Estado</th>
         <th class="td_center"><i class="icon-time"></i></th>
      </tr>
   </thead>
   <tbody>
      @foreach($aspirantes as $aspirante)
         <tr style="text-align:center;" class="{{$aspirante['row_color']}}">
            
            <td> {{$aspirante['id']}}</td>
            <td>{{$aspirante['user']}}</td>
            <td>{{$aspirante['email']}}</td>
            <td>{{$aspirante['phone']}}</td>
            <td>{{$aspirante['city']}}</td>
            <td><span class="label label-sm label-danger informacion" title="{{$aspirante['traces']}}">{{$aspirante['seg']}}</span></td>
            <td>
                <a href="#" class="pull-left nameimg" title="{{$aspirante['user_seg']}}">
                  <img alt="" style="border-radius:50px;" width="50" height="50"  src="../{{$aspirante['img_min']}}">
                </a></td>
            <td>{{$aspirante['state']}}</td>
            <td><div class="alert alert-warning">{{$aspirante['tiempo']}}</td></div></tr>
      @endforeach
   </tbody>
</table>

<div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
   <div class="modal-content">
   <form class="form" action="#">
   
      <input type="hidden" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle de prospecto</h4>
      </div>
      <div class="modal-body form-body">
         <div class="col-md-12">
            <div class="form-group">
               <label class="control-label">Comentario</label>
               <div class="input-group">
                   <span class="input-group-addon"><i class="icon-edit"></i></span>
                   <textarea rows="2" class="form-control" id="observation"></textarea>
               </div>
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <!-- <button type="button" class="btn btn-success" onclick="asing_support()">Guardar</button> -->
         <button type="button" class="btn btn-danger rechazar_solicitud"  id="" onclick="realizarProceso2($(this).attr('id'));"><i class="icon-remove"></i>  Rechazar pago</button>
         
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cancelar</button>  
      </div>
   
   </form>   
   </div>
</div>
</div>