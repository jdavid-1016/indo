<link href="{{  URL::to("assets/css/pages/about-us.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::to("assets/css/pages/portfolio.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">  

    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Galeria 
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Aspirantes</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> Galerias</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-file-text"></i>Galeria Feria Colegio Indoamericana
                            </div>
                        </div>
                        <?php
                        if(Auth::user()->id==194){
                        $permisos = 1;
                        }
                        ?>
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">Galeria</a></li>
                                <li><a href="#tab_1_2" data-toggle="tab">Subir Fotos</a></li>
                            </ul>
                            
                            
        <div class="tab-content">

             <div class="tab-pane active" id="tab_1_1">
                        <!-- BEGIN FILTER -->                                                              
                  <div class="tab-content">
                        <!-- BEGIN FILTER -->           
                        <div class="margin-top-10">
                           <ul class="mix-filter">
                              <li class="filter btn" data-filter="all">Todas</li>                              
                              
                           </ul>
                           <div class="row mix-grid">
                               @foreach($imagenes as $img)
                              <div class="col-md-3 col-sm-4 mix 1">
                                 <div class="mix-inner">
                                    <img class="img-responsive" style="width: 384px; height: 282px;" src="{{ URL::to("".$img->photo."")}}" alt="">
                                    <div class="mix-details">
                                       <h4>Galeria feria</h4>
                                       <a class="mix-link" id="img-{{ $img->id }}" ><i class="icon-thumbs-up-alt"> </i></a>
                                       <a class="mix-preview fancybox-button" href="{{ URL::to("".$img->photo."")}}" title="{{$img->text}}" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                    </div>
                                 </div>
                              </div>
                               @endforeach
                           </div>
                        </div>
                        <!-- END FILTER -->
                  </div>                           
                        <!-- END FILTER -->    
             </div>

                <div class="tab-pane" id="tab_1_2">
                        <!-- BEGIN FILTER -->                                                              
                        <div class="tab-content">
                              <!-- BEGIN FILTER -->           
                              <div class="margin-top-10">
               <form class="forminterno" enctype="multipart/form-data" id="forminterno" name="forminterno" action="guardarimgasp" method="post">
                                 <div class="form-group has-success"> 
                       <label class="control-label">Subir imagenes</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                      <input type="file" class="default form-control" name="img_galeria[]" id="img_galeria" size="20" multiple="multiple">
                      <input type="hidden" class="default form-control" name="idgal" id="idgal" value="{{ $idgal }}">
                       </div>
                       <br>
                       <div class="input-group">
                       <input class="btn btn-success" type="submit" value="Guardar">
                    </div>
                       </div>
                                      </form>
                              </div>
                              <!-- END FILTER -->
                        </div>                           
                        <!-- END FILTER -->    
                </div>

        </div>
                                <!--/thumbnails-->
                                <!-- //End Meer Our Team -->        
                                <!-- END PAGE CONTENT-->
                            </div>
                            <!-- END PAGE -->    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
    <!-- END CONTAINER -->