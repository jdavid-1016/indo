
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Prospectos</div>                        
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class=""><a href="prospectos">Prospectos</a></li>
                  <li class=""><a href="inscritos" >Inscritos</a></li>
                  <li class=""><a href="radico" >Radico</a></li>
                  <li class=""><a href="examenf" >Examén</a></li>
                  <li class=""><a href="pagos" >Pago</a></li>
                  <!--<li class=""><a href="pendientes" >Pendientes</a></li>-->
                  <li class="active"><a href="buscar" >Buscar</a></li>
                  <!--<li class=""><a href="descartados" >Descartados</a></li>-->
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                      <div>
   <form class="form-inline" action="verequipos" method="get">
      <div class="search-region">
          <div class="form-group">
          <input type="text" class="form-control" id="nodoc" placeholder="No. Documento" value="{{Input::get('id')}}" onkeyup="tabla_lista_buscar()">
          </div>
          <div class="form-group">
          <input type="text" class="form-control" id="nameasp" placeholder="Nombres" value="{{Input::get('id')}}" onkeyup="tabla_lista_buscar()">
          </div>
          <div class="form-group">
          <input type="text" class="form-control" id="emailasp" placeholder="Email" value="{{Input::get('id')}}" onkeyup="tabla_lista_buscar()">
          </div>
         <div class="form-group">
            <select class="form-control input-small input-medium select2me" name="status" id="status" onchange="tabla_lista_buscar()">
                                       <option value="">Estado</option>
                                       @foreach($estados as $estado)
                                          <option  value="{{$estado->id}}">{{$estado->status}}</option>
                                       @endforeach
            </select>
         </div>
         <div class="form-group">
            <select class="form-control input-small input-medium select2me" name="programa" id="programa" onchange="tabla_lista_buscar()">
                                       <option value="">Programa</option>
                                       @foreach($programas as $programa)
                                          <option  value="{{$programa->id}}">{{$programa->name}}</option>
                                       @endforeach
                                    </select>
         </div>
         <div class="form-group">
            <input type="text" class="form-control" id="code" name="code" placeholder="Codigo">
        </div>
          <div class="form-group">
                                    <button onclick="tabla_lista_buscar()" type="button" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                                 </div>
          
             
      </div>
   </form> 
</div>
                     <div class="table-responsive" id="table_admin_buscar">

                        
                     </div>
                     <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>

<div class="modal fade" id="nuevoregistro" tabindex="-1" role="basic" aria-hidden="true">
                        <div class="modal-dialog">
                           <div class="modal-content">
   <form class="form" action="#">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Prospecto</h4>
      </div>
      <div class="modal-body form-body">
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Nombre</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="name_n" value="">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Email</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control" placeholder="" data-required="true" name="email_n" id="email_n" value="">
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Telefono</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control" placeholder="" data-required="true" name="phone_n" id="phone_n" value="">
                  </div>
               </div>
            </div>
             
             <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Telefono 2</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control" placeholder="" data-required="true" name="phone2_n" id="phone2_n" value="">
                  </div>
               </div>
            </div>
            
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Ciudad</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control" placeholder="" data-required="true" name="city_n" id="city_n" value="">
                  </div>
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Documento</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control" placeholder="" data-required="true" name="document_n" id="document_n" value="">
                  </div>
               </div>
            </div>
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Fuente</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select name="fuentes_n" id="fuentes_n" class="form-control">
				                    <option value="1">Directorio Paginas Amarillas</option>
									<option value="2">www.paginasamarillas.com</option>
									<option value="3">www.indoamericana.edu.co</option>
									<option value="4">www.emagister.com</option>
									<option value="5">Facebook</option>
									<option value="7">Email</option>
									<option value="8">Emisora</option>
									<option value="9">Valla</option>
									<option value="10">Revistas Especializadas</option>
									<option value="11">Feria Colegio</option>
									<option value="12">Fachada de la Institución</option>
									<option value="13">Feria Expo-Extudiantes Corferias</option>
									<option value="14">Feria Expo-Ciencia Corferias</option>
									<option value="15">Feria LIbro Corferias</option>
									<option value="19">Referido por Estudiante CEI</option>
									<option value="20">Referido por Profesor</option>
									<option value="21">Referido por Egresado</option>
									<option value="22">Referido por Familiar</option>
									<option value="23">Referido por Amigo</option>
									<option value="26">Uniforme de la Institución.</option>
									<option value="27">Google</option>
									<option value="31">Afiche, Volante</option>
									<option value="32">Taller Aeronáutico o Visita al Hangar</option>
									<option value="33">aprendemas.com</option>
							</select>
                  </div> 
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Campaña</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select name="campaign_n" id="campaign_n" class="form-control">
                         @foreach($campanas as $cam)
                         <option  value="{{ $cam->id }}">{{ $cam->campaign }}</option>
                         @endforeach
                     </select>
                  </div> 
               </div>   
            </div>
            
         </div>
          
          <div class="row">
          <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Metodo Inscripcion</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select name="method_n" id="method_n" class="form-control">
                         @foreach($metodos as $met)
                         <option  value="{{ $met->id }}">{{ $met->inscription_method }}</option>
                         @endforeach
                     </select>
                  </div>
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Programa</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select name="programs_n" id="programs_n" class="form-control">
                         @foreach($programas as $pro)
                         <option  value="{{ $pro->id }}">{{ $pro->name }}</option>
                         @endforeach
                     </select>
                  </div> 
               </div>   
            </div> 
              </div>

      </div>
       
      <div class="modal-footer">
      
      
            <button type="button" class="btn btn-success rechazar_solicitud" id="botonguardarnuevo" onclick="guardarNuevosAspirantes()"><i id="icon" class="icon-ok"></i>Guardar</button>
         
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   </form>
   </div>
                        </div>
                        <!-- /.modal-dialog -->}
</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
    Tipped.create('.nameimg', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
  });
  
  function recargarTabla(){
       
       var html = $.ajax({
                     type: "GET",
                     url: "prospectos",
                     cache: false,
                     data: {nombre: "hola"},
                     async: false
                    }).responseText;
                                        
        $('#table_admin_prospects').html(html);
       
   }
   
   function tabla_lista_buscar(){
          
    var emailasp = $("#emailasp").val();
    var nameasp = $("#nameasp").val();
    var nodoc = $("#nodoc").val();
    var code = $("#code").val();
    var status = $("#status").val();
    var programa = $("#programa").val();
          
        $.get('buscar', {
            emailasp: emailasp,
            nameasp: nameasp,
            nodoc: nodoc,
            code: code,
            status: status,
            programa: programa,
            nombre: 1
        } ,

        function(response){
            $('#table_admin_buscar').html(response);
            //var datos = JSON.parse(response);
        });
    }
</script>