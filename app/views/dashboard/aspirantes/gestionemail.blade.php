<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Gestion de Emails
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Aspirantes</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Gestion Emails</a> 
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="page-breadcrumb breadcrumb">
                <h4><b>Enviar un nuevo Email</b></h4>
            </ul>
        </div>
    </div>
    <div class="row">
        {{Form::open( array('url'=>'aspirantes/enviaremail', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-1 control-label">Para:</label>
                    <div class="col-md-6">
                        
                        <div class="form-group">
                            <label class="col-md-4 control-label">Convocatoria</label>
                            <div class="col-md-8">
                                <select id="convocatoria_id" name="convocatoria_id" class="form-control">
                                    @foreach($convocatorias as $con)
                                    <option value="{{$con->id}}">{{$con->convocation}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <!--<div class="form-group">
                            <label class="col-md-4 control-label">Periodo</label>
                            <div class="col-md-8">
                                <select id="pensum_id" class="form-control">
                                    @foreach($periodos as $per)
                                    <option value="{{$per->id}}">{{$per->period}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>-->
                        <div class="form-group">
                            <label class="col-md-4 control-label">Programas</label>
                            <div class="col-md-8">
                                <select id="programa_id" name="programa_id" class="form-control">
                                <option value="">Programa</option>
                                    @foreach($programas as $pro)
                                    <option value="{{$pro->id}}">{{$pro->acronyms}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-4 control-label">Estados</label>
                            <div class="col-md-8">
                                <select id="estado_id" name="estado_id" class="form-control">
                                    <option value="">Estado</option>
                                    @foreach($estados as $est)
                                    <option value="{{$est->id}}">{{$est->status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div><br>
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-1 control-label">Asunto:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="asunto" name="asunto" required/>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Mensaje:</label>
                    <div class="col-md-6">
                        <textarea class="ckeditor form-control" id="mensaje" name="mensaje" rows="6"></textarea>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Archivo Adjunto:</label>
                    <div class="col-md-4">
                        <input type="file" class="default form-control" name="file_email" id="file_email" size="10">
                    </div>
                </div>
            </div>
        </div>
        <div class="form-actions fluid">
            <div class="col-md-1 col-md-6">
                <button type="submit" class="btn btn-info btn-block">Enviar</button>
            </div>
        </div>            

        {{Form::close()}}
    </div>
</div>