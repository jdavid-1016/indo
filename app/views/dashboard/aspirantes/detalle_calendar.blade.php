<?php 
   $clase = '';
  if ($detalle_agendas[0]->class == "event-info") {
    $clase = 'note-info';
  }
  if ($detalle_agendas[0]->class == "event-important") {
    $clase = 'note-danger';
  }
  if ($detalle_agendas[0]->class == "event-success") {
    $clase = 'note-success';
  }
  if ($detalle_agendas[0]->class == "event-warning") {
    $clase = 'note-warning';
  }
  

?>

<div class="modal-dialog " >
    <div class="modal-content">
    <form class="form" action="#" ng-submit="adminSupport()">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Infrmación {{$detalle_agendas[0]->observacion}}</h4>
        </div>
        <div class="modal-body form-body {{$clase}}">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Nombre Institución</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="nombre_edit" value="{{$detalle_agendas[0]->AspirantsSchoolAgendas->name}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Nombre Contacto</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="contacto_edit" value="{{$detalle_agendas[0]->AspirantsSchoolAgendas->name_contact}}">
                       </div> 
                    </div> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Dirección</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="direccion_edit" value="{{$detalle_agendas[0]->AspirantsSchoolAgendas->address}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Teléfono</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="telefono_edit" value="{{$detalle_agendas[0]->AspirantsSchoolAgendas->phone}}">
                       </div> 
                    </div> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Email</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="email_edit" value="{{$detalle_agendas[0]->AspirantsSchoolAgendas->email}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Fecha Feria</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control fecha_required" placeholder="" data-required="true" name="name_n" id="feria_edit" value="{{$detalle_agendas[0]->date_fair}}">
                       </div> 
                    </div> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Rector</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="rector_edit" value="{{$detalle_agendas[0]->AspirantsSchoolAgendas->name_rector}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Observaciones</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <textarea  class="form-control" placeholder="" data-required="true" name="name_n" id="observaciones_edit" >{{$detalle_agendas[0]->AspirantsSchoolAgendas->observations}}</textarea>
                       </div> 
                    </div> 
                </div>   
            </div>

            
        </div>
        <div class="modal-footer">
            <!-- <a class="btn btn-success" onclick="editar_agenda({{$detalle_agendas[0]->id}})"><i class="icon-edit"></i> Editar</a> -->
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_detalle">Cerrar</button>
        </div>
  
    </form>   
    </div>
</div>