<div class="modal-dialog modal-wide">
   <div class="modal-content ">
   <form class="form" action="#" >   
      <input type="hidden" value="" id="prospect_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle Programas</h4>
      </div>
      <div class="modal-body form-body">
                  
               <form name="formpro" id="formpro">
            <div class="row">
                <div class="col-md-6">
                   <div class="form-group">
                      <label class="control-label">Programa</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         
                         <select class="form-control" placeholder="" data-required="true" name="programa_asignar" id="programa_asignar" value=""> 
                            <option value=""></option>
                            @foreach($programas as $pro)
                            <option value="{{$pro->id}}">{{$pro->name}}</option>
                            @endforeach
                         </select>
                      </div>
                   </div>
                </div>
            </div>
                   </form>
        
            <table class="table top-blue" data-target="soporte/callSupport/">
                <thead>
                    <tr>
                        <th>Programa</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody id="tabla_programas_asig">

                    @foreach($asignaciones_programas as $asignado)
                    <tr style="text-align:center;">
                        <td>{{$asignado->name}}</td>
                        <td><a href="#" class="btn btn-danger"><i class="icon-remove"></i></a></td>

                    </tr>
                    @endforeach

                </tbody>
            </table>

      </div>
      <div class="modal-footer">

         <button type="button" class="btn btn-success" onclick="asignar_programas({{ $id_asignacion }})">Asignar Programa</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>

   </form>
   </div>
</div>

<script type="text/javascript">
    function asignar_programas(id){
        var programa_asignar = $("#programa_asignar").val();        
        var programa_name = $("#programa_asignar option:selected").text();

        $.get('asignarprogramas', {
            programa_asignar: programa_asignar,
            id:id
        } ,

        function(response){
           $('#tabla_programas_asig').append('<tr style="text-align:center;">\n\
                    <td>'+programa_name+'</td><td><a href="#" class="btn btn-danger"><i class="icon-remove"></i></a></td></tr>');
            toastr.success('Se Asignó correctamente el programa', 'Asignación de programa');
        });

    }
</script>