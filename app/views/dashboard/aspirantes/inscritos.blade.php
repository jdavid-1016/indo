
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Inscritos</div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class=""><a href="prospectos">Prospectos</a></li>
                  <li class="active"><a href="inscritos" >Inscritos</a></li>
                  <li class=""><a href="radico" >Radico</a></li>
                  <li class=""><a href="examenf" >Examén</a></li>
                  <li class=""><a href="pagos" >Pago</a></li>
                  <!--<li class=""><a href="pendientes" >Pendientes</a></li>-->
                  <li class=""><a href="buscar" >Buscar</a></li>
                  <!--<li class=""><a href="descartados" >Descartados</a></li>-->
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_supports">
                        <div>
                           <form class="form-inline" action="prospectos" method="get">
                              <div class="search-region">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                                 </div>
                                 
                                 
                                 <div class="form-group">
                                    <select class="form-control input-small" name="status">
                                       <option value="">Estado</option>
                                       @foreach($estados as $estado)
                                          <option  value="{{$estado->id}}">{{$estado->status}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                                 <div class="form-group">
                                    <select class="form-control input-small" name="programa">
                                       <option value="">Programa</option>
                                       @foreach($programas as $programa)
                                          <option  value="{{$programa->id}}">{{$programa->name}}</option>
                                       @endforeach
                                    </select>
                                 </div>
                                 
                                 <div class="form-group">
                                    <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                                 </div>    
                              </div>
                           </form> 
                        </div>
                        @include('dashboard.aspirantes.tabla_inscritos')
                     </div>
                     <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />
<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
    Tipped.create('.nameimg', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
  });
</script>