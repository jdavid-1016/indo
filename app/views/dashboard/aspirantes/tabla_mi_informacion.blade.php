<div class="tab-pane " id="inscritos">
                <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="tabbable tabbable-custom">
                <div class="tab-content">
                <div class="row">
                                    <div class="col-md-12">

                                        <form class="form-horizontal" id="form">
                                            <div class="form-body">
                                                <div class="row">                            
                                                    <div class="col-md-12 note note-warning">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                            </div>
                                                            <div class="col-md-2">                
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label class="control-label">Imagen</label>
                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                                <img src="../{{$inscrito[0]->image}}" alt="">
                                                                            </div>
                                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                            <div>
                                                                                <span class="btn btn-default btn-file">
                                                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                                    <input type="file" class="default form-control" name="imagen_asp" id="imagen_asp" size="20">
                                                                                </span>
                                                                                <div class="help-block">
                                                                                    Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Programa Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="programs" id="programs" class="form-control">
                                                                         @foreach($programas as $prog)
                                                                         <option <?php if($inscrito[0]->aspirants_programs_id==$prog->id){ echo "selected"; } ?> value="{{ $prog->id }}">{{ $prog->name }}</option>
                                                                         @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Periodo Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="period" id="period" class="form-control">
                                                                            @foreach($periodos as $per)
                                                                            <option <?php if($inscrito[0]->aspirants_periods_id==$per->id){ echo "selected"; } ?> value="{{ $per->id }}">{{ $per->period }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Tipo de Aspirante</label>
                                                                    <div class="col-md-4">
                                                                        <select name="type_aspirant" id="type_aspirant" class="form-control">
                                                                            <option value="Nuevo">Nuevo</option>
                                                                            <option value="Homologante Externo">Homologante Externo</option>
                                                                            <option value="Reintegro">Reintegro</option>
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Jornada</label>
                                                                    <div class="col-md-6">
                                                                        <div class="radio-list">
                                                                            <label class="radio-inline">
                                                                                <div id="uniform-optionsRadios25"><input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked></div> Mañana
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                                <div  id="uniform-optionsRadios26"><input type="radio" name="optionsRadios" id="optionsRadios26" value="option2" ></div> Tarde
                                                                            </label>
                                                                            <label class="radio-inline">
                                                                                <div  id="uniform-optionsRadios27"><input type="radio" name="optionsRadios" id="optionsRadios27" value="option3" disabled></div> Noche
                                                                            </label>  
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="row">

                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Personales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="first_name" name="first_name" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->first_name}}" placeholder="Primer Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="middle_name" name="middle_name" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->middle_name}}" placeholder="Segundo Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name" name="last_name" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->last_name}}" placeholder="Primer Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name2" name="last_name2" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->last_name2}}" placeholder="Segundo Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="type_document" id="type_document" class="form-control">
                                                                            <option value="0">Tipo de documento</option>
                                                                            @foreach($tipos_documentos as $tipo)
                                                                            <option value="{{ $tipo->id }}" <?php if($inscrito[0]->aspirants_type_documents_id==$tipo->id){ echo "selected"; } ?>>{{ $tipo->type }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="document" name="document" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->document}}" placeholder="N° Doc Identidad">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="training" id="training" class="form-control">
                                                                            <option value="0">Nivel de formacion</option>
                                                                            @foreach($formacion as $for)
                                                                            <option value="{{ $for->id }}" <?php if($inscrito[0]->aspirants_trainings_id==$for->id){ echo "selected"; } ?>>{{ $for->training }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="military_card" id="military_card" class="form-control">
                                                                            <option value="0">Libreta Militar</option>
                                                                            <option value="1" <?php if($inscrito[0]->military_card==1){ echo "selected"; } ?>>Si (Libreta Militar)</option>
                                                                            <option value="2" <?php if($inscrito[0]->military_card==2){ echo "selected"; } ?>>No (Libreta Militar)</option>
                                                                        </select>
                                                                        
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="birthday" name="birthday" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->birthday}}" placeholder="Fecha Nacimiento">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="marital_status" id="marital_status" class="form-control">
                                                                            <option value="0">Estado Civil</option>
                                                                            @foreach($estado_civil as $estado)
                                                                            <option value="{{ $estado->id }}" <?php if($inscrito[0]->aspirants_marital_statuses_id==$estado->id){ echo "selected"; } ?>>{{ $estado->marital_status }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="gender" id="gender" class="form-control">
                                                                            <option value="0">Genero</option>
                                                                            @foreach($generos as $genero)
                                                                            <option value="{{ $genero->id }}" <?php if($inscrito[0]->aspirants_genders_id==$genero->id){ echo "selected"; } ?>>{{ $genero->aspirants_gender }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="blood_group" id="blood_group" class="form-control">
                                                                            <option value="0">Grupo Sanguineo</option>
                                                                            <option value="O+" <?php if($inscrito[0]->blood_group=="O+"){ echo "selected"; } ?>>O+</option>
                                                                            <option value="O-" <?php if($inscrito[0]->blood_group=="O-"){ echo "selected"; } ?>>O-</option>
                                                                            <option value="A+" <?php if($inscrito[0]->blood_group=="A+"){ echo "selected"; } ?>>A+</option>
                                                                            <option value="A-" <?php if($inscrito[0]->blood_group=="A-"){ echo "selected"; } ?>>A-</option>
                                                                            <option value="AB+" <?php if($inscrito[0]->blood_group=="AB+"){ echo "selected"; } ?>>AB+</option>
                                                                            <option value="AB-" <?php if($inscrito[0]->blood_group=="AB-"){ echo "selected"; } ?>>AB-</option>
                                                                            <option value="B+" <?php if($inscrito[0]->blood_group=="B+"){ echo "selected"; } ?>>B-</option>
                                                                            <option value="B-" <?php if($inscrito[0]->blood_group=="B-"){ echo "selected"; } ?>>B+</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nacionalidad">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address" name="address" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->address}}" placeholder="Dirección de Residencia">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="location" id="location" class="form-control">
                                                                            <option value="0">Locacion</option>
                                                                            @foreach($locations as $location)
                                                                            <option value="{{ $location->id }}" <?php if($inscrito[0]->aspirants_locations_id==$location->id){ echo "selected"; } ?>>{{ $location->location }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="neighborhood" name="neighborhood" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->neighborhood}}" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ciudad de Residencia" value="">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone" name="phone" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->phone}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cell_phone" name="cell_phone" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->cell_phone}}" placeholder="Celular" value="">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="email" name="email" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->email}}" placeholder="Email" value="">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Lugar de Nacimiento">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="observations" class="form-control campos_cpu" rows="3" placeholder="Observaciones">{{$inscrito[0]->observations}}</textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Laborales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="company" name="company" type="text" class="form-control campos_cpu" value="{{$work[0]->company}}" placeholder="Empresa Donde Trabaja">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation" name="occupation" type="text" class="form-control campos_cpu" value="{{$work[0]->occupation}}" placeholder="Cargo Actual">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="work_phone" name="work_phone" type="text" class="form-control campos_cpu" value="{{$work[0]->phone}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="work_address" name="work_address" type="text" class="form-control campos_cpu" value="{{$work[0]->address}}" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4 class="block">Datos Familiares</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name_father" name="name_father" type="text" class="form-control campos_cpu" value="{{$family[0]->name_father}}" placeholder="Nombre Padre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation_father" name="occupation_father" type="text" class="form-control campos_cpu" value="{{$family[0]->occupation_father}}" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address_father" name="address_father" type="text" class="form-control campos_cpu" value="{{$family[0]->address_father}}" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone_father" name="phone_father" type="text" class="form-control campos_cpu" value="{{$family[0]->phone_father}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name_mother" name="name_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->name_mother}}" placeholder="Nombre Madre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation_mother" name="occupation_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->occupation_mother}}" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address_mother" name="address_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->address_mother}}" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone_mother" name="phone_mother" type="text" class="form-control campos_cpu" value="{{$family[0]->phone_mother}}" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name_keeper" name="name_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->name_keeper}}" placeholder="Nombre Acudiente">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="occupation_keeper" name="occupation_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->occupation_keeper}}" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="address_keeper" name="address_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->address_keeper}}" placeholder="Dirección">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="phone_keeper" name="phone_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->phone_keeper}}" placeholder="Teléfono">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="cellphone_keeper" name="cellphone_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->cellphone_keeper}}" placeholder="Celular">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="email_keeper" name="email_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->email_keeper}}" placeholder="Email">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="neighborhood_keeper" name="neighborhood_keeper" type="text" class="form-control campos_cpu" value="{{$family[0]->neighborhood}}" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3> &nbsp &nbsp</h3>

                                                    </div>
                                                </div>
                                                <div class="row">


                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Estudios Realizados</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="school" name="school" type="text" class="form-control campos_cpu" value="{{$studies[0]->school}}" placeholder="*Bachillerato: Nombre de la Institucion:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="university" name="university" type="text" class="form-control campos_cpu" value="{{$studies[0]->university}}" placeholder="Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="career" name="career" type="text" class="form-control campos_cpu" value="{{$studies[0]->career}}" placeholder="Carrera Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="others" name="others" type="text" class="form-control campos_cpu" value="{{$studies[0]->others}}" placeholder="Otros:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="motivation" name="motivation" class="form-control campos_cpu"  placeholder="Que lo motivo a estudiar en la CORPORACIÓN EDUCATIVA INDOAMERICANA:">{{$studies[0]->motivation}}</textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Fuentes De Infomación</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="source" id="source" class="form-control">
                                                                            <option value="0">Fuente de informacion</option>
                                                                            @foreach($fuentes as $fuente)
                                                                            <option value="{{ $fuente->id }}" <?php if($inscrito[0]->aspirants_sources_id==$fuente->id){ echo "selected"; } ?>>{{ $fuente->source }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                        
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="source_details" name="source_details" type="text" class="form-control campos_cpu" value="{{$inscrito[0]->source_details}}" placeholder="*Especifique:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-actions fluid">
                                                    <div class="col-md-1 col-md-11">
                                                        <a class="btn btn-success" onclick="guardareditasp()">Guardar</a>
                                                    </div>
                                                </div>                        
                                            </div>
                                        </form>

                                    </div>
                                </div>
                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
            </div>

<div class="pagination">
</div>
<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });    
  });
</script>
<script type="text/javascript">
   function cargarDetalleCursos(id){
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'detallemicurso',
         type: 'get',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
</script>
<script type="text/javascript">
    function asignarAlumnoCurso(id){
        var parametros = {
            "id": id
        };
        $.ajax({
            data: parametros,
            url:  'asignaralumnocurso',
            type: 'get',

            success: function(response){
               $("#asignar_alumnos_curso").html(response);
            }
        });
    }
</script>