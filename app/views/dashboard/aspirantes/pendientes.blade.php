
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Inscritos</div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class=""><a href="prospectos">Prospectos</a></li>
                  <li class=""><a href="inscritos" >Inscritos</a></li>
                  <li class=""><a href="radicados" >Radico</a></li>
                  <li class=""><a href="examen" >Examén</a></li>
                  <li class=""><a href="pago" >Pago</a></li>
                  <li class="active"><a href="pendientes" >Pendientes</a></li>
                  <li class=""><a href="buscar" >Buscar</a></li>
                  <!--<li class=""><a href="descartados" >Descartados</a></li>-->
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                      <div>
   <form class="form-inline" action="verequipos" method="get">
      <div class="search-region">         
         <div class="form-group">
            <select class="form-control input-medium select2me" name="applicant" id="applicant" onchange="tabla_lista_pendientes()">
            <option value="">Usuario</option>
            @foreach($nombres as $nom)
            <option value="{{ $nom->id }}">{{ $nom->name }}</option>
            @endforeach
            </select>
         </div>
         <div class="form-group">
            <select class="form-control input-medium select2me" name="documento" id="documento" onchange="tabla_lista_pendientes()">
            <option value="">Documento</option> 
            @foreach($documentos as $doc)
            <option value="{{ $doc->id }}">{{ $doc->document }}</option>
            @endforeach
            </select>
         </div>
         <div class="form-group">
            <input type="text" class="form-control fecha_required_asp" id="f_pendiente" name="f_pendiente" placeholder="Fecha Entrega">
        </div>
          <div class="form-group">
                                    <button onclick="tabla_lista_pendientes()" type="button" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                                 </div>
          
             
      </div>
   </form> 
</div>
                     <div class="table-responsive" id="table_pendientes">                         
                        @include('dashboard.aspirantes.tabla_pendientes')
                     </div>
                     <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />
<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
    Tipped.create('.nameimg', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
  });
  
  function tabla_lista_pendientes(){
     
     var applicant = $("#applicant").val();
     var documento = $("#documento").val();
     var f_pendiente = $("#f_pendiente").val();

        $.get('pendientes', {
            applicant: applicant,
            documento: documento,
            f_pendiente: f_pendiente,
            nombre: 1
        } ,

        function(response){
            $('#table_pendientes').html(response);
            //var datos = JSON.parse(response);
        });
    }
</script>