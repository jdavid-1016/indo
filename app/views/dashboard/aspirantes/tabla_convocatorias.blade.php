<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th>Convocatoria</th>
         <th>Periodo</th>
         <th>Fecha</th>
         <th>Acciones</th>
      </tr>
   </thead>
   <tbody>
      @foreach($convocatorias as $conv)
         <tr style="text-align:center;">
            <td>{{$conv->convocation}}</td>
            <td>{{$conv->AspirantsPeriods->period}}</td>
            <td>{{$conv->date}}</td>
            <td><a class="btn btn-default" href="excelconvocatorias?id={{$conv->id}}" target="_blank"><i class="icon-sort-by-attributes"></i> Excel</a>
            <a class="btn btn-default" href="#cargamasiva" data-toggle="modal" ><i class="icon-upload-alt"></i> Subir</a>
            <a class="btn btn-default" href="generarpdfcit?id={{$conv->id}}" target="_blank"><i class="icon-file"></i> PDF</a></td>
            </tr>
      @endforeach
   </tbody>
</table>

<script type="text/javascript">
   function descargar_excel_conv(id) {
   window.open('http://localhost/indo/public/excelconvocatorias?id='+id,'','width=600,height=400,left=50,top=50,toolbar=yes');
   }
</script>