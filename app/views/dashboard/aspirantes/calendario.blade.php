
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Directorio de Colegios</div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class=""><a href="directorio">Directorio</a></li>
                  <li class=""><a href="feriascolegios">Ferias de Colegios</a></li>
                  <!-- <li class=""><a href="directorioferias">Proximas Ferias</a></li> -->
                  <li class="active"><a href="calendario">Calendario</a></li>
                  
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  <!-- <a href="#ajax2" data-toggle="modal" class="btn btn-info  pull-right">Ingresar Contacto</a>                           -->
               </ul>
                <div class="container">
                    <!-- <ol class="breadcrumb">
                        <li><a href="calendar">Calendario</a></li>
                        <li><a href="events">Añadir evento</a></li>
                    </ol> -->
                    <div class="row">
                        <div class="page-header">
                            <div class="pull-right form-inline">
                                <div class="btn-group">
                                    <button class="btn btn-primary" data-calendar-nav="prev"><< Anterior</button>
                                    <button class="btn" data-calendar-nav="today">Hoy</button>
                                    <button class="btn btn-primary" data-calendar-nav="next">Siguiente >></button>
                                </div>
                                <div class="btn-group">
                                    <button class="btn btn-warning" data-calendar-view="year">Año</button>
                                    <button class="btn btn-warning active" data-calendar-view="month">Mes</button>
                                    <button class="btn btn-warning" data-calendar-view="week">Semana</button>
                                    <!-- <button class="btn btn-warning" data-calendar-view="day">Día</button> -->
                                </div>
                            </div>
                        </div>  
                        <label class="checkbox">
                            <!-- <input type="checkbox" value="#events-modal" id="events-in-modal" checked=""> Abrir eventos en una ventana modal -->
                        </label>    
                    </div><hr>
                    <div class="row">
                        <div id="calendar"></div>
                    </div>
                    <a class=" btn btn-default" data-target="#modal_agenda2" data-toggle="modal" id="abre_el_pto_modal" style="display:none"><i class="icon-eye-open"></i></a>
                   
                   

                   
                    



                </div>
   </div>
         </div>
      </div>
   </div>
</div>