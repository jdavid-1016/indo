<link href="{{  URL::to("assets/css/pages/about-us.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::to("assets/css/pages/portfolio.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">  

    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Intranet</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-file-text"></i>FORMATOS
                            </div>
                        </div>
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a id="documentacion" href="#tab_1_2" data-toggle="tab" >Documentación</a></li>
                                <li><a id="gestion" href="#tab_1_3" data-toggle="tab" >Gestion de archivos</a></li>
                            </ul>
                            <div class="tab-content">
                                

                                <div class="tab-pane active" id="tab_1_2">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="portlet" id="docnav">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-list-ul"></i>Navegación</div>
                                                </div>

                                                <div class="portlet-body">
                                                    <span id="listdocument" class="clicked">
                                                        <ul>
                                                            @foreach($carpetas as $car)
                                                            <li dir="{{$car->dir_folder}}" id="{{ $car->id }}" class="folderasp"><i class="icon-folder-close"></i>{{ $car->name_folder }}
                                                            </li>
                                                            <div id="ad{{ $car->dir_folder }}">
                                                            </div>
                                                            @endforeach
                                                        </ul>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-7 ">
                                            <div class="portlet">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-file"></i>Documento</div>
                                                </div>
                                                <div class="portlet-body" id="docview">
                                                    <embed id="doc-container" src="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                
                                <div class="tab-pane" id="tab_1_3">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="portlet" id="docnav">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-list-ul"></i>Crear nuevo archivo</div>
                                                </div>

                                                <div class="portlet-body">
                                                    
                                                    {{Form::open( array('url'=>'aspirantes/guardararc', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}

                                                    <div class="row">
                                                    <div class="col-md-5">
                                                                <label>Nombre</label>
                                                                    <input class="form-control" type="text" name="nombre" id="nombre">
                                                                <label>Seleccione los archivos</label>
                                                                {{Form::file('arc')}}
                                                            </div></div><br/>
                                                            <div class="row">
                                                            <div class="col-md-5">
                                                                 <label class="control-label">Carpeta</label>                                                                
                                                                    <select  class="form-control input-medium select2me" name="carpetas" id="carpetas" data-placeholder="Select...">
                                                                    @foreach($carpetas as $car)
                                                                    <option value="{{ $car->id }}">{{$car->name_folder}}</option>
                                                                    @endforeach
                                                                </select>
                                                                 </div>
                                                                </div><br />
                                                                 <div class="row">
                                                                     <div class="col-md-5">
                                                                    <span class="help-block">
                                                                    <a href="#gestiondoc" data-toggle="modal" class="btn btn-success"><i class="icon-plus"></i> Nueva carpeta</a>
                                                               </span>
                                                                     </div>
                                                                     </div>
                                                            
                                                            <?php
                                                    $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                                    ?>
                                                    <input type="hidden" name="url" id="url" value="<?php echo $url; ?>">
                                                    <br /><button type="submit" class="btn btn-info btn-block">Subir</button>
                                                {{Form::close()}}
                                                </div>
                                                    
                                                </div>

                                            </div>
                                        <div class="col-md-7">
                                            <div class="portlet">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-file"></i>Documento</div>
                                                </div>
                                                <div class="portlet-body" id="docview">
                                                    <embed id="doc-container" src="{{Input::get('embed')}}">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        
                                    </div> 

                                </div> 
                                <!--/thumbnails-->
                                <!-- //End Meer Our Team -->        
                                <!-- END PAGE CONTENT-->
                            </div>
                            <!-- END PAGE -->    
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="gestiondoc" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Nueva carpeta</h4>
                    </div>
                    <div class="modal-body">
                        <label class="control-label col-md-3">Nombre:</label>
                        <input class="form-control input-medium" type="text" name="carpeta" id="carpeta">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="crearDirasp()">Crear</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
    </div>
    <!-- END CONTAINER -->