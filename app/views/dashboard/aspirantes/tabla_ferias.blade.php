<table class="table top-blue" data-target="soporte/callSupport/">
      <thead>
         <tr>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>Direccion</th>
            <th>Teléfono</th>
            <th>Email</th>
            <th>Feria</th>
            <th>Observación</th>
            <th></th>
            
         </tr>
      </thead>
      <tbody>
         @foreach($datos_agendas as $datos)
            <tr style="text-align:center;" class="">
               
               <td>{{$datos['name']}}</td>
               <td>{{$datos['name_contact']}}</td>
               <td>{{$datos['address']}}</td>
               <td>{{$datos['phone']}}</td>
               <td>{{$datos['email']}}</td>
               <td>{{$datos['date_fair']}}</td>
               <td>{{$datos['observation']}}</td>
               <td><a class=" btn btn-default" data-target="#ajax1" id="{{$datos['hid']}}" data-toggle="modal" onclick='cargar_seguimientos_feria($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a>
               <a class=" btn btn-default" data-target="#ajax3" id="{{$datos['hid']}}" data-toggle="modal" onclick='cargar_detalle_feria($(this).attr("id"));return false;'><i class="icon-edit"></i></a>
               <a href="galeriasferias?id={{$datos['hid']}}" target="_blank" class=" btn btn-default" id="{{$datos['hid']}}"><i class="icon-camera"></i></a></td>
                                             
         @endforeach
      </tbody>
   </table>