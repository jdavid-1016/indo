<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Informe</title>
<link href="{{  URL::to("css/pdf.css")}}" rel="stylesheet" type="text/css"/>
</head>
 
<body>
    <table align="center" class="table " width="100%">
    <tr class="">
    
  </tr> 
  <tr align="center">
    <td>Convocatoria: {{ $citaciones[0]->convocation }}</td>
    </tr>
    <tr align="center">
    <td>Periodo Academico: {{ $inscrito[0]->AspirantsPeriods->period }}</td>
    </tr>
    <tr align="center">
    <td>Apreciado: {{ $inscrito[0]->first_name }} {{ $inscrito[0]->middle_name }} {{ $inscrito[0]->last_name }} {{ $inscrito[0]->last_name2 }}</td>
    </tr>
    <tr align="center">
    <td>Bienvenido al proceso de admision para el ingreso a la </td>
    </tr>
    <tr align="center">
    <td>Corporacion Educativa Indoamericana</td>
    </tr>
    <tr align="center">
    <td>Programa: {{ $inscrito[0]->AspirantsPrograms->name }}</td>
    </tr>
    <tr align="center">
    <td>Jornada: {{ $inscrito[0]->AspirantsTimes->day_trip }}</td>
    </tr>
    <tr align="center">
    <td>Fecha Citacion: {{ $citaciones[0]->date }}</td>
    </tr>
    <tr align="center">
    <td>Examen de conocimientos: {{ $citaciones[0]->time }} {{ $citaciones[0]->classroom }}</td>
    </tr>
    <tr align="center">
    <td>Examen Psicologico: {{ $citaciones[1]->time }} {{ $citaciones[1]->classroom }}</td>
  </tr>
</table>
 
</body>
 
</html>
        