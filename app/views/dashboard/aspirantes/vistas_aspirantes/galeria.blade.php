<link href="{{  URL::to("assets/css/pages/about-us.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::to("assets/css/pages/portfolio.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->


    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Galeria 
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Aspirantes</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> Galerias</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-file-text"></i>Galeria Feria Colegio Indoamericana
                            </div>
                        </div>
                        <?php
                        if(Auth::user()->id==194){
                        $permisos = 1;
                        }
                        ?>
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">Imagenes</a></li>
                                <li><a href="#tab_1_2" data-toggle="tab">Videos</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">

                            <div class="tab-pane active" id="tab_1_1">
                                                <!-- BEGIN FILTER -->                                                              
                                <div class="tab-content">
                                                <!-- BEGIN FILTER -->           
                                                <div class="margin-top-10">
                                                   <ul class="mix-filter">
                                                      <li class="filter btn active" data-filter="all">Todas</li>                              
                                                      
                                                   </ul>
                                                   <div class="row mix-grid">
                                                      <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                         <div class="mix-inner">
                                                            <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/img1.jpg" alt="">
                                                            <div class="mix-details">
                                                               <h4>Galeria feria</h4>
                                                               <a class="mix-link" id="img-1"><i class="icon-thumbs-up-alt"> </i></a>
                                                               <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/img1.jpg" title="1" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                        <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                            <div class="mix-inner">
                                                                <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/img2.jpg" alt="">
                                                                <div class="mix-details">
                                                                    <h4>Galeria feria</h4>
                                                                    <a class="mix-link" id="img-2"><i class="icon-thumbs-up-alt"> </i></a>
                                                                    <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/img2.jpg" title="2" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                                                     <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                         <div class="mix-inner">
                                                            <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/img3.jpg" alt="">
                                                            <div class="mix-details">
                                                               <h4>Galeria feria</h4>
                                                               <a class="mix-link" id="img-3"><i class="icon-thumbs-up-alt"> </i></a>
                                                               <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/img3.jpg" title="3" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                                                     <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                         <div class="mix-inner">
                                                            <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/Penguins.jpg" alt="">
                                                            <div class="mix-details">
                                                               <h4>Galeria feria</h4>
                                                               <a class="mix-link" id="img-22"><i class="icon-thumbs-up-alt"> </i></a>
                                                               <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/Penguins.jpg" title="" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                                                     <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                         <div class="mix-inner">
                                                            <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/Tulips.jpg" alt="">
                                                            <div class="mix-details">
                                                               <h4>Galeria feria</h4>
                                                               <a class="mix-link" id="img-23"><i class="icon-thumbs-up-alt"> </i></a>
                                                               <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/Tulips.jpg" title="" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                                                     <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                         <div class="mix-inner">
                                                            <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/Koala.jpg" alt="">
                                                            <div class="mix-details">
                                                               <h4>Galeria feria</h4>
                                                               <a class="mix-link" id="img-29"><i class="icon-thumbs-up-alt"> </i></a>
                                                               <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/Koala.jpg" title="" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                         <div class="mix-inner">
                                                            <img class="img-responsive" style="width: 384px; height: 282px;" src="http://192.168.0.18/indo/public/assets/img/ferias/Koala.jpg" alt="">
                                                            <div class="mix-details">
                                                               <h4>Galeria feria</h4>
                                                               <a class="mix-link" id="img-30"><i class="icon-thumbs-up-alt"> </i></a>
                                                               <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/Koala.jpg" title="" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                                            </div>
                                                         </div>
                                                      </div>
                                                                                  </div>
                                                </div>
                                                <!-- END FILTER -->
                                </div>                           
                                                <!-- END FILTER -->    
                            </div>

                            <div class="tab-pane" id="tab_1_2">
                                
                                <div class="tab-content">
                                
                                    <div class="margin-top-10">
                                        <div class="row mix-grid">

                                        @foreach($searchResponse['items'] as $searchResult)
                                            @if($searchResult['id']['kind'] == "youtube#video")
                                                <div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;">
                                                   <div class="mix-inner">
                                                      <img class="img-responsive" style="width: 384px; height: 282px;" src="{{$searchResult['snippet']['thumbnails']['high']['url']}}" alt="">
                                                      <div class="mix-details">
                                                         <h4>{{$searchResult['snippet']['title']}}</h4>
                                                         <a class="mix-link" id="img-30"><i class="icon-thumbs-up-alt"> </i></a>
                                                         <!-- <a class="mix-preview fancybox-button" href="http://192.168.0.18/indo/public/assets/img/ferias/Koala.jpg" title="" data-rel="fancybox-button"><i class="icon-play"></i></a> -->
                                                         <a class="mix-preview  btn btn-default" style="" data-target="#ajax" data-rel="fancybox-button" titulo="{{$searchResult['snippet']['title']}}" id="{{$searchResult['id']['videoId']}}" data-toggle="modal" onclick='cargar_video_modal($(this).attr("id"));return false;'><i class="icon-play"></i></a>
                                                      </div>
                                                   </div>
                                                </div>
                                            @endif
                                        @endforeach
                                        </div>
                                    </div>
                                </div>                           
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
    <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-wide" ng-controller="help_deskCtrl" ng-app>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title" id="titulo_video"></h4>
                </div>
                <div class="modal-body form-body" id="video_puesto">

                </div>
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript">
        function cargar_video_modal(id){
            $("#video_puesto").html('<iframe width="100%" height="80%" src="https://www.youtube.com/embed/'+id+'" frameborder="0" fullscreen></iframe>') 
            $("#video_puesto").html(titulo);
        }
    </script>