<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Indoamericana</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="MobileOptimized" content="320">
        <link href="{{URL::to("logo.ico")}}" rel="shortcut icon" >
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
        <link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/select2/select2_conquer.css")}}" />
        <link rel="stylesheet" href="{{  URL::to("assets/plugins/data-tables/DT_bootstrap.css")}}" />

        <link rel="stylesheet" href="../js/tragator/fm.tagator.jquery.css"/>


        <!-- END THEME STYLES -->   
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->   
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->  
                <a class="navbar-brand" href="">
                    <img src="{{URL::to("assets/img/logo.png")}}" width="55px" alt="logo" style="margin-top: -30px" class="img-responsive" />
                </a>
                <!--<form class="search-form search-form-header" role="form" action="index.html" >
                   <div class="input-icon right">
                      <i class="icon-search"></i>
                      <input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
                   </div>
                </form>-->                
                <h2 style="color: #ffffff;">Formulario De Registro Aspirantes</h2>                
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
                 
                <!-- END RESPONSIVE MENU TOGGLER -->

                <!-- BEGIN TOP NAVIGATION MENU -->        

                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>

        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <!----------------------------------------------------------------------------------HEAD---------------------------------------------------------------------------------------->
        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <hr><hr>
        <div class="">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <div class="tabbable tabbable-custom">
                            <div class="tab-content">

                                <div class="row">
                                    <div class="col-md-12">

                                        <form class="form-horizontal" id="form">
                                            <div class="form-body">
                                                <div class="row">                            
                                                    <div class="col-md-12 note note-warning">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group">
                                                                    <div class="col-md-12">
                                                                        <label class="control-label">Imagen*</label>
                                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                                            </div>
                                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                            <div>
                                                                                <span class="btn btn-default btn-file">
                                                                                    <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                                    <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                                    <input type="file" class="default form-control" name="imagen_alumno" id="imagen_alumno" size="20">
                                                                                </span>
                                                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                                                                                <div class="help-block">
                                                                                    Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>  
                                                            </div>
                                                            <div class="col-md-7">
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Programa Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="programs" id="id_programa" class="form-control">
                                                                            <option value=""></option>
                                                                            @foreach($programas as $pro)
                                                                            <option <?php if ($aspirante[0]->aspirants_programs_id == $pro->id) {
                                                                                echo "selected";
                                                                            } ?> value="{{ $pro->id }}">{{ $pro->name }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Periodo Académico</label>
                                                                    <div class="col-md-4">                                                                 
                                                                        <select name="period" id="periodo" class="form-control">
                                                                            @foreach($periodo as $periodo)
                                                                                <option value="{{$periodo->id}}">{{$periodo->period}}</option>
                                                                            @endforeach
                                                                            <!-- <option value="Primer Semestre 2016">Primer Semestre 2016</option> -->
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Tipo de Aspirante</label>
                                                                    <div class="col-md-4">
                                                                        <select name="type_aspirant" id="type_aspirant" class="form-control">
                                                                            <option value="Nuevo">Nuevo</option>
                                                                            <!--<option value="Homologante Externo">Homologante Externo</option>
                                                                            <option value="Reintegro">Reintegro</option> -->
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                
                                                                <div class="form-group">
                                                                    <label class="col-md-4 control-label">Jornada</label>
                                                                    <div class="col-md-4">
                                                                        <select name="jornada" id="jornada" class="form-control">
                                                                            @foreach($jornadas as $jornada)
                                                                                <option value="{{$jornada->id}}">{{$jornada->day_trip}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>


                                                    </div>

                                                </div>

                                                <div class="row">
                                                <input id="id_prospecto" name="id_prospecto" type="hidden" class="form-control campos_cpu" value="{{$id_prospecto}}" >
                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Personales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="name" name="name" type="text" class="form-control campos_cpu" value="{{$name[0]}}" placeholder="Primer Nombre*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="middle_name" name="middle_name" type="text" class="form-control campos_cpu" value="<?php if (isset($name[1])) { ?>{{$name[1]}} <?php } ?>" placeholder="Segundo Nombre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name" name="last_name" type="text" class="form-control campos_cpu" value="<?php if (isset($last_name[0])) { ?>{{$last_name[0]}} <?php } ?>" placeholder="Primer Apellido*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="last_name2" name="last_name2" type="text" class="form-control campos_cpu" value="<?php if (isset($last_name[1])) { ?>{{$last_name[1]}} <?php } ?>" placeholder="Segundo Apellido">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="type_document" id="type_document" class="form-control">
                                                                            <option value="">Tipo de documento*</option>
                                                                            @foreach($typedoc as $type)
                                                                            <option value="{{ $type->id }}">{{ $type->type }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="document" name="document" type="text" class="form-control campos_cpu" value="{{$aspirante[0]->document}}" placeholder="N° Doc Identidad*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="nivel_formacion" id="nivel_formacion" class="form-control">
                                                                            <option value="">Nivel de formacion*</option>
                                                                            @foreach($nivelf as $nivel)
                                                                            <option value="{{ $nivel->id }}">{{ $nivel->training }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="libreta_militar" id="libreta_militar" class="form-control">
                                                                            <option value="">Libreta Militar*</option>
                                                                            <option value="1">Si</option>
                                                                            <option value="2">No</option>
                                                                        </select>
                                                                        
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="fecha_nacimiento" name="fecha_nacimiento" type="text" class="form-control campo_fecha" placeholder="Fecha Nacimiento*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="estado_civil" id="estado_civil" class="form-control">
                                                                            <option value="">Estado Civil*</option>
                                                                            @foreach($estadocivil as $estcivil)
                                                                            <option value="{{ $estcivil->id }}">{{ $estcivil->marital_status }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="genero" id="genero" class="form-control">
                                                                            <option value="">Genero*</option>
                                                                            @foreach($genero as $gen)
                                                                            <option value="{{ $gen->id }}">{{ $gen->aspirants_gender }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="grupo_sanguineo" id="grupo_sanguineo" class="form-control">
                                                                            <option value="">Grupo Sanguineo*</option>
                                                                            @foreach($tipo_sangre as $tipo_sangre)
                                                                            <option value="{{ $tipo_sangre->id }}">{{ $tipo_sangre->group }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="nacionalidad" id="nacionalidad" class="form-control">
                                                                            <option value="">Pais*</option>
                                                                            @foreach($countries as $countries)
                                                                            <option value="{{$countries->id }}">{{$countries->country}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="direccion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección de Residencia*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="localidad" id="localidad" class="form-control">
                                                                            <option value="">Localidad*</option>
                                                                            @foreach($localidades as $loc)
                                                                            <option value="{{ $loc->id }}">{{ $loc->location }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="barrio" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Barrio*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        
                                                                        <select name="ciudad" id="ciudad" class="form-control">
                                                                            <option value="">Ciudad Residencia*</option>
                                                                            @foreach($lugarnac as $lugar)
                                                                            <option value="{{ $lugar->id }}">{{ $lugar->birth_place }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono*">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="celular" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Celular*" value="{{$aspirante[0]->phone2}}">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="email" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Email*" value="{{$aspirante[0]->email}}">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <select name="lugar_nacimiento" id="lugar_nacimiento" class="form-control">
                                                                            <option value="">Lugar de Nacimiento*</option>
                                                                            @foreach($lugarnac as $lugar)
                                                                            <option value="{{ $lugar->id }}">{{ $lugar->birth_place }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="observaciones" class="form-control campos_cpu" rows="3" placeholder="Observaciones"></textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>                                    



                                                    </div>
                                                    <div class="col-md-6 note note-info">
                                                        <h4 class="block">Datos Laborales</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="empresa" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Empresa Donde Trabaja">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="empresa_cargo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Cargo Actual">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="empresa_telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="empresa_direccion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4 class="block">Datos Familiares</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="padre_nombre" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nombre Padre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="padre_ocupacion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="padre_direccion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="padre_telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="madre_nombre" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nombre Madre">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="madre_ocupacion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="madre_direccion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="madre_telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h4> &nbsp</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_nombre" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Nombre Acudiente">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_ocupacion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Ocupación">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_direccion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfono">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_celular" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Celular">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_email" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Email">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="acudiente_barrio" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Barrio">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <h3> &nbsp &nbsp</h3>





                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Estudios Realizados</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="estudios_institucion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="*Bachillerato: Nombre de la Institucion*:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="estudios_universidad" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="estudios_carrera" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Carrera Universidad:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="estudios_otros" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Otros:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="motivo_estudio" name="cpu_serial" class="form-control campos_cpu" placeholder="Que lo motivo a estudiar en la Corporacion Educativa Indoamericana*:"></textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6" style="visibility: hidden;">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <textarea id="" name="" class="form-control campos_cpu" placeholder=""></textarea>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>                                                        
                                                    
                                                    </div>


                                                    <div class="col-md-6 note note-warning">
                                                        <h4 class="block">Fuentes De Infomación</h4>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        
                                                                        <select name="fuentes_informacion" id="fuentes_informacion" class="form-control">
                                                                            <option value="">Fuentes de Información*</option>
                                                                            @foreach($fuentes_inf as $fuentes_inf)
                                                                            <option value="{{ $fuentes_inf->id }}">{{ $fuentes_inf->source }}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group" >
                                                                <div class="col-md-12">

                                                                    <div class="input-group">
                                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                        <input id="fuentes_especifique" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Especifique:">
                                                                    </div> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>                                                    
                                                </div>
                                                <div class="row">
                                                    
                                                    </div>
                                                <div class="form-actions fluid">
                                                    <div class="col-md-1 col-md-11">
                                                        <a class="btn btn-success" onclick="enviar_datos_nuevo_inscripcion()">Guardar</a>
                                                    </div>
                                                </div>                        
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- BEGIN FOOTER -->

        <div class="footer">
            <div class="footer-inner">
                2015 &copy; SGI Indoamericana.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="icon-angle-up"></i>
                </span>
            </div>
        </div>
        <div id="sound">
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <![endif]-->

        <script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>

        <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
        <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
        <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
        // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
        <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
        <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->   
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
        <!-- END PAGE LEVEL SCRIPTS -->     
        <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
        <script src="{{ URL::to("js/app.js")}}"></script>
        <script src="{{ URL::to("js/upload.js")}}"></script>
        <script src="{{ URL::to("js/validar.js")}}"></script>                
        <script src="{{ URL::to("js/aspirantes.js")}}"></script>
        <script src="{{ URL::to("js/angular.min.js")}}"></script>        
        <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
        <script src="{{ URL::to("js/jQuery_Mask/src/jquery.mask.js")}}"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

        <script src="../js/tragator/fm.tagator.jquery.js"></script>

        <script>
                                                            jQuery(document).ready(function () {
                                                                App.init(); // initlayout and core plugins
                                                                Portfolio.init();
                                                                TableEditable.init();
                                                                Index.init();
                                                                Index.initJQVMAP(); // init index page's custom scripts
                                                                Index.initCalendar(); // init index page's custom scripts
                                                                Index.initCharts(); // init index page's custom scripts
                                                                Index.initChat();
                                                                Index.initMiniCharts();
                                                                Index.initPeityElements();
                                                                Index.initKnowElements();
                                                                Index.initDashboardDaterange();
                                                                Tasks.initDashboardWidget();
                                                                UITree.init();
                                                                UIToastr.init();
                                                                FormComponents.init();
                                                            });
        </script>   
        <script>
    $( ".campo_fecha" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '-80y' });
 </script>   
        <script type="text/javascript">
        
        function enviar_datos_nuevo_inscripcion(){
            validacion = validar_campos_form_inscripcion();
            if (validacion == 1) {

                var imagen_alumno   = $("#imagen_alumno").val();
                var id_programa = $("#id_programa").val();
                var periodo = $("#periodo").val();
                var type_aspirant   = $("#type_aspirant").val();
                var jornada = $("#jornada").val();
                var name    = $("#name").val();
                var middle_name = $("#middle_name").val();
                var last_name   = $("#last_name").val();
                var last_name2  = $("#last_name2").val();
                var type_document   = $("#type_document").val();
                var document    = $("#document").val();
                var nivel_formacion = $("#nivel_formacion").val();
                var libreta_militar = $("#libreta_militar").val();
                var fecha_nacimiento    = $("#fecha_nacimiento").val();
                var estado_civil    = $("#estado_civil").val();
                var genero  = $("#genero").val();
                var grupo_sanguineo = $("#grupo_sanguineo").val();
                var nacionalidad    = $("#nacionalidad").val();
                var direccion   = $("#direccion").val();
                var localidad   = $("#localidad").val();
                var barrio  = $("#barrio").val();
                var ciudad  = $("#ciudad").val();
                var telefono    = $("#telefono").val();
                var celular = $("#celular").val();
                var email   = $("#email").val();
                var lugar_nacimiento    = $("#lugar_nacimiento").val();
                var observaciones   = $("#observaciones").val();


                var empresa = $("#empresa").val();
                var empresa_cargo   = $("#empresa_cargo").val();
                var empresa_telefono    = $("#empresa_telefono").val();
                var empresa_direccion   = $("#empresa_direccion").val();

                var padre_nombre    = $("#padre_nombre").val();
                var padre_ocupacion = $("#padre_ocupacion").val();
                var padre_direccion = $("#padre_direccion").val();
                var padre_telefono  = $("#padre_telefono").val();
                var madre_nombre    = $("#madre_nombre").val();
                var madre_ocupacion = $("#madre_ocupacion").val();
                var madre_direccion = $("#madre_direccion").val();
                var madre_telefono  = $("#madre_telefono").val();
                var acudiente_nombre    = $("#acudiente_nombre").val();
                var acudiente_ocupacion = $("#acudiente_ocupacion").val();
                var acudiente_direccion = $("#acudiente_direccion").val();
                var acudiente_telefono  = $("#acudiente_telefono").val();
                var acudiente_celular   = $("#acudiente_celular").val();
                var acudiente_email = $("#acudiente_email").val();
                var acudiente_barrio    = $("#acudiente_barrio").val();

                var estudios_institucion    = $("#estudios_institucion").val();
                var estudios_universidad    = $("#estudios_universidad").val();
                var estudios_carrera    = $("#estudios_carrera").val();
                var estudios_otros  = $("#estudios_otros").val();
                var motivo_estudio  = $("#motivo_estudio").val();
                var fuentes_informacion = $("#fuentes_informacion").val();
                var fuentes_especifique = $("#fuentes_especifique").val();
                var id_prospecto = $("#id_prospecto").val();
                
//                var estrato = $("#estrato").val();
//                var eps = $("#eps").val();
//                var sisben = $("#sisben").val();
//                var desplazado = $("#desplazado").val();
//                var indigena = $("#indigena").val();
//                var reinsertado = $("#reinsertado").val();
//                var discapacidad = $("#discapacidad").val();
//                var cual_discapacidad = $("#cual_discapacidad").val();
//                var cabeza_familia = $("#cabeza_familia").val();
//                var frontera = $("#frontera").val();
//                var afro = $("#afro").val();
                
//                var html = $.ajax({
//                     type: "GET",
//                     url: "guardarnuevoaspirante",
//                     cache: false,
//                     data: {
//                         
//                     imagen_alumno : imagen_alumno,
//                     id_programa : id_programa,
//                     periodo : periodo,
//                     type_aspirant : type_aspirant,
//                     jornada : jornada,
//                     name : name,
//                     middle_name : middle_name,
//                     last_name : last_name,
//                     last_name2 : last_name2,
//                     type_document : type_document,
//                     document : document,
//                     nivel_formacion : nivel_formacion,
//                     libreta_militar : libreta_militar,
//                     fecha_nacimiento : fecha_nacimiento,
//                     estado_civil : estado_civil,
//                     genero : genero,
//                     grupo_sanguineo : grupo_sanguineo,
//                     nacionalidad : nacionalidad,
//                     direccion : direccion,
//                     localidad : localidad,
//                     barrio : barrio,
//                     ciudad : ciudad,
//                     telefono : telefono,
//                     celular : celular,
//                     email : email,
//                     lugar_nacimiento : lugar_nacimiento,
//                     observaciones : observaciones,
//                     empresa : empresa,
//                     empresa_cargo : empresa_cargo,
//                     empresa_telefono : empresa_telefono,
//                     empresa_direccion : empresa_direccion,
//                     padre_nombre : padre_nombre,
//                     padre_ocupacion : padre_ocupacion,
//                     padre_direccion : padre_direccion,
//                     padre_telefono : padre_telefono,
//                     madre_nombre : madre_nombre,
//                     madre_ocupacion : madre_ocupacion,
//                     madre_direccion : madre_direccion,
//                     madre_telefono : madre_telefono,
//                     acudiente_nombre : acudiente_nombre,
//                     acudiente_ocupacion : acudiente_ocupacion,
//                     acudiente_direccion : acudiente_direccion,
//                     acudiente_telefono : acudiente_telefono,
//                     acudiente_celular : acudiente_celular,
//                     acudiente_email : acudiente_email,
//                     acudiente_barrio : acudiente_barrio,
//                     estudios_institucion : estudios_institucion,
//                     estudios_universidad : estudios_universidad,
//                     estudios_carrera : estudios_carrera,
//                     estudios_otros : estudios_otros,
//                     motivo_estudio : motivo_estudio,
//                     fuentes_informacion : fuentes_informacion,
//                     fuentes_especifique : fuentes_especifique,
//                     id_prospecto :id_prospecto
//                         
//                         
//                     },
//                     async: false
//                    }).responseText;
//                    
//                    if (html === 1) {
//                        
//                        toastr.success('Se ingresaron correctamente los datos', 'Registro de Datos');                                                
//                        
//                    } else {
//                        
//                        toastr.error('No ha sido posible ingresar los datos.', 'Error');                              
//                        
//                    }

                $("#imagen_alumno").upload('../guardarnuevoaspirante', 
                {
                     
                     imagen_alumno : imagen_alumno,
                     id_programa : id_programa,
                     periodo : periodo,
                     type_aspirant : type_aspirant,
                     jornada : jornada,
                     name : name,
                     middle_name : middle_name,
                     last_name : last_name,
                     last_name2 : last_name2,
                     type_document : type_document,
                     document : document,
                     nivel_formacion : nivel_formacion,
                     libreta_militar : libreta_militar,
                     fecha_nacimiento : fecha_nacimiento,
                     estado_civil : estado_civil,
                     genero : genero,
                     grupo_sanguineo : grupo_sanguineo,
                     nacionalidad : nacionalidad,
                     direccion : direccion,
                     localidad : localidad,
                     barrio : barrio,
                     ciudad : ciudad,
                     telefono : telefono,
                     celular : celular,
                     email : email,
                     lugar_nacimiento : lugar_nacimiento,
                     observaciones : observaciones,
                     empresa : empresa,
                     empresa_cargo : empresa_cargo,
                     empresa_telefono : empresa_telefono,
                     empresa_direccion : empresa_direccion,
                     padre_nombre : padre_nombre,
                     padre_ocupacion : padre_ocupacion,
                     padre_direccion : padre_direccion,
                     padre_telefono : padre_telefono,
                     madre_nombre : madre_nombre,
                     madre_ocupacion : madre_ocupacion,
                     madre_direccion : madre_direccion,
                     madre_telefono : madre_telefono,
                     acudiente_nombre : acudiente_nombre,
                     acudiente_ocupacion : acudiente_ocupacion,
                     acudiente_direccion : acudiente_direccion,
                     acudiente_telefono : acudiente_telefono,
                     acudiente_celular : acudiente_celular,
                     acudiente_email : acudiente_email,
                     acudiente_barrio : acudiente_barrio,
                     estudios_institucion : estudios_institucion,
                     estudios_universidad : estudios_universidad,
                     estudios_carrera : estudios_carrera,
                     estudios_otros : estudios_otros,
                     motivo_estudio : motivo_estudio,
                     fuentes_informacion : fuentes_informacion,
                     fuentes_especifique : fuentes_especifique,
                     id_prospecto :id_prospecto
                    
                },

                function(respuesta){
                    
                    if (respuesta === 1) {
                        
                        toastr.success('Se ingresaron correctamente los datos', 'Registro de Datos');
                        vaciar_campos_form_inscripcion();
                        
                    } else {
                        
                        //alert(respuesta);
                        toastr.error('No ha sido posible ingresar los datos.', 'Error');
                        
                    }
                });

            }else{
                //toastr.error('No ha sido posible ingresar los datos.', 'Error');
            }   
        }

        function validar_campos_form_inscripcion(){
            var imagen_alumno   = $("#imagen_alumno").val();
            var id_programa = $("#id_programa").val();
            var periodo = $("#periodo").val();
            var type_aspirant   = $("#type_aspirant").val();
            var jornada = $("#jornada").val();
            var name    = $("#name").val();
            var middle_name = $("#middle_name").val();
            var last_name   = $("#last_name").val();
            var last_name2  = $("#last_name2").val();
            var type_document   = $("#type_document").val();
            var document    = $("#document").val();
            var nivel_formacion = $("#nivel_formacion").val();
            var libreta_militar = $("#libreta_militar").val();
            var fecha_nacimiento    = $("#fecha_nacimiento").val();
            var estado_civil    = $("#estado_civil").val();
            var genero  = $("#genero").val();
            var grupo_sanguineo = $("#grupo_sanguineo").val();
            var nacionalidad    = $("#nacionalidad").val();
            var direccion   = $("#direccion").val();
            var localidad   = $("#localidad").val();
            var barrio  = $("#barrio").val();
            var ciudad  = $("#ciudad").val();
            var telefono    = $("#telefono").val();
            var celular = $("#celular").val();
            var email   = $("#email").val();
            var lugar_nacimiento    = $("#lugar_nacimiento").val();
            var observaciones   = $("#observaciones").val();


            var empresa = $("#empresa").val();
            var empresa_cargo   = $("#empresa_cargo").val();
            var empresa_telefono    = $("#empresa_telefono").val();
            var empresa_direccion   = $("#empresa_direccion").val();

            var padre_nombre    = $("#padre_nombre").val();
            var padre_ocupacion = $("#padre_ocupacion").val();
            var padre_direccion = $("#padre_direccion").val();
            var padre_telefono  = $("#padre_telefono").val();
            var madre_nombre    = $("#madre_nombre").val();
            var madre_ocupacion = $("#madre_ocupacion").val();
            var madre_direccion = $("#madre_direccion").val();
            var madre_telefono  = $("#madre_telefono").val();
            var acudiente_nombre    = $("#acudiente_nombre").val();
            var acudiente_ocupacion = $("#acudiente_ocupacion").val();
            var acudiente_direccion = $("#acudiente_direccion").val();
            var acudiente_telefono  = $("#acudiente_telefono").val();
            var acudiente_celular   = $("#acudiente_celular").val();
            var acudiente_email = $("#acudiente_email").val();
            var acudiente_barrio    = $("#acudiente_barrio").val();

            var estudios_institucion    = $("#estudios_institucion").val();
            var estudios_universidad    = $("#estudios_universidad").val();
            var estudios_carrera    = $("#estudios_carrera").val();
            var estudios_otros  = $("#estudios_otros").val();
            var motivo_estudio  = $("#motivo_estudio").val();
            var fuentes_informacion = $("#fuentes_informacion").val();
            var fuentes_especifique = $("#fuentes_especifique").val();                       


            if(imagen_alumno == ""){
                toastr.error('El campo imagen_alumno No puede estar vacio', 'Error');
                $("#imagen_alumno").focus();
                return false;
            }
            if(id_programa == ""){
                toastr.error('El campo id_programa No puede estar vacio', 'Error');
                $("#id_programa").focus();
                return false;
            }
            if(periodo == ""){
                toastr.error('El campo periodo No puede estar vacio', 'Error');
                $("#periodo").focus();
                return false;
            }
            if(type_aspirant == ""){
                toastr.error('El campo type_aspirant No puede estar vacio', 'Error');
                $("#type_aspirant").focus();
                return false;
            }
            if(jornada == ""){
                toastr.error('El campo jornada No puede estar vacio', 'Error');
                $("#jornada").focus();
                return false;
            }
            if(name == ""){
                toastr.error('El campo name No puede estar vacio', 'Error');
                $("#name").focus();
                return false;
            }            
            if(last_name == ""){
                toastr.error('El campo last_name No puede estar vacio', 'Error');
                $("#last_name").focus();
                return false;
            }            
            if(type_document == ""){
                toastr.error('El campo type_document No puede estar vacio', 'Error');
                $("#type_document").focus();
                return false;
            }
            if(document == ""){
                toastr.error('El campo document No puede estar vacio', 'Error');
                $("#document").focus();
                return false;
            }
            if(nivel_formacion == ""){
                toastr.error('El campo nivel_formacion No puede estar vacio', 'Error');
                $("#nivel_formacion").focus();
                return false;
            }
            if(libreta_militar == ""){
                toastr.error('El campo libreta_militar No puede estar vacio', 'Error');
                $("#libreta_militar").focus();
                return false;
            }
            if(fecha_nacimiento == ""){
                toastr.error('El campo fecha_nacimiento No puede estar vacio', 'Error');
                $("#fecha_nacimiento").focus();
                return false;
            }
            if(estado_civil == ""){
                toastr.error('El campo estado_civil No puede estar vacio', 'Error');
                $("#estado_civil").focus();
                return false;
            }
            if(genero == ""){
                toastr.error('El campo genero No puede estar vacio', 'Error');
                $("#genero").focus();
                return false;
            }
            if(grupo_sanguineo == ""){
                toastr.error('El campo grupo_sanguineo No puede estar vacio', 'Error');
                $("#grupo_sanguineo").focus();
                return false;
            }
            if(nacionalidad == ""){
                toastr.error('El campo nacionalidad No puede estar vacio', 'Error');
                $("#nacionalidad").focus();
                return false;
            }
            if(direccion == ""){
                toastr.error('El campo direccion No puede estar vacio', 'Error');
                $("#direccion").focus();
                return false;
            }
            if(localidad == ""){
                toastr.error('El campo localidad No puede estar vacio', 'Error');
                $("#localidad").focus();
                return false;
            }
            if(barrio == ""){
                toastr.error('El campo barrio No puede estar vacio', 'Error');
                $("#barrio").focus();
                return false;
            }
            if(ciudad == ""){
                toastr.error('El campo ciudad No puede estar vacio', 'Error');
                $("#ciudad").focus();
                return false;
            }
            if(telefono == ""){
                toastr.error('El campo telefono No puede estar vacio', 'Error');
                $("#telefono").focus();
                return false;
            }
            if(celular == ""){
                toastr.error('El campo celular No puede estar vacio', 'Error');
                $("#celular").focus();
                return false;
            }
            if(email == ""){
                toastr.error('El campo email No puede estar vacio', 'Error');
                $("#email").focus();
                return false;
            }
            if(lugar_nacimiento == ""){
                toastr.error('El campo lugar_nacimiento No puede estar vacio', 'Error');
                $("#lugar_nacimiento").focus();
                return false;
            }
            if(estudios_institucion == ""){
                toastr.error('El campo estudios_institucion No puede estar vacio', 'Error');
                $("#estudios_institucion").focus();
                return false;
            }
            if(motivo_estudio == ""){
                toastr.error('El campo motivo_estudio No puede estar vacio', 'Error');
                $("#motivo_estudio").focus();
                return false;
            }
            if(fuentes_informacion == ""){
                toastr.error('El campo fuentes_informacion No puede estar vacio', 'Error');
                $("#fuentes_informacion").focus();
                return false;
            }

            return 1;
        }
        function vaciar_campos_form_inscripcion(){
            $("#imagen_alumno").val("");
            $("#id_programa").val("");
            $("#periodo").val("");
            $("#type_aspirant").val("");
            $("#jornada").val("");
            $("#name").val("");
            $("#middle_name").val("");
            $("#last_name").val("");
            $("#last_name2").val("");
            $("#type_document").val("");
            $("#document").val("");
            $("#nivel_formacion").val("");
            $("#libreta_militar").val("");
            $("#fecha_nacimiento").val("");
            $("#estado_civil").val("");
            $("#genero").val("");
            $("#grupo_sanguineo").val("");
            $("#nacionalidad").val("");
            $("#direccion").val("");
            $("#localidad").val("");
            $("#barrio").val("");
            $("#ciudad").val("");
            $("#telefono").val("");
            $("#celular").val("");
            $("#email").val("");
            $("#lugar_nacimiento").val("");
            $("#observaciones").val("");
            $("#empresa").val("");
            $("#empresa_cargo").val("");
            $("#empresa_telefono").val("");
            $("#empresa_direccion").val("");
            $("#padre_nombre").val("");
            $("#padre_ocupacion").val("");
            $("#padre_direccion").val("");
            $("#padre_telefono").val("");
            $("#madre_nombre").val("");
            $("#madre_ocupacion").val("");
            $("#madre_direccion").val("");
            $("#madre_telefono").val("");
            $("#acudiente_nombre").val("");
            $("#acudiente_ocupacion").val("");
            $("#acudiente_direccion").val("");
            $("#acudiente_telefono").val("");
            $("#acudiente_celular").val("");
            $("#acudiente_email").val("");
            $("#acudiente_barrio").val("");
            $("#estudios_institucion").val("");
            $("#estudios_universidad").val("");
            $("#estudios_carrera").val("");
            $("#estudios_otros").val("");
            $("#motivo_estudio").val("");
            $("#fuentes_informacion").val("");
            $("#fuentes_especifique").val("");
        }

        </script>