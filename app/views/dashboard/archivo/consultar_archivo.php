<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Consulta de Archivos
            </h3>            
        </div>
    </div>
    
        
    <div class="row">
        <div class="col-md-6">
            <div class="col-md-12">
            <ul class="page-breadcrumb breadcrumb">
                <h4><b>Formulario De consulta</b></h4>
            </ul>
        </div>
        <form class="form-horizontal" id="form">
            <div class="form-body">
                
                <div class="form-group">
                    <label class="col-md-2 control-label">Proveedor</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="proveedor" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">NIT</label>
                    <div class="col-md-3">
                        <input type="text" class="form-control" id="nit" required/>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-md-1 col-md-11">
                    <a class="btn btn-success">Consultar</a>
                </div>
            </div>

        </form>
            </div>
        
    </div>
        
            </div>
    
    
    
</div>

<script>    
function guardarprovarc(){
    $("#formarcprov").submit();
}

function exportarprov(){
    window.open("exportarprov");
}
</script>