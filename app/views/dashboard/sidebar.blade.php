<!--BEGIN SIDEBAR -->
<div class="page-sidebar navbar-collapse collapse" style="position:fixed">
   <!-- BEGIN SIDEBAR MENU -->        
   <ul class="page-sidebar-menu">
      <li>
         <form class="search-form search-form-sidebar" role="form">
            <div class="input-icon right">
               <i class="icon-search"></i>
               <input type="text" class="form-control input-medium input-sm" placeholder="Search...">
            </div>
         </form>
      </li>
      <li>
         <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
         <div class="sidebar-toggler"></div>
         <div class="clearfix"></div>
         <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
      </li>
      <!-- <li class="start  ">
         <a href="{{URL::to("informacion")}}">
         <i class="icon-home"></i> 
         <span class="title">Dashboard</span>
         <span class="selected"></span>
         </a>
      </li> -->
      <?php
//      $bandera="";
//                foreach ($user->profiles as $perfil){
//                foreach($perfil->submenus as $submenu){
//                           
//                $menu = Menus::find($submenu->menu_id);
//                if($bandera != $menu){
//                           echo $menu->menu."<br>";
//                           $bandera=$menu;
//                }
//                
//                          echo $submenu->submenu.'<br>';
//                           
//                }
//                
//                }
      ?>
      
      <?php
      $bandera="";
      $cont=0;
      ?>
      @foreach ($user->Profiles as $perfil)
      <?php
      $perfil_id = $perfil->id;      
      $sql = "SELECT menus.id, menu, icon, profiles_id, submenus_id, submenus.id as id_submenu, submenu, link, menu_id FROM profiles_submenus, submenus, menus where profiles_id = '$perfil_id' and submenus.id = submenus_id and menus.id = menu_id order by submenus.menu_id, submenus.id";
      $consulta = DB::select($sql);
        ?>
         @foreach($consulta as $submenu)
         <?php
            $menu = $submenu->menu_id;
         ?>
            @if($bandera != $menu)
               @if($cont !=0)
                     </ul>
                  </li>
            @endif
         <?php
            $cont=1;
         ?>
            @if($menu_activo == $submenu->menu)
            <li class="start active">
            @else
            <li class="">
            @endif            
            <a href="javascript:;">
         <i class="{{$submenu->icon}}"></i> 
         <span class="title">{{$submenu->menu}}</span>
         <span class="arrow "></span>
         </a>
            <ul class="sub-menu">
                           <?php
                           $bandera=$menu;
                           ?>
                @endif
                @if($submenu_activo == $submenu->submenu)
            <li class="active">
            @else
            <li class="">
            @endif
               <a href="{{URL::to($submenu->link)}}">
                   @if($submenu->id_submenu == 4)
                   @if($notiadm > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiadm }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 5)
                   @if($notiate > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiate }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 30)
                   @if($notiaapr > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiaapr }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 27)
                   @if($notiadmcomp > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiadmcomp }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 26)
                   @if($notigestcomp > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notigestcomp }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 34)
                   @if($notiadmpag > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiadmpag }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 37)
                   @if($noticaupag > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $noticaupag }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 36)
                   @if($notiverpag > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiverpag }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 38)
                   @if($notitespag > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notitespag }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 63)
                   @if($notiveractiv > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notiveractiv }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                   @if($submenu->id_submenu == 66)
                   @if($notimisactiv > 0)
                   <span class="badge badge-important" id="{{ $submenu->id_submenu }}">{{ $notimisactiv }}</span>
                   @else
                   <span class="badge badge-important" style="display:none" id="{{ $submenu->id_submenu }}">0</span>
                   @endif
                   @endif
                  {{$submenu->submenu}}</a>
            </li>
                @endforeach                
                @endforeach
       </ul>
        </li>      
       
   </ul>
   <!-- END SIDEBAR MENU -->
</div>
<!-- END SIDEBAR