<?php 
function interval_date_noti($init,$finish){
    //formateamos las fechas a segundos tipo 1374998435
    $diferencia = strtotime($finish) - strtotime($init);
 
    //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
    //floor devuelve el número entero anterior, si es 5.7 devuelve 5
    if($diferencia < 60){
        $tiempo = floor($diferencia) . " Seg";
    }else if($diferencia > 60 && $diferencia < 3600){
        $tiempo = floor($diferencia/60) .  ' Min';
    }else if($diferencia > 3600 && $diferencia < 86400){
        $tiempo = floor($diferencia/3600) . " Horas";
    }else if($diferencia > 86400 && $diferencia < 2592000){
        $tiempo = floor($diferencia/86400) . " Días";
    }else if($diferencia > 2592000 && $diferencia < 31104000){
        $tiempo = floor($diferencia/2592000) . " Meses";
    }else if($diferencia > 31104000){
        $tiempo = floor($diferencia/31104000) . " Años";
    }else{
        $tiempo = "Error";
    }
    return $tiempo;
}
?>

   <div class="page-content">
   <!-- BEGIN PAGE HEADER-->
      <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Mis Notificaciones
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Página Principal</a> 
                  
               </li>
               
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <div class="tabbable tabbable-custom">
                  
                  <div class="tab-content">
                     <div class="tab-pane active" id="my_supports">
                        <div class="table-responsive">
                            <table class="table top-blue">                            
                            @foreach($notifications as $notification )
                            <?php $style = ''; ?>
                            <?php $user = Users::find($notification->user_id1); 
                                 $fecha = $notification->created_at;
                                 $fecha_actual = date('Y-m-d H:i:s');
                                 $fecha_t =  interval_date_noti($fecha, $fecha_actual);
                            ?>
                            @if($notification->type_notifications_id == 1) 
                                <?php $style = 'green'; ?>
                            @elseif($notification->type_notifications_id == 2 )
                                <?php $style = 'yellow'; ?>

                            @elseif($notification->type_notifications_id == 3 )
                                <?php $style = 'blue'; ?>
                            @elseif($notification->type_notifications_id == 5) 
                                <?php $style = 'orange'; ?>
                            @endif

                            @if($notification->estate == 1)
                              <?php $class = 'bgcolor="#E6E6E6"'; ?>
                              <?php $style = 'gray'; ?>
                            @else
                              <?php $class = '' ?>
                            @endif
                           <tr <?php echo $class; ?> class="{{$style}}">
                               <td>
                                    <div style="font-size: 13px">
                                        <a href="{{$notification->link}}" id="{{$notification->id}}" onclick="desactive_notification($(this).attr('id'));return false;">
                                            <img alt="" class="top-avatar" src="{{URL::to($user->img_min)}}"/>
                                            <i class="{{$notification->TypeNotifications->icon}}"></i>
                                            <strong><span>{{$user->name}} {{$user->name2}} {{$user->last_name}} {{$user->last_name2}}</span></strong>
                                            <span>{{$notification->description}}</span>
                                            <span class="time">hace {{$fecha_t}}</span>
                                        </a>
                                    </div>
                               </td>                           
                           </tr>
                           @endforeach
                           </table>
                        </div>
                     </div>   
                     
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>           
<script type="text/javascript">

function funcion2(hola){
   var hola = hola;

  
   $('#ocult'+hola).toggle('slow');
}
</script>

