<div class="page-content">
   <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->         
   <div class="modal fade" id="portlet-config" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Modal title</h4>
            </div>
            <div class="modal-body">
               Widget settings form goes here
            </div>
            <div class="modal-footer">
               <button type="button" class="btn btn-success">Save changes</button>
               <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
         </div>
         <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
   </div>
   <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
   <!-- BEGIN STYLE CUSTOMIZER -->         
   <div class="theme-panel hidden-xs hidden-sm">
      <div class="toggler"><i class="icon-gear"></i></div>
      <div class="theme-options">
         <div class="theme-option theme-colors clearfix">
            <span>Theme Color</span>
            <ul>
               <li class="color-black current color-default tooltips" data-style="default" data-original-title="Default"></li>
               <li class="color-grey tooltips" data-style="grey" data-original-title="Grey"></li>
               <li class="color-blue tooltips" data-style="blue" data-original-title="Blue"></li>
               <li class="color-red tooltips" data-style="red" data-original-title="Red"></li>
               <li class="color-light tooltips" data-style="light" data-original-title="Light"></li>
            </ul>
         </div>
         <div class="theme-option">
            <span>Layout</span>
            <select class="layout-option form-control input-small">
               <option value="fluid" selected="selected">Fluid</option>
               <option value="boxed">Boxed</option>
            </select>
         </div>
         <div class="theme-option">
            <span>Header</span>
            <select class="header-option form-control input-small">
               <option value="fixed" selected="selected">Fixed</option>
               <option value="default">Default</option>
            </select>
         </div>
         <div class="theme-option">
            <span>Sidebar</span>
            <select class="sidebar-option form-control input-small">
               <option value="fixed">Fixed</option>
               <option value="default" selected="selected">Default</option>
            </select>
         </div>
         <div class="theme-option">
            <span>Footer</span>
            <select class="footer-option form-control input-small">
               <option value="fixed">Fixed</option>
               <option value="default" selected="selected">Default</option>
            </select>
         </div>
      </div>
   </div>
   <!-- END BEGIN STYLE CUSTOMIZER -->            
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         <h3 class="page-title">
            Basic Tables <small>basic table samples</small>
         </h3>
         <ul class="page-breadcrumb breadcrumb">
            <li>
               <i class="icon-home"></i>
               <a href="index.html">Home</a> 
               <i class="icon-angle-right"></i>
            </li>
            <li>
               <a href="#">Data Tables</a>
               <i class="icon-angle-right"></i>
            </li>
            <li><a href="#">Basic Tables</a></li>
         </ul>
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row">
      <div class="col-md-6">
         <!-- BEGIN SAMPLE TABLE PORTLET-->
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-cogs"></i>Listado de menus</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="table-responsive">
                  <table class="table table-hover">
                     <thead>
                        <tr>
                           <th>ID</th>
                           <th>Nombre</th>
                           <th>Link</th>
                           <th>Operaciones</th>
                        </tr>
                     </thead>
                     <tbody>
                     @if(isset($error))
                     <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <strong>Error!</strong> {{$error}}.
                     </div>
                     @else
                     <!-- $users es la variable enviada de controles with() -->
                     @if($menus)
                     <!-- asignamos a un bucle de array $users a $user -->
                     @foreach($menus->menus as $menu)
                        <tr>
                           <td>{{$menu->id}} </td>
                           <td>{{$menu->name}}</td>
                           <td>{{$menu->link}}</td>
                           <td>
                              <a href="#" title="{{$menu->id}}" class="btn btn-danger delete">
                                 <i class="icon-remove"></i>
                              </a>
                           </td>
                        </tr>
                     @endforeach
                     @endif
                     @endif
                     </tbody>
                  </table>
                  <?php $status=Session::get('status'); ?>
                  @if($status == 'ok_create')
                  <div class="alert alert-info alert-dismissable">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                     <strong>Exito!</strong> El Usuario fue creado correctamene.
                  </div>
                  @endif
                  @if($status == 'ok_delete')
                  <div class="alert alert-danger alert-dismissable">
                     <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                     <strong>Exito!</strong> El Usuario fue eliminado correctamene.
                  </div>
                  @endif
               </div>
            </div>
         </div>
         <!-- END SAMPLE TABLE PORTLET-->
      </div>
   </div>
   
   <!-- END PAGE CONTENT-->
</div>

