<div class="page-content" ng-app>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Información General
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="informacion">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Intranet</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li><a href="#"> 
                        Información General
                    </a></li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab_general" data-toggle="tab">Información General</a></li>
                        <li class=""><a id="directoriofun" href="#tab_funcionarios" data-toggle="tab">Directorio Funcionarios</a></li>
                        <li class=""><a id="calendarfun" href="#tab_calendar" data-toggle="tab">Calendario Indoamericana</a></li>

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_general">
        
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="portlet">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-reorder"></i>Quienes Somos</div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="tabbable tabs-left">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab_6_1" data-toggle="tab">Misión</a></li>
                                                    <li class=""><a href="#tab_6_2" data-toggle="tab">Visión</a></li>
                                                    <li class=""><a href="#tab_6_3" data-toggle="tab">Reseña Historica</a></li>
                                                    <li class=""><a href="#tab_6_4" data-toggle="tab">Brochure</a></li>
                                                    <li class=""><a href="#tab_6_5" data-toggle="tab">Certificaciones</a></li>
                                                    <li class=""><a href=""></a></li>
                                                    <li class=""><a href=""></a></li>
                                                    <p></p>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_6_1">
                                                        <div class="col-md-4 blog-img blog-tag-data">
                                                            <img src="assets/img/2.png" alt="" width="500px" class="img-responsive">
                                                        </div>

                                                        <p>La Misión de la Corporación Educativa Indoamericana es capacitar técnicamente personal en el campo de la aviación, el transporte y el turismo, 
                                                            mediante el enfoque de formación integral paralela a la instrucción, que les permita desempeñarse laboralmente de forma idónea, eficaz y eficiente 
                                                            a nivel Nacional e Internacional.</p>
                                                    </div>
                                                    <div class="tab-pane" id="tab_6_2">
                                                        <div class="col-md-4 blog-img blog-tag-data">
                                                            <img src="assets/img/1.png" alt="" class="img-responsive">                           
                                                        </div>
                                                        <p>Nuestra visión para el año 2016 está centrada en ser la institución de formación técnica aeronáutica de referencia para Centro y Suramérica, apreciados 
                                                            por el profesionalismo y ética de nuestro capital humano y reconocidos por la excelente instrucción académica impartida, basada en el desarrollo de 
                                                            competencias laborales que fortalecen el desempeño de estudiantes y egresados. </p>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_6_3">
                                                        <div class="col-md-4 blog-img blog-tag-data">
                                                            <img src="assets/img/4.jpg" alt="" class="img-responsive">                           
                                                        </div>
                                                        <p>
                                                            La CORPORACIÓN EDUCATIVA INDOAMERICANA LTDA., fue creada el
                                                            22 de septiembre de 1987 por medio de Escritura Pública. Las
                                                            instalaciones de la Institución se encuentran ubicadas en la Calle 39 Nº
                                                            14-62 en el Barrio Teusaquillo, en donde se manejan los programas
                                                            académicos: Técnico en Línea de Avión (TLA), Técnico en Línea de
                                                            Helicópteros (TLH), Técnico en Aviónica (TEEI), Despachador de
                                                            Aeronaves (DPA), Auxiliar de Servicios Abordo y Servicios Aeroportuarios
                                                            (ASA), Asistente en Turismo y Agencias de Viaje (ATA), Agente en
                                                            Logística Aeroportuaria (ALA) y Conductor de Transporte Público (CTP).
                                                        </p>
                                                        <p>
                                                            La Institución cuenta con más de 26 años de experiencia en la formación
                                                            de técnicos laborales capacitados para desempeñarse en el sector
                                                            aeronáutico y se ha venido posicionando a través de los principios y la
                                                            filosofía institucional: "Formación Integral paralela a la Instrucción",
                                                            proyectada hacia diversas instituciones del estado y empresas que
                                                            incursionan en el sector aeronáutico.
                                                        </p>
                                                        <p>
                                                            En la actualidad el Centro de Instrucción Aeronáutico cuenta con una
                                                            planta física con un área de construcción total de 6.884,26 m² y
                                                            7.182,19 m² de terreno disponible, la cual está dotada de varios
                                                            espacios como: aulas, talleres, laboratorios, maquetas y 2 hangares que
                                                            permiten el desarrollo adecuado de las clases.
                                                            1. RESEÑA HISTÓRICA
                                                        </p>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_6_4">
                                                        <div class="col-md-4 blog-img blog-tag-data">
                                                            <img src="assets/img/brochure.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <p>La Corporación Educativa Indoamericana capacita Técnicos Aeronáuticos desde 1987, supliendo las necesidades de 
                                                            formación existentes en el sector aéreo. Durante sus 26 años de trayectoria, ha trabajado inagotablemente para ofrecer 
                                                            a sus estudiantes educación técnica con altos estándares de calidad, entregando a la industria aeronáutica capital humano 
                                                            altamente capacitado en los programas de Agente Logística Aeroportuaria, Auxiliar de Servicios Abordo y Aeroportuarios, 
                                                            Técnico Línea de Aviones, Técnico Línea de Helicópteros, Técnico Especialista en Aviónica, Técnico en Despacho de 
                                                            Aeronaves, Técnico en Agente de Turismo, Agencias de Viaje y Conductor de Transporte Público.</p>
                                                        <div class="col-md-1 blog-img blog-tag-data">
                                                            <a href="{{ URL::to("assets/img/resources/BROCHURE 2014-II.pdf")}}" target="_blank"><img src="assets/img/pdf.png" alt="" class="img-responsive" style=" width: 50px; height: 50px;"></a>
                                                        </div><br />
                                                        <a href="{{ URL::to("assets/img/resources/BROCHURE 2014-II.pdf")}}" target="_blank"><i class="icon-paperclip"></i> Ver Brochure completo</a>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_6_5">
                                                        <div class="col-md-4 blog-img blog-tag-data">
                                                            <img src="assets/img/certificaciones.jpg" alt="" class="img-responsive">
                                                        </div>
                                                        <div class="col-md-6">
                                                            <ul class="list-group">
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/CERTIFICACION ICONTEC.pdf")}}" target="_blank"><i class="icon-file-text"></i> CERTIFICACIÓN ICONTEC SC-CER148627</a></li>
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/CERTIFICACION IQNET.pdf")}}" target="_blank" ><i class="icon-file-text"></i> CERTIFICACIÓN IQNET CO-SC-CER148627</a>   </li>
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/Resolución 13006 Secretaria de Educación febrero 01 de 2013.pdf")}}" target="_blank" ><i class="icon-paperclip"></i> Resolución 13006 Secretaria de Educación febrero 01 de 2013</a></li>
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/resolucion_secretaria.pdf")}}" target="_blank" ><i class="icon-paperclip"></i> Resolución 13-0091 Secretaria de Educación 16 Junio 2010</a>   </li>
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/especificacion_operacion_2013.pdf")}}" target="_blank" ><i class="icon-paperclip"></i> Especificaciones de Operaciones</a>   </li>
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/certificado_funcionamiento2.pdf")}}" target="_blank" ><i class="icon-paperclip"></i> Certificado de Funcionamiento</a>   </li>
                                                                <li class="list-group-item"><a href="{{ URL::to("assets/img/resources/resolucion2008.pdf")}}" target="_blank" ><i class="icon-paperclip"></i> Resolución 2008</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    
                                        <div class="row">
        <div class="col-md-12">
            <ul class="page-breadcrumb breadcrumb">
                <h4>Parametros Institucionales y Normatividad</h4>
            </ul>
        </div>
    </div>
                                                                                                                            
                                                <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Normatividad</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <a href="#normatividad" data-toggle="modal" id="">
                                                            <img src="{{ URL::to("assets/img/resources/normatividad.png")}}" alt="test" style="width: 100%; height: 400px; display: block;" data-src="holder.js/100%x200" />
                                                        </a>
                                                    </div>
                                                </div>
                                                </div>
                                                
                                                <div class="col-md-6">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h3 class="panel-title">Parámetros Institucionales</h3>
                                                    </div>
                                                    <div class="panel-body">
                                                        <a href="#parametros" data-toggle="modal" id="">
                                                            <img src="{{ URL::to("assets/img/resources/parametros.png")}}" alt="test" style="width: 100%; height: 400px; display: block;" data-src="holder.js/100%x200" />
                                                        </a>
                                                    </div>
                                                </div>                                                
                                            </div>

                                    <div class="portlet">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-reorder"></i>Políticas y Objetivos Institucionales</div>
                                            <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                            </div>
                                        </div>
                                        <div class="portlet-body">
                                            <div class="tabbable tabs-left">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#tab_7_1" data-toggle="tab">Política de la calidad</a></li>
                                                    <li class=""><a href="#tab_7_2" data-toggle="tab">Política de salud ocupacional</a></li>
                                                    <li class=""><a href="#tab_7_3" data-toggle="tab">Política de gestion de la seguridad operacional</a></li>
                                                    <li class=""><a href="#tab_7_4" data-toggle="tab">Objetivos de la calidad</a></li>
                                                    <li class=""><a href="#tab_7_5" data-toggle="tab">Objetivos de salud ocupacional</a></li>
                                                    <li class=""><a href="#tab_7_6" data-toggle="tab">Mapa de Procesos</a></li>
                                                    <p></p>

                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_7_1">

                                                        <p>La Corporación Educativa Indoamericana, Institución de Formación para
                                                            el Trabajo y el Desarrollo Humano cuya misión es la formación de
                                                            Auxiliares y Técnicos en el sector Aeronáutico, Turístico y Transporte, se
                                                            compromete con los estudiantes a satisfacer sus necesidades y
                                                            expectativas colocando a su disposición la infraestructura física y
                                                            tecnológica necesaria para el desarrollo de las clases junto con un
                                                            equipo de instructores idóneos, actualizados y licenciados capaces de
                                                            brindar excelentes conocimientos y fortalecer sus competencias
                                                            laborales. Su talento Humano trabaja día a día eficientemente en el
                                                            mejoramiento continuo de los procesos, la formación integral paralela a
                                                            la instrucción y en lograr una formación académica de excelencia, que
                                                            nos permita acreditar altos estándares de calidad cumpliendo con los
                                                            requisitos de las normas ISO 9001:2008 y NTC 5555 en todos los
                                                            procesos y NTC 5581 para los programas académicos.</p>
                                                    </div>
                                                    <div class="tab-pane" id="tab_7_2">
                                                        <p>La CORPORACIÓN EDUCATIVA INDOAMERICANA LTDA., declara su
                                                            compromiso con el cumplimiento de la normas de seguridad en materia
                                                            de Salud Ocupacional, el mejoramiento continuo de las condiciones de
                                                            trabajo y la protección de sus colaboradores a través de la
                                                            implementación de procesos de identificación y eliminación de riesgos,
                                                            desarrollo de prácticas de auto cuidado, fortalecimiento de
                                                            comportamientos de trabajo seguro y la ejecución de programas de
                                                            formación en Salud Ocupacional.</p>
                                                        <p>La CORPORACIÓN EDUCATIVA INDOAMERICANA LTDA., considera que
                                                            los accidentes y enfermedades laborales pueden ser prevenidos, por
                                                            tanto la protección de la salud de sus colaboradores es un valor
                                                            primordial transversal a todos los procesos, el cual se refleja en la actitud
                                                            constante de vigilancia y control de la seguridad personal y colectiva.</p>
                                                        <div class="col-md-1 blog-img blog-tag-data">
                                                            <a href="{{ URL::to("assets/img/resources/POLITICAS_SO.pdf")}}" target="_blank"><img src="assets/img/pdf.png" alt="" class="img-responsive" style=" width: 50px; height: 50px;"></a>
                                                        </div><br />
                                                        <a href="{{ URL::to("assets/img/resources/POLITICAS_SO.pdf")}}" target="_blank"><i class="icon-paperclip"></i> Ver Archivo PDF</a>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_7_3">                                                        
                                                        <p>
                                                            La CORPORACIÓN EDUCATIVA INDOAMERICANA, Centro de
                                                            Instrucción certificado ante la Aeronáutica Civil, consciente de preservar
                                                            la salud, la propiedad, la seguridad y el medio ambiente, se
                                                            compromete a mantener un sistema que asegure la mitigación de las
                                                            situaciones de riesgo, mediante la promoción de la seguridad, la
                                                            asignación de recursos necesarios y el cumplimiento de la normatividad
                                                            nacional e internacional.
                                                        </p>
                                                        <div class="col-md-1 blog-img blog-tag-data">
                                                            <a href="{{ URL::to("assets/img/resources/POLITICA_GESTION_DE_LA_SEGURIDAD_OPERACIONAL.pdf")}}" target="_blank"><img src="assets/img/pdf.png" alt="" class="img-responsive" style=" width: 50px; height: 50px;"></a>
                                                        </div><br />
                                                        <a href="{{ URL::to("assets/img/resources/POLITICA_GESTION_DE_LA_SEGURIDAD_OPERACIONAL.pdf")}}" target="_blank"><i class="icon-paperclip"></i> Ver Archivo PDF</a>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_7_4">                                                        
                                                        <p>
                                                        <ul>
                                                            <li>Conseguir nuevos clientes para los cursos regulares.</li>
                                                            <p></p>
                                                            <li>Conseguir nuevos clientes provenientes del exterior.</li>
                                                            <p></p>
                                                            <li>Cumplir con la formaci&oacute;n establecida.</li>
                                                            <p></p>
                                                            <li>Capacitar al cliente interno.</li>
                                                            <p></p>
                                                            <li>Obtener nuevas certificaciones ante entidades externas.</li>
                                                        </ul>
                                                        </p>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_7_5">                                                        
                                                        <p>
                                                        <ul>
                                                            <li>Revisar permanentemente las condiciones de trabajo con el&nbsp;objetivo de garantizar ambientes laborales seguros y saludables.</li>
                                                            <li>Promover la participaci&oacute;n activa de los trabajadores en relaci&oacute;n a la&nbsp;observaci&oacute;n, diagn&oacute;stico y notificaci&oacute;n oportuna de todas las&nbsp;condiciones de riesgo que observen durante la jornada laboral.</li>
                                                            <li>Verificar que el personal de la Corporaci&oacute;n reciba la informaci&oacute;n&nbsp;relacionada con los riesgos a los que est&aacute;n expuestos, sus efectos y&nbsp;las medidas correctivas que deber&aacute;n&nbsp;implementarse.</li>
                                                            <li>Proporcionar los medios necesarios para el normal desempe&ntilde;o de&nbsp;las funciones y actividades del plan escolar de emergencia y&nbsp;contingencia.</li>
                                                            <li>Facilitar la asistencia de los colaboradores a los diferentes cursos de&nbsp;capacitaci&oacute;n y entrenamiento que coordine y/o realice el&nbsp;coordinador del sistema de gesti&oacute;n de la&nbsp;seguridad y salud en el&nbsp;trabajo para la prevenci&oacute;n y control de riesgos, accidentes,&nbsp;incidentes y enfermedades laborales.</li>
                                                            <li>Promover la cultura del auto cuidado a trav&eacute;s de adopci&oacute;n de&nbsp;comportamientos seguros en los diferentes ambientes laborales en&nbsp;los que se desempe&ntilde;a el colaborador.</li>
                                                        </ul>
                                                        </p>
                                                    </div>
                                                    <div class="tab-pane fade" id="tab_7_6">
                                                        <p>
                                                        <a href="#mapaprocesos" data-toggle="modal" id="">
                                                            <img src="{{ URL::to("assets/img/resources/mapaprocesos.png")}}" alt="test" style="width: 65%; height: 230px; display: block;" data-src="holder.js/100%x200" />
                                                        </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-13">
                                        <div class="portlet grey">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-file-text"></i>Reglamentos</div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="list-group">
                                                    <a href="#" class="list-group-item active bg-yellow">Enlaces de interés</a>
                                                    <a href="{{ URL::to("assets/img/resources/MANUAL GENERAL DE DIRECTIVAS DE INSTRUCCION.pdf")}}"
                                                       target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Manual general de directivas de instrucción</a>
                                                    <a href="{{ URL::to("assets/img/resources/REGLAMENTO_INTERNO_DE_TRABAJO_DE_CORPORACION_EDUCATIVA_INDOAMERICANA_LIMITADA.PDF")}}"
                                                       target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Reglamento interno de trabajo</a>
                                                    <a href="{{ URL::to("assets/img/resources/MANUAL DE CONVIVENCIA CEI.pdf")}}"
                                                       target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Manual De Convivencia CEI</a>   
                                                    <!--<a href="{{ URL::to("assets/img/resources/GI7-DOC1 MANUAL DEL USUARIO.pdf")}}"
                                                       target="_blank" class="list-group-item"><i class="icon-paperclip"></i> GI7-MUT001 Manual del Usuario TIC.s</a> -->
                                                    <a href="{{ URL::to("assets/img/resources/MANUAL DE FUNCIONES INDOAMERICANA CEI REVISION 7.pdf")}}"
                                                       target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Manual de Funciones</a>   
                                                    <a href="{{ URL::to("assets/img/resources/SGC2 MC MANUAL DE LA CALIDAD INDOAMERICANA ULTIMA REVISION.pdf")}}"
                                                       target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Manual de la Calidad Indoamericana</a>
                                                </div>
                                            </div>
                                        </div>                                        
                                        <!-- <div class="portlet grey">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-file"></i>VOip Presentación</div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="list-group">
                                                    <a href="#" class="list-group-item active bg-yellow">Enlaces de interés</a>
                                                    <a href="{{ URL::to("assets/img/resources/CAPACITACION Admon voip.pdf")}}" target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Presentación Capacitación Sistemas de Comunicación ASTERIKS y Uso de Telefonos VOip "YEALINK"</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="portlet grey">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-link"></i>Contáctos VOip ASTERISK</div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="list-group">
                                                    <a href="#" class="list-group-item active bg-yellow">Enlaces de interés</a>
                                                    <a href="listaEmpleados" id="employees" class="list-group-item">  
                                                        <i class="icon-paperclip"></i> 
                                                        Listado de empleados, correos electrónicos y nueva asignación de extensiones.<span class="badge badge-danger">destacado</span>
                                                    </a> 
                                                    <a href="{{ URL::to("assets/img/resources/GI7-DOC5 MANUAL SYSAID.pdf")}}" target="_blank" class="list-group-item"><i class="icon-paperclip"></i> SysAid</a>   
                                                    <a href="{{ URL::to("assets/img/resources/GI7-DOC5 MANUAL SYSAID.pdf")}}" target="_blank" class="list-group-item"><i class="icon-paperclip"></i> Consulte Manual</a>   
                                                </div>
                                            </div>
                                        </div> -->


                                    </div>


                                </div>

                                <div class="col-md-4">
                                    <div class="portlet grey">  

                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-reorder"></i>Ultimas Noticias</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                                    <!-- Indicators -->
                                                    <ol class="carousel-indicators">
                                                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="5"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="6"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="7"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="8"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="9"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="10"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="11"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="12"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="13"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="14"></li>
                                                        <li data-target="#carousel-example-generic" data-slide-to="15"></li>
                                                        <!-- <li data-target="#carousel-example-generic" data-slide-to="16"></li> -->
                                                    </ol>

                                                    <!-- Wrapper for slides -->
                                                    <div class="carousel-inner">
                                                        <!-- <div class="item active">
                                                            <a href="#help_desk" data-toggle="modal" id="">
                                                                <img src="{{ URL::to("assets/img/noticias/help_desk.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div> -->
                                                        <div class="item active">
                                                            <a href="#dianino" data-toggle="modal" id="">
                                                                <img src="{{ URL::to("assets/img/noticias/ninomin.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="#salud" data-toggle="modal" id="">
                                                                <img src="{{ URL::to("assets/img/noticias/saludmin.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="#compras" data-toggle="modal" id="">
                                                                <img src="{{ URL::to("assets/img/noticias/compras.png")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="#compensar" data-toggle="modal" id="">
                                                                <img src="{{ URL::to("assets/img/noticias/marcha_por_la_vida.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                                                                                                                                        
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/Brigada-emergencia.pdf")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/conformacionbrigada.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <img src="{{ URL::to("assets/img/noticias/slide_cumpmarzo.jpg")}}" width="500px" alt="...">      
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>                                                        
                                                        <div class="item">
                                                            <a href="http://www.aerocivil.gov.co/AAeronautica/Rrglamentacion/RAC/Paginas/Inicio.aspx" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/SLIDE RAC.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="http://indoamericana.edu.co/index.php?option=com_content&view=article&id=97" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/slide_noticias_intranet.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/QU%C3%89%20TAN%20FELIZ%20ERES%20EN%20TU%20TRABAJO.pptm")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/que tan feliz.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/ecologica administrativos.pdf")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/ecologica_administrativos_slide.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/nueva%20estructura%20brigada%202013.ppt")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/brigada_2013_1.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/GH10_comunicado_NO_137.pdf")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/comunicado_137.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/PQs%20SP.doc")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/OACI.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <a href="{{ URL::to("assets/img/noticias/files/ACAC001.pdf")}}" target="_BLANK">
                                                                <img src="{{ URL::to("assets/img/noticias/acac.jpg")}}" width="500px" alt="...">
                                                            </a>
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <img src="{{ URL::to("assets/img/noticias/LIBRANZA.jpg")}}" width="500px" alt="...">
                                                            <div class="carousel-caption">
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <!-- Controls -->
                                                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                                    </a>
                                                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                                    </a>
                                                </div> <!-- Carousel -->
                                            </div>
                                        </div>
                                    </div>


                                    <div class="portlet grey">
                                        <div class="portlet-title">
                                            <div class="caption">Así estamos conformados</div>
                                        </div>
                                        <div class="portlet-body">

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Organigrama</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <a href="#organigrama" data-toggle="modal" id="">
                                                        <!--<a href="{{ URL::to("assets/img/resources/ORGANIGRAMA_GENERAL_INDOAMERICANA_13.pdf")}}" target="_blank">-->
                                                        <img src="{{ URL::to("assets/img/organigrama.png")}}" alt="test" style="width: 100%; height: 200px; display: block;" data-src="holder.js/100%x200" />
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Logo</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <a href="#logo" data-toggle="modal" id="">
                                                        <!--<a href="{{ URL::to("assets/img/resources/logo.png")}}" target="_blank">-->
                                                        <img src="{{  URL::to("assets/img/resources/logo.png")}}" alt="test" style="width: 100%; height: 222px; display: block;"/>
                                                    </a>                                          
                                                </div>
                                            </div>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Sistemas de gestión</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <a href="{{ URL::to("assets/img/resources/CARTILLA.pdf")}}" target="_blank">
                                                        <img src="{{ URL::to("assets/img/sistemagestion.png")}}" alt="test" style="width: 100%; height: 200px; display: block;" data-src="holder.js/100%x200" />
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Calendario 2015</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <a href="{{  URL::to("assets/img/resources/plegable_indoamericana_2015.pdf")}}" target="_BLANK">
                                                        <!--<a href="{{ URL::to("assets/img/resources/logo.png")}}" target="_blank">-->
                                                        <img src="{{  URL::to("assets/img/resources/calendario_2015.png")}}" alt="test" style="width: 100%; height: 222px; display: block;"/>
                                                    </a>                                          
                                                </div>
                                            </div>

                                            <div class="panel panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Datos de contacto</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="portlet-body">
                                                        <div class="well">
                                                            <h4>Información General</h4>
                                                            <address>
                                                                <strong><i class="icon-map-marker"></i> Dirección:</strong> Calle 39 No. 14-62<br>
                                                                <strong><i class="icon-phone"></i> Teléfono:</strong> 3 23 97 50<br>
                                                                <strong><i class="icon-file-text"></i> FAX:</strong> 2 45 32 49<br>
                                                                <strong><i class="icon-envelope"></i> E-mail:</strong> <a href="mailto:info@indoamericana.edu.co">info@indoamericana.edu.co</a><br>
                                                            </address>
                                                            <h4>Redes Sociales</h4>
                                                            <ul class="social-icons margin-bottom-10">
                                                                <li>
                                                                    <a href="https://es-la.facebook.com/CEIIndoamericana" data-original-title="facebook" class="facebook" title="Indoamericana Facebook" target="_blank" ></a>   
                                                                </li>
                                                                <li>
                                                                    <a href="https://plus.google.com/u/0/+indoamericana/posts" data-original-title="Goole Plus" class="googleplus" title="Indoamericana Google+" target="_blank"></a>
                                                                </li>    
                                                                <li>
                                                                    <a href="http://twitter.com/indoamericana" data-original-title="twitter" class="twitter" title="Indoamericana Twitter" target="_blank"></a>      
                                                                </li>
                                                                <li>
                                                                    <a href="http://youtube.com/indoamericana1" data-original-title="youtube" class="youtube" title="Indoamericana Youtube" target="_blank"></a>      
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="tab-pane" id="tab_funcionarios">
                            <div class="col-md-12" id="funcionarios" ng-controller="FunCtrl">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-table"></i>Información</div>                  
                                                <div class="tools">                      
                                                    <input id="buscatuto" type="text" ng-model="search" placeholder="Buscador">                                                    
                                                    <a href="javascript:;" class="collapse"></a>                                                    
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <table class="table table-striped table-hover top-blue">
                                                        <thead>
                                                            <tr>
                                                                <th>Imagen</th>
                                                                <th>Nombre </th>
                                                                <th>Cargo</th>
                                                                <th>Proceso </th>
                                                                <th>Extensión</th>
                                                                <th>Correo</th>
                                                                <th>Celular</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr  ng-repeat="funcionario in funcionarios| filter:search">
                                                                <td>                               
                                                                    <a href="#basic" data-toggle="modal" id="@{{funcionario.id}}" onclick="detalle($(this).attr('id'));
                                                                                        return false;">
                                                                        <img class="profile" src="@{{funcionario.img}}"alt="" style=" width: 152px; height: 126px; display: block;" />
                                                                    </a>
                                                                </td>
                                                                <td>@{{funcionario.last_name}} @{{funcionario.last_name2}} @{{funcionario.name}} @{{funcionario.name2}}</td>
                                                                <td>@{{funcionario.profile}}</td>
                                                                <td>@{{funcionario.acronyms}}</td>
                                                                <td>@{{funcionario.extension}}</td>
                                                                <td>@{{funcionario.email_institutional}}</td>
                                                                <td>@{{funcionario.cell_phone}}</td>
                                                            </tr>

                                                            <tr>
                                                                <td></td>
                                                                <td>PORTERÍA</td>
                                                                <td>PORTERÍA</td>
                                                                <td></td>
                                                                <td>1026</td>
                                                                <td></td>
                                                                <td></td>   
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>SALA DE PROFESORES</td>
                                                                <td>SALA DE PROFESORES</td>
                                                                <td></td>
                                                                <td>1029</td>
                                                                <td></td>
                                                                <td></td>   
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>CAFETERIA</td>
                                                                <td>CAFETERIA</td>
                                                                <td></td>
                                                                <td>1030</td>
                                                                <td></td>
                                                                <td></td>   
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>HELM</td>
                                                                <td>HELM</td>
                                                                <td></td>
                                                                <td>2020</td>
                                                                <td></td>
                                                                <td></td>   
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>FINCOMERCIO</td>
                                                                <td>FINCOMERCIO</td>
                                                                <td></td>
                                                                <td>2026</td>
                                                                <td></td>
                                                                <td></td>   
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td>FAX</td>
                                                                <td>FAX</td>
                                                                <td></td>
                                                                <td>1003</td>
                                                                <td></td>
                                                                <td></td>   
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-envelope"></i>Lista de Correos</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>                                                    
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="table-responsive">
                                                    <label ng-repeat="funcionario in funcionarios">
                                                        @{{funcionario.email_institutional}};
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab_calendar">
                            <iframe src="https://www.google.com/calendar/embed?src=jtuta%40indoamericana.edu.co&ctz=America/Bogota" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>

                        </div>                       
                    </div>         
                </div>   
            </div>   
        </div>
    </div>  


</div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">

    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="compras" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Modulo Compras y Pagos</h4>
            </div>
            <div class="modal-body">
                <iframe width="1070" height="600" src="https://www.youtube.com/embed/NtyMQPH9r88" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="help_desk" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Help-Desk</h4>
            </div>
            <div class="modal-body">
                <img width="1070" height="600" src="{{ URL::to("assets/img/noticias/help_desk.jpg")}}" frameborder="0" allowfullscreen></img>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="dianino" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Celebración Dia Del Niño</h4>
            </div>
            <div class="modal-body">
                <img src="{{  URL::to("assets/img/noticias/nino.jpg")}}" alt="test" style="width:100%"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="salud" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Día mundial de la seguridad y salud en el trabajo</h4>
            </div>
            <div class="modal-body">
                <img src="{{  URL::to("assets/img/noticias/salud.jpg")}}" alt="test" style="width:100%"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="compensar" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Marcha por la vida</h4>
            </div>
            <div class="modal-body">
                <embed id="doc-container" src="http://192.168.0.18/demo_galeria/">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="logo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">                
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Logo Corporacion Educativa Indoamericana</h4>
            </div>
            <div class="modal-body">
                <img src="{{  URL::to("assets/img/resources/logo.png")}}" alt="test" style="width: 100%; height: 280px;"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>


<div class="modal fade" id="organigrama" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Organigrama Corporacion Educativa Indoamericana</h4>
            </div>
            <div class="modal-body">
                <img src="{{  URL::to("assets/img/organigramabig.png")}}" alt="test" style="width: 100%; height: 100%;"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="normatividad" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Normatividad Corporacion Educativa Indoamericana</h4>
            </div>
            <div class="modal-body">
                <img src="{{  URL::to("assets/img/resources/normatividad.png")}}" alt="test" style="width: 100%; height: 100%;"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="parametros" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Parametros Institucionales Corporacion Educativa Indoamericana</h4>
            </div>
            <div class="modal-body">
                <img src="{{  URL::to("assets/img/resources/parametros.png")}}" alt="test" style="width: 100%; height: 100%;"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="mapaprocesos" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog modal-wide">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Mapa de Procesos Corporacion Educativa Indoamericana</h4>
            </div>
            <div class="modal-body">
                <img src="{{ URL::to("assets/img/resources/mapaprocesos.png") }}" alt="test" style="width: 100%; height: 50%;"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@if(Auth::user()->tutorial==0)
<div class="opacidad">
      <div class="padre" id="padre">
          
          <div class="alert alert-block alert-info fade in inicio" id="paso1">
                        <h3 class="alert-heading" style="text-align:center;"><b>Asistente SGI</b></h3>
                        </br>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px;">
                           Bienvenido al asistente del sistema de gestión de indoamericana, a continuación lo guiaremos por las nuevas funcionalidades de la aplicación.
                        </p>
                        <p>
                            <img src="{{ URL::to("assets/img/logotuto.png") }}" style="width: 100%; height: 25%;">
                        </p>
                        </br>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(1)">Siguiente</a>
                        </p>
                     </div>
          
                     <div class="alert alert-block alert-info fade in inicio" style="display: none;" id="paso2">
                        <h3 class="alert-heading" style="text-align:center;"><b>Buscador en Directorio</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Ahora el directorio de funcionarios, cuenta con un buscador en tiempo real, para que sea más fácil encontrar información de un usuario especifico.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/buscador.png") }}" style="width: 100%; height: 25%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(2)">Siguiente</a>
                        </p>
                     </div>

                     <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso3">
                        <h3 class="alert-heading" style="text-align:center;"><b>Calendario</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           En el calendario del SGI podrás ver la fecha de cumpleaños de cada uno de los funcionarios administrativos.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/calendario.png") }}" style="width: 100%; height: 50%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(3)">Siguiente</a>
                        </p>
                     </div>
   </div>
</div>
@endif
<script>    
    function detalle(id) {

        $.post('callUserProfile', {
            id: id
        },
        function (response) {
            $('#basic').html(response);
            //var datos = JSON.parse(response);
        });
    }    
    //$('.opacidad').fadeIn(1000);
            
      function Siguiente(id){         
         switch (id) {
                case 1:                
                $('#paso1').fadeOut(0);
                $('#paso2').fadeIn(0);
                $('#directoriofun').click();
                    break;

                case 2:
                //$("#header_inbox_bar").addClass("quemas");
                //$("#header_inbox_bar").animate({ 'zoom': 1.2 }, 400);
                $('#calendarfun').click();
                $('#paso2').fadeOut(0);
                $('#paso3').fadeIn(0);               
                break;

                case 3:
                //$("#hola").animate({ 'zoom': 1 }, 400);
                //$("#hola").removeClass("quemas");
                $('#paso3').fadeOut(0);
                $('.opacidad').fadeOut(1000);
                window.location = "my_supports_pending";
                break;
             }
      }
</script>