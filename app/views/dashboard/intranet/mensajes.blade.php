<link href="{{  URL::to("assets/css/pages/profile.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Mis Mensajes
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Intranet</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> Mis Mensajes</a>
                        </li>
                    </ul>
                </div>
            </div>

            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row profile">
                <div class="col-md-12">
                    <div class="row profile-account">
                        <div class="col-md-3">
                        <form action="usuario_completo.php" method="get">
                            <div style=" width:200px; padding-left:3px; " >
                                <input type="text" class="busca" id="caja_busqueda" name="clave" onkeydown="search_users()" /><br />
                            </div> 
                            <div id="display"></div>
                        </form><p>
                            <ul class="ver-inline-menu tabbable margin-bottom-10">                                
                                @foreach($chats as $chat)
                                <?php
                                $chat_id = $chat->chat_id;
                                $sql = "SELECT * FROM `messages` WHERE chat_id = '$chat_id' order by id desc limit 1";
                                $messages = DB::select($sql);
                                ?>
                                @foreach($messages as $message)
                                <?php
                                $user = Users::find($message->users_id);
                                $user_id = $user->id;
                                if ($user_id == Auth::user()->id) {
                                    $user = Users::find($message->users_id1);
                                }
                                ?>
                                <li id="chat_{{$chat->chat_id}}" onclick="conversacion({{ $chat->chat_id }})">
                                    <a data-toggle="tab" href="#tab_1-1"><i><img class="avatar img-responsive" alt="" src="{{ $user->img_min }}" /></i>{{ $user->name." ".$user->last_name }}</a>
                                </li>
                                @endforeach
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <div class="tab-content">
                                <div id="tab_1-1" class="tab-pane active" >
                                    <div class="portlet" id="nuevo_chat">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-comments"></i>Conversacion</div>
                                        </div>
                                        <div class="portlet-body" id="chats">
                                            <div class="scroller" style="height: 500px;" data-always-visible="10" data-rail-visible1="10" id="chat_messages">
                                                    <h2 style="text-align: center;">Seleccione una conversacion</h2>
                                                    <img width="50px" src="assets/img/cargando.gif" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px;">
                                            </div>                                            
                                            <div class="chat-form">
                                                <div class="input-cont">
                                                    <input class="form-control" id="mensaje_nuevo" type="text" value="" placeholder="Escribe tu mensaje aquí..." />
                                                </div>
                                                <div class="btn-cont" onclick="enviarmsj()"> 
                                                    <span class="arrow"></span>
                                                    <a class="btn btn-primary icn-only"><i class="icon-ok icon-white"></i></a>
                                                </div>
                                            </div>

                                        </div>
                                    </div>                                    


                                </div>

                            </div>
                        </div>
                        <!--end col-md-9-->


                    </div>
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
    <input type="hidden" id="id_chat" value="">
    <input id="img_min_chat" type="hidden" value="{{ Auth::user()->img_min }}">
    <input id="nombre_chat" type="hidden" value="{{ Auth::user()->name." ".Auth::user()->last_name }}">
    <script>                        
        
        function nuevaConversacion() {
        $("#id_chat").val(id);
        $.ajax({
        type: "GET",
                url: "conversacion",
                data: { id: id}
        })
                .done(function(data) {
                $("#chat_messages").html(data);
                        var cont = $('#chats');
                        var list = $('.chats', cont);
                        $('.scroller', cont).slimScroll({
                scrollTo: list.height()
                });
                        if (parseInt(data) == 2){
                alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
                }
                });
        } 
        
        function conversacion(id) {
            $("#id_chat").val(id);
            $.ajax({
            type: "GET",
                    url: "conversacion",
                    data: { id: id}
            })
            .done(function(data) {
            $("#chat_messages").html(data);
                    var cont = $('#chats');
                    var list = $('.chats', cont);
                    $('.scroller', cont).slimScroll({
            scrollTo: list.height()
            });
                    if (parseInt(data) == 2){
            alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
            }
            });
        
        }
        function create_new_chat(id) {
            $("#id_chat").val("0");
            $.ajax({
            type: "GET",
                    url: "conversacion_new_chat",
                    data: { id: id}
            })
            .done(function(data) {
                $("#chat_messages").html(data);                
                $("#display").css("display", "none");
                var cont = $('#chats');
                var list = $('.chats', cont);
                $('.scroller', cont).slimScroll({
                scrollTo: list.height()                
            });            
            if (parseInt(data) == 2){
                alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
            }
            });
        
        }

        function search_users() {
            var texto = $("#caja_busqueda").val();
            
                $.ajax({
                        type: "GET",
                        url: "search_users",
                        data: { palabra: texto}
                })
                .done(function(data) {
                
                if (parseInt(data) == 2){
                    $("#display").css("display", "none");
                }else{
                    $("#display").css("display", "block");
                    $("#display").html(data);
                }
                });
        }

        function enviarmsj() {
            event.preventDefault();
            var cont = $('#chats');
            var list = $('.chats', cont);
            var mensaje = $("#mensaje_nuevo").val();
            var id = $("#id_chat").val();
            var id_user = $("#id_user_caht").val();
            $.ajax({
            type: "GET",
                    url: "nuevomsj",
                    data: { id: id, mensaje: mensaje, id_user_chat : id_user}
            })
            .done(function(data) {
                $("#id_chat").val(data[3]);
            if (data != 0){

            var nuevomsj = '<li class="out">\n\
                            <img class="avatar img-responsive" alt="" src="' + $("#img_min_chat").val() + '" />\n\
                            <div class="message"><span class="arrow"></span><a href="#" class="name">' + $("#nombre_chat").val() + ' </a><span class="datetime"> at '+data[2]+'</span>\n\
                            <span class="body">' + mensaje + '</span></div></li>';
            
                $(".chats").append(nuevomsj);
                $("#mensaje_nuevo").val("");
                $('.scroller', cont).slimScroll({
                scrollTo: list.height()

            });

                if(id==0){
                    $(".chats").attr("id", "chat_int_"+data[3]);
                    var nuevomsj2 = '<li id="chat_'+data[3]+'" onclick="conversacion('+data[3]+')">\n\
                                            <a data-toggle="tab" href="#tab_1-1"><i><img class="avatar img-responsive" alt="" src="' + data[4] + '" /></i>' + data[5] + '</a>\n\
                                            </li>';

                    $(".ver-inline-menu").prepend(nuevomsj2);
                    
                    setTimeout(function(){
                        $("#chat_"+data[3]).click();
                        $("#chat_"+data[3]).addClass("active");
                    }, 500);
                    
                    var type_event = ["nuevaconv", data[0], data[1], $("#img_min_chat").val(), $("#nombre_chat").val()];
                }else{        
                var type_event = ["nuevochat", data[0], data[1], $("#img_min_chat").val(), $("#nombre_chat").val()];
                }
                send( type_event);
            } else{
                alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
            }
            });
        }
        
        //Bind keypress event to textbox
            
               
    </script>

    <style type="text/css">
    #caja_busqueda /*estilos para la caja principal de busqueda*/
    {
    width:300px;
    height:25px;
    border:solid 2px #979DAE;
    font-size:16px;
    }
    #display /*estilos para la caja principal en donde se puestran los resultados de la busqueda en forma de lista*/
    {
    width:300px;
    position:fixed;
    display:none;
    background:white;
    overflow:hidden;
    z-index:10;
    border: solid 1px #666;
    }
    .display_box /*estilos para cada caja unitaria de cada usuario que se muestra*/
    {
    padding:2px;
    padding-left:6px; 
    font-size:18px;
    height:63px;
    text-decoration:none;
    color:#3b5999; 
    }

    .display_box:hover /*estilos para cada caja unitaria de cada usuario que se muestra. cuando el mause se pocisiona sobre el area*/
    {
    background: #7f93bc;
    color: #FFF;
    }
    .desc
    {
    color:#666;
    font-size:16;
    }
    .desc:hover
    {
    color:#FFF;
    }

    /* Easy Tooltip */
    </style>
     <script type="text/javascript">
    // $(document).ready(function(){

    // function() //se crea la funcioin keyup
    // {
    // var texto = $(this).val();//se recupera el valor de la caja de texto y se guarda en la variable texto
    // var dataString = 'palabra='+ texto;//se guarda en una variable nueva para posteriormente pasarla a search.php
    // if(texto==''){//si no tiene ningun valor la caja de texto no realiza ninguna accion
    
    // }else{
    //     $.ajax({//metodo ajax
    //         type: "get",//aqui puede  ser get o post
    //         url: "search_users",//la url adonde se va a mandar la cadena a buscar
    //         data: dataString,
    //         cache: false,
    //         success: function(html){//funcion que se activa al recibir un dato
            
    //             $("#display").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
    //         }
    //     });
    // }return false;    
    // });
    // });
    // jQuery(function($){//funcion jquery que muestra el mensaje "Buscar amigos..." en la caja de texto
    //    $("#caja_busqueda").Watermark("Buscar amigos...");
    //    });
    // </script>
<script language="JavaScript" src="assets/busc/jquery-1.5.1.min.js"></script>
<script language="JavaScript" src="assets/busc/jquery.watermarkinput.js"></script>