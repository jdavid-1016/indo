
            <ul class="chats" id="chat_int_{{ $mensajes[0]->chat_id }}">
                @foreach($mensajes as $msj)
                <?php
                $class = "";
                $user = Users::find($msj->users_id);
                $user_id = $user->id;
                if ($user_id == Auth::user()->id) {
                    $class="out";                    
                }else{
                    $class="in";                    
                }
                ?>
                <li class="{{ $class }}">
                    <img class="avatar img-responsive" alt="" src="{{ $user->img_min }}" />
                    <div class="message">
                        <span class="arrow"></span>
                        <a href="#" class="name">{{ $user->name." ".$user->last_name }}</a>
                        <span class="datetime">at <?php echo date("M d, Y H:i",strtotime($msj->created_at)); ?></span>
                        <span class="body">
                            {{ $msj->message }}
                        </span>
                    </div>
                </li>
               @endforeach

            </ul>