<link href="{{  URL::to("assets/css/pages/about-us.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::to("assets/css/pages/portfolio.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">  

    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        {{$proceso[0]->proceso}}
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Intranet</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> {{$proceso[0]->proceso}}</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-file-text"></i>PROCESO {{$proceso[0]->acronyms}}
                            </div>
                        </div>
                        <?php
                        if(Auth::user()->id==194){
                        $permisos = 1;
                        }
                        ?>
                        <div class="portlet-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#tab_1_1" data-toggle="tab">Información General</a></li>
                                <li><a id="documentacion" href="#tab_1_2" data-toggle="tab" dir="{{$proceso[0]->id_proceso}}">Documentación</a></li>                                
                                @if($permisos==1)
                                <li><a id="gestion" href="#tab_1_3" data-toggle="tab" dir="{{$proceso[0]->id_proceso}}">Gestion de archivos</a></li>                                
                                @endif
                                @if($imagenes)
                                <li><a id="gestion" href="#tab_1_4" data-toggle="tab" dir="{{$proceso[0]->id_proceso}}">Galeria</a></li>
                                @endif
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">

                                    <div class="row margin-bottom-30">
                                        @foreach ($proceso as $row)
                                        @if($row->responsible==1)
                                        <div class="col-md-4">
                                            <div class="meet-our-team">
                                                <h3>{{ $row->name." ".$row->last_name." ".$row->last_name2 }}       <small><b>{{ $row->profile }}</b></small></h3>
                                                <img src="{{ URL::to($row->img_profile)}}" alt="" class="img-responsive"/>
                                                <div class="team-info">
                                                    <p><b>Ext.</b> {{ $row->extension }} <br /><b>Email:</b> {{ $row->email_institutional }}</p>
                                                    <ul class="social-icons social-icons-color pull-right">
                                                        @if($row->twitter)
                                                        <li><a href="{{ $row->twitter }}" target="_blank" data-original-title="twitter" class="twitter"></a></li>
                                                        @endif
                                                        @if($row->facebook)
                                                        <li><a href="{{ $row->facebook }}" target="_blank" data-original-title="facebook" class="facebook"></a></li>
                                                        @endif
                                                        @if($row->linked)
                                                        <li><a href="{{ $row->linked }}" target="_blank" data-original-title="linkedin" class="linkedin"></a></li>
                                                        @endif
                                                        @if($row->google)
                                                        <li><a href="{{ $row->google }}" target="_blank" data-original-title="Goole Plus" class="googleplus"></a></li>
                                                        @endif
                                                        @if($row->skype)
                                                        <li><a href="https://secure.skype.com/portal/profile/{{ $row->skype }}" target="_blank" data-original-title="skype" class="skype"></a></li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @endforeach
                                        <div class="col-md-6" id="resena">
                                            <div class="scroller" style="height:550px" data-rail-color="red" data-handle-color="black">
                                            <h3>&nbsp;&nbsp;&nbsp;&nbsp;Reseña</h3><br />
                                            <blockquote class="hero" id="descriptionresena">
                                                <p style="text-align:justify"><?php echo $proceso[0]->description; ?></p>
                                            </blockquote>
                                            @if($permisos==1)
                                            <a href="#editarresena" data-toggle="modal" id=""><div id="botoneditar" style="display:none"><button type="button" class="btn btn-info btn-sm">Editar</button></div></a>
                                            @endif
                                            </div>
                                        </div>

                                    </div>
                                    <!--/row-->   
                                    <!-- Meer Our Team -->
                                    <div class="headline">
                                        <h3></h3>
                                    </div>
                                    <div class="portlet">
                                        <div class="portlet-title">
                                            <div class="caption"><i class="icon-group"></i>Integrantes del Proceso</div>
                                        </div>
                                        <div class="portlet-body"> 
                                            <div class="row thumbnails">
                                                <div class="scroller" style="height:500px" data-rail-color="red" data-handle-color="black">
                                                    @foreach ($proceso as $row)
                                                    @if($row->responsible!=1)
                                                    <div class="col-md-3">
                                                        <div class="meet-our-team">
                                                            <h3>{{ $row->name." ".$row->last_name }}      <small><b>{{ $row->profile }}</b></small></h3>
                                                            <img width="376" height="295" src="{{ URL::to($row->img_profile)}}" alt="" class="img-responsive" />
                                                            <div class="team-info">
                                                                <p><b>Ext.</b> {{ $row->extension }} <br /><b>Email:</b> {{ $row->email_institutional }}</p>
                                                                <ul class="social-icons social-icons-color pull-right">
                                                                    @if($row->twitter)
                                                                    <li><a href="{{ $row->twitter }}" target="_blank" data-original-title="twitter" class="twitter"></a></li>
                                                                    @endif
                                                                    @if($row->facebook)
                                                                    <li><a href="{{ $row->facebook }}" target="_blank" data-original-title="facebook" class="facebook"></a></li>
                                                                    @endif
                                                                    @if($row->linked)
                                                                    <li><a href="{{ $row->linked }}" target="_blank" data-original-title="linkedin" class="linkedin"></a></li>
                                                                    @endif
                                                                    @if($row->google)
                                                                    <li><a href="{{ $row->google }}" target="_blank" data-original-title="Goole Plus" class="googleplus"></a></li>
                                                                    @endif
                                                                    @if($row->skype)
                                                                    <li><a href="https://secure.skype.com/portal/profile/{{ $row->skype }}" target="_blank" data-original-title="skype" class="skype"></a></li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endif
                                                    @endforeach 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab-pane" id="tab_1_2">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="portlet" id="docnav">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-list-ul"></i>Navegación</div>
                                                </div>

                                                <div class="portlet-body">
                                                    <span id="listdocument" class="clicked">
                                                        <ul>
                                                            @foreach($carpetas as $car)
                                                            <li dir="{{$car->name_folder}}" id="{{ $car->id }}" class="folder"><i class="icon-folder-close"></i>{{ $car->name }}
                                                            </li>
                                                            <div id="ad{{ $car->name_folder }}">
                                                            </div>
                                                            @endforeach
                                                        </ul>
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-md-7 ">
                                            <div class="portlet">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-file"></i>Documento</div>
                                                </div>
                                                <div class="portlet-body" id="docview">
                                                    <embed id="doc-container" src="">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                @if($permisos==1)
                                <div class="tab-pane" id="tab_1_3">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="portlet" id="docnav">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-list-ul"></i>Crear nuevo archivo</div>
                                                </div>

                                                <div class="portlet-body">
                                                    
                                                    {{Form::open( array('url'=>'guardarArc', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}

                                                    <div class="row">
                                                    <div class="col-md-5">
                                                                <label>Nombre</label>
                                                                    <input class="form-control" type="text" name="nombre" id="nombre">
                                                                <label>Seleccione los archivos</label>
                                                                {{Form::file('arc')}}
                                                            </div></div><br/>
                                                            <div class="row">
                                                            <div class="col-md-5">
                                                                 <label class="control-label">Carpeta</label>                                                                
                                                                    <select  class="form-control input-medium select2me" name="carpetas" id="carpetas" data-placeholder="Select...">
                                                                    @foreach($carpetas as $car)
                                                                    <option value="{{ $car->id }}">{{$car->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                                 </div>
                                                                </div><br />
                                                                 <div class="row">
                                                                     <div class="col-md-5">
                                                                    <span class="help-block">
                                                                    <a href="#gestiondoc" data-toggle="modal" class="btn btn-success"><i class="icon-plus"></i> Nueva carpeta</a>
                                                               </span>
                                                                     </div>
                                                                     </div>
                                                            
                                                            <?php
                                                    $url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                                    ?>
                                                    <input type="hidden" name="url" id="url" value="<?php echo $url; ?>">
                                                    <br /><button type="submit" class="btn btn-info btn-block">Subir</button>
                                                {{Form::close()}}
                                                </div>
                                                    
                                                </div>

                                            </div>
                                        <div class="col-md-7">
                                            <div class="portlet">
                                                <div class="portlet-title">
                                                    <div class="caption"><i class="icon-file"></i>Documento</div>
                                                </div>
                                                <div class="portlet-body" id="docview">
                                                    <embed id="doc-container" src="{{Input::get('embed')}}">
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        
                                    </div>
                                @endif
                                
                                
             <div class="tab-pane" id="tab_1_4">
                        <!-- BEGIN FILTER -->                                                              
                  <div class="tab-content">
                        <!-- BEGIN FILTER -->           
                        <div class="margin-top-10">
                           <ul class="mix-filter">
                              <li class="filter btn" data-filter="all">Todas</li>
                              @foreach($galerias as $gal)
                              <li class="filter btn" data-filter="{{ $gal->id }}">{{ $gal->name }}</li>
                              @endforeach
                           </ul>
                           <div class="row mix-grid">
                               @foreach($imagenes as $img)
                              <div class="col-md-3 col-sm-4 mix {{ $img->gallery_id }}">
                                 <div class="mix-inner">
                                    <img class="img-responsive" style="width: 384px; height: 282px;" src="{{ URL::to("assets/files/".$proceso[0]->acronyms."/thumb/".$img->name_gallery."/".$img->name_image."")}}" alt="">
                                    <div class="mix-details">
                                       <h4>Proceso {{ $proceso[0]->acronyms }}</h4>                                       
                                       <a class="mix-link" id="img-{{ $img->id }}" onclick="likeImage({{ $img->id }})"><i class="icon-thumbs-up-alt"> {{ $img->likes }}</i></a>
                                       <a class="mix-preview fancybox-button" href="{{ URL::to("assets/files/".$proceso[0]->acronyms."/images/".$img->name_gallery."/".$img->name_image."")}}" title="{{$img->name}}" data-rel="fancybox-button"><i class="icon-search"></i></a>
                                    </div>
                                 </div>
                              </div>
                               @endforeach
                           </div>
                        </div>
                        <!-- END FILTER -->
                  </div>                           
                        <!-- END FILTER -->    
                     </div>                                

                                </div> 
                                <!--/thumbnails-->
                                <!-- //End Meer Our Team -->        
                                <!-- END PAGE CONTENT-->
                            </div>
                            <!-- END PAGE -->    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @if($permisos==1)
        <div class="modal fade" id="editarresena" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog modal-wide">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Edicion Reseña</h4>
                    </div>
                    <div class="modal-body">
                        <textarea class="ckeditor form-control" id="editor" name="editor" rows="6"><?php echo $proceso[0]->description; ?></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="guardarResena(<?php echo $proceso[0]->id_proceso ?>)">Guardar Cambios</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <div class="modal fade" id="gestiondoc" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Nueva carpeta</h4>
                    </div>
                    <div class="modal-body">
                        <label class="control-label col-md-3">Nombre:</label>
                        <input class="form-control input-medium" type="text" name="carpeta" id="carpeta">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="crearDir('<?php echo $proceso[0]->id_proceso; ?>', '<?php echo $proceso[0]->acronyms; ?>')">Crear</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        @endif
    </div>
    <!-- END CONTAINER --> 



@if(Auth::user()->tutorial==0)
   <div class="opacidad">
        <div class="padre" id="padre">
          <div class="alert alert-block alert-info fade in tuto" style="display: none;"id="paso2">                        
             <h3 style="text-align:center;" class="alert-heading"><b>Galería de Imagenes de los procesos</b></h3>
             <p style="background-color:#F2F2F2; padding:5px; border-radius:5px">
                Te presentamos la nueva galería de imagenes de cada uno de los procesos.
             </p>
             <p>
                <img style="margin-left: auto; margin-right: auto; display:block;padding-top:10px" src="{{ URL::to("assets/img/tutorial/galeria.png") }}">
             </p>
             <p>
                <a class="btn btn-info" href="#" onclick="Siguiente(2)">Siguiente</a>
             </p>
          </div>

          <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso3">                        
             <h3 style="text-align:center;" class="alert-heading"><b>Listado de notificacones</b></h3>
             <p style="background-color:#F2F2F2; padding:5px; border-radius:5px;">
                Nuevo listado de notificaciones que se mostrarán en tiempo real, ademas se resaltaran las que no has visto.
             </p>
             <p>

             <img style="margin-left: auto; margin-right: auto; display:block; padding-top:10px" src="{{ URL::to("assets/img/tutorial/notificaciones.png") }}">
                
             </p>
             <p>
                <a class="btn btn-info" href="#" onclick="Siguiente(3)">Siguiente</a>
             </p>
          </div>

          <div class="alert alert-block alert-info fade in tuto"  id="paso1">                        
             <h3 style="text-align:center;" class="alert-heading"><b>Galería de Imagenes de los procesos</b></h3>
             <p style="background-color:#F2F2F2; padding:5px; border-radius:5px">
                Le presentamos la nueva galería de imágenes de cada uno de los procesos, podrá ver en detalle cada una de ellas y dar like a sus favoritas.
             </p>
             <p>
                <img style="width: 100%; height: 50%; margin-left: auto; margin-right: auto; display:block;padding-top:10px" src="{{ URL::to("assets/img/tutorial/galeria.png") }}">
             </p>
             <p>
                <a class="btn btn-info" href="#" onclick="Siguiente(3)">Siguiente</a>
             </p>
          </div>                         
        </div>
    </div>

    <?php 

        $imagen = Auth::user()->img_min;
        $nombre = Auth::user()->name." ".Auth::user()->last_name;

        ?>
        <input type="hidden" id="img" value="<?php echo $imagen; ?>">
        <input type="hidden" id="name" value="<?php echo $nombre; ?>">
 @endif

    <script>    
        
        
        
                    
          function Siguiente(id){ 

            
            var img = $('#img').val();
            var nombre = $('#name').val();


             switch (id) {
                    case 1:
                    $('#paso1').fadeOut(0);
                    $('#paso2').fadeIn(0);
                    toastr.success('<img alt="" class="top-avatar" src="'+img+'"/> </br>El usuario '+nombre+' Cerro su soporte.', 'Soporte cerrado');
                    //toastr.success('Reseña actualizada correctamente', 'Actualizacion');
                        break;

                    case 2:
                    //$("#header_inbox_bar").addClass("quemas");                
                    //$("#header_inbox_bar").animate({ 'zoom': 1.2 }, 400);
                    
                    $('#icon_notifications').hover();            
                    $('.lista_notificaciones').css({ display: "block"});  
                    $('#paso2').fadeOut(0);
                    $('#paso3').fadeIn(0);
                    break;

                    case 3:
                    //$("#hola").animate({ 'zoom': 1 }, 400);
                    //$("#hola").removeClass("quemas");
                    $('.lista_notificaciones').css({ display: "none"});  
                    $('#paso3').fadeOut(0);
                    $('.opacidad').fadeOut(1000);
                    window.location = "../perfil#tab_1_3";
                    break;
                 }
          }
    </script>