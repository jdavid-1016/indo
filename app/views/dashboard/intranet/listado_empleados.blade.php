    <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Listado de Empleados
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="index.html">Página Principal</a> 
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="#">Intranet</a>
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="#">Información General</a>
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="#">Listado de Empleados</a>
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <div class="caption"><i class="icon-table"></i>Información</div>
                  <div class="tools">
                     <a href="javascript:;" class="collapse"></a>
                     <a href="javascript:;" class="remove"></a>
                  </div>
               </div>
               <div class="portlet-body">
                  <div class="table-responsive">
                     <table class="table table-striped table-hover top-blue">
                        <thead>
                        <tr>
                              <th>Imagen</th>
                              <th>Nombre <a href="/indoamericana/intranet/listaEmpleados/last_name/asc"><i class="icon-sort"></i></a></th>
                              <th>Cargo</th>
                              <th>Proceso <a href="/indoamericana/intranet/listaEmpleados/id_process/asc"><i class="icon-sort"></i></a> </th>
                              <th>Extensión</th>
                              <th>Correo</th>
                              <th>Celular</th>
                           </tr>
                        </thead>
                        <tbody>                         
                        @foreach ($administradores as $row)
                           <tr>
                           <td>                               
                           <a href="#basic" data-toggle="modal" onclick="detalle({{$row->id}})">
                              <img class="profile" src="{{  URL::to($row->img)}}"alt="{{$row->name.' '.$row->last_name.' '.$row->last_name2}}" style=" width: 152px; height: 126px; display: block;" />
                           </a>   
                           </td>
                           <td>{{$row->last_name.' '.$row->last_name2.' '.$row->name.' '.$row->name2}}</td>
                           <td>{{$row->profile}}</td>
                           <td>{{$row->acronyms}}</td>
                           <td>{{$row->extension}}</td>
                           <td>{{$row->email_institutional}}</td>
                           <td>{{$row->cell_phone}}</td>
                           </tr>
                        @endforeach
                           <tr>
                              <td></td>
                              <td>PORTERÍA</td>
                              <td>PORTERÍA</td>
                              <td></td>
                              <td>1026</td>
                              <td></td>
                              <td></td>   
                           </tr>
                           <tr>
                              <td></td>
                              <td>SALA DE PROFESORES</td>
                              <td>SALA DE PROFESORES</td>
                              <td></td>
                              <td>1029</td>
                              <td></td>
                              <td></td>   
                           </tr>
                           <tr>
                              <td></td>
                              <td>CAFETERIA</td>
                              <td>CAFETERIA</td>
                              <td></td>
                              <td>1030</td>
                              <td></td>
                              <td></td>   
                           </tr>
                           <tr>
                              <td></td>
                              <td>HELM</td>
                              <td>HELM</td>
                              <td></td>
                              <td>2020</td>
                              <td></td>
                              <td></td>   
                           </tr>
                           <tr>
                              <td></td>
                              <td>FINCOMERCIO</td>
                              <td>FINCOMERCIO</td>
                              <td></td>
                              <td>2026</td>
                              <td></td>
                              <td></td>   
                           </tr>
                           <tr>
                              <td></td>
                              <td>FAX</td>
                              <td>FAX</td>
                              <td></td>
                              <td>1003</td>
                              <td></td>
                              <td></td>   
                           </tr>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <div class="caption"><i class="icon-envelope"></i>Lista de Correos</div>
                  <div class="tools">
                     <a href="javascript:;" class="collapse"></a>
                     <a href="javascript:;" class="remove"></a>
                  </div>
               </div>
               <div class="portlet-body">
                  <div class="table-responsive">
                     @foreach ($administradores as $row)
                              {{$row->email_institutional.'; '}}
                     @endforeach
                  </div>
               </div>
            </div>
         </div>
      </div>
     
   </div>
</div>

            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                       
                        <!-- /.modal-dialog -->
                     </div>
<script>
    function detalle(id){
        
        $.post('callUserProfile', {
                  id:id
                } ,

                function(response){
                    $('#basic').html(response);
                    //var datos = JSON.parse(response);
                });
    }
</script>