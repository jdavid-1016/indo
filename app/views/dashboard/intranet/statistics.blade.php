<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>A date range picker for Bootstrap</title>
        <!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> -->
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" media="all" href="assets/calendario/daterangepicker-bs3.css" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="assets/calendario/moment.js"></script>
        <script type="text/javascript" src="assets/calendario/daterangepicker.js"></script>
    </head>
    <body>	
        <div class="page-content">
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Estadísticas
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i>
                            <a href="javascript:;">Página Principal</a> 
                            <i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="javascript:;">Help Desk</a>
                            <i class="icon-angle-right"></i> 
                        </li>
                        <li>
                            <a href="javascript:;">Estadísticas</a> 
                        </li>
                        <li class="pull-right">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                        </li>
                    </ul>                            				

                    <script type="text/javascript">
                        $(document).ready(function () {

                            var cb = function (start, end, label) {
                                console.log(start.toISOString(), end.toISOString(), label);
                                var fecha_inicio = start.format('YYYY-M-D');
                                var fecha_fin = end.format('YYYY-M-D');
                                //alert(fecha_inicio);
                                //alert(fecha_fin);

//                                $.ajax({
//                                    type: "GET",
//                                    url: "changedate",
//                                    data: {fecha_inicio: fecha_inicio, fecha_fin: fecha_fin},
//                                })
//                                        .done(function (data) {
//                                            alert(data);
//                                        });


                                document.getElementById("grafica").src = "changedate?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica2").src = "changedatefiles?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
                                document.getElementById("grafica3").src = "changedateprocess?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";

                                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
                            }

                            var optionSet1 = {
                                startDate: moment().subtract(29, 'days'),
                                endDate: moment(),
                                minDate: '01/01/2012',
                                maxDate: '12/31/2015',
                                dateLimit: {days: 700},
                                showDropdowns: true,
                                showWeekNumbers: true,
                                timePicker: false,
                                timePickerIncrement: 1,
                                timePicker12Hour: true,
                                ranges: {
                                    'Hoy': [moment(), moment()],
                                    'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                                    'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                                    'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                                    'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                },
                                opens: 'left',
                                buttonClasses: ['btn btn-default'],
                                applyClass: 'btn-small btn-primary',
                                cancelClass: 'btn-small',
                                format: 'MM/DD/YYYY',
                                separator: ' to ',
                                locale: {
                                    applyLabel: 'Aplicar',
                                    cancelLabel: 'Clear',
                                    fromLabel: 'From',
                                    toLabel: 'To',
                                    customRangeLabel: 'Personalizada',
                                    daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                                    monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                                    firstDay: 1
                                }
                            };

                            var optionSet2 = {
                                startDate: moment().subtract(7, 'days'),
                                endDate: moment(),
                                opens: 'left',
                                ranges: {
                                    'Today': [moment(), moment()],
                                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                                }
                            };

                            $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

                            $('#reportrange').daterangepicker(optionSet1, cb);

                            $('#reportrange').on('show.daterangepicker', function () {
                                console.log("show event fired");
                            });
                            $('#reportrange').on('hide.daterangepicker', function () {
                                console.log("hide event fired");
                            });
                            $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
                                console.log("apply event fired, start/end dates are "
                                        + picker.startDate.format('MMMM D, YYYY')
                                        + " to "
                                        + picker.endDate.format('MMMM D, YYYY')
                                        );
                            });
                            $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
                                console.log("cancel event fired");
                            });

                            $('#options1').click(function () {
                                $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
                            });

                            $('#options2').click(function () {
                                $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
                            });

                            $('#destroy').click(function () {
                                $('#reportrange').data('daterangepicker').remove();
                            });

                        });
                    </script>

                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Ingreso de Usuarios</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportar(1)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica" width="100%" height="500px" src="graphic_ingresos">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Documentos mas vistos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportar(2)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica2" width="100%" height="500px" src="graphic_files">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>
                
                <div class="col-md-4 col-sm-12">
                    <div class="portlet">
                        <div class="portlet-title">
                            <div class="caption"><i class="icon-bar-chart"></i>Procesos mas vistos</div>
                            <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-success btn-sm" onclick="exportar(3)">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        <input type="radio" name="exportar" id="exportar"  class="toggle">Exportar
                                   </label>
                                </div>
                            </div>
                            <div class="tools">
                            </div>
                        </div>
                        <div class="portlet-body" >
                           <div class="table-responsive">
                                <div id="pun_promedio"></div>

                                <img id="grafica3" width="100%" height="500px" src="graphic_process">
                                <!--<img width="100%" height="100%" src="changedate?fecha_inicio=2014-10-15&fecha_fin=2014-10-17">-->

                            </div>   
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>