<div class="modal-dialog">
   <div class="modal-content">                             
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h3 class="modal-title">{{ $data[0]->nombre.' '.$data[0]->nombre2.' '.$data[0]->apellido.' '.$data[0]->apellido2 }}</h3>
      </div>
      <div class="modal-body form-body">
         <div class="row">
            <div class="col-md-5">
               <img src="{{  URL::to($data[0]->img_profile)}}" class="img-responsive img-profile" alt="">
            </div>
            <div class="col-md-7">
              <h3>{{ $data[0]->nombre }}</h3>
              <br/>
              <h4><i class="icon-headphones"></i> {{ $data[0]->extencion }}</h4>
              <h4><i class="icon-envelope"></i> {{ $data[0]->correo_institucional }}</h4>
              <h4><i class="icon-phone"></i> {{ $data[0]->celular }}</h4>
            </div>      
         </div>   
      </div>
   </div>
</div>