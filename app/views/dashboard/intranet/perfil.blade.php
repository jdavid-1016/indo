<link href="{{  URL::to("assets/css/pages/profile.css")}}" rel="stylesheet" type="text/css"/>
<!-- BEGIN BODY -->
<body class="page-header-fixed">
    <!-- BEGIN CONTAINER -->   
    <div class="page-container">
        <!-- BEGIN PAGE -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <h3 class="page-title">
                        Mi Perfil
                    </h3>
                    <ul class="page-breadcrumb breadcrumb">
                        <li>
                            <i class="icon-home"></i><a href="javascript:;" id="br_home">Página Principal</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#">Intranet</a><i class="icon-angle-right"></i>
                        </li>
                        <li>
                            <a href="#"> Mi Perfil</a>
                        </li>
                    </ul>
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <!-- BEGIN PAGE CONTENT-->
            <div class="row profile">
                <div class="col-md-12">
                    <!--BEGIN TABS-->
                    <div class="tabbable tabbable-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab_1_1" data-toggle="tab">Perfil</a></li>
                            <li><a href="#tab_1_3" data-toggle="tab">Editar Perfil</a></li> 
                            <li><a href="#tab_1_4" data-toggle="tab">Mi Equipo</a></li>                     
                            <li><a href="#tab_1_5" data-toggle="tab">Mis Soportes</a></li>                    
                        </ul>                        
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="row">
                                    <div class="col-md-3">
                                        <ul class="list-unstyled profile-nav">
                                            <li><img src="{{ URL::to($perfiles[0]->img_profile)}}" class="img-responsive" alt="" />                                     
                                            </li>
                                            <li><a href="proceso/{{ $perfiles[0]-> id_proceso}}">{{ $perfiles[0]-> proceso}}</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="row">
                                            <div class="col-md-7 profile-info">
                                                <h1>{{ $perfiles[0]-> name." ".$perfiles[0]-> name2." ".$perfiles[0]-> last_name." ".$perfiles[0]-> last_name2}}</h1>
                                                <ul class="list-unstyled">
                                                    <li><i class="icon-briefcase"></i> {{ $perfiles[0]-> profile}}</li>
                                                    <li><i class="icon-headphones"></i>{{ Auth::user()->cell_phone }}</li>
                                                    <li><i class="icon-home"></i>{{ Auth::user()->phone }}</li>
                                                    <li><i class="icon-map-marker"></i>{{ Auth::user()->address }}</li>
                                                    <li><i class="icon-envelope"></i>{{ Auth::user()->email }}</li>
                                                </ul>
                                            </div>
                                            <!--end col-md-8-->
                                            <div class="col-md-5">
                                                <div class="portlet sale-summary">
                                                    <div class="portlet-title">
                                                        <div class="caption">Informacion Laboral</div>
                                                    </div>
                                                    <div class="portlet-body">
                                                        <ul class="list-unstyled">
                                                            <li>
                                                                <span class="sale-info">Extension <i class="icon-headphones"></i></span> 
                                                                <span class="sale-num">{{ $perfiles[0]-> extension}}</span>
                                                            </li>
                                                            <li>
                                                                <span class="sale-info">Email <i class="icon-envelope"></i></span>
                                                                <span class="sale-num">{{ Auth::user()->email_institutional }}</span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--end col-md-4-->
                                        </div>
                                        <!--end row-->                                        
                                    </div>
                                </div>
                            </div>
                            <!--tab_1_2-->
                            <div class="tab-pane" id="tab_1_3">
                                <div class="row profile-account">
                                    <div class="col-md-3">
                                        <ul class="ver-inline-menu tabbable margin-bottom-10">
                                            <li class="active">
                                                <a data-toggle="tab" href="#tab_1-1">
                                                    <i class="icon-user"></i> 
                                                    Información personal
                                                </a> 
                                                <span class="after"></span>                                    
                                            </li>                                 
                                            <li ><a id="redestuto" data-toggle="tab" href="#tab_2-2"><i class="icon-thumbs-up"></i> Redes Sociales</a></li>
                                            <li ><a id="passtuto" data-toggle="tab" href="#tab_3-3"><i class="icon-lock"></i> Cambiar contraseña</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="tab-content">
                                            <div id="tab_1-1" class="tab-pane active" >
                                                <div class="form-body">
                                                    <form>

                                                        <div class="col-md-5">
                                                            <div class="form-group"> 
                                                                <label class="control-label">Primer Nombre</label>
                                                                <div class="input-group">
                                                                    <input name="name" class="form-control" type="text" value="{{ $perfiles[0]-> name}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Segundo Nombre</label>
                                                                <div class="input-group">
                                                                    <input name="sname" class="form-control" type="text" value="{{ $perfiles[0]-> name2}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5">
                                                            <div class="form-group">
                                                                <label class="control-label">Primer Apellido</label>
                                                                <div class="input-group">
                                                                    <input name="last_name" class="form-control" type="text" value="{{ $perfiles[0]-> last_name}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Segundo Apellido</label>
                                                                <div class="input-group">
                                                                    <input name="slast_name" class="form-control" type="text" value="{{ $perfiles[0]-> last_name2}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5">   
                                                            <div class="form-group">
                                                                <label class="control-label editable">Número Celular</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ $perfiles[0]-> cell_phone}}" class="form-control" name="celular" id="celular">
                                                                    <span class="input-group-addon"><i class="icon-mobile-phone"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label editable">Teléfono residencia</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ Auth::user()->phone }}" class="form-control" name="telefono" id="telefono">
                                                                    <span class="input-group-addon"><i class="icon-phone"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>   

                                                        <div class="col-md-5">   
                                                            <div class="form-group">
                                                                <label class="control-label">Correo electrónico</label>
                                                                <div class="input-group">
                                                                    <input name="email_institutional" class="form-control" type="text" value="{{ $perfiles[0]-> email_institutional}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label editable">Correo electrónico alternativo</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ Auth::user()->email }}" class="form-control" name="email" id="email">
                                                                    <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5"> 
                                                            <div class="form-group">
                                                                <label class="control-label editable">Dirección de residencia</label>
                                                                <div class="input-group">
                                                                    <input type="text" value="{{ Auth::user()->address }}" class="form-control" name="direccion" id="direccion">
                                                                    <span class="input-group-addon"><i class="icon-map-marker"></i></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Cargo</label>
                                                                <div class="input-group">
                                                                    <input name="profile" class="form-control" type="text" value="{{ $perfiles[0]-> profile}}" readonly>
                                                                    <span class="input-group-addon"><i class="icon-briefcase"></i></span>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-5">
                                                            <div class="margiv-top-10">
                                                                <button type="button" onclick="actualizar()" class="btn btn-success">Actualizar</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            
                                            <div id="tab_2-2" class="tab-pane">
                                                <form id="formredes">
                                                <div class="form-group">
                                                    <label class="control-label">LinkedIn</label>
                                                    <div class="input-icon">
                                                            <i class=" icon-linkedin"></i>
                                                            <input type="text" value="{{ Auth::user()->linked }}" placeholder="https://co.linkedin.com/" class="form-control" name="linked" id="linked">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Google Plus</label>
                                                    <div class="input-icon">
                                                            <i class=" icon-google-plus"></i>
                                                            <input type="text" value="{{ Auth::user()->google }}" placeholder="https://plus.google.com/" class="form-control" name="google" id="google">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Facebook</label>
                                                    <div class="input-icon">
                                                            <i class="icon-facebook"></i>
                                                            <input type="text" value="{{ Auth::user()->facebook }}" placeholder="https://www.facebook.com/" class="form-control" name="facebook" id="facebook">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Twitter</label>
                                                    <div class="input-icon">
                                                            <i class="icon-twitter"></i>
                                                            <input type="text" value="{{ Auth::user()->twitter }}" placeholder="https://twitter.com/" class="form-control" name="twitter" id="twitter">
                                                    </div>
                                                </div>
                                                    <div class="form-group">
                                                    <label class="control-label">Skype</label>
                                                    <div class="input-icon">
                                                            <i class="icon-skype"></i>
                                                            <input type="text" value="{{ Auth::user()->skype }}" placeholder="Usuario" class="form-control" name="skype" id="skype">
                                                    </div>                                                    
                                                </div>
                                                <div class="margin-top-10">
                                                    <a class="btn btn-success" onclick="redes()">Guardar</a>
                                                </div>
                                                    </form>

                                            </div>
                                            
                                            <div id="tab_3-3" class="tab-pane">
                                                <form id="formpass">
                                                <div class="form-group">
                                                    <label class="control-label">Contraseña actual</label>
                                                    <input type="password" class="form-control" id="actual" required/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Nueva contraseña</label>
                                                    <input type="password" class="form-control" id="nueva" required/>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Re-ingresar nueva contraseña</label>
                                                    <input type="password" class="form-control" id="renueva" required/>
                                                </div>
                                                <div class="margin-top-10">
                                                    <a class="btn btn-success" onclick="cambiarpass()">Cambiar Contraseña</a>
                                                </div>
                                                    </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!--end col-md-9-->
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_1_4">
                                <div class="row profile-account">
                                    <div class="col-md-12">
                                        <div class="portlet">
                                           <div class="portlet-title">
                                             <div class="caption">Información General</div>
                                             <div class="tools">
                                               <a href="javascript:;" class="expand"></a>
                                             </div>
                                           </div>
                                           <div class="portlet-body">
                                             <div class="row">
                                               <div class="col-md-6">
                                                 <input type="hidden" value="{{$equipos[0]->id_user}}" name="tipo-equipo" id="id_equipo_edit">
                                                 
                                                 @foreach($equipos as $equipo)
                                                 @if($equipo->equipment_types_id == 7)
                                                 <table class="table middle well">
                                                   <tbody>
                                                     <tr>
                                                        <td>Tipo de Equipo </td>
                                                        <td>
                                                         {{$equipo->type}}
                                                        </td>
                                                     </tr>
                                                     <tr>
                                                       <td>Imagen</td>
                                                       <td>
                                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                                           <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                @if($equipo->image == "")
                                                                    <img src="assets/img/equipos/-1.png" alt="">
                                                                @else
                                                                    <img src="{{$equipo->image}}" alt="">
                                                                @endif
                                                           </div>
                                                           <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                           
                                                           
                                                         </div>
                                                       </td>
                                                     </tr>
                                                   </tbody>
                                                 </table>
                                                 @endif
                                                 @endforeach
                                               </div>
                                               <div class="col-md-6">
                                                @foreach($equipos as $equipo)
                                                @if($equipo->equipment_types_id == 7)
                                                 <table class="table middle well">
                                                   <tbody>  
                                                     <tr>
                                                       <td>Marca</td>
                                                       <td><input name="marca" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->mark}}" id="equipo_marca_editar"></td>
                                                     </tr>
                                                     <tr>
                                                       <td>Modelo</td>
                                                       <td><input name="modelo" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->model}}" id="equipo_modelo_editar"></td>
                                                     </tr>
                                                     <tr>
                                                       <td>Serial</td>
                                                       <td><input name="serial" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->serial}}" id="equipo_serial_editar"></td>
                                                     </tr>
                                                     <tr>
                                                       <td>Entrega del Equipo</td>
                                                       <td><input name="entrega" class="tag form-control input-medium clear-border"  value="{{$equipo->date_asign}}" id="equipo_fechaengrega_editar"></td>
                                                     </tr>
                                                     <tr>
                                                       <td>Estado</td>
                                                       <td><input name="estado" class="tag form-control clear-border input-medium" type="text"  value="{{$equipo->status}}" id="equipo_estado_editar"></td>
                                                     </tr>
                                                   </tbody>
                                                 </table>
                                                 @endif
                                                 @endforeach 
                                               </div>
                                             </div>
                                           </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                    <div class="portlet">
                                        <div class="portlet-title">
                                          <div class="caption">Características</div>
                                          <div class="tools">
                                            <a href="javascript:;" class="expand"></a>
                                          </div>
                                        </div>
                                        <div class="portlet-body display-hide">
                                          <div class="row">
                                            <div class="col-md-6">
                                            @foreach($equipos as $equipo)
                                            @if($equipo->equipment_types_id == 7)
                                                <table class="table middle well">
                                                  <tbody>
                                                    <tr>
                                                      <td>IP</td>
                                                      <td><input  name="ip" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->ip}}" id="equipo_ip_editar"></td>
                                                    </tr>
                                                   
                                                    <tr>
                                                      <td>MAC</td>
                                                      <td><input  name="mac" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->mac}}" id="equipo_mac_editar"></td>
                                                    </tr>
                                                    <tr>
                                                      <td>Memoria RAM</td>
                                                      <td>
                                                        <span class="tag">
                                                          <select class="form-control input-medium clear-border" name="ram" id="equipo_ram_editar" >
                                                            <option value="-1">{{$equipo->memory}}</option>
                                                            
                                                          </select>
                                                        </span>
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>Disco Duro</td>
                                                      <td><input  name="disco-duro" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->hard_drive}}" id="equipo_disco_editar"></td>
                                                    </tr>
                                                    <tr>
                                                      <td>Procesador</td>
                                                      <td>
                                                        <input  name="procesador" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->processor}}" id="equipo_procesador_editar">  
                                                      </td>
                                                    </tr>
                                                  </tbody>
                                                </table>
                                            @endif
                                            @endforeach 
                                              </div>
                                              <div class="col-md-6">
                                            @foreach($equipos as $equipo)
                                            @if($equipo->equipment_types_id == 7)
                                                <table class="table middle well">
                                                  <tbody>
                                                    <tr>
                                                      <td>Office</td>
                                                      <td>
                                                        <input  name="licencia-office" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->operating_system}}">
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>Licencia Office</td>
                                                      <td><input  name="licencia-office" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->license_of}}"></td>
                                                    </tr>
                                                    <tr>
                                                      <td>Sistema Operativo</td>
                                                      <td>
                                                        
                                                          <input  name="licencia-office" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->operating_system}}">
                                                        
                                                      </td>
                                                    </tr>
                                                    <tr>
                                                      <td>Versión</td>
                                                      <td><input  name="os-version" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->version_op}}"></td>
                                                    </tr>
                                                    <tr>
                                                      <td>N° de licencia SO</td>
                                                      <td><input  name="os-numero" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->no_license_op}}"></td>
                                                    </tr>
                                                  </tbody>
                                            @endif
                                            @endforeach 
                                                </table>    
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                              <div class="caption">Perifericos</div>
                                              <div class="tools">
                                                <a href="javascript:;" class="expand"></a>
                                              </div>
                                            </div>
                                            <div class="portlet-body display-hide">
                                              <div class="row">
                                                <div class="col-md-12">
                                                    <table class="table middle well">
                                                        
                                                            <tr>
                                                              <th style="text-align:center">Elemento</th>
                                                              <th style="text-align:center">Marca</th>
                                                              <th style="text-align:center">Modelo</th>
                                                              <th style="text-align:center">Serial</th>
                                                            </tr>
                                                        
                                                        <tbody>
                                                @foreach($equipos as $equipo)
                                                @if($equipo->equipment_types_id != 7)
                                                        <tr style="text-align:center">
                                                          <td>{{$equipo->type}}</td>
                                                          <td><input name="ip" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->mark}}" readonly=""></td>
                                                          <td><input name="ip" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->model}}" readonly=""></td>
                                                          <td><input name="ip" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->serial}}" readonly=""></td>
                                                        </tr>
                                                @endif
                                                @endforeach 
                                                      </tbody>
                                                    </table>
                                                  </div>
                                                  
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="tab_1_5">
                                <div class="row profile-account">
                                    <div class="col-md-12">
                                    <div class="tab-pane" id="tab_1_3">
                                       <div class="scroller" id="" style="height: 450px;" data-always-visible="1" data-rail-visible1="1">

                                        <table class="table top-blue" id="admin-support" data-target="soporte/callSupport/"> 
                                           <thead>
                                              <tr>
                                                 <th></th>
                                                 <!-- <th>Ticket</th> -->
                                                 <th>Estado</th>
                                                 <th>Responsable</th>
                                                 <th>Categoría</th>
                                                 <th>Solicitud</th>
                                                 <th>Creado</th>
                                                 <!-- <th>Cerrado</th> -->
                                                 <th>Tiempo</th>
                                                 <th class="td_center">Comentario</th>
                                                 <th class="td_center"><i class="icon-ok"></i></th>
                                              </tr>
                                           </thead>
                                           <tbody id="">
                                           @foreach($supports as $support)
                                           
                                              <tr style="{{$support['display']}}" id="{{$support['ocult']}}" class="center {{$support['row_color']}}">
                                                 <td><i class="{{$support['scaled']}}" id="{{$support['id_plus']}}" onclick="funcion2($(this).attr('id'));return false;"></i></td>
                                                 <!-- <td>{{$support['id']}}</td> -->
                                                 <td><span class="label label-sm {{$support['label']}}">{{$support['state']}}</span></td>
                                                 <td><img alt="" class="top-avatar" src="{{$support['responsible_img']}}" style="border-radius:50px"></td>
                                                 <td>{{$support['category']}}</td>
                                                 <td>{{$support['message']}}</td>
                                                 <td>{{$support['created_at']}}</td>
                                                 <!-- <td>{{$support['closed_at']}}</td> -->
                                                 <td>
                                                    <div style="border-radius:10px; background-color:#fbeed5; padding:5px; color:#c09853;width:80px;">
                                                        {{$support['time']}}
                                                    </div>
                                                 </td>
                                                 <td>{{$support['observation']}}</td>
                                                 @if($support['rating_status'] == 0)
                                                 <td id="cal{{$support['hid']}}" class="td_center"><a class="btn btn-success btn-xs" style="{{$support['display']}}" data-target="#ajax" data-toggle="modal" id="{{$support['hid']}}" onclick='realizarProceso($(this).attr("id"));return false;'><i class="icon-ok"></i> Calificar</a></td>      
                                                 @else
                                                 <td class="td_center"><i class="icon-check"></i></td>             
                                                 @endif
                                              </tr>
                                           @endforeach
                                           </tbody>
                                        </table> 
                                       </div>
                                    </div>
                                    </div>
                                    <!--end col-md-9-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--END TABS-->
                </div>
            </div>
            <!-- END PAGE CONTENT-->
        </div>
        <!-- END PAGE -->
    </div>
    <!-- END CONTAINER -->
   
    @if(Auth::user()->tutorial==0)
  <div class="opacidad">
      <div class="padre" id="padre">
          
          <div class="alert alert-block alert-info fade in inicio" id="paso1">
                        <h3 class="alert-heading" style="text-align:center;"><b>Perfil</b></h3>
                        </br>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           En su perfil podrá cambiar la información básica que será mostrada a los demás usuarios.
                        </p>
                        <p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/perfil.png") }}" style="width: 100%; height: 25%;">
                        </p>
                        </p>
                        </br>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(1)">Siguiente</a>
                        </p>
                     </div>
          
                     <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso50">
                        <h3 class="alert-heading" style="text-align:center;"><b>Redes Sociales</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Aca podras compartir todas tus redes sociales con los demas usuarios.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/redes.png") }}" style="width: 100%; height: 40%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(2)">Siguiente</a>
                        </p>
                     </div>

                     <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso2">
                        <h3 class="alert-heading" style="text-align:center;"><b>Olvido de contraseña</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           En caso de olvidar su contraseña y no poder iniciar sesión, podrá recuperar su cuenta fácilmente.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/olvido.png") }}" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px; width: 70%; height: 50%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(3)">Siguiente</a>
                        </p>
                     </div>
          
                    <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso4">
                        <h3 class="alert-heading" style="text-align:center;"><b>Olvido de contraseña</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Solo será necesario indicar su correo corporativo y una nueva contraseña llegara a su bandeja de entrada.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/olvido2.png") }}" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px; width: 60%; height: 40%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(4)">Siguiente</a>
                        </p>
                     </div>
          
                    <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso5">
                        <h3 class="alert-heading" style="text-align:center;"><b>Cambio de contraseña</b></h3>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px; background-color: #F2F2F2">
                           Para acceder a la aplicación correctamente es indispensable que cambie su contraseña por defecto, por una más segura.
                        </p>
                        <p>
                           <img src="{{ URL::to("assets/img/tutorial/cambio.png") }}" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px; width: 90%; height: 40%;">
                        </p>
                        <p>
                           <a class="btn btn-info" href="#" onclick="Siguiente(5)">Finalizar</a>
                        </p>
                     </div>
   </div>
</div>
    @endif
    <script>
function Siguiente(id){
         switch (id) {
                case 1:                
                $('#paso1').fadeOut(0);
                $('#paso2').fadeIn(0);
                $('#passtuto').click();
                    break;

                case 2:                
                //$('#passtuto').click();
                $('#paso2').fadeOut(0);
                $('#paso3').fadeIn(0);
                break;

                case 3:
                $('#paso2').fadeOut(0);
                $('#paso4').fadeIn(0);
                break;
                
                case 4:
                $('#paso4').fadeOut(0);
                $('#paso5').fadeIn(0);
                break;
                
                case 5:
                    $.ajax({
                            type: "GET",
                            url: "actTutorial",
                            data: { cont:"1"}
                            })
                            .done(function( data ) {
                            if(parseInt(data)==2){
                            alert("Ha ocurrido un error en el sistema, comuniquese con el area de sistemas.");
                            }
                        });
                $('#paso5').fadeOut(0);
                $('.opacidad').fadeOut(1000);
                break;
             }
      }
        </script>