<div class="page-content" ng-app>
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Blog
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="informacion">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Blog</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
        </div>
    </div>
    
     <div class="row">
            <div class="col-md-12 blog-page">
               <div class="row">
                  <div class="col-md-9 col-sm-8 article-block">
                     <h1>Latest Blog</h1>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="assets/img/gallery/image4.jpg" alt="" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#">April 16, 2013</a></li>
                              <li><i class="icon-comments"></i> <a href="#">38 Comments</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i> 
                                 <a href="#">Technology</a> 
                                 <a href="#">Education</a>
                                 <a href="#">Internet</a>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="page_blog_item.html">Hello here will be some recent news..</a></h3>
                           <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                           <a class="btn btn-info" href="page_blog_item.html">
                           Read more 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="assets/img/gallery/image3.jpg" alt="" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#">April 16, 2013</a></li>
                              <li><i class="icon-comments"></i> <a href="#">38 Comments</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i> 
                                 <a href="#">Technology</a> 
                                 <a href="#">Education</a>
                                 <a href="#">Internet</a>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="page_blog_item.html">Hello here will be some recent news..</a></h3>
                           <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                           <a class="btn btn-info" href="page_blog_item.html">
                           Read more 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="assets/img/gallery/image4.jpg" alt="" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#">April 16, 2013</a></li>
                              <li><i class="icon-comments"></i> <a href="#">38 Comments</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i> 
                                 <a href="#">Technology</a> 
                                 <a href="#">Education</a>
                                 <a href="#">Internet</a>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="page_blog_item.html">Hello here will be some recent news..</a></h3>
                           <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                           <a class="btn btn-info" href="page_blog_item.html">
                           Read more 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="assets/img/gallery/image3.jpg" alt="" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#">April 16, 2013</a></li>
                              <li><i class="icon-comments"></i> <a href="#">38 Comments</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i> 
                                 <a href="#">Technology</a> 
                                 <a href="#">Education</a>
                                 <a href="#">Internet</a>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="page_blog_item.html">Hello here will be some recent news..</a></h3>
                           <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat. Pellentesque viverra vehicula sem ut volutpat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                           <a class="btn btn-info" href="page_blog_item.html">
                           Read more 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                        </div>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="assets/img/gallery/image5.jpg" alt="" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#">April 16, 2013</a></li>
                              <li><i class="icon-comments"></i> <a href="#">38 Comments</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i> 
                                 <a href="#">Technology</a> 
                                 <a href="#">Education</a>
                                 <a href="#">Internet</a>
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="page_blog_item.html">Hello here will be some recent news..</a></h3>
                           <p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non libero consectetur adipiscing elit magna. Sed et quam lacus. Fusce condimentum eleifend enim a feugiat.</p>
                           <a class="btn btn-info" href="page_blog_item.html">
                           Read more 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                        </div>
                     </div>
                  </div>
                  <!--end col-md-9-->
                  <div class="col-md-3 col-sm-4 blog-sidebar">
                     <h3>Top Entiries</h3>
                     <div class="top-news">
                        <a href="#" class="btn btn-danger">
                        <span>Conquer News</span>
                        <em>Posted on: April 16, 2013</em>
                        <em>
                        <i class="icon-tags"></i>
                        Money, Business, Google
                        </em>
                        <i class="icon-briefcase top-news-icon"></i>
                        </a>
                        <a href="#" class="btn btn-success">
                        <span>Top Week</span>
                        <em>Posted on: April 15, 2013</em>
                        <em>
                        <i class="icon-tags"></i>
                        Internet, Music, People
                        </em>
                        <i class="icon-music top-news-icon"></i>                             
                        </a>
                        <a href="#" class="btn btn-info">
                        <span>Gold Price Falls</span>
                        <em>Posted on: April 14, 2013</em>
                        <em>
                        <i class="icon-tags"></i>
                        USA, Business, Apple
                        </em>
                        <i class="icon-globe top-news-icon"></i>                             
                        </a>
                        <a href="#" class="btn btn-warning">
                        <span>Study Abroad</span>
                        <em>Posted on: April 13, 2013</em>
                        <em>
                        <i class="icon-tags"></i>
                        Education, Students, Canada
                        </em>
                        <i class="icon-book top-news-icon"></i>                              
                        </a>
                        <a href="#" class="btn btn-info">
                        <span>Top Destinations</span>
                        <em>Posted on: April 12, 2013</em>
                        <em>
                        <i class="icon-tags"></i>
                        Places, Internet, Google Map
                        </em>
                        <i class="icon-bolt top-news-icon"></i>                              
                        </a>
                     </div>
                     <div class="space20"></div>
                     <h3>Flickr</h3>
                     <ul class="list-inline blog-images">
                        <li>
                           <a  class="fancybox-button" data-rel="fancybox-button" title="390 x 220 - keenthemes.com" href="assets/img/blog/1.jpg">
                           <img alt="" src="assets/img/blog/1.jpg">
                           </a>
                        </li>
                        <li><a href="#"><img alt="" src="assets/img/blog/2.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/3.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/4.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/5.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/6.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/8.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/10.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/11.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/1.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/2.jpg"></a></li>
                        <li><a href="#"><img alt="" src="assets/img/blog/7.jpg"></a></li>
                     </ul>
                     <div class="space20"></div>
                     <h3>Tabs</h3>
                     <div class="tabbable tabbable-custom">
                        <ul class="nav nav-tabs">
                           <li class="active"><a data-toggle="tab" href="#tab_1_1">Section 1</a></li>
                           <li ><a data-toggle="tab" href="#tab_1_2">Section 2</a></li>
                        </ul>
                        <div class="tab-content">
                           <div id="tab_1_1" class="tab-pane active">
                              <p>I'm in Section 1.</p>
                              <p>
                                 Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.
                              </p>
                           </div>
                           <div id="tab_1_2" class="tab-pane">
                              <p>Howdy, I'm in Section 2.</p>
                              <p>
                                 Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.
                              </p>
                           </div>
                        </div>
                     </div>
                     <div class="space20"></div>
                     <h3>Recent Twitts</h3>
                     <div class="blog-twitter">
                        <div class="blog-twitter-block">
                           <a href="">@keenthemes</a> 
                           <p>At vero eos et accusamus et iusto odio.</p>
                           <a href="#"><em>http://t.co/sBav7dm</em></a> 
                           <span>2 hours ago</span>
                           <i class="icon-twitter blog-twiiter-icon"></i>
                        </div>
                        <div class="blog-twitter-block">
                           <a href="">@keenthemes</a> 
                           <p>At vero eos et accusamus et iusto odio.</p>
                           <a href="#"><em>http://t.co/sBav7dm</em></a> 
                           <span>5 hours ago</span>
                           <i class="icon-twitter blog-twiiter-icon"></i>
                        </div>
                        <div class="blog-twitter-block">
                           <a href="">@keenthemes</a> 
                           <p>At vero eos et accusamus et iusto odio.</p>
                           <a href="#"><em>http://t.co/sBav7dm</em></a> 
                           <span>7 hours ago</span>
                           <i class="icon-twitter blog-twiiter-icon"></i>
                        </div>
                     </div>
                  </div>
                  <!--end col-md-3-->
               </div>
               <ul class="pagination pull-right">
                  <li><a href="#"><i class="icon-angle-left"></i></a></li>
                  <li><a href="#">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#">5</a></li>
                  <li><a href="#">6</a></li>
                  <li><a href="#"><i class="icon-angle-right"></i></a></li>
               </ul>
            </div>
         </div>        

</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">

    <!-- /.modal-dialog -->
</div>