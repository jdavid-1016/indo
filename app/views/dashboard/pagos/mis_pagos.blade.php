<div class="page-content" id="adminhelpdesk">
   
      
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Mis Pagos</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class=""><a href="ingresar">Ingresar Pagos</a></li>
                  <li class="active"><a href="mispagos" >Histórico</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_supports">
                        <div>
                           <form class="form-inline" action="mispagos" method="get">
                              <div class="search-region">
                                 <div class="form-group">
                                    <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                                 </div>
                                 <!-- <div class="form-group">
                                    <select class="form-control input-medium" name="applicant">
                                    <option value="">Solicitó</option> 
	                                 @foreach($applicants as $applicant)
	                                 @if($applicant->id == Input::get('applicant'))
	                                    <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
	                                 @else
	                                    <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
	                                 @endif  
	                                 @endforeach
                                    
                                    
                                    </select>
                                 </div> -->
                                 
                                 <div class="form-group">
                                    <select class="form-control input-small" name="status">
                                       <option value="">Estado</option>
                                       <option  value="2">Aprobado</option>
                                       <option  value="3">Rechazado</option>
                                       <option  value="6">Finalizado</option>
                                       <option  value="5">Causado</option>
                                       <option  value="4">Verificado</option>
                                    </select>
                                 </div>
                                 
                                 <div class="form-group">
                                    <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                                 </div>    
                              </div>
                           </form> 
                        </div>
                        @include('dashboard.pagos.tabla_aprobar_pagos_hist')
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
      
   
</div>
<div id="asdf"></div>
   <script type="text/javascript">
    
   
   function recargarTabla(){
       
       var html = $.ajax({
                     type: "GET",
                     url: "aprobar",
                     cache: false,
                     data: {nombre: "hola"},
                     async: false
                    }).responseText;
                                        
        $('#table_admin_supports').html(html);
       
   }
   setInterval(recargarTabla, 300000);
   </script>