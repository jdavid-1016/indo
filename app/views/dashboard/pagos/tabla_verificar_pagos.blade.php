
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th></th>
         <th>Verificar</th>
         <th>Cod</th>
         <th>Estado</th>
         <th>Proveedor</th>
         <th>Identificacion</th>
         <th>Concepto</th>
         <th>Metodo de pago</th>
         <th>Valor</th>
         <th>Fecha Pago</th>
         <th>Archivo</th>
         
         <th class="td_center"><i class="icon-time"></i></th> 
      </tr>
   </thead>
   <tbody>
   @foreach($pagos as $payment)
      <tr style="text-align:center;">
         <td><a class=" btn btn-default" style="{{$payment['id_compra']}}" data-target="#ajax" id="{{$payment['id_compra']}}" data-toggle="modal" onclick='cargarDetalleCompra($(this).attr("id"));return false;'><i class="icon-edit"></i></a></td>
         <td>
            <a class=" btn btn-success" data-target="#ajax" name="{{$payment['hid']}}" id="{{$payment['hid']}}" data-toggle="modal" onclick="cargar_datos($(this).attr('id'));return false;"><i class="icon-edit"></i></a>
         </td>
         <td>{{$payment['id']}}</td>
         <td>
            <span class="label label-sm {{$payment['label']}}">{{$payment['status']}}</span>
         </td>
         <td>{{$payment['provider']}} </td>
         <td>{{$payment['identificacion']}} </td>
         <td>{{$payment['concept']}} {{$payment['description']}}</td>
         <td>{{$payment['metodo']}} </td>
         <td>{{$payment['value']}}</td>
         <td>{{$payment['f_pago']}}</td>
         <td><a href="../{{$payment['file']}}" class="btn btn-success" target="_blanc"><i class="icon-paper-clip"></i></a></td>
         <td>{{$payment['time']}}</td>
      </tr>
   @endforeach
   </tbody>
</table>

<div class="pagination">
</div>



<script type="text/javascript">
function realizarProceso(valorCaja1){
      var estado = $("#status").val();
      var comentario = $("#observation_apr").val();
      var devuelto = 0;
      if (estado == "") {
            toastr.error('El campo estado esta vacio.', 'Error');
            return;
      }
      if (estado == 2) {
         var devuelto = 1;
      }

      if (comentario == "") {
            toastr.error('El campo Comentario esta vacio.', 'Error');
            return;
      }

   var parametros = {
      "valorCaja1": valorCaja1,
      "estado": estado,
      "devuelto": devuelto,
      "observation": comentario
   };
   $.ajax({
      data: parametros,
      url:  'aprobacion',
      type: 'get',

      success: function(response){
            toastr.success('Se aprobo la solicitud de pago correctamente', 'Pago aprobado');
            $("#close_modal").click();
            recargarTabla();
            var type_event = ["tesoreriapayments", response[0]["name"],response[0]["last_name"],response[0]["img_min"], response[0]["id"], valorCaja1];
            send(type_event);
      }
   });
}
function rechazar_solicitud(id){
   $('.rechazar_solicitud').attr({id: id});
}
function realizarProceso2(valorCaja1){
   var observation = $("#observation").val();
   if (observation == "") {
      toastr.error('El campo Observaciones está vacio', 'Error');
   }else{
      $("#close_modal").click();
      var parametros = {
         "valorCaja1"   : valorCaja1,
         "estado"       : '3',
         "fuente"       : '2',
         "observation"  : observation
      };
      $.ajax({
         data: parametros,
         url:  'aprobacion',
         type: 'get',

         success: function(response){
               
               toastr.error('Se rechazo la solicitud de pago correctamente', 'Pago rechazado');
               recargarTabla();
               
               var type_event = ["verrechazarpayments", response[0]["name"],response[0]["last_name"],response[0]["img_min"], response[0]["id"], valorCaja1];
                send(type_event);
         }
      });

   }

}
function cargarDetalleCompra(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  '../compras/detallehistorico',
      type: 'get',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
function cargar_datos(valorCaja1){
   
   
   var parametros = {
      "valorCaja1": valorCaja1,
      "valorCaja2": valorCaja1
   };
   $.ajax({
      data: parametros,
      url:  'gestionpago',
      type: 'post',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
function validar_estado() {
   var status = $("#status").val();

   
   
   switch (status) {
      case '7':
               
               $(".rechazar_solicitud").removeClass("btn-success");
               $(".rechazar_solicitud").addClass("btn-danger");
               
               
               $("#icon").removeClass("icon-ok");
               $("#icon").addClass("icon-remove");
          break;
      default:
               $("#ocultar_neto").show('show');
               $("#ocultar_tipo").show('show');
               $(".rechazar_solicitud").addClass("btn-success");
               
               
               $("#icon").removeClass("icon-remove");
               $("#icon").addClass("icon-ok");
            break;
      
   }
   


}
</script>

