<div class="page-content">
         <div class="tab-content">
            <div class="tab-pane active" id="tab_1_2">
               <div class="portlet ">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="icon-reorder"></i> Ingresar Pagos
                     </div>
                  </div>
                  
                  <div class="portlet-body">
                     <ul  class="nav nav-tabs">
                        <li class="active"><a href="#tab_1_1" data-toggle="tab">Ingresar Pagos</a></li>
                        <li class=""><a href="mispagos" >Histórico</a></li>
                        <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                        
                     </ul>
                     <form class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="form-body">
                        @if(isset($datos))
                           <div class="alert alert-success">
                              <strong></strong> Se ingreso la solicitud correctamente.
                           </div>
                        @else
                        @endif   
                           
                           <div class="row">
                              <div class="col-md-6">
                                 
                                 <hr>
                                 <div class="col-md-12">
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <select id="concept" name="concept" class="form-control input-lg" onchange="cargar_form()">
                                                <option value="">Concepto</option>
                                                @foreach($concepts as $concept)
                                                <option value="{{$concept->id}}">{{$concept->description}}</option>
                                                @endforeach
                                             </select>
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group" id="form_nombre">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input id="caja_busqueda" name="clave" type="text" class="form-control input-lg" placeholder="Nombre" onkeydown="search_users()">
                                             <div id="display"></div>
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group" id="form_cedula">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input id="nit" name="nit" type="text" class="form-control input-lg money" placeholder="Cedula">
                                             <input id="nit2" name="nit" type="hidden" class="form-control input-lg money" placeholder="Cedula">
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group" id="form_factura">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input id="factura" name="factura" type="text" class="form-control input-lg" placeholder="N° Factura">
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             
                                             <input id="archivo"  name="archivo" type="file" class="form-control input-lg">
                                          </div> 
                                       </div>
                                    </div>
                                    
                                 </div>
                              </div>
                              <div class="col-md-12">
                                 
                                 <hr>
                                 <div class="col-md-6">
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input id="fecha_pago" name="fecha_pago" type="text" class="form-control input-lg fecha_required" placeholder="Fecha de pago">
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <select id="metodo_pago" name="concept" class="form-control input-lg">
                                                <option value="">Metodo de pago</option>
                                                @foreach($payments_methods as $method)
                                                <option value="{{$method->id}}">{{$method->name}}</option>
                                                @endforeach
                                             </select>
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             <input id="value" name="value" type="text" class="form-control input-lg money" placeholder="Valor">
                                          </div> 
                                       </div>
                                    </div>
                                    <div class="form-group">
                                       <div class="col-md-12">
                                          <div class="input-group">
                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                             
                                             <textarea id="description" name="description" placeholder="Descripción" class="form-control input-lg"></textarea>
                                          </div> 
                                       </div>
                                    </div>
                                    
                                    
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="form-actions right">                           
                           <a class="btn btn-success" onclick="validar_campos()">Registrar</a>                            
                        </div>
                     
                  </div>

               </div>
            </div>
         </div>         
      
   
</div>
   
<script>
    function terminar_proceso() {
        var concept     = $("#concept").val();
        var factura     = $("#factura").val();
        var value       = $("#value").val();
        var description = $("#description").val();
        var nit2        = $("#nit2").val();
        var fecha_pago  = $("#fecha_pago").val();
        var metodo_pago = $("#metodo_pago").val();
        var archivo     = $('#archivo').val();

        if (archivo == "") {
            toastr.info('Error. por favor seleccione un archivo de soporte.', 'Info');
            return;
        }
        
        
        
        $("#archivo").upload('createpayment', 
        {
            
            concept: concept,
            factura: factura,
            value: value,
            description: description,
            nit: nit2,
            fecha_pago: fecha_pago,
            forma_pago:metodo_pago
            

        },
        function(respuesta) {
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente la solicitud', 'Nueva solicitud de pago');
                var concept     = $("#concept").val("");
                var factura     = $("#factura").val("");
                var value       = $("#value").val("");
                var description = $("#description").val("");
                var nit2        = $("#nit2").val("");
                var fecha_pago  = $("#fecha_pago").val("");
                var metodo_pago = $("#metodo_pago").val("");
                var archivo     = $('#archivo').val("");
                var archivo     = $('#caja_busqueda').val("");
                var archivo     = $('#nit').val("");
            } else {
                toastr.error('No ha sido posible ingresar el nuevo pago.', 'Error');                              
                

                
                //var type_event = ["pendingshopping", respuesta[0]["name"], respuesta[0]["last_name"], respuesta[0]["img_min"], respuesta[0]["id"], 6, id];
                //send(type_event);
            }
                                        
        }, function(progreso, valor) {
            //Barra de progreso.
            $("#barra_de_progreso_").val(valor);
        });
    }
    
    function validar_campos(){
        var concept     = $("#concept").val();
        if (concept == "") {
            toastr.info('Error. por favor seleccione un Concepto de pago.', 'Info');
            return;
        }


        var factura     = $("#factura").val();
        var value       = $("#value").val();
        var description = $("#description").val();
        var nit2        = $("#nit2").val();
        var fecha_pago  = $("#fecha_pago").val();
        var metodo_pago = $("#metodo_pago").val();

        switch (concept) {
         case '13':
         case '14':
                if (fecha_pago == "") {
                    toastr.info('Error. el campo de fecha de pago no puede estar vacio.', 'Info');
                    return;
                }
                if (metodo_pago == "") {
                    toastr.info('Error. el campo de metodo de pago no puede estar vacio.', 'Info');
                    return;
                }
                if (value == "") {
                    toastr.info('Error. el campo de valor no puede estar vacio.', 'Info');
                    return;
                }
                if (description == "") {
                    toastr.info('Error. el campo de Descripción no puede estar vacio.', 'Info');
                    return;
                }

                terminar_proceso();
             break;
         default:
                if (nit2 == "") {
                    toastr.info('Error. el campo de proveedor no puede estar vacio.', 'Info');
                    return;
                }
                if (factura = "") {
                    toastr.info('Error. el campo de Factura no puede estar vacio.', 'Info');
                    return;
                }
                if (fecha_pago == "") {
                    toastr.info('Error. el campo de fecha de pago no puede estar vacio.', 'Info');
                    return;
                }
                if (metodo_pago == "") {
                    toastr.info('Error. el campo de metodo de pago no puede estar vacio.', 'Info');
                    return;
                }
                if (value == "") {
                    toastr.info('Error. el campo de valor no puede estar vacio.', 'Info');
                    return;
                }
                if (description == "") {
                    toastr.info('Error. el campo de Descripción no puede estar vacio.', 'Info');
                    return;
                }


                terminar_proceso();
             break;
        }
    }
    function search_users() {
       var texto = $("#caja_busqueda").val();
       
           $.ajax({
                   type: "GET",
                   url: "consultarprov",
                   data: { palabra: texto}
           })
           .done(function(data) {
           
           if (parseInt(data) == 2){
               $("#display").css("display", "none");
           }else{
               $("#display").css("display", "block");
               $("#display").html(data);
           }
           });
    }
    function agregarNit(nit, nombre, id){
     
      $('#nit2').val(id);
      $('#nit').val(nit);
      $('#caja_busqueda').val(nombre);
      $("#display").hide();
      
    }
   
    function agregarProv(nit, nombre, id){
     
      $('#id_proveedor').val(id);
      $('#nit').val(nit);
      $('#caja_busqueda').val(nombre);
      $("#display").hide();
      $("#displaynit").hide();
      
    }
    function focusFunction() {
       $("#display").hide('slow');
    }

    function blurFunction() {
       $("#display").show('slow');
    }
    function focusFunctionNit() {    
       $("#displaynit").hide('slow');
    }

    function blurFunctionNit() {
       $("#displaynit").show('slow');
    }

    function cargar_form() {
       var concept = $("#concept").val();

       
       
       switch (concept) {
        
        case '5':
                $('#metodo_pago> option[value=""]').attr('selected', '');
                $("#factura").attr('placeholder','N° Consecutivo');
                $('#form_nombre').show('show');
                $('#form_factura').show('show');
                $('#form_cedula').show('show');
                $("#factura").val("");
                $("#factura").prop('disabled', false); 
            break;
        case '2':
        case '8':
        case '10':
        case '15':
                
                verificar_fac();
                $('#metodo_pago> option[value=""]').attr('selected', '');
                $('#form_nombre').show('show');
                $('#form_factura').show('show');
                $('#form_cedula').show('show');
                $("#factura").prop('disabled', true);
            break;
        case '6':
                $('#metodo_pago> option[value=""]').attr('selected', '');   
                $("#factura").attr('placeholder','N° Contrato');
                $('#form_nombre').show('show');
                $('#form_factura').show('show');
                $('#form_cedula').show('show');
                $("#factura").val("");
                $("#factura").prop('disabled', false); 
            break;
        case '9':
                $('#metodo_pago> option[value=""]').attr('selected', '');  
                $("#factura").attr('placeholder','N° Formulario');
                $('#form_nombre').show('show');
                $('#form_factura').show('show');
                $('#form_cedula').show('show');
                $("#factura").val("");
                $("#factura").prop('disabled', false); 
            break;
        case '12':
        case '14':
        case '13':
                $("#form_nombre").hide('slow');
                $("#form_factura").hide('slow');
                $("#form_cedula").hide('slow');
                $('#metodo_pago> option[value="3"]').attr('selected', 'selected');
                $("#factura").val("");
                $("#factura").prop('disabled', false); 
            break;
        default:
                $('#form_nombre').show('show');
                $('#form_factura').show('show');
                $('#form_cedula').show('show');
                $("#factura").attr('placeholder','N° factura');
                $('#metodo_pago> option[value=""]').attr('selected', '');
                $("#factura").val(""); 
                $("#factura").prop('disabled', false); 
            break;
       }
       


    }
    function verificar_fac(){
        var concept = $("#concept").val();
        $.ajax({
                type: "GET",
                url:  "consultafactura",
                data: { concept: concept}
        })
        .done(function(data) {
           $("#factura").val(data); 
            
        });

    }




</script>

<style type="text/css">
#displayprov /*estilos para la caja principal en donde se puestran los resultados de la busqueda en forma de lista*/
{
width:400px;
display:none;
overflow:hidden;
z-index:10;
border: solid 1px #666;
}
.display_box /*estilos para cada caja unitaria de cada usuario que se muestra*/
{
padding:2px;
padding-left:6px; 
font-size:15px;
height:30px;
text-decoration:none;
color:#3b5999; 
}

.display_box:hover /*estilos para cada caja unitaria de cada usuario que se muestra. cuando el mause se pocisiona sobre el area*/
{
background: #7f93bc;
color: #FFF;
}
/* Easy Tooltip */
</style>