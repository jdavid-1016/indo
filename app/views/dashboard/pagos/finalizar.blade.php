
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Tesorería</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="#tab_1_1" data-toggle="tab">Solicitudes</a></li>
                  <li class=""><a href="finalizarhist" >Histórico</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_final_payments">
                        @include('dashboard.pagos.tabla_finalizar_pagos')
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>


<script type="text/javascript">
   function recargarTabla(){
       
       var html = $.ajax({
                     type: "GET",
                     url: "finalizar",
                     cache: false,
                     data: {nombre: "hola"},
                     async: false
                    }).responseText;
                                        
        $('#table_final_payments').html(html);
       
   }

</script>