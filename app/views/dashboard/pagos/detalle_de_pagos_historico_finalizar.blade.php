<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   @foreach($payments as $payment)
      <input type="hidden" ng-model="support_id" value="{{$payment->id}}" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Historicos de pago</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="row">
            <div class="col-md-12">
            <label>
               <span class="label label-sm label-info">Solicitud {{$payment->Users->name}} {{$payment->Users->last_name}}</span><br>
            </label>
            </div>
            <div class="col-md-12">
               <div class="form-group">
                  
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2" readonly class="form-control">{{$payment->description}}</textarea>
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Proveedor</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->Providers->provider}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Nit</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->Providers->nit}}">
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Valor</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->value}}">
                        
                     </select>
                  </div> 
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Metodo de pago</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->PaymentsMethods->name}}">
                  </div> 
               </div>   
            </div> 
            
         </div>
         <div class="row">
            
            <div class="col-md-6" id="ocultar_neto">
               <div class="form-group">
                  <label class="control-label">Neto a pagar</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" readonly="" class="form-control money" id="neto_pagar" placeholder="" data-required="true" value="{{$payment->net_pay}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6" id="ocultar_tipo">
               <div class="form-group"> 
                  <label class="control-label">Comprobante</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     
                     <input type="text" readonly="" class="form-control" placeholder="" id="numero_comp" data-required="true" value="{{$payment->type_purchase}}">
                  </div>
               </div>   
            </div> 
            <div class="col-md-2" id="ocultar_tipo">
            
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group" id="frm_comentario">
                  
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
                           Comentarios
                           </a>
                        </h4>
                     </div>
                     <div id="collapse_4" class="panel-collapse collapse" style="height: auto;">
                        <div class="panel-body">
                           @foreach($traces as $trace)
                              <div class="alert alert-success">
                                 <strong>{{$trace->Users->name}} {{$trace->Users->last_name}} {{$trace->description}}!</strong> {{$trace->created_at}} <br><span class="text-primary">Comentario:</span> {{$trace->comment}}.
                                 
                              </div>
                           @endforeach
                        </div>
                     </div>
                  </div> 
                  
               </div>
            </div>      
         </div> 
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Creado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <input readonly type="text" class="form-control" value="{{$payment->created_at}}">
                  </div> 
               </div>
            </div>

            
            
               
               <div class="col-md-6" style="@if($payment->payments_statuses_id != 6) display:none @endif">
                  <div class="form-group @if($payment->accounting_document == "") has-success @endif">
                     <label class="control-label">Doc. Contable</label>
                     <div class="input-group">
                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                        @if($payment->payments_statuses_id == 6)
                        @if($payment->accounting_document == "")
                           
                        <select id="siglas" class="form-control" onchange="validar_siglas()">
                           <option value="">Comprobante</option>
                           @foreach($comprobantes as $comprobante)
                           @if($comprobante->tipo == 1)
                           <option value="{{$comprobante->siglas}}">{{$comprobante->siglas}}</option>
                           @endif
                           @endforeach

                        </select>
                        <input type="text"  id="numero_compro" class="form-control" onblur="validar_siglas()" value="{{$payment->accounting_document}}" @if($payment->accounting_document != "") readonly @endif>
                        @endif
                        @endif 
                        <input type="text" readonly="" class="form-control" placeholder="" id="documento_contable" data-required="true" value="{{$payment->accounting_document}}">
                     </div> 
                  </div>
               </div>
               
            
         </div> 
   
         <div class="row" id="observacion_editar" style="display:none">
            <div class="col-md-12">
               <div class="form-group has-success">
                  <label class="control-label">Comentario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2" class="form-control" id="observation_edit"></textarea>
                  </div> 
               </div>
            </div>      
         </div>
      </div>   
      <div class="modal-footer">
      
         
      
      
      
      @if($payment->payments_statuses_id == 6)
         @if($payment->accounting_document == "")
            <button type="button" class="btn btn-success rechazar_solicitud"  id="{{$payment->id}}" onclick="guardar_doc($(this).attr('id'))"><i id="icon" class="icon-ok"></i>Enviar</button>
         @else
            <button type="button" class="btn btn-info editar_observacion"  id="{{$payment->id}}" onclick="activar_doc($(this).attr('id'))"><i id="icon" class="icon-edit"></i>Editar</button>
         @endif

      @endif 
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
   @endforeach
   </form>   
   </div>
</div>

<script type="text/javascript">
function validar_siglas() {
   var siglas = $("#siglas").val();
   var numero_comp = $("#numero_compro").val();

   if (siglas != "") {
      if (numero_comp != "") {
         $("#documento_contable").val(siglas+"_"+numero_comp);
      }  
   }
}

function activar_doc() {
   
   $("#observacion_editar").show('show');
   $(".editar_observacion").attr('onclick','editar_doc_contable($(this).attr("id"))');
     
}

function editar_doc_contable(id) {
   
   var observacion = $("#observation_edit").val();
   if (observacion == "") {
      toastr.error('El campo comentario esta vacio', 'Error');
   }
   var parametros = {
      "id": id,
      "observacion": observacion
   };
   $.ajax({
      data: parametros,
      url:  'editardocumento',
      type: 'get',

      success: function(response){
         toastr.success('Se envió la información correctamente.', 'Edición doc. contable');   
         $("#close_modal").click();
      }
   });

}
</script>