
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th></th>
         <th>Aprobar</th>
         <th>
            Cod 
         </th>
         <th>Estado</th>
         <th>Usuario</th>
         <th>Concepto</th>
         <th>Valor</th>
         <th>Fecha Pago</th>
         <th>
            Archivo
            
         </th>
         <!-- <th>Solicitud</th>
         <th>Estado</th>
         <th>Responsable</th>
         <th>Creado</th>
         <th class="td_center"><i class="icon-time"></i></th>
         <th class="td_center">Comentario</th> -->
         <th class="td_center"><i class="icon-time"></i></th>
      </tr>
   </thead>
   <tbody>
   @foreach($pagos as $payment)
      <tr style="text-align:center;">
         <td><a class=" btn btn-default" style="{{$payment['id_compra']}}" data-target="#ajax2" id="{{$payment['id_compra']}}" data-toggle="modal" onclick='cargarDetalleCompra($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a></td>
         <td>
            <a href="#" class="btn btn-success"  id="{{$payment['hid']}}" onclick="realizarProceso($(this).attr('id'));return false;"><i class="icon-ok"></i></a>
            <a class=" btn btn-danger" data-target="#ajax" name="{{$payment['hid']}}" id="{{$payment['hid']}}" data-toggle="modal" onclick="rechazar_solicitud($(this).attr('id'));return false;"><i class="icon-remove"></i></a>
         </td>
         <td>
            <a class=" btn btn-default" data-target="#ajax2" id="{{$payment['hid']}}" data-toggle="modal" onclick='cargar_datos($(this).attr("id"));return false;'>{{$payment['id']}}</a>
         </td>
         <td>
            <span class="label label-sm {{$payment['label']}}">{{$payment['status']}}</span>
         </td>
         <td>{{$payment['user']}} </td>
         <td>{{$payment['concept']}} {{$payment['description']}}</td>
         <td>{{$payment['value']}}</td>
         <td>{{$payment['f_pago']}}</td>
         <td><a href="../{{$payment['file']}}" class="btn btn-success" target="_blanc"><i class="icon-paper-clip"></i></a></td>
         <td>{{$payment['time']}}</td>
      </tr>
   @endforeach
   </tbody>
</table>

<div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
   <div class="modal-content">
   <form class="form" action="#">
   
      <input type="hidden" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Rechazar soliciud de pago</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="col-md-12">
            <div class="form-group">
               <label class="control-label">Comentario</label>
               <div class="input-group">
                   <span class="input-group-addon"><i class="icon-edit"></i></span>
                   <textarea rows="2" class="form-control" id="observation"></textarea>
               </div> 
            </div>
         </div>
      </div>   
      <div class="modal-footer">
         <!-- <button type="button" class="btn btn-success" onclick="asing_support()">Guardar</button> -->
         <button type="button" class="btn btn-danger rechazar_solicitud"  id="" onclick="realizarProceso2($(this).attr('id'));"><i class="icon-remove"></i>  Rechazar pago</button>
         
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cancelar</button>  
      </div>
   
   </form>   
   </div>
</div>
</div>



<script type="text/javascript">


function realizarProceso(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1,
      "estado": '2'
   };
   $.ajax({
      data: parametros,
      url:  'aprobacion',
      type: 'get',

      success: function(response){
          
        var type_event = ["causarpayments", response[0]["name"],response[0]["last_name"],response[0]["img_min"], response[0]["id"], valorCaja1];
            send(type_event);
            
            toastr.success('Se aprobo la solicitud de pago correctamente', 'Pago aprobado');
            recargarTabla();
      }
   });
}

function rechazar_solicitud(id){
   $('.rechazar_solicitud').attr({id: id});
}
function realizarProceso2(valorCaja1){
   var observation = $("#observation").val();
   if (observation == "") {
      toastr.error('El campo Observaciones está vacio', 'Error');
   }else{
      $("#close_modal").click();
      var parametros = {
         "valorCaja1"   : valorCaja1,
         "estado"       : '7',
         "estado_debuelto"       : '1',
         "observation"  : observation
      };
      $.ajax({
         data: parametros,
         url:  'aprobacion',
         type: 'get',

         success: function(response){
               
               toastr.error('Se rechazo la solicitud de pago correctamente', 'Pago rechazado');
               recargarTabla();
         }
      });

   }

}
function cargarDetalleCompra(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  '../compras/detallehistorico',
      type: 'get',

      success: function(response){
            $("#ajax2").html(response);
      }
   });
}
function cargar_datos(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1,
      "valorCaja2": valorCaja1
   };
   $.ajax({
      data: parametros,
      url:  'modalhistorico',
      type: 'post',

      success: function(response){
            $("#ajax2").html(response);
      }
   });
}
</script>

