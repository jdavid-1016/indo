
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th></th>
         <th>Finalizar</th>
         <th>Cod</th>
         <th>Proveedor</th>
         <th>Identificacion</th>
         <th>Concepto</th>
         <th>Metodo de pago</th>
         <th>Valor</th>
         <th>F. pago</th>
         <th>Archivo</th>
         <th>Excel</th>
         <th class="td_center"><i class="icon-time"></i></th>         
      </tr>
   </thead>
   <tbody>
   @foreach($pagos as $payment)
      <tr style="text-align:center;">
         <td><a class="btn btn-default" style="{{$payment['id_compra']}}" data-target="#ajax" id="{{$payment['id_compra']}}" data-toggle="modal" onclick='cargarDetalleCompra($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a></td>
         <td>
            <a href="#" class="btn btn-success"  id="{{$payment['hid']}}" onclick="realizarProceso($(this).attr('id'), <?php if($payment['id_compra'] != "display:none"){ echo $payment['id_compra']; }else{ echo "0"; } ?>);return false;"><i class="icon-ok"></i></a>
            <a class=" btn btn-danger" data-target="#ajax2" name="{{$payment['hid']}}" id="{{$payment['hid']}}" data-toggle="modal" onclick="rechazar_solicitud($(this).attr('id'));return false;"><i class="icon-remove"></i></a>
            
         </td>
         <td>{{$payment['id']}}</td>
         <td>{{$payment['provider']}} </td>
         <td>{{$payment['identificacion']}} </td>
         <td>{{$payment['concept']}} {{$payment['description']}}</td>
         <td>{{$payment['metodo']}} </td>
         <td>{{$payment['value']}}</td>
         <td>{{$payment['f_pago']}}</td>
         <td><a href="../{{$payment['file']}}" class="btn btn-success" target="_blanc"><i class="icon-paper-clip"></i></a></td>
         <td><?php if($payment['metodo']=="Transferencia"){ ?> <input type="checkbox" name="hola" id="" value="{{$payment['hid']}}" >  <?php } ?></td>
         <td>{{$payment['time']}}</td>
      </tr>
   @endforeach
   <tr style="text-align:center;">
         <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
         <td><a href="#" class="btn btn-info"  id="" onclick="GenerarExcel();"><i class="icon-download-alt"></i></a></td>
         <td></td>
      </tr>
   </tbody>
</table>
<div class="pagination">
</div>


<div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Rechazar solicitud de pago</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="col-md-12">
            <div class="form-group">
               <label class="control-label">Comentario</label>
               <div class="input-group">
                   <span class="input-group-addon"><i class="icon-edit"></i></span>
                   <textarea rows="2" class="form-control" ng-model="observation" id="observation"></textarea>
               </div> 
            </div>
         </div>
      </div>
      <div class="modal-footer">
         <!-- <button type="button" class="btn btn-success" onclick="asing_support()">Guardar</button> -->
         <button type="button" class="btn btn-danger rechazar_solicitud"  id="" onclick="realizarProceso2($(this).attr('id'));"><i class="icon-remove"></i>  Rechazar pago</button>
         
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cancelar</button>  
      </div>
   
   </form>   
   </div>
</div>
</div>

<script type="text/javascript">
 function GenerarExcel(){
     
   var proveedores_excel = new Array();      
   
   $("input[type=checkbox]:checked").each(function(){
	//cada elemento seleccionado
      proveedores_excel.push($(this).val());      
   });
   
   if (proveedores_excel.length > 0) {
       
       window.open("exportarexcelprov?proveedores_excel="+proveedores_excel);
                   
   }else{
       toastr.error('Debe seleccionar al menos un pago', 'Error');
   }
      
}
    
function realizarProceso(valorCaja1, tipo){
    
   var parametros = {
      "valorCaja1": valorCaja1,
      "tipo": tipo,
      "estado": '6'
   };
   $.ajax({
      data: parametros,
      url:  'aprobacion',
      type: 'get',

      success: function(response){          
            toastr.success('Se aprobo la solicitud de pago correctamente', 'Pago aprobado');
            recargarTabla();
            var type_event = ["finalizarpayments", response[0]["name"],response[0]["last_name"],response[0]["img_min"], response[0]["id"], valorCaja1, tipo];
            send(type_event);
      }
   });
}
function rechazar_solicitud(id){
   $('.rechazar_solicitud').attr({id: id});
}
function realizarProceso2(valorCaja1){
   var observation = $("#observation").val();
   if (observation == "") {
      toastr.error('El campo Observaciones está vacio', 'Error');
   }else{
      $("#close_modal").click();
      var parametros = {
         "valorCaja1"   : valorCaja1,
         "estado"       : '3',
         "fuente"       : '1',
         "observation"  : observation
      };
      $.ajax({
         data: parametros,
         url:  'aprobacion',
         type: 'get',

         success: function(response){
               
               toastr.error('Se rechazo la solicitud de pago correctamente', 'Pago rechazado');
               recargarTabla();
               
               var type_event = ["finrechazarpayments", response[0]["name"],response[0]["last_name"],response[0]["img_min"], response[0]["id"], valorCaja1];
                send(type_event);
         }
      });

   }

}
function cargarDetalleCompra(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  '../compras/detallehistorico',
      type: 'get',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
</script>

