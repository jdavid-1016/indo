
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th></th>
         <th>Causar</th>
         <th>Cod</th>
         <th>Estado</th>
         <th>Proveedor</th>
         <th>Identificacion</th>
         <th>Concepto</th>
         <th>Metodo de pago</th>
         <th>Valor</th>
         <th>Fecha Pago</th>
         <th>Archivo</th>
         
         <th class="td_center"><i class="icon-time"></i></th>    
         
      </tr>
   </thead>
   <tbody>
   @foreach($pagos as $payment)
      <tr style="text-align:center;" id="{{$payment['hid']}}">
         <td><a class=" btn btn-default" style="{{$payment['id_compra']}}" data-target="#ajax" id="{{$payment['id_compra']}}" data-toggle="modal" onclick='cargarDetalleCompra($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a></td>
         <td>
            <a class=" btn btn-success" data-target="#ajax" name="{{$payment['hid']}}" id="{{$payment['hid']}}" data-toggle="modal" onclick="cargar_datos($(this).attr('id'));return false;"><i class="icon-edit"></i></a>
         </td>
         <td>{{$payment['id']}}</td>
         <td>
            <span class="label label-sm {{$payment['label']}}">{{$payment['status']}}</span>
         </td>
         <td>{{$payment['provider']}} </td>
         <td>{{$payment['identificacion']}} </td>
         <td>{{$payment['concept']}} {{$payment['description']}}</td>
         <td>{{$payment['metodo']}} </td>
         <td>{{$payment['value']}}</td>
         <td>{{$payment['f_pago']}}</td>
         <td><a href="../{{$payment['file']}}" class="btn btn-success" target="_blanc"><i class="icon-paper-clip"></i></a></td>
         <td>{{$payment['time']}}</td>
      </tr>
   @endforeach
   </tbody>
</table>

<div class="pagination">
</div>


<div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
   
</div>


<script type="text/javascript">
function realizarProceso(valorCaja1){
   var observation = $("#observation_apr").val();
   var neto_pagar = $("#neto_pagar").val();
   var tipo_compra = $("#tipo_compra").val();
   var estado = $("#status").val();

   if (estado == 4) {
      if (neto_pagar == "") {
         toastr.error('El campo Neto a pagar está vacio.', 'Error');
         return;
      }
      if (tipo_compra == "") {
         toastr.error('Los campos de comprobante están vacios.', 'Error');
         return;
      }
   }
   if (observation == "") {
         toastr.error('El campo observacion está vacio.', 'Error');
         return;
   }
   if (estado == "") {
         toastr.error('El campo estado esta vacio.', 'Error');
         return;
   }

   var parametros = {
      "valorCaja1"   : valorCaja1,
      "observation"  : observation,
      "neto_pagar"   : neto_pagar,
      "tipo_compra"  : tipo_compra,
      "estado"       : estado,
      "estado_debuelto": 2
   };
   $.ajax({
      data: parametros,
      url:  'aprobacion',
      type: 'get',

      success: function(response){
         $("#close_modal").click();
         $("#close_modal1").click();
         toastr.success('Se envió la informacion correctamente', 'Pago Gestionado');         
         $("#"+valorCaja1).css("display", "none"); 
         
         var type_event = ["verificarpayments", response[0]["name"],response[0]["last_name"],response[0]["img_min"], response[0]["id"], valorCaja1];
            send(type_event);

      }
   });
}
function rechazar_solicitud(id){
   $('.rechazar_solicitud').attr({id: id});
   $("#observation").val("");

}

function realizarProceso2(valorCaja1){
   var observation = $("#observation").val();
   if (observation == "") {
      toastr.error('El campo Observaciones está vacio', 'Error');
   }else{
      $("#close_modal").click();
      $("#close_modal1").click();
      var parametros = {
         "valorCaja1"   : valorCaja1,
         "estado"       : '3',
         "observation"  : observation
      };
      $.ajax({
         data: parametros,
         url:  'aprobacion',
         type: 'get',

         success: function(response){
               
               toastr.error('Se rechazo la solicitud de pago correctamente', 'Pago rechazado');
               recargarTabla();
         }
      });

   }

}
function cargarDetalleCompra(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  '../compras/detallehistorico',
      type: 'get',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}

function cargar_datos(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1
   };
   $.ajax({
      data: parametros,
      url:  'gestionpago',
      type: 'post',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
function validar_estado() {
   var status = $("#status").val();

   
   
   switch (status) {
      case '7':
               $("#ocultar_neto").hide('slow');
               $("#ocultar_tipo").hide('slow');
               $(".rechazar_solicitud").removeClass("btn-success");
               $(".rechazar_solicitud").addClass("btn-danger");
               $("#icon").removeClass("icon-ok");
               $("#icon").addClass("icon-remove");
          break;
      default:
               $("#ocultar_neto").show('show');
               $("#ocultar_tipo").show('show');
               $(".rechazar_solicitud").addClass("btn-success");
               $("#icon").removeClass("icon-remove");
               $("#icon").addClass("icon-ok");
            break;
      
   }
   


}
function validar_siglas() {
   var siglas = $("#siglas").val();
   var numero_comp = $("#numero_comp").val();

   if (siglas != "") {
      if (numero_comp != "") {
         $("#tipo_compra").val(siglas+"_"+numero_comp);
      }  
   }
}

</script>

