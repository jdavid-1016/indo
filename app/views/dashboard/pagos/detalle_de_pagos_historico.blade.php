<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   @foreach($payments as $payment)
      @if($payment->payments_statuses_id != 7)
         <?php  $editable = "readonly"; ?>
         <?php  $estilo = ""; ?>
      @else
         <?php  $editable = ""; ?>
         <?php  $estilo = "has-success"; ?>
      @endif

      <input type="hidden" ng-model="support_id" value="{{$payment->id}}" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Historicos de pago</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="row">
            <div class="col-md-12">
            <label>
               <span class="label label-sm label-info">Solicitud {{$payment->Users->name}} {{$payment->Users->last_name}}</span><br>
            </label>
            </div>
            <div class="col-md-12">
               <div class="form-group">
                  
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2" readonly="" class="form-control">{{$payment->description}}</textarea>
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Factura</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->n_factura}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group has-success">
                  <label class="control-label">Fecha de pago</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input  type="text" id="fecha_pago" class="form-control input-lg fecha_required" placeholder="" data-required="true" value="{{$payment->payment_date}}">
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Proveedor</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->Providers->provider}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Nit</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->Providers->nit}}">
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Valor</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->value}}">
                        
                     </select>
                  </div> 
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="form-group {{$estilo}}"> 
                  <label class="control-label">Metodo de pago</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="metodo_pago" name="concept" class="form-control">
                        <option value="">Metodo de pago</option>
                        @foreach($payments_methods as $method)
                        @if($payment->payments_methods_id == $method->id)
                        <option selected="" value="{{$method->id}}">{{$method->name}}</option>
                        @else
                        <option value="{{$method->id}}">{{$method->name}}</option>
                        @endif
                        @endforeach
                     </select>
                  </div> 
               </div>   
            </div> 
            
         </div>
         <div class="row">
            
            <div class="col-md-6" id="ocultar_neto">
               <div class="form-group">
                  <label class="control-label">Neto a pagar</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" readonly="" class="form-control money" id="neto_pagar" placeholder="" data-required="true" value="{{$payment->net_pay}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6" id="ocultar_tipo">
               <div class="form-group"> 
                  <label class="control-label">Comprobante</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     
                     <input type="text" readonly="" class="form-control" placeholder="" id="numero_comp" data-required="true" value="{{$payment->type_purchase}}">
                  </div>
               </div>   
            </div> 
            <div class="col-md-2" id="ocultar_tipo">
            
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group" id="frm_comentario">

                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
                           Comentarios
                           </a>
                        </h4>
                     </div>
                     <div id="collapse_4" class="panel-collapse collapse" style="height: auto;">
                        <div class="panel-body">
                           @foreach($traces as $trace)
                              <div class="alert alert-success">
                                 <strong>{{$trace->Users->name}} {{$trace->Users->last_name}} {{$trace->description}}!</strong> {{$trace->created_at}} <br><span class="text-primary">Comentario:</span> {{$trace->comment}}.
                                 
                              </div>
                           @endforeach
                        </div>
                     </div>
                  </div> 
                 
               </div>
            </div>      
         </div>
         
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Creado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <input readonly="" type="text" class="form-control" value="{{$payment->created_at}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group has-success">
                  <label class="control-label">Archivo</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     
                     <input type="file" id="archivo" class="form-control">
                  </div> 
               </div>
            </div>

             
         </div> 
      </div>   
      <div class="modal-footer">
      
         
      
      
      @if($payment->payments_statuses_id == 7)
         <button type="button" class="btn btn-success rechazar_solicitud"  id="{{$payment->id}}" onclick="actualizar_datos($(this).attr('id'))"><i id="icon" class="icon-ok"></i>  Enviar</button>   
      @endif
      
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
   @endforeach
   </form>   
   </div>
</div>

<script type="text/javascript">
   $( ".fecha_required" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
   function actualizar_datos(id) {
       var fecha_pago  = $("#fecha_pago").val();
       var metodo_pago = $("#metodo_pago").val();
       var archivo     = $('#archivo').val();
       if (archivo == "") {
           toastr.info('Error. por favor seleccione un archivo de soporte.', 'Info');
           return;
       }
       $("#archivo").upload('actualizardatos', 
       {
         fecha_pago: fecha_pago,
           forma_pago:metodo_pago,
           id:id
       },
       function(respuesta) {
           
           if (respuesta === 1) {
               toastr.success('Se ingreso correctamente la solicitud', 'Nueva solicitud de pago');
               $("#close_modal").click();
               
           } else {
               toastr.error('No ha sido posible ingresar el nuevo pago.', 'Error');                              
               
           }
                                       
       }, function(progreso, valor) {
           //Barra de progreso.
           $("#barra_de_progreso_").val(valor);
       });
   }
</script>