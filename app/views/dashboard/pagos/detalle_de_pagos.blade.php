<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   @foreach($payments as $payment)
      <input type="hidden" ng-model="support_id" value="{{$payment->id}}" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Causación de pago</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="row">
            <div class="col-md-12">
            <label>
               <span class="label label-sm label-info">Solicitud {{$payment->Users->name}} {{$payment->Users->last_name}}</span><br>
            </label>
            </div>
            <div class="col-md-12">
               <div class="form-group">
                  
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2" readonly class="form-control">{{$payment->description}}</textarea>
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Proveedor</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->Providers->provider}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Nit</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->Providers->nit}}">
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Valor</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->value}}">
                        
                     </select>
                  </div> 
               </div>
            </div>
            
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Metodo de pago</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$payment->PaymentsMethods->name}}">
                  </div> 
               </div>   
            </div> 
            
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="form-group has-success">
                  <label class="control-label">Estado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="status" class="form-control" onchange="validar_estado()">
                        <option value="">Estado</option>
                        <option value="4">Aprobado</option>
                        <option value="7">Rechazado</option>
                     </select>
                  </div> 
               </div>
            </div>
            <div class="col-md-4" id="ocultar_neto">
               <div class="form-group has-success">
                  <label class="control-label">Neto a pagar</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control money" id="neto_pagar" placeholder="" data-required="true" @if($payment->net_pay != "")value="{{$payment->net_pay}} @endif">
                  </div> 
               </div>
            </div>
            <div class="col-md-4" id="ocultar_tipo">
               <div class="form-group has-success"> 
                  <label class="control-label">Comprobante</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="siglas" class="form-control" onchange="validar_siglas()">
                        <option value="">Comprobante</option>
                        @foreach($comprobantes as $comprobante)
                        @if($comprobante->tipo == 0)
                        <option value="{{$comprobante->siglas}}">{{$comprobante->siglas}}</option>
                        @endif
                        @endforeach

                     </select>
                     <input type="text" class="form-control" placeholder="" id="numero_comp" data-required="true" value="" onblur="validar_siglas()">
                  </div>

                  
                     <input type="hidden" class="form-control" placeholder="" id="tipo_compra" data-required="true" value="">

               </div>   
            </div> 
            <div class="col-md-2" id="ocultar_tipo">
            
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group" id="frm_comentario">
                  
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
                           Comentarios
                           </a>
                        </h4>
                     </div>
                     <div id="collapse_4" class="panel-collapse collapse" style="height: auto;">
                        <div class="panel-body">
                           @foreach($traces as $trace)
                              <div class="alert alert-success">
                                 <strong>{{$trace->Users->name}} {{$trace->Users->last_name}} {{$trace->description}}!</strong> {{$trace->created_at}} <br><span class="text-primary">Comentario:</span> {{$trace->comment}}.
                                 
                              </div>
                           @endforeach
                        </div>
                     </div>
                  </div> 

               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group has-success">
                  <label class="control-label">Comentario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2"  class="form-control" id="observation_apr"></textarea>
                  </div> 
               </div>
            </div>      
         </div>  
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Creado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <input readonly type="text" class="form-control" value="{{$payment->created_at}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6" @if($payment->type_purchase == "") style="display:none" @endif>
               <div class="form-group has-warning">
                  <label class="control-label">Comprobante</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <input readonly type="text" class="form-control" value="{{$payment->type_purchase}}">
                  </div> 
               </div>
            </div>
         </div> 
      </div>   
      <div class="modal-footer">
      
         
      
      
      
         
         <button type="button" class="btn btn-success rechazar_solicitud"  id="{{$payment->id}}" onclick="realizarProceso($(this).attr('id'))"><i id="icon" class="icon-ok"></i>  Enviar</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
   @endforeach
   </form>   
   </div>
</div>