
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th></th>
         <th>
            Cod 
         </th>
         <th>Estado</th>
         <th>Usuario</th>
         <th>Concepto</th>
         <th>Comentario</th>
         <th>Valor</th>
         <th>Fecha Pago</th>
         <th>
            Archivo
            
         </th>
         <!-- <th>Solicitud</th>
         <th>Estado</th>
         <th>Responsable</th>
         <th>Creado</th>
         <th class="td_center"><i class="icon-time"></i></th>
         <th class="td_center">Comentario</th> -->
         <th class="td_center"><i class="icon-time"></i></th>
      </tr>
   </thead>
   <tbody>
   @foreach($pagos as $payment)
      <tr style="text-align:center;">
         <td><a class="btn btn-success" style="{{$payment['id_compra']}}" data-target="#ajax" id="{{$payment['id_compra']}}" data-toggle="modal" onclick='cargarDetalleCompra($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a></td>
         <td>
            <a class=" btn btn-default" data-target="#ajax" id="{{$payment['hid']}}" data-toggle="modal" onclick='cargar_datos($(this).attr("id"));return false;'>{{$payment['id']}}</a>
         </td>
         <td>
            <span class="label label-sm {{$payment['label']}}">{{$payment['status']}}</span>
         </td>
         <td>{{$payment['user']}} </td>
         <td>{{$payment['concept']}} {{$payment['description']}}</td>
         <td>{{$payment['comment']}}</td>
         <td>{{$payment['value']}}</td>
         <td>{{$payment['f_pago']}}</td>
         <td><a href="../{{$payment['file']}}" class="btn btn-success" target="_blanc"><i class="icon-paper-clip"></i></a></td>
         <td>{{$payment['time']}}</td>
      </tr>
   @endforeach
   </tbody>
</table>

<div class="pagination">
{{$pag->appends(array("code" => Input::get('code'),"applicant" => Input::get('applicant'),"responsible" => Input::get('responsible'),"status" => Input::get('status'),"priority" => Input::get('priority')))->links()}}
</div>

<script type="text/javascript">
function realizarProceso(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1,
      "estado": '4'
   };
   $.ajax({
      data: parametros,
      url:  'aprobacion',
      type: 'get',

      success: function(response){
            alert('se gurado todo');
            recargarTabla();
      }
   });
}
function realizarProceso2(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1,
      "estado": '3'
   };
   $.ajax({
      data: parametros,
      url:  'aprobacion',
      type: 'get',

      success: function(response){
            alert('se gurado todo');
            recargarTabla();
      }
   });
}
function cargarDetalleCompra(id){
   var parametros = {
      "id": id
   };
   $.ajax({
      data: parametros,
      url:  '../compras/detallehistorico',
      type: 'get',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
function cargar_datos(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1,
      "valorCaja2": valorCaja1
   };
   $.ajax({
      data: parametros,
      url:  'modalhistorico',
      type: 'post',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
</script>

