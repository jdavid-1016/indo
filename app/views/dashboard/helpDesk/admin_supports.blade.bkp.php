<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.4.min.js"></script> 
   <div class="page-content">
      <!-- BEGIN PAGE HEADER-->
      <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Administrar
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Página Principal</a> 
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="javascript:;">Help Desk</a>
                  <i class="icon-angle-right"></i> 
               </li>
               <li>
                  <a href="javascript:;">Administrar</a> 
               </li>
            </ul>
         </div>
      </div>
      <div class="row" id="resultado">
         <div class="col-md-12">
            <div class="table-responsive">
               <div class="tabbable tabbable-custom">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="allSupports">Solicitudes</a></li>
                     <li class=""><a href="adminHistorico">Histórico</a></li>
                     <li class=""><a href="indoamericana/soporte/adminCampos">Campos</a></li>
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane active" id="all-supports">
                        <div class="table-responsive">
                           <table class="table top-blue" id="admin-support" data-target="soporte/allSupports/" ng-controller="help_deskCtrl" ng-app>
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>
                                       Ticket <i class="icon-sort-up" ng-click="ordenarPor('id')"></i>
                                       <i class="icon-sort-down" ng-click="ordenarPor('-id')"></i>
                                    </th>
                                    <th>Editar</th>
                                    <th>Solicitó</th>
                                    <th>
                                       Prioridad
                                       <i class="icon-sort-up" ng-click="ordenarPor('user_priority')"></i>
                                       <i class="icon-sort-down" ng-click="ordenarPor('-user_priority')"></i>
                                    </th>
                                    <th>Solicitud</th>
                                    <th>Estado</th>
                                    <th>Responsable</th>
                                    <th>Creado<a href="indoamericana/soporte/allSupports/created"><!--<i class="icon-sort" ></i>--></a></th>
                                    <th class="td_center"><i class="icon-time"></i></th>
                                    <th class="td_center">Comentario</th>
                                    <!--<th class="td_center"><i class="icon-time"></i></th>-->
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr ng-repeat="support in supports | orderBy:ordenSeleccionado" class="@{{support.row_color}} ">
                                    <td>
                                    </td>
                                    <td class="td_center">
                                       @{{support.id}}
                                    </td>
                                    <td class="td_center">
                                       <a class=" btn btn-default" data-target="#ajax" name="{{support.hid}}" id="3" data-toggle="modal" onclick='realizarProceso($(this.attr("id"));return false;'><i class="icon-edit"></i></a>
                                    </td>
                                    <td class="td_center">
                                       @{{support.user}}
                                    </td>
                                    <td class="td_center">
                                       <span class="badge @{{support.class_priority}}">@{{support.user_priority}}</span>
                                    </td>
                                    <td >
                                    <div class="comment more">
                                       @{{ support.message }}
                                       
                                    </div>
                                    </td>
                                    <td class="td_center">
                                       <span class="label label-sm @{{support.label}} ">@{{ support.state }}</span>
                                    </td>
                                    <td class="td_center">
                                       responsable
                                    </td>
                                    <td class="td_center">
                                       @{{ support.created_at|date}}
                                    </td>
                                    <td class="td_center">
                                       <div class="alert alert-warning">
                                          @{{support.tiempo}}
                                       </div>
                                    </td>
                                    <td class="td_center">
                                       comentario
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                           </table>
                           <div class="pagination">
                              
                           </div>
                        </div>
                        <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                           <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <script type="text/javascript">
   function realizarProceso(valorCaja1){
      var parametros = {
         "valorCaja1": valorCaja1
      };
      $.ajax({
         data: parametros,
         url:  'gestionajax',
         type: 'post',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   </script>
     