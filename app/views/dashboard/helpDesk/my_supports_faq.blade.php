	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title">
					Mis Solicitudes
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="javascript:;">Página Principal</a> 
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:;">Help Desk</a>
						<i class="icon-angle-right"></i> 
					</li>
					<li>
						<a href="javascript:;">FAQ</a> 
					</li>
				</ul>
			</div>
		</div>
		<div class="row">
         	<div class="col-md-12">
	            <div class="table-responsive">
	               <div class="tabbable tabbable-custom">
	                    <ul class="nav nav-tabs">
	                    	<li class=""><a href="my_supports">Nueva solicitud</a></li>
	                    	<li class=""><a href="my_supports_pending">Mis solicitudes</a></li>
	                    	<li class=""><a href="my_supports_history">Histórico</a></li>
	                    	<li class="active"><a href="my_supports_faq" style="background:#E6E6E6">FAQ</a></li>
	                  	</ul>
	                  	<div class="tab-content">
	                    	<div class="tab-pane active" id="fqa_supports">
		                        <div class="row">
						            <div class="col-md-12">
						        		<div class="col-md-3">
						        		   <ul class="ver-inline-menu tabbable margin-bottom-10">
						        		      @foreach($categories as $category)
						        		      <?php $num = 0; ?>
						        		      	@foreach($support_faqs as $support_faq)
							        		      	@if($category->id == $support_faq->support_category_id)
							        		      		<?php $num++; ?>
							        		      	@endif
						        		      	@endforeach
						        		      	<li onclick="welcomeocultar();"><a data-toggle="tab" href="#tab_{{$category->id}}"><i class="icon-group"></i> {{$category->name}} ( {{$num}} )</a></li>

						        		      @endforeach
						        		   </ul>
						        		</div>
						        		<div class="col-md-9">
						        		   	<div class="tab-content">
						        		   	@foreach($categories as $category)
						        		   	<div id="tab_{{$category->id}}" class="tab-pane">
						        		      	@foreach($support_faqs as $support_faq)
						        		      	@if($category->id == $support_faq->support_category_id)
						        		      	
						        		         	<div id="accordion1" class="panel-group">
						        		            	<div class="panel panel-default">
						        		               		<div class="panel-heading">
						        		                  		<h4 class="panel-title">
						        		                     		<a estado="0" id="faq_cont_{{$support_faq->id}}" onclick="guardarcontador($(this).attr('estado'), {{$support_faq->id}});" class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_{{$support_faq->id}}">
						        		                     		{{$support_faq->title}}
						        		                     		</a>
						        		                  		</h4>
						        		               		</div>
						        		               		@if(Input::get('id') == $support_faq->id)

						        		               		<div id="accordion1_{{$support_faq->id}}" class="panel-collapse collapse in">
						        		               		@else
						        		               		<div id="accordion1_{{$support_faq->id}}" class="panel-collapse collapse">
						        		               		@endif
						        		                  		<div class="panel-body">
						        		                  		<div class="scroller" id="" style="height: 600px;" data-always-visible="1" data-rail-visible1="1">
						        		                  			
						        		                   		{{$support_faq->text}}  
						        		                  		</div>
						        		                  		</div>
						        		               		</div>
						        		            	</div>
						        		         	</div>
						        		      	@endif
						        		      	

						        		      	@endforeach
						        		   	</div>
						        		   		
						        		   	@endforeach
						        		     
						        		   	</div>
						        		</div>        
									</div>
									<div class="col-md-8" id="welcome-ocultar">
									    <div id="tab_asdjfkja" class="tab_pane">
									    	<div id="accordion1" class="panel-group">
									    			<div class="panel panel-default">
									    		   		<div class="panel-heading">
									    		      		<h4 class="panel-title">
									    		         		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_">
									    		         		Bienvenido
									    		         		</a>
									    		      		</h4>
									    		   		</div>
									    		   		<div id="accordion1_" class="panel-collapse collapse in">
									    		   			<div class="panel-body">
									    		   				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									    		   				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									    		   				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									    		   				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
									    		   				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
									    		   				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

									    		   				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
									    		   				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
									    		   				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
									    		   				consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
									    		   				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
									    		   				proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
									    		   			</div>
									    		   		</div>
									    			</div>
									    	</div>
									    	
									    </div>  
									</div>
								</div>
	                    	</div>   
	                    </div>
	                </div>
	            </div>
         	</div>
        </div>
    </div>
</div>

<script>
    function welcomeocultar(){
        $("#welcome-ocultar").css("display", "none");
    }
    
    function guardarcontador(estado, id){
        if(estado==0){
            
            var parametros = {
            id: id
            };
        
        $.ajax({
            data: parametros,
            url:  'faqcontador',
            type: 'get',
            success: function(response){                
                
            }
        });
                        
            $("#faq_cont_"+id).attr('estado', '1');
       
        }else if(estado==1){
                        
            $("#faq_cont_"+id).attr('estado', '0');
            
        }
        
    }
    
    </script>