<table class="table top-blue">
   <thead>
      <tr>
         <th></th>
          <th>Atender</th>
         <th>
            Ticket 
         </th>
        
         <th>Solicitó</th>
         <th>
            Prioridad
            
         </th>
         <th>Solicitud</th>
         <th>Estado</th>
         <th>Responsable</th>
         <th>Creado</th>
         <th class="td_center"><i class="icon-time"></i></th>
         <th class="td_center">Comentario</th>
         <!--<th class="td_center"><i class="icon-time"></i></th>-->
      </tr>
   </thead>
   <tbody>
      @foreach($supports as $support)
      <tr style="">
         <td></td>
         <td class="td_center">

            <a class=" btn btn-default btn_juego" {{$support['button_style']}} data-target="#ajax" id="{{$support['hid']}}" data-toggle="modal" onclick='atendSupport($(this).attr("id"));return false;'><i class="{{$support['icon-btn']}}"></i></a>
            
         </td>
         <td class="td_center">
            {{$support['id']}}
         </td>
         <td class="td_center">
            {{$support['user']}}
         </td>
         <td class="td_center">
            <span class="badge {{$support['class_priority']}}">{{$support['user_priority']}}</span>
         </td>
         <td class="td_center">
            {{$support['message'] }}
         </td>
         <td class="td_center">
            <span class="label label-sm {{$support['label']}} ">{{$support['state'] }}</span>
         </td>
         <td class="td_center">
            {{$support['responsible']}}
         </td>
         <td class="td_center">
            {{$support['created_at']}}
         </td>
         <td class="td_center">
            <div class="alert alert-warning">
               {{$support['tiempo']}}
            </div>
         </td>
         <td class="td_center">
            {{$support['observation']}}
         </td>
         
      </tr>
      @endforeach
   </tbody>
</table>
<div class="pagination">
</div>
<a class=" btn btn-default btn_juego" data-target="#modal_juego"  data-toggle="modal"><i class="icon-download"></i></a>



