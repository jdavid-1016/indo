
<table class="table top-blue" data-target="soporte/callSupport/">
   <thead>
      <tr>
         <th></th>
         <th>
            Ticket 
         </th>
         <th>Editar</th>
         <th>Solicitó</th>
         <th>
            Prioridad
            
         </th>
         <th>Solicitud</th>
         <th>Estado</th>
         <th>Responsable</th>
         <th>Creado</th>
         <th class="td_center"><i class="icon-time"></i></th>
         <th class="td_center">Comentario</th>
         <!--<th class="td_center"><i class="icon-time"></i></th>-->
      </tr>
   </thead>
   <tbody>
   @foreach($supports as $support)
      <tr class="{{$support['row_color']}} " style="{{$support['display']}}" id="{{$support['ocult']}}" >
         <td>
            <i class="{{$support['scaled']}}" id="{{$support['id_plus']}}" onclick="funcion2($(this).attr('id'));return false;"></i>
         </td>
         <td class="td_center">
            {{$support['id']}}
         </td>
         <td class="td_center">
            <a class=" btn btn-default" style="{{$support['display']}}" data-target="#ajax" id="{{$support['hid']}}" data-toggle="modal" onclick='realizarProceso($(this).attr("id"));return false;'><i class="icon-edit"></i></a>
         </td>
         <td class="td_center">
            {{$support['user']}}
         </td>
         <td class="td_center">
            <span class="badge {{$support['class_priority']}}">{{$support['user_priority']}}</span>
         </td>
         <td class="td_center">
         <div class="comment more">
            {{$support['message'] }}
            
         </div>
         </td>
         <td class="td_center">
            <span class="label label-sm {{$support['label']}}" >{{$support['state'] }}</span>
         </td>
         <td class="td_center">
         <span class="label label-sm {{$support['responsible_color']}} ">
         {{$support['responsible']}}
         {{$support['responsible_scaled']}}
         </span>
            
         </td>
         <td class="td_center">
            {{$support['created_at']}}
         </td>
         <td class="td_center">
            <div class="alert alert-warning">
               {{$support['tiempo']}}
               
            </div>
         </td>
         <td class="td_center">
            {{$support['observation']}}
         </td>
      </tr>
   @endforeach
   </tbody>
</table>

<div class="pagination">
</div>

<script type="text/javascript">
function realizarProceso(valorCaja1){
   var parametros = {
      "valorCaja1": valorCaja1
   };
   $.ajax({
      data: parametros,
      url:  'gestionajax',
      type: 'post',

      success: function(response){
            $("#ajax").html(response);
      }
   });
}
</script>

