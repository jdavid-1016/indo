
<div class="page-content">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Administrar</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class=""><a href="respond_supports">Solicitudes</a></li>
                  <li class="active"><a href="my_supports_asign_history">Histórico</a></li>
                  <li class=""><a href="statistics_tec" >Estadisticas</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul> 
              <div class="tab-content">
                 <div class="tab-pane active" id="historico">
                    <div class="table-responsive">
                       <div>
                          <form class="form-inline" action="my_supports_asign_history" method="get">
                             <div class="search-region">
                                <div class="form-group">
                                   <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                                </div>
                                <div class="form-group">
                                   <select class="form-control input-medium" name="applicant">
                                   <option value="">Solicitó</option> 
                                @foreach($applicants as $applicant)
                                @if($applicant->id == Input::get('applicant'))
                                   <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                                @else
                                   <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                                @endif  
                                @endforeach
                                   
                                   
                                   </select>
                                </div>
                                <div class="form-group">
                                   <input type="text" class="form-control" id="text" name="text" placeholder="Texto" value="{{Input::get('texto')}}">
                                </div>
                                <!-- <div class="form-group">
                                   <select class="form-control input-medium" name="responsible">
                                   <option value="">Responsable</option>   
                                   @foreach($technicals as $technical)
                                   @if($technical->id == Input::get('responsible'))
                                      <option value="{{$technical->id}}" selected="">{{$technical->name}} {{$technical->last_name}}</option>
                                   @else
                                      <option value="{{$technical->id}}">{{$technical->name}} {{$technical->last_name}}</option>
                                   @endif 
                                   @endforeach
                                   
                                   </select>
                                </div> -->
                                <div class="form-group">
                                   <select class="form-control input-small" name="status">
                                      <option value="">Estado</option>
                                      <option  value="2">cerrado</option>
                                      <option  value="3">rechazado</option>
                                   </select>
                                </div>
                                <div class="form-group">
                                   <select class="form-control input-small" name="priority">
                                      <option value="">Prioridad</option>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                      <option value="5">5</option>
                                   </select>
                                </div>
                                <div class="form-group">
                                   <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                                </div>    
                             </div>
                          </form> 
                       </div>
                       <table class="table top-blue" id="admin-support" data-target="soporte/allSupports/">
                          <thead>
                             <tr>
                                
                                <th>Ticket</th>
                                <th>Solicitó</th>
                                
                                <!--<th>Categoría</th>-->
                                <th>Estado</th>
                                <th>Solicitud</th>
                                
                                
                                <th>Cerrado<a href="/indoamericana/soporte/allSupports/closed"><!--<i class="icon-sort" ></i>--></a></th>
                                <th><i class="icon-time"></i></th>
                                <th>Comentario del Tecnico</th>
                                
                             </tr>
                          </thead>
                          <tbody>
                          @foreach($supports as $support)
                                
                            <tr>
                               <td class="td_center">{{$support['id']}}</td>
                               <td class="td_center">{{$support['user']}}</td>
                               
                              
                               <td class="td_center"><span class="label label-sm  {{$support['label']}}">{{$support['state']}}</span></td>
                               <td class="td_center"><div class="comment more">{{$support['message']}}</div></td>
                               
                               
                               <td class="td_center">{{$support['closed_at']}}</td>
                               <td class="td_center">
                                  <div class="alert alert-warning">
                                     {{$support['time']}}
                                  </div>
                               </td>  
                               <td class="td_center"><div class="comment more">{{$support['observation']}}</div></td>
                               
                            </tr>
                                      
                                     
                                         
                           @endforeach           
                          </tbody>
                       </table>
                       <div class="pagination">
                         {{$pag->appends(array("code" => Input::get('code'),"applicant" => Input::get('applicant'),"text" => Input::get('text'),"status" => Input::get('status'),"priority" => Input::get('priority')))->links()}}
                       </div>
                    </div>
                    <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                       <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                    </div>
                 </div>
              </div> 
              </div>
           </div>
        </div>
     </div>
  </div>     