   <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Mis Solicitudes
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Página Principal</a> 
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="javascript:;">Help Desk</a>
                  <i class="icon-angle-right"></i> 
               </li>
               <li>
                  <a href="javascript:;">Mis Solicitudes</a> 
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <div class="tabbable tabbable-custom">
                  <ul class="nav nav-tabs">
                     <li class="active"><a href="my_supports">Nueva solicitud</a></li>
                     <li class=""><a href="my_supports_pending">Mis solicitudes</a></li>
                     <li class=""><a href="my_supports_history">Histórico</a></li>
                     <li class=""><a href="my_supports_faq" style="background:#E6E6E6">FAQ</a></li>
                     
                  </ul>
                  <div class="tab-content">
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     <div class="tab-pane active" id="admin_suppotrs" ng-controller="help_deskCtrl" ng-app>
                        @if(count($pending) >= 1)
                        <div class="note note-warning">
                              <h4 class="block">Atención:</h4>
                              <p>
                                 Tiene solicitudes pendientes por calificar, para solicitar un nuevo soporte debes calificar la asistencia ténica prestada. <a class="btn btn-success btn-xs" href="my_supports_history"> <i class="icon-ok"></i> Calificar</a>
                              </p>
                        </div>

                        @elseif(count($old_support) == "1")
                        <div class="note note-warning">
                              <h4 class="block">Atención:</h4>
                              <p>
                                 No puede Ingresar mas solicitudes hasta que no sean cerradas todas las solicitudes pendientes. <a class="btn btn-success btn-xs" href="my_supports_pending"> <i class="icon-ok"></i>Ver Mis Solicitudes Pendientes</a>
                              </p>
                        </div>

                        @else

                           <form class="form-horizontal" ng-submit="newSupport()">
                              <div class="form-body">
                                 
                                    <!-- <div class="error alert alert-danger"></div> -->
                                 
                                 <div class="form-group">
                                    <label class="col-md-1 control-label">Descripción</label>
                                    <div class="col-md-11">
                                       <textarea name="description" id="description" class="form-control" rows="2" maxlength="480" ng-model="description"></textarea>
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <label class="col-md-1 control-label">Categoría</label>
                                    <div class="col-md-2">
                                       <select name="category" id="category" class="form-control" ng-model="category">
                                          <option value="">Categoría</option>
                                          <option value="@{{category.id}}" ng-repeat="category in categories">@{{category.name}}</option>
                                       </select>
                                    </div>
                                    <label class="col-md-1 control-label">Prioridad</label>
                                    <div class="col-md-2">
                                       <select name="priority" id="priority" class="form-control" ng-model="priority">
                                          <option value=""></option>
                                          <option value="5">5</option>
                                          <option value="4">4</option>
                                          <option value="3">3</option>
                                          <option value="2">2</option>
                                          <option value="1">1</option>
                                       </select>
                                       <span class="help-block">1 es Urgente, 5 es Leve </span>
                                    </div>
                                 </div>
                              </div>
                              <div class="form-actions fluid">
                                 <div class="col-md-1 col-md-11">
                                    <button type="submit" class="btn btn-success">Enviar</button>
                                 </div>
                              </div>
                           
                           </form>
                        @endif

                     </div>
                     <div id="support_faq">
                         
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div> 

