<div class="modal-dialog">
  <div class="modal-content">
    <form>
    <input type="hidden" ng-model="support_id" value="{{$data->id}}" id="support_id">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        <h4 class="modal-title">Atender solicitud</h4>
      </div>
      <div class="modal-body form-body">
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Solicitud</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-edit"></i></span>
                <textarea rows="2" readonly name="message" class="form-control">{{$data->message}}</textarea>
              </div> 
            </div>
          </div>      
        </div> 
        <div class="row">
          <div class="col-md-5">
            <div class="form-group"> 
              <label class="control-label">Solicitó</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-edit"></i></span>
                <input readonly name="applicant" type="text" class="form-control" data-required="true" value="{{$data->Users->name}} {{$data->Users->last_name}}">
              </div> 
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group"> 
              <label class="control-label">Prioridad</label>
              <input readonly name="status" type="text" class="form-control" placeholder="" data-required value="{{$data->priority}}">
            </div>   
          </div> 
          <div class="col-md-5">
            <div class="form-group">
              <label class="control-label">Creado</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-time"></i></span>
                <input readonly name="created" type="text" class="form-control" placeholder="" data-required value="{{$data->created_at}}">
              </div> 
            </div>
          </div>
        </div>
        <div class="row"> 
          <div class="col-md-3" >
            <div class="form-group">
              <div class="input-group">
                <input  type="text" class="form-control" value="Escalar">
                <span class="input-group-addon">
                  <span><input name="escalar" type="checkbox" class="escalar" id="escalar" onclick="funcion($(this))" ></span>
                </span>
              </div>
            </div>
          </div>
          <div class="col-md-9" id="select_responsible" style="display:none" >  
            <div class="form-group">
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-edit"></i></span>
                <select name="responsible" id="responsible" class="form-control">
                  <option></option>
                  @foreach($technicals as $technical)
                  <option value="{{$technical->id}}">{{$technical->name}} {{$technical->last_name}}</option>
                  @endforeach
                </select>
              </div> 
            </div>
          </div> 
        </div>  
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="control-label">Comentario</label>
              <div class="input-group">
                <span class="input-group-addon"><i class="icon-edit"></i></span>
                <textarea rows="2" name="observation" class="form-control" required id="observation"></textarea>
              </div> 
            </div>
          </div>      
        </div>  
        <div class="row"> 
          <div class="col-md-3" >
            <div class="form-group">
              <div class="input-group">
                <input  type="text" class="form-control" value="Cerrar" disabled="">
                <span class="input-group-addon">
                  <span><input name="close" type="checkbox" onchange="mostrar_form_nav()" value="true" id="closed"  @if($data->support_status_id == 4)checked=""@else @endif></span>
                </span>
              </div>
            </div>
          </div>
        </div> 
        <!-- <hr> -->
        <div class="col-md-12" id="form_navegacion" @if($data->support_status_id == 4)  @else style="display:none" @endif>
            <div class="note note-info">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group"> 
                            <label class="control-label">Velocidad de Descarga</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <input id="descarga" type="text" class="form-control" placeholder="Mb/s">
                            </div> 
                        </div>
                    </div>
                  
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Velocidad de Subida</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-time"></i></span>
                                <input id="subida" type="text" class="form-control" placeholder="Mb/s" >
                            </div> 
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="alert alert-info">
            <strong>Info!</strong> Señor tecnico recuerde realizar el Test de Velocidad al atender este el soporte.
        </div>
      </div>         
      <div class="modal-footer">
        <button type="button" class="btn btn-success" onclick="attend_support()">@if($data->support_status_id == 4) Cerrar @else Atender @endif</button>
        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Volver</button>  
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">
    function mostrar_form_nav(){
        if ($("#closed").is(':checked')) {
            $("#form_navegacion").show('show');
        } else {
            $("#form_navegacion").hide('slow');
        }
    }
</script>