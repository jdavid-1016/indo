<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Gestion FAQ
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Help Desk</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Gestion FAQ</a> 
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <ul class="page-breadcrumb breadcrumb">
                <h4><b>Ingresa una nueva FAQ</b></h4>
            </ul>
        </div>
    </div>
    <div class="row">
        <form class="form-horizontal" id="form">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-1 control-label">Categoría</label>
                    <div class="col-md-2">
                        <select name="categoria" id="categoria" class="form-control">
                            @foreach ($categorias as $categoria)
                            <option value="{{ $categoria->id }}">{{ $categoria->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label">Pregunta</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="pregunta" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-1 control-label">Descripción</label>
                    <div class="col-md-6">
                        <textarea class="ckeditor form-control" id="descripcion" name="descripcion" rows="6"></textarea>
                    </div>
                </div>
            </div>
            <div class="form-actions fluid">
                <div class="col-md-1 col-md-11">
                    <a class="btn btn-success" onclick="guardarfaq()">Guardar</a>
                </div>
            </div>

        </form>
    </div>
</div>