
<div class="page-content" id="adminhelpdesk">
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Administrar</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  <li class="active"><a href="#tab_1_1" data-toggle="tab">Solicitudes</a></li>
                  <li class=""><a href="adminHistorico" >Histórico</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_admin_supports">
                        
                        @include('dashboard.helpDesk.table_admin_supports')
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                     
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>
   <script type="text/javascript">
   function realizarProceso(valorCaja1){
      var parametros = {
         "valorCaja1": valorCaja1
      };
      $.ajax({
         data: parametros,
         url:  'gestionajax',
         type: 'post',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   </script>
   <script type="text/javascript">
   function cargaHistorico(){
     
      $.ajax({
         data: null,
         url:  'adminHistorico',
         type: 'get',

         success: function(response){
               $("#historico").html(response);
         }
      });
   }
   function funcion2(hola){
      var hola = hola;

     
      $('#ocult'+hola).toggle('slow');
   }   
   
   function recargarTabla(){
       
       var html = $.ajax({
                     type: "GET",
                     url: "admin_supports",
                     cache: false,
                     data: {nombre: "hola"},
                     async: false
                    }).responseText;
                                        
        $('#table_admin_supports').html(html);
       
   }
   setInterval(recargarTabla, 300000);
   
   function buscarPendientes(){
       
       var html = $.ajax({
                     type: "GET",
                     url: "buscar_pendientes",
                     cache: false,
                     data: {nombre: "hola"},
                     async: false
                    }).responseText;
                    
                    if(html==1){
                     $("#sound").html('<EMBED SRC="assets/WAV/Hope.wav" AUTOSTART=TRUE WIDTH=1 HEIGHT=1>')
                     toastr.info('Tiene solicitudes pendientes sin asignar', 'Solicitudes sin asignar');
                    }
       
   }
   setInterval(buscarPendientes, 100000);
   </script>