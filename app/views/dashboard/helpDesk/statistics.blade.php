        <!DOCTYPE html>
        <html dir="ltr" lang="en-US">
           <head>
              <meta charset="UTF-8" />
              <title>A date range picker for Bootstrap</title>
              <!-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> -->
              <link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
              <link rel="stylesheet" type="text/css" media="all" href="assets/calendario/daterangepicker-bs3.css" />
              <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
              <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
              <script type="text/javascript" src="assets/calendario/moment.js"></script>
              <script type="text/javascript" src="assets/calendario/daterangepicker.js"></script>
           </head>
           <body>
	<div class="page-content">
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title">
					Estadísticas
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="icon-home"></i>
						<a href="javascript:;">Página Principal</a> 
						<i class="icon-angle-right"></i>
					</li>
					<li>
						<a href="javascript:;">Help Desk</a>
						<i class="icon-angle-right"></i> 
					</li>
					<li>
						<a href="javascript:;">Estadísticas</a> 
					</li>
                                                      <li></li>
                                                      <li>
                                                            <label class="btn btn-success btn-sm" onclick="exportarMensual()">
                                                                <span class="icon-sort-by-attributes-alt"></span>
                                                                Exportar
                                                           </label>
                                                      </li>
					<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
					   <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
					   <span></span> <b class="caret"></b>
					</div>
				</ul>

				   <script type="text/javascript">
				   $(document).ready(function() {

	                            var cb = function (start, end, label) {
	                                console.log(start.toISOString(), end.toISOString(), label);
	                                var fecha_inicio = start.format('YYYY-M-D');
	                                var fecha_fin = end.format('YYYY-M-D');
	                                


	                                document.getElementById("graphic_technical").src = "graphic_technical?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
	                                document.getElementById("graphic_rating").src = "graphic_rating?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
	                                document.getElementById("graphic_time").src = "graphic_time?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
	                                document.getElementById("graphic_user").src = "graphic_user?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
	                                document.getElementById("graphic_category").src = "graphic_category?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
	                                document.getElementById("graphic_statuses").src = "graphic_statuses?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";

	                                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	                                //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
	                            }

				      var optionSet1 = {
				        startDate: moment().subtract(29, 'days'),
				        endDate: moment(),
				        minDate: '01/01/2012',
				        maxDate: '12/31/2015',
				        dateLimit: { days: 60 },
				        showDropdowns: true,
				        showWeekNumbers: true,
				        timePicker: false,
				        timePickerIncrement: 1,
				        timePicker12Hour: true,
				        ranges: {
				           'Today': [moment(), moment()],
				           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				           'This Month': [moment().startOf('month'), moment().endOf('month')],
				           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				        },
				        opens: 'left',
				        buttonClasses: ['btn btn-default'],
				        applyClass: 'btn-small btn-primary',
				        cancelClass: 'btn-small',
				        format: 'MM/DD/YYYY',
				        separator: ' to ',
				        locale: {
				            applyLabel: 'Submit',
				            cancelLabel: 'Clear',
				            fromLabel: 'From',
				            toLabel: 'To',
				            customRangeLabel: 'Custom',
				            daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
				            monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				            firstDay: 1
				        }
				      };

				      var optionSet2 = {
				        startDate: moment().subtract(7, 'days'),
				        endDate: moment(),
				        opens: 'left',
				        ranges: {
				           'Today': [moment(), moment()],
				           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				           'This Month': [moment().startOf('month'), moment().endOf('month')],
				           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
				        }
				      };

				      $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

				      $('#reportrange').daterangepicker(optionSet1, cb);

				      $('#reportrange').on('show.daterangepicker', function() { console.log("show event fired"); });
				      $('#reportrange').on('hide.daterangepicker', function() { console.log("hide event fired"); });
				      $('#reportrange').on('apply.daterangepicker', function(ev, picker) { 
				        console.log("apply event fired, start/end dates are " 
				          + picker.startDate.format('MMMM D, YYYY') 
				          + " to " 
				          + picker.endDate.format('MMMM D, YYYY')
				        ); 
				      });
				      $('#reportrange').on('cancel.daterangepicker', function(ev, picker) { console.log("cancel event fired"); });

				      $('#options1').click(function() {
				        $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
				      });

				      $('#options2').click(function() {
				        $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
				      });

				      $('#destroy').click(function() {
				        $('#reportrange').data('daterangepicker').remove();
				      });

				   });
				   </script>

				
			</div>
		</div>
		<div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-bar-chart"></i>Solicitudes atendidas
						
						</div>
						<div class="actions">
							<a class="btn btn-success" href="graphic_technical?exportar=1"><span class="icon-sort-by-attributes-alt"></span>Exportar</a>
						</div>
						<div class="tools">
	                        
	                    
	                    </div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<div></div>
							<div id="sol_atendidas"></div>
							
								
								<img width="100%" height="500px" src="graphic_technical" id="graphic_technical">
							
						</div>		
					</div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-bar-chart"></i>Puntaje promedio</div>
						<div class="actions">
							<a class="btn btn-success" href="graphic_rating?exportar=1"><span class="icon-sort-by-attributes-alt"></span> Exportar</a>
						</div>
						<div class="tools">
	                        
	                    </div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<div></div>
							<div id="pun_promedio"></div>
							
								<img width="100%" height="500px" src="graphic_rating" id="graphic_rating">
								
							
						</div>   
					</div>
				</div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-bar-chart"></i>Tiempo promedio de atención (Horas)</div>
						<div class="actions">
							<a class="btn btn-success" href="graphic_time?exportar=1"><span class="icon-sort-by-attributes-alt"></span> Exportar</a>
						</div>
						<div class="tools">
	                        
	                    </div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<div></div>
							<div id="sol_tiempo"></div>
							
								<img width="100%" height="500px" src="graphic_time" id="graphic_time">
						</div>		
					</div>
                </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-bar-chart"></i>Usuarios que más solicitan soporte</div>
						<div class="actions">
							<a class="btn btn-success" href="graphic_user?exportar=1"><span class="icon-sort-by-attributes-alt"></span> Exportar</a>
						</div>
						<div class="tools">
	                        
	                    </div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<div></div>
							<div id="sol-mas-soporte"></div>
							<img width="100%" height="500px" src="graphic_user" id="graphic_user">
							
						</div>		
					</div>
                </div>
            </div>
		    <div class="col-md-4 col-sm-12">
                <div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-bar-chart"></i>Soportes por Categoría</div>
						<div class="actions">
							<a class="btn btn-success" href="graphic_category?exportar=1"><span class="icon-sort-by-attributes-alt"></span> Exportar</a>
						</div>
						<div class="tools">
	                        
	                    </div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<div></div>
							<div id="sol-categorias"></div>
							<img width="100%" height="500px" src="graphic_category" id="graphic_category">
							
						</div>		
					</div>
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-bar-chart"></i>Soportes por Estados</div>
						<div class="actions">
							<a class="btn btn-success" href="graphic_statuses?exportar=1"><span class="icon-sort-by-attributes-alt"></span> Exportar</a>
						</div>
						<div class="tools">
	                        
	                    </div>
					</div>
					<div class="portlet-body">
						<div class="table-responsive">
							<div></div>
							<div id="sol-estados"></div>
							<img width="100%" height="500px" src="graphic_statuses" id="graphic_statuses">
							
						</div>		
					</div>
                </div>
            </div>
        </div>




              <div class="container">
                 <div class="span12">

                  <!-- <h1>Usage Examples</h1>

                  <hr />

                    <h4>Basic Date Range Picker</h4>
                    <div class="well">

                       <form class="form-horizontal">
                         <fieldset>
                          <div class="control-group">
                            <div class="controls">
                             <div class="input-prepend input-group">
                               <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span><input type="text" style="width: 200px" name="reservation" id="reservation" class="form-control" value="03/18/2013 - 03/23/2013" /> 
                             </div>
                            </div>
                          </div>
                         </fieldset>
                       </form>

                       <script type="text/javascript">
                       $(document).ready(function() {
                          $('#reservation').daterangepicker(null, function(start, end, label) {
                            console.log(start.toISOString(), end.toISOString(), label);
                          });
                       });
                       </script>

                    </div>


                    <h4>Basic Single Date Picker</h4>
                    <div class="well">

                       <form class="form-horizontal">
                         <fieldset>
                          <div class="control-group">
                            <div class="controls">
                             <div class="input-prepend input-group">
                               <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span><input type="text" style="width: 200px" name="birthday" id="birthday" class="form-control" value="03/18/2013" /> 
                             </div>
                            </div>
                          </div>
                         </fieldset>
                       </form>

                       <script type="text/javascript">
                       $(document).ready(function() {
                          $('#birthday').daterangepicker({ singleDatePicker: true }, function(start, end, label) {
                            console.log(start.toISOString(), end.toISOString(), label);
                          });
                       });
                       </script>

                    </div>

                    <h4>Date Range &amp; Time Picker with 30 Minute Increments</h4>
                    <div class="well">

                       <form class="form-horizontal">
                         <fieldset>
                          <div class="control-group">
                            <label class="control-label" for="reservationtime">Choose your check-in and check-out times:</label>
                            <div class="controls">
                             <div class="input-prepend input-group">
                               <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                               <input type="text" style="width: 400px" name="reservation" id="reservationtime" class="form-control" value="08/01/2013 1:00 PM - 08/01/2013 1:30 PM"  class="span4"/>
                             </div>
                            </div>
                          </div>
                         </fieldset>
                       </form>

                       <script type="text/javascript">
                       $(document).ready(function() {
                          $('#reservationtime').daterangepicker({
                            timePicker: true,
                            timePickerIncrement: 30,
                            format: 'MM/DD/YYYY h:mm A'
                          }, function(start, end, label) {
                            console.log(start.toISOString(), end.toISOString(), label);
                          });
                       });
                       </script>

                    </div> -->            

                   


                    <!-- <h4>Plays nicely with Bootstrap dropdowns</h4>

                    <div class="well">

                       <div class="dropdown" style="display: inline-block;">
                         <a data-toggle="dropdown" class="btn btn-primary" href="#">Regular dropdown</a>
                         <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
                           <li><a href="#">item</a></li>
                         </ul>
                       </div>

                       <div id="reportrange2" class="btn" style="display: inline-block; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                          <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                          <span></span> <b class="caret"></b>
                       </div>

                       <script type="text/javascript">
                       $(document).ready(function() {
                          $('#reportrange2 span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));
                          $('#reportrange2').daterangepicker();
                       });
                       </script>

                    </div> -->


                 </div>
              </div>

           </body>
        </html>

    </div>
      