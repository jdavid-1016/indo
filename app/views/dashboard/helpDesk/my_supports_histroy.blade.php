      <div class="page-content">
   <!-- BEGIN PAGE HEADER-->
      <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Mis Solicitudes
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Página Principal</a> 
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="javascript:;">Help Desk</a>
                  <i class="icon-angle-right"></i> 
               </li>
               <li>
                  <a href="javascript:;">Mis Solicitudes</a> 
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <div class="tabbable tabbable-custom">
                  <ul class="nav nav-tabs">
                     <li class=""><a href="my_supports">Nueva solicitud</a></li>
                     <li class=""><a href="my_supports_pending">Mis solicitudes</a></li>
                     <li class="active"><a href="my_supports_history">Histórico</a></li>
                     <li class=""><a href="my_supports_faq" style="background:#E6E6E6">FAQ</a></li>
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane active" id="past_supports">
                        <div class="">
                           <div>
                              <form class="form-inline" action="my_supports_history" method="get">
                                 <div class="search-region">
                                    <div class="form-group">
                                       <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                                    </div>
                                    <div class="form-group">
                                       <select class="form-control input-medium" name="responsible">
                                       <option value="">Responsable</option>   
                                       @foreach($technicals as $technical)
                                       @if($technical->id == Input::get('responsible'))
                                          <option value="{{$technical->id}}" selected="">{{$technical->name}} {{$technical->last_name}}</option>
                                       @else
                                          <option value="{{$technical->id}}">{{$technical->name}} {{$technical->last_name}}</option>
                                       @endif 
                                       @endforeach
                                       
                                       </select>
                                    </div>
                                    <div class="form-group">
                                       <select class="form-control input-small" name="status">
                                          <option value="">Estado</option>
                                          <option  value="2">cerrado</option>
                                          <option  value="3">rechazado</option>
                                       </select>
                                    </div>
                                    <div class="form-group">
                                       <select class="form-control input-small" name="priority">
                                          <option value="">Prioridad</option>
                                          <option value="1">1</option>
                                          <option value="2">2</option>
                                          <option value="3">3</option>
                                          <option value="4">4</option>
                                          <option value="5">5</option>
                                       </select>
                                    </div>
                                    <div class="form-group">
                                       <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                                    </div>    
                                 </div>
                              </form> 
                           </div>
                           <table class="table top-blue" id="admin-support" data-target="soporte/callSupport/"> 
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>Ticket</th>
                                    <th>Estado</th>
                                    <th>Responsable</th>
                                    <th>Categoría</th>
                                    <th>Solicitud</th>
                                    <th>Creado</th>
                                    <th>Cerrado</th>
                                    <th>Tiempo</th>
                                    <th class="td_center">Comentario</th>
                                    <th class="td_center"><i class="icon-ok"></i></th>
                                 </tr>
                              </thead>
                              <tbody>
                              @foreach($supports as $support)
                              
                                 <tr style="{{$support['display']}}" id="{{$support['ocult']}}">
                                    <td><i class="{{$support['scaled']}}" id="{{$support['id_plus']}}" onclick="funcion2($(this).attr('id'));return false;"></i></td>
                                    <td>{{$support['id']}}</td>
                                    <td><span class="label label-sm {{$support['label']}}">{{$support['state']}}</span></td>
                                    <td>{{$support['responsible']}}</td>
                                    <td>{{$support['category']}}</td>
                                    <td>{{$support['message']}}</td>
                                    <td>{{$support['created_at']}}</td>
                                    <td>{{$support['closed_at']}}</td>
                                    <td>{{$support['time']}}</td>
                                    <td>{{$support['observation']}}</td>
                                    @if($support['rating_status'] == 0)
                                    <td id="cal{{$support['hid']}}" class="td_center"><a class="btn btn-success btn-xs" style="{{$support['display']}}" data-target="#ajax" data-toggle="modal" id="{{$support['hid']}}" onclick='realizarProceso($(this).attr("id"));return false;'><i class="icon-ok"></i> Calificar</a></td>      
                                    @else
                                    <td class="td_center"><i class="icon-check"></i></td>             
                                    @endif
                                 </tr>
                              @endforeach
                              </tbody>
                           </table> 
                           <div class="pagination"></div>
                        </div>
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                           <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>           
<script type="text/javascript">
   function realizarProceso(valorCaja1){
      var parametros = {
         "valorCaja1": valorCaja1
      };
      $.ajax({
         data: parametros,
         url:  'rating_support',
         type: 'post',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   </script>

   <script type="text/javascript">
   function funcion2(hola){
      var hola = hola;

     
      $('#ocult'+hola).toggle('slow');
   }
   </script>