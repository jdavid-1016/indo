   <div class="page-content">
   <!-- BEGIN PAGE HEADER-->
      <div class="row">
         <div class="col-md-12">
            <h3 class="page-title">
               Mis Solicitudes
            </h3>
            <ul class="page-breadcrumb breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="javascript:;">Página Principal</a> 
                  <i class="icon-angle-right"></i>
               </li>
               <li>
                  <a href="javascript:;">Help Desk</a>
                  <i class="icon-angle-right"></i> 
               </li>
               <li>
                  <a href="javascript:;">Mis Solicitudes</a> 
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="table-responsive">
               <div class="tabbable tabbable-custom">
                  <ul class="nav nav-tabs">
                     <li class=""><a href="my_supports">Nueva solicitud</a></li>
                     <li class="active"><a href="my_supports_pending">Mis solicitudes</a></li>
                     <li class=""><a href="my_supports_history">Histórico</a></li>
                     <li class=""><a href="my_supports_faq" style="background:#E6E6E6">FAQ</a></li>
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane active" id="my_supports">
                        <div class="table-responsive">
                           <table class="table top-blue" id="admin-support" data-target="soporte/callSupport/" ng-controller="help_deskCtrl" ng-app> 
                              <thead>
                                 <tr>
                                    <th></th>
                                    <th>Ticket</th>
                                    <th>Estado</th>
                                    <th>Responsable</th>
                                    <th>Categoría</th>
                                    <th>Solicitud</th>
                                    <th>Creado</th>
                                    <th>Tiempo</th>
                                    <th class="td_center">Comentario</th>
                                    <th>Anular</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr ng-repeat="supports in supportsPending" style="@{{supports.display}}" id="@{{supports.ocult}}">
                                    <td class="td_center"><i class="@{{supports.scaled}}" id="@{{supports.id_plus}}" onclick="funcion2($(this).attr('id'));return false;"></i></td>
                                    <td class="td_center">@{{supports.id}}</td>
                                    <td class="td_center">
                                       <span class="label label-sm @{{supports.label}} ">@{{ supports.state }}</span>
                                    </td>
                                    <td class="td_center">
                                       <span class="label label-sm @{{supports.responsible_color}} ">@{{ supports.responsible }}</span>
                                    </td>
                                    <td class="td_center">@{{supports.category}}</td>
                                    <td class="td_center">
                                       <div class="comment more">
                                          @{{ supports.message }}
                                       </div>
                                    </td>
                                    <td class="td_center">@{{supports.created_at}}</td>
                                    <td class="td_center">
                                       @{{supports.tiempo}}
                                    </td>
                                    <td class="td_center">@{{supports.observation}}</td>
                                    <td class="td_center">

                                    
                                       <a href="#" id="@{{supports.hid}}" style="@{{supports.cancel}}" class="btn btn-danger delete" onclick='cancel_support($(this).attr("id"));return false;'>
                                          <i class="icon-remove"></i>
                                       </a>
                                    </td>
                                 </tr>
                              </tbody>
                           </table> 
                           <div class="pagination"></div>
                        </div>
                     </div>   
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>           
<script type="text/javascript">

function funcion2(hola){
   var hola = hola;

  
   $('#ocult'+hola).toggle('slow');
}
</script>
@if(Auth::user()->tutorial==0)
<div class="opacidad">
    <div class="padre" id="padre">
      <div class="alert alert-block alert-info fade in tuto" style="display: none;"id="paso2">                        
         <h3 style="text-align:center;" class="alert-heading"><b>Notificaciones en tiempo real.</b></h3>
         <p style="background-color:#F2F2F2; padding:5px; border-radius:5px">
            Ahora, el sistema de gestión de indoamericana le muestra notificaciones en tiempo real.
         </p>
         <p>
            <img style="margin-left: auto; margin-right: auto; display:block;padding-top:10px" src="{{ URL::to("assets/img/tutorial/notificaciones2.png") }}">
         </p>
         <p>
            <a class="btn btn-info" href="#" onclick="Siguiente(2)">Siguiente</a>
         </p>
      </div>

      <div class="alert alert-block alert-info fade in tuto" style="display: none;" id="paso3">                        
         <h3 style="text-align:center;" class="alert-heading"><b>Listado de notificacones</b></h3>
         <p style="background-color:#F2F2F2; padding:5px; border-radius:5px;">
            Nuevo listado de notificaciones que se muestra en tiempo real, adicionalmente se resaltaran las que no has visto.
         </p>
         <p>

         <img style="margin-left: auto; margin-right: auto; display:block; padding-top:10px" src="{{ URL::to("assets/img/tutorial/notificaciones.png") }}">
            
         </p>
         <p>
            <a class="btn btn-info" href="#" onclick="Siguiente(3)">Siguiente</a>
         </p>
      </div>

      <div class="alert alert-block alert-info fade in tuto"  id="paso1">                        
         <h3 style="text-align:center;" class="alert-heading"><b>Help Desk</b></h3>
         <p style="background-color:#F2F2F2; padding:5px; border-radius:5px">
            Anular sus soportes: esta nueva funcionalidad le permitirá anular sus soportes cuando haya logrado resolver el problema y ya no sea necesaria la asistencia de un técnico.
         </p>
         <p>
            <img style="margin-left: auto; margin-right: auto; display:block;padding-top:10px" src="{{ URL::to("assets/img/tutorial/anular.png") }}">
         </p>
         <p>
            <a class="btn btn-info" href="#" onclick="Siguiente(1)">Siguiente</a>
         </p>
      </div>                         
    </div>
</div>

<?php 

    $imagen = Auth::user()->img_min;
    $nombre = Auth::user()->name." ".Auth::user()->last_name;

    ?>
    <input type="hidden" id="img" value="<?php echo $imagen; ?>">
    <input type="hidden" id="name" value="<?php echo $nombre; ?>">
@endif
<script>    
    
    
    
                
      function Siguiente(id){ 

        
        var img = $('#img').val();
        var nombre = $('#name').val();


         switch (id) {
                case 1:  
                
                            
                $('#paso1').fadeOut(0);
                $('#paso2').fadeIn(0);
                toastr.success('<img alt="" class="top-avatar" src="'+img+'"/> </br>El usuario '+nombre+' Cerro su soporte.', 'Soporte cerrado');
                //toastr.success('Reseña actualizada correctamente', 'Actualizacion');
                    break;

                case 2:
                //$("#header_inbox_bar").addClass("quemas");                
                //$("#header_inbox_bar").animate({ 'zoom': 1.2 }, 400);                
                $('#icon_notifications').hover();            
                $('.lista_notificaciones').css({ display: "block"});  
                $('#paso2').fadeOut(0);
                $('#paso3').fadeIn(0);
                var listanot = '<li>\n\
                    <a href="" class="notificacion" id="">\n\
                    <span class="label label-sm label-icon ">\n\
                    <img alt class="top-avatar" src="'+img+'">\n\
                    </span>\n\
                    <strong>'+nombre+' </strong> Cerro su soporte, por favor califiquelo <span class="time"> Hace un momento\n\
                    </span>\n\
                    </a>\n\
                    </li>';
                $("#lista-not").prepend(listanot);
                var cont = $("#cont-not").html();
                cont = parseInt(cont);
                cont = cont + 1;
                var icono = '<i class="icon-warning-sign"></i><span class="badge badge-danger" id="cont-not">'+cont+'</span>';
                $("#icon_notifications").html(icono);
                break;

                case 3:
                //$("#hola").animate({ 'zoom': 1 }, 400);
                //$("#hola").removeClass("quemas");
                $('.lista_notificaciones').css({ display: "none"});  
                $('#paso3').fadeOut(0);
                $('.opacidad').fadeOut(1000);
                window.location = "proceso/7#tab_1_4";
                break;
             }
      }
</script>