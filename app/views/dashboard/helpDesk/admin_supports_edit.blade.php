<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   @foreach($data as $support)
      <input type="hidden" ng-model="support_id" value="{{$support->supports_id}}" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Actualizar solicitud de soporte técnico</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <label class="control-label">Solicitud</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2" readonly class="form-control">{{$support->message}}</textarea>
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Solicito</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$support->Users->name}} {{$support->Users->last_name}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Responsable</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="responsible" class="form-control" ng-model="responsible" required id="responsible">
                        <option value="">Sin asignar</option>

                     @foreach($technicals as $technical)
                     @if($support->user_id == $technical->id)
                        <option value="{{$technical->id}}" selected="">{{$technical->name}} {{$technical->last_name}}</option>
                     @else
                        <option value="{{$technical->id}}">{{$technical->name}} {{$technical->last_name}}</option>
                     @endif
                     @endforeach
                     </select>   
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                  <label class="control-label">Estado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="status" class="form-control" ng-model="status" id="status">
                        @foreach($statuses as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                        @endforeach
                     </select>
                  </div> 
               </div>
            </div>
            <div class="col-md-4">
               <div class="form-group"> 
                  <label class="control-label">Prioridad</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-asterisk"></i></span>
                     <select  id="priority" class="form-control" ng-model="priority" id="priority">
                        <option value="5">5</option>
                        <option value="4">4</option>
                        <option value="3">3</option>
                        <option value="2">2</option>
                        <option value="1">1</option>
                     </select>
                  </div> 
               </div>   
            </div> 
            <div class="col-md-4">
               <div class="form-group">
                  <label class="control-label">Categoría</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-info-sign"></i></span>
                     <select id="category" class="form-control" ng-model="category" id="category">
                        @foreach($categories as $category)
                        @if($support->support_category_id == $category->id)
                        <option value="{{$category->id}}" selected="">{{$category->name}}</option>
                        @else
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endif
                        
                        @endforeach
                     </select>
                  </div> 
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <label class="control-label">Comentario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2"  class="form-control" ng-model="observation" id="observation"></textarea>
                  </div> 
               </div>
            </div>      
         </div>  
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Creado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <input readonly type="text" class="form-control" value="{{$support->created_at}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Tipo Solicitud</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <select id="tipo_solicitud" class="form-control" ng-model="category" id="category">
                     <option value=""></option>
                        @foreach($tipo_soporte as $tipo_soporte)
                          <option value="{{$tipo_soporte->id}}"  @if($support->support_types_id == $tipo_soporte->id) selected="" @endif>{{$tipo_soporte->tipo}}</option>
                        @endforeach
                     </select>
                  </div> 
               </div>
            </div>
         </div> 
      </div>   
      <div class="modal-footer">
      
         <button type="button" class="btn btn-success" onclick="asing_support()" @if($support->support_status_id == 4){{"disabled"}}@endif >Guardar</button>
      
      
      
         <!-- <button type="button" class="btn btn-success" onclick="asing_support()">Guardar</button> -->
      
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
   @endforeach
   </form>   
   </div>
</div>