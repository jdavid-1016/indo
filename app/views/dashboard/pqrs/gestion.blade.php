<div class="page-content">
    <div class="portlet">
        <div class="portlet-title">
            <div class="caption"><i class="icon-reorder"></i>Gestión de PQRS</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"></a>
                <a href="#portlet-config" data-toggle="modal" class="config"></a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="tabbable tabs-left">
                <ul class="nav nav-tabs" style="min-height:500px; min-width:300px"> 
                    <div class="scroller" style="height:500px;" data-always-visible="1" data-rail-visible="0">
                        <table class="table table-hover" >
                        @foreach($requests as $pqr)
                            <li class="">
                                <tr>
                                   <th>
                                      <input type="checkbox" name="chck[]" value="{{$pqr->id}}" onclick="activarcasilla(this)" id="id{{$pqr->id}}">
                                   </th>
                                   <td>
                                      <a href="#pqr_{{$pqr->id}}" tabindex="-1" data-toggle="tab" id="{{$pqr->id}}" onclick="cargar_datos_pqr({{$pqr->id}});return false;">
                                         <strong>{{$pqr->Users->name}} {{$pqr->Users->last_name}} : {{$pqr->created_at}}</strong><br>
                                         {{$pqr->id}}-{{$pqr->title}} 
                                      </a>
                                   </td>
                                </tr>
                            </li>
                        @endforeach
                        </table>
                    </div>
                    <a id="btn_masivo" value="" style="display:none; margin:0 auto; margin:15px;" data-toggle="modal" href="#wide" class="btn btn-info">Respuesta Masiva</a>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_6_1">
                        <div class="row">
                            <div class="col-md-8" id="detalle_pqr">
                                
                                
                            </div>
                            
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div> 
   
</div>
     

<script type="text/javascript">
    function activarcasilla(check){
        var check = $("input[type='checkbox']:checked").length;
        if(check != ""){
            $("#btn_masivo").show('show');
        }else{
            $("#btn_masivo").hide('slow');
        }
    }
    function cargar_datos_pqr(id_pqr){
        var parametros = {
            "id_pqr" : id_pqr
        };
        $.ajax({
            data:  parametros,
            url:   'detallepqr',
            type:  'get',
            beforeSend: function () {
                $("#detalle_pqr").html("Procesando, espere por favor...");
            },
            success:  function (response) {
                $("#detalle_pqr").html(response);
            }
        });
    }
</script>