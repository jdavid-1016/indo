<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.4.min.js"></script>

<div class="page-content">
   <!-- BEGIN PAGE HEADER-->
   <div class="row">
      <div class="col-md-12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->
         <h3 class="page-title">
            Gestión de pqrs <small>gestion de pqrs</small>
         </h3>
         <ul class="page-breadcrumb breadcrumb">
            <li>
               <i class="icon-home"></i>
               <a href="index.html">Home</a> 
               <i class="icon-angle-right"></i>
            </li>
            <li>
               <a href="#">Data Tables</a>
               <i class="icon-angle-right"></i>
            </li>
            <li><a href="#">Basic Tables</a></li>
         </ul>
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   <!-- BEGIN PAGE CONTENT-->
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-bell"></i>Historial de PQRS</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="table-responsive">
               <div class="well">
                  <table style="margin:0 auto;">
                     <thead>
                        <tr>
                           <th class="text-center">
                              <input type="text" class="form-control input-sm" id="valor1" placeholder="N° PQR" onkeyup="realizaProceso($('#valor1').val(), $('#valor2').val(), $('#valor3').val() , $('#valor4').val());return false;">
                           </th>
                           
                           <th class="text-center" style="width:120px"> 
                                 <select class="form-control" id="valor3" onchange="realizaProceso($('#valor1').val(), $('#valor2').val(), $('#valor3').val() , $('#valor4').val());return false;">
                                    <option>Estado</option>
                                    @foreach($state_requests as $state_request)
                                    <option value="{{$state_request->id}}">{{$state_request->name}}</option>
                                    @endforeach
                                 </select>
                           </th>
                           <th class="text-center" style="min-width:140px">
                              <select class="form-control" id="valor2" onchange="realizaProceso($('#valor1').val(), $('#valor2').val(), $('#valor3').val(), $('#valor4').val());return false;">
                                 <option>Proceso</option>
                                 @foreach($processes as $process)
                                    <option value="{{$process->id}}">{{$process->name}}</option>
                                 @endforeach
                              </select>
                           </th>
                           
                           <th class="text-center" style="min-width:115px"> 
                              
                              <select  class="form-control" id="valor4" onchange="realizaProceso($('#valor1').val(), $('#valor2').val(), $('#valor3').val(), $('#valor4').val());return false;">
                                 <option>Tipo PQR</option>
                                 @foreach($TypeRequests as $typeRequest)
                                 <option value='{{$typeRequest->id}}'>{{$typeRequest->name}}</option>

                                 @endforeach
                              </select>
                           </th>
                           
                        </tr>
                     </thead>
                  </table>
                                     
               </div>
               
                  <table class=" table-striped table-bordered table-advance table-hover text-center" id="resultado" onload="">
                     <thead>
                        <tr>
                           <th class="text-center" style="padding:10px">
                              N° PQR
                           </th>
                           <th class="text-center" style="min-width:150px">
                              Motivo
                           </th>
                           <th class="text-center hidden-xs" >
                              Descripción
                           </th>
                           <th class="text-center" style="min-width:140px">
                              Usuaro
                           </th>
                           <th class="text-center" style="width:110px"> 
                              Estado
                           </th>
                           <th class="text-center" style="min-width:140px">
                              Proceso
                           </th>
                           <th class="text-center" style="min-width:140px">
                              Tipo de alumno
                           </th>
                           <th class="text-center" style="min-width:115px"> 
                              Tipo PQR
                           </th>
                           
                        </tr>
                     </thead>
                     <tbody >
                     
                     @if($requests)
                     <!-- $requests es la variable enviada desde el controlador -->
                     @foreach($requests as $request)
                        <tr>
                           <td class="highlight" style="width:120px;">
                           @if($request->stateRequests->name == 'Pendiente')
                              <a class="btn btn-default" data-toggle="modal" href="#wide" onclick="realizaProcesodos({{$request->id}});return false;">
                                 <i class="icon-edit"></i> PQR-00{{$request->id}}
                              </a>
                           @else
                              <a class="btn btn-default" data-toggle="modal" href="#wide" disabled="" onclick="realizaProcesodos({{$request->id}});return false;">
                                 <i class="icon-edit"></i> PQR-00{{$request->id}}
                              </a>
                           @endif
                           </td>
                           <td >{{$request->title}}</td>
                           <td class="hidden-xs" style="padding:10px">{{$request->description}}</td>
                           <td>
                              {{$request->user->name}}
                              {{$request->user->last_name}}
                           </td>
                           <td>
                              @if($request->stateRequests->name == 'Pendiente')
                              <span class="label label-danger">{{$request->StateRequests->name}}</span>
                              @elseif($request->stateRequests->name == 'Finalizado')
                              <span class="label label-success">{{$request->StateRequests->name}}</span>
                              @else
                              <span class="label label-info">{{$request->StateRequests->name}}</span>
                              @endif
                           </td>
                           <td>{{$request->process->name}}</td>
                           <td>{{$request->TypeUsers->name}}</td>
                           <td>{{$request->TypeRequests->name}}</td>
                           
                        </tr>
                     @endforeach
                     @endif
                        <tr>
                        <th colspan="8">
                           <div style="margin:0 auto;">
                           <?php echo $requests->links(); ?>
                           </div>
                        </th>
                           
                        </tr>
                     </tbody>

                  </table>
                  
               </div>
            </div>
         </div>
         
      </div>

      
   </div>
   
   <!-- END PAGE CONTENT-->
</div>

<div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog modal-wide">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Modal Title</h4>
         </div>
         <div class="modal-body" id="datosmodal">
            

           
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-info">Save changes</button>
         </div>
      </div>
      <!-- /.modal-content -->
   </div>
   <!-- /.modal-dialog -->
</div>


<script>
function realizaProceso(valorCaja1, valorCaja2, valorCaja3, valorCaja4){
        var parametros = {
                "valorCaja1" : valorCaja1,
                "valorCaja2" : valorCaja2,
                "valorCaja3" : valorCaja3,
                "valorCaja4" : valorCaja4
        };
        $.ajax({
                data:  parametros,
                url:   'historial',
                type:  'post',
                beforeSend: function () {
                        $("#wide").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
                }
        });
}
</script>
<script>
function realizaProcesodos(valorCaja1){
        var parametros = {
                "valorCaja1" : valorCaja1
               
        };
        $.ajax({
                data:  parametros,
                url:   'gestion',
                type:  'post',
                beforeSend: function () {
                        $("#datosmodal").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#datosmodal").html(response);
                }
        });
}
</script>