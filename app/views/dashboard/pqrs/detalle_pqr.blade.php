@foreach($pqrs as $pqr)
<table class="table table-hover table-striped table-bordered">
    <tr>
        <th>Motivo</th>
        <td colspan="">
            <input class="form-control" value="{{$pqr->title}}" disabled="">
        </td>
        <th>Tipo De PQR:</th>
        <td>
            <select  class="form-control" disabled="" onchange='cargarSelect2(this.value);'>
                <option value=''>Selecciona una opcion</option>
                @foreach($tipo_pqrs as $tipo)
                <option value='{{$tipo->id}}' @if($pqr->pqrs_types_id == $tipo->id) selected="" @endif>{{$tipo->name}}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <th>Fecha de Publicación:</th>
        <td colspan="">
            {{$pqr->created_at}}
        </td>
        <th>Tipo de Usuario</th>
        <td>
            <select  class="form-control" disabled="" onchange='cargarSelect2(this.value);'>
                <option value=''>Selecciona una opcion</option>
                @foreach($tipo_usuarios as $tipo_usuario)
                <option value='{{$tipo_usuario->id}}' @if($pqr->pqrs_type_users_id == $tipo_usuario->id) selected="" @endif>{{$tipo_usuario->name}}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <th>Proceso Involucrado:</th>
        <td colspan="">
            <select  class="form-control" disabled="" onchange='cargarSelect2(this.value);'>
                <option value=''>Selecciona una opcion</option>
                @foreach($procesos as $proceso)
                <option value='{{$proceso->id}}' @if($pqr->processes_id == $proceso->id) selected="" @endif>{{$proceso->name}}</option>
                @endforeach
            </select>
        </td>
     
        <th>Soporte</th>
        <td>
            <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_confidential"><i class="icon-paperclip"></i>Soporte </a>
        </td>
    </tr>
    <tr>
        <th>Descripcion:</th>
            <td colspan="3">
                <textarea  name="coment" disabled="" class="form-control" maxlength="200" rows="3" placeholder="" >{{$pqr->description}}</textarea>
            </td>
        </td>
    </tr>
    <tr>
        <th>Tipo De PQR:</th>
        <td>
            <select  class="form-control" id="tipo_pqr" onchange='cargar_especifique();'>
                <option value=''>Selecciona una opcion</option>
                @foreach($tipo_pqrs as $tipo)
                <option value='{{$tipo->id}}' @if($pqr->pqrs_types_id == $tipo->id) selected="" @endif>{{$tipo->name}}</option>
                @endforeach
            </select>
        </td>
        <th>Especifique:</th>
        <td>
            <select  class="form-control" disabled="" id="especifique" onchange='cargarSelect2(this.value);'>
                <option value=''>Selecciona una opcion</option>
                @foreach($tipo_pqrs as $tipo)
                <option value='{{$tipo->id}}' @if($pqr->pqrs_types_id == $tipo->id) selected="" @endif>{{$tipo->name}}</option>
                @endforeach
            </select>
        </td>
    </tr>
    <tr>
        <th>COMENTARIO:</th>
        <td colspan="3">
            <textarea  name="coment" id="coment" class="form-control" maxlength="200" rows="3" placeholder="Escriba un comentario. EJ: Se asigna solicitud al usuario..." ></textarea>
            <!-- <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_text"><i class="icon-plus"></i> Agregar textos</a> -->
            <div class="btn-group" style="float:right;">
                
                <a type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" id="btn_texto_defecto"><i class="icon-plus"></i> Agregar textos</a>
                <div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu" style="width:400px">
                    <input type="text" class="form-control" value="" id="texto_defecto" placeholder="Texto">
                    <hr>
                    <a class="btn btn-success pull-right" onclick="pqrs_guardar_texto()"><i class="icon-ok"></i></a>
                    <!-- <br>
                    <div id="" class="width:100%">
                        <table id="pqrs_textos" class="table">
                        </table>
                    </div> -->
                </div>
            </div>
            <select name="no_conformidad" id="textos_usuario" onchange="pqr_cargar_texto()" style="width:200px" class="form-control"> 
                <option value="">Selecciona una Opcion</option> 
                @foreach($textos as $texto)
                <option value='{{$texto->text}}'>{{$texto->text}}</option>
                @endforeach
            </select>
             
        </td>
    </tr>
    <tr>
        <th>CONFIDENCIAL:</th>
        <td colspan="3">
            <textarea id="confidential" name="confidential" class="form-control" maxlength="200" rows="2" placeholder="Este campo es confidencial y solo puede ser visto por administrativos...." ></textarea>
            <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_text"><i class="icon-plus"></i> Agregar textos</a>
        
            <select name="no_conformidad" id="no_confidential" onchange="selecOp()" style="width:200px" class="form-control"> 
                <option value="">Selecciona una Opcion</option> 
            </select>
        </td>
    </tr>
    <tr>
        <th>
            <input type="submit" class="btn btn-success" value="Enviar"> 
        </th> 
    </tr>
</table>
@endforeach
<script type="text/javascript">
    function cargar_especifique(){
        var tipo_pqr = $("#cargar_especifique").val();
        
        $("#especifique").load('cargarresponsable?id_proceso=' + process);
        
    }


    function pqrs_guardar_texto(){

        var text = $("#texto_defecto").val();

        $.ajax({
                type: "post",
                url:  "ingresartexto",
                data: { 
                    text:text
                }
        })
        .done(function(data) {
            if (data == 1) {
                $("#textos_usuario").load('cargartextosusuario');
                $("#texto_defecto").val('');
                toastr.success('Se ingreso correctamente el texto por defecto', 'Nuevo texto por defecto');
            }else{
                
                toastr.error('No ha sido posible cargar el usuario', 'Error');

            }
        });

    }
    function pqrs_guardar_confidencial(){

        var text = $("#texto_defecto").val();

        $.ajax({
                type: "post",
                url:  "ingresartextoconfidencial",
                data: { 
                    text:text
                }
        })
        .done(function(data) {
            if (data == 1) {
                $("#textos_usuario").load('cargarresponsable?id_proceso=' + process);
                $("#texto_defecto").val('');
            }else{
                
                toastr.error('No ha sido posible cargar el usuario', 'Error');

            }
        });

    }
    function pqr_cargar_texto(){
        var text = $("#textos_usuario").val();
        $("#coment").val(text);

    }


</script>