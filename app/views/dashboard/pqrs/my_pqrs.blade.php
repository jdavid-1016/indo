<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Mis PQRS</div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class="active"><a href="#tab-agregar_pqr" data-toggle="tab">Mis PQRS</a></li>
                        <li class=""><a href="#tab-mis_pqrs" data-toggle="tab">Agregar PQR</a></li>
                    </ul>
                    <div  class="tab-content">
                        <div class="tab-pane fade in" id="tab-mis_pqrs">
                            <div class="row">
                                <form class="form-horizontal col-md-9" role="form" action="new" method="post" enctype="multipart/form-data">
                                    <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                                    <div class="form-body">
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Fecha y Hora:</label>
                                            <div class="col-md-9">
                                                <?php date_default_timezone_set('America/Bogota') ?>
                                                <input type="text" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}" id="pqr_date">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Tipo de Solicitud:</label>
                                            <div class="col-md-9">
                                                <select  class="form-control" required="" name="type_requests_id" id="pqr_type_pqr">
                                                <option></option>
                                                @if($TypeRequests)
                                                @foreach($TypeRequests as $TypeRequests)
                                                    <option value="{{$TypeRequests->id}}">{{$TypeRequests->name}}</option>
                                                @endforeach
                                                @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Proceso Involucrado:</label>
                                            <div class="col-md-9">
                                                <select  class="form-control" name="process_id" required="" id="select1" onchange="cargarSelect2(this.value);" id="pqr_proccess_id">
                                                    <option value=""></option>
                                                    @if($process)
                                                    @foreach($process as $process)
                                                        <option value="{{$process->id}}">{{$process->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Detalle:</label>
                                            <div class="col-md-9">
                                                <select  class="form-control" required="" id="select2" disabled="" name="detalle_id">
                                                    <option value="">Selecciona una opción</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Tipo de Usuario:</label>
                                            <div class="col-md-9">
                                                <select  class="form-control" required="" name="type_users_id" id="pqr_type_user_id">
                                                    <option></option>
                                                    @if($TypeUsers)
                                                    @foreach($TypeUsers as $typeUsers)
                                                        <option value="{{$typeUsers->id}}">{{$typeUsers->name}}</option>
                                                    @endforeach
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Motivo: </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control"  placeholder="" required="" name="title" id="pqr_title">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label  class="col-md-3 control-label">Descripción</label>
                                            <div class="col-md-9">
                                                <textarea class="form-control" rows="3" required="" name="description" id="pqr_description"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputFile" class="col-md-3 control-label">Soporte de PQR</label>
                                            <div class="col-md-9">
                                                <input type="file" id="exampleInputFile" name="support">
                                                <p class="help-block">Solo se aceptan imágenes JPG o GIF</p>
                                            </div>
                                        </div>
                                 
                                    </div>
                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn btn-success" onclick="enviar_pqr()">Enviar PQR</button>
                                            <button type="button" class="btn btn-default">Cancel</button>                              
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="tab-pane fade active in" id="tab-agregar_pqr">
                            <div class="col-md-12">
                                <div class="portlet">
                                    <div class="portlet-title">
                                        <div class="caption"><i class="icon-bell"></i>Mis PQRS</div>
                                        <div class="tools">
                                            <a href="javascript:;" class="collapse"></a>
                                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                            <a href="javascript:;" class="reload"></a>
                                            <a href="javascript:;" class="remove"></a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="table-responsive">
                                            <table class="table top-blue">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center">N° PQR</th>
                                                        <th class="text-center" style="min-width:150px"> Motivo</th>
                                                        <th class="text-center hidden-xs" > Descripción</th>
                                                        <th class="text-center" style="min-width:140px"> Usuario</th>
                                                        <th class="text-center"> Estado</th>
                                                        <th class="text-center" style="min-width:140px"> Proceso</th>
                                                        <th class="text-center" style="min-width:140px"> Tipo de alumno</th>
                                                        <th class="text-center"> Tipo de pqr</th>
                                                        <th class="text-center"> Acciones</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(isset($error))
                                                        <div class="alert alert-info alert-dismissable">
                                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                            <strong>Error!</strong> {{$error}}.
                                                        </div>
                                                    @else
                                                        @if($pqrs)
                                                            @foreach($pqrs as $pqr)
                                                                <tr>
                                                                    <td>{{$pqr['id_pqr']}}</td>
                                                                    <td>{{$pqr['title']}}</td>
                                                                    <td>{{$pqr['description']}}</td>
                                                                    <td>{{$pqr['usuario']}}</td>
                                                                    <td>{{$pqr['estado']}}</td>
                                                                    <td>{{$pqr['proceso']}}</td>
                                                                    <td>{{$pqr['tipo_usuario']}}</td>
                                                                    <td>{{$pqr['tipo_pqr']}}</td>
                                                                </tr>
                                                            @endforeach
                                                            
                                                        @endif
                                                    @endif
                                           
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                         
                        </div>
                      
                    </div>
                </div>
            </div>
         
        </div>
    </div>
</div>
<script type="text/javascript">
   
    function cargarSelect2(valor){
        var arrayValores=new Array(
            @if($detalle)
                @foreach($detalle as $detalle)
                    new Array({{$detalle->process_id}},{{$detalle->id}},'{{$detalle->name}}'),
                @endforeach
            @endif
            new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
        );
        if(valor==0){
            document.getElementById("select2").disabled=true;
        }else{
            document.getElementById("select2").options.length=0;
            document.getElementById("select2").options[0]=new Option("Selecciona una opcion", "0");
            for(i=0;i<arrayValores.length;i++){
                if(arrayValores[i][0]==valor){
                    document.getElementById("select2").options[document.getElementById("select2").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                }
            }
            document.getElementById("select2").disabled=false;
        }
    }
    function enviar_pqr(){

        var pqr_date = $("#pqr_date").val();
        var pqr_type_pqr = $("#pqr_type_pqr").val();
        var pqr_proccess_id = $("#pqr_proccess_id").val();
        var select2 = $("#select2").val();
        var pqr_type_user_id = $("#pqr_type_user_id").val();
        var pqr_title = $("#pqr_title").val();
        var pqr_description = $("#pqr_description").val();
        var exampleInputFile = $("#exampleInputFile").val();

        if(pqr_date == ""){
            toastr.error('El campo pqr_date esta vacio.', 'error');
            return;
        }
        if(pqr_type_pqr == ""){
            toastr.error('El campo pqr_type_pqr esta vacio.', 'error');
            return;
        }
        if(pqr_proccess_id == ""){
            toastr.error('El campo pqr_proccess_id esta vacio.', 'error');
            return;
        }
        if(select2 == ""){
            toastr.error('El campo select2 esta vacio.', 'error');
            return;
        }
        if(pqr_type_user_id == ""){
            toastr.error('El campo pqr_type_user_id esta vacio.', 'error');
            return;
        }
        if(pqr_title == ""){
            toastr.error('El campo pqr_title esta vacio.', 'error');
            return;
        }
        if(pqr_description == ""){
            toastr.error('El campo pqr_description esta vacio.', 'error');
            return;
        }
        if(exampleInputFile == ""){
            toastr.error('El campo exampleInputFile esta vacio.', 'error');
            return;
        }

    }
</script>
