<script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.4.4.min.js"></script> 
<script type="text/javascript">
   function activarcasilla(check){
      var check = $("input[type='checkbox']:checked").length;
      if(check != ""){
         document.x.b1.style.visibility="visible";
      }else{
         document.x.b1.style.visibility="hidden";
      }






      
        
   }
</script> 
<script>
function realizaProceso(valorCaja1){
        var parametros = {
                "valorCaja1" : valorCaja1
               
        };
        $.ajax({
                data:  parametros,
                url:   'responder',
                type:  'post',
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                        $("#resultado").html(response);
                }
        });
}
</script>
<?php 
    $host= $_SERVER["HTTP_HOST"];
    $url= $_SERVER["REQUEST_URI"];
    $direccion = "http://" . $host . $url;
    
?>

<div class="page-content">
   <!-- BEGIN PAGE CONTENT-->
   <div class="row">
      <div class="col-md-12">
         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i>Gestión de PQRS</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="tabbable tabs-left">
              
                  <ul class="nav nav-tabs tab-pane">
                  @if(isset($error))
                     <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                        <strong>Error!</strong> {{$error}}.
                     </div>
                  @else
                  @if(isset($requests))
                  <form method="post" action="pqrs/massive" accept-charset="utf-8" enctype="multipart/form-data" name="x">
                  <input type="hidden" class="form-control" name="url_completa" value="<?php echo $direccion; ?>">
                    <div class="scroller" style="height:500px;" data-always-visible="1" data-rail-visible="0">

                       <table class="table table-hover table- table-">
                       
                       @foreach($requests as $request)
                          <li>
                             <tr>
                                <th>
                                   <input type="checkbox" name="chck[]" value="{{$request->id}}" onclick="activarcasilla(this)" id="id{{$request->id}}">
                                </th>
                                <td>
                                   <a href="#pqr_{{$request->id}}" tabindex="-1" data-toggle="tab" id="{{$request->id}}" onclick="realizaProceso({{$request->id}});return false;">
                                      <strong>{{$request->user->name}} {{$request->user->last_name}} : {{$request->date}}</strong><br>
                                      {{$request->id}}-{{$request->title}} 
                                   </a>
                                </td>
                             </tr>
                          </li>
                       @endforeach
                       
                       </table>
                    </div>
                    <input name="b1" type="submit" value="Respuesta Masiva" style="visibility:hidden; width:150px; border-radius:5px; margin:0 auto; margin:15px;" data-toggle="modal" href="#wide" class="btn btn-info">
                  
                    <div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
                       
                       <div class="modal-dialog modal-wide">
                          <div class="modal-content">
                             <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h2 class="modal-title text-center">Respuesta Masiva</h2>

                                
                                


                                <table class="table table-hover table-striped table-bordered">
                                  
                                  <?php date_default_timezone_set("America/Bogota"); ?>
                                  <input type="hidden" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}">
                                  

                                     <script type="text/javascript">
                                        /**
                                         * Funcion que se ejecuta al seleccionar una opcion del primer select
                                         */
                                        function cargarSelect2(valor)
                                        {
                                            /**
                                             * Este array contiene los valores sel segundo select
                                             * Los valores del mismo son:
                                             *  - hace referencia al value del primer select. Es para saber que valores
                                             *  mostrar una vez se haya seleccionado una opcion del primer select
                                             *  - value que se asignara
                                             *  - testo que se asignara
                                             */
                                              

                                            var arrayValores=new Array(
                                        <?php foreach ($especifique as $key_especifique): ?>
                                              <?php echo 'new Array('.$key_especifique->type_request_id.",".$key_especifique->id.',"'.$key_especifique->detalle.'"),'?>
                                        <?php endforeach ?>
                                                
                                                new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
                                            );
                                            if(valor==0)
                                            {
                                                // desactivamos el segundo select
                                                document.getElementById("select2").options.length=0;
                                                document.getElementById("select2").options[0]=new Option("Selecciona una opcion", "");
                                                document.getElementById("select2").disabled=true;

                                                document.getElementById("select3").options.length=0;
                                                document.getElementById("select3").options[0]=new Option("Selecciona una opcion", "");
                                                document.getElementById("select3").disabled=true;

                                            }else{
                                                // eliminamos todos los posibles valores que contenga el select2
                                                document.getElementById("select2").options.length=0;
                                                
                                                // añadimos los nuevos valores al select2
                                                document.getElementById("select2").options[0]=new Option("Selecciona una opcion", "");
                                                for(i=0;i<arrayValores.length;i++)
                                                {
                                                    // unicamente añadimos las opciones que pertenecen al id seleccionado
                                                    // del primer select
                                                    if(arrayValores[i][0]==valor)
                                                    {
                                                        document.getElementById("select2").options[document.getElementById("select2").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                                                    }
                                                }
                                                
                                                // habilitamos el segundo select
                                                document.getElementById("select2").disabled=false;
                                            }
                                        }

                                     
                                        
                                     </script>

                                  <tr>
                                     <th>TIPO DE PQR:</th>
                                     <td colspan="">
                                        <select  class="form-control" name="id_request_type" required="" id='select1' onchange='cargarSelect2(this.value);'>
                                           <option value=''>Selecciona una opcion</option>
                                           @foreach($TypeRequests as $typeRequest)
                                           <option value='{{$typeRequest->id}}'>{{$typeRequest->name}}</option>

                                           @endforeach
                                        </select>
                                     </td>
                                     
                                     <th>ESPECIFIQUE</th>
                                     <td>
                                        <select disabled="" class="form-control" name="specify" id='select2'required="" onchange='cargarSelect3(this.value);'>
                                            <option value=''>Selecciona una opcion</option>
                                        </select>
                                     </td>
                                  </tr>

                                     <script type="text/javascript">
                                        /**
                                         * Funcion que se ejecuta al seleccionar una opcion del primer select
                                         */
                                        function cargarSelect3(valor)
                                        {
                                            /**
                                             * Este array contiene los valores sel segundo select
                                             * Los valores del mismo son:
                                             *  - hace referencia al value del primer select. Es para saber que valores
                                             *  mostrar una vez se haya seleccionado una opcion del primer select
                                             *  - value que se asignara
                                             *  - testo que se asignara
                                             */
                                              

                                            var arrayValores=new Array(
                                        <?php foreach ($description as $descripcion): ?>
                                              <?php echo 'new Array('.$descripcion->especifique_id .",".$descripcion->id.',"'.$descripcion->descripcion.'"),'?>
                                        <?php endforeach ?>
                                                
                                                new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
                                            );
                                            if(valor==0)
                                            {
                                                // desactivamos el segundo select
                                                document.getElementById("select3").options.length=0;
                                                document.getElementById("select3").options[0]=new Option("Selecciona una opcion", "");
                                                document.getElementById("select3").disabled=true;
                                            }else{
                                                // eliminamos todos los posibles valores que contenga el select2
                                                document.getElementById("select3").options.length=0;
                                                
                                                // añadimos los nuevos valores al select2
                                                document.getElementById("select3").options[0]=new Option("Selecciona una opcion", "");
                                                for(i=0;i<arrayValores.length;i++)
                                                {
                                                    // unicamente añadimos las opciones que pertenecen al id seleccionado
                                                    // del primer select
                                                    if(arrayValores[i][0]==valor)
                                                    {
                                                        document.getElementById("select3").options[document.getElementById("select3").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                                                    }
                                                }
                                                
                                                // habilitamos el segundo select
                                                document.getElementById("select3").disabled=false;
                                            }
                                        }

                                     
                                        
                                     </script>

                                  <tr>
                                     <th>DESCRIPCION:</th>
                                     <td colspan="">
                                        <select  class="form-control" disabled="" name="specify_description" required="" id="select3">
                                           <option value=''>Selecciona una opcion</option>
                                           @foreach($description as $descripcion)
                                           <option value='{{$descripcion->id}}'>{{$descripcion->descripcion}}</option>

                                           @endforeach
                                        </select>
                                     </td>
                                     
                                     <th>ESTADO DE LA PQR</th>
                                     <td>
                                        <select  class="form-control" name="state_request" id="state_request" required="" onchange="selecOp()">
                                           <option value=''></option>
                                           @foreach($state_requests as $state_request)
                                           <option value="{{$state_request->id}}">{{$state_request->name}}</option>
                                           @endforeach
                                        </select>
                                     </td>
                                  </tr>
                                  <tr>
                                     <th>ASIGNAR PQR:</th>
                                     <td colspan="2">
                                        <select  class="form-control" name="user_asign" required="">
                                           <option value=''></option>
                                           @foreach($processes as $process)
                                           <option value='{{$process->id}}'>{{$process->processes->name}}-{{$process->name}}</option>
                                           @endforeach
                                        </select>
                                     </td>
                                     
                                     
                                  </tr>
                                  <tr>
                                     <th>COMENTARIO:</th>
                                     <td colspan="3">
                                     <script type="text/javascript"> 
                                           function selecOp2(){ 
                                              var op=document.getElementById("no_conformidad"); 
                                              
                                              var tt2=document.getElementById("coment"); 
                                              
                                              tt2.value=tt2.value=op.options[op.options.selectedIndex].value;; 

                                           } 
                                        </script> 
                                        <textarea  name="coment" id="coment" class="form-control" maxlength="200" rows="3" placeholder="Escriba un comentario. EJ: Se asigna solicitud al usuario..." required=""></textarea>
                                        
                                        <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_text">Agregar textos <span class=""><i class="icon-plus"></i></span></a>
                                        
                                        <select name="no_conformidad" id="no_conformidad" onchange="selecOp2()" style="width:200px" class="form-control"> 
                                           <option value="">Selecciona una Opcion</option> 
                                           @foreach($default_text as $default)
                                           <option value='{{$default->text}}'>{{$default->text}}</option>

                                           @endforeach
                                        </select>
                                         
                                     </td>
                                  </tr>
                                  <tr>
                                     <th>CONFIDENCIAL:</th>
                                     <td colspan="3">
                                        <script type="text/javascript"> 
                                           function selecOp(){ 
                                              var op=document.getElementById("no_confidential"); 
                                              
                                              var tt2=document.getElementById("confidential"); 
                                              
                                              tt2.value=tt2.value=op.options[op.options.selectedIndex].value;; 

                                           } 
                                        </script> 
                                        <textarea id="confidential" name="confidential" class="form-control" maxlength="200" rows="2" placeholder="Este campo es confidencial y solo puede ser visto por administrativos...." ></textarea>
                                        <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_confidential">Agregar textos <span class=""><i class="icon-plus"></i></span></a>
                                        
                                        <select name="no_conformidad" id="no_confidential" onchange="selecOp()" style="width:200px" class="form-control"> 
                                           <option value="">Selecciona una Opcion</option> 
                                           @foreach($default_confidential as $default_conf)
                                           <option value='{{$default_conf->text}}'>{{$default_conf->text}}</option>

                                           @endforeach
                                        </select>
                                     </td>
                                  </tr>
                                  <tr>
                                     <th>
                                        <input type="submit" class="btn btn-success" value="Enviar"> 
                                     </th> 
                                  </tr>
                                </table>
                               
                             </div>
                             <div class="modal-body">
                                
                                   
                                 
                             </div>
                             <div class="modal-footer">
                                <button type="button" data-dismiss="modal" class="btn btn-default">Close</button>
                                
                             </div>
                          </div>
                       </div>
                       
                    </div>

                  </form>
                  @else
                  @endif
                  @endif
                  </ul>
                
                  
                  
                     

                        <div class="col-md-8">
                           <div class="portlet">
                              
                              <div class="portlet-body" id="resultado">
                              <?php $status=Session::get('status'); ?>
                              @if($status == 'ok_create')
                              <div class="alert alert-info alert-dismissable">
                                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                 <strong>Exito!</strong> Se Evió la gestión de la pqr correcamente.
                              </div>
                              @endif
                             
                                          
                                             
                                          
                              </div>
                           </div>
                        </div>
                     
                  
                  
               </div>
            </div>
         </div>
         
      </div>

      
   </div>
   
   <!-- END PAGE CONTENT-->
</div>
     
