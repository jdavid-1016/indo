<head>
   <meta charset="utf-8" />
   <title>Indoamericana</title>
   <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta content="initial-scale=1.0" name="viewport" />
   <meta content="" name="description" />
   <meta content="" name="author" />
   <meta name="MobileOptimized" content="320">
   <link href="{{URL::to("logo.ico")}}" rel="shortcut icon" >
   <!-- BEGIN GLOBAL MANDATORY STYLES -->
   <link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
   <!-- END GLOBAL MANDATORY STYLES -->
   <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
   <link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
   <link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
   <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
   <!-- END PAGE LEVEL PLUGIN STYLES -->
   <!-- BEGIN THEME STYLES --> 
   <link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
   <link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
   <link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
   <link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
   <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/select2/select2_conquer.css")}}" />
   <link rel="stylesheet" href="{{  URL::to("assets/plugins/data-tables/DT_bootstrap.css")}}" />

   <link rel="stylesheet" href="../js/tragator/fm.tagator.jquery.css"/>
   <link rel="stylesheet" href="{{ URL::to("assets/scripts/tags/jquery.tag-editor.css")}}">

   <!-- tipped -->
   <link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />
   <!-- select2 -->
   <link rel="stylesheet" type="text/css" href="{{ URL::to("assets/plugins/select2/select2_conquer.css")}}" />
  <!-- calendar -->
   <link rel="stylesheet" type="text/css" href="{{ URL::to("bower_components/bootstrap-calendar/css/calendar.css")}}" />
   <!-- END THEME STYLES -->   
</head>
<link href="assets/css/pages/blog.css" rel="stylesheet" type="text/css"/>
<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Blog
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="informacion">Página Principal</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="#">Blog</a>
                    <i class="icon-angle-right"></i>
                </li>
            </ul>
        </div>
    </div>
    
     <div class="row">
            <div class="col-md-12 blog-page">
               <div class="row">
                  <div class="col-md-9 col-sm-8 article-block">
                     <h1>Latest Blog</h1>
                     <?php foreach ($blogs as $blog) { 
                         $tags = BlogTags::where('blogs_id', $blog->id)->get();
                         $comentarios = BlogComments::where('blogs_id', $blog->id)->get();
                         ?>
                     <div class="row">
                        <div class="col-md-4 blog-img blog-tag-data">
                           <img src="<?php echo $blog->image_1; ?>" alt="" width="390px" height="292px" class="img-responsive">
                           <ul class="list-inline">
                              <li><i class="icon-calendar"></i> <a href="#"><?php echo date("\n F jS, Y", strtotime($blog->created_at)); ?></a></li>
                              <li><i class="icon-comments"></i> <a href="#">{{ count($comentarios) }} Comentarios</a></li>
                           </ul>
                           <ul class="list-inline blog-tags">
                              <li>
                                 <i class="icon-tags"></i>
                                 @foreach($tags as $tag)
                                 <a href="tag?tag={{ $tag->tag }}">{{ $tag->tag }}</a>
                                 @endforeach
                              </li>
                           </ul>
                        </div>
                        <div class="col-md-8 blog-article">
                           <h3><a href="detallepost?id={{ $blog->id }}">{{ $blog->title }}</a></h3>
                           <p>{{ $blog->description }}</p>
                           <a class="btn btn-info" href="detallepost?id={{ $blog->id }}">
                           Leer mas 
                           <i class="m-icon-swapright m-icon-white"></i>
                           </a>
                           
                        </div>
                     </div>
                     <?php } ?>
                  </div>
                  <!--end col-md-9-->
                  <div class="col-md-3 col-sm-4 blog-sidebar">
                     <h3>Top Entradas</h3>
                     <div class="top-news">
                         <?php foreach ($tops as $top) { 
                         $tags = BlogTags::where('blogs_id', $top->id)->get();
                         ?>
                        <a href="detallepost?id={{ $top->id }}" class="btn btn-success">
                        <span>{{ $top->title }}</span>
                        <em>Posteado en: <?php echo date("\n F jS, Y", strtotime($top->created_at)); ?></em>
                        <em>
                        <i class="icon-tags"></i>
                        @foreach($tags as $tag)
                         {{ $tag->tag }},
                        @endforeach
                        </em>
                        <i class="icon-briefcase top-news-icon"></i>
                        </a>
                         <?php } ?>
                     </div>
                     <div class="space20"></div>                     
                     <h3>Blog Tags</h3>
                     <ul class="list-inline sidebar-tags">
                         @foreach($tags_general as $tag)
                         <li><a href="tag?tag={{ $tag->tag }}"><i class="icon-tags"></i>{{ $tag->tag }}</a></li>
                        @endforeach
                     </ul>
                     <div class="space20"></div>
                     
                  </div>
                  <!--end col-md-3-->
               </div>
               <div class="pagination">
                {{$pag->links()}}
              </div>
            </div>
         </div>        

</div>

<script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>   
   <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
   <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   
   <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
   <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
   // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
   <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
   <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->   
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
   <!-- END PAGE LEVEL SCRIPTS -->     
   <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
   <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/tags/jquery.caret.min.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/tags/jquery.tag-editor.js")}}"></script>
   <script src="{{ URL::to("js/app.js")}}"></script>
   <script src="{{ URL::to("js/upload.js")}}"></script>
   <script src="{{ URL::to("js/validar.js")}}"></script>
   <script src="{{ URL::to("js/compras.js")}}"></script>
   <script src="{{ URL::to("js/continuada.js")}}"></script>
   <script src="{{ URL::to("js/equipos.js")}}"></script>
   <script src="{{ URL::to("js/blog.js")}}"></script>
   <script src="{{ URL::to("js/aspirantes.js")}}"></script>
   <script src="{{ URL::to("js/angular.min.js")}}"></script>
   <script src="{{ URL::to("js/documentacion.js")}}"></script>
   <script src="{{ URL::to("js/tareas.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->
  <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
  <script src="{{ URL::to("js/jQuery_Mask/src/jquery.mask.js")}}"></script>
  <script src="{{ URL::to("assets/scripts/see-more.js")}}"></script>
   <!-- END PAGE LEVEL SCRIPTS -->   
   <!-- tipped -->
   <script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
   <!-- <script src="{{ URL::to("assets/dompdf.js")}}"></script> -->
   
   <!-- graficos jquery -->

   <script src="{{ URL::to("Highcharts-4.1.4/js/highcharts.js")}}"></script>
   <script src="{{ URL::to("Highcharts-4.1.4/js/modules/exporting.js")}}"></script>

    <script src="{{ URL::to("bower_components/underscore/underscore-min.js")}}"></script>
    <script src="{{ URL::to("bower_components/bootstrap-calendar/js/calendar.js")}}"></script>
    <script src="{{ URL::to("bower_components/bootstrap-calendar/js/language/es-ES.js")}}"></script>

    <div class="modal fade" id="modal_agenda2" tabindex="-1" role="basic" aria-hidden="true">
        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
    </div>


 <script type="text/javascript">
  $(document).ready(function () {
      setInterval(function () {
          var iScroll = $('#scroller').scrollTop();
          iScroll = iScroll + 100;
          $('#scroller').animate({
              scrollTop: iScroll
          }, 1000);
      }, 2000);
  });
  
 </script>

   
   <script>
      jQuery(document).ready(function() {
         App.init(); // initlayout and core plugins
         Portfolio.init();
         TableEditable.init();
         Index.init();
         Index.initJQVMAP(); // init index page's custom scripts
         Index.initCalendar(); // init index page's custom scripts
         Index.initCharts(); // init index page's custom scripts
         Index.initChat();
         Index.initMiniCharts();
         Index.initPeityElements();
         Index.initKnowElements();
         Index.initDashboardDaterange();         
         Tasks.initDashboardWidget();
         UITree.init();
         UIToastr.init();
         FormComponents.init();
      });
      Tipped.create('.informacion', { 
        size: 'medium',
        skin: 'light',
        maxWidth: 300
        
      });
      
      Tipped.create('.nameimg', { 
        size: 'medium',
        skin: 'light',
        maxWidth: 300
        
      });
   </script>