<!DOCTYPE html>
<!-- 
Template Name: Conquer - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.0
Version: 1.5
Author: KeenThemes
Website: http://www.keenthemes.com/
Purchase: http://themeforest.net/item/conquer-responsive-admin-dashboard-template/3716838?ref=keenthemes
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="ISO-8859-1" />
        <title>Indoamericana</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <meta name="MobileOptimized" content="320">
        <link href="{{URL::to("logo.ico")}}" rel="shortcut icon" >
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
        <link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
        <link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
        <!-- END PAGE LEVEL PLUGIN STYLES -->
        <!-- BEGIN THEME STYLES --> 
        <link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
        <link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
        <link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
        <link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/select2/select2_conquer.css")}}" />
        <link rel="stylesheet" href="{{  URL::to("assets/plugins/data-tables/DT_bootstrap.css")}}" />

        <link rel="stylesheet" href="js/tragator/fm.tagator.jquery.css"/>

<link href="assets/css/pages/blog.css" rel="stylesheet" type="text/css"/>
        <!-- END THEME STYLES -->   
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed">
        <!-- BEGIN HEADER -->   
        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!-- BEGIN TOP NAVIGATION BAR -->
            <div class="header-inner">
                <!-- BEGIN LOGO -->  
                <a class="navbar-brand" href="blog">
                    <img src="{{URL::to("assets/img/logo.png")}}" alt="logo" style="margin-top: -15px"class="img-responsive" />
                </a>
                <!--<form class="search-form search-form-header" role="form" action="index.html" >
                   <div class="input-icon right">
                      <i class="icon-search"></i>
                      <input type="text" class="form-control input-medium input-sm" name="query" placeholder="Search...">
                   </div>
                </form>-->
                <h2 style="color: #ffffff">Blog Indoamericana</h2>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER --> 
                <a href="javascript:;" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <img src="{{URL::to("assets/img/menu-toggler.png")}}" alt="" />
                </a> 
                <!-- END RESPONSIVE MENU TOGGLER -->

                <!-- BEGIN TOP NAVIGATION MENU -->        

                <!-- END TOP NAVIGATION MENU -->
            </div>
            <!-- END TOP NAVIGATION BAR -->
        </div>

        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <!----------------------------------------------------------------------------------HEAD---------------------------------------------------------------------------------------->
        <!------------------------------------------------------------------------------------------------------------------------------------------------------------------------------>
        <hr><hr>
        <?php
header('Content-Type: text/html; charset=UTF-8'); 
?>
        <div class="" style="background: white">
               <div class="row">
            <div class="col-md-8 blog-page" style="margin-left: 15%">
               <div class="row">
                  <div class="col-md-8 article-block">
                     <h3>{{ $blog[0]->title }}</h3>
                     <div class="blog-tag-data">
                         <img src="{{ $blog[0]->image_2 }}" class="img-responsive" alt="">
                        <div class="row">
                           <div class="col-md-6">
                              <ul class="list-inline blog-tags">
                                 <li>
                                    <i class="icon-tags"></i>
                                    @foreach($tags as $tag)
                                    <a href="tag?tag={{ $tag->tag }}">{{$tag->tag }}</a>
                                    @endforeach
                                 </li>
                              </ul>
                           </div>
                           <div class="col-md-6 blog-tag-data-inner">
                              <ul class="list-inline">
                                 <li><i class="icon-calendar"></i> <a href="#"><?php echo date("\n F jS, Y", strtotime($blog[0]->created_at)); ?></a></li>
                                 <li><i class="icon-comments"></i> <a href="#">{{ count($comentarios) }} Comentarios</a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <!--end news-tag-data-->
                     <div>
                        <p>{{ $blog[0]->article }}</p>
                     </div>
                     <hr>
                     <div class="row">
                        <div class="col-md-3">
                            <img alt="" src="assets/img/button-face.png" class="media-object">
                        </div>
                        <div class="col-md-2">
                            <a href="https://twitter.com/share" class="twitter-share-button" data-text="Visita el Post en el blog Indoamericana" data-size="large">Twittear</a>
                            <script>!function(d, s, id){var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location)?'http':'https'; if (!d.getElementById(id)){js = d.createElement(s); js.id = id; js.src = p + '://platform.twitter.com/widgets.js'; fjs.parentNode.insertBefore(js, fjs); }}(document, 'script', 'twitter-wjs');</script>
                        </div>
                        <div class="col-md-2">
                            <!-- Inserta esta etiqueta en la sección "head" o justo antes de la etiqueta "body" de cierre. -->
                            <script src="https://apis.google.com/js/platform.js" async defer>
                                {lang: 'es'}
                            </script>

                            <!-- Inserta esta etiqueta donde quieras que aparezca Botón Compartir. -->
                            <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                        </div>
                    </div>
                     <hr>
                     <!--end media-->
                     <div id="comentarios_post">
                     @foreach($comentarios as $comentario)
                     <div class="media">
                        <a href="#" class="pull-left">
                        <img alt="" src="{{ $comentario->Users->img_min }}" class="media-object">
                        </a>
                        <div class="media-body">
                           <h4 class="media-heading">{{ $comentario->Users->name }} {{ $comentario->Users->last_name }}<span><?php echo date("\n F jS, Y", strtotime($comentario->created_at)); ?></span></h4>
                           <p>{{ $comentario->comment }}</p>
                        </div>
                     </div>
                     @endforeach
                     </div>
                     <!--end media-->
                    
<!--                     <div class="post-comment">
                        <h3>Deja un Comentario</h3>                        
                           <div class="form-group">
                              <label class="control-label">Mensaje<span class="required">*</span></label>
                              <textarea class="col-md-10 form-control" rows="8" id="comentario_user" name="comentario_user"></textarea>
                           </div>
                        <button class="margin-top-20 btn btn-info" type="button" onclick="enviarComentarioPost({{ $blog[0]->id }});">Enviar Comentario</button>
                     </div>-->
                  </div>
                  <!--end col-md-9-->
                  <div class="col-md-4 blog-sidebar">                    
                     <h3>Top Entradas</h3>
                     <div class="top-news">
                         <?php foreach ($tops as $top) { 
                         $tags = BlogTags::where('blogs_id', $top->id)->get();
                         ?>
                        <a href="detallepost?id={{ $top->id }}" class="btn btn-success">
                            <span>{{ mb_strcut($top->title, 0, 41, "UTF-8") }}...</span>
                        <em>Posteado en: <?php echo date("\n F jS, Y", strtotime($top->created_at)); ?></em>
                        <em>
                        <i class="icon-tags"></i>
                        @foreach($tags as $tag)
                         {{ $tag->tag }},
                        @endforeach
                        </em>
                        <i class="icon-briefcase top-news-icon"></i>
                        </a>
                         <?php } ?>
                     </div>
                     <div class="space20"></div>
                     <h3>Blog Tags</h3>
                     <ul class="list-inline sidebar-tags">
                        @foreach($tags_general as $tag)
                         <li><a href="tag?tag={{ $tag->tag }}"><i class="icon-tags"></i>{{ $tag->tag }}</a></li>
                        @endforeach
                     </ul>
                     <div class="space20"></div>                    
                  </div>
                  <!--end col-md-3-->
               </div>
            </div>
         </div>  
        </div>







        <!-- BEGIN FOOTER -->

        <div class="footer">
            <div class="footer-inner">
                2015 &copy; SGI Indoamericana.
            </div>
            <div class="footer-tools">
                <span class="go-top">
                    <i class="icon-angle-up"></i>
                </span>
            </div>
        </div>
        <div id="sound">
        </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE PLUGINS -->   
        <!--[if lt IE 9]>
        <script src="assets/plugins/respond.min.js"></script>
        <script src="assets/plugins/excanvas.min.js"></script>
        <![endif]-->

        <script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>

        <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
        <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
        <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
        <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->

        <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
        <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
        // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
        <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
        <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
        <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
        <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->   
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
        <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
        <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
        <!-- END PAGE LEVEL SCRIPTS -->     
        <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
        <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
        <script src="{{ URL::to("js/app.js")}}"></script>
        <script src="{{ URL::to("js/upload.js")}}"></script>
        <script src="{{ URL::to("js/validar.js")}}"></script>
        <script src="{{ URL::to("js/compras.js")}}"></script>
        <script src="{{ URL::to("js/continuada.js")}}"></script>
        <script src="{{ URL::to("js/aspirantes.js")}}"></script>
        <script src="{{ URL::to("js/angular.min.js")}}"></script>
        <script src="{{ URL::to("js/documentacion.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
        <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
        <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
        <script src="{{ URL::to("js/jQuery_Mask/src/jquery.mask.js")}}"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

        <script src="js/tragator/fm.tagator.jquery.js"></script>

        <script>
                                                            jQuery(document).ready(function () {
                                                                App.init(); // initlayout and core plugins
                                                                Portfolio.init();
                                                                TableEditable.init();
                                                                Index.init();
                                                                Index.initJQVMAP(); // init index page's custom scripts
                                                                Index.initCalendar(); // init index page's custom scripts
                                                                Index.initCharts(); // init index page's custom scripts
                                                                Index.initChat();
                                                                Index.initMiniCharts();
                                                                Index.initPeityElements();
                                                                Index.initKnowElements();
                                                                Index.initDashboardDaterange();
                                                                Tasks.initDashboardWidget();
                                                                UITree.init();
                                                                UIToastr.init();
                                                                FormComponents.init();
                                                            });
        </script>   