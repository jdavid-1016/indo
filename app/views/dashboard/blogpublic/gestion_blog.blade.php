<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Nuevo Post
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Nuevo Post</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <!-- <li>
                    <a href="javascript:;">Aspirantes</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Gestion Emails</a> 
                </li> -->
            </ul>
        </div>
    </div>

    <div class="row">
        {{Form::open( array('id'=>'form_blog', 'url'=>'blog/guardarpost', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}

        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-3 control-label">Titulo:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="titulo" name="titulo" required/>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Descripcion Corta:</label>
                    <div class="col-md-6">
                        <textarea class="ckeditor form-control" id="desc" name="desc" rows="6"></textarea>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Articulo:</label>
                    <div class="col-md-6">
                        <textarea class="ckeditor form-control" id="articulo" name="articulo" rows="6"></textarea>
                    </div>
                </div>
            </div>
        </div><br>
        <!--<div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Icono:</label>
                    <div class="col-md-6">
                        <i class="icon-money"></i><input type="checkbox" name="iconopost[]" id="icono_posts" value="icon-money">
                        <i class="icon-music"></i><input type="checkbox" name="iconopost[]" id="icono_postd" value="icon-music">
                        <i class="icon-plane"></i><input type="checkbox" name="iconopost[]" id="icono_postg" value="icon-plane">
                        <i class="icon-wrench"></i><input type="checkbox" name="iconopost[]" id="icono_postj" value="icon-wrench">
                        <i class="icon-camera"></i><input type="checkbox" name="iconopost[]" id="icono_postk" value="icon-camera">
                    </div>
                </div>
            </div>
        </div><br>-->
        <div class="row">
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Etiquetas:</label>
                    <div class="col-md-6">
                        <textarea id="tags_2"></textarea>
                        <input type="hidden" id="final_tags" name="final_tags"/>
                    </div>
                </div>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-3 control-label">Publico:</label>
                    <div class="col-md-6">
                        <select class="form-control" id="public" name="public">
                            <option value="0">No</option>
                            <option value="1">Si</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
        <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Imagen Previa*</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                <input type="file" class="default form-control" name="imagen_previa" id="imagen_previa" size="20">
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                            <div class="help-block">
                               Medidas (390 X 292), Extenciones (.jpg, .png)
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
    <div class="col-md-2">
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label">Imagen Post*</label>
                    <div class="fileupload fileupload-new" data-provides="fileupload">
                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                        </div>
                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                        <div>
                            <span class="btn btn-default btn-file">
                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                <input type="file" class="default form-control" name="imagen_post" id="imagen_post" size="20">
                            </span>
                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                            <div class="help-block">
                                Medidas (946 X 381), Extenciones (.jpg, .png)
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <br>
        <div class="form-actions fluid">
            <div class="col-md-1 col-md-6">
                <button type="button" class="btn btn-info btn-block" onclick="finaltags();">Enviar</button>
            </div>
        </div>

        {{Form::close()}}
    </div>
</div>