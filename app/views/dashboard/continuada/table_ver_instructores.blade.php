<table class="table table-striped table-hover top-blue">
	<thead>
		<tr>
			<th>ID</th>
			<th></th>
			<th>NOMBRES</th>
			<th>APELLIDOS</th>
			<th>N° DOCUMENTO</th>
			<th>F. INGRESO</th>
			<th>ACCIONES</th>
		</tr>
	</thead>
	<tbody class="center">                         
		@foreach ($instructores as $row)
		<tr>
			<td>
				<a class=" btn btn-default" data-target="#ajax2" id="{{$row->id}}" data-toggle="modal" onclick="cargar_detalle_instructor($(this).attr(&quot;id&quot;));return false;"><i class="icon-eye-open"></i> {{'INST' . str_pad($row->id, 6, "0", STR_PAD_LEFT);}} </a>
			</td>
			<td>
				<a href="#" class="pull-left nameimg" title=""><img alt="" style="border-radius:50px;" width="50" height="50" src="../{{$row->img}}"></a>
			</td>
			<td>{{$row->name}}</td>
			<td>{{$row->last_name}}</td>
			<td>{{$row->document}}</td>
			<td>{{$row->created_at}}</td>
			<td>
				<a class=" btn btn-default" id="{{$row->id}}" onclick="cargar_hoja_vida($(this).attr(&quot;id&quot;));return false;"><i class="icon-file"></i></a>
			</td>
			                  
		</tr>
		@endforeach
	</tbody>
</table>