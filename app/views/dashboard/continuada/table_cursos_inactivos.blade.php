<a class="btn btn-default">Total Registros: {{count($num)}}</a>
<table class="table top-blue" data-target="soporte/callshopping/">
      <thead>
         <tr>
            <th></th>
            <th>ID</th>
            <th>OCSA</th>
            <th>Curso</th>
            <th>Categoria</th>
            <th>Inicio</th>
            <th>Fin</th>
            <th>Ocsa</th>
         </tr>
      </thead>
      <tbody>
      @foreach($cursosina as $cursoina)
         <tr class=" " style="" id="" >
             <td class="td_center">
               <a class=" btn btn-default" style="" data-target="#ajax" id="{{$cursoina->id}}" data-toggle="modal" onclick='cargarDetalleCursosIna($(this).attr("id"));return false;'><i class="icon-copy"></i></a>
               <a class=" btn btn-default" style="" data-target="#ajax" id="{{$cursoina->id}}" data-toggle="modal" onclick='cargarDetalleCursos($(this).attr("id"));return false;'><i class="icon-edit"></i></a>
               <a class=" btn btn-default" style="" data-target="#asignar_alumnos_curso" id="{{$cursoina->id}}" data-toggle="modal" onclick='asignarAlumnoCurso($(this).attr("id"));return false;'><i class="icon-resize-horizontal"></i></a>
            </td>
            <td class="td_center">
               {{$cursoina->id}}
            </td>
            <td class="td_center">
               {{$cursoina->ocsa}}
            </td>
            <td class="td_center">
               {{$cursoina->name_curso}}
            </td>
            <td class="td_center">
               {{$cursoina->CoursesCategories->category}}
            </td>
            <td class="td_center">
               {{$cursoina->start_date}}
            </td>
            <td class="td_center">
               {{$cursoina->end_date}}
            </td>
            <td>
               <a class=" btn btn-default"  id="{{$cursoina->id}}" href="generarocsa?id={{$cursoina->id}}" target="_blank"><i class="icon-file"></i>Ver Ocsa</a>
            </td>
         </tr>
      @endforeach
      </tbody>
   </table>

<div class="pagination">
    {{$pag->appends(array("cod" => Input::get('cod'),"curso" => Input::get('curso'),"cate" => Input::get('cate')))->links()}}
</div>
<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
  });
</script>
   <script type="text/javascript">
   function cargarDetalleCursosIna(id){
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'detallecursoina',
         type: 'get',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   </script>
   <script type="text/javascript">
      function cargarDetalleCursos(id){
         var parametros = {
            "id": id
         };
         $.ajax({
            data: parametros,
            url:  'detallecurso',
            type: 'get',

            success: function(response){
                  $("#ajax").html(response);
            }
         });
      }
   </script>
   <script type="text/javascript">
       function asignarAlumnoCurso(id){
           var parametros = {
               "id": id
           };
           $.ajax({
               data: parametros,
               url:  'asignaralumnocurso',
               type: 'get',

               success: function(response){
                  $("#asignar_alumnos_curso").html(response);
               }
           });
       }
   </script>