<div class="page-content" id="adminhelpdesk">
    <div class="row">
       <div class="col-md-12">
           <h3 class="page-title">
               Cursos
           </h3>
           
       </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <div class="tabbable tabbable-custom">
                    <ul class="nav nav-tabs">
                        <li class=""><a href="nuevocurso">Nuevo Curso</a></li>
                        <li class=""><a href="cursos" >Cursos Activos</a></li>
                        <li class="active"><a href="cursosinactivos" >Cursos Inactivos</a></li>
                    </ul>
                    <div>
                        <form class="form-inline" action="verequipos" method="get">
                            <div class="search-region">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="ocsa" placeholder="OCSA" value="{{Input::get('id')}}" onkeyup="tabla_lista_cursosina()">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="emp" placeholder="Empresa" value="{{Input::get('id')}}" onkeyup="tabla_lista_cursosina()">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="curso" placeholder="Curso" value="{{Input::get('id')}}" onkeyup="tabla_lista_cursosina()">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="cate" placeholder="Categoria" value="{{Input::get('id')}}" onkeyup="tabla_lista_cursosina()">
                                </div>
                            </div>
                        </form> 
                    </div>
                    <div class="tab-pane fade active in" id="tab_1_1">
                        <div class="table-responsive col-md-12" id="table_cursos_inactivos">
                            @include('dashboard.continuada.table_cursos_inactivos')
                        </div>
                    </div>
                    
                    <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                    </div>

                    <div class="modal fade" id="asignar_alumnos_curso" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    //$( ".fecha_detalle_curso").datepicker({ dateFormat: "yy-mm-dd", minDate: '-5y' });
 </script>
<div id="asdf"></div>
<script type="text/javascript">
    function tabla_lista_cursosina(){

        var ocsa    = $("#ocsa").val();
        var emp     = $("#emp").val();
        var curso   = $("#curso").val();
        var cate    = $("#cate").val();
        $.ajax({
            type: "GET",
            url:  "cursosinactivos",
            data: { ocsa:ocsa, emp:emp, curso:curso, cate:cate, filtro:1}
        })
        .done(function(data) {
            $("#table_cursos_inactivos").html(data); 
        }); 
    }
</script>