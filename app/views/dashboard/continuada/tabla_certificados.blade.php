<table class="table top-blue" data-target="soporte/callshopping/">
    <thead>
        <tr>
        	<th>Detalle</th>
        	<th>Curso</th>
                <th>Estudiante</th>
        	<th>Inicio</th>
        	<th>Fin</th>
                <th>Certificado</th>
        </tr>
    </thead>
    <tbody>
    @foreach($cursos as $curso)
        <tr class=" " style="" id="" >
            <td class="td_center">
                <a class=" btn btn-default" style="" data-target="#ajax" id="{{$curso->id}}" data-toggle="modal" onclick='cargarDetalleCursos($(this).attr("id"));return false;'><i class="icon-edit"></i></a>
            </td>
        	<td class="td_center">{{$curso->name_curso}}</td>
                <td class="td_center">{{$curso->name_curso}}</td>
        	<td class="td_center">{{$curso->start_date}}</td>
        	<td class="td_center">{{$curso->end_date}}</td>
                <td class="td_center cert_{{$curso->id_asignacion}}" >

            <a class="btn btn-success" data-toggle="modal" id="{{$curso->id_asignacion}}" onclick="Guardaridcurso($(this).attr('id'));return false;" href="#static"><i class="icon-ok"></i></a>
            <a class=" btn btn-danger" id="{{$curso->id_asignacion}}" onclick="Rechazarcertificado($(this).attr('id'));return false;"><i class="icon-remove"></i></a>

            </td>
                
        </tr>
    @endforeach
    </tbody>
</table>

<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                 <h4 class="modal-title">Confirmacion</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Libro</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input type="text" class="form-control " placeholder="" data-required="true" name="libro" id="libro" value="">
                                                </div> 
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label">Folio</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input type="text" class="form-control " placeholder="" data-required="true" name="folio" id="folio" value="">
                                                </div> 
                                            </div>
                                        </div>
                                 </div>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
                                 <button type="button" data-dismiss="modal" onclick='Aprobarcertificado();return false;' class="btn btn-success">Aceptar</button>
                              </div>
                           </div>
                        </div>
                     </div>
<div class="pagination">
</div>

<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
  });
</script>
<script type="text/javascript">
   var id_curso_cert = 0;
   function Guardaridcurso(id){
       id_curso_cert = id;
   }
   
   function cargarDetalleCursos(id){
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'detallemicurso',
         type: 'get',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   
    function Aprobarcertificado(){ 
        
        var libro = $("#libro").val();
        var folio = $("#folio").val();
        
      var parametros = {
         "id": id_curso_cert,
         "libro": libro,
         "folio": folio,
         "status": 3
      };
      
      if(libro != "" && folio!=""){
            $.ajax({
               data: parametros,
               url:  'gestioncertificado',
               type: 'get',

               success: function(response){
                   $(".cert_"+id_curso_cert).html('<a class=" btn btn-success">Aprobado</a>');
               }
            });
     }
   }
   
   function Rechazarcertificado(id){
        
      var parametros = {
         "id": id,
         "status": 4
      };
      $.ajax({
         data: parametros,
         url:  'gestioncertificado',
         type: 'get',

         success: function(response){
             $(".cert_"+id_curso_cert).html('<a class=" btn btn-success">Solicitado</a>');
         }
      });
   }
</script>