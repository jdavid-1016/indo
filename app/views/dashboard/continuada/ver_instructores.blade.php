<div class="page-content" id="adminhelpdesk">
   	<div class="row">
		<div class="portlet">
		   	<div class="portlet-title">
		      	<div class="caption"><i class="icon-reorder"></i>Instructores</div>
		   	</div>
		   	<div class="portlet-body">
			    <ul  class="nav nav-tabs">
			        <li class="active"><a href="verinstructores" >Ver Instructores</a></li>
			        <li class=""><a href="instructores">Ingresar Instructores</a></li>
			    </ul> 
			    <div>
			        <form class="form-inline" action="verequipos" method="get">
			            <div class="search-region">
			                <div class="form-group">
			                    <input type="text" class="form-control" id="cod" placeholder="Código" value="{{Input::get('id')}}" onkeyup="tabla_lista_instructores()">
			                </div>
			                <div class="form-group">
			                    
			                    <input type="text" class="form-control" id="nomb" placeholder="Nombres" value="{{Input::get('id')}}" onkeyup="tabla_lista_instructores()">
			                </div>
			                <div class="form-group">
			                    <input type="text" class="form-control" id="doc" placeholder="Documento" value="{{Input::get('id')}}" onkeyup="tabla_lista_instructores()">
			                </div>
			                
			            </div>
			        </form> 
			    </div>
				<div class="table-responsive" id="tabla_ver_instructores">
					@include('dashboard.continuada.table_ver_instructores')
				</div>
			      
			</div>
   		
  	</div>
</div>
    <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
</div>    
<script type="text/javascript">
   	function cargar_detalle_instructor(id) {
      
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'detalleinstructor',
         type: 'get',

         success: function(response){
            $("#ajax2").html(response);
         }
      });

   	}
   	function cargar_hoja_vida(id){

   		window.open('pdfinstructor?id='+id)
   	}
   	function tabla_lista_instructores(){

   		var cod 	= $("#cod").val();
   		var nomb 	= $("#nomb").val();
   		var doc 	= $("#doc").val();
		   $.ajax({
   		        type: "GET",
   		        url:  "verinstructores",
   		        data: { cod:cod, nomb:nomb, doc:doc, filtro:1}
   		})
   		.done(function(data) {
   		    $("#tabla_ver_instructores").html(data); 
   		}); 
   	}
</script>