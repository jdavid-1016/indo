<div class="page-content" id="adminhelpdesk">
   <div class="row">
       <div class="col-md-12">
           <h3 class="page-title">
               Cursos
           </h3>
           <ul class="page-breadcrumb breadcrumb">
               <li>
                   <i class="icon-home"></i>
                   <a href="javascript:;">Página Principal</a> 
                   <i class="icon-angle-right"></i>
               </li>
               <li>
                   <a href="javascript:;">Continuada</a>
                   <i class="icon-angle-right"></i> 
               </li>
               <li>
                   <a href="javascript:;">Cursos</a> 
               </li>
           </ul>
       </div>
   </div>
   <div class="row">
      <div class="col-md-12">
         
         <div class="table-responsive">
            <div class="tabbable tabbable-custom">

            
               <ul class="nav nav-tabs">
                   
                   <li class=""><a href="nuevocurso">Nuevo Curso</a></li>
                   <li class="active"><a href="cursos" >Cursos</a></li>
                   <li class=""><a href="gestionprov" >Categorias</a></li>
                                      
               </ul>
               <div  class="tab-content">
                  <div class="tab-pane fade active in" id="tab_1_1">
                     <div class="table-responsive" id="table_aprobar_shoppings">
                         @include('dashboard.continuada.table_cursos')
                     </div>
                     <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<div id="asdf"></div>