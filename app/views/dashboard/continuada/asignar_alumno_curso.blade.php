<script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/plugins/select2/select2_conquer.css")}}" />
<script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
<div class="modal-dialog modal-wide">
   <div class="modal-content ">
   <form class="form" action="#" >   
      <input type="hidden" value="" id="prospect_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Asignar Alumnos a un Curso</h4>
      </div>
      <div class="modal-body form-body">
                   
            @if($curso->available == 0)
                <div class="alert alert-danger">
                    <strong>Error!</strong> No hay disponibilidad en este Curso para asignar.
                </div>
            
            @endif    
          
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                        <label class="control-label">Alumno</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <select type="text" class="form-control select2me" id="id_alumno_asignar" required="">
                                <option value=""></option>
                                @foreach($alumnos as $al)
                                <option value="{{ $al->id }}">{{ $al->last_name }} {{ $al->last_name2 }} {{ $al->name }} {{ $al->name2 }}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group"> 
                        <label class="control-label">Empresa</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <select type="text" class="form-control select2me" id="id_empresa_asignar" required="">
                                <option value="0">Particular</option>
                                @foreach($empresas as $empresa)
                                <option value="{{ $empresa->id }}">{{ $empresa->name }}</option>
                                @endforeach
                            </select>
                        </div> 
                    </div>
                </div>
                
            </div>          
            
            <a href="#" class="btn btn-info">Disponibles: 
                <span class="badge badge-danger">{{$curso->available}}</span>
            </a>
                      
         <hr>
       
                <table class="table top-blue" data-target="">
                    <thead>
                        <tr>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Estado</th>
                        </tr>
                    </thead>
                    <tbody >
                        @foreach($estudiantes as $estudiante)
                            <tr style="text-align:center;">
                                <td>{{$estudiante->name}} {{$estudiante->name2}}</td>
                                <td>{{$estudiante->last_name}} {{$estudiante->last_name2}}</td>
                                <td>Activo</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

      </div>
      <div class="modal-footer">
        
            <button type="button" class="btn btn-success" onclick="asignar_alumno({{$curso->id}})">Agregar Alumno</button>
        
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>
   </div>
</div>
<script>
    //$( ".fecha_required_next").datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
 </script>
<script type="text/javascript">
    function recargartabla(id){
        var nombre = "1";
        var html = $.ajax({
                type: "GET",
                url: "asignaralumnocurso",
                data: {id:id, nombre:nombre},
                async: false
            }).responseText;
            
            $('#asignar_alumnos_curso').html(html);
            
    }

function asignar_alumno(id) {
    var validacion = 0;
    var id_alumno = $('#id_alumno_asignar').val();
    var id_empresa = $('#id_empresa_asignar').val();
    var disponibles = {{$curso->available}};
    
            if(id_alumno == ""){
               toastr.error('El campo Alumno está vacio', 'Error');
               return;
            }

            var html = $.ajax({
                type: "GET",
                url: "guardaralumno",
                data: {id:id, id_alumno:id_alumno, id_empresa:id_empresa, disponibles:disponibles},
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    asignarAlumnoCurso(id);
                
                    $('#id_alumno_asignar').val('');
                    $('#id_empresa_asignar').val('');

                toastr.success('Se ha asignado el alumno correctamente', 'Nuevo Alumno');

            } else {
                toastr.error('No ha sido posible guardar el Alumno', 'Error');
            }
        
}
</script>