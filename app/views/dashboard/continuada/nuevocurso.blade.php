<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Gestion Cursos
            </h3>
        </div>
    </div>
    <div class="portlet-body">
        <ul  class="nav nav-tabs">
            <li class="active"><a href="nuevocurso">Nuevo Curso</a></li>
            <li class=""><a href="cursos" >Cursos Activos</a></li>
            <li class=""><a href="cursosinactivos" >Cursos Inactivos</a></li>

        </ul>

        <div class="row">
            <div class="col-md-6">
                <div class="col-md-12">
                    <ul class="page-breadcrumb breadcrumb">
                        <h4><b>Formulario para Cursos</b></h4>
                    </ul>
                </div>
                <form class="form-horizontal" id="form">
                    <div class="form-body">
                            <div class="row">
                                
                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Codigo</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="coding" required="">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nombre</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="name_curso" required="">
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Abierto</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="open" required="">
                                                <option value="0">Si</option>
                                                <option value="1">No</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Estado</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="status" required="">
                                                <option value="0">Inactivo</option>
                                                <option value="1">Activo</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Intensidad Horaria</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="time_intensity" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">OCSA</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="ocsa" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Lugar instrucción teórica</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="inst_teorica" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Lugar instrucción practica</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="inst_practica" required="">
                                        </div>
                                    </div>                                    
                                    
                                </div>

                                <div class="col-md-6">
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Fecha de inicio</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control fecha_nuevoalum" id="start_date" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Fecha de finalizacion</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control fecha_nuevoalum" id="end_date" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Categoria</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="courses_categories_id" required="">
                                                @foreach($categories as $cat)
                                                <option value="{{ $cat->id }}">{{ $cat->category }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Cupo</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="cupo" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Horario</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="schedule" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Inspector</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="inspector" required="">
                                                @foreach($inspectores as $inspector)
                                                <option value="{{ $inspector->id }}">{{ $inspector->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Fechas</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="fecha_teorica" required="">
                                        </div>
                                    </div>
                                    <br>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Fechas</label>
                                        <div class="col-md-8">
                                           <input type="text" class="form-control" id="fecha_practica" required="">
                                        </div>
                                    </div>
                                    
                                    <div class="form-group" style="display:none">
                                        <label class="col-md-4 control-label">Instructor</label>
                                        <div class="col-md-8">
                                            <select type="text" class="form-control" id="instructors_id" required="">
                                                @foreach($instructors as $ins)
                                                <option value="{{ $ins->id }}">{{ $ins->name }} {{ $ins->last_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        <div class="form-actions fluid">
                            <div class="col-md-1 col-md-11">
                                <a class="btn btn-success" onclick="guardarCourse()">Guardar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="col-md-6">
                <div class="col-md-12">
                    <ul class="page-breadcrumb breadcrumb">
                        <h4><b>Formulario para Categorias</b></h4>
                    </ul>
                </div>
                <form class="form-horizontal" id="form">
                    <div class="form-body">
                            <div class="row">
                                
                                <div class="col-md-6">
                                                                        
                                    <div class="form-group">
                                        <label class="col-md-4 control-label">Nombre*</label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="category" required="">
                                            <div id="displayprovp"></div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="col-md-6">

                                </div>
                            </div>
                        <div class="form-actions fluid">
                            <div class="col-md-1 col-md-11">
                                <a class="btn btn-success" onclick="guardarCategory()">Guardar</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>