<table class="table top-blue" data-target="soporte/callshopping/">
    <thead>
        <tr>
        	<th>Alumno</th>
        	<th></th>
            @foreach($fechas as $fecha)
            <th>{{$fecha->date_faill}}</th>
            @endforeach
            @if($fecha_nueva != "")
                <th>{{$fecha_nueva}}</th>
            @endif
        </tr>
    </thead>
    <tbody>
    <?php $i = 0; ?>
    @foreach($alumnos as $alumno)
        <tr class=" " style="" id="" >
            <td class="td_center">{{$alumno->name}} {{$alumno->name2}} {{$alumno->last_name}} {{$alumno->last_name2}}</td>
            <td class="td_center">
                <a href="#" class="pull-left nameimg" title="">
                    <img alt="" style="border-radius:50px;" width="50" height="50" src="../{{$alumno->img_min}}">
                </a>
            </td>
            <?php $j = 0; ?>
            @foreach($fechas as $fecha)
            
            <td class="td_center">
                <a class=" btn btn-default" id="falla_valor_{{$fallas[$i][$j]['id']}}" onclick="cambiar_falla({{$fallas[$i][$j]['id']}},{{$fallas[$i][$j]['valor']}})"><i id="falla_{{$fallas[$i][$j]['id']}}" class="{{$fallas[$i][$j]['falla']}}"></i></a>
                
            </td>


            <?php $j++; ?>
            @endforeach
            @if($fecha_nueva != "")
            <td class="td_center" >
                <a class=" btn btn-success" id="valor_{{$alumno->id}}" onclick='cambiar_valor({{$alumno->id}}, 1)'><i class="icon-ok" id="icon_{{$alumno->id}}" ></i></a>
                <input type="hidden" id="valor_asist_{{$alumno->id}}" value="1">
            </td>
            @endif    
        </tr>
    <?php $i++; ?>
    @endforeach
    <input type="hidden" id="fecha" value="{{$fecha_nueva}}">
    </tbody>
</table>
<div class="form-group">
    <div class="col-md-3">
        <a class=" btn btn-success" id="" onclick='guardar_asistencia(); return false;'><i class="icon-ok"></i> Guardar</a></td>
    </div>
</div>
<script type="text/javascript">
    function cambiar_valor(id, valor){
        
        if (valor == 1) {

            $("#icon_"+id).removeClass("icon-ok").addClass("icon-remove");
            $("#valor_"+id).removeClass("btn-success").addClass("btn-danger");
            $("#valor_asist_"+id).val("0");
            $("#valor_"+id).attr( "onclick", 'cambiar_valor('+id+', 0)' );
        }else{
            $("#icon_"+id).removeClass("icon-remove").addClass("icon-ok");
            $("#valor_"+id).removeClass("btn-danger").addClass("btn-success");
            $("#valor_asist_"+id).val("1");
            $("#valor_"+id).attr( "onclick", 'cambiar_valor('+id+', 1)' );
        }
    }
    function cambiar_falla(id, valor){

        if (valor == 1) {

            
            $("#falla_"+id).removeClass("icon-remove").addClass("icon-ok");
            $("#falla_valor_"+id).attr( "onclick", 'cambiar_falla('+id+', 0)' );
        }else{
            $("#falla_"+id).removeClass("icon-ok").addClass("icon-remove");
            $("#falla_valor_"+id).attr( "onclick", 'cambiar_falla('+id+', 1)' );
        }


        var html = $.ajax({
            type: "GET",
            url: "cambiarfalla",
            data: {id:id, valor:valor},
            async: false
        }).responseText;
            
        if (html == 1) {
            toastr.success('Se actualizo correctamente la Asistencia', 'Actualizacion de Asistencia');
        }else{
            toastr.error('No ha sido posible actualizar la Asistencia', 'Error');
        }


    }
    function guardar_asistencia(){
        var id_alum = new Array();
        var valor   = new Array();
        var i = 0;

        @foreach($alumnos as $alumno)

            id_alum[i]  = {{$alumno->id}};
            valor[i]    = $("#valor_asist_{{$alumno->id}}").val();
            i++;
        @endforeach

        var fecha = $("#fecha").val();
        var curso = {{$curso}};

        


        // alert(fecha);
        // return;

        var html = $.ajax({
            type: "POST",
            url: "guardarfallas",
            data: {fecha:fecha, id_alum:id_alum, valor:valor, curso:curso},
            async: false
        }).responseText;
            
        if (html == 1) {
            toastr.success('Se ingreso correctamente las fallas', 'Cargue de Fallas');
        }
    }
</script>