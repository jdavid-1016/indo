<div class="modal-dialog modal-wide">
   <div class="modal-content ">
   <form class="form" action="#" >   
      <input type="hidden" value="" id="prospect_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle Curso</h4>
      </div>
      <div class="modal-body form-body">
                   
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Codigo</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="coding" id="coding" value="{{$curso->coding}}" disabled="">
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Nombre</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="name_curso" id="name_curso" value="{{$curso->name_curso}}" disabled="">
                  </div> 
               </div>
            </div>
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Categoria</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select type="text" class="form-control" id="courses_categories_id" required="" disabled="">
                          @foreach($categories as $cat)
                          <option <?php if($curso->courses_categories_id==$cat->id){ echo "selected"; } ?> value="{{ $cat->id }}">{{ $cat->category }}</option>
                          @endforeach
                      </select>
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Abierto</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select type="text" class="form-control" id="open" required="" disabled="">                                
                            <option <?php if($curso->open==1){ echo "Verdadero"; }else{ echo "Falso"; }?> value="1">Verdadero</option>
                            <option <?php if($curso->open==1){ echo "Verdadero"; }else{ echo "Falso"; }?> value="0">Falso</option>
                     </select>
                  </div>
               </div>
            </div>
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Fecha de inicio</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control fecha_required_next" placeholder="" data-required="true" name="start_date" id="start_date" value="{{$curso->start_date}}" disabled="">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Fecha de finalizacion</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control fecha_required_next" placeholder="" data-required="true" name="end_date" id="end_date" value="{{$curso->end_date}}" disabled="">
                  </div> 
               </div>
            </div>
         </div>          
                 
         <!-- <button type="button" class="btn btn-success" onclick="editar_curso({{$curso->id}})">Guardar Cambios</button> -->
                      
         <hr>
         <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_estudiantes">
                              Estudiantes
                              </a>
                           </h4>
                        </div>
                    <div id="collapse_estudiantes" class="panel-collapse collapse" style="height: auto;">
                        <table class="table top-blue" data-target="">
                            <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Estado</th>
                                </tr>
                            </thead>
                            <tbody >
                                @foreach($estudiantes as $estudiante)
                                    <tr style="text-align:center;">
                                        <td>{{$estudiante->name}} {{$estudiante->name2}}</td>
                                        <td>{{$estudiante->last_name}} {{$estudiante->last_name2}}</td>
                                        <td>Activo</td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        </div>
             </div>
         
         <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_materias">
                        Instructores
                        </a>
                    </h4>
                </div>
                <div id="collapse_materias" class="panel-collapse collapse" style="height: auto;">
                    <table class="table top-blue" data-target="">
                        <thead>
                            <tr>
                                <th>Instructor</th>
                                <th>Foto</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody id="tabla_materias_cursos">
                            @include('dashboard.continuada.body_materias')
                        </tbody>
                    </table>
                </div>
            </div>        
      </div>
      <div class="modal-footer">
                 
         <!-- <button type="button" class="btn btn-success" onclick="asignar_materias({{$curso->id}})">Guardar Materia</button> -->
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>
   </div>
</div>
<script>
    $( ".fecha_required_next").datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
 </script>
<script type="text/javascript">
    function recargarBody(id){
        var nombre = "1";
        var html = $.ajax({
                type: "GET",
                url: "detallecurso",
                data: {id:id, nombre:nombre},
                async: false
            }).responseText;
            
            $('#tabla_materias_cursos').html(html);
            
    }
    
    
    //funcion para guardar un nuevo curso
function asignar_materias(id) {
    var validacion = 0;
    var materia = $('#materia').val();
    var instructor = $('#instructor').val();
    
    if(materia !="" && instructor !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
                return;
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarmateria",
                data: {id:id, materia:materia, instructor:instructor },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    recargarBody(id);
                
                    $('#materia').val('');

                toastr.success('Se ha asignado la materia correctamente', 'Nueva Materia');

            } else {
                toastr.error('No ha sido posible guardar la materia', 'Error');
            }
        }
}
    
    
//funcion para guardar un nuevo curso
function editar_curso(id) {
    var validacion = 0;
    var coding = $('#coding').val();
    var name_curso = $('#name_curso').val();
    var open = $('#open').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var courses_categories_id = $('#courses_categories_id').val();
    
    if(coding !="" && name_curso !="" && start_date !="" && end_date !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "duplicarcurso",
                data: {id:id, coding:coding, name_curso:name_curso, open:open, start_date:start_date, 
                    end_date:end_date, courses_categories_id:courses_categories_id },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('El nuevo curso se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar el Curso', 'Error');
            }
        }
}


</script>
<script>
    $( ".fecha_required_next").datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
    
    function generar_diploma(id_curso, id_alumn){
        
        var dir = "generarmidiplomacont?id="+id_curso;
        window.open(dir);
    }
 </script>