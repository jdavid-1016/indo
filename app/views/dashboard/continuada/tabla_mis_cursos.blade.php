<table class="table top-blue" data-target="soporte/callshopping/">
    <thead>
        <tr>
        	<th>Detalle</th>
        	<th>Curso</th>
        	<th>Inicio</th>
        	<th>Fin</th>
                <th>Certificado</th>
        </tr>
    </thead>
    <tbody>
    @foreach($cursos as $curso)
        <tr class=" " style="" id="" >
            <td class="td_center">
                <a class=" btn btn-default" style="" data-target="#ajax" id="{{$curso->id}}" data-toggle="modal" onclick='cargarDetalleCursos($(this).attr("id"));return false;'><i class="icon-edit"></i></a>
            </td>
        	<td class="td_center">{{$curso->name_curso}}</td>
        	<td class="td_center">{{$curso->start_date}}</td>
        	<td class="td_center">{{$curso->end_date}}</td>
                <td class="td_center cert_{{$curso->id_asignacion}}" >
                 @if($curso->status_certification==0)
                <a class=" btn btn-info" data-toggle="modal" id="{{$curso->id_asignacion}}" onclick='Guardaridcurso($(this).attr("id"));return false;' href="#static">Solicitar</a>
                @elseif($curso->status_certification==1)
                <a class=" btn btn-success">Solicitado</a>
                @elseif($curso->status_certification==3)
                <a class=" btn btn-success" data-toggle="modal" href="#static2">Aprobado</a>
                <!--<a href="#" class=" btn btn-success" onclick="generar_diploma({{ $curso->id }}, {{$curso->id_alumno}}, {{$curso->id_asignacion}})"><i class="icon-print"></i> Descargar</a>-->
                @endif
            </td>
                
        </tr>
    @endforeach
    </tbody>
</table>

<div class="pagination">
</div>

<div id="static" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                 <h4 class="modal-title">Confirmacion</h4>
                              </div>
                              <div class="modal-body">
                                 <p>La respuesta de su solicitud tiene un tiempo de respuesta de 48 horas,
                                     desea continuar con la solicitud?</p>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
                                 <button type="button" data-dismiss="modal" onclick='GuardarSolCertificado();return false;' class="btn btn-success">Aceptar</button>
                              </div>
                           </div>
                        </div>
                     </div>

<div id="static2" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
                        <div class="modal-dialog">
                           <div class="modal-content">
                              <div class="modal-header">
                                 <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                 <h4 class="modal-title">Certificado Aprobado</h4>
                              </div>
                              <div class="modal-body">
                                 <p>Su solicitud de certificado ha sido aprobada, por favor acerquese a las oficinas de educacion continuada para reclamarlo.</p>
                              </div>
                              <div class="modal-footer">
                                 <button type="button" data-dismiss="modal" class="btn btn-default">Cancelar</button>
                                 <button type="button" data-dismiss="modal" class="btn btn-success">Aceptar</button>
                              </div>
                           </div>
                        </div>
                     </div>
<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });    
  });
</script>
<script type="text/javascript">
   var id_curso_cert = 0;
   function Guardaridcurso(id){
       id_curso_cert = id;
   }
   
   function cargarDetalleCursos(id){
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'detallemicurso',
         type: 'get',

         success: function(response){
               $("#ajax").html(response);
         }
      });
   }
   
    function GuardarSolCertificado(){
        
      var parametros = {
         "id": id_curso_cert
      };
      $.ajax({
         data: parametros,
         url:  'solicitudcertificado',
         type: 'get',

         success: function(response){
             $(".cert_"+id_curso_cert).html('<a class=" btn btn-success">Solicitado</a>');
         }
      });
   }
   
   function generar_diploma(id_curso, id_alumno, id_asignacion){
        
        var parametros = {
         "id": id_asignacion
        };
      $.ajax({
         data: parametros,
         url:  'certificadodescargado',
         type: 'get',

         success: function(response){
             $(".cert_"+id_curso_cert).html('<a class=" btn btn-info" data-toggle="modal" id="{{$curso->id_asignacion}}" onclick="Guardaridcurso($(this).attr("id"));return false;" href="#static">Solicitar</a>');
         }
      });
      
        var dir = "generardiplomacont?id_curso="+id_curso+"&id_alumno="+id_alumno;        
        window.open(dir);
    }
</script>