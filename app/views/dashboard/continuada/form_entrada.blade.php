<div class="page-content" id="">
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Formulario de Entrada</div>
            </div>
            <div class="portlet-body">
                <ul  class="nav nav-tabs">
                    <li class="active"><a href="formentrada">Formulario de Entrada</a></li>
                    <li class=""><a href="formsalida">Formulario de Salida</a></li>
                </ul> 
                <div class="row">
                    <div class="col-md-6">
                        
                        <h3>Propósito</h3>
                        <p>
                            La Corporación Educativa Indoamericana se encuentra interesada en conocer sus expectativas respecto al Curso Especial  al que va a ingresar, por tal motivo es muy importante que diligencie el siguiente formato teniendo en cuenta que la información debe ser verídica ya que esta contribuye al desarrollo del curso y al alcance de los objetivos propuestos.
                        </p>
                        <p>
                            1 ¿Cuáles son sus expectativas de aprendizaje respecto al curso que va a ingresar?
                            <textarea class="form-control" id="pregunta1"></textarea>
                        </p>
                        <p>
                        <p>
                            2 ¿Cuáles son los temas en que le gustaría profundizar para mejorar su formación?
                            <textarea class="form-control" id="pregunta2"></textarea>
                        </p>
                        <p>
                            3 ¿Cuál es la experiencia que tiene con respecto a la temática del curso?
                            <textarea class="form-control" id="pregunta3"></textarea>
                        </p>
                        <p>
                            4 En caso de tener experiencia ¿Cómo puede contribuir para dinamizar el desarrollo del curso?
                            <textarea class="form-control" id="pregunta4"></textarea>
                        </p>
                        <p>
                            5 ¿Qué compromisos puede proyectar para ser multiplicador de los conocimientos adquiridos durante el desarrollo del curso?
                            <textarea class="form-control" id="pregunta5"></textarea>
                        </p>
                    </div>

                </div>
                <button type="button" class="btn btn-success" onclick="EvaluacionEntrada();">Guardar</button>
            </div>
        </div>
    </div>
</div>