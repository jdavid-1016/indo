<div class="modal-dialog modal-wide">
   <div class="modal-content ">
   <form class="form" action="#" >   
      <input type="hidden" value="" id="prospect_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle Curso</h4>
      </div>
      <div class="modal-body form-body">
        <ul  class="nav nav-tabs">
            <li class="active"><a href="#info_curso" data-toggle="tab">Detalle del Curso</a></li>
            <li class=""><a href="#alumnos"  data-toggle="tab">Alumnos</a></li>
            <li class=""><a href="#instructores"  data-toggle="tab">Instructores</a></li>
            <li class=""><a href="#cargar_horas"  data-toggle="tab">Cargar Horas</a></li>
        </ul>
        <div class="tab-content">      
            <div class="tab-pane fade active in" id="info_curso">
                   
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Nombre</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <input type="text" class="form-control " placeholder="" data-required="true" name="name_curso" id="name_curso" value="{{$curso->name_curso}}">
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group"> 
                            <label class="control-label">Categoria</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <select type="text" class="form-control" id="courses_categories_id" required="">
                                    @foreach($categories as $cat)
                                    <option <?php if($curso->courses_categories_id==$cat->id){ echo "selected"; } ?> value="{{ $cat->id }}">{{ $cat->category }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
          
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group"> 
                          <label class="control-label">Intensidad Horaria</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control " placeholder="" data-required="true" name="time_intensity" id="time_intensity" value="{{$curso->time_intensity}}">
                          </div>
                       </div>
                    </div>
                      <div class="col-md-6">
                       <div class="form-group"> 
                          <label class="control-label">Horario</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control " placeholder="" data-required="true" name="schedule" id="schedule" value="{{$curso->schedule}}">
                          </div>
                       </div>
                      </div>
                </div>
          
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Abierto</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <select type="text" class="form-control" id="open" required="">
                                        <option <?php if($curso->open==1){ echo "selected"; } ?> value="1">Si</option>
                                        <option <?php if($curso->open==0){ echo "selected"; }?> value="0">No</option>
                             </select>
                          </div>
                       </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Inspector</label>
                          <label class="control-label" onclick="crear_nuevo_instructor()" id="btn_nuevo_ins"><i class="icon-plus"></i></label>
                            <div class="input-group" id="crear_nuevo_instructor" style="display:none">
                                <input type="text" class="form-control " placeholder="Nombre Instructor" data-required="true" name="inspector" id="nombre_inspector" value="">
                                <button type="button" class="btn btn-success" onclick="crear_instructor()">Crear Instructor</button>
                                
                            </div>
                          <div class="input-group" id="campo_inspector">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <select  class="form-control" id="inspector" required="">
                                @foreach($inspectores as $inspector)
                                <option @if($curso->courses_inspectors_id==$inspector->id) selected @endif  value="{{ $inspector->id }}">{{ $inspector->name }}</option>
                                @endforeach
                             </select>
                             <!--<input type="text" class="form-control " placeholder="" data-required="true" name="inspector" id="inspector" value="{{$curso->inspector}}">-->
                          </div> 
                       </div>
                    </div>
                </div>
          
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group"> 
                          <label class="control-label">Lugar instrucción teórica</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control " placeholder="" data-required="true" name="inst_teorica" id="inst_teorica" value="{{$curso->theorist_places}}">
                          </div>
                       </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Fechas</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control " placeholder="" data-required="true" name="fecha_teorica" id="fecha_teorica" value="{{$curso->theorist_dates}}">
                          </div> 
                       </div>
                    </div>
                </div>
          
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group"> 
                          <label class="control-label">Lugar instrucción practica</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control " placeholder="" data-required="true" name="inst_practica" id="inst_practica" value="{{$curso->practice_places}}">
                          </div>
                       </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Fechas</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control " placeholder="" data-required="true" name="fecha_practica" id="fecha_practica" value="{{$curso->practice_dates}}">
                          </div> 
                       </div>
                    </div>
                </div>
          
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group"> 
                          <label class="control-label">Fecha de inicio</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control fecha_detalle_curso" placeholder="" data-required="true" name="start_date" id="start_date" value="{{$curso->start_date}}">
                          </div> 
                       </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Fecha de finalizacion</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control fecha_detalle_curso" placeholder="" data-required="true" name="end_date" id="end_date" value="{{$curso->end_date}}">
                          </div> 
                       </div>
                    </div>
                </div>          
                 
                <button type="button" class="btn btn-success" onclick="editar_curso({{$curso->id}})">Guardar Cambios</button>
            </div>          
            <div class="tab-pane fade" id="alumnos">
                <div class="panel panel-default">
                    
                    
                        <table class="table top-blue" data-target="">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Ultima Actualizacion</th>
                                    <th>Nota</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($estudiantes as $estudiante)
                                    <tr style="text-align:center;" id="table-est-{{$estudiante->id_relacion}}">
                                        <td>
                                            <a class=" btn btn-default" style="" href="detallealumno?id={{$estudiante->id_est}}&idcurso={{$curso->id}}" target="_blank" id="" data-toggle="modal"><i class="icon-edit"></i> {{$estudiante->id_est}}</a>
                                            <a href="#" class="btn btn-danger" onclick="eliminar_asignacion_est({{$estudiante->id_relacion}})"><i class="icon-remove"></i></a>
                                        </td>
                                        <td>{{$estudiante->name}} {{$estudiante->name2}}</td>
                                        <td>{{$estudiante->last_name}} {{$estudiante->last_name2}}</td>
                                        <td>{{$estudiante->updated_at}}</td>
                                        @if($cont != -1)
                                            @foreach($notas as $nota) 
                                                @if($nota['id_student'] == $estudiante->id_relacion)
                                                    <td><input type="text" class="form-control" style="max-width:100px" id="notas_{{$estudiante->id_relacion}}" value="{{$nota['nota_f']}}"></td>
                                                @endif
                                            @endforeach
                                        @else
                                            <td><input type="text" class="form-control" style="max-width:100px" id="notas_{{$estudiante->id_relacion}}" value="100"></td>
                                        @endif
                                        
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    
                    <div class="modal-footer">
                        @if($cont == -1)
                        <button type="button" class="btn btn-success" onclick="guardar_notas({{$curso->id}})">Guardar Notas</button>
                        @endif
                        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="instructores">
                <div class="panel panel-default">
                    
                    
                        <table class="table top-blue" data-target="">
                            <thead>
                                <tr>
                                    <th>Instructor</th>
                                    <th>Pago por hora</th>
                                    <th>Foto</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tabla_materias_cursos">
                                @include('dashboard.continuada.body_materias')
                            </tbody>
                        </table>
                    
                </div>
         
         
                <div class="row">
                    <div class="col-md-6" style="display:none">
                        <div class="form-group"> 
                            <label class="control-label">Materia</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <input type="text" class="form-control " placeholder="" data-required="true" name="materia" id="materia" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Instructor</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <select class="form-control" placeholder="" data-required="true" name="instructor" id="instructor">
                                    @foreach($instructors as $ins)
                                    <option value="{{ $ins->id }}">{{ $ins->last_name }} {{ $ins->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Pago por hora</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-dollar"></i></span>
                                <input class="form-control" placeholder="" data-required="true" name="price" id="price">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                   <button type="button" class="btn btn-success" onclick="asignar_materias({{$curso->id}})">Guardar Instructor</button>
                   <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                </div>
            </div>
            <div class="tab-pane fade" id="cargar_horas">
                <div class="col-md-12 note note-info">
                    <h4 class="block">Cargar horas a instructor</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group" >
                                <div class="col-md-12">

                                    <label class="control-label">Instructor</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                        <select id="horas_intructor" name="estudios" class="form-control">
                                            @foreach($materias as $mat)
                                                <option value="{{$mat->id}}"> {{$mat->CoursesInstructors->Users->name}} {{$mat->CoursesInstructors->Users->last_name}}</option>
                                            @endforeach
                                            
                                        </select>
                                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" >
                                <div class="col-md-12">
                                    <label class="control-label">Fecha</label>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                        <input id="horas_fecha" type="text" class="form-control" placeholder="fecha">
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" >
                                <div class="col-md-12">
                                    <label class="control-label">Hora</label>
                                    <div class="input-group">

                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                        <input id="horas_hora" type="text" class="form-control" placeholder="hora">
                                    </div> 
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group" >
                                <div class="col-md-12">
                                    <label class="control-label">Aula</label>
                                    <div class="input-group">

                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                        <input id="horas_aula" type="text" class="form-control" placeholder="aula">
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table top-blue" data-target="">
                                <thead>
                                    <tr>
                                        <th>INSTRUCTOR</th>
                                        <th>FECHA</th>
                                        <th>HORA</th>
                                        <th>SALON</th>
                                        <th>TEMA</th>
                                        <th>OBSERVACIÓN</th>
                                        <th>OBSERVACIÓN</th>
                                        <th>VERIFICADA</th>
                                    </tr>
                                </thead>
                                <tbody id="tabla_horas_instructor">
                                    @include('dashboard.continuada.body_horas')
                                </tbody>
                            </table>
                        </div>
                    </div>
            
            
                
                <div class="modal-footer">
                   <button type="button" class="btn btn-success" onclick="asignar_horas({{$curso->id}})">Guardar Horas</button>
                   <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                </div>
            </div>

        </div>
      
   
        </div>
   </form>
   
</div>
<script type="text/javascript">
    function asignar_horas(id_curso){

        var horas_intructor = $("#horas_intructor").val();
        var horas_fecha = $("#horas_fecha").val();
        var horas_hora = $("#horas_hora").val();
        var horas_aula = $("#horas_aula").val();

        var html = $.ajax({
                type: "GET",
                url: "../pqrs/ingresarhoras",
                data: { horas_intructor:horas_intructor,
                        horas_fecha:horas_fecha,
                        horas_hora:horas_hora,
                        horas_aula:horas_aula,
                        id_curso:id_curso
                    },
                async: false
            }).responseText;
        if (html != 1) {
            toastr.success('Se cargaron las horas correctamente', 'Cargue de horas');
            $("#tabla_horas_instructor").html(html);
        }else{
            toastr.error('No ha sido posible cargar las horas', 'Error');
        }
    }
    function crear_nuevo_instructor(){
        $("#campo_inspector").hide('slow');
        $("#crear_nuevo_instructor").show('show');
        $("#btn_nuevo_ins").html('<i class="icon-remove"></i>');

        $("#btn_nuevo_ins").attr('onclick','cancelar_creacion()');


        


    }
    function cancelar_creacion(){
        $("#campo_inspector").show('show');
        $("#crear_nuevo_instructor").hide('slow');
        $("#btn_nuevo_ins").html('<i class="icon-plus"></i>');

        $("#btn_nuevo_ins").attr('onclick','crear_nuevo_instructor()');
    }
    function crear_instructor(){
        var nombre_inspector = $("#nombre_inspector").val();

        var html = $.ajax({
                type: "GET",
                url: "crearinspector",
                data: {nombre:nombre_inspector},
                async: false
            }).responseText;
            $("#campo_inspector").show('show');
            $("#crear_nuevo_instructor").hide('slow');
            $("#btn_nuevo_ins").html('<i class="icon-plus"></i>');
            $("#btn_nuevo_ins").attr('onclick','crear_nuevo_instructor()');
            $("#nombre_inspector").val("");
            $('#inspector').html(html);

    }
    function recargarBody(id){
        var nombre = "1";
        var html = $.ajax({
                type: "GET",
                url: "detallecurso",
                data: {id:id, nombre:nombre},
                async: false
            }).responseText;
            
            $('#tabla_materias_cursos').html(html);
            
    }
    function cargarDetalleAlumno(id){
        var parametros = {
            "id": id
        };
        $.ajax({
            data: parametros,
            url:  'detallealumno',
            type: 'get',

            success: function(response){
                window.open(response);
                $("#ajax2").html(response);
            }
        });
    }
    
    //funcion para guardar un nuevo curso
    function asignar_materias(id) {
        var validacion = 0;
        var materia = $('#materia').val();
        var instructor = $('#instructor').val();
        var price = $('#price').val();
        
        if(instructor !=""){
                   validacion = 1;
                }else{
                    toastr.error('Todos los campos con son obligatorios', 'Error');
                    return;
                }

            if(validacion == 1){
                var html = $.ajax({
                    type: "GET",
                    url: "guardarmateria",
                    data: {id:id, materia:materia, instructor:instructor, price:price },
                    async: false
                }).responseText;

                if (parseInt(html) == 1) {
                    
                        recargarBody(id);
                    
                        $('#materia').val('');
                        $('#price').val('');

                    toastr.success('Se ha asignado el instructor correctamente', 'Asignacion instructor');

                } else {
                    toastr.error('No ha sido posible asignar el instructor', 'Error');
                }
            }
    }
    
    
    //funcion para guardar un nuevo curso
    function editar_curso(id) {

        var validacion = 0;    
        var name_curso = $('#name_curso').val();
        var courses_categories_id = $('#courses_categories_id').val();
        var time_intensity = $('#time_intensity').val();
        var schedule = $('#schedule').val();
        var open = $('#open').val();
        var inspector = $('#inspector').val();    
        var inst_teorica = $('#inst_teorica').val();
        var fecha_teorica = $('#fecha_teorica').val();
        var inst_practica = $('#inst_practica').val();
        var fecha_practica = $('#fecha_practica').val();
        var start_date = $('#start_date').val();
        var end_date = $('#end_date').val();
        
        if(name_curso !="" && start_date !="" && end_date !="" && time_intensity !="" && schedule !="" && schedule !=""){
                   validacion = 1;
                }else{
                    toastr.error('Todos los campos con son obligatorios', 'Error');
                }

            if(validacion == 1){
                var html = $.ajax({
                    type: "GET",
                    url: "editarcurso",
                    data: {id:id, name_curso:name_curso, courses_categories_id:courses_categories_id, time_intensity:time_intensity, schedule:schedule, 
                        open:open, inspector:inspector, inst_teorica:inst_teorica, fecha_teorica:fecha_teorica, inst_practica:inst_practica, fecha_practica:fecha_practica, 
                        start_date:start_date, end_date:end_date },
                    async: false
                }).responseText;

                if (parseInt(html) == 1) {

                    toastr.success('El curso se ha guardado correctamente', 'Actualizar Curso');

                } else {
                    toastr.error('No ha sido posible guardar los cambios', 'Error');
                }
            }
    }
    function guardar_notas(id_curso){
        var id_alum     = new Array();
        var valor       = new Array();
        var id_prof     = new Array();
        var i = 0;
        var j = 0;

        @foreach($estudiantes as $alumno)
            
            id_alum[i]  = {{$alumno->id_relacion}};
            valor[i]    = $("#notas_{{$alumno->id_relacion}}").val();
            i++;
        @endforeach

        @foreach($materias as $mat2)
            id_prof[j]    = {{$mat2->CoursesInstructors->id}};
            j++;
        @endforeach

        
        var curso = id_curso;
        var html = $.ajax({
            type: "POST",
            url: "guardarnotas2",
            data: {id_alum:id_alum, valor:valor, curso:curso, id_prof:id_prof},
            async: false
        }).responseText;
            
        if (html == 1) {
            toastr.success('Se ingreso correctamente las Notas', 'Cargue de Notas');
        }else{
            toastr.error('No ha sido posible ingresar las notas', 'Error');
        }
    }
</script>