<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <h3 class="page-title">
                Gestión de Blog
            </h3>
            <ul class="page-breadcrumb breadcrumb">
                <li>
                    <i class="icon-home"></i>
                    <a href="javascript:;">Gestión de Blog</a> 
                    <i class="icon-angle-right"></i>
                </li>
                <!-- <li>
                    <a href="javascript:;">Aspirantes</a>
                    <i class="icon-angle-right"></i>
                </li>
                <li>
                    <a href="javascript:;">Gestion Emails</a> 
                </li> -->
            </ul>
        </div>
    </div>
    
    <div class="row">
        {{Form::open( array('url'=>'aspirantes/enviaremail', 'method' => 'POST', 'files' => 'true', 'enctype' => "multipart/form-data"))}}
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-3 control-label">Destinatarios:</label>
                    <div class="col-md-6">
                        <select  class="form-control" id="para" name="para" required/>
                        <option value=""></option>
                        <option value="1">Alumnos Activos</option>
                        <option value="1">Alumnos Inactivos</option>
                        <option value="1">Todos</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-3 control-label">Lectura Obligatoria:</label>
                    <div class="col-md-6">
                        <select  class="form-control" id="para" name="para" required/>
                        <option value=""></option>
                        <option value="1">SI</option>
                        <option value="1">NO</option>
                        
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-3 control-label">Titulo:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="para" name="para" required/>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row"> 
            <div class="col-md-10">
                <div class="form-group">
                    <label class="col-md-1 control-label">Descripcion:</label>
                    <div class="col-md-6">
                        <textarea class="ckeditor form-control" id="mensaje" name="mensaje" rows="6"></textarea>
                    </div>
                </div>
            </div>
        </div><br>
        <!-- <div class="row"> 
            <div class="col-md-5">
                <div class="form-group">
                    <label class="col-md-3 control-label">:</label>
                    <div class="col-md-6">
                        <input type="text" class="form-control" id="asunto" name="asunto" required/>
                    </div>
                </div>
            </div>
        </div> -->
        <br>
            <div class="form-actions fluid">
                <div class="col-md-1 col-md-6">
                    <button type="submit" class="btn btn-info btn-block">Enviar</button>
                </div>
            </div>            

        {{Form::close()}}
    </div>
</div>