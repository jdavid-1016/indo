@foreach($horas as $horas)
    <tr style="text-align:center;" id="table-inst-{{$horas->id}}" >
        <td>{{$horas->CoursesSubjects->CoursesInstructors->Users->name}} {{$horas->CoursesSubjects->CoursesInstructors->Users->last_name}}</td>
        <td>{{$horas->date}}</td>
        <td>{{$horas->hour}}</td>
        <td>{{$horas->GeneralClassrooms->classroom}}</td>
        <td>{{$horas->theme}}</td>
        <td>{{$horas->observation_teacher}}</td>
        <td>@if($horas->observation_adm == "") <textarea class="form-control" id="observacion_control_{{$horas->id}}"></textarea> @else {{$horas->observation_adm}} @endif</td>
        <td>@if($horas->verified == 0) <a href="#" class="btn btn-info" onclick="guardar_verificacion_clase({{$horas->id}})"><i class="icon-ok"></i></a> @else <i class="icon-ok"></i> @endif</td>

    </tr>
@endforeach
<script type="text/javascript">
	function guardar_verificacion_clase(id_control){
		
		var observacion_control = $("#observacion_control_"+id_control).val();

		
		if(observacion_control == ""){
			toastr.error('El campo Observacion no puede estar vacío', 'Error');
			return;
		}


		var html = $.ajax({
		    type: "GET",
		    url: "../pqrs/guardarverificacionclase",
		    data: {observacion_control:observacion_control, id_control:id_control},
		    async: false
		}).responseText;

		if (html == 1) {
			toastr.success('Se la verificación del control de clase correctamente', 'Verificación de control de clase');
		}else{
			toastr.error('No se ha podido enviar la verificación del control de clase', 'Error');
		}
	}
</script>