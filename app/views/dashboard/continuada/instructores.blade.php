
<div class="page-content" id="adminhelpdesk">
   	<div class="row">
		<div class="portlet">
		   	<div class="portlet-title">
		      	<div class="caption"><i class="icon-reorder"></i>Instructores</div>
		   	</div>
		   	<div class="portlet-body">
                <ul  class="nav nav-tabs">
                    <li class=""><a href="verinstructores" >Ver Instructores</a></li>
                    <li class="active"><a href="instructores">Ingresar Instructores</a></li>
			         
			         
                </ul>   		
            <form class="form-horizontal" id="form">
   		        <div class="form-body">
   		            <div class="row">                            
   		                <div class="col-md-12 note note-warning">
   		                    <div class="row">
   		                        <div class="col-md-2">
   		                        </div>
   		                        <div class="col-md-2">                
   		                            <div class="form-group">
   		                                <div class="col-md-12">
   		                                    <label class="control-label">Imagen</label>
   		                                    <div class="fileupload fileupload-new" data-provides="fileupload">
   		                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
   		                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
   		                                        </div>
   		                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
   		                                        <div>
   		                                            <span class="btn btn-default btn-file">
   		                                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
   		                                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
   		                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_ins"><i class="icon-trash"></i> Eliminar</a>
   		                                                <input type="file" class="default form-control" name="imagen_ins" id="imagen_ins" size="20">
   		                                            </span>
   		                                            <div class="help-block">
   		                                                Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
   		                                            </div>
   		                                        </div>
   		                                    </div>
   		                                </div>
   		                            </div>  
   		                        </div>
   		                        <div class="col-md-7">
   		                            
   		                            <h4 class="block">Datos Personales</h4>
   		                            <div class="col-md-3">
   		                                <div class="form-group">
   		                                    <div class="col-md-12">
                                                <div class="input-group">
   		                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
   		                                            <input id="nombre1" type="text" class="form-control" value="" placeholder="Primer Nombre">
   		                                        </div> 
   		                                    </div>
   		                                </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="nombre2" type="text" class="form-control" value="" placeholder="Segundo Nombre">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
   		                            <div class="col-md-3">
   		                                <div class="form-group" >
   		                                    <div class="col-md-12">
                                                <div class="input-group">
   		                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
   		                                            <input id="apellido1"  type="text" class="form-control" value="" placeholder="Primer Apellido">
   		                                        </div> 
   		                                    </div>
   		                                </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group" >
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="apellido2"  type="text" class="form-control" value="" placeholder="Segundo Apellido">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
   		                                <div class="form-group" >
   		                                    <div class="col-md-12">

   		                                        <div class="input-group">
   		                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
   		                                            <input id="tipo_identificacion"  type="text" class="form-control" value="" placeholder="Tipo Identificación">
   		                                        </div> 
   		                                    </div>
   		                                </div>
   		                            </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="n_documento"  type="text" class="form-control" value="" placeholder="N° Identificacion">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <select id="lugar_nacimiento"   class="form-control" value="" placeholder="Lugar De Nacimiento">
                                                        <option value="">Lugar De Nacimiento</option>
                                                    @foreach($paises as $pais)
                                                        <option value="{{$pais->id}}">{{$pais->cod_country}} {{$pais->country}}</option>
                                                    @endforeach
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="fecha_nacimiento"  type="text" class="form-control" value="" placeholder="Fecha De Nacimiento">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    
                                                    <select id="estado_civil"  type="text" class="form-control" value="" placeholder="Estado Civil">
                                                        <option value="">Estado Civil</option>
                                                        @foreach($estado_civil as $estado_civil)
                                                        <option value="{{$estado_civil->id}}">{{$estado_civil->marital_status}}</option>
                                                        @endforeach
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <select id="genero"  type="text" class="form-control" value="" placeholder="Genero">
                                                        <option value="">Género</option>
                                                        @foreach($generos as $generos)
                                                        <option value="{{$generos->id}}">{{$generos->gender}}</option>
                                                        @endforeach
                                                    </select>
                                                    
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    
                                                    <select id="grupo_sanguineo"  type="text" class="form-control" value="" placeholder="Grupo Sanguineo">
                                                        <option value="">Grupo Sanguineo</option>
                                                        @foreach($grupo_sanguineo as $grupo_sanguineo)
                                                        <option value="{{$grupo_sanguineo->id}}">{{$grupo_sanguineo->group}}</option>
                                                        @endforeach
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="direccion"  type="text" class="form-control" value="" placeholder="Direccion Residencia">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="telefono"  type="text" class="form-control" value="" placeholder="Telefono">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="celular"  type="text" class="form-control" value="" placeholder="Celular">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="email"  type="text" class="form-control" value="" placeholder="Email">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">

                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    
                                                    <select id="ciudad_residencia"  type="text" class="form-control" value="" placeholder="">
                                                        <option value="">Ciudad Residencia</option>
                                                        @foreach($ciudades as $ciudades)
                                                        <option value="{{$ciudades->id}}">{{$ciudades->name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group" >
                                            <div class="col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="numero_hijos"  type="text" class="form-control" value="" placeholder="Numero Hijos">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

   		                            
   		                        </div>
   		                    </div>
                        </div>
                        <div class="col-md-12 note note-info ">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="block">Información Academica</h4>
                                   
                            @foreach($tipo_estudios as $tipo)

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Titulo Obtenido</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="titulo_{{$tipo->id}}" type="text" class="form-control" value="" placeholder="{{$tipo->course}}">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Institución</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="institucion_{{$tipo->id}}" type="text" class="form-control" value="" placeholder="{{$tipo->course}}">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Fecha de finalizacion</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="fecha_finalizacion_{{$tipo->id}}" type="text" class="form-control fecha_required" value="" placeholder="{{$tipo->course}}">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    
                            @endforeach
                                </div>
                            </div> 
                        </div>       

   		            </div>
					<div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success" onclick="enviar_datos_instructor()">Guardar</a>
   		                </div>
   		            </div>                        
   		        </div>
   		    </form>
   		</div>
   		
  	</div>
</div>
<script type="text/javascript">
	function enviar_datos_instructor(){

            var imagen_ins              = $("#imagen_ins").val();
			var nombre1                 = $("#nombre1").val();
            var nombre2                 = $("#nombre2").val();
            var apellido1               = $("#apellido1").val();
            var apellido2               = $("#apellido2").val();
            var tipo_identificacion     = $("#tipo_identificacion").val();
            var n_documento             = $("#n_documento").val();
            var lugar_nacimiento        = $("#lugar_nacimiento").val();
            var fecha_nacimiento        = $("#fecha_nacimiento").val();
            var estado_civil            = $("#estado_civil").val();
            var genero                  = $("#genero").val();
            var grupo_sanguineo         = $("#grupo_sanguineo").val();
            var nacionalidad            = $("#nacionalidad").val();
            var direccion               = $("#direccion").val();
            var telefono                = $("#telefono").val();
            var celular                 = $("#celular").val();
            var email                   = $("#email").val();
            var ciudad_residencia       = $("#ciudad_residencia").val();
            var numero_hijos            = $("#numero_hijos").val();

            if(imagen_ins == ""){
                toastr.error('El campo imagen_ins no puede estar vacío', 'Error');
                $("#imagen_ins").focus();
                return;
            }
            if(nombre1 == ""){
                toastr.error('El campo nombre1 no puede estar vacío', 'Error');
                $("#nombre1").focus();
                return;
            }
            

            if(apellido1 == ""){
                toastr.error('El campo apellido1 no puede estar vacío', 'Error');
                $("#apellido1").focus();
                return;
            }
            

            if(tipo_identificacion == ""){
                toastr.error('El campo tipo_identificacion no puede estar vacío', 'Error');
                $("#tipo_identificacion").focus();
                return;
            }
            if(n_documento == ""){
                toastr.error('El campo n_documento no puede estar vacío', 'Error');
                $("#n_documento").focus();
                return;
            }
            if(lugar_nacimiento == ""){
                toastr.error('El campo lugar_nacimiento no puede estar vacío', 'Error');
                $("#lugar_nacimiento").focus();
                return;
            }
            if(fecha_nacimiento == ""){
                toastr.error('El campo fecha_nacimiento no puede estar vacío', 'Error');
                $("#fecha_nacimiento").focus();
                return;
            }
            if(estado_civil == ""){
                toastr.error('El campo estado_civil no puede estar vacío', 'Error');
                $("#estado_civil").focus();
                return;
            }
            if(genero == ""){
                toastr.error('El campo genero no puede estar vacío', 'Error');
                $("#genero").focus();
                return;
            }
            if(grupo_sanguineo == ""){
                toastr.error('El campo grupo_sanguineo no puede estar vacío', 'Error');
                $("#grupo_sanguineo").focus();
                return;
            }
            if(nacionalidad == ""){
                toastr.error('El campo nacionalidad no puede estar vacío', 'Error');
                $("#nacionalidad").focus();
                return;
            }
            if(direccion == ""){
                toastr.error('El campo direccion no puede estar vacío', 'Error');
                $("#direccion").focus();
                return;
            }
            if(telefono == ""){
                toastr.error('El campo telefono no puede estar vacío', 'Error');
                $("#telefono").focus();
                return;
            }
            if(celular == ""){
                toastr.error('El campo celular no puede estar vacío', 'Error');
                $("#celular").focus();
                return;
            }
            if(email == ""){
                toastr.error('El campo email no puede estar vacío', 'Error');
                $("#email").focus();
                return;
            }
            if(ciudad_residencia == ""){
                toastr.error('El campo ciudad_residencia no puede estar vacío', 'Error');
                $("#ciudad_residencia").focus();
                return;
            }
            if(numero_hijos == ""){
                toastr.error('El campo numero_hijos no puede estar vacío', 'Error');
                $("#numero_hijos").focus();
                return;
            }



            var ids             = new Array();
            var titulos         = new Array();
            var instituciones   = new Array();
            var fechas          = new Array();
            var i = 0;
            var val = 0;
            

                @foreach($tipo_estudios as $tipo)

                    if($("#titulo_{{$tipo->id}}").val() != "" &&  $("#institucion_{{$tipo->id}}").val() != "" && $("#fecha_finalizacion_{{$tipo->id}}").val() != "") {  
                        
                        ids[i]              = {{$tipo->id}};
                        
                        titulos[i]          = $("#titulo_{{$tipo->id}}").val()
                        instituciones[i]    = $("#institucion_{{$tipo->id}}").val()
                        fechas[i]           = $("#fecha_finalizacion_{{$tipo->id}}").val()
                        
                        val = 1;
                        


                    i++;
                    }

                @endforeach

                if (val == 0) {
                    toastr.error('Debe Ingresar Información Academica.', 'Error'); 
                    return;
                }
                


			$("#imagen_ins").upload('ingresarnuevoinstructor', 
			{
                nombre1:nombre1,
                nombre2:nombre2,
                apellido1:apellido1,
                apellido2:apellido2,
                tipo_identificacion:tipo_identificacion,
                n_documento:n_documento,
                lugar_nacimiento:lugar_nacimiento,
                fecha_nacimiento:fecha_nacimiento,
                estado_civil:estado_civil,
                genero:genero,
                grupo_sanguineo:grupo_sanguineo,
                nacionalidad:nacionalidad,
                direccion:direccion,
                telefono:telefono,
                celular:celular,
                email:email,
                ciudad_residencia:ciudad_residencia,
                numero_hijos:numero_hijos,

                ids:ids,
                titulos:titulos,
                instituciones:instituciones,
                fechas:fechas
			    
			    
			    
			},
			function(respuesta){
			    
			    if (respuesta === 1) {
			        toastr.success('Se ingreso correctamente el nuevo Instructor', 'Nuevo Instructor');
                    $("#imagen_ins").val('');
                    $("#nombre1").val('');
                    $("#nombre2").val('');
                    $("#apellido1").val('');
                    $("#apellido2").val('');
                    $("#tipo_identificacion").val('');
                    $("#n_documento").val('');
                    $("#lugar_nacimiento").val('');
                    $("#fecha_nacimiento").val('');
                    $("#estado_civil").val('');
                    $("#genero").val('');
                    $("#grupo_sanguineo").val('');
                    $("#nacionalidad").val('');
                    $("#direccion").val('');
                    $("#telefono").val('');
                    $("#celular").val('');
                    $("#email").val('');
                    $("#ciudad_residencia").val('');
                    $("#numero_hijos").val('');
			    } else {
			        toastr.error('No ha sido posible ingresar el nuevo Instructor.', 'Error');                              
			        
			    }
			});

	}
</script>