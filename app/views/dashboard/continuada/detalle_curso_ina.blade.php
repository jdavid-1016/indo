<div class="modal-dialog modal-wide">
   <div class="modal-content ">
   <form class="form" action="#" >
      <input type="hidden" value="" id="prospect_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle Curso Inactivo</h4>
      </div>
      <div class="modal-body form-body">
                   
          <div class="row">            
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Nombre</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="name_curso" id="name_curso" value="{{$curso->name_curso}}">
                  </div>
               </div>
            </div>
              <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Categoria</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select type="text" class="form-control" id="courses_categories_id" required="">
                        @foreach($categories as $cat)
                        <option <?php if($curso->courses_categories_id==$cat->id){ echo "selected"; } ?> value="{{ $cat->id }}">{{ $cat->category }}</option>
                        @endforeach
                    </select>
                  </div>
               </div>
            </div>
         </div>
          
          <div class="row">
              <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Intensidad Horaria</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="time_intensity" id="time_intensity" value="{{$curso->time_intensity}}">
                  </div>
               </div>
            </div>
              <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Horario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="schedule" id="schedule" value="{{$curso->schedule}}">
                  </div>
               </div>
              </div>                        
         </div>
          <div class="row">
              <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Abierto</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select type="text" class="form-control" id="open" required="">                                
                                <option <?php if($curso->open==1){ echo "selected"; } ?> value="1">Verdadero</option>
                                <option <?php if($curso->open==0){ echo "selected"; }?> value="0">Falso</option>
                     </select>
                  </div>
               </div>
            </div>
              
              <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Inspector</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="inspector" id="inspector" value="{{$curso->inspector}}">
                  </div> 
               </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Lugar instrucción teórica</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="inst_teorica" id="inst_teorica" value="{{$curso->theorist_places}}">
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Fechas</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="fecha_teorica" id="fecha_teorica" value="{{$curso->theorist_dates}}">
                  </div> 
               </div>
            </div>
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Lugar instrucción practica</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="inst_practica" id="inst_practica" value="{{$curso->practice_places}}">
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Fechas</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " placeholder="" data-required="true" name="fecha_practica" id="fecha_practica" value="{{$curso->practice_dates}}">
                  </div> 
               </div>
            </div>
         </div>
          
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Fecha de inicio</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control fecha_required_next" placeholder="" data-required="true" name="start_date" id="start_date" value="{{$curso->start_date}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Fecha de finalizacion</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control fecha_required_next" placeholder="" data-required="true" name="end_date" id="end_date" value="{{$curso->end_date}}">
                  </div> 
               </div>
            </div>
         </div>

         <hr>
                  
         <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_estudiantes">
                              Estudiantes
                              </a>
                           </h4>
                        </div>
                    <div id="collapse_estudiantes" class="panel-collapse collapse" style="height: auto;">
                        <table class="table top-blue" data-target="">
                            <thead>
                                <tr>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Ultima Actualizacion</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody >
                                @foreach($estudiantes as $estudiante)
                                    <tr style="text-align:center;" id="table-est-{{$estudiante->id}}">
                                        <td>{{$estudiante->name}} {{$estudiante->name2}}</td>
                                        <td>{{$estudiante->last_name}} {{$estudiante->last_name2}}</td>
                                        <td>{{$estudiante->updated_at}}</td>
                                        <td>
                                            <!-- {{$estudiante->updated_at}} -->
                                            <a class=" btn btn-default" style="" data-target="#ajax2" id="{{$estudiante->id}}" data-toggle="modal" onclick='cargarDetalleAlumno($(this).attr("id"));return false;'><i class="icon-edit"></i> {{$estudiante->id}}</a>
                                            <a href="#" class="btn btn-danger" onclick="eliminar_materia({{$estudiante->id}})"><i class="icon-remove"></i></a>
                                        </td>
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                        </div>
             </div>
        
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_materias">
                        Instructores
                        </a>
                    </h4>
                </div>
                <div id="collapse_materias" class="panel-collapse collapse" style="height: auto;">
                    <table class="table top-blue" data-target="">
                        <thead>
                            <tr>
                                <th>Instructor</th>
                                <th>Foto</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody id="tabla_materias_cursos">
                            @include('dashboard.continuada.body_materias')
                        </tbody>
                    </table>
                </div>
            </div>         
         
         <hr>

      </div>
      <div class="modal-footer">

         <button type="button" class="btn btn-success" onclick="duplicarCurso({{$curso->id}})">Crear Curso</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>
   </div>
</div>
<script>
    $( ".fecha_required_next").datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
 </script>
<script type="text/javascript">
    function recargarBody(id){
        var nombre = "1";
        var html = $.ajax({
                type: "GET",
                url: "detallecursoina",
                data: {id:id, nombre:nombre},
                async: false
            }).responseText;
            
            $('#tabla_materias_cursos').html(html);
            
    }
    
    function cargarDetalleAlumno(id){
        var parametros = {
            "id": id
        };
        $.ajax({
            data: parametros,
            url:  'detallealumno',
            type: 'get',

            success: function(response){
                $("#ajax2").html(response);
            }
        });
    }
    //funcion para guardar un nuevo curso
function asignar_materias(id) {
    var validacion = 0;
    var materia = $('#materia').val();
    var instructor = $('#instructor').val();
    
    if(materia !="" && instructor !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
                return;
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarmateria",
                data: {id:id, materia:materia, instructor:instructor },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    recargarBody(id);
                
                    $('#materia').val('');

                toastr.success('Se ha asignado la materia correctamente', 'Nueva Materia');

            } else {
                toastr.error('No ha sido posible guardar la materia', 'Error');
            }
        }
}

//funcion para guardar un nuevo curso
function editar_curso(id) {
    var validacion = 0;    
    var name_curso = $('#name_curso').val();
    var courses_categories_id = $('#courses_categories_id').val();
    var time_intensity = $('#time_intensity').val();
    var schedule = $('#schedule').val();
    var open = $('#open').val();
    var inspector = $('#inspector').val();    
    var inst_teorica = $('#inst_teorica').val();
    var fecha_teorica = $('#fecha_teorica').val();
    var inst_practica = $('#inst_practica').val();
    var fecha_practica = $('#fecha_practica').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    
    if(name_curso !="" && start_date !="" && end_date !="" && time_intensity !="" && schedule !="" && schedule !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "editarcurso",
                data: {id:id, name_curso:name_curso, courses_categories_id:courses_categories_id, time_intensity:time_intensity, schedule:schedule, 
                    open:open, inspector:inspector, inst_teorica:inst_teorica, fecha_teorica:fecha_teorica, inst_practica:inst_practica, fecha_practica:fecha_practica, 
                    start_date:start_date, end_date:end_date },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('El nuevo curso se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar el Curso', 'Error');
            }
        }
}
    
</script>