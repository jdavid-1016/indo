<div class="modal-dialog modal-wide">
    <div class="modal-content">
    <form class="form" action="#" >
        <input type="hidden" value="" id="instructors_id">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Instructor {{$instructor[0]->Users->name}} {{$instructor[0]->Users->name2}} {{$instructor[0]->Users->last_name}} {{$instructor[0]->Users->last_name2}}</h4>
        </div>
        <div class="modal-body form-body">
            <ul  class="nav nav-tabs">
                    <li class="active"><a href="#datos_personales" data-toggle="tab">Datos personales</a></li>
                    <li class=""><a href="#info_academica" data-toggle="tab">Informacion Academica</a></li>
                    <li class=""><a href="#licencias"  data-toggle="tab">Licencias</a></li>
                    <li class=""><a href="#cursos_dictados"  data-toggle="tab">Cursos dictados</a></li>
                    <li class=""><a href="#cambiarpass" data-toggle="tab">Cambiar Contraseña</a></li>
                </ul>
            <div class="tab-content">      
                <div class="tab-pane fade active in" id="datos_personales">
                    <form class="form-horizontal" id="form">
           		        <div class="form-body">
           		            <div class="row">
           		                <div class="col-md-12 note note-warning">
           		                    <div class="row">
                                        
                                        <div class="col-md-2">                
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <label class="control-label">Imagen</label>
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="../{{$instructor[0]->Users->img}}" alt="">
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_ins"><i class="icon-trash"></i> Eliminar</a>
                                                                <input type="file" class="default form-control" name="imagen_ins" id="imagen_ins" size="20">
                                                            </span>
                                                            <div class="help-block">
                                                                Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>  
                                        </div>
                                        <div class="col-md-10">
                                            
                                            <h4 class="block">Datos Personales</h4>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="nombre1" type="text" class="form-control" value="{{$instructor[0]->Users->name}}" placeholder="Primer Nombre">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="nombre2" type="text" class="form-control" value="{{$instructor[0]->Users->name2}}" placeholder="Segundo Nombre">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group" >
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="apellido1"  type="text" class="form-control" value="{{$instructor[0]->Users->last_name}}" placeholder="Primer Apellido">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group" >
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="apellido2"  type="text" class="form-control" value="{{$instructor[0]->Users->last_name2}}" placeholder="Segundo Apellido">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="tipo_identificacion"  type="text" class="form-control" value="{{$instructor[0]->Users->type_document}}" placeholder="Tipo Identificación">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="n_documento"  type="text" class="form-control" value="{{$instructor[0]->Users->document}}" placeholder="N° Identificacion">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <select id="lugar_nacimiento"   class="form-control" value="" placeholder="Lugar De Nacimiento">
                                                                <option value="">Lugar De Nacimiento</option>
                                                                @foreach($paises as $pais)
                                                                <option value="{{$pais->id}}" @if($instructor[0]->Users->countries_id == $pais->id) selected="" @endif>{{$pais->country}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="fecha_nacimiento"  type="text" class="form-control" value="{{$instructor[0]->Users->birthday}}" placeholder="Fecha De Nacimiento">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="estado_civil"  type="text" class="form-control" value="" placeholder="Estado Civil">
                                                                <option value="">Estado Civil</option>
                                                                @foreach($estado_civil as $estado_civil)
                                                                <option value="{{$estado_civil->id}}" @if($instructor[0]->Users->general_marital_status_id == $estado_civil->id) selected="" @endif>{{$estado_civil->marital_status}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <select id="genero"  type="text" class="form-control" value="" placeholder="Genero">
                                                                <option value="">Género</option>
                                                                @foreach($generos as $generos)
                                                                <option value="{{$generos->id}}" @if($instructor[0]->Users->general_genres_id == $generos->id) selected="" @endif>{{$generos->gender}}</option>
                                                                @endforeach
                                                            </select>
                                                            
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="grupo_sanguineo"  type="text" class="form-control" value="" placeholder="Grupo Sanguineo">
                                                                <option value="">Grupo Sanguineo</option>
                                                                @foreach($grupo_sanguineo as $grupo_sanguineo)
                                                                <option value="{{$grupo_sanguineo->id}}" @if($instructor[0]->Users->general_blood_group_id == $grupo_sanguineo->id) selected="" @endif>{{$grupo_sanguineo->group}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="direccion"  type="text" class="form-control" value="{{$instructor[0]->Users->address}}" placeholder="Direccion Residencia">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="telefono"  type="text" class="form-control" value="{{$instructor[0]->Users->phone}}" placeholder="Telefono">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="celular"  type="text" class="form-control" value="{{$instructor[0]->Users->cell_phone}}" placeholder="Celular">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="email"  type="text" class="form-control" value="{{$instructor[0]->Users->email_institutional}}" placeholder="Email">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">

                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="ciudad_residencia"  type="text" class="form-control" value="" placeholder="">
                                                                <option value="">Ciudad Residencia</option>
                                                                @foreach($ciudades as $ciudades)
                                                                <option value="{{$ciudades->id}}" @if($instructor[0]->Users->city_id == $ciudades->id) selected="" @endif>{{$ciudades->name}}</option>
                                                                @endforeach
                                                            </select>

                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" >
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="numero_hijos"  type="text" class="form-control" value="{{$instructor[0]->number_of_children}}" placeholder="Numero Hijos">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>

                                       </div>
                                    </div>    

           		                </div>
                            </div>
                        </div>
                    </form>
                    <div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success" onclick="EditarInstructor({{$instructor[0]->Users->id}},{{$instructor[0]->id}})">Guardar</a>
                            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
   		                </div>
   		            </div>
                </div>
                
                <div class="tab-pane fade" id="info_academica">
                        <div class="col-md-12 note note-info ">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="block">Información Academica</h4>
                                   
                            @foreach($estudios as $estudio)

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Titulo Obtenido</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="titulo_{{$estudio->id}}" type="text" class="form-control" value="{{$estudio->obtained_title}}">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Institución</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="institucion_{{$estudio->id}}" type="text" class="form-control" value="{{$estudio->institution}}">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Fecha de finalizacion</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="fecha_finalizacion_{{$estudio->id}}" type="text" class="form-control fecha_required" value="{{$estudio->end_date}}">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    
                            @endforeach
                                </div>
                            </div> 
                        </div>
                        <div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success" onclick="editar_estudios_instructor()">Guardar</a>
                                    <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
   		                </div>
   		            </div>

   		            </div>
                
                <div class="tab-pane fade" id="licencias">
                        <div class="col-md-12 note note-warning ">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="block">Licencias</h4>
                                   
                            @foreach($licencias as $licencia)

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Tipo Licencia</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <select id="tipo_licencia_{{$licencia->id}}" type="text" class="form-control" >

                                                    @foreach($tipos_licencias as $tipo)
                                                        @if($tipo->id == $licencia->courses_technical_licenses_id)
                                                            <option value="{{$tipo->id}}" selected="">{{$tipo->programs}}</option>
                                                        @else
                                                            <option value="{{$tipo->id}}" >{{$tipo->programs}}</option>
                                                        @endif

                                                    @endforeach
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">N° Licencia</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="n_licencia_{{$licencia->id}}" type="text" class="form-control" value="{{$licencia->number_licence}}" placeholder="">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Fecha de Expiración</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="fecha_expiracion_{{$licencia->id}}" type="text" class="form-control fecha_required" value="{{$licencia->expiration_date}}" placeholder="">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>

                                    
                            @endforeach
                                </div>
                            </div> 
                        </div>
                                                 <!--  -->
                    
                    <div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success" onclick="editar_licencia_instructor()">Guardar</a>
                                    <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
   		                </div>
   		            </div>

   		       </div>
                
                <div class="tab-pane fade" id="cursos_dictados">
                    <div class="col-md-12 note note-info ">
                        <div class="row">
                            <div class="col-md-12">
                                <h4 class="block">Cursos Dictados</h4>

                                <div class="col-md-12">
                                    <table class="table top-blue">
                                        <tr>
                                            <th>Ocsa</th>
                                            <th>Nombre</th>
                                            <th>Fecha</th>
                                            <th>Calificación</th>
                                        </tr>
                                        @foreach($cursos as $curso)
                                        <tr>
                                            <td>{{$curso->ocsa}}</td>
                                            <td>{{$curso->name_curso}}</td>
                                            <td>{{$curso->start_date}}</td>
                                            <td></td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div> 
                    </div>
                    <div class="form-actions fluid">
                        <div class="col-md-1 col-md-11">
                            <a class="btn btn-success" onclick="CambiarPassIns({{$instructor[0]->Users->id}})">Guardar</a>
                            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                        </div>
                    </div>

                </div>
                
                <div class="tab-pane fade" id="cambiarpass">
                        <div class="col-md-12 note note-info ">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="block">Cambiar contraseña</h4>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Nueva contraseña</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="nueva" type="password" class="form-control" value="">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Re-ingresar nueva contraseña</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="renueva" type="password" class="form-control" value="">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                                                
                                </div>
                            </div> 
                        </div>
                        <div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success" onclick="CambiarPassIns({{$instructor[0]->Users->id}})">Guardar</a>
                                    <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
   		                </div>
   		            </div>

   		        </div>
                    
                        
   		        </div>
                   </div>
        </form>
                </div>
                                
        </div>  
<script>
    $( ".fecha_required_next" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
</script>   
<script type="text/javascript">

function editar_licencia_instructor(){


    var id_asignacion   = new Array();
    var valor_tipo      = new Array();
    var valor_numero    = new Array();
    var valor_fecha     = new Array();
    var i = 0;

    @foreach($licencias as $licencias)

        id_asignacion[i]  = {{$licencias->id}};
        valor_tipo[i]     = $("#tipo_licencia_{{$licencias->id}}").val();
        valor_numero[i]   = $("#n_licencia_{{$licencias->id}}").val();
        valor_fecha[i]    = $("#fecha_expiracion_{{$licencias->id}}").val();
        i++;
    @endforeach

    var html = $.ajax({
        type: "POST",
        url: "editarlicenciasinstructor",
        data: {id_asignacion:id_asignacion, valor_tipo:valor_tipo, valor_numero:valor_numero, valor_fecha:valor_fecha},
        async: false
    }).responseText;

    if (html == 1) {
        toastr.success('Se Editaron correctamente las Licencias', 'Edición de Licencias');
    }else{
        toastr.error('No ha sido posible editar las licencias', 'Error');
    }
}
function editar_estudios_instructor(){


    var id_asignacion       = new Array();
    var titulo              = new Array();
    var institucion         = new Array();
    var fecha_finalizacion  = new Array();
    var i = 0;

    @foreach($estudios as $estudio)

        id_asignacion[i]        = {{$estudio->id}};
        titulo[i]               = $("#titulo_{{$estudio->id}}").val();
        institucion[i]          = $("#institucion_{{$estudio->id}}").val();
        fecha_finalizacion[i]   = $("#fecha_finalizacion_{{$estudio->id}}").val();
        i++;
    @endforeach

    var html = $.ajax({
        type: "POST",
        url: "editarestudiosinstructor",
        data: {id_asignacion:id_asignacion, titulo:titulo, institucion:institucion, fecha_finalizacion:fecha_finalizacion},
        async: false
    }).responseText;

    if (html == 1) {
        toastr.success('Se Edito correctamente los Estudios del instructor', 'Edición de Estudios del instructor');
    }else{
        toastr.error('No ha sido posible editar las Estudios del instructor', 'Error');
    }
}
</script>