
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Subir Notas</div>
            </div>
            <div class="portlet-body">
                <ul  class="nav nav-tabs">
                    <li class="active"><a href="subirnotas" >Subir Notas</a></li>
                    <li class=""><a href="subirfallas">Subir Fallas</a></li>
                    <li class=""><a href="../pqrs/control">Subir Control Clase</a></li>
                     
                     
                </ul> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-6">
                            <label class="control-label">Seleccione Un Curso</label>
                                <select  class="form-control input-lg  select2me" data-placeholder="Curso..." id="id_curso" onchange="cargar_alumnos_del_curso()">
                                    <option value=""></option>
                                    @foreach($cursos as $curso)
                                    <option value="{{$curso->Courses->id}}">{{$curso->Courses->name_curso}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="row" id="">
                    <div class="col-md-6" id="tabla_alumnos_curso">
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function cargar_alumnos_del_curso(){
        var id_curso = $("#id_curso").val();
        var html = $.ajax({
            type: "GET",
            url: "cargaralumnosnotas",
            data: {id_curso:id_curso},
            async: false
        }).responseText;
            
        $('#tabla_alumnos_curso').html(html);
            
    }
</script>