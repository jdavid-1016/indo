
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Subir Controles de Clase</div>
            </div>
            <div class="portlet-body">
                <ul  class="nav nav-tabs">
                    <li class=""><a href="subirnotas" >Subir Notas</a></li>
                    <li class=""><a href="subirfallas">Subir Fallas</a></li>
                    <li class="active"><a href="../pqrs/control">Subir Control Clase</a></li>
                    
                     
                     
                </ul> 
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-6">
                            <label class="control-label">Seleccione Un Curso</label>
                                <select  class="form-control input-lg  select2me" data-placeholder="Curso..." id="id_curso" onchange="cargar_horas_curso()">
                                    <option value=""></option>
                                    @foreach($cursos as $curso)
                                    <option value="{{$curso->id}}">{{$curso->Courses->name_curso}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                        </div>
                        
                    </div>
                    
                </div>
                <div class="row" id="">
                    <div class="col-md-12" id="tabla_alumnos_curso">
                        <table class="table top-blue" data-target="">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>INSTRUCTOR</th>
                                    <th>FECHA</th>
                                    <th>HORA</th>
                                    <th>SALON</th>
                                    <th>TEMA</th>
                                    <th>OBSERVACIÓN</th>
                                    <th>DICTADA</th>
                                    <th>VERIFICADA</th>
                                    <th>OBSERVACIÓN</th>
                                </tr>
                            </thead>
                            <tbody id="tabla_horas_instructor">
                                
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function cargar_horas_curso(){
        var id_curso = $("#id_curso").val();
        var fecha = $("#fecha").val();
        // if (fecha == "") {
        //     $('#tabla_alumnos_curso').html("");
        //     return;
        // }

        var html = $.ajax({
            type: "GET",
            url: "cargarhorascurso",
            data: {id_curso:id_curso, fecha:fecha},
            async: false
        }).responseText;
            
        $('#tabla_horas_instructor').html(html);
            
    }
</script>