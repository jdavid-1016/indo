<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Formulario de Salida</div>
            </div>
            <div class="portlet-body">
                <ul  class="nav nav-tabs">
                    <li class=""><a href="formentrada">Formulario de Entrada</a></li>
                    <li class="active"><a href="formsalida">Formulario de Salida</a></li>
                </ul> 
                <div class="row">
                    <div class="col-md-6">
                        
                        <h3>Encuesta de satisfacción educación continuada</h3>
                        <p>
                            La corporación educativa indoamericana es consciente de la importancia que tiene el retroalimentar los procesos y tomar acciones enfocadas hacia la mejora continua. Lo invitamos a evaluar los siguientes aspectos presentados durante el entrenamiento de .NOMBRE DEL CURSO. En una escala de 1 a 5 siendo 1 malo y 5 excelente, evalué el nivel de desempeño según los ítems establecidos.
                        </p>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-12">
                        <p>
                            1. Desempeño de instructores
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Intructor</th>
                                    <th>Conocimiento</th>
                                    <th>Metodologia</th>
                                    <th>Dominio de Grupo</th>
                                    <th>Capacidad de Comunicación</th>
                                    <th>Solución de inquietudes</th>
                                    <th>Nivel de Actualización</th>
                                    <th>Material de Consulta</th>

                                </tr>
                                <tr>
                                    <td></td>
                                    <td><input type="text" class="form-control"></td>
                                    <td><input type="text" class="form-control"></td>
                                    <td><input type="text" class="form-control"></td>
                                    <td><input type="text" class="form-control"></td>
                                    <td><input type="text" class="form-control"></td>
                                    <td><input type="text" class="form-control"></td>
                                    <td><input type="text" class="form-control"></td>
                                    
                                </tr>

                            </table>
                        </p> 
                        <p>
                        <p>
                            2. <strong>Logística del evento:</strong> Evalué de 1 a 5 el apoyo de los funcionarios de la institución y la coordinación general del curso.
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Ítem </th>
                                    <th>Nota </th>
                                    <th>Observaciones</th>

                                </tr>
                                <tr>
                                    <td>Apoyo por parte de los funcionarios de la institución</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>

                                </tr>
                                <tr>
                                    <td>Manejo de tiempos</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Ayudas audiovisuales</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Material adecuado</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>
                                </tr>
                                <tr>
                                    <td>Coordinación de tiempos</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>
                                </tr>

                            </table>
                        </p>
                        <p>
                            3. <strong>Instalaciones:</strong> Evalué el servicio prestado en las instalación es de la institución 
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Ítem </th>
                                    <th>Nota </th>
                                    <th>Observaciones</th>

                                </tr>
                                <tr>
                                    <td>Instalaciones adecuadas</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>

                                </tr>
                                <tr>
                                    <td>Atención al personal</td>
                                    <td><input type="text" class="form-control" id="" name="" value=""></td>
                                    <td><textarea class="form-control"></textarea></td>

                                </tr>

                            </table>
                        </p>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-6">
                        <p>
                            4. ¿Sobre qué tema le parece que se debería profundizar y que otro tipo de formación le gustaría tomar?
                            <textarea class="form-control"></textarea>
                        </p>
                        <p> 
                            5 ¿Que temas le parecieron más relevantes en la formación  ?
                            <textarea class="form-control"></textarea>
                        </p>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>