
<div class="page-content" id="adminhelpdesk">
   	<div class="row">
		<div class="portlet">
		   	<div class="portlet-title">
		      	<div class="caption"><i class="icon-reorder"></i>Alumnos</div>
		   	</div>
		   	<div class="portlet-body">
			      <ul  class="nav nav-tabs">
			         <li class="active"><a href="alumnos">Ingresar Alumnos</a></li>
			         <li class=""><a href="veralumnos" >Ver Alumnos</a></li>
			         
			         
			         
			      </ul>   		

   		    <form class="form-horizontal" id="form">
   		        <div class="form-body">
   		            <div class="row">                            
   		                <div class="col-md-12">
                             <div class="row">
                                 <div class="col-md-12">
                                     <ul class="page-breadcrumb breadcrumb">
                                         <h4></h4>
                                     </ul>
                                 </div>
                             </div>

                             <form class="form-horizontal" id="form">
                                 <div class="form-body">
                                     <div class="row">                            
                                         <div class="col-md-12 note note-warning">
                                             <div class="row">
                                                 <div class="col-md-2">
                                                 </div>
                                                 <div class="col-md-2">                
                                                     <div class="form-group">
                                                         <div class="col-md-12">
                                                             <label class="control-label">Imagen</label>
                                                             <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                 <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                     <img src="../assets/img/AAAAAA&text=no+image.gif" alt="">
                                                                 </div>
                                                                 <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                 <div>
                                                                     <span class="btn btn-default btn-file">
                                                                         <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                         <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                         <input type="file" class="default form-control" name="imagen_alumno" id="imagen_alumno" size="20">
                                                                     </span>
                                                                     <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_imagen_alumno"><i class="icon-trash"></i> Eliminar</a>
                                                                     <div class="help-block">
                                                                         Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </div>  
                                                 </div>
                                                 <div class="col-md-8">
                                                 
                                                     <div class="col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="nombre1" name="name" type="text" class="form-control campos_cpu" value="" placeholder="Primer Nombre">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>

                                                     <div class="col-md-6">
                                                         <div class="form-group">
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="nombre2" name="middle_name" type="text" class="form-control campos_cpu" value="" placeholder="Segundo Nombre">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="segundo_nombre1" name="last_name" type="text" class="form-control campos_cpu" value="<?php if (isset($name[2])) { ?>{{$name[2]}} <?php } ?>" placeholder="Primer Apellido">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="segundo_nombre2" name="last_name2" type="text" class="form-control campos_cpu" value="<?php if (isset($name[3])) { ?>{{$name[3]}} <?php } ?>" placeholder="Segundo Apellido">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <select name="genero" id="genero" class="form-control">
                                                                         <option value="">Genero</option>
                                                                         @foreach($genero as $gen)
                                                                         <option value="{{ $gen->id }}">{{ $gen->aspirants_gender }}</option>
                                                                         @endforeach
                                                                     </select>
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <select name="estado_civil" id="estado_civil" class="form-control">
                                                                         <option value="">Estado Civil</option>
                                                                         @foreach($estadocivil as $estcivil)
                                                                         <option value="{{ $estcivil->id }}">{{ $estcivil->marital_status }}</option>
                                                                         @endforeach
                                                                     </select>
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="f_nacimiento" name="cpu_serial" type="text" class="form-control fecha_nuevoalum" placeholder="Fecha Nacimiento">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <select name="nacionalidad" id="nacionalidad" class="form-control">
                                                                         <option value="">Nacionalidad</option>
                                                                         @foreach($nacionalidades as $nacionalidad)
                                                                         <option value="{{ $nacionalidad->id }}">{{ $nacionalidad->country }}</option>
                                                                         @endforeach
                                                                     </select>
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="lugar_nacimiento" name="lugar_nacimiento" type="text" class="form-control campos_cpu" placeholder="Lugar de Nacimiento">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="profesion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Profesión">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <select name="curso_realizar" id="curso_realizar" class="form-control">
                                                                         <option value="">Curso a Realizar</option>
                                                                         @foreach($programas as $programas)
                                                                         <option value="{{ $programas->id }}">{{ $programas->name_curso }}</option>
                                                                         @endforeach
                                                                     </select>
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                                     <div class="col-md-6">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="fecha_inscipcion" name="profesion" type="text" class="form-control fecha_nuevoalum" placeholder="Fecha Inscripción">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>


                                                 
                                                 </div>
                                                    


                                             </div>  
                                             <div class="row">
                                                 <div class="col-md-8 pull-right">
                                                         <div class="form-group" >
                                                             <div class="col-md-12">

                                                                 <div class="input-group">
                                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                     <input id="email" name="profesion" type="text" class="form-control" placeholder="email">
                                                                 </div> 
                                                             </div>
                                                         </div>
                                                     </div>
                                             </div>
                                         </div>

                                     </div>

                                     <div class="row">

                                         <div class="col-md-6 note note-info">
                                             <h4 class="block">Datos Personales</h4>
                                             
                                             <div class="col-md-12">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Tipo de documento</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <select name="tipo_documento" id="tipo_documento" class="form-control">
                                                                 <option value="">Tipo de documento</option>
                                                                 @foreach($typedoc as $type)
                                                                 <option value="{{ $type->id }}">{{ $type->type }}</option>
                                                                 @endforeach
                                                             </select>
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">N° Doc Identidad</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="n_documento" name="document" type="text" class="form-control campos_cpu" value="" placeholder="N° Doc Identidad">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Expedido en</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="lugar_expedicion" name="document" type="text" class="form-control campos_cpu" value="" placeholder="Expedido en">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Dirección de Residencia</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="direccion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Dirección de Residencia">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Teléfonos</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfonos">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Empresa donde Trabaja</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="empresa" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Empresa donde Trabaja">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Teléfonos</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="empresa_telefono" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Teléfonos">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Cargo Actual</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="cargo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Cargo Actual">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             <div class="col-md-6">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                        <label class="control-label">Tiempo de Experiencia</label>
                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <input id="tiempo_experiencia" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Tiempo de Experiencia">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             



                                         </div>
                                         <div class="col-md-6 note note-info">
                                             <h4 class="block">Estudios Realizados</h4>
                                             <div class="col-md-12">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">

                                                         <div class="input-group">
                                                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                             <select name="estudios" id="estudios" class="form-control">
                                                                 <option value="">Bachillerato</option>
                                                                 <option value="1">Bachillerato</option>
                                                                 <option value="2">Academico</option>
                                                                 <option value="3">Normalista</option>
                                                                 <option value="4">Comercial</option>
                                                                 <option value="5">Técnico</option>
                                                             </select>
                                                             
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>

                                             @foreach($estudios as $estudio)
                                             <div class="row">
                                                <div class="col-md-2">
                                                    <label class="control-label">{{$estudio->course}}</label>
                                                    
                                                </div>
                                                 <div class="col-md-4">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="tecnico_institucion_{{$estudio->id}}" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Institución">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">

                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="tecnico_titulo_{{$estudio->id}}" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Título">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div>
                                            @endforeach
                                            <!-- <div class="row">
                                                <div class="col-md-2">
                                                    
                                                         <label class="control-label">Tecnológica</label>
                                                </div>
                                                 <div class="col-md-4">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">
                                                            
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="tecnologico_institucion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Institución">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                            
                                                             <div class="input-group">
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="tecnologico_titulo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Título">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    
                                                         <label class="control-label">Profesional</label>
                                                </div>
                                                 <div class="col-md-4">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">
                                                            
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="profesional_institucion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Institución">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                            
                                                             <div class="input-group">
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="profesional_titulo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Título">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    
                                                         <label class="control-label">Postgrado</label>
                                                </div>
                                                 <div class="col-md-4">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">
                                                            
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="postgrado_institucion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Institución">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                            
                                                             <div class="input-group">
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="profesional_titulo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Título">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    
                                                         <label class="control-label">Especialización</label>
                                                </div>
                                                 <div class="col-md-4">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">
                                                            
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="especializacion_institucion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Institución">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                            
                                                             <div class="input-group">
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="especializacion_titulo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Título">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    
                                                         <label class="control-label">Seminarios</label>
                                                </div>
                                                 <div class="col-md-4">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                             <div class="input-group">
                                                            
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="seminarios_institucion" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Institución">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                                 <div class="col-md-6">
                                                     <div class="form-group" >
                                                         <div class="col-md-12">
                                                            
                                                             <div class="input-group">
                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                 <input id="seminarios_titulo" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Título">
                                                             </div> 
                                                         </div>
                                                     </div>
                                                 </div>
                                            </div> -->
                                             
                                             
                                             <h3> &nbsp &nbsp</h3>





                                         </div>
                                     </div>
                                     <div class="row">


                                         <div class="col-md-12 note note-warning">
                                             <h4 class="block">Licencias Técnicas</h4>
                                         <div class="row">
                                            @foreach($licencias as $licencia)
                                             <div class="col-md-2">
                                                 <div class="form-group" >
                                                     <div class="col-md-12">
                                                     <label class="control-label">{{$licencia->programs}}</label>
                                                         <div class="input-group">
                                                             <input id="tla_numero_{{$licencia->id}}" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="Número">
                                                             <input id="tla_vencimiento_{{$licencia->id}}" name="cpu_serial" type="text" class="form-control fecha_nuevoalum" placeholder="F vencimiento">
                                                         </div> 
                                                     </div>
                                                 </div>
                                             </div>
                                             @endforeach
                                             
                                            
                                         </div>
                                         
                                     </div>

                                                            
                                 </div>
                             </form>

                         </div>

   		            </div>
					<div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success pull-right" onclick="enviar_datos_instructor()">Guardar</a>
   		                </div>
   		            </div>                        
   		        </div>
   		    </form>
   		</div>
   		
  	</div>
</div>
<script type="text/javascript">
	function enviar_datos_instructor(){
			var imagen_alumno = $("#imagen_alumno").val();
            var nombre1 = $("#nombre1").val();
            var nombre2 = $("#nombre2").val();
            var segundo_nombre1 = $("#segundo_nombre1").val();
            var segundo_nombre2 = $("#segundo_nombre2").val();
            var email   = $("#email").val();
            var genero = $("#genero").val();
            var estado_civil = $("#estado_civil").val();
            var f_nacimiento = $("#f_nacimiento").val();
            var nacionalidad = $("#nacionalidad").val();
            var lugar_nacimiento = $("#lugar_nacimiento").val();
            var profesion = $("#profesion").val();
            var curso_realizar = $("#curso_realizar").val();
            var fecha_inscipcion = $("#fecha_inscipcion").val();
            var tipo_documento = $("#tipo_documento").val();
            var n_documento = $("#n_documento").val();
            var lugar_expedicion = $("#lugar_expedicion").val();
            var direccion = $("#direccion").val();
            var telefono = $("#telefono").val();
            var empresa = $("#empresa").val();
            var empresa_telefono = $("#empresa_telefono").val();
            var cargo = $("#cargo").val();
            var tiempo_experiencia = $("#tiempo_experiencia").val();
            var estudios = $("#estudios").val();
            var tecnico_institucion = $("#tecnico_institucion").val();
            var tecnico_titulo = $("#tecnico_titulo").val();
            var tecnologico_institucion = $("#tecnologico_institucion").val();
            var tecnologico_titulo = $("#tecnologico_titulo").val();
            var profesional_institucion = $("#profesional_institucion").val();
            var profesional_titulo = $("#profesional_titulo").val();
            var postgrado_institucion = $("#postgrado_institucion").val();
            var profesional_titulo = $("#profesional_titulo").val();
            var especializacion_institucion = $("#especializacion_institucion").val();
            var especializacion_titulo = $("#especializacion_titulo").val();
            var seminarios_institucion = $("#seminarios_institucion").val();
            var seminarios_titulo = $("#seminarios_titulo").val();
            var tla_numero = $("#tla_numero").val();

            // var tlh_numero = $("#tlh_numero").val();
            // var term_numero = $("#term_numero").val();
            // var teei_numero = $("#teei_numero").val();
            // var teh_numero = $("#teh_numero").val();
            // var temc_numero = $("#temc_numero").val();
            // var ait_numero = $("#ait_numero").val();
            // var faa_numero = $("#faa_numero").val();
            // var tesh_numero = $("#tesh_numero").val();
            // var otras_numero = $("#otras_numero").val();

            if(imagen_alumno == ""){
                toastr.error('El campo Imagen está vacio.', 'Error');
                $("#imagen_alumno").focus();
                return;
            }
            if(nombre1 == ""){
                toastr.error('El campo Primer Nombre está vacio.', 'Error');
                $("#nombre1").focus();
                return;
            }
            // if(nombre2 == ""){
            //     toastr.error('El campo nombre2 está vacio.', 'Error');
            //     $("#nombre2").focus();
            //     return;
            // }
            if(segundo_nombre1 == ""){
                toastr.error('El campo Primer Apellido está vacio.', 'Error');
                $("#segundo_nombre1").focus();
                return;
            }
            // if(segundo_nombre1 == ""){
            //     toastr.error('El campo segundo_nombre1 está vacio.', 'Error');
            //     $("#segundo_nombre1").focus();
            //     return;
            // }
            if(genero == ""){
                toastr.error('El campo Genero está vacio.', 'Error');
                $("#genero").focus();
                return;
            }
            if(estado_civil == ""){
                toastr.error('El campo Estado Civil está vacio.', 'Error');
                $("#estado_civil").focus();
                return;
            }
            if(f_nacimiento == ""){
                toastr.error('El campo Fecha de Nacimiento está vacio.', 'Error');
                $("#f_nacimiento").focus();
                return;
            }
            if(nacionalidad == ""){
                toastr.error('El campo Nacionalidad está vacio.', 'Error');
                $("#nacionalidad").focus();
                return;
            }
            if(lugar_nacimiento == ""){
                toastr.error('El campo Lugar de Nacimiento está vacio.', 'Error');
                $("#lugar_nacimiento").focus();
                return;
            }
            if(profesion == ""){
                toastr.error('El campo Profesión está vacio.', 'Error');
                $("#profesion").focus();
                return;
            }
            if(curso_realizar == ""){
                toastr.error('El campo Curso a Realizar está vacio.', 'Error');
                $("#curso_realizar").focus();
                return;
            }
            if(fecha_inscipcion == ""){
                toastr.error('El campo Fecha de Inscripción está vacio.', 'Error');
                $("#fecha_inscipcion").focus();
                return;
            }
            if(email == ""){
                toastr.error('El campo Email está vacio.',"Error");
                $("#email").focus();
                return;
            }else{
                var validacion = validarEmail( email );
                if (validacion == 1) {
                    toastr.error('El Email '+ email +' no es valido.',"Error"); 
                    $("#email").focus(); 
                    return;
                }
            }
            if(tipo_documento == ""){
                toastr.error('El campo Tipo de Documento está vacio.', 'Error');
                $("#tipo_documento").focus();
                return;
            }
            if(n_documento == ""){
                toastr.error('El campo Numero de Documento está vacio.', 'Error');
                $("#n_documento").focus();
                return;
            }
            if(lugar_expedicion == ""){
                toastr.error('El campo Expedido en está vacio.', 'Error');
                $("#lugar_expedicion").focus();
                return;
            }
            if(direccion == ""){
                toastr.error('El campo Dirección está vacio.', 'Error');
                $("#direccion").focus();
                return;
            }
            if(telefono == ""){
                toastr.error('El campo Teléfono está vacio.', 'Error');
                $("#telefono").focus();
                return;
            }
            if(empresa == ""){
                toastr.error('El campo Empresa donde Trabaja está vacio.', 'Error');
                $("#empresa").focus();
                return;
            }
            if(empresa_telefono == ""){
                toastr.error('El campo Teléfono de Empresa está vacio.', 'Error');
                $("#empresa_telefono").focus();
                return;
            }
            if(cargo == ""){
                toastr.error('El campo Cargo Actual está vacio.', 'Error');
                $("#cargo").focus();
                return;
            }
            if(tiempo_experiencia == ""){
                toastr.error('El campo Tiempo de Experiencia está vacio.', 'Error');
                $("#tiempo_experiencia").focus();
                return;
            }
            if(estudios == ""){
                toastr.error('El campo Estudios está vacio.', 'Error');
                $("#estudios").focus();
                return;
            }

            // if(tecnico_institucion == ""){
            //     toastr.error('El campo tecnico_institucion está vacio.', 'Error');
            //     $("#tecnico_institucion").focus();
            //     return;
            // }
            // if(tecnico_titulo == ""){
            //     toastr.error('El campo tecnico_titulo está vacio.', 'Error');
            //     $("#tecnico_titulo").focus();
            //     return;
            // }
            // if(tecnologico_institucion == ""){
            //     toastr.error('El campo tecnologico_institucion está vacio.', 'Error');
            //     $("#tecnologico_institucion").focus();
            //     return;
            // }
            // if(tecnologico_titulo == ""){
            //     toastr.error('El campo tecnologico_titulo está vacio.', 'Error');
            //     $("#tecnologico_titulo").focus();
            //     return;
            // }
            // if(profesional_institucion == ""){
            //     toastr.error('El campo profesional_institucion está vacio.', 'Error');
            //     $("#profesional_institucion").focus();
            //     return;
            // }
            // if(profesional_titulo == ""){
            //     toastr.error('El campo profesional_titulo esta vacio.', 'Error');
            //     $("#profesional_titulo").focus();
            //     return;
            // }
            // if(postgrado_institucion == ""){
            //     toastr.error('El campo postgrado_institucion está vacio.', 'Error');
            //     $("#postgrado_institucion").focus();
            //     return;
            // }
            // if(profesional_titulo == ""){
            //     toastr.error('El campo profesional_titulo está vacio.', 'Error');
            //     $("#profesional_titulo").focus();
            //     return;
            // }
            // if(especializacion_institucion == ""){
            //     toastr.error('El campo especializacion_institucion está vacio.', 'Error');
            //     $("#especializacion_institucion").focus();
            //     return;
            // }
            // if(especializacion_titulo == ""){
            //     toastr.error('El campo especializacion_titulo está vacio.', 'Error');
            //     $("#especializacion_titulo").focus();
            //     return;
            // }
            // if(seminarios_institucion == ""){
            //     toastr.error('El campo seminarios_institucion está vacio.', 'Error');
            //     $("#seminarios_institucion").focus();
            //     return;
            // }
            // if(seminarios_titulo == ""){
            //     toastr.error('El campo seminarios_titulo está vacio.', 'Error');
            //     $("#seminarios_titulo").focus();
            //     return;
            // }
            // if(tla_numero == ""){
            //     toastr.error('El campo tla_numero está vacio.', 'Error');
            //     $("#tla_numero").focus();
            //     return;
            // }
            
            var id_c = new Array();
            var institucion = new Array();
            var titulo = new Array();
            var i = 0;

            @foreach($estudios as $estudio)
                id_c[i] = {{$estudio->id}};
                institucion[i] = $("#tecnico_institucion_{{$estudio->id}}").val();
                titulo[i] = $("#tecnico_titulo_{{$estudio->id}}").val();
                i++;
            @endforeach

            var id_licencia = new Array();
            var numero = new Array();
            var vencimiento = new Array();
            
            var j = 0;

            @foreach($licencias as $licencia)
                id_licencia[j] = {{$licencia->id}};
                numero[j] = $("#tla_numero_{{$licencia->id}}").val();
                vencimiento[j] = $("#tla_vencimiento_{{$licencia->id}}").val();
                j++;
            @endforeach


            
			$("#imagen_alumno").upload('ingresarnuevoalumno', 
			{
			    
			     
                 nombre1: nombre1,
                 nombre2: nombre2,
                 segundo_nombre1: segundo_nombre1,
                 segundo_nombre2: segundo_nombre2,
                 email: email,
                 genero: genero,
                 estado_civil: estado_civil,
                 f_nacimiento: f_nacimiento,
                 nacionalidad: nacionalidad,
                 lugar_nacimiento: lugar_nacimiento,
                 profesion: profesion,
                 curso_realizar: curso_realizar,
                 fecha_inscipcion: fecha_inscipcion,
                 tipo_documento: tipo_documento,
                 n_documento: n_documento,
                 lugar_expedicion: lugar_expedicion,
                 direccion: direccion,
                 telefono: telefono,
                 empresa: empresa,
                 empresa_telefono: empresa_telefono,
                 cargo: cargo,
                 tiempo_experiencia: tiempo_experiencia,
                 estudios: estudios,
                 id_curso:id_c,
                 institucion: institucion,
                 titulo: titulo,
                 id_licencia:id_licencia,
                 numerolic:numero,
                 vencimiento:vencimiento
			    
			    
			    
			},
			function(respuesta){
			    
			    if (respuesta === 1) {
			        toastr.success('Se ingreso correctamente el nuevo Alumno', 'Nuevo Alumno');
			            
			        $("#nombre").val();
			        $("#apellido").val();
			        $("#documento").val();
			        $("#imagen_ins").val();
			        

			        
			        
			        
			    } else {
			        toastr.error('No ha sido posible ingresar el nuevo Alumno.', 'Error');                              
			        
			    }
			});

	}
    function validarEmail( email ) {
        expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if ( !expr.test(email) ){
            return 1;
        }
            
    }
        
</script>