<div class="modal-dialog " >
    <div class="modal-content">
    <form class="form" action="#">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Editar Empresa</h4>
        </div>
        <div class="modal-body form-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Nombre Empresa</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="nombre_edit" value="{{$detalle_empresa[0]->name}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Nombre Contacto</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="contacto_edit" value="{{$detalle_empresa[0]->name_contact}}">
                       </div> 
                    </div> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Dirección</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="direccion_edit" value="{{$detalle_empresa[0]->address}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Teléfono</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="telefono_edit" value="{{$detalle_empresa[0]->phone}}">
                       </div> 
                    </div> 
                </div>   
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Email</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="email_edit" value="{{$detalle_empresa[0]->email}}">
                       </div> 
                    </div> 
                </div> 
                <div class="col-md-6">
                    <div class="form-group"> 
                       <label class="control-label">Observaciones</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <textarea  class="form-control" placeholder="" data-required="true" name="name_n" id="observaciones_edit" >{{$detalle_empresa[0]->observations}}</textarea>
                       </div> 
                    </div> 
                </div>   
            </div>
            
        </div>
        <div class="modal-footer">
            <a class="btn btn-success" onclick="editar_empresa({{$detalle_empresa[0]->id}})"><i class="icon-edit"></i> Editar</a>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_detalle">Cerrar</button>
        </div>
  
    </form>   
    </div>
</div>