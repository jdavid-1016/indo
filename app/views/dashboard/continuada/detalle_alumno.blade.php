<link href="{{  URL::to("assets/plugins/font-awesome/css/font-awesome.min.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/plugins/bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/plugins/uniform/css/uniform.default.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{URL::to("assets/plugins/fancybox/source/jquery.fancybox.css")}}" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<link href="{{  URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/plugins/gritter/css/jquery.gritter.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css")}}" rel="stylesheet" type="text/css" />
<link href="{{  URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/plugins/jqvmap/jqvmap/jqvmap.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css")}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/fuelux/css/tree-conquer.css")}}" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN THEME STYLES --> 
<link href="{{  URL::to("assets/css/style-conquer.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/css/style.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/css/style-responsive.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/css/pages/tasks.css")}}" rel="stylesheet" type="text/css"/>
<link href="{{  URL::to("assets/css/themes/blue.css")}}" rel="stylesheet" type="text/css" id="style_color"/>
<link href="{{  URL::to("assets/css/custom.css")}}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{URL::to("assets/plugins/bootstrap-toastr/toastr.min.css")}}">
<link rel="stylesheet" type="text/css" href="{{  URL::to("assets/plugins/select2/select2_conquer.css")}}" />
<link rel="stylesheet" href="{{  URL::to("assets/plugins/data-tables/DT_bootstrap.css")}}" />

<script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>
<script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
   <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   
   <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
   <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
   // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
   <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
   <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->   
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
   <!-- END PAGE LEVEL SCRIPTS -->     
   <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
   <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
   <script src="{{ URL::to("js/app.js")}}"></script>
   <script src="{{ URL::to("js/upload.js")}}"></script>
   <script src="{{ URL::to("js/validar.js")}}"></script>
   <script src="{{ URL::to("js/compras.js")}}"></script>
   <script src="{{ URL::to("js/continuada.js")}}"></script>
   <script src="{{ URL::to("js/equipos.js")}}"></script>
   <script src="{{ URL::to("js/aspirantes.js")}}"></script>
   <script src="{{ URL::to("js/angular.min.js")}}"></script>
   <script src="{{ URL::to("js/documentacion.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>

   
<div class="modal-dialog modal-wide">
    <div class="modal-content ">
        <form class="form" action="#" >   
            <input type="hidden" value="" id="prospect_id">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Detalle Alumno</h4>
            </div>
            <div class="modal-body form-body">
                <ul  class="nav nav-tabs">
                    <li class="active"><a href="#info_estudiante" data-toggle="tab">Detalle del Alumno</a></li>
                    <li class=""><a href="#cursos_estudiante"  data-toggle="tab">Cursos</a></li>
                    <li class=""><a href="#estudios_estudiante"  data-toggle="tab">Estudios</a></li>
                    <li class=""><a href="#licencias_estudiante"  data-toggle="tab">Licencias</a></li>
                    <li class=""><a href="#cambiarpass"  data-toggle="tab">Cambiar Contraseña</a></li>
                    <li class=""><a href="#evaluacionentrada"  data-toggle="tab">Evaluacion Entrada</a></li>
                    <li class=""><a href="#evaluacionsalida"  data-toggle="tab">Evaluacion Salida</a></li>
                    <li class=""><a href="#respuestas"  data-toggle="tab">Respuestas</a></li>
                </ul>
                <div class="tab-content">      
                    <div class="tab-pane fade active in" id="info_estudiante">
                    @foreach($detallealumno as $detalle)
                        <form class="form-horizontal" id="form">
                            <div class="form-body">
                                <div class="row">                            
                                    <div class="col-md-12">
                                         <form class="form-horizontal" id="form">
                                             <div class="form-body">
                                                 <div class="row">                            
                                                     <div class="col-md-12 note note-warning">
                                                         <div class="row">
                                                             <div class="col-md-2">
                                                             </div>
                                                             <div class="col-md-2">                
                                                                 <div class="form-group">
                                                                     <div class="col-md-12">
                                                                         <label class="control-label">Imagen</label>
                                                                         <div class="fileupload fileupload-new" data-provides="fileupload">
                                                                             <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                                 <img src="../{{$detalle->Users->img_min}}" alt="">
                                                                             </div>
                                                                             <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                                             <div>
                                                                                <!--  <span class="btn btn-default btn-file">
                                                                                     <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                                     <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                                     <input type="file" class="default form-control" name="imagen_alumno" id="imagen_alumno" size="20">
                                                                                 </span>
                                                                                 <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_imagen_alumno"><i class="icon-trash"></i> Eliminar</a>
                                                                                 <div class="help-block">
                                                                                     Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                                 </div> -->
                                                                             </div>
                                                                         </div>
                                                                     </div>
                                                                 </div>  
                                                             </div>
                                                             <div class="col-md-8">
                                                             
                                                                 <div class="col-md-6">
                                                                     <div class="form-group">
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="nombre1_edit" name="name" type="text" class="form-control" value="{{$detalle->Users->name}}" placeholder="Primer Nombre">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>

                                                                 <div class="col-md-6">
                                                                     <div class="form-group">
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="nombre2_edit" name="middle_name" type="text" class="form-control" value="{{$detalle->Users->name2}}" placeholder="Segundo Nombre">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="apellido1_edit" name="last_name" type="text" class="form-control" value="{{$detalle->Users->last_name}}" placeholder="Primer Apellido">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="apellido2_edit" name="last_name2" type="text" class="form-control" value="{{$detalle->Users->last_name2}}" placeholder="Segundo Apellido">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <select id="genero_edit" name="genero" class="form-control">
                                                                                 @foreach($generos as $genero)
                                                                                    @if($genero->id == $detalle->Users->general_genres_id)
                                                                                        <option value="{{$genero->id}}" selected="">{{$genero->gender}}</option>
                                                                                    @else
                                                                                        <option value="{{$genero->id}}">{{$genero->gender}}</option>
                                                                                    @endif
                                                                                 @endforeach
                                                                                     
                                                                                 </select>
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <select id="estado_civil_edit" name="estado_civil"  class="form-control">
                                                                                     <!-- <option value="">{{$detalle->Users->GeneralMaritalStatus->marital_status}}</option> -->

                                                                                     @foreach($estado_civil as $estadoc)
                                                                                        @if($estadoc->id == $detalle->Users->general_marital_statuses_id)
                                                                                            <option value="{{$estadoc->id}}" selected="">{{$estadoc->marital_status}}</option>
                                                                                        @else
                                                                                            <option value="{{$estadoc->id}}">{{$estadoc->marital_status}}</option>
                                                                                        @endif
                                                                                     @endforeach
                                                                                     
                                                                                 </select>
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="f_nacimiento_edit" type="text" class="form-control fecha_detalle_alumno" placeholder="Fecha Nacimiento (0000-00-00)" value="{{$detalle->Users->birthday}}">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <select id="nacionalidad_edit" name="nacionalidad" class="form-control">
                                                                                     <!-- <option value="">{{$detalle->Users->Countries->country}}</option> -->
                                                                                    @foreach($nacionalidades as $nacionalidad)
                                                                                       @if($nacionalidad->id == $detalle->Users->countries_id)
                                                                                           <option value="{{$nacionalidad->id}}" selected="">{{$nacionalidad->country}}</option>
                                                                                       @else
                                                                                           <option value="{{$nacionalidad->id}}">{{$nacionalidad->country}}</option>
                                                                                       @endif
                                                                                    @endforeach
                                                                                     
                                                                                 </select>
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>

                                                                                <select id="lugar_nac_edit" name="nacionalidad" class="form-control">
                                                                                    <!-- <option value="">{{$detalle->Users->Countries->country}}</option> -->
                                                                                   @foreach($lugarnac as $lugarnac)
                                                                                      @if($lugarnac->id == $detalle->Users->city_id)
                                                                                          <option value="{{$lugarnac->id}}" selected="">{{$lugarnac->name}}</option>
                                                                                      @else
                                                                                          <option value="{{$lugarnac->id}}">{{$lugarnac->name}}</option>
                                                                                      @endif
                                                                                   @endforeach
                                                                                    
                                                                                </select>
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                 <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="profesion_edit" type="text" class="form-control" placeholder="Profesión" value="{{$detalle->profession}}">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                                <div class="col-md-6">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="fecha_inscipcion_edit" name="profesion" type="text" class="form-control fecha_detalle_alumno" value="{{$detalle->date_inscription}}" placeholder="Fecha Inscripción (0000-00-00)">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                </div>


                                                             
                                                             </div>
                                                                


                                                         </div>  
                                                         <div class="row">
                                                             <div class="col-md-8 pull-right">
                                                                     <div class="form-group" >
                                                                         <div class="col-md-12">

                                                                             <div class="input-group">
                                                                                 <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                                 <input id="email_edit" name="profesion" type="text" class="form-control" value="{{$detalle->Users->email_institutional}}" placeholder="email">
                                                                             </div> 
                                                                         </div>
                                                                     </div>
                                                                 </div>
                                                         </div>
                                                     </div>

                                                 </div>


                                            <div class="col-md-12 note note-info">
                                                 <div class="row">
                                                         <h4 class="block">Datos Personales</h4>
                                                         
                                                         <div class="col-md-12">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Tipo de documento</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <select id="tipo_documento_edit" name="tipo_documento" class="form-control">
                                                                            @foreach($typedoc as $typedoc)
                                                                               @if($typedoc->id == $detalle->Users->type_document)
                                                                                   <option value="{{$typedoc->id}}" selected="">{{$typedoc->type}}</option>
                                                                               @else
                                                                                   <option value="{{$typedoc->id}}">{{$typedoc->type}}</option>
                                                                               @endif
                                                                            @endforeach
                                                                         </select>
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">N° Doc Identidad</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="n_documento_edit" name="document" type="text" class="form-control" value="{{$detalle->Users->document}}" placeholder="N° Doc Identidad">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Expedido en</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <select id="lugar_expedicion_edit" name="document" class="form-control">
                                                                         @foreach($expedido as $expedido)
                                                                            @if($expedido->id == $detalle->birthplace)
                                                                                <option value="{{$expedido->id}}" selected="">{{$expedido->name}}</option>
                                                                            @else
                                                                                <option value="{{$expedido->id}}">{{$expedido->name}}</option>
                                                                            @endif
                                                                         @endforeach
                                                                         </select>
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Dirección de Residencia</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="direccion_edit" type="text" class="form-control" value="{{$detalle->Users->address}}" placeholder="Dirección de Residencia">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Teléfonos</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="telefono_edit" type="text" class="form-control" value="{{$detalle->Users->phone}}" placeholder="Teléfonos">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Empresa donde Trabaja</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="empresa_edit" type="text" class="form-control" value="{{$detalle->employment_business}}" placeholder="Empresa donde Trabaja">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Teléfonos</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="empresa_telefono_edit" type="text" class="form-control" value="{{$detalle->employment_business_phone}}" placeholder="Teléfonos">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Cargo Actual</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="cargo_edit" type="text" class="form-control" value="{{$detalle->current_position}}" placeholder="Cargo Actual">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Tiempo de Experiencia</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="tiempo_experiencia_edit" type="text" class="form-control" value="{{$detalle->time_experience}}" placeholder="Tiempo de Experiencia">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">N° Libreta Militar</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="n_libreta_militar_edit" type="text" class="form-control" value="{{$detalle->military_card}}" placeholder="N° Libreta Militar">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         <div class="col-md-6">
                                                             <div class="form-group" >
                                                                 <div class="col-md-12">
                                                                    <label class="control-label">Distrito Militar</label>
                                                                     <div class="input-group">
                                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                                         <input id="distrito_militar_edit" type="text" class="form-control" value="{{$detalle->military_district}}" placeholder="Distrito Militar">
                                                                     </div> 
                                                                 </div>
                                                             </div>
                                                         </div>
                                                         



                                                     </div>
                                                     
                                                 </div>
                                                 <div class="row">


                                                     

                                             </div>
                                         </form>

                                     </div>

                                </div>
                                <div class="form-actions fluid">
                                    <div class="col-md-1 col-md-11">
                                        <a class="btn btn-success pull-right" onclick="enviar_datos_alumno({{$detalle->id}}, {{$detalle->Users->id}})">Guardar</a>
                                    </div>
                                </div>                        
                            </div>
                        </form>
                    @endforeach
                        

                    </div>
                    </div>
                    <div class="tab-pane fade" id="cursos_estudiante">
                        <h4 style="text-align: center"><b>CURSOS</b></h4>
                        <hr>
                        <table class="table top-blue" data-target="soporte/callSupport/">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>OCSA</th>
                                    <th>Curso</th>
                                    <th>Inicio</th>
                                    <th>Fin</th>
                                    <th>Estado</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody id="tabla_materias_cursos">
                                @foreach($cursos as $curso)
                                <tr style="text-align:center;">
                                    <td>{{ $curso->id }}</td>
                                    <td>{{ $curso->ocsa }}</td>
                                    <td>{{ $curso->name_curso }}</td>
                                    <td>{{ $curso->start_date }}</td>
                                    <td>{{ $curso->end_date }}</td>
                                    <td>@if($curso->status==1) Activo @else Inactivo @endif</td>
                                    <td><a href="#" class="btn btn-info" onclick="generar_diploma({{ $curso->id }}, {{$detallealumno[0]->id}})"><i class="icon-print"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
                                    <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
   		                </div>
   		            </div>
                    </div>
                    
                    <div class="tab-pane fade" id="cambiarpass">
                        <div class="col-md-12 note note-info ">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="block">Cambiar contraseña</h4>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Nueva contraseña</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="nueva" type="password" class="form-control" value="">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <div class="col-md-12">
                                            <label class="control-label">Re-ingresar nueva contraseña</label>
                                                <div class="input-group">
                                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                    <input id="renueva" type="password" class="form-control" value="">
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                                                
                                </div>
                            </div> 
                        </div>
                        <div class="form-actions fluid">
   		                <div class="col-md-1 col-md-11">
   		                    <a class="btn btn-success" onclick="CambiarPassEst({{$detallealumno[0]->Users->id}})">Guardar</a>
                                    <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
   		                </div>
   		            </div>

   		            </div>
                    <div class="tab-pane fade" id="estudios_estudiante">
                        <div class="col-md-12 note note-info">
                            <h4 class="block">Estudios Realizados</h4>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group" >
                                        <div class="col-md-12">

                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                <select id="tipo_estudios" name="estudios" class="form-control">
                                                    <option value="">Tipo de Estudios</option>
                                                    @foreach($tipo_estudios as $tipo_estudio)
                                                        <option value="{{$tipo_estudio->id}}">{{$tipo_estudio->course}}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" >
                                        <div class="col-md-12">
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                <input id="institucion_estudios" type="text" class="form-control" placeholder="Institución">
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" >
                                        <div class="col-md-12">
                                            <div class="input-group">

                                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                <input id="titulo_estudios" type="text" class="form-control" placeholder="Título">
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <table class="top-blue table">
                                        <tr>
                                            <th>EDUCACIÓN</th>
                                            <th>INSTITUCIÓN</th>
                                            <th>TÍTULO</th>

                                        </tr>
                                        <tbody id="tabla_estudios_alumno">
                                        @foreach($estudios as $estudio)
                                        <tr class="center">
                                            <td>
                                                <select id="tipo_estudios_edit" name="estudios" class="form-control">
                                                    @foreach($tipo_estudios as $tipo_estudio)
                                                        @if($tipo_estudio->id == $estudio->courses_students_studies_id)
                                                            <option value="{{$tipo_estudio->id}}" selected="">{{$tipo_estudio->course}}</option>
                                                        @else
                                                            <option value="{{$tipo_estudio->id}}" >{{$tipo_estudio->course}}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <!-- {{$estudio->courses_students_studies_id}} -->
                                            </td>
                                            <td>
                                                
                                                <input id="institucion_estudios_edit" type="text" class="form-control" placeholder="Institucion" value="{{$estudio->institution}}">
                                            </td>
                                            <td>
                                                
                                                <input id="titulo_estudios_edit" type="text" class="form-control" placeholder="Título" value="{{$estudio->title}}">
                                            </td>
                                            
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-1 col-md-11">
                                    <a class="btn btn-success pull-right" onclick="enviar_estudios_alumno({{$detallealumno[0]->id}}, {{$detallealumno[0]->Users->id}})">Guardar Estudios</a>
                                </div>
                            </div> 
                    </div>
                    <div class="tab-pane fade" id="licencias_estudiante">
                        <div class="col-md-12 note note-warning">
                                <h4 class="block">Licencias Técnicas</h4>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group" >
                                        <div class="col-md-12">
                                        <label class="control-label"></label>
                                            <div class="input-group">
                                                <select id="tipo_licencia" name="estudios" class="form-control">
                                                    <option value="">Tipo de licencias</option>
                                                    @foreach($tipos_licencias as $tipos_licencia)
                                                        <option value="{{$tipos_licencia->id}}">{{$tipos_licencia->programs}}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" >
                                        <div class="col-md-12">
                                        <label class="control-label"></label>
                                            <div class="input-group">
                                                <input id="licencia_numero" type="text" class="form-control" placeholder="Número">
                                                
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group" >
                                        <div class="col-md-12">
                                        <label class="control-label"></label>
                                            <div class="input-group">
                                                <input id="licencia_vencimiento" type="text" class="form-control fecha_nuevoalum" placeholder="F vencimiento">
                                            </div> 
                                        </div>
                                    </div>
                                </div>

                            </div>                                                     
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <table class="top-blue table">
                                    <tr>
                                        <th>LICENCIA</th>
                                        <th>NÚMERO</th>
                                        <th>FECHA VENCIMIENTO</th>

                                    </tr>
                                    <tbody id="tabla_licencias_alumno">
                                    @foreach($licencias as $licencia)
                                    <tr class="center">
                                        <td>
                                            <select id="" name="estudios" class="form-control">
                                                @foreach($tipos_licencias as $tipos_licencia)
                                                    @if($tipos_licencia->id == $licencia->courses_technical_licenses_id)
                                                        <option value="{{$tipos_licencia->id}}" selected="">{{$tipos_licencia->programs}}</option>
                                                    @else
                                                        <option value="{{$tipos_licencia->id}}" >{{$tipos_licencia->programs}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            
                                        </td>
                                        <td>{{$licencia->number_licence}}</td>
                                        <td>{{$licencia->expiration_date}}</td>
                                        
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <div class="form-actions fluid">
                        <div class="col-md-1 col-md-11">
                            <a class="btn btn-success pull-right" onclick="enviar_licencias_alumno({{$detallealumno[0]->id}}, {{$detallealumno[0]->Users->id}})">Guardar Licencias</a>
                        </div>
                    </div> 
                    </div>
                    
                    <div class="tab-pane fade" id="evaluacionentrada">
                                                                                 
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Formulario de Entrada</div>
            </div>
            <div class="portlet-body">                
                <div class="row">
                    <div class="col-md-12">
                        
                        <h3>Propósito</h3>
                        <p>
                            La Corporación Educativa Indoamericana se encuentra interesada en conocer sus expectativas respecto al Curso Especial  al que va a ingresar, por tal motivo es muy importante que diligencie el siguiente formato teniendo en cuenta que la información debe ser verídica ya que esta contribuye al desarrollo del curso y al alcance de los objetivos propuestos.
                        </p>


                        @foreach($preguntas_entrada as $preguntas_ent)
                        
                        <p>
                            {{$preguntas_ent['pregunta']}}
                            <textarea class="form-control" id="pregunta{{$preguntas_ent['id_pregunta']}}" idresp="{{$preguntas_ent['id_respuesta']}}" {{$preguntas_ent['funcion']}} {{$preguntas_ent['funcion2']}}>{{$preguntas_ent['respuesta']}}</textarea>
                        </p>
                        <!-- <p>
                            1 ¿Cuáles son sus expectativas de aprendizaje respecto al curso que va a ingresar?
                            <textarea class="form-control" id="pregunta1"></textarea>
                        </p>
                        <p>
                        <p>
                            2 ¿Cuáles son los temas en que le gustaría profundizar para mejorar su formación?
                            <textarea class="form-control" id="pregunta2"></textarea>
                        </p>
                        <p>
                            3 ¿Cuál es la experiencia que tiene con respecto a la temática del curso?
                            <textarea class="form-control" id="pregunta3"></textarea>
                        </p>
                        <p>
                            4 En caso de tener experiencia ¿Cómo puede contribuir para dinamizar el desarrollo del curso?
                            <textarea class="form-control" id="pregunta4"></textarea>
                        </p>
                        <p>
                            5 ¿Qué compromisos puede proyectar para ser multiplicador de los conocimientos adquiridos durante el desarrollo del curso?
                            <textarea class="form-control" id="pregunta5"></textarea>
                        </p> -->
                        @endforeach

                    </div>

                </div>
                <button type="button" class="btn btn-success" onclick="EvaluacionEntrada({{$detallealumno[0]->id}}, {{$idcurso}});" style="{{$ocultar_btn}}">Guardar</button>
            </div>
        </div>
    </div>
                        
                    </div>
                    
                    <div class="tab-pane fade" id="evaluacionsalida">
                                         
    <div class="row">
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption"><i class="icon-reorder"></i>Formulario de Salida</div>
            </div>
            <div class="portlet-body">
               
                <div class="row">
                    <div class="col-md-12">
                        
                        <h3>Encuesta de satisfacción educación continuada</h3>
                        <p>
                            La corporación educativa indoamericana es consciente de la importancia que tiene el retroalimentar los procesos y tomar acciones enfocadas hacia la mejora continua. Lo invitamos a evaluar los siguientes aspectos presentados durante el entrenamiento de .NOMBRE DEL CURSO. En una escala de 1 a 5 siendo 1 malo y 5 excelente, evalué el nivel de desempeño según los ítems establecidos.
                        </p>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-12">
                        <p>
                            1. Desempeño de instructores
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Intructor</th>
                                    
                                    <th>Conocimiento</th>
                                    <th>Metodologia</th>
                                    <th>Dominio de Grupo</th>
                                    <th>Capacidad de Comunicación</th>
                                    <th>Solución de inquietudes</th>
                                    <th>Nivel de Actualización</th>
                                    <th>Material de Consulta</th>
                                    <th>Trato Personal</th>
                                    

                                </tr>
                                
                                @foreach($materias as $mat)
                                <tr>                                    
                                    <td class="col-md-2">{{$mat->CoursesInstructors->Users->name}} {{$mat->CoursesInstructors->Users->last_name}}</td>
                                    <td><input type="text" maxlength="1" id="pregunta6-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta7-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta8-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta9-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta10-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta11-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta12-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                    <td><input type="text" maxlength="1" id="pregunta13-{{$mat->CoursesInstructors->id}}" class="form-control solo-numero"></td>
                                </tr>
                                @endforeach

                            </table>
                        </p> 
                        <p>
                        <p>
                            2. <strong>Logística del evento:</strong> Evalué de 1 a 5 el apoyo de los funcionarios de la institución y la coordinación general del curso.
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Ítem </th>
                                    <th>Nota </th>
                                    <th>Observaciones</th>

                                </tr>
                                <tr>
                                    <td>Apoyo por parte de los funcionarios de la institución</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta14not" name="" value=""></td>
                                    <td><textarea class="form-control" id="pregunta14obs" ></textarea></td>

                                </tr>
                                <tr>
                                    <td>Manejo de tiempos</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta15not" name="" value=""></td>
                                    <td><textarea class="form-control" id="pregunta15obs" ></textarea></td>
                                </tr>
                                <tr>
                                    <td>Ayudas audiovisuales</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta16not" name="" value=""></td>
                                    <td><textarea class="form-control" id="pregunta16obs" ></textarea></td>
                                </tr>
                                <tr>
                                    <td>Material adecuado</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta17not" name="" value=""></td>
                                    <td><textarea class="form-control" id="pregunta17obs" ></textarea></td>
                                </tr>
                                <tr>
                                    <td>Coordinación de tiempos</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta18not" name="" value=""></td>
                                    <td><textarea class="form-control" id="pregunta18obs" ></textarea></td>
                                </tr>

                            </table>
                        </p>
                        <p>
                            3. <strong>Instalaciones:</strong> Evalué el servicio prestado en las instalación es de la institución 
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Ítem </th>
                                    <th>Nota </th>
                                    <th>Observaciones</th>

                                </tr>
                                <tr>
                                    <td>Instalaciones adecuadas</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta19not" name="" value=""></td>
                                    <td><textarea class="form-control" id="pregunta19obs" ></textarea></td>

                                </tr>
                                <tr>
                                    <td>Atención al personal</td>
                                    <td><input type="text" maxlength="1" class="form-control solo-numero" id="pregunta20not" name="" value=""></td>
                                    <td><textarea class="form-control"id="pregunta20obs" ></textarea></td>

                                </tr>

                            </table>
                        </p>
                    </div>
                </div>
                <div class="row">
                    
                    <div class="col-md-6">
                        <p>
                            4. ¿Sobre qué tema le parece que se debería profundizar y que otro tipo de formación le gustaría tomar?
                            <textarea class="form-control"id="pregunta21" ></textarea>
                        </p>
                        <p> 
                            5 ¿Que temas le parecieron más relevantes en la formación  ?
                            <textarea class="form-control"id="pregunta22" ></textarea>
                        </p>
                    </div>              

            </div>
                <button type="button" class="btn btn-success" onclick="EvaluacionSalida({{$detallealumno[0]->id}}, {{$idcurso}});">Guardar</button>
        </div>
    </div>
</div>
                        
                    </div>
                    <div class="tabl-pane fade" id="respuestas">
                        <p>
                            1. Desempeño de instructores
                            <table class="table top-blue table-bordered">
                                <tr>
                                    <th>Intructor</th>
                                    @foreach($preguntas_salida1 as $preguntas_sal1)
                                    <th>{{$preguntas_sal1['pregunta']}}</th>
                                    <!-- <th>Conocimiento</th>
                                    <th>Metodologia</th>
                                    <th>Dominio de Grupo</th>
                                    <th>Capacidad de Comunicación</th>
                                    <th>Solución de inquietudes</th>
                                    <th>Nivel de Actualización</th>
                                    <th>Material de Consulta</th>
                                    <th>Trato Personal</th> -->
                                    @endforeach

                                </tr>
                                <?php $i = 0; ?>
                                @foreach($preguntas_profesores as $preg_prof)
                                <tr>
                                    <td>{{$preg_prof['profesor']}}</td>


                                    @foreach($preguntas_profesores_resp as $resp)
                                    @if($resp->courses_instructors_id == $preg_prof['id_profesor'])
                                    <td>{{$resp->note}} </td>
                                    @endif
                                    @endforeach

                                <?php $i++; ?>
                                </tr>
                                @endforeach

                                <!-- @foreach($materias as $mat)
                                <tr>                                    
                                    <td class="col-md-2">{{$mat->CoursesInstructors->Users->name}} {{$mat->CoursesInstructors->Users->last_name}}</td>
                                    <td><input type="text" id="pregunta6-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta7-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta8-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta9-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta10-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta11-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta12-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                    <td><input type="text" id="pregunta13-{{$mat->CoursesInstructors->id}}" class="form-control"></td>
                                </tr>
                                @endforeach -->

                            </table>
                        </p> 
                    </div>

                   <!--  -->
                    
                </div>  
                          
                <hr>
            </div>            
       </form>
    </div>
</div>
<script>

   $( ".fecha_detalle_alumno").datepicker({ dateFormat: "yy-mm-dd", minDate: '-50y' });
    
    function generar_diploma(id_curso, id_alumno){
        
        var dir = "generardiplomacont?id_curso="+id_curso+"&id_alumno="+id_alumno;        
        window.open(dir);
    }
</script>
<script type="text/javascript">            
        $('.solo-numero').keyup(function (){            
            
            this.value = (this.value + '').replace(/[^0-5]/g, '');
          });
    
    function enviar_datos_alumno(id_alumno, id_user){

        
        var nombre1_edit                = $("#nombre1_edit").val();
        var nombre2_edit                = $("#nombre2_edit").val();
        var apellido1_edit              = $("#apellido1_edit").val();
        var apellido2_edit              = $("#apellido2_edit").val();
        var genero_edit                 = $("#genero_edit").val();
        var estado_civil_edit           = $("#estado_civil_edit").val();
        var f_nacimiento_edit           = $("#f_nacimiento_edit").val();
        var nacionalidad_edit           = $("#nacionalidad_edit").val();

        var lugar_nac_edit              = $("#lugar_nac_edit").val();
        var profesion_edit              = $("#profesion_edit").val();
        var fecha_inscipcion_edit       = $("#fecha_inscipcion_edit").val();
        var email_edit                  = $("#email_edit").val();
        var tipo_documento_edit         = $("#tipo_documento_edit").val();
        var n_documento_edit            = $("#n_documento_edit").val();
        var lugar_expedicion_edit       = $("#lugar_expedicion_edit").val();
        var direccion_edit              = $("#direccion_edit").val();
        var telefono_edit               = $("#telefono_edit").val();
        var empresa_edit                = $("#empresa_edit").val();
        var empresa_telefono_edit       = $("#empresa_telefono_edit").val();
        var cargo_edit                  = $("#cargo_edit").val();
        var tiempo_experiencia_edit     = $("#tiempo_experiencia_edit").val();

        var n_libreta_militar_edit      = $("#n_libreta_militar_edit").val();
        var distrito_militar_edit       = $("#distrito_militar_edit").val();

        if(nombre1_edit == ""){
            toastr.error('El campo nombre1_edit no puede estar vacio.', 'Error');
            return;
        }
        // if(nombre2_edit == ""){
        //     toastr.error('El campo nombre2_edit no puede estar vacio.', 'Error');
        //     return;
        // }
        if(apellido1_edit == ""){
            toastr.error('El campo apellido1_edit no puede estar vacio.', 'Error');
            return;
        }
        // if(apellido2_edit == ""){
        //     toastr.error('El campo apellido2_edit no puede estar vacio.', 'Error');
        //     return;
        // }
        if(genero_edit == ""){
            toastr.error('El campo genero_edit no puede estar vacio.', 'Error');
            return;
        }
        if(estado_civil_edit == ""){
            toastr.error('El campo estado_civil_edit no puede estar vacio.', 'Error');
            return;
        }
        if(f_nacimiento_edit == ""){
            toastr.error('El campo f_nacimiento_edit no puede estar vacio.', 'Error');
            return;
        }
        if(nacionalidad_edit == ""){
            toastr.error('El campo nacionalidad_edit no puede estar vacio.', 'Error');
            return;
        }
        if(lugar_nac_edit == ""){
            toastr.error('El campo lugar_nac_edit no puede estar vacio.', 'Error');
            return;
        }
        if(profesion_edit == ""){
            toastr.error('El campo profesion_edit no puede estar vacio.', 'Error');
            return;
        }
        if(fecha_inscipcion_edit == ""){
            toastr.error('El campo fecha_inscipcion_edit no puede estar vacio.', 'Error');
            return;
        }
        if(email_edit == ""){
            toastr.error('El campo email_edit no puede estar vacio.', 'Error');
            return;
        }
        if(tipo_documento_edit == ""){
            toastr.error('El campo tipo_documento_edit no puede estar vacio.', 'Error');
            return;
        }
        if(n_documento_edit == ""){
            toastr.error('El campo n_documento_edit no puede estar vacio.', 'Error');
            return;
        }
        if(lugar_expedicion_edit == ""){
            toastr.error('El campo lugar_expedicion_edit no puede estar vacio.', 'Error');
            return;
        }
        if(direccion_edit == ""){
            toastr.error('El campo direccion_edit no puede estar vacio.', 'Error');
            return;
        }
        if(telefono_edit == ""){
            toastr.error('El campo telefono_edit no puede estar vacio.', 'Error');
            return;
        }
        if(empresa_edit == ""){
            toastr.error('El campo empresa_edit no puede estar vacio.', 'Error');
            return;
        }
        if(empresa_telefono_edit == ""){
            toastr.error('El campo empresa_telefono_edit no puede estar vacio.', 'Error');
            return;
        }
        if(cargo_edit == ""){
            toastr.error('El campo cargo_edit no puede estar vacio.', 'Error');
            return;
        }
        if(tiempo_experiencia_edit == ""){
            toastr.error('El campo tiempo_experiencia_edit no puede estar vacio.', 'Error');
            return;
        }



        var parametros = {
            id_alumno: id_alumno,
            id_user: id_user,
            nombre1_edit: nombre1_edit,
            nombre2_edit: nombre2_edit,
            apellido1_edit: apellido1_edit,
            apellido2_edit: apellido2_edit,
            genero_edit: genero_edit,
            estado_civil_edit: estado_civil_edit,
            f_nacimiento_edit: f_nacimiento_edit,
            nacionalidad_edit: nacionalidad_edit,
            lugar_nac_edit: lugar_nac_edit,
            profesion_edit: profesion_edit,
            fecha_inscipcion_edit: fecha_inscipcion_edit,
            email_edit: email_edit,
            tipo_documento_edit: tipo_documento_edit,
            n_documento_edit: n_documento_edit,
            lugar_expedicion_edit: lugar_expedicion_edit,
            direccion_edit: direccion_edit,
            telefono_edit: telefono_edit,
            empresa_edit: empresa_edit,
            empresa_telefono_edit: empresa_telefono_edit,
            cargo_edit: cargo_edit,
            tiempo_experiencia_edit: tiempo_experiencia_edit,
            n_libreta_militar_edit: n_libreta_militar_edit,
            distrito_militar_edit: distrito_militar_edit
        };
        $.ajax({
            data: parametros,
            url:  'actualizardatosalumno',
            type: 'get',
            success: function(response){
                if (response == "1") {
                    toastr.success('Se Actualizaron los datos correctamente.', 'Actualización de datos')
                }else{
                    toastr.error('No se han podido actualizar los datos.', 'Error')
                }
                
            }
        });
        

    }
    function enviar_estudios_alumno(id_alumno, id_user){
        var tipo_estudios = $("#tipo_estudios").val();
        var institucion_estudios = $("#institucion_estudios").val();
        var titulo_estudios = $("#titulo_estudios").val();

        if(tipo_estudios == ""){
            toastr.error('El campo Tipo de Estudios no puede estar vacio', "Error");
            return;
        }
        if(institucion_estudios == ""){
            toastr.error('El campo Institución no puede estar vacio', "Error");
            return;
        }
        if(titulo_estudios == ""){
            toastr.error('El campo Título no puede estar vacio', "Error");
            return;
        }

        var parametros = {
            id_alumno: id_alumno,
            id_user: id_user,
            tipo_estudios: tipo_estudios,
            institucion_estudios: institucion_estudios,
            titulo_estudios: titulo_estudios
        };
        $.ajax({
            data: parametros,
            url:  'actualizarestudiosalumno',
            type: 'get',
            success: function(response){
                if (response != "0") {
                    toastr.success('Se Actualizaron los Estudios correctamente.', 'Actualización de Estudios');
                    $("#tipo_estudios").val("");
                    $("#institucion_estudios").val("");
                    $("#titulo_estudios").val("");
                    $("#tabla_estudios_alumno").html(response);

                }else{
                    toastr.error('No se han podido actualizar los Estudios.', 'Error');
                }
                
            }
        });
    }
    function enviar_licencias_alumno(id_alumno, id_user){

        var tipo_licencia = $("#tipo_licencia").val();
        var licencia_numero = $("#licencia_numero").val();
        var licencia_vencimiento = $("#licencia_vencimiento").val();



        if(tipo_licencia == ""){
            toastr.error('El campo Tipo de Licencia no puede estar vacio', "Error");
            return;
        }
        if(licencia_numero == ""){
            toastr.error('El campo Número de licencia no puede estar vacio', "Error");
            return;
        }
        if(licencia_vencimiento == ""){
            toastr.error('El campo Fecha de vencimiento no puede estar vacio', "Error");
            return;
        }
        

        var parametros = {
            id_alumno: id_alumno,
            id_user: id_user,
            tipo_licencia: tipo_licencia,
            licencia_numero: licencia_numero,
            licencia_vencimiento: licencia_vencimiento
        };
        $.ajax({
            data: parametros,
            url:  'actualizarlicenciasalumno',
            type: 'get',
            success: function(response){
                if (response != "0") {
                    toastr.success('Se Actualizaron las licencias correctamente.', 'Actualización de licencias');
                    $("#tipo_licencia").val("");
                    $("#licencia_numero").val("");
                    $("#licencia_vencimiento").val("");
                    $("#tabla_licencias_alumno").html(response);

                }else{
                    toastr.error('No se han podido actualizar las licencias.', 'Error');
                }
                
            }
        });
    }
    
    
        //funcion para guardar evaluacion de salida
function EvaluacionSalida(idalumno, idcurso) {
    
    var notasprofesores = new Array();
    
    var validacion = 1;
        
        @foreach($materias as $mat)
        
            notasprofesores.push($('#pregunta6-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta7-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta8-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta9-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta10-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta11-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta12-'+{{$mat->CoursesInstructors->id}}).val());
            notasprofesores.push($('#pregunta13-'+{{$mat->CoursesInstructors->id}}).val());

        @endforeach
                
    var pregunta14not = $('#pregunta14not').val();
    var pregunta14obs = $('#pregunta14obs').val();
    var pregunta15not = $('#pregunta15not').val();
    var pregunta15obs = $('#pregunta15obs').val();
    var pregunta16not = $('#pregunta16not').val();
    var pregunta16obs = $('#pregunta16obs').val();
    var pregunta17not = $('#pregunta17not').val();
    var pregunta17obs = $('#pregunta17obs').val();
    var pregunta18not = $('#pregunta18not').val();
    var pregunta18obs = $('#pregunta18obs').val();
    var pregunta19not = $('#pregunta19not').val();
    var pregunta19obs = $('#pregunta19obs').val();
    var pregunta20not = $('#pregunta20not').val();
    var pregunta20obs = $('#pregunta20obs').val();
    
    var pregunta21 = $('#pregunta21').val();
    var pregunta22 = $('#pregunta22').val();
        

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "evaluacionsalida",
                data: {idalumno:idalumno, idcurso:idcurso, notasprofesores:notasprofesores, pregunta14not:pregunta14not, pregunta14obs:pregunta14obs, 
                    pregunta15not:pregunta15not, pregunta15obs:pregunta15obs, pregunta16not:pregunta16not, pregunta16obs:pregunta16obs, 
                    pregunta17not:pregunta17not, pregunta17obs:pregunta17obs, pregunta18not:pregunta18not, pregunta18obs:pregunta18obs, 
                    pregunta19not:pregunta19not, pregunta19obs:pregunta19obs, pregunta20not:pregunta20not, pregunta20obs:pregunta20obs, 
                    pregunta21:pregunta21, pregunta22:pregunta22},
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('La evaluacion se ha guardado correctamente', 'Cambios guardados');

            } else {
                toastr.error('No ha sido posible guardar la evaluacion', 'Error');
            }
        }
}
    
    
</script>