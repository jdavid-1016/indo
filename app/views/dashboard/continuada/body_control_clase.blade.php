@foreach($horas as $horas)
    <tr style="text-align:center;" id="table-inst-{{$horas->id}}" >
        <td>{{$horas->id}}</td>
        <td>{{$horas->CoursesSubjects->CoursesInstructors->Users->name}} {{$horas->CoursesSubjects->CoursesInstructors->Users->last_name}}</td>
        <td>{{$horas->date}}</td>
        <td>{{$horas->hour}}</td>
        <td>{{$horas->GeneralClassrooms->classroom}}</td>
        <td>@if($horas->theme == "") <textarea class="form-control" id="tema_control_{{$horas->id}}"></textarea> @else {{$horas->theme}} @endif</td>
        <td>@if($horas->observation_teacher == "") <textarea class="form-control" id="observacion_control_{{$horas->id}}"></textarea> @else {{$horas->observation_teacher}} @endif</td>
        <td>@if($horas->dictated == 0) <a href="#" class="btn btn-info" onclick="guardar_control_clase({{$horas->id}})"><i class="icon-ok"></i></a> @else <i class="icon-ok"></i> @endif</td>
        <!-- <td>@if($horas->verified == 0) <a href="#" class="btn btn-info" onclick="guardar_control_clase({{$horas->id}})"><i class="icon-ok"></i></a> @else <i class="icon-ok"></i> @endif</td> -->
        <td><i class="@if($horas->verified == 0) icon-remove @else icon-ok @endif"></i></td>
        <td>{{$horas->observation_adm}}</td>


    </tr>
@endforeach
<script type="text/javascript">
	function guardar_control_clase(id_control){
		var tema_control = $("#tema_control_"+id_control).val();
		var observacion_control = $("#observacion_control_"+id_control).val();

		if(tema_control == ""){
			toastr.error('El campo Tema no puede estar vacío', 'Error');
			return;
		}
		if(observacion_control == ""){
			toastr.error('El campo Observacion no puede estar vacío', 'Error');
			return;
		}


		var html = $.ajax({
		    type: "GET",
		    url: "guardarcontrolclase",
		    data: {tema_control:tema_control, observacion_control:observacion_control, id_control:id_control},
		    async: false
		}).responseText;

		if (html == 1) {
			toastr.success('Se cargo el control de clase correctamente', 'Cargue de control de clase');
		}else{
			toastr.error('No se ha podido cargar el control de clase', 'Error');
		}
	}
</script>