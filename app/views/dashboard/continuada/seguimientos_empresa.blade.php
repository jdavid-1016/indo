<div class="modal-dialog modal-wide" >
    <div class="modal-content">
    <form class="form" action="#" >
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Seguimientos en la Empresa</h4>
        </div>
        <div class="modal-body form-body">
            <!--  -->
            <div class="row">
               <div class="col-md-12">
                  <div class="form-group" id="frm_comentario">
                     
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h4 class="panel-title">
                              <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_seguimientos">
                              Seguimientos
                              </a>
                           </h4>
                        </div>
                        <div id="collapse_seguimientos" class="panel-collapse in" style="height: auto;">
                            <table class="table top-blue">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Usuario</th>
                                        <th>Comentario</th>
                                        <th>Fecha</th>
                                    </tr>
                                </thead>
                                <tbody class="center">
                                    @foreach($detalle_empresas as $detalle_empresa)
                                    <tr>
                                        <td>
                                            <a href="#" class="pull-left">
                                                <img alt="" style="border-radius:50px;" width="50%"  src="../{{$detalle_empresa->Users->img_min}}">
                                            </a>
                                        </td>
                                        <td>{{$detalle_empresa->Users->name}} {{$detalle_empresa->Users->last_name}}</td>
                                        <td>{{$detalle_empresa->observation}}</td>
                                        <td>{{$detalle_empresa->created_at}}</td>
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                               
                        </div>
                     </div> 

                  </div>
               </div>
            </div>
            <div class="row">
                
                <div class="col-md-12">
                    <div class="form-group has-success"> 
                       <label class="control-label">Nuevo Seguimiento</label>
                       <div class="input-group">
                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                          <textarea  class="form-control" placeholder="Seguimiento" data-required="true" name="name_n" id="observaciones_seg" ></textarea>
                       </div> 
                    </div>
                </div>                
            </div> 

            
        </div>
        <div class="modal-footer">
            <a class="btn btn-success" onclick="ingresar_seguimiento({{$id_empresa}})"><i class="icon-edit"></i> Ingresar Seguimiento</a>
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_metodo">Cerrar</button>
        </div>
  
    </form>   
    </div>
</div>

<script src="/indo/public/js/upload.js"></script>   

<script>
function ingresar_seguimiento(id){

    var f = new Date();
    

    var observaciones = $('#observaciones_seg').val();
    
    if (observaciones == "") {
        toastr.error('El campo Seguimiento esta vacio', 'Error');
        return;
    }
    
    var html = $.ajax({
        type: "GET",
        url: "ingresarseguimientos",
        data: {observaciones:observaciones, id:id},
        async: false
    }).responseText;

    if (parseInt(html) == 1) {

            toastr.success('Se ingreso correctamente el nuevo seguimiento', 'Nuevo Seguimiento');
            var texto = '<div class="note note-success">\n\
            <a href="#" class="pull-left">\n\
            <img alt="" style="border-radius:50px;" width="50%"  src="../{{Auth::user()->img_min}}"></a>\n\
            <span class="text-success">Commentario {{Auth::user()->name}} {{Auth::user()->last_name}}:</span>\n\
            <span class="pull-right">'+f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate() +" "+ f.getHours() +":"+f.getMinutes()+":"+ f.getSeconds()+'</span>\n\
            <p>'+observaciones+'</p></div>';
                
            var texto = '<tr>\n\
                            <td>\n\
                            <a href="#" class="pull-left"><img alt="" style="border-radius:50px;" width="50%"  src="../{{Auth::user()->img_min}}"></a>\n\
                            </td>\n\
                            <td>{{Auth::user()->name}} {{Auth::user()->last_name}}</td>\n\
                            <td>'+observaciones+'</td>\n\
                            <td>'+f.getFullYear()+ "/" + (f.getMonth() +1) + "/" + f.getDate() +" "+ f.getHours() +":"+f.getMinutes()+":"+ f.getSeconds()+'</td>\n\
                        </tr>';
                $('#observaciones_seg').val("");
                $("#collapse_seguimientos").append(texto);

    } else {
        toastr.error('No ha sido posible Ingresar El Seguimiento', 'Error');
    }

}
</script>