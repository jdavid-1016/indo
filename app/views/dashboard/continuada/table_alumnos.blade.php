<a class="btn btn-default">Total Registros: {{count($num)}}</a><br><br>
<table class="table top-blue" data-target="soporte/callshopping/">
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>No Documento</th>
            <th>F. Ingreso</th>
            <th>Origen</th>
        </tr>
    </thead>
    <tbody>
    @foreach($alumnos as $alum)
        <tr class=" " style="" id="" >
            <td class="td_center">
                <a class=" btn btn-default" style="" data-target="#ajax2" id="{{$alum->id}}" data-toggle="modal" onclick='cargarDetalleAlumno($(this).attr("id"));return false;'><i class="icon-edit"></i> {{$alum->id}}</a>
            </td>
            <td>
                <a href="#" class="pull-left nameimg" title="">
                <img alt="" style="border-radius:50px;" width="50" height="50" src="../{{$alum->img_min}}">
                </a>
			</td>
            <td class="td_center">{{$alum->name}} {{$alum->name2}}</td>
            <td class="td_center">{{$alum->last_name}} {{$alum->last_name2}}</td>
            <td class="td_center">{{$alum->document}}</td>
            <td class="td_center">{{$alum->created_at}}</td>
            <td class="td_center">
                <a class=" btn btn-default" id="{{$alum->id}}" onclick="cargar_hoja_vida($(this).attr(&quot;id&quot;));return false;"><i class="icon-file"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div class="pagination">
    {{$pag->appends(array("cod" => Input::get('cod'),"nomb" => Input::get('nomb'),"doc" => Input::get('doc')))->links()}}
</div>
<!-- imagesLoaded -->
<script type="text/javascript" src="{{ URL::to("js/imagesloaded.pkgd.min.js")}}"></script>
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!-- Tipped -->
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />

    <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
<img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
    </div>

<script type='text/javascript'>
    $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });    
  });
</script>   