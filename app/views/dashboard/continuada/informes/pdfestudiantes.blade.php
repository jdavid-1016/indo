<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Informe</title>
<link href="{{  URL::to("css/pdf.css")}}" rel="stylesheet" type="text/css"/>
</head>
 
<body>
    <table class="table" style="border: 2px solid black" width="100%">     
  <tr valign="middle"> 
      <th rowspan="3" style="border: 2px solid black"><img src="assets/img/logotuto.png" width="60px"></th>
    <th align="center" rowspan="3" style="border: 2px solid black">INFORME DE ESTUDIANTES POR CURSO</th>
    <th colspan="2" style="border: 2px solid black">CODIGO: 256156</th>
  </tr>
  <tr align="center" valign="middle"> 
      <th colspan="2" style="border: 2px solid black">FECHA ACTUALIZACION: 19/03/2015</th>
    
  </tr>
  <tr align="center" valign="middle" > 
    <td style="border: 2px solid black">PAGINA: 1</td>
    <td style="border: 2px solid black">REVISION: 1</td>
    
  </tr>

</table>
    <br>
    <h2 class="center">Informe de estudiantes por curso</h2>
    <br>
    <table align="center" class="table table-bordered" width="100%">
    <tr class="cabecera">
    <th scope="col">Curso</th>
    <th scope="col">No. de estudiantes</th>
  </tr>
 @foreach($datos as $dat)
  <tr align="center">
    <td>{{$dat->name_curso}} </td>
    <td>{{ $dat->dato }}</td>
  </tr>
 <?php $cont=$cont+$dat->dato; ?>
 @endforeach
  <tr>
    <th scope="row">TOTAL</th>
    <td align="center"><strong>{{ $cont }}</strong></td>
  </tr>
</table>
 
</body>
 
</html>