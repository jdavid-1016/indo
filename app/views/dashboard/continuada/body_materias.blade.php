@foreach($materias as $mat)
    <tr style="text-align:center;" id="table-inst-{{$mat->id}}" >
        <td>{{$mat->CoursesInstructors->Users->last_name}} {{$mat->CoursesInstructors->Users->name}}</td>
        <td>$ <?php echo number_format($mat->price); ?></td>
        <td>
            <a href="#" class="btn btn-default">
                <img alt="" style="border-radius:50px;" width="50" height="50"  src="../{{$mat->CoursesInstructors->Users->img}}">
            </a>
        </td>
        <td><a href="#" class="btn btn-danger" onclick="eliminar_materia({{$mat->id}})"><i class="icon-remove"></i></a></td>

    </tr>
@endforeach