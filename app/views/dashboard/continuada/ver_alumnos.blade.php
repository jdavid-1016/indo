<div class="page-content" id="adminhelpdesk">
   	<div class="row">
		<div class="portlet">
		   	<div class="portlet-title">
		      	<div class="caption"><i class="icon-reorder"></i>Alumnos</div>
		   	</div>
		   	<div class="portlet-body">
			    <ul  class="nav nav-tabs">
			       <li class=""><a href="alumnos">Ingresar Alumnos</a></li>
			       <li class="active"><a href="veralumnos" >Ver Alumnos</a></li>
			    </ul>
                <div>
                    <form class="form-inline" action="verequipos" method="get">
                        <div class="search-region">
                            <div class="form-group">
                                <input type="text" class="form-control" id="cod" placeholder="Código" value="{{Input::get('id')}}" onkeyup="tabla_lista_alumnos()">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="nomb" placeholder="Nombres" value="{{Input::get('id')}}" onkeyup="tabla_lista_alumnos()">
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="doc" placeholder="Documento" value="{{Input::get('id')}}" onkeyup="tabla_lista_alumnos()">
                            </div>
                        </div>
                    </form> 
                </div>
                <div class="table-responsive" id="tabla_ver_alumnos">
                    @include('dashboard.continuada.table_alumnos')
                </div>			
			      
			</div>
   		
  	</div>
</div>
    <div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">
                        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
                     </div>
    </div>
<script type="text/javascript">
    function cargarDetalleAlumno(id, idcurso){
        var parametros = {
            "id": id, "idcurso":idcurso
        };
        $.ajax({
            data: parametros,
            url:  'detallealumno',
            type: 'get',

            success: function(response){
                $("#ajax2").html(response);
            }
        });
    }
    function cargar_hoja_vida(id){

        window.open('pdfalumno?id='+id)
    }
    function tabla_lista_alumnos(){

        var cod   = $("#cod").val();
        var nomb  = $("#nomb").val();
        var doc   = $("#doc").val();
        $.ajax({
            type: "GET",
            url:  "veralumnos",
            data: { cod:cod, nomb:nomb, doc:doc, filtro:1}
        })
        .done(function(data) {
            $("#tabla_ver_alumnos").html(data); 
        }); 
    }
   </script>