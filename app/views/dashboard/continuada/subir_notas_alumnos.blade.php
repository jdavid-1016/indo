<table class="table top-blue" data-target="soporte/callshopping/">
    <thead>
        <tr>
        	<th>Alumno</th>
        	<th></th>
            <th>Nota</th>
        </tr>
    </thead>
    <tbody>
    <?php $i = 0; ?>
    @foreach($alumnos as $alumno)
        <tr class=" " style="" id="" >
            <td class="td_center">{{$alumno->name}} {{$alumno->name2}} {{$alumno->last_name}} {{$alumno->last_name2}}</td>
            <td class="td_center">
                <a href="#" class="pull-left nameimg" title="">
                    <img alt="" style="border-radius:50px;" width="50" height="50" src="../{{$alumno->img_min}}">
                </a>
            </td>
            @if(count($notas2) != "")
            <?php $j = 0; ?>
                @foreach($notas2 as $nota2)

                <td class="" >
                    <input type="text" id="valor_nota_{{$notas[$i][$j]['id']}}" class="form-control input-medium" value="{{$notas[$i][$j]['value']}}" onclick="cambiar_nota({{$notas[$i][$j]['id']}})">
                </td>
                
                <?php $j++; ?>
                @endforeach
            @else
                <td class="" >
                    <input type="text" id="valor_nota_{{$alumno->id}}" class="form-control input-medium" value="100" >
                </td>
            @endif
        </tr>
        <?php $i++; ?>
    @endforeach
    </tbody>
</table>
<div class="form-group">
    <div class="col-md-3">
        <a class=" btn btn-success" id="" onclick='guardar_notas(); return false;'><i class="icon-ok"></i> Guardar</a></td>
    </div>
</div>

<script type="text/javascript">
    function guardar_notas(){
        var id_alum = new Array();
        var valor   = new Array();
        var i = 0;

        @foreach($alumnos as $alumno)

            id_alum[i]  = {{$alumno->id}};
            valor[i]    = $("#valor_nota_{{$alumno->id}}").val();
            i++;
        @endforeach

        
        var curso = {{$curso}};
        var html = $.ajax({
            type: "POST",
            url: "guardarnotas",
            data: {id_alum:id_alum, valor:valor, curso:curso},
            async: false
        }).responseText;
            
        if (html == 1) {
            toastr.success('Se ingreso correctamente las Notas', 'Cargue de Notas');
        }else{
            toastr.error('No ha sido posible ingresar las notas', 'Error');
        }
    }
</script>