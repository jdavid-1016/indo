<div class="opacidadcal" style="display:none;">
      <div class="padrecal" id="padrecal">
          
          <div class="alert alert-block alert-info fade in pendcal" id="pendcal">
                        <h3 class="alert-heading" style="text-align:center;"><b>Calificaciones Pendientes</b></h3>
                        </br>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px;">
                           Usted tiene soportes pendientes por calificar, le solicitamos que los califique para que podamos mejorar nuestra atencion.
                        </p>
                        <p>
                            <img src="{{ URL::to("assets/img/logotuto.png") }}" style="width: 100%; height: 25%;">
                        </p>
                        </br>
                        <p>
                           <a class="btn btn-info" href="/indo/public/my_supports_history">Ir a Calificar</a>
                           <a class="btn btn-danger" href="#" onclick="CerrarCal()">Cerrar</a>
                        </p>
                     </div>
   </div>
</div>

<div class="opacidadasp" style="display:none;">
      <div class="padreasp" id="padreasp">
          
          <div class="alert alert-block alert-info fade in pendasp" id="pendasp">
                        <h3 class="alert-heading" style="text-align:center;"><b>Tareas Pendientes</b></h3>
                        </br>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px;">
                           Usted tiene algunos seguimientos programados para el dia de hoy.
                        </p></br>
                        <div id="contenidoasp">
                        
                        </div>
                        </br>
                        <p>
                           <a class="btn btn-info" href="/indo/public/aspirantes/prospectos">Ir a gestionar</a>
                           <a class="btn btn-danger" href="#" onclick="CerrarAsp()">Cerrar</a>
                        </p>
                     </div>
   </div>
</div>
<div class="opacidadfec" style="display:none;">
      <div class="padrefec" id="padrefec">
          
          <div class="alert alert-block alert-info fade in pendfec" id="pendfec">
                        <h3 class="alert-heading" style="text-align:center;"><b>Tareas Pendientes</b></h3>
                        </br>
                        <p style="text-align:justify; border-radius: 5px; padding: 5px;">
                           Usted tiene algunas fechas de entrega de documentos, programadas para el dia de hoy.
                        </p></br>
                        <div id="contenidofec">
                        
                        </div>
                        </br>
                        <p>
                           <a class="btn btn-info" href="/indo/public/aspirantes/pendientes">Ir a gestionar</a>
                           <a class="btn btn-danger" href="#" onclick="CerrarFec()">Cerrar</a>
                        </p>
                     </div>
   </div>
</div>
<!-- BEGIN FOOTER -->
   
   <input type="hidden" id="id_unico_loguin" value="<?php echo Auth::user()->id; ?>">
   <div class="footer">
      <div class="footer-inner">
         2015 &copy; SGI Indoamericana.
      </div>
      <div class="footer-tools">
         <span class="go-top">
         <i class="icon-angle-up"></i>
         </span>
      </div>
   </div>
   <div id="sound">
       </div>   
   <!-- END FOOTER -->
   <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
   <!-- BEGIN CORE PLUGINS -->   
   <!--[if lt IE 9]>
   <script src="assets/plugins/respond.min.js"></script>
   <script src="assets/plugins/excanvas.min.js"></script>
   <![endif]-->
   @if($submenu_activo == "Estadísticas" || $submenu_activo == "Informes")
   
   @else   
     <script src="{{ URL::to("assets/plugins/jquery-1.10.2.min.js")}}" type="text/javascript"></script>
   @endif
   <script src="{{ URL::to("assets/plugins/jquery-migrate-1.2.1.min.js")}}" type="text/javascript"></script>   
   <!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
   <script src="{{ URL::to("assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/plugins/bootstrap/js/bootstrap.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/bootstrap-hover-dropdown/twitter-bootstrap-hover-dropdown.min.js")}}" type="text/javascript" ></script>
   <script src="{{ URL::to("assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.blockui.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.cookie.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/uniform/jquery.uniform.min.js")}}" type="text/javascript" ></script>
   <!-- END CORE PLUGINS -->
   <!-- BEGIN PAGE LEVEL PLUGINS -->
   
   <script src="{{ URL::to("assets/plugins/jquery.peity.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.pulsate.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-knob/js/jquery.knob.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/flot/jquery.flot.resize.js")}}" type="text/javascript"></script>
   <!-- <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/moment.min.js")}}" type="text/javascript"></script>
   // <script src="{{ URL::to("assets/plugins/bootstrap-daterangepicker/daterangepicker.js")}}" type="text/javascript"></script>      -->
   <script src="{{ URL::to("assets/plugins/gritter/js/jquery.gritter.js")}}" type="text/javascript"></script>
   <!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
   <script src="{{ URL::to("assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/plugins/jquery.sparkline.min.js")}}" type="text/javascript"></script>  
   <script type="text/javascript" src="{{ URL::to("assets/plugins/ckeditor/ckeditor.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->   
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/bootstrap-toastr/toastr.min.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/ui-toastr.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/app.js")}}" type="text/javascript"></script>
   <script src="{{ URL::to("assets/scripts/index.js")}}" type="text/javascript"></script>  
   <script src="{{ URL::to("assets/scripts/tasks.js")}}" type="text/javascript"></script>  
   <!-- BEGIN PAGE LEVEL SCRIPTS -->
   <script src="{{ URL::to("assets/plugins/fuelux/js/tree.min.js")}}"></script>  
   <!-- END PAGE LEVEL SCRIPTS -->     
   <script src="{{ URL::to("assets/scripts/ui-tree.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/bootstrap-fileupload/bootstrap-fileupload.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/form-components.js")}}"></script>
   <script language="javascript" src="{{ URL::to("js/fancywebsocket.js")}}"></script>
   <script src="{{ URL::to("js/app.js")}}"></script>
   <script src="{{ URL::to("js/upload.js")}}"></script>
   <script src="{{ URL::to("js/validar.js")}}"></script>
   <script src="{{ URL::to("js/compras.js")}}"></script>
   <script src="{{ URL::to("js/continuada.js")}}"></script>
   <script src="{{ URL::to("js/equipos.js")}}"></script>
   <script src="{{ URL::to("js/aspirantes.js")}}"></script>
   <script src="{{ URL::to("js/angular.min.js")}}"></script>
   <script src="{{ URL::to("js/documentacion.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/jquery-mixitup/jquery.mixitup.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/fancybox/source/jquery.fancybox.pack.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/select2/select2.min.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/jquery.dataTables.js")}}"></script>
   <script type="text/javascript" src="{{ URL::to("assets/plugins/data-tables/DT_bootstrap.js")}}"></script>
   <script src="{{ URL::to("assets/scripts/table-editable.js")}}"></script>
   <!-- END PAGE LEVEL PLUGINS -->
  <script src="{{ URL::to("assets/scripts/portfolio.js")}}"></script>
  <script src="{{ URL::to("js/jQuery_Mask/src/jquery.mask.js")}}"></script>
  <script src="{{ URL::to("assets/scripts/see-more.js")}}"></script>
   <!-- END PAGE LEVEL SCRIPTS -->
   
   <script src="../js/tragator/fm.tagator.jquery.js"></script>

   <!-- tipped -->
   <script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>
   <!--<script src="{{ URL::to("assets/dompdf.js")}}"></script>-->
   
   <!-- graficos jquery -->
   <script src="../Highcharts-4.1.4/js/highcharts.js"></script>
   <script src="../Highcharts-4.1.4/js/modules/exporting.js"></script>



 <script type="text/javascript">
  $(document).ready(function () {
      setInterval(function () {
          var iScroll = $('#scroller').scrollTop();
          iScroll = iScroll + 100;
          $('#scroller').animate({
              scrollTop: iScroll
          }, 1000);
      }, 2000);
  });
  
 </script>

   
   <script>
      jQuery(document).ready(function() {
         App.init(); // initlayout and core plugins
         Portfolio.init();
         TableEditable.init();
         Index.init();
         Index.initJQVMAP(); // init index page's custom scripts
         Index.initCalendar(); // init index page's custom scripts
         Index.initCharts(); // init index page's custom scripts
         Index.initChat();
         Index.initMiniCharts();
         Index.initPeityElements();
         Index.initKnowElements();
         Index.initDashboardDaterange();         
         Tasks.initDashboardWidget();
         UITree.init();
         UIToastr.init();
         FormComponents.init();
      });
      Tipped.create('.informacion', { 
        size: 'medium',
        skin: 'light',
        maxWidth: 300
        
      });
      
      Tipped.create('.nameimg', { 
        size: 'medium',
        skin: 'light',
        maxWidth: 300
        
      });
   </script>
   <script type="text/javascript">
    //ingresamos en base de datos si el usuario tiene calificaciones pendientes
   $.ajax({
        type: "GET",
        url: "/indo/public/actCalificaciones",
        data: { cont:"1"}
        })
        .done(function( data ) {            
        if (data==1){
            $('.opacidadcal').fadeIn(0);
            $('#pendcal').fadeIn(0);
        }
    });            
    
    function CerrarCal(){
           
           $('#pendcal').fadeOut(0);
           $('.opacidadcal').fadeOut(1000);
           
       }
       
 //ingresamos en base de datos si el usuario tiene Tareas de aspirantes pendientes
 var pend_seguimientos ="";
 var pend_fechas ="";
 
   $.ajax({
        type: "GET",
        url: "/indo/public/actAspirantes",
        data: { cont:"1"}
        })
        .done(function( data ) {
            pend_seguimientos = data;
            
        if (data!=="0"){
            $('#contenidoasp').html(data);
            $('.opacidadasp').fadeIn(0);
            $('#pendasp').fadeIn(0);
        }
    });
    
    $.ajax({
        type: "GET",
        url: "/indo/public/actPendingfechas",
        data: { cont:"1"}
        })
        .done(function( data ) {
            pend_fechas = data;
            
        if (data!=="0"){
            $('#contenidofec').html(data);
            $('.opacidadfec').fadeIn(0);
            $('#pendfec').fadeIn(0);
        }
    });
       
       function CerrarAsp(){
           
           $('#pendasp').fadeOut(0);
           $('.opacidadasp').fadeOut(1000);
           
       }
       
       function CerrarFec(){
           
           $('#pendfec').fadeOut(0);
           $('.opacidadfec').fadeOut(1000);
           
       }
       
       
   $(document).ready (function(){  
                      
       $('.myCarousel').carousel()
       
      $('.delete').click (function(){

         if (confirm("¿Esta seguro que desea eliminar un usuario?")) {
            var id= $(this).attr ("title");
            document.location.href='users/delete/'+id;
         }
      });

      $('[name="escalar"].escalar').click(function() {
        if($(this).is(':checked')) {
          alert('Se hizo check en el checkbox.');
        } else {
          alert('Se destildo el checkbox');
        }
      });                  
         
   });      
</script>
<script>
$(document).ready(function(){
   $('.edit').click(function(){

      $('[name=user]').val($(this).attr ('id'));

      var faction ="<?php echo URL::to('user/getuser/data'); ?>";

      var fdata = $('#val').serialize();
         $('#load').show();
      $.post(faction, fdata, function(json){
         if (json.success) {
            $('#formEdit input[name="name_edit"]').val(json.name);
            $('#formEdit input[name="date_edit"]').val(json.date);
            $('#formEdit input[name="phone_edit"]').val(json.phone);
            $('#formEdit input[name="username_edit"]').val(json.username);
         }
      });
   });
}
</script>
   <!-- END JAVASCRIPTS -->    
</body>
<!-- END BODY -->
</html>
<!--
Mostrar alerta cuando se sube el archivo de excel de proveedores
-->
@if(Input::get('ok'))
<script>
    toastr.success('Su archivo ha sido subido correctamente', 'Archivo Subido');
</script>
@endif
<!--
Mostrar alerta cuando se sube el archivo de excel de proveedores
-->