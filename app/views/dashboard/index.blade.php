   <!-- incluimos la carpeta de estilos que se ecuentra en la carpeta includes -->
   @include('dashboard.head')
   <!-- END THEME STYLES -->
<?php
$user = Users::find(Auth::user()->id);
$notiadm =count(Supports::where('support_status_id', 1)->get());
$my_id = Auth::user()->id;
$my_profile = Users::find($my_id);

foreach ($my_profile->Profiles as $perfil) {
    $my_process = $perfil->Processes->id;
}
$sql = "SELECT shoppings.id, shopping_statuses.name as estado, shopping_statuses.description as description, shoppings.users_id, shoppings.shopping_statuses_id, shoppings.justification, shoppings.comment, shoppings.created_at, processes.name AS proceso, users.name, users.last_name FROM shoppings JOIN profiles_users ON (shoppings.users_id = profiles_users.users_id) JOIN shopping_statuses ON (shopping_statuses.id = shoppings.shopping_statuses_id) JOIN profiles ON (profiles_users.profiles_id = profiles.id) JOIN processes ON (processes.id = profiles.processes_id) JOIN users ON (users.id = shoppings.users_id) WHERE shoppings.shopping_statuses_id = 1 AND processes.id='".$my_process."' ORDER BY shoppings.id DESC";
$notiaapr = count(DB::select($sql));

$notiadmcomp =count(Shoppings::where('shopping_statuses_id', 3)->get());
$notigestcomp =count(Shoppings::where('shopping_statuses_id','<>' ,1)
   ->where('shopping_statuses_id','<>' ,3)
   ->where('shopping_statuses_id','<>' ,5)
   ->where('shopping_statuses_id','<>' ,6)
   ->where('shopping_statuses_id','<>' ,7)
   ->get());

//$notiaapr =0;
$notiate = count(SupportTraces::join('supports', 'supports.id', '=','supports_id')
		->where('supports.support_status_id', '<>','2')
		->where('supports.support_status_id', '<>', '3')
      ->where('supports.support_status_id', '<>', '6')
      ->where('consecutive', '<>', '1')

      
      
		->where('user_id', '=', $user->id)
		->get());

$notiadmpag =count(Payments::where('payments_statuses_id', 1)->get());
$noticaupag =count(Payments::where('payments_statuses_id', 2)->get());
$notiverpag =count(Payments::where('payments_statuses_id', 4)->get());
$notitespag =count(Payments::where('payments_statuses_id', 5)->get());


// consulta pra notificaciones de Actividades
$notiveractiv =count(
   DB::table('tasks')
   ->where('tasks.users_id', Auth::user()->id)
   ->where('tasks.tasks_statuses_id', '<>', 2)
   ->where('tasks.tasks_statuses_id', '<>', 4)
   ->get()
);
$notimisactiv =count(
   DB::table('tasks_has_users')
   ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
   ->where('tasks_has_users.users_id', Auth::user()->id)
   ->where('tasks_has_users.status', 1)
   ->where('tasks.tasks_statuses_id', '<>', 2)
   ->where('tasks.tasks_statuses_id', '<>', 4)
   ->get()
);
?>
   <!-- END HEADER -->
   <div class="clearfix"></div>
   <!-- BEGIN CONTAINER -->
   <div class="page-container">
   <!-- incluimos la carpeta de estilos que se ecuentra en la carpeta includes -->
   @include('dashboard.sidebar')
   <!-- END THEME STYLES -->
   <!-- incluimos la carpeta de estilos que se ecuentra en la carpeta includes -->
   @include($container)
   <!-- END THEME STYLES -->
   </div>
   <!-- END CONTAINER -->
    <!-- incluimos la carpeta de estilos que se ecuentra en la carpeta includes -->
   @include('dashboard.footer')
   <!-- END THEME STYLES -->
  