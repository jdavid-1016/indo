<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   
  
      <input type="hidden" ng-model="support_id" value="{{$data->id}}" id="id_user">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Editar Usuario</h4>
      </div>
     <div class="modal-body">
         <form role="form" action="envio" method="post">
             <div class="form-body">
                 <div class="form-group">
                     <label>Nombre</label>
                     <input type="text" class="form-control" id="name" required=""  ng-model="name" value="{{$data->name}}">
                 </div>
                 <div class="form-group">
                     <label>Segundo Nombre</label>
                     <input type="text" class="form-control" id="name2" required=""  ng-model="name2" value="{{$data->name2}}">
                 </div>
                 <div class="form-group">
                     <label>Primer apellido</label>
                     <input type="text" class="form-control" id="last_name" required=""  ng-model="last_name" value="{{$data->last_name}}">
                 </div>
                 <div class="form-group">
                     <label>Segundo apellido</label>
                     <input type="text" class="form-control" id="last_name2" required=""  ng-model="last_name2" value="{{$data->last_name2}}">
                 </div>
                 <div class="form-group">
                     <label>Correo Institucional</label>
                     <input type="text" class="form-control" id="email_institutional" required=""  ng-model="email_institutional" value="{{$data->email_institutional}}">
                 </div>
                 <div class="form-group">
                     <label>Correo Personal</label>
                     <input type="text" class="form-control" id="email" required=""  ng-model="email" value="{{$data->email}}">
                 </div>
                 <div class="form-group">
                     <label>Celular</label>
                     <input type="text" class="form-control" id="cell_phone" required=""  ng-model="cell_phone" value="{{$data->cell_phone}}">
                 </div>
                 <div class="form-group">
                     <label>Teléfono</label>
                     <input type="text" class="form-control" id="phone" required=""  ng-model="phone" value="{{$data->phone}}">
                 </div>
                 <div class="form-group">
                     <label>direccion</label>
                     <input type="text" class="form-control" id="direccion" required=""  ng-model="address" value="{{$data->address}}">
                 </div>

                 <div class="form-group">
                     <label>Tipo Documento</label>
                     <select class="form-control" id="type_document" required=""  ng-model="type_document">
                         <option value=""></option>                        
                        <option value="CC" @if($data->type_document == "CC") selected="" @endif>Cedula de Ciudadania</option>
                         <option value="TI" @if($data->type_document == "TI") selected="" @endif>Tarjeta de Identidad</option>
                         <option value="CE" @if($data->type_document == "CE") selected="" @endif>Cedula de Extanjería</option>
                     </select>
                 </div>
                 <div class="form-group">
                     <label>N° Documento</label>
                     <input type="text" class="form-control" id="document" required=""  ng-model="document" value="{{$data->document}}">
                 </div>
                 <div class="form-group">
                     <label>Ciudad {{$data->name}}</label>
                     <select class="form-control" id="city_id" required=""  ng-model="city_id">
                     @foreach($cities as $city)
                        @if($data->city_id == $city->id)
                            <option ng-repeat="city in cityes" value="{{$city->id}}" selected="">{{$city->name}}</option>
                        @else
                            <option ng-repeat="city in cityes" value="{{$city->id}}" >{{$city->name}}</option>
                        @endif
                     @endforeach
                         
                     </select>
                 </div>
                 @foreach ($data->Profiles as $perfil)
                 <?php 
                  $perfil_user = $perfil->id; 
                  $process_user = $perfil->processes_id; 
                ?>
                 @endforeach
                <!--  <div class="form-group">
                     <label>Proceso: </label>
                     <select id="edit_process" class="form-control" id="proceso_perfil">
                     @foreach($processes as $process)
                        <option value='{{$process->id}}' @if($process_user == $process->id) selected="" @endif>{{$process->name}}</option>
                     @endforeach 
                     </select>
                 </div> -->
                 <!-- <div class="form-group" id="perfil">
                     <label>Cargo:</label>
                      <select id="profile" class="form-control" id="profile_user" ng-model="profile_user">
                      @foreach($profiles as $profile)      
                          <option value='{{$profile->id}}' @if($perfil_user == $profile->id) selected="" @endif>{{$profile->profile}}</option>
                      @endforeach 
                      </select>
                 </div> -->
             </div>
     </div>   
      <div class="modal-footer">
         <button type="button" class="btn btn-success" onclick="actualizar_usuario()">Guardar</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
  
   
   </div>
</div>

<script type="text/javascript">
    function actualizar_usuario(){
        var name = $("#name").val();
            if(name == ""){
                toastr.error('Error el campo Primer Nombre esta vacio', 'Error');
                return;
            }
        var name2 = $("#name2").val();
            if(name2 == ""){
                toastr.error('Error el campo Segundo Nombre esta vacio', 'Error');
                return;
            }
        var last_name = $("#last_name").val();
            if(last_name == ""){
                toastr.error('Error el campo Primer Apellido esta vacio', 'Error');
                return;
            }
        var last_name2 = $("#last_name2").val();
            if(last_name2 == ""){
                toastr.error('Error el campo Segundo Apellido esta vacio', 'Error');
                return;
            }
        var email_institutional = $("#email_institutional").val();
            if(email_institutional == ""){
                toastr.error('Error el campo Correo Institucional esta vacio', 'Error');
                return;
            }
        var email = $("#email").val();
            if(email == ""){
                toastr.error('Error el campo Correo esta vacio', 'Error');
                return;
            }
        var cell_phone = $("#cell_phone").val();
            if(cell_phone == ""){
                toastr.error('Error el campo Celular esta vacio', 'Error');
                return;
            }
        var phone = $("#phone").val();
            if(phone == ""){
                toastr.error('Error el campo Teléfono esta vacio', 'Error');
                return;
            }
        var direccion = $("#direccion").val();
            if(direccion == ""){
                toastr.error('Error el campo Direccion esta vacio', 'Error');
                return;
            }
        var type_document = $("#type_document").val();
            if(type_document == ""){
                toastr.error('Error el campo Tipo de Documento esta vacio', 'Error');
                return;
            }
        var document = $("#document").val();
            if(document == ""){
                toastr.error('Error el campo N° Documento esta vacio', 'Error');
                return;
            }
        var city_id = $("#city_id").val();
            if(city_id == ""){
                toastr.error('Error el campo Ciudad esta vacio', 'Error');
                return;
            }
        var proceso_perfil = $("#proceso_perfil").val();
            if(proceso_perfil == ""){
                toastr.error('Error el campo Proceso esta vacio', 'Error');
                return;
            }
        var profile_user = $("#profile_user").val();
            if(profile_user == ""){
                toastr.error('Error el campo Cargo esta vacio', 'Error');
                return;
            }
        var id_user = $("#id_user").val();
            if(profile_user == ""){
                toastr.error('Error el campo id_usuario esta vacio', 'Error');
                return;
            }

        var parametros = {
           "name": name,
           "name2": name2,
           "last_name": last_name,
           "last_name2": last_name2,
           "email_institutional": email_institutional,
           "email": email,
           "cell_phone": cell_phone,
           "phone": phone,
           "direccion": direccion,
           "type_document": type_document,
           "document": document,
           "city_id": city_id,
           "proceso_perfil": proceso_perfil,
           "profile_user": profile_user,
           "id_user": id_user
        };
        $.ajax({
           data: parametros,
           url:  'actualizarusuario',
           type: 'get',

           success: function(response){
                 toastr.success('Se actualizó la información correctamente', 'Actualización de datos');
                 $('#close_modal').click();
                 
           }
        });
    }
</script>