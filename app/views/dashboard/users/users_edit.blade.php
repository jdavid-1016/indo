<div class="modal-dialog" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="kaldsjf" ng-submit="adminSupport()">
  
      <input type="hidden" ng-model="support_id" value="{{$data->id}}" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Actualizar solicitud de soporte técnico</h4>
      </div>
      <div class="modal-body form-body">   
         <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <label class="control-label">Nombres de usuario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$data->id}} {{$data->name}} {{$data->name2}} {{$data->last_name}} {{$data->last_name2}}">
                  </div> 
               </div>
            </div>      
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Perfil Actual</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="profile" class="form-control" ng-model="responsible" required id="responsible" disabled="">
                        @foreach($data->Profiles as $perfil)
                           <option>{{$perfil->id}} {{$perfil->profile}}</option>
                        @endforeach
                     </select>   
                  </div> 
               </div>
            </div>
         </div>
         <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Submenus Activos</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     @foreach ($data->Profiles as $perfil)
                        @foreach($perfil->submenus as $submenu)                  
                           <input readonly type="text" class="form-control" placeholder="" data-required="true" value="{{$submenu->submenu}}">
                        @endforeach
                     @endforeach
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Agregar Submenu</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     @foreach ($submenus as $submenu)
                           {{$submenu->id}} {{$submenu->submenu}}<br>
                     @endforeach
                     
                  </div> 
               </div>
            </div>
               
         </div>   
         <div class="row">
            <div class="col-md-4">
               <div class="form-group">
                  <label class="control-label">Estado del Usuario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <select id="status" class="form-control" ng-model="status" id="status" disabled="">
                        
                        <option value="">
                        @if($data->status == 1 )
                        Activo 
                        @else 
                        Inactivo 
                        @endif</option>
                        
                     </select>
                  </div> 
               </div>
            </div>
         </div>
         <!-- <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                  <label class="control-label">Comentario</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <textarea rows="2"  class="form-control" ng-model="observation" id="observation"></textarea>
                  </div> 
               </div>
            </div>      
         </div> -->  
         <div class="row">
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Creado</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-time"></i></span>
                     <input readonly type="text" class="form-control" value="{{$data->created_at}}">
                  </div> 
               </div>
            </div>
         </div> 
      </div>   
      <div class="modal-footer">
         <!-- <button type="button" class="btn btn-success" onclick="asing_support()">Guardar</button> -->
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      </div>
  
   </form>   
   </div>
</div>