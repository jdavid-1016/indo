<link href="{{  URL::to("css/multi-select.css")}}" media="screen" rel="stylesheet" type="text/css" />
<link href="{{  URL::to("css/theme.css")}}" media="screen" rel="stylesheet" type="text/css" />
<link href="{{  URL::to("css/application.css")}}" media="screen" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.5/angular.min.js"></script>

<div class="page-content" ng-controller="TodoCtrl" ng-app>

    <!-- BEGIN PAGE CONTENT-->
    <a class="btn btn-default" href="usuarios"><i class="icon-eye-open"></i> Ver Usuarios</a>
    <a class="btn btn-default" onclick="form_createusers()"><i class="icon-plus"></i> Crear Usuario</a>
    <a data-toggle="modal" href="#wide2" class="btn btn-default"><i class="icon-plus"></i> Crear Perfil</a>
    <a data-toggle="modal" href="#wide3" class="btn btn-default"><i class="icon-remove"></i> Eliminar permisos</a>
    <a data-toggle="modal" href="#wide4" class="btn btn-default"><i class="icon-plus"></i> Agregar permisos</a>
    <div class="modal fade" id="wide" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog  modal-wide">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="icon-plus"></i>  Agregar Usuarios</h4>
                </div>
                <div class="modal-body">
                    <form role="form" ng-submit="addNew()">
                        <div class="form-body">
                            <div class="form-group">
                                <label>Nombre</label>
                                <input type="text" class="form-control" name="name" required=""  ng-model="name">
                            </div>
                            <div class="form-group">
                                <label>Segundo Nombre</label>
                                <input type="text" class="form-control" name="name2" required=""  ng-model="name2">
                            </div>
                            <div class="form-group">
                                <label>Primer apellido</label>
                                <input type="text" class="form-control" name="last_name" required=""  ng-model="last_name">
                            </div>
                            <div class="form-group">
                                <label>Segundo apellido</label>
                                <input type="text" class="form-control" name="last_name2" required=""  ng-model="last_name2">
                            </div>
                            <div class="form-group">
                                <label>Correo Institucional</label>
                                <input type="text" class="form-control" name="email_institutional" required=""  ng-model="email_institutional">
                            </div>
                            <div class="form-group">
                                <label>Correo Personal</label>
                                <input type="text" class="form-control" name="email" required=""  ng-model="email">
                            </div>
                            <div class="form-group">
                                <label>Celular</label>
                                <input type="text" class="form-control" name="cell_phone" required=""  ng-model="cell_phone">
                            </div>
                            <div class="form-group">
                                <label>Teléfono</label>
                                <input type="text" class="form-control" name="phone" required=""  ng-model="phone">
                            </div>
                            <div class="form-group">
                                <label>direccion</label>
                                <input type="text" class="form-control" name="phone" required=""  ng-model="address">
                            </div>

                            <div class="form-group">
                                <label>Tipo Documento</label>
                                <select class="form-control" name="type_document" required=""  ng-model="type_document">
                                    <option value=""></option>
                                    <option value="CC">Cedula de Ciudadania</option>
                                    <option value="TI">Tarjeta de Identidad</option>
                                    <option value="CE">Cedula de Extanjería</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>N° Documento</label>
                                <input type="text" class="form-control" name="document" required=""  ng-model="document">
                            </div>
                            <div class="form-group">
                                <label>Ciudad</label>
                                <select class="form-control" name="city_id" required=""  ng-model="city_id">
                                    <option ng-repeat="city in cityes" value="@{{city.id}}">@{{city.name}}</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Proceso:</label>
                                <select id="edit_process" class="form-control" name="proceso_perfil">
                                @foreach($processes as $process)      
                                    <option value='{{$process->id}}'>{{$process->name}}</option>
                                @endforeach 
                                </select>
                            </div>
                            <div class="form-group" id="perfil">
                                <label>Cargo:</label>
                                 <select id="profile" class="form-control" name="profile_user" ng-model="profile_user">
                                 @foreach($profiles as $profile)      
                                     <option value='{{$profile->id}}'>{{$profile->profile}}</option>
                                 @endforeach 
                                 </select>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary">Add new</button>

                </div>
                </form>



            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="wide2" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog  modal-wide">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="icon-plus"></i>  Agregar Perfiles</h4>
                </div>
                <div class="modal-body">                  
                    <div class="form-body">
                        <div class="form-group">
                            <label>Nombre del Perfil:</label>
                            <input type="text" class="form-control" name="name_profile" id="name_new_profile" required="" >
                        </div>
                        <div id="error_new_profile"></div>
                        <div class="form-group">
                            <label>Proceso:</label>
                            <select id="proceso_perfil" class="form-control" name="proceso_perfil">
                            @foreach($processes as $process)      
                                <option value='{{$process->id}}'>{{$process->name}}</option>
                           @endforeach 
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Submenus Que puede ver el perfil:</label>
                            <select multiple="multiple" id="callbacks" name="my-select[]">
                              @foreach($menus as $menu)
                                <optgroup label='{{$menu->menu}}'>
                                @foreach($submenus as $submenu)
                                    @if($submenu->menu_id == $menu->id)
                                       <option value='{{$submenu->id}}'>{{$submenu->submenu}}</option>
                                    @endif
                                @endforeach
                                 </optgroup>            
                              @endforeach
                                    
                                           
                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close_modal_perfil" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="guardarperfil();" id="create_new_profile">Add new</button>

                </div>               



            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="wide4" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog  modal-wide">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="icon-plus"></i>  Agregar permisos</h4>
                </div>
                <div class="modal-body">                  
                    <div class="form-body">
                        <div class="form-group">
                            <label>Proceso:</label>
                            <select id="add_processes" class="form-control" name="add_processes">
                            @foreach($processes as $process)      
                                <option value='{{$process->id}}'>{{$process->name}}</option>
                            @endforeach 
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nombre del Perfil:</label>
                            <select id="profiles_change_add" class="form-control" name="profiles_change_add" >
                             @foreach($profiles as $profile)
                                 <option value='{{$profile->id}}'>{{$profile->profile}}</option>
                                 
                            @endforeach 
                             </select>
                        </div>

                        <div class="form-group" id="permisos_user_add">                           
                        </div>                        

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close_modal_perfil_a" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="agregarPermisos();" id="create_new_profile">Guardar</button>

                </div>               



            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    
    <div class="modal fade" id="wide3" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog  modal-wide">
            <div class="modal-content" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><i class="icon-remove"></i>  Eliminar permisos</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Proceso:</label>
                            <select id="edit_processes" class="form-control" name="proceso_perfil">
                            @foreach($processes as $process)      
                                <option value='{{$process->id}}'>{{$process->name}}</option>
                            @endforeach 
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Nombre del Perfil:</label>
                            <select id="profiles_cahge" class="form-control" name="profile_e" >
                             @foreach($profiles as $profile)      
                                 <option value='{{$profile->id}}'>{{$profile->profile}}</option>
                                 
                            @endforeach 
                             </select>
                        </div>

                        <div class="form-group" id="permisos_user">                           
                        </div>                        

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="close_modal_perfil_e" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" onclick="eliminarPermisos();" id="create_new_profile">Eliminar</button>

                </div>               



            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <!-- modal para crear usuarios -->
    <div class="row" id="crear_usuarios">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-user"></i>Listado de Usuarios</div>
                    <div class="tools">
                        <input type="text" ng-model="search" placeholder="Buscador">
                        <a href="javascript:;" class="collapse"></a>
                        <a href="javascript:;" class="reload"></a>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <table class="table table-hover" id="registros2">
                            <thead>
                                <tr>

                                    <th>Nombre</th>
                                    <th>Apellido</th>
                                    <th>Teléfono</th>
                                    <th>Email</th>

                                    <th>documento</th>
                                    <th>Operaciones</th>
                                </tr>
                            </thead>
                            <tbody >                     
                                <tr id="registros" ng-repeat="todo in todos| filter:search">

                                    <td>@{{todo.name}} </td>
                                    <td>@{{todo.last_name}} </td>
                                    <td>@{{todo.cell_phone}} </td>
                                    <td>@{{todo.email_institutional}} </td>
                                    <td>@{{todo.document}} </td>

                                    <td>
                                        <!-- <a href="#" class="btn btn-danger delete">
                                            <i class="icon-remove"></i>
                                        </a> -->

                                        <a class=" btn btn-default" style="" data-target="#ajax" id="@{{todo.id}}" data-toggle="modal" onclick='realizarProceso($(this).attr("id"));
                                          return false;'><i class="icon-search"></i></a>

                                        <a class=" btn btn-success" style="" data-target="#ajax" id="@{{todo.id}}" data-toggle="modal" onclick='realizarProceso2($(this).attr("id"));
                                          return false;'><i class="icon-edit"></i>
                                        </a>
                                        <!-- <a href="" class="btn btn-success delete">
                                            <i class="icon-edit"></i>
                                        </a> -->
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
        <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
    </div>

    <!-- END PAGE CONTENT-->
</div>
<input id='val' type='hidden' name='user' calass='input-block-level' value=''>

<script src="{{ URL::to("js/jquery.js")}}" type="text/javascript"></script>
<script src="{{ URL::to("js/jquery-1.10.2.min.js")}}" type="text/javascript"></script>
<script src="{{  URL::to("js/jquery.tinysort.js")}}" type="text/javascript"></script>
<script src="{{  URL::to("js/jquery.quicksearch.js")}}" type="text/javascript"></script>
<script src="{{  URL::to("js/jquery.multi-select.js")}}" type="text/javascript"></script>
<script src="{{  URL::to("js/rainbow.js")}}" type="text/javascript"></script>
<script src="{{  URL::to("js/application.js")}}" type="text/javascript"></script>
<script src="{{  URL::to("js/handlebars.js")}}" type="text/javascript"></script>

<script type="text/javascript">
    function realizarProceso(valorCaja1) {
        var parametros = {
            "valorCaja1": valorCaja1
        };
        $.ajax({
            data: parametros,
            url: 'edit_user',
            cache: false,
            type: 'get',
            success: function (response) {
                $("#ajax").html(response);
            }
        });
    }
</script>

<script type="text/javascript">
    function realizarProceso2(valorCaja1) {
        var parametros = {
            "valorCaja1": valorCaja1
        };
        $.ajax({
            data: parametros,
            url: 'edit_user2',
            cache: false,
            type: 'get',
            success: function (response) {
                $("#ajax").html(response);
            }
        });
    }
</script>
<script type="text/javascript">
    function form_createusers() {
        var parametros = {
            "id": "hola"
        };
        $.ajax({
            data: parametros,
            url: 'form_createusers',
            cache: false,
            type: 'get',
            success: function (response) {
                $("#crear_usuarios").html(response);
            }
        });
    }
</script>