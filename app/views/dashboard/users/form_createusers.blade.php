<div class="table-responsive">
   <div class="tabbable tabbable-custom">
      
      <div class="tab-content">
         <div class="tab-pane active" id="tab_1_2">
            <div class="portlet ">
               <div class="portlet-title">
                  <div class="caption">
                     <i class="icon-reorder"></i> Nuevo Usuario
                  </div>
               </div>
               
               <div class="portlet-body form">
                  
                  <form class="form-horizontal" role="form">
                     <div class="form-body">
                        
                           
                        
                        <div class="row">
                           <div class="col-md-6">
                              <h4>Información Personal</h4>
                              <hr>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input id="name" type="text" class="form-control input-lg" placeholder="Primer Nombre" required>
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input id="name2" type="text" class="form-control input-lg" placeholder="Segundo Nombre">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input id="last_name" type="text" class="form-control input-lg" placeholder="Primer Apellido" required>
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-edit"></i></span>
                                          <input id="last_name2" type="text" class="form-control input-lg" placeholder="Segundo Apellido">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-info-sign"></i></span>
                                          <!-- <input id="type_document" type="text" class="form-control input-lg" placeholder="Tipo De Documento" required> -->
                                          <select id="type_document" class="form-control input-lg">
                                             <option value="">Tipo de documento</option>
                                             <option value="CC">Cedula de Ciudadania</option>
                                             <option value="TI">Tarjeta de Identidad</option>
                                          </select>
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-info-sign"></i></span>
                                          <input id="document" type="text" class="form-control input-lg" placeholder="N° Documento" required>
                                       </div> 
                                    </div>
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                          <input id="email" type="text" class="form-control input-lg" placeholder="Correo personal">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-group"></i></span>
                                          <select id="city_id" class="form-control input-lg">
                                            <option value="">Ciudad</option>
                                          @foreach($cities as $city)
                                          <option value="{{$city->id}}">{{$city->name}}</option>
                                          @endforeach
                                          </select>
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-map-marker"></i></span>
                                          <input id="address" type="text" class="form-control input-lg" placeholder="Dirección vivienda">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-mobile-phone"></i></span>
                                          <input id="cell_phone" type="text" class="form-control input-lg" placeholder="Teléfono celular">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-phone"></i></span>
                                          <input id="phone" type="text" class="form-control input-lg" placeholder="Teléfono fijo">
                                       </div> 
                                    </div>
                                 </div>
                              </div>
                           </div>   
                           <div class="col-md-6">
                              <h4>Información Laboral</h4>
                              <hr>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-headphones"></i></span>
                                          <input id="extention" type="text" class="form-control input-lg" placeholder="Extensión">
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-envelope"></i></span>
                                          <input id="email_institutional" type="text" class="form-control input-lg" placeholder="Correo institucional">
                                       </div> 
                                    </div>
                                 </div>   
                              </div>
                              <div class="col-md-6">
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-group"></i></span>
                                          <select name="process_id" id="create_user_process" class="form-control input-lg" onchange="buscar_proceso()">
                                          <option value="">Proceso</option>
                                          @foreach($processes as $process)
                                          <option value="{{$process->id}}">{{$process->name}}</option>
                                          @endforeach
                                          </select>
                                       </div> 
                                    </div>
                                 </div>
                                 <div class="form-group">
                                    <div class="col-md-12">
                                       <div class="input-group">
                                          <span class="input-group-addon"><i class="icon-group"></i></span>
                                          <select name="create_user_profile" id="create_user_profile" class="form-control input-lg">
                                          <option value="">Cargo</option>
                                          @foreach($profiles as $profile)
                                          <option value="{{$profile->id}}">{{$profile->profile}}</option>
                                          @endforeach
                                          </select>
                                       </div> 
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- <div class="row">
                           <div class="col-md-12">
                              <h4>Imágenes</h4>
                              <hr>
                                 <div class="form-group last">
                                    
                                    <div class="col-md-4">
                                       <label>Completa</label>
                                       <div class="fileupload fileupload-new" data-provides="fileupload">
                                          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                             <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                          </div>
                                          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                          <div>
                                             <span class="btn btn-default btn-file">
                                             <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                             <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar </span>
                                             <input type="file" class="default" id="img_complete" size="20" />
                                             </span>
                                             <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Eliminar </a>
                                          </div>
                                       </div>
                                       <span class="label label-danger">Nota</span>
                                       <span>
                                          La imagen debe ser de 180px por 240px
                                       </span>
                                    </div>
               
                                    <div class="col-md-4">
                                       <label>Puesto de trabajo</label>
                                       <div class="fileupload fileupload-new" data-provides="fileupload">
                                          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                             <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                          </div>
                                          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                          <div>
                                             <span class="btn btn-default btn-file">
                                             <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                             <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar </span>
                                             <input type="file" class="default" id="img_wortkstation" size="20" />
                                             </span>
                                             <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Eliminar </a>
                                          </div>
                                       </div>
                                       <span class="label label-danger">Nota</span>
                                       <span>
                                       La imagen debe ser de 180px por 240px
                                       </span>
                                    </div>

                                    <div class="col-md-4">
                                       <label>Miniatura</label>
                                       <div class="fileupload fileupload-new" data-provides="fileupload">
                                          <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                             <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                          </div>
                                          <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                          <div>
                                             <span class="btn btn-default btn-file">
                                             <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                             <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar </span>
                                             <input type="file" class="default" id="img_mini" size="20" />
                                             </span>
                                             <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Eliminar </a>
                                          </div>
                                       </div>
                                       <span class="label label-danger">Nota</span>
                                       <span>
                                       La imagen debe ser de 80px por 80px
                                       </span>
                                    </div>
                                 </div>
                              </div>
                           </div> 
                        </div> -->  
                     </div>
                     <div class="form-actions right">                           
                        <button type="button" class="btn btn-success" onclick="create_new_user()">Registrar</button>                            
                     </div>
                  
               </div>

            </div>
         </div>
      </div>         
   </div>   
</div> 


