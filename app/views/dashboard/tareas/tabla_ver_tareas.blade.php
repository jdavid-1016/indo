<table class="table top-blue">
    <thead>
        <tr >
            <th>Id</th>
            <th>Actividades</th>
            <th>Estado</th>
            <th>Cateoria</th>
            <th>Periodicidad</th>
            <th>Técnico</th>
            <th>Progreso</th>
            <th>Inicio 
                <a href="#" onclick="tabla_lista_tareas('asc')"><i class="icon-circle-arrow-down" style="color:fff"></i></a>
                <a href="#" onclick="tabla_lista_tareas('desc')"><i class="icon-circle-arrow-up" style="color:fff"></i></a>
            </th>
            <th>Seguimiento</th>
        </tr>
    </thead>
    <tbody id="">
        @foreach($tareas as $tarea)
            <tr class="{{$tarea['row_color']}}">
                <td class="td_center">
                    {{$tarea['codigo']}}
                </td>
                <td class="tarea">
                    <span class="tarea-titulo ">{{$tarea['tarea']}}</span>
                    <span class="tarea-cuerpo comment more ">{{$tarea['descripcion']}}</span>
                </td>
                <!-- <td class="">
                    <a href="#" class=" nameimg" title="">
                      <img alt="" style="border-radius:50px;" width="50" height="50" src="../{{$tarea['img_usuario']}}">
                        {{$tarea['usuario']}}
                    </a>
                </td>  -->
                <td class="td_center">
                    {{$tarea['estado']}}
                </td>
                <td class="td_center">
                    {{$tarea['categoria']}}
                </td>
                <td class="td_center">{{$tarea['periodicidad']}}</td>
                <td class="td_center">{{$tarea['tecnico']}}</td>
                <td>
                    <div class="progress progress-striped">
                        <div class="progress-bar {{$tarea['progress_class']}}" role="progressbar" aria-valuenow="{{$tarea['progreso']}}" aria-valuemin="0" aria-valuemax="100" style="width: {{$tarea['progreso']}}%">
                            <span>{{$tarea['progreso']}} %</span>
                        </div>
                    </div>
                </td>
                <td class="td_center">{{$tarea['f_inicio']}}</td>
                <td class="td_center">
                    <a class="btn btn-primary btn_comment"   id="{{$tarea['id']}}" data-target="#seguimiento_tareas" data-toggle="modal" onclick='cargar_seguimiento($(this).attr("id"));return false;'>
                        <i class="icon-comment-alt"></i>
                        <span class="badge badge-warning" id="comentarios_{{$tarea['id']}}">{{$tarea['comentarios']}}</span>
                    </a>       
                    <a class=" btn btn-default" style="" data-target="#ajax" id="{{$tarea['id']}}" data-toggle="modal" onclick='cargar_detalle_tarea($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>