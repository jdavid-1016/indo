
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Historico de Actividades</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class=""><a href="ver">Ver Actividades</a></li>
                        <li class="active"><a href="historico">Historico de Actividades</a></li>
                        <!-- <li class=""><a href="#tab_1_4" data-toggle="tab">Grupos de Equipos</a></li>
                        <li class=""><a href="#tab_1_5" data-toggle="tab">Agregar Equipos A Un Grupo</a></li>
                        <li class=""><a href="#tab_1_6" data-toggle="tab">Eliminar Equipos De Un Grupo</a></li> -->

                        
                    </ul>
                    <div  class="tab-content">
                        
                        <div class="tab-pane fade active in" id="tab_1_2">
                            <div class="row">
                                <div class="col-md-12">  
                                    <div>
                                        <form class="form-inline" >
                                            <div class="search-region">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('id')}}" onkeyup="tabla_lista_tareas()">
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control input-medium select2me" id="categoria" onchange="tabla_lista_tareas()">
                                                    <option value="">Categoria</option> 
                                                        @foreach($categories as $categorie)
                                                            @if($categorie->id == Input::get('categorie'))
                                                               <option value="{{$categorie->id}}" selected="">{{$categorie->category}}</option>
                                                            @else
                                                               <option value="{{$categorie->id}}">{{$categorie->category}}</option>
                                                            @endif  
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control input-medium select2me" id="periodicidad" onchange="tabla_lista_tareas()">
                                                    <option value="">Periodicidad</option> 
                                                        @foreach($periodicidad as $periodicidad)
                                                            @if($periodicidad->id == Input::get('periodicidad'))
                                                                <option value="{{$periodicidad->id}}" selected="">{{$periodicidad->periodicity}}</option>
                                                            @else
                                                                <option value="{{$periodicidad->id}}">{{$periodicidad->periodicity}}</option>
                                                                
                                                            @endif  
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control input-medium select2me" id="responsable" onchange="tabla_lista_tareas()">
                                                    <option value="">Responsable</option> 
                                                        @foreach($usuarios as $applicant)
                                                            @if($applicant->id == Input::get('applicant'))
                                                               <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                                                            @else
                                                               <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                                                            @endif  
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <select class="form-control input-medium select2me" id="progreso" onchange="tabla_lista_tareas()">
                                                    <option value="">Progreso</option> 
                                                        @foreach($progresos as $progreso)
                                                            @if($progreso->progress == Input::get('progresos'))
                                                               <option value="{{$progreso->progress}}" selected="">{{$progreso->progress}}%</option>
                                                            @else
                                                               <option value="{{$progreso->progress}}">{{$progreso->progress}}%</option>
                                                            @endif  
                                                        @endforeach
                                                    </select>
                                                </div>
                                                
                                            </div>
                                        </form> 
                                    </div>                          
                                    <div class="table-responsive" id="tabla_tareas_historico">
                                        @include('dashboard.tareas.tabla_ver_tareas_hist')
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ajax" tabindex="-1" role="basic" aria-hidden="true">
   <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
</div>
<div class="modal fade" id="seguimiento_tareas" tabindex="-1" role="basic" aria-hidden="true">
   <img src="/indoamericana/webroot/img/input-spinner.gif" alt="" class="loading">
</div>
<script type="text/javascript">
    function cargar_detalle_tarea(id_tarea){
        
        $.ajax({
                type: "GET",
                url:  "detalletareaadmin",
                data: { id_tarea:id_tarea}
        })
        .done(function(data) {
            $("#ajax").html(data); 
        });
    }
    function cargar_seguimiento(id_tarea){
        
        $.ajax({
                type: "GET",
                url:  "cargarseguimiento",
                data: { id_tarea:id_tarea}
        })
        .done(function(data) {
            $("#seguimiento_tareas").html(data); 
        });
    }
    function tabla_lista_tareas(orden){

        
        var code            = $("#code").val();
        var periodicidad    = $("#periodicidad").val();
        var responsable     = $("#responsable").val();
        var categoria       = $("#categoria").val();
        var progreso        = $("#progreso").val();


        $.ajax({
                type: "GET",
                url:  "historico",
                data: { orden:orden, code:code, periodicidad:periodicidad, responsable:responsable, categoria:categoria, progreso:progreso, filtro:1}
        })
        .done(function(data) {
            $("#tabla_tareas_historico").html(data); 
        });   
    }

</script>

