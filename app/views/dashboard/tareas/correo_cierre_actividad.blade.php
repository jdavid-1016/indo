<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
   <style type="text/css">
   </style>
</head>
<body>
	
<table width="800" cellspacing="0" cellpadding="0" align="center" style="border:3px solid; font-family: helvetica; border-color:#58ACFA #58ACFA; border-style:solid;">
	<tr>
		<td>
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt; ">
				<tr>
					<td width="800">
						<!-- <img src="http://indoamericana.edu.co/airmail/Diplomado/images/header.jpg" width="800"  style="margin:0;padding:0;"> -->
                        <img src="http://indoamericana.edu.co/airmail/sgi/finalizacion_actividad.jpg" width="800"  style="margin:0;padding:0;">
					</td>
				</tr>
			</table>
			<table width="760" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:11pt; color:#3d3d3d; line-height:18px;">
                <br>
                <tr><td><strong> Hola {{$tareas->Users->name}}  {{$tareas->Users->last_name}}, El Usuario {{Auth::user()->name}} {{Auth::user()->last_name}} ha finalizado una Actividad.</strong></td></tr>
                <br>
                
				<tr>
					<td>
						<div class="row">
                     <div class="page-content">
                     @if(count($tareas) != 0 )
                        <h2 style="color:#58ACFA" >Actividad Finalizada</h2>
                        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt;">
                            <tr style="background: #58ACFA; color: #fff; padding: 9px;" bgcolor="#c7d8a7">
                                <th>Id</th>
                                <th>Actividad</th>
                                <th>Estado</th>
                                <th>Cateoria</th>
                                <th>Periodicidad</th>
                                <th>Progreso</th>
                                <th>Inicio</th>
                                
                            </tr>
                        <!-- </table>
                        <table style="font-size: 14px;font-weight: 600;" width="100%" > -->
                            <tbody>
                            
                                <tr style="padding: 9px;" align="center">
                                    
                                    <td>{{'TR' . str_pad($tareas->id, 6, "0", STR_PAD_LEFT)}}</td>
                                    <td>{{$tareas->tasks}}</td>
                                    <td>{{$tareas->TasksStatuses->status}}</td>
                                    <td>{{$tareas->TasksCategories->category}}</td>
                                    <td>{{$tareas->TasksPeriodicity->periodicity}}</td>
                                    <td>{{$tareas->progress}}%</td>
                                    <td>{{$tareas->start_date}}</td>
                                    
                                </tr>
                            
                            </tbody>
                        </table>
                        @endif
                     </div>
                  </div>
					</td>
				</tr>
			</table>
            <br>
            <hr>
			<table width="800" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: helvetica; font-size:10pt; background-color:#58ACFA;">
				<tr>
					<td></td>
					<td style="color:#fff;">
						<p align="center" style="font-size:8pt">
							<strong style="font-size:9pt;">Contacto:</strong> <br>
							Corporación Educativa Indoamericana - Area de sistemas - 3239750 ext 2006 - <a href="mailto:dzarate@indoamericana.edu.co">dzarate@indoamericana.edu.co</a> - Casa Piper
						</p>
					</td>
					<td></td>
				</tr>
			</table>
		</td>
	</tr>	
</table>	
</body>
</html>