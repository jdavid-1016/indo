<div class="modal-dialog " ng-controller="help_deskCtrl" ng-app>
    <div class="modal-content">
   		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        	<h4 class="modal-title">Seguimiento de Tareas</h4>
    	</div>
      	<div class="modal-body form-body">
      	    <ul class="chats">
                @foreach($tareas as $tarea)
                <li class="in">
                    <img class="avatar " alt="" src="../{{$tarea['img_usuario']}}">
                    <div class="message">
                        <span class="text-primary">{{$tarea['tarea']}}</span>
                        <span class="arrow"></span>
                        <span class="body"><textarea required class="form-control" rows="2" name="message" disabled="">{{$tarea['descripcion']}}</textarea></span>
                    </div>
                </li>
                @endforeach
                @foreach($comentarios as $comentarios)
                <li class="{{$comentarios['align']}}">
                    <img class="avatar " alt="" src="../{{$comentarios['img_user']}}">
                    <div class="message">
                        <span class="text-primary">{{$comentarios['user']}}</span> {{$comentarios['date']}}
                        <span class="arrow"></span>
                        <span class="body"><textarea id="nuevo_comentario1" class="form-control" rows="2" name="message" disabled="">{{$comentarios['comment']}}</textarea></span>
                    </div>
                </li>
                @endforeach
                

        @if($tareas[0]['estado'] == 4 || $tareas[0]['estado'] == 2)
        <div class="modal-footer">
            
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
        </div>
        @else
                <li class="out">
                    <img class="avatar " alt="" src="../{{Auth::user()->img_min}}">
                    <div class="message">
                        <span class="arrow"></span>
                        <span class="body"><textarea id="nuevo_comentario" class="form-control" rows="2" name="message"></textarea></span>
                    </div>
                </li>
            </ul>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success refresh" onclick="nuevo_comentario({{$tarea['id']}})">Guardar</button>  
            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_seguimiento">Cerrar</button>  
        </div>
        @endif
    </div>
</div>
<script type="text/javascript">
    function progreso(signo){
        var progress = parseFloat($("#progress").val());
        var total = progress;

        if (signo == 1) {
            if (progress != 100) {
                total = progress+5;
            }
        }else{
            if (progress != 0) {
                total = progress-5;
            }
            
        }

        $("#progress").val(total);
    }

</script>

<script type="text/javascript">
    function nuevo_comentario(id){
        
        var nuevo_comentario = $("#nuevo_comentario").val();
        if (nuevo_comentario == "") {
            toastr.error('El campo Comentario está vacío', 'Error');
            $("#nuevo_comentario").focus();
            return;
        }

        $.ajax({
            type: "POST",
            url:  "nuevocomentario",
            data: { id_item: id, observacion:nuevo_comentario}
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se ingreso correctamente el Comentario', 'Nuevo Comentario');
                $("#close_modal_seguimiento").click();
                    
            }else{
                toastr.error('No se ingreso el Comentario', 'Error');
            }

        });
        
    }
</script>