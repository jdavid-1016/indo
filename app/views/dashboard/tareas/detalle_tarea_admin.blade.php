<div class="modal-dialog modal-wide" ng-controller="help_deskCtrl" ng-app>
    <div class="modal-content">
   		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        	<h4 class="modal-title">Detalle de Actividad admin</h4>
    	</div>
      	<div class="modal-body form-body">
        <ul  class="nav nav-tabs">
            <li class="active"><a href="#detalle_tarea" data-toggle="tab">Detalle de la Tarea</a></li>
            <!-- <li class=""><a href="#items_de_tarea"  data-toggle="tab">Items</a></li> -->
        </ul>
        <div class="tab-content">      
            <div class="tab-pane fade active in" id="detalle_tarea">
          		<div class="row">

          		   	<div class="col-md-12">
          		      	<div class="form-group"> 
          		         	<label class="control-label">Tarea</label>
          		         	<div class="input-group">
          		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
          		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->tasks}}">
          		         	</div> 
          		      	</div>
          		   	</div>
          		   	<div class="col-md-12">
          		      	<div class="form-group"> 
          		         	<label class="control-label">Descripción</label>
          		         	<div class="input-group">
          		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
          		            	<textarea rows="5" readonly="" class="form-control" >{{$tareas[0]->description}}</textarea>
          		         	</div> 
          		      	</div>
          		   	</div>
          		   	<div class="col-md-6">
          		      	<div class="form-group"> 
          		         	<label class="control-label">Fecha de Inicio</label>
          		         	<div class="input-group">
          		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
          		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->start_date}}">
          		         	</div> 
          		      	</div>
          		   	</div>
                  <div class="col-md-6">
                      <div class="form-group"> 
                        <label class="control-label">Fecha de Vencimiento</label>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                            <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->end_date}}">
                        </div> 
                      </div>
                  </div>
          		   	<div class="col-md-6">
          		      	<div class="form-group"> 
          		         	<label class="control-label">Periodicidad</label>
          		         	<div class="input-group">
          		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
          		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->TasksPeriodicity->periodicity}}">
          		         	</div> 
          		      	</div>
          		   	</div>
          		   	<div class="col-md-6">
          		      	<div class="form-group"> 
          		         	<label class="control-label">Responsable</label>
          		         	<div class="input-group">
          		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
                                @foreach($responsables as $responsable)
                                    <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$responsable->Users->name}} {{$responsable->Users->last_name}}">
                                @endforeach
          		            	
          		         	</div> 
                      </div>
          		   	</div>
          		   	<div class="col-md-6">
          		      	<div class="form-group"> 
          		         	<label class="control-label">Prioridad</label>
          		         	<div class="input-group">
          		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
          		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->priority}}">
          		         	</div> 
          		      	</div>
          		   	</div>
                @if($tareas[0]->tasks_statuses_id == 1 || $tareas[0]->tasks_statuses_id == 3)
                    <div class="btn-group">
                        
                        <a type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" id="btn_most_users"><i class="icon-group"></i> Asignar a Otro Usuario</a>
                        <div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu" style="width:200px">
                            <div class="input-group">
                                <select class="form-control" id="proceso" onchange="cargar_responsables2()">
                                    <option></option>
                                    @foreach($procesos as $proceso)
                                    <option value="{{$proceso->id}}">{{$proceso->acronyms}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group">
                                <select class="form-control" id="responsable2" >
                                    <option></option>
                                </select>
                            </div>
                            <a class="btn btn-success pull-right" onclick="reasignar_tarea({{$id_tarea}})"><i class="icon-ok"></i> </a>
                            <br>
                            <div id="" class="width:100%">
                                <table id="tareas_usuarios" class="table">
                                </table>
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-success"> 
                            <label class="control-label">Estado de la Solicitud</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <select class="form-control" id="estado_tarea" onchange="activar_calificacion()">
                                    <option></option>
                                    @foreach($estados as $estado)
                                    <option value="{{$estado->id}}">{{$estado->status}}</option>
                                    @endforeach
                                </select>
                            </div> 
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group has-success" style="display:none" id="campo_calificacion"> 
                            <label class="control-label">Calificación de la tarea</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <select class="form-control" id="calificacion_tarea">
                                    <option></option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    
                                </select>
                            </div> 
                        </div>
                    </div>
                         
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" id="" onclick="cambiar_estado({{$id_tarea}})">Guardar</button>  
                        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
                    </div>
                @endif
            <!-- </div>
            <div class="tab-pane fade in" id="items_de_tarea"> -->
                <div class="table-responsive">
                    <table class="table top-blue">
                        <thead>
                            <tr >
                                <th>Item</th>
                                <th>Estado</th>
                                <th>Comentario</th>
                                <th>Usuario</th>
                                
                            </tr>
                        </thead>
                        <tbody id="tabla_tareas">
                            @foreach($data_item as $tarea)
                                <tr class="{{$tarea['row_class']}}">
                                    <td class="td_center">
                                        <span >{{$tarea['item']}}</span>
                                        
                                    </td>
                                    <td class="td_center">
                                        {{$tarea['estado']}}
                                            
                                        
                                    </td> 
                                        
                                    <td class="td_center">
                                        {{$tarea['text']}}
                                    </td>
                                    <td class="td_center">
                                        {{$tarea['user']}}
                                    </td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                        <div class="btn-group">
                            
                            <a type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" id="btn_most_users"><i class="icon-plus"></i> Agregar Subtareas</a>
                            <div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu" style="width:400px">
                                <input type="text" class="form-control" value="" id="otro_nuevo_item" placeholder="Subtarea">
                                <hr>
                                
                                <a class="btn btn-success pull-right" onclick="agregar_nuevo_item({{$tareas[0]->id}})"><i class="icon-ok"></i> </a>
                                <br>
                                
                            </div>
                        </div>
                    </table>
                    
                </div>
      	<div class="modal-footer">
      		<button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
      	</div>
            </div>
        </div>
        </div>
    </div>
</div>