
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Actividades</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class="active"><a href="crear" >Crear Actividades</a></li>
                        <li class=""><a href="items">Items de Actividades</a></li>
                        <!-- <li class=""><a href="#tab_1_4" data-toggle="tab">Grupos de Equipos</a></li>
                        <li class=""><a href="#tab_1_5" data-toggle="tab">Agregar Equipos A Un Grupo</a></li>
                        <li class=""><a href="#tab_1_6" data-toggle="tab">Eliminar Equipos De Un Grupo</a></li> -->

                        
                    </ul>
                    <div  class="tab-content">
                        
                        <div class="tab-pane fade active in" id="tab_1_2">
                            <div class="table-responsive" id="table_admin_supports">
                                <div class="row">
                                    
                                    <div class="col-md-6">
                                        <div class="portlet">
                                            
                                            <div class="portlet-body">
                                                <div class="form-group"> 
                                                   <label class="control-label">Actividad</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control" id="tarea" value="">
                                                   </div> 
                                                </div>
                                                <div class="form-group"> 
                                                   <label class="control-label">descripcion</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <textarea class="form-control" id="tarea_desc" value=""></textarea>
                                                   </div> 
                                                </div>
                                                <div class="form-group"> 
                                                   <label class="control-label">Periodicidad</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <select class="form-control" id="tarea_per" >
                                                        <option></option>
                                                        @foreach($periodicidad as $per)
                                                        <option value="{{$per->id}}">{{$per->periodicity}}</option>
                                                        @endforeach
                                                    </select>
                                                   </div> 
                                                </div>
                                                <div class="form-group col-md-6"> 
                                                   <label class="control-label">Fecha de Inicio</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control fecha_required" value="" id="tarea_fecha_ini">

                                                      
                                                   </div> 
                                                </div>
                                                <div class="form-group"> 
                                                   <label class="control-label">Fecha de Vencimiento</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control fecha_required" value="" id="tarea_fecha_venci">

                                                      
                                                   </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet">
                                            <div class="portlet-body">
                                                <!-- <div class="form-group"> 
                                                    <label class="control-label">proceso</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select class="form-control" id="proceso" onchange="cargar_responsables()">
                                                            <option></option>
                                                            @foreach($procesos as $proceso)
                                                            <option value="{{$proceso->id}}">{{$proceso->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="control-label">Responsable</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select class="form-control" id="responsable" >
                                                            <option></option>
                                                           
                                                        </select>
                                                    </div> 
                                                </div> -->
                                                
                                                <div class="form-group"> 
                                                   <label class="control-label">Prioridad</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <select class="form-control" id="prioridad" >
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        
                                                    </select>
                                                   </div> 
                                                    <div class="help-block">Donde 1 es la más urgente, 5 la menos urgente.</div>
                                                </div>
                                                <div class="form-group"> 
                                                   <label class="control-label">Categoria</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <select class="form-control" id="categoria" >
                                                        <option value=""></option>
                                                        @foreach($categorias as $categorias)
                                                        <option value="{{$categorias->id}}">{{$categorias->category}}</option>
                                                        @endforeach
                                                        
                                                    </select>
                                                   </div> 
                                                    
                                                </div>
                                                

                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">
                                                            <input type="checkbox" class="toggle" id="noti_correo"> 
                                                        </span>
                                                        <label class="control-label" for="noti_correo">  Notificación por Correo</label>
                                                    </div>
                                                </div>
                                                   
                                                <div class="btn-group">
                                                    
                                                    <a type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" id="btn_most_users"><i class="icon-group"></i> Usuarios</a>
                                                    <div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu" style="width:200px">
                                                        <div class="input-group">
                                                            <select class="form-control" id="proceso" onchange="cargar_responsables()">
                                                                <option></option>
                                                                @foreach($procesos as $proceso)
                                                                <option value="{{$proceso->id}}">{{$proceso->acronyms}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                        <div class="input-group">
                                                            <select class="form-control" id="responsable" >
                                                                <option></option>
                                                            </select>
                                                        </div>
                                                        <a class="btn btn-success pull-right" onclick="tareas_guardar_usuario()"><i class="icon-ok"></i> </a>
                                                        <br>
                                                        <div id="" class="width:100%">
                                                            <table id="tareas_usuarios" class="table">
                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="btn-group">
                                                    
                                                    <a type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" id="btn_most_users"><i class="icon-ok"></i> Subtareas</a>
                                                    <div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu" style="width:400px">
                                                        
                                                            <input type="text" class="form-control" value="" id="item1" placeholder="Subtarea">
                                                        <hr>
                                                        
                                                        <a class="btn btn-success pull-right" onclick="tareas_guardar_item()"><i class="icon-ok"></i> </a>
                                                        <br>
                                                        <div id="" class="width:100%">
                                                            <table id="tareas_items" class="table">
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
           </div>
            <a class="btn btn-success pull-right" onclick="crear_tarea()"><i class="icon-ok"></i> Crear</a>
        </div>
    </div>
</div>


    

