
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Agregar Items a Actividades</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class=""><a href="crear" >Crear Actividades</a></li>
                        <li class="active"><a href="items">Items de Actividades</a></li>
                        <!-- <li class=""><a href="#tab_1_4" data-toggle="tab">Grupos de Equipos</a></li>
                        <li class=""><a href="#tab_1_5" data-toggle="tab">Agregar Equipos A Un Grupo</a></li>
                        <li class=""><a href="#tab_1_6" data-toggle="tab">Eliminar Equipos De Un Grupo</a></li> -->

                        
                    </ul>
                    <div  class="tab-content">
                        
                        <div class="tab-pane fade active in" id="tab_1_2">
                            <div class="table-responsive" id="table_admin_supports">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="portlet">
                                            <div class="portlet-body">
                                                <div class="form-group"> 
                                                    <label class="control-label">Actividad</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select class="form-control select2me" id="id_tarea" onchange="cargar_detalle_tarea()">
                                                            <option></option>
                                                            @foreach($tareas as $tarea)
                                                            <option value="{{$tarea->id}}">{{$tarea->tasks}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet">
                                            <div class="portlet-body">
                                                <div class="form-group"> 
                                                    <label class="control-label">Item</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input type="text" class="form-control" id="item_tarea" value="">
                                                    </div> 
                                                </div> 
                                                
                                            </div>
                                        </div>

                                    <a class="btn btn-success pull-right" onclick="crear_item_tarea()"><i class="icon-ok"></i> Crear</a>
                                    </div>
                                    
                                </div>
                                <div class="row" >
                                    <div id="detalle_tarea">
                                            
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group has-success note note-info" id="">
                                            <label class="control-label">Items de la Actividad</label>
                                            <div id="items_de_tareas" class="row">
                                                
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- <a class="btn btn-success pull-right" onclick="crear_tarea()"><i class="icon-ok"></i> Crear</a> -->
        </div>
    </div>
</div>
  

<script type="text/javascript">
    function cargar_detalle_tarea(){
        var id_tarea = $("#id_tarea").val();

        $("#detalle_tarea").load('detalletarea?id_tarea=' + id_tarea + '&item=1');
        $("#items_de_tareas").load('ingresaritem?id_tarea=' + id_tarea);
        
    }
    function crear_item_tarea(){
        var id_tarea = $("#id_tarea").val();
        var item_tarea = $("#item_tarea").val();

        

        $.ajax({
                type: "GET",
                url:  "ingresaritem",
                data: { 

                    id_tarea: id_tarea,
                    item: item_tarea
                }
        })
        .done(function(data) {
            if (data != 1) {
                toastr.success('Creo correctamente la tarea', 'Nueva tarea');
                $("#item_tarea").val("");
                $("#items_de_tareas").html(data)
                
                 
            }else{
                toastr.error('No ha sido posible crear la tarea', 'Error');
            }
        });
        
    }
    function eliminar_item(id){
        $.ajax({
                type: "GET",
                url:  "eliminaritem",
                data: { 
                    item: id
                }
        })
        .done(function(data) {
            if (data == 1) {
                var id_tarea = $("#id_tarea").val();
                $("#items_de_tareas").load('ingresaritem?id_tarea=' + id_tarea);
                toastr.success('Se Elimino correctamente el Item', 'Item Eliminado');
                
                 
            }else{
                toastr.warning('No ha sido posible Eliminar El Item', 'Error');
            }
        });

    } 
    function editar_item(id){
        var texto = $("#texto_"+id).val();

        $.ajax({
                type: "GET",
                url:  "editaritems",
                data: { 
                    item: id,
                    texto: texto
                }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.info('Se editó correctamente el Item', 'Edición de Item');
                
                
                 
            }else{
                toastr.warning('No ha sido posible editar El Item', 'Error');
            }
        });

    }    

        //$("#profile_css").css("display", "block");
    
</script>

