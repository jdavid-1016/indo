<div class="modal-dialog modal-wide" ng-controller="help_deskCtrl" ng-app>
    <div class="modal-content">
   		<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
        	<h4 class="modal-title">Detalle de Actividad</h4>
    	</div>
      	<div class="modal-body form-body">
            <ul  class="nav nav-tabs">
                <li class="active"><a href="#detalle_tarea" data-toggle="tab">Detalle de la Tarea</a></li>
                <li class=""><a href="#items_de_tarea"  data-toggle="tab">Items</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade active in" id="detalle_tarea">
              		<div class="row">
              		   	<div class="col-md-12">
              		      	<div class="form-group"> 
              		         	<label class="control-label">Tarea</label>
              		         	<div class="input-group">
              		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
              		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->tasks}}">
              		         	</div> 
              		      	</div>
              		   	</div>
              		   	<div class="col-md-12">
              		      	<div class="form-group"> 
              		         	<label class="control-label">Descripción</label>
              		         	<div class="input-group">
              		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
              		            	<textarea rows="5" readonly="" class="form-control" >{{$tareas[0]->description}}</textarea>
              		         	</div> 
              		      	</div>
              		   	</div>
              		   	<div class="col-md-6">
              		      	<div class="form-group"> 
              		         	<label class="control-label">Fecha de Inicio</label>
              		         	<div class="input-group">
              		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
              		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->start_date}}">
              		         	</div> 
              		      	</div>
              		   	</div>
                      <div class="col-md-6">
                          <div class="form-group"> 
                            <label class="control-label">Fecha de Vencimiento</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->end_date}}">
                            </div> 
                          </div>
                      </div>
              		   	<div class="col-md-6">
              		      	<div class="form-group"> 
              		         	<label class="control-label">Periodicidad</label>
              		         	<div class="input-group">
              		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
              		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->TasksPeriodicity->periodicity}}">
              		         	</div> 
              		      	</div>
              		   	</div>
              		   	<div class="col-md-6">
              		      	<div class="form-group"> 
              		         	<label class="control-label">Responsable</label>
              		         	<div class="input-group">
              		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
                                    @foreach($responsables as $responsable)
                                        <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$responsable->Users->name}} {{$responsable->Users->last_name}}">
                                    @endforeach
              		         	</div> 
              		      	</div>
              		   	</div>
              		   	<div class="col-md-6">
              		      	<div class="form-group"> 
              		         	<label class="control-label">Prioridad</label>
              		         	<div class="input-group">
              		            	<span class="input-group-addon"><i class="icon-edit"></i></span>
              		            	<input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$tareas[0]->priority}}">
              		         	</div> 
                          </div>
                      </div>
                              
                    </div>
                        <!-- <div class="col-md-12">
                            <div class="form-group"> 
                                <label class="control-label">Prioridad</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                    <input readonly="" type="text" class="form-control" placeholder="" data-required="true" value="{{$id_asig}}">
                                </div> 
                            </div>
                        </div> -->
                        @if($tareas[0]->tasks_statuses_id == 1)
                        <div class="col-md-6">
                            <div class="form-group has-success"> 
                                <label class="control-label">Gestión</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                    <select class="form-control" id="id_gestion" onchange="validar_gestion()">
                                        <option value="1">Atender</option>
                                        <option value="2">Rechazar</option>
                                        <option value="3">Escalar</option>
                                    </select>
                                </div> 
                            </div>
                        </div>
                        <div class="col-md-6" id="escalar_actividad" style="display:none">
                            <div class="form-group has-success"> 
                                <label class="control-label">Proceso</label>
                                <div class="input-group">
                                    <select class="form-control" id="proceso" onchange="cargar_responsables2()">
                                        <option></option>
                                        @foreach($procesos as $proceso)
                                        <option value="{{$proceso->id}}">{{$proceso->acronyms}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group has-success"> 
                                <label class="control-label">Usuarios</label>
                                <div class="input-group">
                                    <select class="form-control" id="responsable2" >
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6" id="rechazar_actividad" style="display:none">
                            <div class="form-group has-success"> 
                                <label class="control-label">Comentario</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                    <textarea rows="5" class="form-control" id="comentario_rechazado"></textarea>
                                </div> 
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-success refresh" id="btn_atender_solicitud" onclick="atender_tarea({{$tareas[0]->id}}, {{$id_asig}})">Atender Actividad</button>  
                            <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
                        </div>
                        @endif
                </div>
                <div class="tab-pane fade" id="items_de_tarea">
                    
                    <label class="">Items De la tarea</label>
                    <div class="row">
                        @foreach($data_item as $item)
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <input type="checkbox" {{$item['check']}} class="toggle" id="item_{{$item['id_asig']}}" onclick="activar_campo({{$item['id_asig']}})"> 
                                    </span>
                                    <label class="control-label" for="item_{{$item['id_asig']}}">  {{$item['item']}}</label>
                                </div>
                                    <textarea id="texto_item_{{$item['id_asig']}}" class="spinner-input form-control" placeholder="Descripción" style="{{$item['display']}}">{{$item['text']}}</textarea>
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div id="spinner1">
                                <label class="control-label">Progreso</label>
                                <div class="input-group input-small">
                                    <input name="progress" id="progress" type="text" class="spinner-input form-control" maxlength="3" readonly value="{{$tareas[0]->progress}}">
                                    <div class="spinner-buttons input-group-btn btn-group-vertical">                   
                                        <button id="plus" type="button" class="btn spinner-up btn-xs btn-info" onclick="progreso(1)">                       
                                            <i class="icon-angle-up"></i>                    
                                        </button>                     
                                        <button id="minus" type="button" class="btn spinner-down btn-xs btn-info" onclick="progreso(0)">                        
                                            <i class="icon-angle-down"></i>                     
                                        </button>                  
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table top-blue">
                            <thead>
                                <tr >
                                    <th>Item</th>
                                    <th>Estado</th>
                                    <th>Comentario</th>
                                    <th>Usuario</th>
                                    
                                </tr>
                            </thead>
                            <tbody id="tabla_tareas">
                                @foreach($data_realizados as $tarea)
                                    <tr class="{{$tarea['row_class']}}">
                                        <td class="td_center">
                                            <span >{{$tarea['item']}}</span>
                                            
                                        </td>
                                        <td class="td_center">
                                            {{$tarea['estado']}}
                                                
                                            
                                        </td> 
                                            
                                        <td class="td_center">
                                            {{$tarea['text']}}
                                        </td>
                                        <td class="td_center">
                                            {{$tarea['user']}}
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                            
                        </table>
                        
                    </div>
                  	<div class="modal-footer">
                    @if($tareas[0]->tasks_statuses_id == 1 || $tareas[0]->tasks_statuses_id == 3)
                        <button type="button" class="btn btn-success refresh" onclick="guardar_cambios({{$tareas[0]['id']}})">Guardar</button>  
                    @endif
                  		<button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>  
                  	</div>
                </div>
            </div>
            <hr>
            
        </div>
    </div>
</div>

<script type="text/javascript">
    function guardar_cambios(id){
        var descripciones = new Array();
        var i = 0;
        var ids = new Array();
        var estados = new Array();
        var val = 0;
        

            @foreach($data_item as $tipo)

                if($("#item_{{$tipo['id_asig']}}").is(':checked')) {  
                    // if ($("#texto_item_{{$tipo['id_asig']}}").val() == "") {
                    //     toastr.error('Debe Ingresar una descripcion.', 'Error'); 
                    //     return;
                    // }
                    ids[i] = {{$tipo['id_asig']}};
                    descripciones[i] = $("#texto_item_{{$tipo['id_asig']}}").val();
                    
                    val = 1;

                }

                // if (val == 0) {
                //     toastr.error('Debe seleccionar por lo menos un item.', 'Error'); 
                //     return;
                // }
                i++;
            @endforeach

            var progress = $("#progress").val();
            var id = id;


            $.ajax({
                type: "POST",
                url:  "guardarseguimiento",
                data: { id_item: ids, descripciones:descripciones, progress:progress, id:id}
            })
            .done(function(data) {
                if (data != 2) {
                    toastr.success('Se ingreso correctamente el Seguimiento', 'Nuevo Seguimiento');
                    $("#close_modal").click();
                    actualizar_tabla_mis_tareas();

                    if (data[1] == 1) {
                        // alert('Ha Finalizado una Actividad');
                        var type_event = ["finalizacionactividad", data[0]["name"],data[0]["last_name"], data[0]["img_min"], data[1], data[2]];
                        send( type_event);
                    }else if(data[1] == 2){
                        // alert('Ha Hecho un avance de Actividad');
                        var type_event = ["avanceactividad", data[0]["name"],data[0]["last_name"], data[0]["img_min"], data[1], data[2]];
                        send( type_event);
                    }



                    
                    
                }else{
                    toastr.error('No se ingreso el Seguimiento', 'Error');
                }

            });
        
    }
</script>