<script type="text/javascript">
    

    var arreglo1 = new Array();
    var arreglo2 = new Array();
    var arreglo3 = new Array();


    @foreach($data_tipos as $data_tipo)
        arreglo1.push(new Array('{{$data_tipo->type}}', {{$data_tipo->dato}}));
    @endforeach

    @foreach($data_estados as $data_estados)
        arreglo2.push(new Array('{{$data_estados->status}}', {{$data_estados->dato}}));
    @endforeach
    
    @foreach($data_grupos as $data_grupos)
        arreglo3.push(new Array('{{$data_grupos->name_group}}', {{$data_grupos->dato}}));
    @endforeach
    
    grafico1(arreglo1);
    grafico2(arreglo2);
    grafico3(arreglo3);


</script>
