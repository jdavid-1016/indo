
<style>
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }
    #wrapper {
        padding: 15px;
    }
    #software_adicionales {
        width: 300px;
    }
</style>



<div class="page-content">
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1_2">
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Asignar Elementos a un Equipo
                    </div>
                </div>
                  
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class=""><a href="equipos">Ingresar Equipos</a></li>
                        <li class="active"><a href="elementos">Asignar elementos a un Equipo</a></li>
                        <li class=""><a href="asignar">Asignar Equipos a usuarios</a></li>
                        <li class=""><a href="eliminar">Eliminar Asignación</a></li>
                    </ul>
                    <form>
                        <div class="form-body">
                           <div class="row">
                                <div class="form-group has-success">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <select  class="form-control input-lg  select2me" data-placeholder="Elementos..." id="id_elemento_asignar">
                                                    <option value=""></option>
                                                    @foreach($elementos as $elementos)
                                                    <option value="{{$elementos->id}}">{{$elementos->id}} {{$elementos->EquipmentTypes->type}} {{$elementos->mark}} {{$elementos->serial}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <select  class="form-control input-lg  select2me" data-placeholder="N° Equipo..." id="n_equipo">
                                                    <option value=""></option>
                                                    @foreach($equipos as $equipo)
                                                    <option value="{{$equipo['id_equipo']}}">{{$equipo['id_equipo']}} {{$equipo['usuarios']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions right">                           
                           <a class="btn btn-success" onclick="asignar_elementos()">Registrar</a>                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    function asignar_elementos(){
        var elemento = $("#id_elemento_asignar").val();
        var equipo = $("#n_equipo").val();

        
        $.ajax({
                type: "GET",
                url:  "asignarelemento",
                data: { elemento: elemento, equipo: equipo}
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se Asigno correctamente el Elemento', 'Asinación de elemento');
                
                $("#id_elemento_asignar").val("");
                $("#n_equipo").val("");
                
            }else{
                toastr.error('No ha sido posible Signar el Elemento', 'Error');
            }
        });
    }
</script>
