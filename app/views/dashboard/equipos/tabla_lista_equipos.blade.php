

<a href="#" class="btn btn-default">Número de Registros {{$numero_equipos}}</a>
<a href="pdflistaqrs" class="btn btn-info pull-right" target="_blanck">Ver QRS</a>
<table class="table table-striped table-hover top-blue">
   <thead>
   <tr>
         <th>Imagen</th>
         <th>Usuario</th>
         <th>Marca  </th>
         <th>Tipo  <a href="#" onclick="tabla_lista_equipos(1,{{$data_filtros['num_order_tipo']}},{{$data_filtros['num_order_nombre']}})"><i class="{{$data_filtros['icon_order_tipo']}}" style="color:fff"></i></a></th>
         <th>Nombre Equipo <a href="#" onclick="tabla_lista_equipos(2,{{$data_filtros['num_order_tipo']}},{{$data_filtros['num_order_nombre']}})"><i class="{{$data_filtros['icon_order_nombre']}}" style="color:fff"></i></a></th>
         <th>Sede</th>
         <th><a href="pdflistaequipos?code={{Input::get('code')}}&applicant={{Input::get('applicant')}}&marca={{Input::get('marca')}}&f_compra={{Input::get('f_compra')}}&tipo_equipo={{Input::get('tipo_equipo')}}&order_tipo={{Input::get('order_tipo')}}" target="_blanck" class="btn btn-default"><i class="icon-print"></i></a></th>

         
      </tr>
   </thead>
   <tbody class="center">                         
  
        @foreach ($data as $row)
        <tr>
            <td>                               
                <a href="#basic" data-toggle="modal" class="btn btn-default" onclick="detalle({{$row['id_user']}}, {{$row['id_equipo']}})">
                    <img src="../{{$row['img_equipo']}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                </a>   
            </td>
            <td>
                <!-- <a href="#" class="nameimg icon-btn" title="{{$row['user']}}" >
                  <img alt="" style="border-radius:50px;" width="30" height="0"  src="../{{$row['img_user']}}">
                </a> -->
                <span class="label label-sm {{$row['label_user']}} ">
                    {{$row['user']}} {{$row['id_equipo']}}
                </span>

            </td>
            <td>{{$row['marca']}}</td>
            <td>{{$row['tipo']}}</td>
            <td>{{$row['name_equipo']}}</td>
            <td>{{$row['sede']}}</td>   
            <td>
            @if($row['tipo'] == "CPU" || $row['tipo'] == "PORTATIL" || $row['tipo'] == "ALL IN ONE")
                <a href="generarpdfequipo?id={{$row['id_user']}}&id_equipo={{$row['id_equipo']}}&id_elemento={{$row['id_elemento']}}" class="btn btn-info" target="_blank" {{$row['disabled']}} ><i class="icon-print"></i></a>
            @endif  
                  <!-- {{$row['id_elemento']}} -->    
            </td>
        </tr>
        @endforeach
   </tbody>
</table>
<div class="pagination">
  {{$pag->appends(array("code" => Input::get('code'),"applicant" => Input::get('applicant'),"marca" => Input::get('marca'),"f_compra" => Input::get('f_compra'),"tipo_equipo" => Input::get('tipo_equipo'),"order_tipo" => Input::get('order_tipo'),"order_nombre" => Input::get('order_nombre')))->links()}}
</div>

