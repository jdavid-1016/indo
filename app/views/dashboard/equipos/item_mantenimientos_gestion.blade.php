<div class="col-md-12">
   <div class="form-group">
      	<div class="col-md-6">
	        @foreach($tipos as $tipo)
	      	<div class="input-group">
	         	<input type="checkbox" class="toggle" id="item_{{$tipo->id}}"> {{$tipo->item}}
	      	</div>
	        @endforeach
	    </div>
	    
   </div>
</div>

<script type="text/javascript">
	function enviar_mantenimientos_final(id){ 
	    var itmes = new Array();
	    var i = 0;
	    var ids = new Array();
	    var estados = new Array();
	    var descripcion = $("#observation_mantenimiento").val();
	    var tipo_mant_asignar = $("#id_tipo_mant_asignar").val();

	    @foreach($tipos as $tipo)

	    	if($("#item_{{$tipo->id}}").is(':checked')) {  
	    		ids[i] = {{$tipo->id}};
	    	    
	    	} else {
	    		@if($tipo->required == "1")
	    			toastr.error('El Item {{$tipo->item}} es Requerido para este Tipo de Mantenimiento', 'Error'); 
	    			return;        
		    	@endif
	    	}
	    	i++;
		@endforeach
		var descripcion = $("#observation_mantenimiento").val();
		if (descripcion == "") {
			toastr.error('El campo Comentario es Requerido.', 'Error'); 
			return;
		}
	    $.ajax({
	        type: "POST",
	        url:  "enviarmantenimientos",
	        data: { id_details: ids, descripcion:descripcion, id:id, tipo_mant_asignar:tipo_mant_asignar}
	    })
	    .done(function(data) {
	        toastr.success('Se ingreso correctamente el Mantenimiento', 'Nuevo Mantenimiento');
	        $("#close_modal_mantenimiento").click();
	    });
	      
	}
</script>