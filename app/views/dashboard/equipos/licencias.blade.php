    <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <i class="icon-reorder"></i> Licencias
                  <div class="actions">
                                <div class="btn-group" data-toggle="buttons">
                                    <a href="#nuevalicencia" data-toggle="modal" id="">
                                    <label class="btn btn-success btn-sm">
                                        <span class="icon-sort-by-attributes-alt"></span>
                                        Nueva Licencia
                                   </label>
                                        </a>
                                </div>
                            </div>
               </div>
               <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  
                  <li class="active"><a href="licencias" >Licencias</a></li>
                  <!-- <li class=""><a href="licenciasequipos" >Equipos Con Licencia</a></li> -->
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
               </ul>
                  <div class="table-responsive">
                    <div>
                       <form class="form-inline" method="get">
                          <div class="search-region">
                             <div class="form-group">
                                <input type="text" class="form-control" id="code" name="code" placeholder="Id" onkeyup="recargar_lista_licencias()">
                             </div>
                             <div class="form-group">
                                <select class="form-control input-medium" id="producto" onchange="recargar_lista_licencias()">
                                    <option value="">Producto</option> 
                                    
                                    @foreach($productos as $producto)
                                    @if($producto->product == Input::get('producto'))
                                       <option value="{{$producto->product}}" selected="">{{$producto->product}}</option>
                                    @else
                                       <option value="{{$producto->product}}">{{$producto->product}}</option>
                                    @endif  
                                    @endforeach
                                </select>
                             </div>
                             <div class="form-group">
                                <select class="form-control input-medium" id="version" onchange="recargar_lista_licencias()">
                                    <option value="">Versión</option> 
                                     @foreach($versiones as $version)
                                     @if($version->version == Input::get('version'))
                                        <option value="{{$version->version}}" selected="">{{$version->version}}</option>
                                     @else
                                        <option value="{{$version->version}}">{{$version->version}}</option>
                                     @endif  
                                     @endforeach
                                </select>
                             </div>
                             <!-- <div class="form-group">
                                <input type="text" class="form-control fecha_required" id="f_compra" name="f_compra" placeholder="Fecha Expiración">
                             </div> -->
                             <!-- <div class="form-group">
                                <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                             </div>   -->  
                          </div>
                       </form> 
                    </div>
                    <div id="tabla_lista_licencias">
                        @include('dashboard.equipos.tabla_lista_licencias')
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
     
   </div>
</div>

<div class="modal fade" id="nuevalicencia" tabindex="-1" role="basic" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content">
         <form class="form" action="#">
            <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h4 class="modal-title">Ingresar Licencia</h4>
            </div>
            <div class="modal-body form-body" id="form_licencias">
               <div class="row">
                   <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">ID Licencia</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-edit"></i></span>
                           <input type="text" class="form-control" placeholder="" data-required="true" name="idlicencia_n" id="idlicencia_n" value="">
                        </div> 
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Producto</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-edit"></i></span>
                           <select name="producto_n" id="producto_n" class="form-control">
                               @foreach($tipos as $tip)
                                  <option  value="{{$tip->id}}">{{$tip->product}}</option>
                               @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="row">
                   <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Proveedor</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-edit"></i></span>
                           <input type="text" class="form-control" placeholder="" data-required="true" name="provider_n" id="provider_n" value="">
                        </div> 
                     </div>
                  </div>
                   
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Version</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-edit"></i></span>
                           <input type="text" class="form-control" placeholder="" data-required="true" name="version_n" id="version_n" value="">
                        </div>
                     </div>
                  </div>
                  
               </div>
                <div class="row">
                   <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Fecha Compra</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-edit"></i></span>
                           <input type="text" class="form-control fecha_required" placeholder="" data-required="true" name="fechacompra_n" id="fechacompra_n" value="">
                        </div> 
                     </div>
                  </div>
                   
                  <div class="col-md-6">
                     <div class="form-group">
                        <label class="control-label">Fecha Expiracion</label>
                        <div class="input-group">
                           <span class="input-group-addon"><i class="icon-edit"></i></span>
                           <input type="text" class="form-control fecha_required" placeholder="" data-required="true" name="fechaexpiracion_n" id="fechaexpiracion_n" value="">
                        </div>
                     </div>
                  </div>
                  
               </div>
                <div class="row">
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Cantidad</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="text" class="form-control" placeholder="" data-required="true" name="cantidad_n" id="cantidad_n" value="">
                          </div>
                       </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                          <label class="control-label">Factura</label>
                          <div class="input-group">
                             <span class="input-group-addon"><i class="icon-edit"></i></span>
                             <input type="file" class="form-control">
                          </div>
                       </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-6">
                    <h3><strong>Seriales</strong></h3>
                    </div>
                    <div class="col-md-3"></div>
                    </div>

            </div>
             
            <div class="modal-footer">
            
                  <button type="button" class="btn btn-info" id="botonserial" onclick="NuevoSerial()"><i id="icon" class="icon-ok"></i>Agregar Serial</button>
                  <button type="button" class="btn btn-success rechazar_solicitud" id="botonguardarnuevo" onclick="guardarNuevaLicencia()"><i id="icon" class="icon-ok"></i>Guardar</button>
               
               <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
            </div>
         </form>
      </div>
  </div>
                        <!-- /.modal-dialog -->}
</div>

<div class="modal fade" id="ajax2" tabindex="-1" role="basic" aria-hidden="true">   
</div>

<script type="text/javascript">
   function cargar_detalle_licencia(id) {
      
      var parametros = {
         "id": id
         
      };
      $.ajax({
         data: parametros,
         url:  'detallelicencia',
         type: 'get',

         success: function(response){
              
            $("#ajax2").html(response);
         }
      });

   }
   
   
var serial = new Array();
var tserial = 0;
        
function NuevoSerial(){
    var validarserial = $("#prodserial_"+tserial).val();
    var validarserial2 = $("#serial_"+tserial).val();
    if(validarserial == "" || validarserial2=="" ){
        toastr.error('Debe llenar los datos del serial, para agregar otro', 'Error');
        return;
    }
    tserial = tserial+1;
    
    $("#form_licencias").append('<div class="row deleteseriales"><div class="col-md-6"><div class="form-group"><label class="control-label">Producto</label><div class="input-group"><span class="input-group-addon"><i class="icon-edit"></i></span><input type="text" class="form-control" placeholder="" data-required="true" name="prodserial_'+tserial+'" id="prodserial_'+tserial+'" value=""></div></div></div><div class="col-md-6"><div class="form-group"><label class="control-label">Serial</label><div class="input-group"><span class="input-group-addon"><i class="icon-edit"></i></span><input type="text" class="form-control" placeholder="" data-required="true" name="serial_'+tserial+'" id="serial_'+tserial+'" value=""></div></div></div></div>');
    
}

function guardarNuevaLicencia() {

    var idlicencia = $("#idlicencia_n").val();
    var producto = $("#producto_n").val();
    var proveedor = $("#provider_n").val();
    var version = $("#version_n").val();
    var cantidad = $("#cantidad_n").val();
    var fecha_compra = $("#fechacompra_n").val();
    var fecha_expiracion = $("#fechaexpiracion_n").val();

    if (tserial > 0) {

        if (idlicencia != "",producto != "",proveedor != "",version != "",cantidad != "") {
            
            for(var i=1;tserial>=i;i++){
             serial.push(new Array($("#prodserial_"+i).val(), $("#serial_"+i).val()));
            }

            $.ajax({
                type: "GET",
                url: "nuevalicencia",
                data: {idlicencia: idlicencia, producto: producto, proveedor: proveedor, version: version, cantidad: cantidad, fecha_compra: fecha_compra, fecha_expiracion: fecha_expiracion, serial: serial }
            }).done(function (data) {
                if (data == "0") {
                    toastr.error('No ha sido posible guadar su nueva licencia', 'Error');
                } else {
                    
                    $("#justificacion").val(" ");
                    $("#idlicencia_n").val(" ");
                    $("#producto_n").val(" ");
                    $("#provider_n").val(" ");
                    $("#version_n").val(" ");
                    $("#cantidad_n").val(" ");
                    $("#fechacompra_n").val(" ");
                    $("#fechaexpiracion_n").val(" ");
                    
                    $(".deleteseriales").remove();
                    serial = new Array();                    
                    tserial = 0;
                    toastr.success('Su nueva licencia ha sido guardada.', 'Nueva Licencia');
                }                                    
            });

        } else {
            toastr.error('Todos los campos de la licencia son obligatorios.', 'Error');
        }
        
    } else {
        toastr.error('Debe ingresar al menos un serial.', 'Error');
    }

}
function recargar_lista_licencias(){
    var producto = $("#producto").val();
    var version = $("#version").val();
    var code    = $("#code").val();

    $.get('licencias', {
        producto:producto,
        version:version,
        code:code,
        nombre: 1
    } ,

    function(response){
        $('#tabla_lista_licencias').html(response);
        //var datos = JSON.parse(response);
    });
}
</script>