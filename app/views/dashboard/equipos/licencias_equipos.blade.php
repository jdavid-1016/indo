    <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <i class="icon-reorder"></i> Ingresar Mantenimientos
               </div>
               <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  
                  <li class=""><a href="licencias" >Licencias</a></li>
                  <li class="active"><a href="licenciasequipos" >Equipos Con Licencia</a></li>
                  
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
                  <div class="table-responsive" id="tabla_ver_mantenimientos">
                  <div>
                     <form class="form-inline" action="vermantenimientos" method="get">
                        <div class="search-region">
                           <div class="form-group">
                              <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                           </div>
                           <div class="form-group">
                              <select class="form-control input-medium" name="applicant">
                              <option value="">Usuario</option> 
                               @foreach($applicants as $applicant)
                               @if($applicant->id == Input::get('applicant'))
                                  <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                               @else
                                  <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                               @endif  
                               @endforeach
                              </select>
                           </div>
                           <div class="form-group">
                              <select class="form-control input-medium" name="marca">
                              <option value="">Marca</option> 
                               @foreach($marcas as $marca)
                               @if($marca->mark == Input::get('marca'))
                                  <option value="{{$marca->mark}}" selected="">{{$marca->mark}}</option>
                               @else
                                  <option value="{{$marca->mark}}">{{$marca->mark}}</option>
                               @endif  
                               @endforeach
                              </select>
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control fecha_required" id="f_compra" name="f_compra" placeholder="Fecha Compra">
                           </div>
                           <div class="form-group">
                              <input type="text" class="form-control" id="serial" name="serial" placeholder="Serial" value="{{Input::get('serial')}}">
                           </div>
                           <div class="form-group">
                              <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                           </div>    
                        </div>
                     </form> 
                  </div>
                    <table class="table table-striped table-hover top-blue">
                        <thead>
                            <tr>
                              
                                <th>Fecha <a href="/indoamericana/intranet/listaEmpleados/last_name/asc"><i class="icon-sort"></i></a></th>
                                <th>Usuario</th>
                                <th>tipo</th>
                                <th>Observaciones <a href="/indoamericana/intranet/listaEmpleados/id_process/asc"><i class="icon-sort"></i></a> </th>
                                <th>Equipo</th>
                                <th>Técnico</th>
                              
                            </tr>
                        </thead>
                        <tbody class="center">                         
                            @foreach ($mantenimientos as $row)
                            <tr>
                                <td>{{$row['fecha']}}</td>
                                <td>{{$row['asign']}}</td>
                                <td>
                                    <span class="label label-sm label-success informacion" title="{{$row['traces']}}">
                                    {{$row['tipo']}}
                                    </span>
                                </td>
                                <td>{{$row['observaciones']}}</td>
                                <td>{{$row['equipo']}}</td>
                                <td>
                                    <a href="#" class="nameimg" title="{{$row['tecnico']}}">
                                    <img alt="" style="border-radius:50px;" width="50" height="50"  src="../{{$row['img']}}">
                                    </a>

                                </td>
                            </tr>
                            @endforeach
                           
                        </tbody>
                    </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<script type="text/javascript">
        function tabla_lista_equipos(){


          var code = $("#code").val();
          var applicant = $("#applicant").val();
          var marca = $("#marca").val();
          var f_compra = $("#f_compra").val();
          var serial = $("#serial").val();



            $.get('vermantenimientos', {
                code: code,
                applicant: applicant,
                marca: marca,
                f_compra: f_compra,
                serial: serial,
                nombre: 1
            } ,

            function(response){
                $('#tabla_ver_mantenimientos').html(response);
                //var datos = JSON.parse(response);
            });
        }
    
</script>
