    
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Administrador</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class=""><a href="administrador">Tipos de Mantenimientos</a></li>
                        <li class="active"><a href="adminitems">Items de Mantenimientos</a></li>
                        <li class=""><a href="admigrupos">Grupos de Equipos</a></li>
                        <li class=""><a href="adminaddequipos">Agregar Equipos A Un Grupo</a></li>
                        <li class=""><a href="adminelimequipos">Eliminar Equipos De Un Grupo</a></li>

                        
                    </ul>
                    <div  class="tab-content">
                        
                      <a class="btn btn-info" href="#modal_nuevo_item" data-toggle="modal"><i class="icon-ok"></i> Nuevo Item</a>
                        <div class="tab-pane fade active in" id="tab_1_3">
                            <div class="table-responsive" id="table_admin_">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-edit"></i>Nuevo Item de Mantenimiento</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                    <a href="javascript:;" class="reload"></a>
                                                    <a href="javascript:;" class="remove"></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="form-group"> 
                                                    <label class="control-label">Tipo de Mantenimiento al que pertenece el nuevo Item</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select class="form-control" placeholder="" data-required="true" name="name_n" id="item_tipo_mantenimiento" onchange="cargar_items_de_namt()">
                                                            <option></option>
                                                            @foreach($tipo_mantenimientos as $tipo_mantenimiento)
                                                            <option value="{{$tipo_mantenimiento->id}}">{{$tipo_mantenimiento->type_maintenance}}</option>
                                                            @endforeach()
                                                        </select>
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="control-label">Item de Mantenimiento</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select class="form-control" placeholder="" data-required="true" id="item_mantenimiento" value="">
                                                            <option value=""></option>
                                                            @foreach($item_mantenimientos as $items)
                                                            <option value="{{$items->id}}">{{$items->item}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                                
                                                <a class="btn btn-success" onclick="crear_item_mantenimiento()"><i class="icon-ok"></i> Crear</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9"> 
                                                          
                                        <div class="portlet">
                                          <div class="portlet-title">
                                             <div class="caption"><i class="icon-cogs"></i>Items de Mantenimientos</div>
                                             <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                             </div>
                                          </div>
                                          <div class="portlet-body">
                                             <div class="table-responsive">
                                                <table class="table ">
                                                   <thead>
                                                       <tr>
                                                           <th>Id</th>
                                                           <th>Item Mantenimiento</th>
                                                           <th>Tipo Mantenimiento</th>
                                                           <th>Requerido</th>
                                                           <th>Fecha Creación</th>
                                                           <!-- <th>Opciones</th> -->
                                                       </tr>
                                                   </thead>
                                                   <tbody id="tabla_item_mantenimientos">
                                                   
                                                   </tbody>
                                               </table>
                                             </div>
                                          </div>
                                        </div>
                                    </div>
                                      
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_nuevo_item" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog " ng-controller="help_deskCtrl" ng-app>
        <div class="modal-content">
        <form class="form" action="#" ng-submit="adminSupport()">
       
            <input type="hidden" ng-model="support_id" value="" id="support_id">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Nuevo Item de Mantenimiento</h4>
            </div>
            <div class="modal-body form-body">
                <div class="form-group">
                    <label>Item de mantenimiento</label> <br>  
                    <input class="tag form-control clear-border input-medium" type="text" value="" id="campo_nuevo_item">
                </div>
                <div class="form-group">
                    <label>Requerido</label>   <br>
                    <select class="tag form-control clear-border input-medium" type="text" value="" id="campo_requerido">
                    <option value="1">Si</option>
                    <option value="2">No</option>
                    </select>
                </div>
                
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" onclick="nuevo_item_mantenimiento()"><i class="icon-ok"></i> Crear Item</a>
                <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_mantenimiento">Cerrar</button>
            </div>
               
        </form>   
        </div>
    </div>

</div>




