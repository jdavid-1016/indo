<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Mantenimientos de Equipos</h4>
      </div>
      <div class="modal-body form-body">
         

                <div class="portlet">
                   <div class="portlet-title">
                     <div class="caption">Información General</div>
                     <div class="tools">
                       <a href="javascript:;" class="expand"></a>
                     </div>
                   </div>
                   <div class="portlet-body ">
                     <div class="row">
                       <div class="col-md-6">
                         <input type="hidden" value="1" name="tipo-equipo">
                         <table class="table middle well">
                           <tbody>
                             <tr>
                               <td>Funcionario</td>
                               <td>
                                 <span class="tag">
                                    <select name="usuario" id="" class="form-control input-sm clear-border" readonly="">
                                        <option>{{$users[0]->name}} {{$users[0]->name2}} {{$users[0]->last_name}} {{$users[0]->last_name2}}</option>
                                    </select>
                                 </span>
                                 <span class="test-img"><img class="inside-avatar" src="../{{$users[0]->img_min}}"></span>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>
                             <tr>
                                <td>Tipo de Equipo</td>
                                <td>
                                 {{$equipo->EquipmentTypes->type}}
                                </td>
                             </tr>
                             <tr>
                               <td>Imagen</td>
                               <td>
                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        @if($equipo->image == "")
                                            <img src="../assets/img/equipos/-1.png" alt="">
                                        @else
                                            <img src="../{{$equipo->image}}" alt="">
                                        @endif
                                   </div>
                                   <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                   <div>
                                       <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Eliminar</a>
                                   </div>
                                 </div>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                        @endif
                        @endforeach
                       </div>
                       <div class="col-md-6">
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>  
                             <tr>
                               <td>Marca</td>
                               <td><input name="marca" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->mark}}"></td>
                             </tr>
                             <tr>
                               <td>Modelo</td>
                               <td><input name="modelo" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->model}}"></td>
                             </tr>
                             <tr>
                               <td>Serial</td>
                               <td><input name="serial" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->serial}}"></td>
                             </tr>
                             <tr>
                               <td>Entrega del Equipo</td>
                               <td><input name="entrega" class="tag form-control input-medium clear-border"  readonly="" value="{{$equipo->date_asign}}"></td>
                             </tr>
                             <tr>
                               <td>Estado</td>
                               <td><input name="estado" class="tag form-control clear-border input-medium" type="text"readonly=""  value="{{$equipo->EquipmentStatuses->status}}"></td>
                             </tr>
                           </tbody>
                         </table>
                         @endif
                         @endforeach
                       </div>
                     </div>
                   </div>
                </div>
                
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">Mantenimientos </div>

                        <div class="tools">
                            <a href="javascript:;" class="expand"></a>
                        </div>
                    </div>
                    <div class="portlet-body ">
                        <div class="row" id="tabla_mant">
                            <div class="col-md-12">
                                <table class="table  top-blue">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>FECHA</th>
                                            <th>DETALLE</th>
                                            <th>TIPO MANT</th>
                                            <th>ESTADO</th>
                                            <th>USUARIO</th>
                                        </tr>
                                    </thead>
                                    <tbody class="center">                         
                                  
                                        @foreach ($mantenimientos as $mant)
                                        <tr @if($mant->equipment_maintenances_statuses_id == 1) class="red" @else class="green" @endif>
                                            <td>{{'MANT' . str_pad($mant->id, 6, "0", STR_PAD_LEFT)}}</td> 
                                            <td>{{$mant->created_at}}</td> 
                                            <td>{{$mant->detalle}}</td> 
                                            <td>{{$mant->EquipmentTypeMaintenances->type_maintenance}}</td> 
                                            <td>{{$mant->EquipmentMaintenancesStatuses->status}}</td> 
                                            <td>@if($mant->users_id != "") 
                                                  <span class="test-img"><img class="inside-avatar" src="../{{$mant->Users->img_min}}">{{$mant->Users->name}} {{$mant->Users->last_name}} </span>
                                                  
                                                @endif
                                            </td> 
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                
         
      </div>
      <div class="modal-footer">
         
         <!-- <button type="button" class="btn btn-success refresh" onclick="enviar_mantenimientos_final({{$equipos_datos[0]->id}})">Ingresar</button> -->
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_mantenimiento">Cerrar</button>
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>


      <script type="text/javascript">
        
        function cargar_checkbox_items(){
            var id_tipo = $("#id_tipo_mant_asignar").val();



            $.ajax({
                type: "GET",
                url:  "mostraritemsmantenimientogestion",
                data: { id_tipo: id_tipo }
            })
            .done(function(data) {
                
                 $("#items_mantenimientos").html(data);
            });
        }


      </script>