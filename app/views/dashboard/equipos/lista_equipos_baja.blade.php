    <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <i class="icon-reorder"></i> Ver Equipos
               </div>
               <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  
                  <li class=""><a href="verequipos" >Ver Equipos</a></li>
                  <li class=""><a href="inactivos" >Sin Asignar</a></li>
                  <li class="active"><a href="debaja" >De Baja</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  <a href="#"  class="btn btn-info pull-right" onclick="mostrar_formulario_baja()" id="btn-activar">Dar Equipo de Baja</a>
                  <button type="button" class="btn btn-default pull-right" onclick="ocultar_formulario_baja()" style="display:none" id="btn-desac">Cancelar</button>
               </ul>
                  <div class="table-responsive" id="tabla_equipos_de_baja">
                      <div>
                         <form class="form-inline" action="debaja" method="get">
                            <div class="search-region">
                               <div class="form-group">
                                  <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                               </div>
                               
                               <div class="form-group">
                                  <select class="form-control input-medium" name="marca">
                                  <option value="">Marca</option> 
                                   @foreach($marcas as $marca)
                                   @if($marca->mark == Input::get('marca'))
                                      <option value="{{$marca->mark}}" selected="">{{$marca->mark}}</option>
                                   @else
                                      <option value="{{$marca->mark}}">{{$marca->mark}}</option>
                                   @endif  
                                   @endforeach
                                  </select>
                               </div>
                               <div class="form-group">
                                  <input type="text" class="form-control fecha_required" id="f_compra" name="f_compra" placeholder="Fecha Compra">
                               </div>
                               
                               <div class="form-group">
                                  <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                               </div>
                               <div class="form-group">
                                   
                                       <select  class="form-control input-lg  select2me" data-placeholder="Equipo..." id="id_equipo_asignar">
                                           <option value=""></option>
                                           @foreach($equipos as $equipo)
                                           <option value="{{$equipo->id}}">{{$equipo->id}} {{$equipo->mark}} {{$equipo->name}} {{$equipo->last_name}}</option>
                                           @endforeach
                                       </select>
                                   
                               </div>
                                   
                               
                            </div>
                         </form> 
                      </div>
                    <table class="table table-striped table-hover top-blue">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Fecha De Baja <a href="/indoamericana/intranet/listaEmpleados/last_name/asc"><i class="icon-sort"></i></a></th>
                                <th>Marca</th>
                                <th>Modelo <a href="/indoamericana/intranet/listaEmpleados/id_process/asc"><i class="icon-sort"></i></a> </th>
                                <th>Serial</th>
                                <th>Archivo</th>
                            </tr>
                        </thead>
                        <tbody class="center">                         
                            @foreach ($administradores as $row)
                                <tr>
                                    <td>                               
                                        <a href="#basic" data-toggle="modal" class="btn btn-default" onclick="detalle({{$row->id}})">
                                            @if($row->image == "")
                                              
                                            <img src="../assets/img/equipos/-1.png" width="45"  height="40" >
                                            @else
                                              <img src="../{{$row->image}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                                            @endif
                                        </a>   
                                    </td>
                                    <td>{{$row->updated_at}}</td>
                                    <td>{{$row->mark}}</td>
                                    <td>{{$row->model}}</td>
                                    <td>{{$row->serial}}</td>
                                    <td><a href="../{{$row->decommissioned}}" target="_blac" class="btn btn-info"><i class="icon-file"></i></a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                  </div>
                    <div id="formulario_equipos_de_baja" class="has-success" style="display:none">
                        <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <select  class="form-control input-lg  select2me" data-placeholder="Equipo..." id="id_equipo_de_baja">
                                    <option value=""></option>
                                    @foreach($equipos as $equipo)
                                    <option value="{{$equipo->id}}">{{$equipo->id}} {{$equipo->mark}} {{$equipo->name}} {{$equipo->last_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        
                            
                            <label>Formato:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                <input type="file" id="archivo_dar_de_baja" name="archivo_dar_de_baja" class="form-control">
                            </div> 
                                
                            <div class="form-actions right">                           
                                <button type="button" class="btn btn-success refresh" onclick="dar_equipo_de_baja()">Ingresar</button>
                                <button type="button" class="btn btn-info refresh" onclick="ocultar_formulario_baja()" id="close_modal">Cancelar</button>
                                
                            </div>
                        </div>
                        </div>
                        
                    </div>

               </div>
            </div>
         </div>
      </div>
      
     
   </div>
</div>

            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            </div> 
            
<script>
    function detalle(id){
        
        $.post('detalleinactivos', {
            id:id
        } ,

        function(response){
            $('#basic').html(response);
                 
        });
    }


    function mostrar_formulario_baja(){

        $("#tabla_equipos_de_baja").hide('show');
        $("#formulario_equipos_de_baja").show('show');
        $("#btn-activar").hide('show');
        $("#btn-desac").show('show');
    }
    function ocultar_formulario_baja(){

        $("#formulario_equipos_de_baja").hide('show');
        $("#tabla_equipos_de_baja").show('show');
        $("#btn-activar").show('show');
        $("#btn-desac").hide('show');
    }
    function dar_equipo_de_baja(){
        var id_equipo_de_baja = $("#id_equipo_de_baja").val();
        var archivo_dar_de_baja = $("#archivo_dar_de_baja").val();

        if(id_equipo_de_baja == ""){
            toastr.error("El campo Equipo no puede estar vacio.", "Error");
            return;
        }
        if(archivo_dar_de_baja == ""){
            toastr.error("Debe seleccionar un archivo para dar el Equipo de Baja.", "Error");
            return;
        }

        $("#archivo_dar_de_baja").upload('darequipodebaja', 
        {   
            id_equipo: id_equipo_de_baja
            
            
        },
        function(respuesta) {
            if (respuesta == 1) {
                toastr.success('Se Actuzlizó correctamente la infmormación', 'Actualización de Información');
                ocultar_formulario_baja();
                $("#id_equipo_de_baja").val("");
                $("#archivo_dar_de_baja").val("");
                    
            } else {
                toastr.error('No ha sido posible Actuzlizar La infmormación', 'Error');
                    
            }
                                            
        }, function(progreso, valor) {
                //Barra de progreso.
                
        });
        
    }
</script>