
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Administrador</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class="active"><a href="administrador">Tipos de Mantenimientos</a></li>
                        <li class=""><a href="adminitems">Items de Mantenimientos</a></li>
                        <li class=""><a href="admigrupos">Grupos de Equipos</a></li>
                        <li class=""><a href="adminaddequipos">Agregar Equipos A Un Grupo</a></li>
                        <li class=""><a href="adminelimequipos">Eliminar Equipos De Un Grupo</a></li>

                        
                    </ul>
                    <div  class="tab-content">
                        
                        <div class="tab-pane fade active in" id="tab_1_2">
                            <div class="table-responsive" id="table_admin_">
                                <div class="row">
                                    <div class="col-md-6">                            
                                        <div class="portlet">
                                          <div class="portlet-title">
                                             <div class="caption"><i class="icon-cogs"></i>Tipos de Mantenimientos</div>
                                             <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                             </div>
                                          </div>
                                          <div class="portlet-body">
                                                <button class="btn btn-defaul" type="button" >
                                                    <input type="checkbox" id="check_mantenimientos" onchange="mostrar_inactivos_mantenimientos()"> Mostrar Inactivos
                                                </button>
                                                <a class="btn btn-info" onclick="mostrar_form_agregar_grupo(1)" id="btn_mostrar_form"><i class="icon-ok"></i> Asignar a un Grupo</a>
                                             <div class="table-responsive">
                                                <div id="tabla_form_asign_grupos" style="display:none">
                                                    <div class="form-group"> 
                                                        <label class="control-label">Tipo de Mantenimiento</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <select class="form-control" placeholder="" data-required="true" id="tipo_mantenimiento2" value="">
                                                                <option value=""></option>
                                                                @foreach($tipo_mantenimientos as $tipo_mantenimiento)
                                                                <option value="{{$tipo_mantenimiento->id}}">{{$tipo_mantenimiento->type_maintenance}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="form-group"> 
                                                        <label class="control-label">Grupo de Equipos a los que aplica</label>
                                                        <div class="input-group">
                                                            
                                                            
                                                            @if(count($grupos) == 0)
                                                                <div class="alert alert-warning alert-dismissable">
                                                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                    <strong>Info!</strong> No hay nungún grupo Disponible.
                                                                </div>
                                                            @else
                                                                @foreach($grupos as $grupo)
                                                                    <div id="grupo_{{$grupo->id}}">
                                                                        <input type="checkbox" value="{{$grupo->id}}" id="grupo2_{{$grupo->id}}">{{$grupo->name_group}}<br>
                                                                    </div>   
                                                                @endforeach
                                                            @endif
                                                            
                                                        </div> 
                                                    </div>
                                                    <a class="btn btn-success" onclick="crear_tipo_mantenimiento3()"><i class="icon-ok"></i> Crear</a>
                                                </div>
                                               <table class="table" id="tabla_tipos_mantenimientos">
                                                   <thead>
                                                        <tr>
                                                            <th>Id</th>
                                                            <th>Fecha Creación</th>
                                                            <th>Tipo Mantenimiento</th>
                                                        </tr>
                                                   </thead>
                                                   <tbody id="tabla_tipo_mantenimientos">
                                                   @foreach($tipo_mantenimientos as $tipo_mantenimiento)
                                                        <tr @if($tipo_mantenimiento->status == "0") class="red" @endif >
                                                            <td>{{$tipo_mantenimiento->id}}</td>
                                                            <td style="min-width:150px">{{$tipo_mantenimiento->created_at}}</td>
                                                            <td>
                                                                <div class="input-group" id="id_mantenimiento_{{$tipo_mantenimiento->id}}">
                                                                    <input type="text" class="form-control" value="{{$tipo_mantenimiento->type_maintenance}}" id="texto_{{$tipo_mantenimiento->id}}" onblur="editar_mantenimiento({{$tipo_mantenimiento->id}})" onfocus="capturar_valor({{$tipo_mantenimiento->id}})">
                                                                    <span class="input-group-btn">
                                                                    
                                                                    <button class="btn btn-danger" type="button" onclick="eliminar_mantenimiento({{$tipo_mantenimiento->id}}, @if($tipo_mantenimiento->status == "0") 1 @else 0 @endif)">
                                                                        <i class="icon-trash"></i>
                                                                    </button>
                                                                    <!-- <button class="btn btn-success" type="button" onclick="editar_mantenimiento({{$tipo_mantenimiento->id}})">
                                                                        <i class="icon-edit"></i>
                                                                    </button> -->
                                                                    </span>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                   @endforeach
                                                   </tbody>
                                               </table>

                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-edit"></i>Nuevo tipo de  Mantenimiento</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                    <a href="javascript:;" class="reload"></a>
                                                    <a href="javascript:;" class="remove"></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="form-group"> 
                                                   <label class="control-label">Tipo de Mantenimiento</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="tipo_mantenimiento" value="">
                                                   </div> 
                                                </div>
                                                <div class="form-group"> 
                                                    <label class="control-label">Grupo de Equipos a los que aplica</label>
                                                    <div class="input-group">
                                                        
                                                        
                                                        @if(count($grupos) == 0)
                                                            <div class="alert alert-warning alert-dismissable">
                                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                                                <strong>Info!</strong> No hay nungún grupo Disponible.
                                                            </div>
                                                        @else
                                                            @foreach($grupos as $grupo)
                                                                
                                                                <input type="checkbox" value="{{$grupo->id}}" id="grupo_{{$grupo->id}}">{{$grupo->name_group}}<br>
                                                            @endforeach
                                                        @endif
                                                        
                                                    </div> 
                                                </div>
                                                <div class="form-group"> 
                                                   <label class="control-label">Fecha Limite Para Mantenimiento</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control fecha_required" value="" id="fecha_limite">

                                                      
                                                   </div> 
                                                </div>
                                                <a class="btn btn-success" onclick="crear_tipo_mantenimiento2()"><i class="icon-ok"></i> Crear</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_asignar_grupo" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog " ng-controller="help_deskCtrl" ng-app>
        <div class="modal-content">
        <form class="form" action="#" ng-submit="adminSupport()">
       
            <input type="hidden" ng-model="support_id" value="" id="support_id">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Nuevo Item de Mantenimiento</h4>
            </div>
            <div class="modal-body form-body">
                <div class="form-group">
                    <label>Item de mantenimiento</label> <br>  
                    <input class="tag form-control clear-border input-medium" type="text" value="" id="campo_nuevo_item">
                </div>
                <div class="form-group">
                    <label>Requerido</label>   <br>
                    <select class="tag form-control clear-border input-medium" type="text" value="" id="campo_requerido">
                    <option value="1">Si</option>
                    <option value="2">No</option>
                    </select>
                </div>
                
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" onclick="nuevo_item_mantenimiento()"><i class="icon-ok"></i> Crear Item</a>
                <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_mantenimiento">Cerrar</button>
            </div>
               
        </form>   
        </div>
    </div>

</div>
<script type="text/javascript">
    function cargar_equios_del_grupo(){
        var id_grupo = $("#id_grupo_asign").val();
        var tipo_equipo  = $("#id_tipo_asign").val();
        $.ajax({
                type: "GET",
                url:  "cargarequiposdelgrupo",
                data: { id_grupo: id_grupo, tipo_equipo:tipo_equipo}
        })
        .done(function(data) {
            if (data != 1) {
                //toastr.success('Se Agrego correctamente el el Equipo al Grupo', 'Nuevo Asignación de Equipo');
                //$("#id_equipo_asing").val("");
                //$("#id_grupo_asign").val("");
                
                 $("#multiselect_equipos").html(data);
            }else{
                toastr.error('No ha sido posible ingresar el  Equipo al Grupo', 'Error');
            }
        });
    }
</script>

<script type="text/javascript">
    function cargar_equios_del_grupo2(){
        var id_grupo = $("#id_grupo_asign_elim").val();

        $.ajax({
                type: "GET",
                url:  "cargarequiposdelgrupo",
                data: { id_grupo: id_grupo, eliminar:1}
        })
        .done(function(data) {
            if (data != 1) {
                //toastr.success('Se Agrego correctamente el el Equipo al Grupo', 'Nuevo Asignación de Equipo');
                //$("#id_equipo_asing").val("");
                //$("#id_grupo_asign").val("");
                
                 $("#multiselect_equipos_eliminar").html(data);
            }else{
                toastr.error('No ha sido posible ingresar el  Equipo al Grupo', 'Error');
            }
        });
    }
</script>


<script type="text/javascript">
    function crear_tipo_mantenimiento2(){ 
        
        var i = 0;
        var ids = new Array();
        var mantenimiento = $("#tipo_mantenimiento").val();
        var fecha_limite = $("#fecha_limite").val();


        @foreach($grupos as $grupo)

            if($("#grupo_{{$grupo->id}}").is(':checked')) {  
                ids[i] = {{$grupo->id}};
                i++;  
            } 
        @endforeach
        
        if (mantenimiento == "") {
            toastr.error('El campo Tipo de Mantenimiento es Requerido.', 'Error'); 
            return;
        }
        $.ajax({
            type: "POST",
            url:  "creartipomantenimiento",
            data: { id_details: ids, mantenimiento:mantenimiento, fecha_limite:fecha_limite}
        })
        .done(function(data) {

            toastr.success('Se ingreso correctamente el Mantenimiento', 'Nuevo Mantenimiento');
            var mantenimiento = $("#tipo_mantenimiento").val("");
            var fecha_limite = $("#fecha_limite").val("");
            $("#tabla_tipo_mantenimientos").html(data);
        });
          
    }
</script>
<script type="text/javascript">
    function crear_tipo_mantenimiento3(){ 
        
        var i = 0;
        var ids = new Array();
        var mantenimiento = $("#tipo_mantenimiento2").val();
        


        @foreach($grupos as $grupo)

            if($("#grupo2_{{$grupo->id}}").is(':checked')) {  
                ids[i] = {{$grupo->id}};
                i++;  
            } 
        @endforeach
        
        if (mantenimiento == "") {
            toastr.error('El campo Tipo de Mantenimiento es Requerido.', 'Error'); 
            return;
        }
        $.ajax({
            type: "POST",
            url:  "asignarmantagrupo",
            data: { id_details: ids, mantenimiento:mantenimiento}
        })
        .done(function(data) {

            toastr.success('Se Asigno correctamente el Mantenimiento', 'Asignación de Mantenimiento');
            var mantenimiento = $("#tipo_mantenimiento").val("");
            var fecha_limite = $("#fecha_limite").val("");
            // $("#tabla_tipo_mantenimientos").html(data);
        });
          
    }
</script>


