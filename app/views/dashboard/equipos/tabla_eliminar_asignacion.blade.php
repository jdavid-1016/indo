<div class="row">
    <div class="col-md-9">
        <h2>Elementos del equipo</h2>
        <table class="table top-blue">
           <thead>
           <tr>
                 <th>Imagen</th>
                 <th>Usuario</th>
                 <th>Marca</th>
                 <th>Tipo</th>
                 <th>Estado</th>
                 <th><i class="icon-remove"></i></th>

                 
              </tr>
           </thead>
           <tbody class="center">                         
          
                @foreach ($data as $row)
                <tr id="id_elemento_{{$row['id_equipo_asignacion']}}"  class="{{$row['row']}}">
                    <td>                               
                        <a href="#basic" data-toggle="modal" class="btn btn-default" onclick="detalle({{$row['id_user']}}, {{$row['id_equipo']}})">
                            <img src="../{{$row['img_equipo']}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                        </a>   
                    </td>
                    <td>
                        <!-- <a href="#" class="nameimg icon-btn" title="{{$row['user']}}" >
                          <img alt="" style="border-radius:50px;" width="30" height="0"  src="../{{$row['img_user']}}">
                        </a> -->
                        <span class="label label-sm {{$row['label_user']}} ">
                            {{$row['user']}} 
                        </span>

                    </td>
                    <td>{{$row['marca']}}</td>
                    <td>{{$row['tipo']}}</td>
                    
                    <td>{{$row['status']}}</td>   
                    <td>
                        <a  href="#basic2"  data-toggle="modal" class="btn btn-danger" target="_blank" onclick="cargar_datos_eliminar_elemento({{$row['id_equipo_asignacion']}})"><i class="icon-remove"></i></a>
                        <!-- <a class="btn btn-danger" target="_blank" onclick="eliminar_elemento({{$row['id_equipo_asignacion']}})"><i class="icon-remove"></i></a> -->
                    </td>
                </tr>
                @endforeach
           </tbody>
        </table>
    </div>
    <div class="col-md-3">
        
        <h2>Usuarios</h2>
        <table class="table top-blue">
           <thead>
           <tr>
                 <th>Imagen</th>
                 <th>Usuario</th>
                 <th>Estado</th>
                 <th><i class="icon-remove"></i></th>

                 
              </tr>
           </thead>
           @if($usuarios[0]->id_asignacion_usuario != "")
           <tbody class="center">                         
          
                @foreach ($usuarios as $usuario)
                <tr id="id_usuario_{{$usuario->id_asignacion_usuario}}" @if($usuario->status == 1) class="green" @else class="red" @endif>
                    <td>                               
                       <!--  <a href="#basic" data-toggle="modal" class="btn btn-default">
                            <img src="../{{$usuario->img_min}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                        </a>   -->
                        <a href="#" class="pull-left nameimg" title="">
                          <img alt="" style="border-radius:50px;" width="50" height="50" src="../{{$usuario->img_min}}">
                        </a> 
                    </td>
                    <td>
                        <span class="label label-sm label-success ">{{$usuario->name}} {{$usuario->last_name}}</span>
                    </td>
                    <td>
                        @if($usuario->status == 1) Activo @else Inactivo @endif
                    </td>
                    <td>
                        <a  href="#basic"  data-toggle="modal" class="btn btn-danger" target="_blank" onclick="cargar_datos_eliminar({{$usuario->id_asignacion_usuario}})"><i class="icon-remove"></i></a>
                        <!-- <a class="btn btn-danger" target="_blank" onclick="eliminar_asignacion({{$usuario->id_asignacion_usuario}})"><i class="icon-remove"></i></a> -->
                    </td>
                </tr>
                @endforeach
           </tbody>
           @endif
        </table>
    </div>
</div>

<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog  " ng-controller="help_deskCtrl" ng-app>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmacion para desvincular usuario</h4>
            </div>
            <div class="modal-body form-body">
                <!-- ¿Desea desvincular el usuario del equipo? -->
                <p class="text-info"><h2 class="text-success">¿Desea desvincular el usuario del equipo?</h2></p>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-danger clase_eliminar_usuario"  id="" onclick="eliminar_asignacion($(this).attr('id'))">Eliminar</button>
                <button type="button" class="btn btn-default refresh clase_eliminar_usuario_des" data-dismiss="modal" id="" onclick="des_cargar_datos_eliminar($(this).attr('id'))">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="basic2" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog  " ng-controller="help_deskCtrl" ng-app>
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Confirmacion para eliminar Elementos</h4>
            </div>
            <div class="modal-body form-body">
                <!-- ¿Desea desvincular el usuario del equipo? -->
                <p class="text-info"><h2 class="text-success">¿Desea desvincular el elemento del equipo?</h2></p>
            </div>
            <div class="modal-footer">
                
                <button type="button" class="btn btn-danger clase_eliminar_elemento"  id="" onclick="eliminar_elemento($(this).attr('id'))">Eliminar</button>
                <button type="button" class="btn btn-default refresh clase_eliminar_elemento_des" data-dismiss="modal" id="" onclick="des_cargar_datos_eliminar_elemento($(this).attr('id'))">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function cargar_datos_eliminar(id){
        $('.clase_eliminar_usuario').attr("id", id);
        $('.clase_eliminar_usuario_des').attr("id", id);
        $("#id_usuario_"+id).addClass("red");
    }
    function des_cargar_datos_eliminar(id){
        
        $("#id_usuario_"+id).removeClass("red");
    }
</script>



<script type="text/javascript">
    function cargar_datos_eliminar_elemento(id){
        
        $("#id_elemento_"+id).addClass("red");
        $('.clase_eliminar_elemento').attr("id", id);
        $('.clase_eliminar_elemento_des').attr("id", id);
    }
    function des_cargar_datos_eliminar_elemento(id){
        
        $("#id_elemento_"+id).removeClass("red");
    }
</script>