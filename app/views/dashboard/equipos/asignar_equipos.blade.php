
<style>
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }
    #wrapper {
        padding: 15px;
    }
    #software_adicionales {
        width: 300px;
    }
</style>



<div class="page-content">
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1_2">
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Asignar Equipos a un usuario
                    </div>
                </div>
                  
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class=""><a href="equipos">Ingresar Equipos</a></li>
                        <li class=""><a href="elementos">Asignar elementos a un Equipo</a></li>
                        <li class="active"><a href="asignar">Asignar Equipos a usuarios</a></li>
                        <li class=""><a href="eliminar">Eliminar Asignación</a></li>
                    </ul>
                    <form>
                        <div class="form-body">
                           <div class="row">
                                <div class="form-group has-success">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <select  class="form-control input-lg  select2me" data-placeholder="Equipo..." id="id_equipo_asignar" onchange="cargar_datos_equipo()">
                                                    <option value=""></option>
                                                    @foreach($equipos as $equipo)
                                                    <option value="{{$equipo['id_equipo']}}">{{$equipo['id_equipo']}} {{$equipo['usuarios']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <select  class="form-control input-lg  select2me" data-placeholder="Usuario..." id="usuario_asignar" onchange="cargar_datos_user()">
                                                    <option value=""></option>
                                                    @foreach($usuarios as $usuario)
                                                    <option value="{{$usuario->id}}">{{$usuario->last_name}} {{$usuario->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="form-actions "> 
                            <div class="col-md-4">
                                <div class="input-group" id="">
                                    <div class="col-md-3">
                                            
                                        <div class="input-group" id="info_equipo">
                                                
                                        </div>
                                    </div> 
                                </div>
                                
                            </div>
                            <div class="col-md-4">
                                <div class="input-group" id="">
                                    <div class="col-md-3">
                                            
                                        <div class="input-group" id="info_usuario">
                                                
                                        </div>
                                    </div> 
                                </div>
                            </div>
                            <div class="pull-right">
                                
                               <a class="btn btn-success" onclick="asignar_equipos()">Asignar</a>                            
                            </div>                          
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    function asignar_equipos(){
        var equipo = $("#id_equipo_asignar").val();
        var usuario = $("#usuario_asignar").val();

        
        $.ajax({
                type: "GET",
                url:  "asignarequipo",
                data: { id_equipo: equipo, id_user: usuario}
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se Asigno correctamente el Equipo', 'Asinación de Equipo');
                
                $("#id_equipo_asignar").val("");
                $("#usuario_asignar").val("");
                
            }else{
                toastr.error('No ha sido posible Signar el Equipo', 'Error');
            }
        });
    }
    function cargar_datos_user(){

        var usuario_id = $("#usuario_asignar").val();
        if (usuario_id == "") {
           $("#info_usuario").html(""); 
           return;
        }

        $.ajax({
                type: "GET",
                url:  "cargarinfouser",
                data: { usuario_id: usuario_id}
        })
        .done(function(data) {
            if (data != 1) {
                
                $("#info_usuario").html(data);
            }else{
                toastr.warning('No ha sido posible mostrar los datos del usuario', 'Info');
            }
        });
    }

    function cargar_datos_equipo(){

        var id_equipo_asignar = $("#id_equipo_asignar").val();
        if (id_equipo_asignar == "") {
           $("#info_equipo").html(""); 
           return;
        }

        $.ajax({
                type: "GET",
                url:  "cargarinfoequipo",
                data: { id_equipo_asignar: id_equipo_asignar}
        })
        .done(function(data) {
            if (data != 1) {
                
                $("#info_equipo").html(data);
            }else{
                toastr.warning('No ha sido posible mostrar los datos del usuario', 'Info');
            }
        });
    }

    
    
</script>
