<link rel="stylesheet" type="text/css" href="{{ URL::to("assets/css/tipped.css")}}" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="{{ URL::to("js/tipped.js")}}"></script>

<script type='text/javascript'>
  $(document).ready(function() {
    Tipped.create('.informacion', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
    Tipped.create('.nameimg', { 
      size: 'medium',
      skin: 'light',
      maxWidth: 300
      
    });
    
  });
</script>
<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle de Mantenimientoss</h4>
      </div>
      <div class="modal-body form-body">
         

                <div class="portlet">
                   <div class="portlet-title">
                     <div class="caption">Información General</div>
                     <div class="tools">
                       <a href="javascript:;" class="expand"></a>
                     </div>
                   </div>
                   <div class="portlet-body display-hide">
                     <div class="row">
                       <div class="col-md-6">
                         <input type="hidden" value="1" name="tipo-equipo">
                         <table class="table middle well">
                           <tbody>
                             <tr>
                               <td>Funcionario</td>
                               <td>
                                 <span class="tag">
                                    <select name="usuario" id="" class="form-control input-sm clear-border" readonly="">
                                        <option>{{$users[0]->name}} {{$users[0]->name2}} {{$users[0]->last_name}} {{$users[0]->last_name2}}</option>
                                    </select>
                                 </span>
                                 <span class="test-img"><img class="inside-avatar" src="../{{$users[0]->img_min}}"></span>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                         @foreach($equipos as $equipo)
                         @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>
                             <tr>
                                <td>Tipo de Equipo</td>
                                <td>
                                 {{$equipo->EquipmentTypes->type}}
                                </td>
                             </tr>
                             <tr>
                               <td>Imagen</td>
                               <td>
                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        @if($equipo->image == "")
                                            <img src="http://localhost/tareas/webroot/img/equipos/-1.png" alt="">
                                        @else
                                            <img src="../{{$equipo->image}}" alt="">
                                        @endif
                                   </div>
                                   <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                   <div>
                                       <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Eliminar</a>
                                   </div>
                                 </div>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                         @endif
                         @endforeach
                       </div>
                       <div class="col-md-6">
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 4)
                         <table class="table middle well">
                           <tbody>  
                             <tr>
                               <td>Marca</td>
                               <td><input name="marca" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->mark}}"></td>
                             </tr>
                             <tr>
                               <td>Modelo</td>
                               <td><input name="modelo" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->model}}"></td>
                             </tr>
                             <tr>
                               <td>Serial</td>
                               <td><input name="serial" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->serial}}"></td>
                             </tr>
                             <tr>
                               <td>Entrega del Equipo</td>
                               <td><input name="entrega" class="tag form-control input-medium clear-border"  readonly="" value="{{$equipo->date_asign}}"></td>
                             </tr>
                             <tr>
                               <td>Estado</td>
                               <td><input name="estado" class="tag form-control clear-border input-medium" type="text"readonly=""  value="{{$equipo->EquipmentStatuses->status}}"></td>
                             </tr>
                           </tbody>
                         </table>
                         @endif
                         @endforeach 
                       </div>
                     </div>
                   </div>
                </div>
                @foreach($mantenimientos as $mantenimiento)
                <div class="portlet">
                    <div class="portlet-title">
                      <div class="caption">{{$mantenimiento->created_at}} {{$mantenimiento->name}} {{$mantenimiento->last_name}} {{$mantenimiento->mark}} </div>
                      <div class="tools">
                        <a href="javascript:;" class="expand"></a>
                      </div>
                    </div>
                    <div class="portlet-body ">
                      <div class="row">
                        <div class="col-md-12">
                            <table class="table top-blue">
                                
                                    <tr>
                                      <th style="text-align:center">Tipo</th>
                                      
                                      <th style="text-align:center">Detalle</th>
                                      
                                      
                                    </tr>
                                
                                <tbody>
                        
                              <?php 

                                $detalles = EquipmentMaintenancesDetails::where('equipment_maintenances_id', $mantenimiento->id_mantenimiento)->get();
                                $variable2 = "";
                                foreach ($detalles as $key) {
                                    $texto = DB::table('equipment_type_maintenances')
                                    ->where('id', $key->type_maintenances_id)
                                    ->get();

                                    $variable2 = $variable2.$texto[0]->type_maintenances."<br>";
                                }
                                $i = 1;
                              ?>
                              @foreach($detalles as $detalle)
                                <?php 
                                  $textos = DB::table('equipment_type_maintenances')
                                  ->where('id', $detalle->type_maintenances_id)
                                  ->get();
                                ?>
                                @foreach($textos as $texto)
                                <tr style="text-align:center" class="green informacion" >
                                  <td>@if($texto->tipo == 1) Software @else Hardware @endif</td>
                                  <td>{{$texto->type_maintenances}}</td>
                                  
                                  
                                </tr>
                                @endforeach
                              @endforeach
                              
                              </tbody>
                            </table>
                          </div>
                          
                        </div>
                    </div>
                </div>
                @endforeach 
         
      </div>
      <div class="modal-footer">
         
         
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>



