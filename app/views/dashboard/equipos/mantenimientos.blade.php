<div class="page-content">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet">
                <div class="portlet-title">
                    <i class="icon-reorder"></i> Ingresar Mantenimientos
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class="active"><a href="mantenimientos" >Ingresar Mantenimientos</a></li>
                        <li class=""><a href="gestion" >Ver Mantenimientos</a></li>
                    </ul>
                    <div class="table-responsive">
                            <div>
                               <form class="form-inline" action="verequipos" method="get">
                                  <div class="search-region">
                                     <div class="form-group">
                                        <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                                     </div>
                                     <div class="form-group">
                                        <select class="form-control input-medium select2me" name="applicant" id="applicant" onchange="tabla_nuevo_mantenimiento()">
                                        <option value="">Usuario</option> 
                                         @foreach($applicants as $applicant)
                                         @if($applicant->id == Input::get('applicant'))
                                            <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                                         @else
                                            <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                                         @endif  
                                         @endforeach
                                        </select>
                                     </div>
                                     <div class="form-group">
                                        <select class="form-control input-medium select2me" name="marca" id="marca" onchange="tabla_nuevo_mantenimiento()">
                                        <option value="">Marca</option> 
                                         @foreach($marcas as $marca)
                                         @if($marca->mark == Input::get('marca'))
                                            <option value="{{$marca->mark}}" selected="">{{$marca->mark}}</option>
                                         @else
                                            <option value="{{$marca->mark}}">{{$marca->mark}}</option>
                                         @endif  
                                         @endforeach
                                        </select>
                                     </div>
                                     <div class="form-group">
                                        <select class="form-control input-medium select2me" name="tipo_equipo" id="tipo_equipo" onchange="tabla_nuevo_mantenimiento()">
                                        <option value="">Tipo</option> 
                                         @foreach($tipo_equipos as $tipo_equipo)
                                         @if($tipo_equipo->id == Input::get('tipo_equipo'))
                                            <option value="{{$tipo_equipo->id}}" selected="">{{$tipo_equipo->type}}</option>
                                         @else
                                            <option value="{{$tipo_equipo->id}}">{{$tipo_equipo->type}}</option>
                                         @endif  
                                         @endforeach
                                        </select>
                                     </div>
                                     <div class="form-group">
                                        <select class="form-control input-medium select2me" name="grupo" id="grupo" onchange="tabla_nuevo_mantenimiento()">
                                            <option value="">Grupo</option>
                                            @foreach($grupos as $grupo)
                                            @if($grupo->id == Input::get('grupo'))
                                               <option value="{{$grupo->id}}" selected="">{{$grupo->name_group}}</option>
                                            @else
                                               <option value="{{$grupo->id}}">{{$grupo->name_group}}</option>
                                            @endif 
                                            @endforeach
                                        </select>
                                     </div> 
                                     <div class="form-group">
                                        <select class="form-control input-medium select2me" name="mantenimiento" id="mantenimiento" onchange="tabla_nuevo_mantenimiento()">
                                            <option value="">Mantenimiento</option>
                                            @foreach($tipos_mant as $tipos_mant)
                                            @if($tipos_mant->id == Input::get('mantenimiento'))
                                               <option value="{{$tipos_mant->id}}" selected="">{{$tipos_mant->type_maintenance}}</option>
                                            @else
                                               <option value="{{$tipos_mant->id}}">{{$tipos_mant->type_maintenance}}</option>
                                            @endif 
                                            @endforeach
                                        </select>
                                     </div>
                                     <!-- <div class="form-group">
                                        <input type="text" class="form-control" id="serial" name="serial" placeholder="Serial" value="{{Input::get('serial')}}">
                                     </div> -->
                                         
                                  </div>
                               </form> 
                            </div>
                        
                            @if(isset($_GET['applicant']))
                                
                                <div id="tabla_nuevo_mantenimiento">   
                                    @include('dashboard.equipos.tabla_mantenimientos')

                                </div>
                            @else
                            
                                <div id="tabla_nuevo_mantenimiento">
                                </div>   
                            @endif

                        
                    </div>
                </div>
            </div>
        </div>
    </div>          
</div>
<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
</div>
<script>
    function detalle(id, id_user, id_mant_pendiente){
            
        $.post('frommantenimientos', {
            id:id, id_user:id_user, id_mant_pendiente:id_mant_pendiente
        },
        function(response){
            $('#basic').html(response);
                //var datos = JSON.parse(response);
        });
    }
    function tabla_nuevo_mantenimiento(){


        var code = $("#code").val();
        var applicant = $("#applicant").val();
        var marca = $("#marca").val();
        var f_compra = $("#f_compra").val();
        var serial = $("#serial").val();
        var grupo = $("#grupo").val();
        var tipo_equipo = $("#tipo_equipo").val();
        var mantenimiento = $("#mantenimiento").val();


          $.get('mantenimientos', {
              code: code,
              applicant: applicant,
              marca: marca,
              f_compra: f_compra,
              serial: serial,
              grupo: grupo,
              tipo_equipo:tipo_equipo,
              mantenimiento:mantenimiento,
              nombre: 1
          } ,

          function(response){
              $('#tabla_nuevo_mantenimiento').html(response);
              //var datos = JSON.parse(response);
          });
    }
</script>