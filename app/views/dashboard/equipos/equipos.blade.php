
<style>
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }
    #wrapper {
        padding: 15px;
    }
    #software_adicionales {
        width: 300px;
    }
</style>



<div class="page-content">
         <div class="tab-content">
            <div class="tab-pane active" id="tab_1_2">
               <div class="portlet ">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="icon-reorder"></i> Ingresar Equipos
                     </div>
                  </div>
                  
                  <div class="portlet-body">
                     <ul  class="nav nav-tabs">
                        <li class="active"><a href="equipos">Ingresar Equipos</a></li>
                        <li class=""><a href="elementos">Asignar elementos a un Equipo</a></li>
                        <li class=""><a href="asignar">Asignar Equipos a usuarios</a></li>
                        <li class=""><a href="eliminar">Eliminar Asignación</a></li>
                        
                        <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                        
                        
                        <a href="#basic" data-toggle="modal" class="btn btn-default  pull-right" onclick="detalle()">Gestión Tipo de Equipos</a>                          
                     </ul>
                     <form class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="form-body">
                           
                            <div class="row">
                                
                                <div class="form-group has-success">
                                    <div class="col-md-3">
                                        <label class="control-label">Tipo de Equipo</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                            <select id="tipo_equipo" name="tipo_equipo" class="form-control" onchange="activar_formularios()">
                                            <option value=""></option>
                                            @foreach($tipos as $tipo)
                                            <option value="{{$tipo->id}}">{{$tipo->type}}</option>
                                            @endforeach
                                            </select>
                                        </div> 
                                    </div>
                                    <div class="col-md-3">
                                        <label class="control-label">Usuario</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                            <select id="usuario_id" name="concept" class="form-control" onchange="cargar_form()">
                                            <option value=""></option>
                                            @foreach($usuarios as $usuario)
                                            <option value="{{$usuario->id}}">{{$usuario->name}} {{$usuario->last_name}}</option>
                                            @endforeach
                                            </select>
                                        </div> 
                                    </div>
                                </div>
                                    
                                
                                <div class="col-md-6">
                                    <div class="note note-success" id="formulario_1" style="display:none">
                                        <h4 class="block">Monitor</h4>
                                        <p>
                                            <div class="form-group" id="form_nombre">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_marca" name="clave" type="text" class="form-control" placeholder="Marca">
                                                        <div id="display"></div>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_cedula">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_modelo" name="nit" type="text" class="form-control" placeholder="Modelo">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_serial" name="factura" type="text" class="form-control" placeholder="Serial">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select id="monitor_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                        <option value="">Ubicación</option>
                                                        @foreach($ubicaciones as $ubicacion)
                                                        <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                            </div>

                                        </p>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                <label class="control-label">Imagen Monitor</label>
                                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                                    <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                    </div>
                                                    <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                    <div>
                                                        <span class="btn btn-default btn-file">
                                                        <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                        <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                        <input type="file" class="default form-control" name="imagen_monitor" id="imagen_monitor" size="20">
                                                        </span>
                                                        <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_monitor"><i class="icon-trash"></i> Eliminar</a>
                                                        <div class="help-block">
                                                            Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>                                            
                                        </div>
                                    </div>

                                    <div class="note note-success" id="formulario_2" style="display:none" >
                                        <h4 class="block">Teclado</h4>
                                        <p>
                                            <div>
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="teclado_marca" name="clave" type="text" class="form-control" placeholder="Marca Teclado">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="teclado_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="teclado_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="teclado_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        
                                                        <select id="teclado_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                        <option value="">Ubicación</option>
                                                        @foreach($ubicaciones as $ubicacion)
                                                        <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                            </div>
                                        </p>
                                    </div>
                                    <div class="note note-success" id="formulario_3" style="display:none">
                                        <h4 class="block">Mouse</h4>
                                        <p>
                                            <div> 
                                                <div class="form-group" id="form_nombre" >
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="mouse_marca" name="clave" type="text" class="form-control" placeholder="Marca Mouse">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="mouse_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="mouse_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="mouse_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="mouse_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                            <option value="">Ubicación</option>
                                                            @foreach($ubicaciones as $ubicacion)
                                                            <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                            @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </p>
                                    </div>
                                    <div class="note note-success" id="formulario_5" style="display:none">
                                        <h4 class="block">Telefono</h4>
                                        <p>
                                            <div>
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="telefono_marca" name="clave" type="text" class="form-control" placeholder="Marca Telefono">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="telefono_modelo" name="clave" type="text" class="form-control" placeholder="Modelo Telefono">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="telefono_serial" name="clave" type="text" class="form-control" placeholder="Serial Telefono">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="telefono_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="telefono_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="telefono_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="telefono_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                            <option value="">Ubicación</option>
                                                            @foreach($ubicaciones as $ubicacion)
                                                            <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                            @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                    <label class="control-label">Imagen Telefono</label>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file">
                                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                            <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                            <input type="file" class="default form-control" name="imagen_telefono" id="imagen_telefono" size="20">
                                                            </span>
                                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_telefono"><i class="icon-trash"></i> Eliminar</a>
                                                            <div class="help-block">
                                                                Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </p>
                                    </div>

                                    <div class="note note-success" id="formulario_7" style="display:none" >
                                        <h4 class="block">Impresora</h4>
                                        <p>
                                            <div class="form-group" id="form_nombre">
                                               <div class="col-md-12">
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="impresora_marca" name="clave" type="text" class="form-control" placeholder="Marca">
                                                     <div id="display"></div>
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_cedula">
                                               <div class="col-md-12">
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="impresora_modelo" name="nit" type="text" class="form-control" placeholder="Modelo">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                               <div class="col-md-12">
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="impresora_serial" name="factura" type="text" class="form-control" placeholder="Serial">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="impresora_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="impresora_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="impresora_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_factura">
                                                <div class="col-md-12">
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        
                                                        <select id="impresora_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                        <option value="">Ubicación</option>
                                                        @foreach($ubicaciones as $ubicacion)
                                                        <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                    <label class="control-label">Imagen Impresora</label>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file">
                                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                            <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                            <input type="file" class="default form-control" name="imagen_impresora" id="imagen_impresora" size="20">
                                                            </span>
                                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_impresora"><i class="icon-trash"></i> Eliminar</a>
                                                            <div class="help-block">
                                                                Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>                                            
                                            </div>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-6" id="formulario_4" style="display:none" >
                                    <form id="formulario_cpu"> 
                                        <div class="row note note-warning">
                                            <h4 class="block">CPU</h4>
                                            <p>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Marca</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_marca" name="cpu_marca" type="text" class="form-control campos_cpu">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_cedula">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Modelo</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_modelo" name="cpu_modelo" type="text" class="form-control campos_cpu" placeholder="">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Serial</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_serial" name="cpu_serial" type="text" class="form-control campos_cpu" placeholder="">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Ip</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_ip" name="cpu_ip" type="text" class="form-control campos_cpu" placeholder="ej. 192.168.120.115">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Mac</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_mac" name="cpu_mac" type="text" class="form-control campos_cpu" placeholder="">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Memoria Ram</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_memoria_ram" name="cpu_memoria_ram" type="text" class="form-control campos_cpu" placeholder="">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <div class="col-md-12">
                                                        <label class="control-label">Imagen CPU</label>
                                                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                                            <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                            </div>
                                                            <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                            <div>
                                                                <span class="btn btn-default btn-file">
                                                                <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                                <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                                <input type="file" class="default form-control" name="imagen_cpu" id="imagen_cpu" size="20">
                                                                </span>
                                                                <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_cpu"><i class="icon-trash"></i> Eliminar</a>
                                                                <div class="help-block">
                                                                    Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                                </div>
                                                            </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Disco Duro</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_disco_duro" name="cpu_disco_duro" type="text" class="form-control campos_cpu" placeholder="GB (Gigabyte)">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Procesador</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_procesador" name="cpu_procesador" type="text" class="form-control campos_cpu" placeholder="">
                                                      </div> 
                                                   </div>
                                                </div>

                                                <div class="form-group" id="form_factura">
                                                   <div class="col-md-12">
                                                        <label class="control-label">Sistema Operativo</label>
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="cpu_so" name="cpu_so" type="text" class="form-control campos_cpu" placeholder="">
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Factura</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="cpu_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Fecha Asignacion</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="cpu_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Fecha de Compra</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="cpu_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <label class="control-label">Ubicacion</label>
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="cpu_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                            <option value="">Ubicación</option>
                                                            @foreach($ubicaciones as $ubicacion)
                                                            <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                            @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            </p>

                                        </div>
                                    </form>   
                                    
                                </div>
                                
                            </div>
                            
                        </div>
                            

                        <div class="form-actions right">                           
                           <a class="btn btn-success" onclick="validar_campos()">Registrarrr</a>                            
                        </div>
                     
                  </div>
                  <div id="wrapper">
                    
                    <input value="activate tagator" id="activate_tagator" type="button" style="display:none">
                  </div>

               </div>
            </div>
        </div> 
        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog  modal-wide">
                <div class="modal-content">
               
               
                    <input type="hidden" ng-model="support_id" value="" id="support_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Gestión Tipo de Equipos</h4>
                    </div>
                    <div class="modal-body form-body" id="">
                        <table class="table top-blue">
                            <thead>
                                <tr class="center">
                                    <th>Id</th>
                                    <th>Tipo Equipo</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody class="center" id="tabla_tipo_equipos">
                                @foreach($tipos as $tipo)
                                    @if($tipo->status == 0)  
                                    <tr class="red">
                                    @else
                                    <tr>
                                    @endif
                                        <td>{{$tipo->id}}</td>
                                        <td>{{$tipo->type}}</td>
                                        @if($tipo->status == 0)  
                                            <td><button type="submit" class="btn btn-success" id="{{$tipo->id}}" onclick="desactivar_tipo({{$tipo->id}}, 1)"><i class="icon-ok"></i> Activar</button></td>
                                        @else
                                            <td><button type="submit" class="btn btn-danger" id="{{$tipo->id}}" onclick="desactivar_tipo({{$tipo->id}}, 0)"><i class="icon-remove"></i> Desactivar</button></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                        <div class="row">
                        <div class="form-group has-success" id="form_nombre">
                            <div class="col-md-3">
                            <label class="control-label">Tipo de Equipo</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                    <input id="nuevo_tipo_equipo" name="nuevo_tipo_equipo" class="form-control">
                                </div> 
                            </div>
                        </div>
                        </div>
                              
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success refresh" onclick="Ingresar_tipo_equipo()">Ingresar</button>

                       
                        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                    </div>
               
               
                </div>
            </div>

            <script src="/indo/public/js/upload.js"></script>                
        
        </div>        
      
   
</div>

<script>
    function activar_formularios(){
        var tipo_equipo = $('#tipo_equipo').val();

        @foreach($tipos as $tipo)

            if (tipo_equipo == {{$tipo->id}}) {
                if (tipo_equipo == 6) {
                    $('#formulario_2').show();
                    $('#formulario_3').show();
                    $('#formulario_4').show();
                }else{
                    $('#formulario_{{$tipo->id}}').show();
                }
            }else{
                $('#formulario_{{$tipo->id}}').hide();
            }
        @endforeach

        
        
    }
    function Ingresar_tipo_equipo(){
        var tipo_equipo = $('#nuevo_tipo_equipo').val();
        if (tipo_equipo == "") {
            toastr.error('El campo Tipo de Equipo está vacio.', 'Error');
            return;
        }
        $.ajax({
                type: "GET",
                url:  "creartipoequipo",
                data: { tipo_equipo: tipo_equipo}
        })
        .done(function(data) {
            if (data != 1) {
                toastr.success('Se ingreso correctamente el Tipo de Equipo', 'Nuevo Tipo de Equipo');
                $("#nuevo_tipo_equipo").val("");
                $("#tabla_tipo_equipos").html(data);
            }else{
                toastr.error('No ha sido posible ingresar el  Tipo de Equipo', 'Error');
            }
        });
    }
    function desactivar_tipo(id, status){
        
        $.ajax({
                type: "GET",
                url:  "creartipoequipo",
                data: { desactivar_id: id, status: status}
        })
        .done(function(data) {
            if (data != 2) {
                toastr.success('Se Gestiono correctamente el Tipo de Equipo', 'Desactivo Tipo de Equipo');
                
                $("#tabla_tipo_equipos").html(data);
                
            }else{
                toastr.error('No ha sido posible Gestionar el Tipo de Equipo', 'Error');
            }
        });
    }
    function validar_campos(){

        alert('hola');
        return;

        var user_asign = $('#usuario_id').val();

        var monitor = "";
        var teclado = "";
        var mouse = "";
        var cpu = "";
        var telefono = "";
        var all_in_one = "";
        var impresora = "";


        var monitor_marca = $("#monitor_marca").val();
        var monitor_modelo = $("#monitor_modelo").val();
        var monitor_serial = $("#monitor_serial").val();
        var monitor_factura = $("#monitor_factura").val();
        var monitor_asignacion = $("#monitor_asignacion").val();
        var monitor_compra = $("#monitor_compra").val();
        var monitor_ubicacion = $("#monitor_ubicacion").val();
        var imagen_monitor = $("#imagen_monitor").val();

        if (monitor_marca != "" || monitor_modelo != "" || monitor_serial != "" || imagen_monitor != "" || monitor_compra != "") {
            monitor = 1;
        }
        var teclado_marca = $("#teclado_marca").val();
        if (teclado_marca != "") {
            teclado = 1;
        }
        var mouse_marca = $("#mouse_marca").val();
        if (mouse_marca != "") {
            mouse = 1;
        }
        var cpu_marca           = $("#cpu_marca").val();
        var cpu_modelo          = $("#cpu_modelo").val();
        var cpu_serial          = $("#cpu_serial").val();
        var cpu_ip              = $("#cpu_ip").val();
        var cpu_mac             = $("#cpu_mac").val();
        var cpu_memoria_ram     = $("#cpu_memoria_ram").val();
        var cpu_disco_duro      = $("#cpu_disco_duro").val();
        var cpu_procesador      = $("#cpu_procesador").val();
        var cpu_so              = $("#cpu_so").val();
        var cpu_factura         = $("#cpu_factura").val();
        var cpu_asignacion      = $("#cpu_asignacion").val();
        var cpu_ubicacion       = $("#cpu_ubicacion").val();
        var imagen_cpu          = $("#imagen_cpu").val();
        var cpu_compra          = $("#cpu_compra").val();


        if (cpu_marca != "" || cpu_modelo != "" || cpu_serial != "" || cpu_ip != "" || cpu_mac != "" || cpu_memoria_ram != "" || cpu_disco_duro != "" || cpu_procesador != "" || cpu_so != "" || cpu_compra != "" || imagen_cpu != "") {
            cpu = 1;
        }

        var telefono_marca = $("#imagen_telefono").val();
        var telefono_modelo = $("#imagen_telefono").val();
        var telefono_serial = $("#imagen_telefono").val();
        var telefono_factura = $("#telefono_factura").val();
        var telefono_asignacion = $("#telefono_asignacion").val();
        var telefono_ubicacion = $("#telefono_ubicacion").val();
        var imagen_telefono = $("#imagen_telefono").val();
        var telefono_compra = $("#telefono_compra").val();

        if (telefono_marca != "" || telefono_modelo != "" || telefono_serial != "" || imagen_telefono != "" || telefono_compra != "") {
            telefono = 1;
        }

        var impresora_marca = $("#impresora_marca").val();
        var impresora_modelo = $("#impresora_modelo").val();
        var impresora_serial = $("#impresora_serial").val();
        var impresora_factura = $("#impresora_factura").val();
        var impresora_asignacion = $("#impresora_asignacion").val();
        var impresora_ubicacion = $("#impresora_ubicacion").val();
        var imagen_impresora = $("#imagen_impresora").val();
        var impresora_compra = $("#impresora_compra").val();

        if (impresora_marca != "" || impresora_modelo != "" || impresora_serial != "" || imagen_impresora != "" || impresora_compra != "" ) {
            impresora = 1;
        }

        if(monitor == 1){
            ingresar_monitor(user_asign);
        }
        if(teclado == 1){
            ingresar_teclado(user_asign);
        }
        if(mouse == 1){
            ingresar_mouse(user_asign);
        }
        if(cpu == 1){
            ingresar_cpu(user_asign);
        }
        if(telefono == 1){
            ingresar_telefono(user_asign);
        }
        if(impresora == 1){
            ingresar_impresora(user_asign);
        }



    }
    function ingresar_monitor(user_asign){

        var monitor_marca = $("#monitor_marca").val();
        var monitor_modelo = $("#monitor_modelo").val();
        var monitor_serial = $("#monitor_serial").val();
        var monitor_factura = $("#monitor_factura").val();
        var monitor_asignacion = $("#monitor_asignacion").val();
        var monitor_ubicacion = $("#monitor_ubicacion").val();
        var monitor_imagen = $("#imagen_monitor").val();
        var monitor_compra = $("#monitor_compra").val();

        if (monitor_marca == "" || monitor_modelo == "" || monitor_serial == "" || monitor_imagen == ""|| monitor_factura == "" || monitor_asignacion == "" || monitor_ubicacion == "" || monitor_compra == "") {
           toastr.error('Por favor verifique los campos del Formulario Monitor.', 'Error');                               
           return;
        }



        $("#imagen_monitor").upload('ingresarmonitor', 
        {
            user_asign: user_asign,
            monitor_marca: monitor_marca,
            monitor_modelo: monitor_modelo,
            monitor_serial: monitor_serial,
            monitor_factura: monitor_factura,
            monitor_asignacion: monitor_asignacion,
            monitor_compra: monitor_compra,
            monitor_ubicacion: monitor_ubicacion
        },
        function(respuesta){
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente el Monitor', 'Nuevo Monitor');
                $("#monitor_marca").val("");
                $("#monitor_modelo").val("");
                $("#monitor_serial").val("");
                $("#monitor_factura").val();
                $("#monitor_asignacion").val();
                $("#monitor_ubicacion").val();
                $("#imagen_monitor").val("");
                $("#monitor_compra").val("");
                $("#btn_monitor").click();
                
            } else {
                toastr.error('No ha sido posible ingresar el  Monitor.', 'Error');                              
                
            }
        });
    }
    function ingresar_teclado(user_asign){
        
        var teclado_marca = $("#teclado_marca").val();
        var teclado_factura = $("#teclado_factura").val();
        var teclado_asignacion = $("#teclado_asignacion").val();
        var teclado_ubicacion = $("#teclado_ubicacion").val();
        var teclado_compra = $("#teclado_compra").val();

        if (teclado_marca == "" || teclado_factura == "" || teclado_asignacion == "" || teclado_ubicacion == "" || teclado_compra == "") {
            toastr.error('Por favor verifique los campos del Formulario Teclado.', 'Error');
            return;
        }

        $.ajax({
            type: "GET",
            url: "ingresarteclado",
            data: { 
                    user_asign: user_asign,
                    teclado_marca: teclado_marca,
                    teclado_factura: teclado_factura,
                    teclado_asignacion: teclado_asignacion,
                    teclado_compra: teclado_compra,
                    teclado_ubicacion: teclado_ubicacion
            }
        })
        .done(function(data) {
            toastr.success('Se ingreso correctamente el Teclado', 'Nuevo Teclado');
            $("#teclado_marca").val("");
            $("#teclado_factura").val("");
            $("#teclado_asignacion").val("");
            $("#teclado_ubicacion").val("");
            $("#teclado_compra").val("");
            
        });
    }
    function ingresar_mouse(user_asign){
        var mouse_marca = $("#mouse_marca").val();
        var mouse_factura = $("#mouse_factura").val();
        var mouse_asignacion = $("#mouse_asignacion").val();
        var mouse_ubicacion = $("#mouse_ubicacion").val();
        var mouse_compra = $("#mouse_compra").val();

        if (mouse_marca == "" || mouse_factura == "" || mouse_asignacion == "" || mouse_ubicacion == "" || mouse_compra == "") {
            toastr.error('Por favor verifique los campos del Formulario Mouse.', 'Error');
            return;
        }
        $.ajax({
            type: "GET",
            url: "ingresarmouse",
            data: { 
                    user_asign: user_asign,
                    mouse_marca: mouse_marca,
                    mouse_factura: mouse_factura,
                    mouse_asignacion: mouse_asignacion,
                    mouse_compra:mouse_compra,
                    mouse_ubicacion: mouse_ubicacion
            }
        })
        .done(function(data) {
            toastr.success('Se ingreso correctamente el Mouse', 'Nuevo Mouse');
            $("#mouse_marca").val("");
            $("#mouse_factura").val("");
            $("#mouse_asignacion").val("");
            $("#mouse_ubicacion").val("");
            $("#mouse_compra").val("");
            
        });
    }
    function ingresar_cpu(user_asign){
        var cpu_marca = $("#cpu_marca").val();
        var cpu_modelo = $("#cpu_modelo").val();
        var cpu_serial = $("#cpu_serial").val();
        
        var cpu_ip = $("#cpu_ip").val();
        var cpu_mac = $("#cpu_mac").val();
        var cpu_memoria_ram = $("#cpu_memoria_ram").val();
        var cpu_disco_duro = $("#cpu_disco_duro").val();
        var cpu_procesador = $("#cpu_procesador").val();
        var cpu_so = $("#cpu_so").val();
        var cpu_factura = $("#cpu_factura").val();
        var cpu_asignacion = $("#cpu_asignacion").val();
        var cpu_ubicacion = $("#cpu_ubicacion").val();
        var cpu_compra = $("#cpu_compra").val();
        
        
        var imagen_cpu = $("#imagen_cpu").val();

        if (cpu_marca == "" || cpu_modelo == "" || cpu_serial == "" || cpu_ip == "" || cpu_mac == "" || cpu_memoria_ram == "" || cpu_disco_duro == "" || cpu_procesador == "" || cpu_so == "" || imagen_cpu == "" || cpu_factura == "" || cpu_asignacion == "" || cpu_ubicacion == "" || cpu_compra == "") {
            toastr.error('Por favor verifique los campos del Formulario CPU.', 'Error');
            return;   
        }

        $("#imagen_cpu").upload('ingresarcpu', 
        {
            user_asign: user_asign,
            cpu_marca : cpu_marca,
            cpu_modelo : cpu_modelo,
            cpu_serial : cpu_serial,
            
            cpu_ip : cpu_ip,
            
            cpu_mac : cpu_mac,
            cpu_memoria_ram : cpu_memoria_ram,
            cpu_disco_duro : cpu_disco_duro,
            cpu_procesador : cpu_procesador,
            cpu_so : cpu_so,
            cpu_factura: cpu_factura,
            cpu_asignacion: cpu_asignacion,
            cpu_compra: cpu_compra,
            cpu_ubicacion: cpu_ubicacion

        },
        function(respuesta){
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente la CPU', 'Nueva CPU');
                $("#cpu_marca").val("");
                $("#cpu_modelo").val("");
                $("#cpu_serial").val("");
                $("#cpu_fecha_compra").val("");
                $("#cpu_ip").val("");
                $("#cpu_mac").val("");
                $("#cpu_memoria_ram").val("");
                $("#cpu_disco_duro").val("");
                $("#cpu_procesador").val("");
                $("#cpu_so").val("");
                $("#cpu_factura").val("");
                $("#cpu_asignacion").val("");
                $("#cpu_ubicacion").val("");
                $("#cpu_compra").val("");
                $("#btn_cpu").click();
                
            } else {
                toastr.error('No ha sido posible ingresar la CPU.', 'Error');                              
                
            }
        });
    }
    function ingresar_telefono(user_asign){ 
        var telefono_marca = $("#telefono_marca").val();
        var telefono_modelo = $("#telefono_modelo").val();
        var telefono_serial = $("#telefono_serial").val();
        var telefono_imagen = $("#imagen_telefono").val();
        var telefono_factura = $("#telefono_factura").val();
        var telefono_asignacion = $("#telefono_asignacion").val();
        var telefono_ubicacion = $("#telefono_ubicacion").val();
        var telefono_compra = $("#telefono_compra").val();

        if (telefono_marca == "" || telefono_modelo == "" || telefono_serial == "" || telefono_imagen == "" || telefono_factura == "" || telefono_asignacion == "" || telefono_ubicacion == "" || telefono_compra == "") {
            toastr.error('Por favor verifique los campos del Formulario Telefono.', 'Error');
            return;
        }

        $("#imagen_telefono").upload('ingresartelefono', 
        {   
            user_asign: user_asign,
            telefono_marca: telefono_marca,
            telefono_modelo: telefono_modelo,
            telefono_serial: telefono_serial,
            telefono_factura: telefono_factura,
            telefono_asignacion: telefono_asignacion,
            telefono_compra: telefono_compra,
            telefono_ubicacion: telefono_ubicacion
        },
        function(respuesta){
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente el Telefono', 'Nuevo Telefono');
                $("#telefono_marca").val("");
                $("#telefono_modelo").val("");
                $("#telefono_serial").val("");
                $("#imagen_telefono").val("");
                $("#telefono_factura").val("");
                $("#telefono_asignacion").val("");
                $("#telefono_ubicacion").val("");
                $("#telefono_compra").val("");
                $("#btn_telefono").click();
            } else {
                toastr.error('No ha sido posible ingresar el  Telefono.', 'Error');                              
                
            }
        });
    }
    function ingresar_impresora(user_asign){
        var impresora_marca = $("#impresora_marca").val();
        var impresora_modelo = $("#impresora_modelo").val();
        var impresora_serial = $("#impresora_serial").val();
        var imagen_impresora = $("#imagen_impresora").val();
        var impresora_factura = $("#impresora_factura").val();
        var impresora_asignacion = $("#impresora_asignacion").val();
        var impresora_ubicacion = $("#impresora_ubicacion").val();
        var impresora_compra = $("#impresora_compra").val();

        if (impresora_marca == "" || impresora_modelo == "" || impresora_serial == "" || imagen_impresora == "" || impresora_factura == "" || impresora_asignacion == "" || impresora_ubicacion == "" || impresora_compra == "") {
            toastr.error('Por favor verifique los campos del Formulario Impresora.', 'Error');
            return;  
        }

        $("#imagen_impresora").upload('ingresarimpresora', 
        {
            user_asign: user_asign,
            impresora_marca: impresora_marca,
            impresora_modelo: impresora_modelo,
            impresora_serial: impresora_serial,
            impresora_factura: impresora_factura,
            impresora_asignacion: impresora_asignacion,
            impresora_compra: impresora_compra,
            impresora_ubicacion: impresora_ubicacion
        },
        function(respuesta){
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente la Impresora', 'Nueva Impresora');
                $("#impresora_marca").val("");
                $("#impresora_modelo").val("");
                $("#impresora_serial").val("");
                $("#imagen_impresora").val("");
                $("#impresora_factura").val("");
                $("#impresora_asignacion").val("");
                $("#impresora_ubicacion").val("");
                $("#impresora_compra").val("");
                $("#btn_impresora").click();
            } else {
                toastr.error('No ha sido posible ingresar la Impresora.', 'Error');                              
                
            }
        });
    }

    
    
    

    function cargar_form() {
       var id = $("#usuario_id").val();
       $.ajax({
               type: "GET",
               url:  "ingresarteclado",
               data: { id: id}
       })
       .done(function(data) {
          $("#factura").val(data); 
           
       });


    }
    




</script>
<script src="../js/tragator/jquery-1.11.0.min.js"></script>

<style type="text/css">
#displayprov /*estilos para la caja principal en donde se puestran los resultados de la busqueda en forma de lista*/
{
width:400px;
display:none;
overflow:hidden;
z-index:10;
border: solid 1px #666;
}
.display_box /*estilos para cada caja unitaria de cada usuario que se muestra*/
{
padding:2px;
padding-left:6px; 
font-size:15px;
height:30px;
text-decoration:none;
color:#3b5999; 
}

.display_box:hover /*estilos para cada caja unitaria de cada usuario que se muestra. cuando el mause se pocisiona sobre el area*/
{
background: #7f93bc;
color: #FFF;
}
/* Easy Tooltip */
</style>
<script>
 $(document).ready (function(){  
   
       $('#activate_tagator').click(function () {
         if ($('#software_adicionales').data('tagator') === undefined) {
           $('#software_adicionales').tagator({
             autocomplete: [
             @foreach($programas as $programa)
              "{{$programa->software_additional;}}",
             @endforeach
             ]
           });
           $('#activate_tagator').val('destroy tagator');
         } else {
           $('#software_adicionales').tagator('destroy');
           $('#activate_tagator').val('activate tagator');
         }
       });
       $('#activate_tagator').trigger('click');
    


 });
</script>