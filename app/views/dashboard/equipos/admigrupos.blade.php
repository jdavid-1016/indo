
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Administrador</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class=""><a href="administrador">Tipos de Mantenimientos</a></li>
                        <li class=""><a href="adminitems">Items de Mantenimientos</a></li>
                        <li class="active"><a href="admigrupos">Grupos de Equipos</a></li>
                        <li class=""><a href="adminaddequipos">Agregar Equipos A Un Grupo</a></li>
                        <li class=""><a href="adminelimequipos">Eliminar Equipos De Un Grupo</a></li>

                        
                    </ul>
                    <div  class="tab-content">
                        <div class="tab-pane fade active in" id="tab_1_4">
                            <div class="table-responsive" id="table_admin_">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="portlet">
                                          <div class="portlet-title">
                                             <div class="caption"><i class="icon-cogs"></i>Grupos de Equipos</div>
                                             <div class="tools">
                                                <a href="javascript:;" class="collapse"></a>
                                                <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                <a href="javascript:;" class="reload"></a>
                                                <a href="javascript:;" class="remove"></a>
                                             </div>
                                          </div>
                                          <div class="portlet-body">
                                            <button class="btn btn-defaul" type="button" >
                                                <input type="checkbox" id="check_grupos" onchange="mostrar_inactivos_grupos()"> Mostrar Inactivos
                                            </button>
                                             <div class="table-responsive">
                                                <table class="table ">
                                                   <thead>
                                                       <tr>
                                                           <th>Id</th>
                                                           <th>Nombre del Grupo</th>
                                                           
                                                       </tr>
                                                   </thead>
                                                   <tbody id="tabla_grupo_mantenimiento">
                                                   @foreach($grupos as $grupo)
                                                       <tr @if($grupo->status == "0") class="red" @endif >
                                                           <td>{{$grupo->id}}</td>
                                                           <td>

                                                                
                                                                <div class="input-group" id="id_grupo_{{$grupo->id}}">
                                                                    <input type="text" class="form-control" value="{{$grupo->name_group}}" id="texto_{{$grupo->id}}" onblur="editar_grupo({{$grupo->id}})" onfocus="capturar_valor({{$grupo->id}})">
                                                                    <span class="input-group-btn">
                                                                    
                                                                    <button class="btn btn-danger" type="button" onclick="eliminar_grupo({{$grupo->id}}, @if($grupo->status == "0") 1 @else 0 @endif)">
                                                                        <i class="icon-trash"></i>
                                                                    </button>
                                                                    <!-- <button class="btn btn-success" type="button" onclick="editar_grupo({{$grupo->id}})">
                                                                        <i class="icon-edit"></i>
                                                                    </button> -->
                                                                    </span>
                                                                </div>
                                                                

                                                           </td>
                                                           
                                                       </tr>
                                                   @endforeach
                                                   </tbody>
                                               </table>
                                             </div>
                                          </div>
                                        </div>                  
                                        
                                    </div>
                                    <div class="col-md-3">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-edit"></i>Nuevo Grupo de Equipos</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                    <a href="javascript:;" class="reload"></a>
                                                    <a href="javascript:;" class="remove"></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="form-group"> 
                                                   <label class="control-label">Nombre del Grupo</label>
                                                   <div class="input-group">
                                                      <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                      <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="nombre_grupo" value="">
                                                   </div> 
                                                </div>
                                                <a class="btn btn-success" onclick="crear_grupo_equipos()"><i class="icon-ok"></i> Crear</a>
                                            </div>
                                        </div>
                                    </div>  
                                </div>
                                
                            </div>
                        </div>
                        
                        
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>



