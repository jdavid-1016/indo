<div class="modal-dialog modal-wide">
   <div class="modal-content ">
   <form class="form" action="#" >   
      <input type="hidden" value="" id="prospect_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle Licencia</h4>
      </div>      
      <div class="modal-body form-body">
      @if(count($seriales) != 0 )
          
         @foreach($seriales as $ser)
          <div class="row">
            <div class="col-md-6">
               <div class="form-group"> 
                  <label class="control-label">Producto</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " disabled placeholder="" data-required="true" name="name" id="name" value="{{$ser->product}}">
                  </div> 
               </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <label class="control-label">Serial</label>
                  <div class="input-group">
                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                     <input type="text" class="form-control " disabled placeholder="" data-required="true" name="email" id="email" value="{{$ser->serial}}">
                  </div> 
               </div>
            </div>
         </div>
         @endforeach
      @else
        <div class="alert alert-info">
            <strong>Error!</strong> No hay Seriales en estas licencias para asignar.
        </div>
      @endif
            <a href="#" class="btn btn-info">Disponibles: 
                <span class="badge badge-danger">{{$disp[0]->available}}</span>
            </a>
         <hr>
         
        @if($disp[0]->available == 0)
            <div class="alert alert-danger">
                <strong>Error!</strong> No hay disponibilidad en estas licencias para asignar.
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                   <div class="form-group"> 
                      <label class="control-label">Equipo</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         
                         <select class="form-control" placeholder="" data-required="true" name="email" id="equipo_asignar_id" value=""> 
                            <option value=""></option>
                            @foreach($equipos as $equipo)
                            <option value="{{$equipo->id_elemento}}">{{$equipo->name}} {{$equipo->last_name}} {{$equipo->id_elemento}} {{$equipo->type}} {{$equipo->observation}} {{$equipo->serial}} </option>
                            @endforeach
                         </select>
                      </div> 
                   </div>
                </div>
                <div class="col-md-6">
                   <div class="form-group">
                      <label class="control-label">Serial</label>
                      <div class="input-group">
                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                         
                         <select class="form-control" placeholder="" data-required="true" name="email" id="serial_asignar_id" value=""> 
                            <option value=""></option>
                        @if(count($seriales) != 0 ) 
                            @foreach($seriales as $ser)
                            <option value="{{$ser->id}}">{{$ser->serial}}</option>
                            @endforeach
                        @else
                          <option value="">Sin Seriales</option>
                        @endif
                         </select>
                      </div> 
                   </div>
                </div>
            </div>
            <div class="modal-footer">
      
                 
         <button type="button" class="btn btn-success" onclick="asignar_seriales()">Asignar Licencia</button>
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
      </div>
        @endif
            <table class="table top-blue" data-target="soporte/callSupport/">
                <thead>
                    <tr>
                        <th>Id Equipo</th>
                        <th>Equipo</th>
                        <th>Usuario</th>
                        <th>Marca</th>
                        <th>Serial de Licencia</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody id="tabla_licencias_equipos">
                    
                    @foreach($asignados as $asignado)
                    <tr style="text-align:center;">
                        <td>{{$asignado->id_equipo}}</td>
                        <td>
                            <a href="#" class="btn btn-default">
                                @if($asignado->image == "")
                                <img src="../assets/img/equipos/-1.png" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                                @else
                                <img src="../{{$asignado->image}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                                @endif
                            </a>  
                        </td>
                        <td>{{$asignado->name}} {{$asignado->last_name}} </td>
                        <td>{{$asignado->mark}}</td>
                        <td>{{$asignado->serial}}</td>
                        <td><a href="#" class="btn btn-danger" onclick="eliminar_seriales({{$asignado->id_asignacion}}, {{$asignado->id_licensing}})"><i class="icon-remove"></i></a></td>

                    </tr>
                    @endforeach
                    
                </tbody>
            </table>

      </div>
      
   
   </form>
   </div>
</div>

<script type="text/javascript">
    function asignar_seriales(){
        var equipo_asignar_id = $("#equipo_asignar_id").val();
        var serial_asignar_id = $("#serial_asignar_id").val();

        var disp = {{$disp[0]->available}};
        @if(count($seriales) != 0 ) 
        $.get('asignarseriales', {
            equipo_asignar_id: equipo_asignar_id,
            serial_asignar_id: serial_asignar_id,
            id_licencia: {{$seriales[0]->equipment_licensings_id}},
            disp: disp
            
        } ,

        function(response){
            if (response == 1) {
                var parametros = {
                   "id": {{$seriales[0]->equipment_licensings_id}}
                };
                $.ajax({
                   data: parametros,
                   url:  'detallelicencia',
                   type: 'get',

                   success: function(response){
                        
                      $("#ajax2").html(response);
                   }
                }); 
            // $('#tabla_licencias_equipos').html(response);
            toastr.success('Se Asignó correctamente la Licencia al equipo', 'Asignación de licencia');
            }
        });
      
        @endif

    }

    function eliminar_seriales(id_asignacion, id_licensing){
        

        var disp = {{$disp[0]->available}};
        @if(count($seriales) != 0 ) 
        $.get('eliminarseriales', {
            id_asignacion: id_asignacion,
            serial_asignar_id: id_licensing,
            id_licencia: {{$seriales[0]->equipment_licensings_id}},
            disp: disp
            
        } ,

        function(response){
            if (response == 1) {
                var parametros = {
                   "id": {{$seriales[0]->equipment_licensings_id}}
                };
                $.ajax({
                   data: parametros,
                   url:  'detallelicencia',
                   type: 'get',

                   success: function(response){
                        
                      $("#ajax2").html(response);
                   }
                }); 
            // $('#tabla_licencias_equipos').html(response);
            toastr.success('Se Inhabilito correctamente la Licencia del equipo', 'Inhabilitación de licencia');
            }
        });
      @endif

    }
</script>