
<style>
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }
    #wrapper {
        padding: 15px;
    }
    #software_adicionales {
        width: 300px;
    }
</style>



<div class="page-content">
         <div class="tab-content">
            <div class="tab-pane active" id="tab_1_2">
               <div class="portlet ">
                  <div class="portlet-title">
                     <div class="caption">
                        <i class="icon-reorder"></i> Ingresar Equipos
                     </div>
                  </div>
                  
                  <div class="portlet-body">
                     <ul  class="nav nav-tabs">
                        <li class="active"><a href="equipos">Ingresar Equipos</a></li>
                        <li class=""><a href="elementos">Asignar elementos a un Equipo</a></li>
                        <li class=""><a href="asignar">Asignar Equipos a usuarios</a></li>
                        <li class=""><a href="eliminar">Eliminar Asignación</a></li>
                        
                        <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                        
                        
                        <a href="#basic" data-toggle="modal" class="btn btn-default  pull-right" onclick="detalle()">Gestión Tipo de Equipos</a>                          
                     </ul>
                     <form class="form-horizontal" enctype="multipart/form-data" role="form">
                        <div class="form-body">
                           
                            <div class="row">
                                
                                <div class="form-group has-success">
                                    <div class="col-md-3">
                                        <label class="control-label">Tipo de Equipo</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                            <select id="tipo_equipo" name="tipo_equipo" class="form-control" onchange="activar_formularios_2()">
                                            <option value=""></option>
                                            @foreach($tipos as $tipo)
                                            <option value="{{$tipo->id}}">{{$tipo->id}} {{$tipo->type}}</option>
                                            @endforeach
                                            </select>
                                        </div> 
                                    </div>
                                    <div class="col-md-3" id="campo_usuario_asignar">
                                        <label class="control-label">Usuario</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                            <select id="usuario_id" name="concept" class="form-control" onchange="cargar_datos_user()">
                                            <option value=""></option>
                                            @foreach($usuarios as $usuario)
                                            <option value="{{$usuario->id}}">{{$usuario->name}} {{$usuario->last_name}}</option>
                                            @endforeach
                                            </select>
                                        </div> 
                                    </div>
                                    <div class="col-md-3" id="">
                                        <label class="control-label">Usuario</label>
                                        <div class="input-group" id="info_usuario">
                                            
                                        </div> 
                                    </div>
                                </div>
                                
                                <div class="col-md-12" id="formulario_ingreso_equipos" style="display:none">
                                    <div class="row note note-warning">
                                        <h4 class="block" id="titulo_formulario"></h4>
                                        <p>
                                        <div class="col-md-4">
                                            <div class="form-group" id="form_equipo_marca">
                                               <div class="col-md-12">
                                                    <label class="control-label">Marca</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_marca" name="equipo_marca" type="text" class="form-control campos_cpu">
                                                     <div id="display"></div>
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_modelo">
                                               <div class="col-md-12">
                                                    <label class="control-label">Modelo</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_modelo" name="equipo_modelo" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_serial">
                                               <div class="col-md-12">
                                                    <label class="control-label">Serial</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_serial" name="equipo_serial" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_ip">
                                               <div class="col-md-12">
                                                    <label class="control-label">Ip</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_ip" name="equipo_ip" type="text" class="form-control campos_cpu" placeholder="ej. 192.168.120.115">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_mac">
                                               <div class="col-md-12">
                                                    <label class="control-label">Mac</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_mac" name="equipo_mac" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="form_equipo_memoria_ram">
                                               <div class="col-md-12">
                                                    <label class="control-label">Memoria Ram</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_memoria_ram" name="equipo_memoria_ram" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            

                                            <div class="form-group" id="form_equipo_so">
                                               <div class="col-md-12">
                                                    <label class="control-label">Sistema Operativo</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_so" name="equipo_so" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_factura">
                                                <div class="col-md-12">
                                                    <label class="control-label">Factura</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="equipo_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_asignacion">
                                                <div class="col-md-12">
                                                    <label class="control-label">Fecha Asignacion</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="equipo_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_compra">
                                                <div class="col-md-12">
                                                    <label class="control-label">Fecha de Compra</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="equipo_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="form_equipo_disco_duro">
                                               <div class="col-md-12">
                                                    <label class="control-label">Disco Duro</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_disco_duro" name="equipo_disco_duro" type="text" class="form-control campos_cpu" placeholder="GB (Gigabyte)">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_procesador">
                                               <div class="col-md-12">
                                                    <label class="control-label">Procesador</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="equipo_procesador" name="equipo_procesador" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                                
                                            <div class="form-group" id="form_equipo_ubicacion">
                                                <div class="col-md-12">
                                                    <label class="control-label">Ubicacion</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        
                                                        <select id="equipo_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                        <option value="">Ubicación</option>
                                                        @foreach($ubicaciones as $ubicacion)
                                                        <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                        @endforeach
                                                        </select>
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-md-12" id="form_equipo_imagen">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                    <label class="control-label" id="titulo_imagen"></label>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file">
                                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                            <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                            <input type="file" class="default form-control" name="equipo_imagen" id="equipo_imagen" size="20">
                                                            </span>
                                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_eliminar"><i class="icon-trash"></i> Eliminar</a>
                                                            <div class="help-block">
                                                                Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </p>

                                    </div>
                                    
                                </div>
                                <div class="col-md-12" id="formulario_ingreso_monitor" style="display: none;">
                                    <div class="row note note-success">
                                        <h4 class="block" id="titulo_formulario">MONITOR</h4>
                                        <p>
                                        </p><div class="col-md-4">
                                            <div class="form-group" id="form_equipo_marca" style="">
                                               <div class="col-md-12">
                                                    <label class="control-label">Marca</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="monitor_marca" name="equipo_marca" type="text" class="form-control campos_cpu">
                                                     <div id="display"></div>
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_modelo" style="">
                                               <div class="col-md-12">
                                                    <label class="control-label">Modelo</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="monitor_modelo" name="equipo_modelo" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_serial" style="">
                                               <div class="col-md-12">
                                                    <label class="control-label">Serial</label>
                                                  <div class="input-group">
                                                     <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                     <input id="monitor_serial" name="equipo_serial" type="text" class="form-control campos_cpu" placeholder="">
                                                  </div> 
                                               </div>
                                            </div>
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="form_equipo_factura" style="">
                                                <div class="col-md-12">
                                                    <label class="control-label">Factura</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_asignacion" style="">
                                                <div class="col-md-12">
                                                    <label class="control-label">Fecha Asignacion</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_asignacion" name="factura" type="text" class="form-control fecha_required hasDatepicker" placeholder="Fecha Asignación" autocomplete="off">
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="form-group" id="form_equipo_compra" style="">
                                                <div class="col-md-12">
                                                    <label class="control-label">Fecha de Compra</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <input id="monitor_compra" name="factura" type="text" class="form-control fecha_required hasDatepicker" placeholder="Fecha Compra" autocomplete="off">
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group" id="form_equipo_ubicacion" style="">
                                                <div class="col-md-12">
                                                    <label class="control-label">Ubicacion</label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                        <select id="monitor_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                        <option value="">Ubicación</option>
                                                        @foreach($ubicaciones as $ubicacion)
                                                        <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                        @endforeach
                                                        </select>
                                                        
                                                        
                                                    </div> 
                                                </div>
                                            </div>
                                            <div class="col-md-12" id="form_equipo_imagen" style="">
                                                <div class="form-group">
                                                    <div class="col-md-12">
                                                    <label class="control-label" id="titulo_imagen">Imagen MONITOR</label>
                                                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                                                        <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="">
                                                        </div>
                                                        <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
                                                        <div>
                                                            <span class="btn btn-default btn-file">
                                                            <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                                            <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                                            <input type="file" class="default form-control" name="monitor_imagen" id="monitor_imagen" size="20">
                                                            </span>
                                                            <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_monitor"><i class="icon-trash"></i> Eliminar</a>
                                                            <div class="help-block">
                                                                Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                                            </div>
                                                        </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <p></p>

                                    </div>
                                </div>
                                <div class="col-md-12" id="formulario_ingreso_teclado" style="display:none">
                                    <div class="row note note-success" id="" style="" >
                                        <h4 class="block">Teclado</h4>
                                        <p>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="teclado_marca" name="clave" type="text" class="form-control" placeholder="Marca Teclado">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="teclado_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="teclado_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                            <option value="">Ubicación</option>
                                                            @foreach($ubicaciones as $ubicacion)
                                                            <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                            @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="teclado_asignacion" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="teclado_compra" name="factura" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </p>
                                    </div>
                                </div>
                                <div class="col-md-12" id="formulario_ingreso_mouse" style="display:none">
                                    <div class="row note note-success" id="" style="" >
                                        <h4 class="block">Mouse</h4>
                                        <p>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form_nombre">
                                                   <div class="col-md-12">
                                                      <div class="input-group">
                                                         <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                         <input id="mouse_marca" name="clave" type="text" class="form-control" placeholder="Marca Teclado">
                                                         <div id="display"></div>
                                                      </div> 
                                                   </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="mouse_factura" name="factura" type="text" class="form-control" placeholder="Factura">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            
                                                            <select id="mouse_ubicacion" name="concept" class="form-control" onchange="cargar_form()">
                                                            <option value="">Ubicación</option>
                                                            @foreach($ubicaciones as $ubicacion)
                                                            <option value="{{$ubicacion->id}}">{{$ubicacion->sede}}</option>
                                                            @endforeach
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="mouse_asignacion" name="monitor_asig" type="text" class="form-control fecha_required" placeholder="Fecha Asignación">
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="form-group" id="form_factura">
                                                    <div class="col-md-12">
                                                        <div class="input-group">
                                                            <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                            <input id="mouse_compra" name="monitor_compra" type="text" class="form-control fecha_required" placeholder="Fecha Compra">
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </p>
                                    </div>
                                </div>
                                 
                                
                                
                            </div>
                            
                        </div>
                            

                        <div class="form-actions right">                           
                           <a class="btn btn-success" onclick="validar_campos()">Registrar</a>                            
                        </div>
                     
                  </div>
                  <div id="wrapper">
                    
                    <input value="activate tagator" id="activate_tagator" type="button" style="display:none">
                  </div>

               </div>
            </div>
        </div> 
        <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
            <div class="modal-dialog  modal-wide">
                <div class="modal-content">
               
               
                    <input type="hidden" ng-model="support_id" value="" id="support_id">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Gestión Tipo de Equipos</h4>
                    </div>
                    <div class="modal-body form-body" id="">
                        <table class="table top-blue">
                            <thead>
                                <tr class="center">
                                    <th>Id</th>
                                    <th>Tipo Equipo</th>
                                    <th>Opciones</th>
                                </tr>
                            </thead>
                            <tbody class="center" id="tabla_tipo_equipos">
                                @foreach($tipos as $tipo)
                                    @if($tipo->status == 0)  
                                    <tr class="red">
                                    @else
                                    <tr>
                                    @endif
                                        <td>{{$tipo->id}}</td>
                                        <td>{{$tipo->type}}</td>
                                        @if($tipo->status == 0)  
                                            <td><button type="submit" class="btn btn-success" id="{{$tipo->id}}" onclick="desactivar_tipo({{$tipo->id}}, 1)"><i class="icon-ok"></i> Activar</button></td>
                                        @else
                                            <td><button type="submit" class="btn btn-danger" id="{{$tipo->id}}" onclick="desactivar_tipo({{$tipo->id}}, 0)"><i class="icon-remove"></i> Desactivar</button></td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>

                        </table>
                        <div class="row">
                        <div class="form-group has-success" id="form_nombre">
                            <div class="col-md-3">
                            <label class="control-label">Tipo de Equipo</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                    <input id="nuevo_tipo_equipo" name="nuevo_tipo_equipo" class="form-control">
                                </div> 
                            </div>
                        </div>
                        </div>
                              
                       
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success refresh" onclick="Ingresar_tipo_equipo()">Ingresar</button>

                       
                        <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal">Cerrar</button>
                    </div>
               
               
                </div>
            </div>

            <script src="/indo/public/js/upload.js"></script>                
        
        </div>        
      
   
</div>

<script>
    function cargar_datos_user(){

        var usuario_id = $("#usuario_id").val();
        if (usuario_id == "") {
           $("#info_usuario").html(""); 
           return;
        }

        $.ajax({
                type: "GET",
                url:  "cargarinfouser",
                data: { usuario_id: usuario_id}
        })
        .done(function(data) {
            if (data != 1) {
                
                $("#info_usuario").html(data);
            }else{
                toastr.warning('No ha sido posible mostrar los datos del usuario', 'Info');
            }
        });
    }
    function activar_formularios_2(){
        var tipo_equipo = $('#tipo_equipo').val();
        if (tipo_equipo != "") {
            $("#formulario_ingreso_equipos").show('show');
            // $("#formulario_ingreso_teclado").show('show');
            // $("#formulario_ingreso_mouse").show('show');
        }else{
            $("#formulario_ingreso_equipos").hide('slow');
            $("#formulario_ingreso_teclado").hide('slow');
            $("#formulario_ingreso_mouse").hide('slow');
            $("#formulario_ingreso_monitor").hide('slow');
        }


        @foreach($tipos as $tipo)

            if (tipo_equipo == {{$tipo->id}}) {
                
                $('#titulo_formulario').html('{{$tipo->type}}');
                $('#titulo_imagen').html('Imagen {{$tipo->type}}');
                if (tipo_equipo == 1) {
                    
                    $("#form_equipo_marca").show('show');
                    $("#form_equipo_modelo").show('show');
                    $("#form_equipo_serial").show('show');
                    $("#form_equipo_ip").show('show');
                    $("#form_equipo_mac").show('show');
                    $("#form_equipo_memoria_ram").show('show');
                    $("#form_equipo_imagen").show('show');
                    $("#form_equipo_disco_duro").show('show');
                    $("#form_equipo_procesador").show('show');
                    $("#form_equipo_so").show('show');
                    $("#form_equipo_factura").show('show');
                    $("#form_equipo_asignacion").show('show');
                    $("#form_equipo_compra").show('show');
                    $("#form_equipo_ubicacion").show('show');

                    $("#formulario_ingreso_teclado").show('show');
                    $("#formulario_ingreso_mouse").show('show');
                    $("#formulario_ingreso_monitor").show('show');

                    $("#campo_usuario_asignar").show('show');

                }
                if (tipo_equipo == 2) {
                    
                    $("#form_equipo_marca").show('show');
                    $("#form_equipo_modelo").show('show');
                    $("#form_equipo_serial").show('show');
                    $("#form_equipo_ip").show('show');
                    $("#form_equipo_mac").show('show');
                    $("#form_equipo_memoria_ram").show('show');
                    $("#form_equipo_imagen").show('show');
                    $("#form_equipo_disco_duro").show('show');
                    $("#form_equipo_procesador").show('show');
                    $("#form_equipo_so").show('show');
                    $("#form_equipo_factura").show('show');
                    $("#form_equipo_asignacion").show('show');
                    $("#form_equipo_compra").show('show');
                    $("#form_equipo_ubicacion").show('show');

                    $("#formulario_ingreso_teclado").show('show');
                    $("#formulario_ingreso_mouse").show('show');
                    // $("#formulario_ingreso_monitor").show('show');

                    $("#campo_usuario_asignar").show('show');
                    $("#formulario_ingreso_monitor").hide('slow');

                }
                if (tipo_equipo == 3 || tipo_equipo == 4 || tipo_equipo == 6) {
                    
                    $("#form_equipo_marca").show('show');
                    $("#form_equipo_modelo").show('show');
                    $("#form_equipo_serial").show('show');
                    $("#form_equipo_imagen").show('show');
                    $("#form_equipo_factura").show('show');
                    $("#form_equipo_asignacion").show('show');
                    $("#form_equipo_compra").show('show');
                    $("#form_equipo_ubicacion").show('show');

                    $("#form_equipo_ip").hide('slow');
                    $("#form_equipo_mac").hide('slow');
                    $("#form_equipo_memoria_ram").hide('slow');
                    $("#form_equipo_disco_duro").hide('slow');
                    $("#form_equipo_procesador").hide('slow');
                    $("#form_equipo_so").hide('slow');

                    $("#formulario_ingreso_teclado").hide('slow');
                    $("#formulario_ingreso_mouse").hide('slow');
                    $("#formulario_ingreso_monitor").hide('slow');

                    $("#usuario_id").val('');
                    $("#campo_usuario_asignar").hide('slow');
                    
                }
                
                if (tipo_equipo == 7 || tipo_equipo == 5) {
                    
                    $("#form_equipo_marca").show('show');
                    $("#form_equipo_modelo").show('show');
                    $("#form_equipo_serial").show('show');
                    $("#form_equipo_ip").show('show');
                    $("#form_equipo_mac").show('show');
                    $("#form_equipo_memoria_ram").show('show');
                    $("#form_equipo_imagen").show('show');
                    $("#form_equipo_disco_duro").show('show');
                    $("#form_equipo_procesador").show('show');
                    $("#form_equipo_so").show('show');
                    $("#form_equipo_factura").show('show');
                    $("#form_equipo_asignacion").show('show');
                    $("#form_equipo_compra").show('show');
                    $("#form_equipo_ubicacion").show('show');

                    $("#formulario_ingreso_teclado").hide('hide');
                    $("#formulario_ingreso_mouse").hide('hide');
                    $("#formulario_ingreso_monitor").hide('slow');

                    $("#campo_usuario_asignar").show('show');

                }if (tipo_equipo == 8) {
                    $("#formulario_ingreso_equipos").hide('slow');
                    $("#formulario_ingreso_teclado").show('show');
                    $("#formulario_ingreso_monitor").hide('slow');
                    $("#formulario_ingreso_mouse").hide('slow');

                    $("#campo_usuario_asignar").hide('slow');
                    
                }if (tipo_equipo == 9) {
                    $("#formulario_ingreso_equipos").hide('slow');
                    $("#formulario_ingreso_teclado").hide('slow');
                    $("#formulario_ingreso_monitor").hide('slow');
                    $("#formulario_ingreso_mouse").show('show');

                    $("#campo_usuario_asignar").hide('slow');
                    

                }   
                
            }else{
                $('#formulario_{{$tipo->id}}').hide();
            }
        @endforeach


        
    }
    function Ingresar_tipo_equipo(){
        var tipo_equipo = $('#nuevo_tipo_equipo').val();
        if (tipo_equipo == "") {
            toastr.error('El campo Tipo de Equipo está vacio.', 'Error');
            return;
        }
        $.ajax({
                type: "GET",
                url:  "creartipoequipo",
                data: { tipo_equipo: tipo_equipo}
        })
        .done(function(data) {
            if (data != 1) {
                toastr.success('Se ingreso correctamente el Tipo de Equipo', 'Nuevo Tipo de Equipo');
                $("#nuevo_tipo_equipo").val("");
                $("#tabla_tipo_equipos").html(data);
            }else{
                toastr.error('No ha sido posible ingresar el  Tipo de Equipo', 'Error');
            }
        });
    }
    function desactivar_tipo(id, status){
        
        $.ajax({
                type: "GET",
                url:  "creartipoequipo",
                data: { desactivar_id: id, status: status}
        })
        .done(function(data) {
            if (data != 2) {
                toastr.success('Se Gestiono correctamente el Tipo de Equipo', 'Desactivo Tipo de Equipo');
                
                $("#tabla_tipo_equipos").html(data);
                
            }else{
                toastr.error('No ha sido posible Gestionar el Tipo de Equipo', 'Error');
            }
        });
    }

    function validar_campos(){

        var tipo_equipo = $('#tipo_equipo').val();
        if (tipo_equipo == 1) {
            var validacion_campo_1 = validar_form_normal();
            if (validacion_campo_1 == 1) {
                toastr.error('El campo Imagen esta vacio.', 'Error');
                return;
            }
            if (validacion_campo_1 == 2) {
                toastr.error('Por favor verifique los campos del Formulario.', 'Error');
                return;
            }
            var validacion_campo_1 = validar_form_monitor();
            if (validacion_campo_1 == 1) {
                toastr.error('El campo Imagen del monitor esta vacio.', 'Error');
                return;
            }
            if (validacion_campo_1 == 2) {
                toastr.error('Por favor verifique los campos del Formulario monitor.', 'Error');
                return;
            }
            var validacion_campo_1 = validar_form_teclado();
            if (validacion_campo_1 == 2){
                toastr.error('Por favor verifique los campos del Formulario Teclado.', 'Error');   
                return;
            }
            var validacion_campo_1 = validar_form_mouse();
            if (validacion_campo_1 == 2){
                toastr.error('Por favor verifique los campos del Formulario Mouse.', 'Error'); 
                return;  
            }

            enviar_datos_formulario();
        }
        if (tipo_equipo ==  2) {
            var validacion_campo_1 = validar_form_normal();
            if (validacion_campo_1 == 1) {
                toastr.error('El campo Imagen esta vacio.', 'Error');
                return;
            }
            if (validacion_campo_1 == 2) {
                toastr.error('Por favor verifique los campos del Formulario.', 'Error');
                return;
            }

            
            var validacion_campo_1 = validar_form_teclado();
            if (validacion_campo_1 == 2){
                toastr.error('Por favor verifique los campos del Formulario Teclado.', 'Error');   
                return;
            }
            var validacion_campo_1 = validar_form_mouse();
            if (validacion_campo_1 == 2){
                toastr.error('Por favor verifique los campos del Formulario Mouse.', 'Error'); 
                return;  
            }

            enviar_datos_formulario();
        }
        if (tipo_equipo == 3 || tipo_equipo ==  4 || tipo_equipo == 6) {
            var validacion_campo_1 = validar_form_346();
            if (validacion_campo_1 == 1) {
                toastr.error('Por favor verifique los campos del Formulario.', 'Error');
                return;
            }
            

            enviar_datos_formulario();
            // enviar_datos_monitor();
        }
        if (tipo_equipo == 5 || tipo_equipo ==  7) {
            var validacion_campo_1 = validar_form_normal();
            if (validacion_campo_1 == 1) {
                toastr.error('Por favor verifique los campos del Formulario.', 'Error');
                return;
            }
            

            enviar_datos_formulario();
        }
        if (tipo_equipo == 8) {

            var validacion_campo_1 = validar_form_teclado();
            if (validacion_campo_1 == 2) {
                toastr.error('Por favor verifique los campos del Formulario Teclado.', 'Error');
                return;
            }
            

            // enviar_datos_formulario();
        }
        if (tipo_equipo == 9) {
            var validacion_campo_1 = validar_form_mouse();
            if (validacion_campo_1 == 2) {
                toastr.error('Por favor verifique los campos del Formulario Mouse.', 'Error');
                return;
            }
            

            // enviar_datos_formulario();
        }
        return;
    }
    function validar_form_normal(){
        var form_equipo_marca       = $("#equipo_marca").val();
        var form_equipo_modelo      = $("#equipo_modelo").val();
        var form_equipo_serial      = $("#equipo_serial").val();
        var form_equipo_ip          = $("#equipo_ip").val();
        var form_equipo_mac         = $("#equipo_mac").val();
        var form_equipo_memoria_ram = $("#equipo_memoria_ram").val();
        var form_equipo_disco_duro  = $("#equipo_disco_duro").val();
        var form_equipo_procesador  = $("#equipo_procesador").val();
        var form_equipo_so          = $("#equipo_so").val();
        var form_equipo_factura     = $("#equipo_factura").val();
        var form_equipo_asignacion  = $("#equipo_asignacion").val();
        var form_equipo_compra      = $("#equipo_compra").val();
        var form_equipo_ubicacion   = $("#equipo_ubicacion").val();
        var form_equipo_imagen      = $("#equipo_imagen").val();

        if (form_equipo_imagen == "") {
            return 1;
        }
        if (form_equipo_marca == "" || form_equipo_modelo == "" || form_equipo_serial == "" || form_equipo_ip == "" || form_equipo_mac == "" || form_equipo_memoria_ram == "" || form_equipo_disco_duro == "" || form_equipo_procesador == "" || form_equipo_so == "" || form_equipo_factura == "" || form_equipo_asignacion == "" || form_equipo_compra == "" || form_equipo_ubicacion == "") {
            return 2;
        }
        return 0;
    }
    function validar_form_monitor(){
        var monitor_marca           = $("#monitor_marca").val();
        var monitor_modelo          = $("#monitor_modelo").val();
        var monitor_serial          = $("#monitor_serial").val();
        var monitor_factura         = $("#monitor_factura").val();
        var monitor_asignacion      = $("#monitor_asignacion").val();
        var monitor_compra          = $("#monitor_compra").val();
        var monitor_ubicacion       = $("#monitor_ubicacion").val();
        var monitor_imagen          = $("#monitor_imagen").val();

        if (monitor_imagen == "") {
            return 1;
        }
        if (monitor_marca == "" || monitor_modelo == "" || monitor_serial == "" || monitor_factura == "" || monitor_asignacion == "" || monitor_compra == "" || monitor_ubicacion == "") {
            return 2;
        }
    }
    function validar_form_teclado(){

        var teclado_marca           = $("#teclado_marca").val();
        var teclado_factura         = $("#teclado_factura").val();
        var teclado_asignacion      = $("#teclado_asignacion").val();
        var teclado_compra          = $("#teclado_compra").val();
        var teclado_ubicacion       = $("#teclado_ubicacion").val(); 

        if (teclado_marca == "" || teclado_factura == "" || teclado_asignacion == "" || teclado_compra == "" || teclado_ubicacion == "") {
            return 2;
        }
    }
    function validar_form_mouse(){
        var mouse_marca             = $("#mouse_marca").val();
        var mouse_factura           = $("#mouse_factura").val();
        var mouse_ubicacion         = $("#mouse_ubicacion").val();
        var mouse_asignacion        = $("#mouse_asignacion").val();
        var monitor_compra          = $("#mouse_compra").val();

        if (mouse_marca == "" ||mouse_factura == "" ||mouse_ubicacion == "" ||mouse_asignacion == "" ||monitor_compra == "") {
            return 2;
        }
    }
    function validar_form_346(){
        var form_equipo_marca       = $("#equipo_marca").val();
        var form_equipo_modelo      = $("#equipo_modelo").val();
        var form_equipo_serial      = $("#equipo_serial").val();
        var form_equipo_factura     = $("#equipo_factura").val();
        var form_equipo_asignacion  = $("#equipo_asignacion").val();
        var form_equipo_compra      = $("#equipo_compra").val();
        var form_equipo_ubicacion   = $("#equipo_ubicacion").val();
        var form_equipo_imagen      = $("#equipo_imagen").val();

        if (form_equipo_marca == "" || form_equipo_modelo == "" || form_equipo_serial == "" || form_equipo_factura == "" || form_equipo_asignacion == "" || form_equipo_compra == "" || form_equipo_ubicacion == "" || form_equipo_imagen == "") {
            return 2;
        }
    }

    function enviar_datos_formulario(){

        var tipo_equipo             = $("#tipo_equipo").val();
        var usuario_id              = $("#usuario_id").val();

        var form_equipo_marca       = $("#equipo_marca").val();
        var form_equipo_modelo      = $("#equipo_modelo").val();
        var form_equipo_serial      = $("#equipo_serial").val();
        var form_equipo_ip          = $("#equipo_ip").val();
        var form_equipo_mac         = $("#equipo_mac").val();
        var form_equipo_memoria_ram = $("#equipo_memoria_ram").val();
        var form_equipo_disco_duro  = $("#equipo_disco_duro").val();
        var form_equipo_procesador  = $("#equipo_procesador").val();
        var form_equipo_so          = $("#equipo_so").val();
        var form_equipo_factura     = $("#equipo_factura").val();
        var form_equipo_asignacion  = $("#equipo_asignacion").val();
        var form_equipo_compra      = $("#equipo_compra").val();
        var form_equipo_ubicacion   = $("#equipo_ubicacion").val();
        var form_equipo_imagen      = $("#equipo_imagen").val();

        var teclado_marca           = $("#teclado_marca").val();
        var teclado_factura         = $("#teclado_factura").val();
        var teclado_asignacion      = $("#teclado_asignacion").val();
        var teclado_compra          = $("#teclado_compra").val();
        var teclado_ubicacion       = $("#teclado_ubicacion").val();

        var mouse_marca             = $("#mouse_marca").val();
        var mouse_factura           = $("#mouse_factura").val();
        var mouse_ubicacion         = $("#mouse_ubicacion").val();
        var monitor_asignacion      = $("#monitor_asignacion").val();
        var monitor_compra          = $("#monitor_compra").val(); 

        $("#equipo_imagen").upload('ingresarnuevoequipo', 
        {
            form_equipo_marca: form_equipo_marca,
            form_equipo_modelo: form_equipo_modelo,
            form_equipo_serial: form_equipo_serial,
            form_equipo_ip: form_equipo_ip,
            form_equipo_mac: form_equipo_mac,
            form_equipo_memoria_ram: form_equipo_memoria_ram,
            form_equipo_disco_duro: form_equipo_disco_duro,
            form_equipo_procesador: form_equipo_procesador,
            form_equipo_so: form_equipo_so,
            form_equipo_factura: form_equipo_factura,
            form_equipo_asignacion: form_equipo_asignacion,
            form_equipo_compra: form_equipo_compra,
            form_equipo_ubicacion: form_equipo_ubicacion,
            form_equipo_imagen: form_equipo_imagen,

            teclado_marca: teclado_marca,
            teclado_factura: teclado_factura,
            teclado_asignacion: teclado_asignacion,
            teclado_compra: teclado_compra,
            teclado_ubicacion: teclado_ubicacion,

            mouse_marca: mouse_marca,
            mouse_factura: mouse_factura,
            mouse_ubicacion: mouse_ubicacion,
            monitor_asignacion: monitor_asignacion,
            monitor_compra: monitor_compra,
            tipo_equipo:tipo_equipo,
            usuario_id:usuario_id,
        },
        function(respuesta){
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente el nuevo elemento', 'Nuevo elemento');
                
                if (tipo_equipo == 1 || tipo_equipo ==  2) {
                    enviar_datos_monitor();
                }

                $("#equipo_marca").val("");
                $("#equipo_modelo").val("");
                $("#equipo_serial").val("");
                $("#equipo_ip").val("");
                $("#equipo_mac").val("");
                $("#equipo_memoria_ram").val("");
                $("#equipo_disco_duro").val("");
                $("#equipo_procesador").val("");
                $("#equipo_so").val("");
                $("#equipo_factura").val("");
                $("#equipo_asignacion").val("");
                $("#equipo_compra").val("");
                $("#equipo_ubicacion").val("");
                $("#equipo_imagen").val("");
                $("#teclado_marca").val("");
                $("#teclado_factura").val("");
                $("#teclado_asignacion").val("");
                $("#teclado_compra").val("");
                $("#teclado_ubicacion").val("");
                $("#mouse_marca").val("");
                $("#mouse_factura").val("");
                $("#mouse_ubicacion").val("");
                $("#monitor_asignacion").val("");
                $("#monitor_compra").val(""); 

                $("#btn_eliminar").click();

                $("#tipo_equipo").val("");
                $("#usuario_id").val("");

                
                
            } else {
                toastr.error('No ha sido posible ingresar el  nuevo elemento.', 'Error');                              
                
            }
        });
    }

    function enviar_datos_monitor(){

        var tipo_equipo             = $("#tipo_equipo").val();
        var usuario_id              = $("#usuario_id").val();

        var monitor_marca           = $("#monitor_marca").val();
        var monitor_modelo          = $("#monitor_modelo").val();
        var monitor_serial          = $("#monitor_serial").val();
        var monitor_factura         = $("#monitor_factura").val();
        var monitor_asignacion      = $("#monitor_asignacion").val();
        var monitor_compra          = $("#monitor_compra").val();
        var monitor_ubicacion       = $("#monitor_ubicacion").val();
        var monitor_imagen          = $("#monitor_imagen").val();

        $("#monitor_imagen").upload('ingresarnuevomonitor', 
        {
            monitor_marca :monitor_marca,
            monitor_modelo :monitor_modelo,
            monitor_serial :monitor_serial,
            monitor_factura :monitor_factura,
            monitor_asignacion :monitor_asignacion,
            monitor_compra :monitor_compra,
            monitor_ubicacion :monitor_ubicacion,
            tipo_equipo:tipo_equipo,
            usuario_id:usuario_id
            
        },
        function(respuesta){
            
            if (respuesta === 1) {
                toastr.success('Se ingreso correctamente el nuevo monitor', 'Nuevo monitor');
                    
                $("#monitor_marca").val("");
                $("#monitor_modelo").val("");
                $("#monitor_serial").val("");
                $("#monitor_factura").val("");
                $("#monitor_asignacion").val("");
                $("#monitor_compra").val("");
                $("#monitor_ubicacion").val("");
                $("#monitor_imagen").val("");

                $("#btn_monitor").click();

                $("#tipo_equipo").val("");
                $("#usuario_id").val("");
                
            } else {
                toastr.error('No ha sido posible ingresar el  nuevo monitor.', 'Error');                              
                
            }
        });
    }

    
    




</script>




