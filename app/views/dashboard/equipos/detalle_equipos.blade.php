<script type='text/javascript'>
    $(document).ready(function() {
        Tipped.create('.informacion', { 
            size: 'medium',
            skin: 'light',
            maxWidth: 300
        });
    
        Tipped.create('.nameimg', { 
            size: 'medium',
            skin: 'light',
            maxWidth: 300
      
        });
    });
</script>
<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
    <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Detalle de equipos</h4>
      </div>
      <div class="modal-body form-body">
      <!-- <a class="btn btn-default pull-right"  id="" onclick="activar_de_baja()">Dar Equipo de Baja</a> -->
         

                <div class="portlet">
                   <div class="portlet-title">
                     <div class="caption">Información General</div>
                     <div class="tools">
                       <a href="javascript:;" class="expand"></a>
                     </div>
                   </div>
                   <div class="portlet-body">
                     <div class="row">
                       <div class="col-md-6">
                         <input type="hidden" value="{{$equipos[0]->id_equipo}}" name="tipo-equipo" id="id_equipo_edit">
                         <table class="table middle well">
                           <tbody>
                             <tr>
                               <td>Funcionario</td>
                               <td>
                                 <span class="tag">
                                    <select name="usuario" id="" class="form-control input-sm clear-border" readonly="">
                                        <option>{{$users[0]->name}} {{$users[0]->name2}} {{$users[0]->last_name}} {{$users[0]->last_name2}}</option>
                                    </select>
                                 </span>
                                 <span class="test-img"><img class="inside-avatar" src="../{{$users[0]->img_min}}"></span>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                         @foreach($equipos as $equipo)
                         @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>
                             <tr>
                                <td>Tipo de Equipo </td>
                                <td>
                                 {{$equipo->type}}
                                </td>
                             </tr>
                             <tr>
                               <td>Imagen</td>
                               <td>
                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        @if($equipo->image == "")
                                            <img src="../assets/img/equipos/-1.png" alt="">
                                        @else
                                            <img src="../{{$equipo->image}}" alt="">
                                        @endif
                                   </div>
                                   <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                   
                                   <div>
                                       <span class="btn btn-default btn-file">
                                       <span class="fileupload-new"><i class="icon-paper-clip"></i> Seleccionar imagen</span>
                                       <span class="fileupload-exists"><i class="icon-undo"></i> Cambiar</span>
                                       <input type="file" class="default form-control" name="imagen_monitor_editar" id="equipo_imagen_editar" size="20">
                                       </span>
                                       <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload" id="btn_monitor"><i class="icon-trash"></i> Eliminar</a>
                                       <div class="help-block">
                                           Máximo 1 MB .pdf, .jpg, .png, .docx, .xlsx
                                       </div>
                                   </div>
                                 </div>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                         @endif
                         @endforeach
                       </div>
                       <div class="col-md-6">
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>  
                             <tr>
                               <td>Marca</td>
                               <td><input name="marca" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->mark}}" id="equipo_marca_editar"></td>
                             </tr>
                             <tr>
                               <td>Modelo</td>
                               <td><input name="modelo" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->model}}" id="equipo_modelo_editar"></td>
                             </tr>
                             <tr>
                               <td>Serial</td>
                               <td><input name="serial" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->serial}}" id="equipo_serial_editar"></td>
                             </tr>
                             <tr>
                               <td>Entrega del Equipo</td>
                               <td><input name="entrega" class="tag form-control input-medium clear-border"  value="{{$equipo->date_asign}}" id="equipo_fechaengrega_editar"></td>
                             </tr>
                             <tr>
                               <td>Estado</td>
                               <td><input name="estado" class="tag form-control clear-border input-medium" type="text"  value="@if($equipo->status == 1) Activo @else  Inactivo @endif" id="equipo_estado_editar"></td>
                             </tr>
                             <tr>
                               <td>Nombre Equipo</td>
                               <td><input name="estado" class="tag form-control clear-border input-medium" type="text"  value="{{$equipo->observation}}" id="equipo_nombre_editar"></td>
                             </tr>
                           </tbody>
                         </table>
                         @endif
                         @endforeach 
                       </div>
                     </div>
                   </div>
                </div>
                <div class="portlet">
                    <div class="portlet-title">
                      <div class="caption">Características</div>
                      <div class="tools">
                        <a href="javascript:;" class="expand"></a>
                      </div>
                    </div>
                    <div class="portlet-body display-hide">
                      <div class="row">
                        <div class="col-md-6">
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                            <table class="table middle well">
                              <tbody>
                                <tr>
                                  <td>IP</td>
                                  <td><input  name="ip" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->ip}}" id="equipo_ip_editar"></td>
                                </tr>
                               
                                <tr>
                                  <td>MAC</td>
                                  <td><input  name="mac" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->mac}}" id="equipo_mac_editar"></td>
                                </tr>
                                <tr>
                                  <td>Memoria RAM</td>
                                  <td>
                                      <input name="ram" type="text" class="form-control input-medium clear-border" name="ram" value="{{$equipo->memory}}" id="equipo_ram_editar" >
                                  </td>
                                </tr>
                                <tr>
                                  <td>Disco Duro</td>
                                  <td><input  name="disco-duro" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->hard_drive}}" id="equipo_disco_editar"></td>
                                </tr>
                                <tr>
                                  <td>Procesador</td>
                                  <td>
                                    <input  name="procesador" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->processor}}" id="equipo_procesador_editar">  
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                        @endif
                        @endforeach 
                          </div>
                          <div class="col-md-6">
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                            <table class="table middle well">
                              <tbody>
                                <tr>
                                  <td>Sistema Operativo</td>
                                  <td>
                                    
                                      <input  name="licencia-office" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->operating_system}}">
                                    
                                  </td>
                                </tr>
                                <tr>
                                  <td>Versión</td>
                                  <td><input  name="os-version" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->version_op}}"></td>
                                </tr>
                                <tr>
                                  <td>Office</td>
                                  <td>
                                    <input  name="licencia-office" readonly="" class="tag form-control clear-border input-medium" type="text" value="">
                                  </td>
                                </tr>
                                <tr>
                                  <td>Licencia Office</td>
                                  <td><input  name="licencia-office" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->license_of}}"></td>
                                </tr>
                                <tr>
                                  <td>N° de licencia SO</td>
                                  <td><input  name="os-numero" readonly="" class="tag form-control clear-border input-medium" type="text" value="{{$equipo->no_license_op}}"></td>
                                </tr>
                              </tbody>
                        @endif
                        @endforeach 
                            </table>    
                          </div>
                        </div>
                    </div>
                </div>
                <div class="portlet">
                    <div class="portlet-title">
                      <div class="caption">Perifericos</div>
                      <div class="tools">
                        <a href="javascript:;" class="expand"></a>
                      </div>
                    </div>
                    <div class="portlet-body display-hide">
                      <div class="row">
                        <div class="col-md-12">
                        <button type="button" class="btn btn-info pull-right"  id="btn_editar" onclick="activar_form()">Editar</button>
                        <button type="button" class="btn btn-default pull-right"  id="btn_cancelar" onclick="desact_form()" style="display:none">Cancelar</button>
                        <button type="button" class="btn btn-success pull-right"  id="btn_enviar" onclick="enviar_datos_perifericos()" style="display:none">Actualizar</button>
                            <table class="table middle well">
                                
                                    <tr>
                                      <th style="text-align:center">Elemento</th>
                                      <th style="text-align:center">Marca</th>
                                      <th style="text-align:center">Modelo</th>
                                      <th style="text-align:center">Serial</th>
                                    </tr>
                                
                                <tbody id="tabla_perifericos">
                            @foreach($equipos as $equipo)
                            @if($equipo->equipment_types_id != 7 && $equipo->equipment_types_id != 5 && $equipo->equipment_types_id != 2)
                                <tr style="text-align:center" class="" id="">
                                  <td>{{$equipo->type}}</td>
                                  <td><input id="{{$equipo->type}}_marca" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->mark}}" readonly=""></td>
                                  <td><input id="{{$equipo->type}}_modelo" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->model}}" readonly=""></td>
                                  <td><input id="{{$equipo->type}}_serial" class="tag form-control clear-border input-medium" maxlength="15" size="15" type="text" value="{{$equipo->serial}}" readonly=""></td>
                                </tr>
                            @endif
                            @endforeach 
                              </tbody>
                            </table>
                          </div>
                          
                        </div>
                    </div>
                </div>
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">Mantenimientos</div>
                        <div class="tools">
                            <a href="javascript:;" class="expand"></a>
                        </div>
                    </div>
                    <div class="portlet-body display-hide">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table  top-blue">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>FECHA</th>
                                            <th>Usuario</th>
                                            <th>DETALLE</th>
                                            <th>TIPO MANT</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody class="center">                         
                                  
                                        @foreach ($mantenimientos as $mant)
                                        <tr @if($mant->equipment_maintenances_statuses_id == 1) class="red" @else class="green" @endif>
                                            <td>{{'MANT' . str_pad($mant->id, 6, "0", STR_PAD_LEFT)}}</td> 
                                            <td>{{$mant->created_at}}</td> 
                                            <td>
                                                @if($mant->users_id == "")
                                                Sin Usuario
                                                @else
                                                {{$mant->Users->name}} {{$mant->Users->last_name}}
                                                @endif
                                            </td> 
                                            <td>{{$mant->detalle}}</td> 
                                            <td>{{$mant->EquipmentTypeMaintenances->type_maintenance}}</td> 
                                            <td>{{$mant->EquipmentMaintenancesStatuses->status}}</td> 
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">Codigos QR</div>
                        <div class="tools">
                            <a href="javascript:;" class="expand"></a>
                        </div>
                    </div>
                    <div class="portlet-body display-hide">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table middle well">
                                    <tr>
                                    @foreach($equipos as $equipo)
                                    @if($equipo->equipment_types_id != 8 && $equipo->equipment_types_id != 9 )
                                        
                                        <th style="text-align:center">{{$equipo->type}}</th>
                                        
                                    @endif
                                    @endforeach 
                                        
                                    </tr>
                                        
                                    
                                    <tbody id="tabla_perifericos">
                                        <tr style="text-align:center" class="" id="">
                                            @foreach($equipos as $equipo)
                                            @if($equipo->equipment_types_id != 8 && $equipo->equipment_types_id != 9 )
                                                <td class="center">{{ QrCode::size(100)->generate( URL::to("soporte/equiposqr2?id=$equipo->id_user&id_equipo=$equipo->equipments_id")); }}</td>
                                                
                                            @endif
                                            @endforeach 
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                
         
      </div>
      <div class="modal-footer">
          
          <button type="button" class="btn btn-success"  id="" onclick="actualizar_datos_equipo()">Actualizar</button>
          <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_edit">Cerrar</button>
      </div>
   
   </form>   
   </div>
</div>

<script src="/sgi/public/js/upload.js"></script>


<script type="text/javascript">
    
    function actualizar_datos_equipo(){
        var equipo_marca_editar = $("#equipo_marca_editar").val();
        var equipo_modelo_editar = $("#equipo_modelo_editar").val();
        var equipo_serial_editar = $("#equipo_serial_editar").val();
        var equipo_fechaengrega_editar = $("#equipo_fechaengrega_editar").val();
        var equipo_estado_editar = $("#equipo_estado_editar").val();
        var equipo_ip_editar = $("#equipo_ip_editar").val();
        var equipo_mac_editar = $("#equipo_mac_editar").val();
        var equipo_ram_editar = $("#equipo_ram_editar").val();
        var equipo_disco_editar = $("#equipo_disco_editar").val();
        var equipo_procesador_editar = $("#equipo_procesador_editar").val();
        var id_equipo_edit = $("#id_equipo_edit").val();
        var equipo_nombre_editar = $("#equipo_nombre_editar").val();

        if(equipo_marca_editar == ""){
            toastr.error('El campo Marca no puede estar vacio', 'Error');
            return;
        }
        if(equipo_modelo_editar == ""){
            toastr.error('El campo Modelo no puede estar vacio', 'Error');
            return;
        }
        if(equipo_serial_editar == ""){
            toastr.error('El campo Serial no puede estar vacio', 'Error');
            return;
        }
        if(equipo_fechaengrega_editar == ""){
            toastr.error('El campo Fecha de Entrega no puede estar vacio', 'Error');
            return;
        }
        if(equipo_estado_editar == ""){
            toastr.error('El campo Estado no puede estar vacio', 'Error');
            return;
        }
        if(equipo_ip_editar == ""){
            toastr.error('El campo Ip no puede estar vacio', 'Error');
            return;
        }
        if(equipo_mac_editar == ""){
            toastr.error('El campo Mac no puede estar vacio', 'Error');
            return;
        }
        if(equipo_ram_editar == ""){
            toastr.error('El campo Ram no puede estar vacio', 'Error');
            return;
        }
        if(equipo_disco_editar == ""){
            toastr.error('El campo Disco Duro no puede estar vacio', 'Error');
            return;
        }
        if(equipo_procesador_editar == ""){
            toastr.error('El campo Procesador no puede estar vacio', 'Error');
            return;
        }if (equipo_nombre_editar == "") {
            toastr.error('El campo Nombre equipo no puede estar vacio', 'Error');
            return;
        }

        var equipo_imagen_editar = $("#equipo_imagen_editar").val();

        if (equipo_imagen_editar != "") {
            $("#equipo_imagen_editar").upload('actualizardatosequipos', 
            {   
                equipo_marca_editar:        equipo_marca_editar,
                equipo_modelo_editar:       equipo_modelo_editar,
                equipo_serial_editar:       equipo_serial_editar,
                equipo_fechaengrega_editar: equipo_fechaengrega_editar,
                equipo_ip_editar:           equipo_ip_editar,
                equipo_mac_editar:          equipo_mac_editar,
                equipo_ram_editar:          equipo_ram_editar,
                equipo_disco_editar:        equipo_disco_editar,
                equipo_procesador_editar:   equipo_procesador_editar,
                id_equipo_edit:             id_equipo_edit,
                equipo_nombre_editar:       equipo_nombre_editar
            },
            function(respuesta) {
                if (respuesta == 1) {
                    toastr.success('Se Actuzlizó correctamente la infmormación', 'Actualización de Información');
                    $("#close_modal_edit").click();
                    
                } else {
                    toastr.error('No ha sido posible Actuzlizar La infmormación', 'Error');
                    
                }
                                            
            }, function(progreso, valor) {
                //Barra de progreso.
                
            });
        }else{
            $.ajax({
                    type: "GET",
                    url:  "actualizardatosequipos",
                    data: { 
                        equipo_marca_editar:equipo_marca_editar,
                        equipo_modelo_editar:equipo_modelo_editar,
                        equipo_serial_editar:equipo_serial_editar,
                        equipo_fechaengrega_editar:equipo_fechaengrega_editar,
                        // equipo_estado_editar:equipo_estado_editar,
                        equipo_ip_editar:equipo_ip_editar,
                        equipo_mac_editar:equipo_mac_editar,
                        equipo_ram_editar:equipo_ram_editar,
                        equipo_disco_editar:equipo_disco_editar,
                        equipo_procesador_editar:equipo_procesador_editar,
                        id_equipo_edit:id_equipo_edit,
                        equipo_nombre_editar: equipo_nombre_editar
                    }
            })
            .done(function(data) {
                if (data == 1) {
                    toastr.success('Se Actuzlizó correctamente la infmormación', 'Actualización de Información');
                    
                    $("#close_modal_edit").click();
                    
                }else{
                    toastr.error('No ha sido posible Actuzlizar La infmormación', 'Error');
                }
            });
        }


    }
    function activar_form(){
        $("#tabla_perifericos").addClass('has-success');
        $("#btn_editar").hide('slow');
        $("#btn_cancelar").show('show');
        $("#btn_enviar").show('show');
        

        @foreach($equipos as $equipo)
        @if($equipo->equipment_types_id != 7 && $equipo->equipment_types_id != 5 && $equipo->equipment_types_id != 2)
            
              
              $("#{{$equipo->type}}_marca").removeAttr('readonly');
              $("#{{$equipo->type}}_modelo").removeAttr('readonly');
              $("#{{$equipo->type}}_serial").removeAttr('readonly');
            
        @endif
        @endforeach 


    }
    function desact_form(){
        $("#tabla_perifericos").removeClass('has-success');
        $("#btn_editar").show('show');
        $("#btn_cancelar").hide('slow');
        $("#btn_enviar").hide('slow');

        @foreach($equipos as $equipo)
        @if($equipo->equipment_types_id != 7 && $equipo->equipment_types_id != 5 && $equipo->equipment_types_id != 2)
            
              
              $("#{{$equipo->type}}_marca").attr('readonly', 'true');
              $("#{{$equipo->type}}_modelo").attr('readonly', 'true');
              $("#{{$equipo->type}}_serial").attr('readonly', 'true');
            
        @endif
        @endforeach
    }
    function enviar_datos_perifericos(){
        var ids = new Array();
        var marcas = new Array();
        var modelos = new Array();
        var seriales = new Array();

        var i = 0;

        @foreach($equipos as $equipo)
        @if($equipo->equipment_types_id != 7 && $equipo->equipment_types_id != 5 && $equipo->equipment_types_id != 2)
            
              ids[i]        = {{$equipo->id_equipo}};
              marcas[i]     = {{$equipo->type}}_marca = $("#{{$equipo->type}}_marca").val();
              modelos[i]    = {{$equipo->type}}_modelo = $("#{{$equipo->type}}_modelo").val();
              seriales[i]   = {{$equipo->type}}_serial = $("#{{$equipo->type}}_serial").val();
            

            i++;
        @endif
        @endforeach

        $.ajax({
                type: "GET",
                url:  "actualizardatosperifericos",
                data: { 
                    ids:ids,
                    marcas:marcas,
                    modelos:modelos,
                    seriales:seriales
                }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se Actuzlizó correctamente la infmormación', 'Actualización de Información');
                
                $("#close_modal_edit").click();
                
            }else{
                toastr.error('No ha sido posible Actuzlizar La infmormación', 'Error');
            }
        });




    }
</script>
