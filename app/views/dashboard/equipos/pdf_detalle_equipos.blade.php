<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Informe</title>
<link href="{{  URL::to("css/pdf_equipos.css")}}" rel="stylesheet" type="text/css"/>
</head>
 
<body style="font-family: Arial; font-size:11px">
    <table class="table" style="border: 2px solid black" width="100%">     
        <tr valign="middle"> 
            <th rowspan="3" style="border: 2px solid black"><img src="assets/img/logotuto.png" width="60px"></th>
            <th align="center" rowspan="3" style="border: 2px solid black">HOJA DE VIDA DE COMPUTADORES</th>
            <th colspan="2" style="border: 2px solid black">CODIGO: GI7-REG001</th>
         </tr>
        <tr align="center" valign="middle"> 
            <th colspan="2" style="border: 2px solid black">FECHA ACTUALIZACION: 23/11/2013</th>
        </tr>
        <tr align="center" valign="middle" > 
            <td style="border: 2px solid black">PAGINA: 1</td>
            <td style="border: 2px solid black">REVISIÓN: 2</td>
        </tr>
    </table>
    <br>
    
    <br>



   
<table border="1" style="background-color:#;border-collapse:collapse;border:1px solid #6E6E6E;color:#000000;width:100%" cellpadding="2" cellspacing="3">
    <tr  class="center">
        <td colspan="6" style="background-color:#6E6E6E; color:white; border:1px solid black;"><strong>CORPORACIÓN EDUCATIVA INDOAMERICANA</strong></td>
    </tr>
    <tr class="center">
        <td colspan="2">PROCESO</td>
        <td colspan="2">CARGO</td>
        <td colspan="1">RESPONSABLE</td>
        <td rowspan="2"><img class="inside-avatar" src="{{$users[0]->img_min}}"></td>
    </tr>
    <tr class="center">
        <td colspan="2">{{$users[0]->acronyms}}</td>
        <td colspan="2">{{$users[0]->profile}}</td>
        <td colspan="">{{$users[0]->name}} {{$users[0]->name2}} {{$users[0]->last_name}} {{$users[0]->last_name2}} </td>
    </tr>
    <tr style="background-color:#6E6E6E; color:white; " class="center">
        <td colspan="6" > .</td>
    </tr>
    <tr class="center" style="font-size:10px">
        <td>N° EQUIPO</td>
        <td>NOMBRE DEL EQUIPO</td>
        <td>DIRECCION MAC DEL EQUIPO</td>
        <td>FECHA ENTREGA DEL EQUIPO</td>
        <td>FECHA ULTIMO MANTENIMIENTO HW</td>
        <td>FECHA ULTIMO MANTENIMIENTO SF</td>
    </tr>
    <tr class="center" style="font-size:12px">
        <td>{{$data[0]['num_equipo']}}</td>
        <td>{{$data[0]['nomb_equipo']}}</td>
        <td>{{$data[0]['mac']}}</td>
        <td>{{$data[0]['date_asign']}}</td>
        <td>{{$data_last_m[0]['ultimo']}}</td>
        <td></td>
    </tr>
    <tr  class="center">
        <td colspan="6" style="background-color:#6E6E6E; color:white; border:1px solid black;"><strong>CONFIGURACIÓN DEL DISPOSITIVO O PARTE</strong></td>
    </tr>
    <tr  class="center">
        <td colspan="6" style="background-color:#D8D8D8; color:black; border:1px solid black;"><strong>DESCRIPCION GENERAL</strong></td>
    </tr>
    <tr class="center"> 
        <td rowspan="3" style="border: 2px solid black">CPU</td>
        <td align=""  style="border: 2px solid black">Procesador</td>
        <td colspan="4" style="border: 2px solid black">{{$data[0]['procesador']}}</td>
    </tr>
    <tr class="center"> 
        <td colspan="">Memoria RAM</td>
        <td colspan="4">{{$data[0]['ram']}}</td>
        <!-- <td colspan="" style="border: 2px solid black">CODIGO: GI7-REG001</td> -->
    </tr>
    <tr class="center"> 
        <td colspan="">Disco Duro</td>
        <td colspan="4">{{$data[0]['disco_duro']}}</td>
        <!-- <td colspan="" style="border: 2px solid black">CODIGO: GI7-REG001</td> -->
    </tr>
    <tr class="center" style="background-color:#D8D8D8; color:black; border:1px solid black;">
        <td colspan="2">ELEMENTO</td>
        <td>MARCA</td>
        <td>MODELO</td>
        <td colspan="2">SERIAL</td>
    </tr>
    <?php $i = 0; ?>
    <?php $num = count($data_per); ?>
    @if($num > 0)
        @foreach($data_per as $perifericos)
        
            @if($i == 0)
            <tr class="center"> 
                <td rowspan="{{$num}}" style="border: 2px solid black">Perifericos</td>
            @else
            <tr class="center"> 
            @endif
                <td align=""  style="border: 2px solid black">{{$perifericos->EquipmentTypes->type}}</td>
                <td colspan="" style="border: 2px solid black">@if($perifericos->mark == "") N/A @else  {{$perifericos->mark}} @endif</td>
                <td colspan="" style="border: 2px solid black">@if($perifericos->model == "") N/A @else  {{$perifericos->model}} @endif</td>
                <td colspan="2" style="border: 2px solid black">@if($perifericos->serial == "") N/A @else {{$perifericos->serial}} @endif</td>
            </tr>
            <!-- <td colspan="6" style="background-color:#D8D8D8; color:black; border:1px solid black;"><strong>{{$perifericos->EquipmentTypes->type}}</strong></td> -->
        <?php $i++; ?>
        @endforeach
    @else
        <tr class="center">
            <td align=""  style="border: 2px solid black">PERIFERICOS</td>
            <td align=""  style="border: 2px solid black">N/A</td>
            <td colspan="" style="border: 2px solid black">N/A</td>
            <td colspan="" style="border: 2px solid black">N/A</td>
            <td colspan="2" style="border: 2px solid black">N/A</td>
        </tr>
    @endif
    <tr  class="center">
        <td colspan="6" style="background-color:#6E6E6E; color:white; border:1px solid black;"><strong>INSTALACIÓN DE SOFTWARE</strong></td>
    </tr>
    <tr class="center" style="background-color:#D8D8D8; color:black; border:1px solid black;">
        <td colspan="2">SOFTWARE</td>
        <td>VERSION</td>
        <td colspan="2">TIPO/ID LICENCIA</td>
        <td colspan="">FOTO EQUIPO</td>
    </tr>
    <tr class="center"> 
        <td colspan="">SISTEMA OPERATIVO</td>
        <td colspan="" style="border: 2px solid black">{{$data[0]['sitema_o']}}</td>
        <td colspan="" style="border: 2px solid black">{{$data[0]['version_op']}}</td>
        <td colspan="2" style="border: 2px solid black">{{$data[0]['license_op']}}</td>
        <td colspan="1" rowspan="4" style="border: 2px solid black"><img src="{{$data[0]['img_equipo']}}" width="100"></td>
        <!-- <td colspan="" style="border: 2px solid black">CODIGO: GI7-REG001</td> -->
    </tr>
    
    <tr class="center"> 
        <td colspan="" rowspan="3">SOFTWARE</td>
        <td colspan="" rowspan="">Office</td>
        <td colspan="" style="border: 2px solid black"></td>
        <td colspan="2" style="border: 2px solid black"></td>
    </tr>
    <tr class="center"> 
        <td colspan="" >Antivirus</td>
        <td colspan="" rowspan=""></td>
        <td colspan="2" rowspan=""></td>
        
    </tr>
    <tr class="center"> 
        <td colspan="" >Spark</td>
        <td colspan="" rowspan=""></td>
        <td colspan="2" rowspan=""></td>
        
    </tr>
    <tr  class="center">
        <td colspan="6" style="background-color:#D8D8D8; color:black; border:2px solid black;"><strong>OBSERVACIONES</strong></td>
    </tr>
    <tr class="center"> 
        <td colspan="6" rowspan="" style="background-color:#fff; color:white; border:2px solid black;">.</td>
    </tr>
    
</table>


<table border="1" style="background-color:#;border-collapse:collapse;border:1px solid #6E6E6E;color:#000000;width:100%" cellpadding="2" cellspacing="3">
    
    <tr  class="center">
        <td colspan="6" style="background-color:#6E6E6E; color:white; border:1px solid black;"><strong>MANTENIMIENTOS</strong></td>
        
    </tr>
    <tr class="center" style="background-color:#D8D8D8; color:black; border:1px solid black;">
        <td>ID</td>
        <td>TÉCNICO</td>
        <td>TIPO MANTENIMIENTO</td>
        <td>OBSERVACIONES</td>
        <td colspan="2">FECHA</td>
    </tr>
                               
          
    @foreach ($mantenimientos as $mant)
    <tr class="center">
        <td>{{'MANT' . str_pad($mant->id, 6, "0", STR_PAD_LEFT)}}</td> 
        <td>
            @if($mant->users_id == "")
            Sin Usuario
            @else
            {{$mant->Users->name}} {{$mant->Users->last_name}}
            @endif
        </td> 
        <td>{{$mant->EquipmentTypeMaintenances->type_maintenance}}</td> 
        <td>{{$mant->detalle}}</td> 
        <td colspan="2">{{$mant->created_at}}</td> 
                    
    </tr>
    @endforeach
           
       
        
    
    

</table>

<br>
<table class="table" style="border: 2px solid black; font-size: 12px" width="100%">     
    <tr valign="middle"> 
        <th rowspan="" style="border: 2px solid black">RESPONSABLE DEL EQUIPO</th>
        <th align="center"  style="border: 2px solid black">CARGO</th>
        <th align="center"  style="border: 2px solid black">FIRMA</th>
        <th rowspan="3" style="color:#fff">.</th>
        <th rowspan="" style="border: 2px solid black">FUNCIONARIO QUE ENTREGA</th>
        <th align="center"  style="border: 2px solid black">CARGO</th>
        <th align="center"  style="border: 2px solid black">FIRMA</th>
        
     </tr>
    <tr align="center" valign="middle"> 
        <th colspan="" style="border: 2px solid black">{{$users[0]->name}} {{$users[0]->last_name}}</th>
        <th colspan="" style="border: 2px solid black"></th>
        <th colspan="" style="border: 2px solid black"></th>

        <th colspan="" style="border: 2px solid black">{{Auth::user()->name}} {{Auth::user()->last_name}}</th>
        <th colspan="" style="border: 2px solid black"></th>
        <th colspan="" style="border: 2px solid black"></th>
    </tr>
    
</table>



</body>
 
</html>





