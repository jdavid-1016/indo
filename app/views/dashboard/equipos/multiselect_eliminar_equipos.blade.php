<div style="" align="center">
    <link href="{{  URL::to("css/multi-select.css")}}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{  URL::to("css/theme.css")}}" media="screen" rel="stylesheet" type="text/css" />
    <link href="{{  URL::to("css/application.css")}}" media="screen" rel="stylesheet" type="text/css" />

        <label>Seleccione los Equipos que desea eliminar</label>

        <select multiple="multiple" id="callbacks6" name="my-select[]">
        
            <optgroup label="Equipos">
            @foreach($datos as $dato)
                <option value="{{$dato->id}}">{{$dato->id}} {{$dato->mark}} @if($dato->name != "") {{$dato->name}} {{$dato->last_name}} @else Sin asignar @endif</option>
                      
            @endforeach
            </optgroup>
                            
       </select>
    <script src="{{ URL::to("js/jquery.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/jquery-1.10.2.min.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/jquery.tinysort.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/jquery.quicksearch.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/jquery.multi-select.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/rainbow.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/application.js")}}" type="text/javascript"></script>
    <script src="{{ URL::to("js/handlebars.js")}}" type="text/javascript"></script>
</div>