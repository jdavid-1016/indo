<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">
 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Informe</title>
<!-- <link href="{{  URL::to("css/pdf_equipos.css")}}" rel="stylesheet" type="text/css"/> -->
<link href="{{  URL::to("css/pdf.css")}}" rel="stylesheet" type="text/css"/>
</head>
 
<body>
    <table class="table" style="border: 2px solid black" width="100%">     
        <tr valign="middle"> 
            <th rowspan="3" style="border: 2px solid black"><img src="assets/img/logotuto.png" width="60px"></th>
            <th align="center" rowspan="3" style="border: 2px solid black">INFORME DE INVENTARIO DE EQUIPOS</th>
            <th colspan="2" style="border: 2px solid black">CODIGO: GI7-REG001</th>
         </tr>
        <tr align="center" valign="middle"> 
            <th colspan="2" style="border: 2px solid black">FECHA ACTUALIZACION: 23/11/2013</th>
        </tr>
        <tr align="center" valign="middle" > 
            <td style="border: 2px solid black">PAGINA: 1</td>
            <td style="border: 2px solid black">REVISIÓN: 2</td>
        </tr>
    </table>
    <br>
    
    <br>


<table class="table" width="100%">
    <thead>
        <tr class="cabecera">
            <th>ID</th>
            <th>TIPO</th>
            <th>NOMBRE</th>
            <th>RESPONSABLE</th>
            <th>UBICACIÓN</th>
            <th>VERIFICACION</th>
        </tr>
    </thead>
    <tbody class="center">                         
  
        @foreach ($data as $row)
        <tr valign="middle">
            <td style="border: 1px solid black">{{$row['id_equipo']}}</td>
            <td style="border: 1px solid black">{{$row['tipo']}}</td>
            <td style="border: 1px solid black">{{$row['observation']}}</td>
            <td style="border: 1px solid black">{{$row['user']}}</td>
            <td style="border: 1px solid black">{{$row['ubicacion']}}</td>
            <td style="border: 1px solid black"></td>
            
        </tr>
        @endforeach
    </tbody>
</table>
<div class="pagination">
  {{$pag->appends(array("code" => Input::get('code'),"applicant" => Input::get('applicant'),"marca" => Input::get('marca'),"f_compra" => Input::get('f_compra'),"tipo_equipo" => Input::get('tipo_equipo')))->links()}}
</div>

