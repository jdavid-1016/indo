<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="es" xml:lang="es">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Informe</title>
<link href="{{  URL::to("css/pdf.css")}}" rel="stylesheet" type="text/css"/>
</head>
 
<body>
    <table class="table" style="border: 2px solid black" width="100%">     
  <tr valign="middle"> 
      <th rowspan="3" style="border: 2px solid black"><img src="assets/img/logotuto.png" width="60px"></th>
    <th align="center" rowspan="3" style="border: 2px solid black">INFORME DE ASIGNACION DE LICENSIAS</th>
    <th colspan="2" style="border: 2px solid black">CODIGO: 256156</th>
  </tr>
  <tr align="center" valign="middle"> 
      <th colspan="2" style="border: 2px solid black">FECHA ACTUALIZACION: 19/03/2015</th>
    
  </tr>
  <tr align="center" valign="middle" > 
    <td style="border: 2px solid black">PAGINA: 1</td>
    <td style="border: 2px solid black">REVISION: 1</td>
    
  </tr>

</table>
    <br>
    <h2 class="center">Informe de asignacion de licencias</h2>
    <br>
    <table align="center" class="table table-bordered" width="100%">
    <tr class="cabecera">
    <th scope="col">Id</th>
    <th scope="col">Codigo</th>
    <th scope="col">Proveedor</th>
    <th scope="col">Producto</th>
    <th colspan="2">Asignaciones</th>
    <th scope="col">Disponibilidad</th>
    
  </tr>
 @foreach($datos as $dat)
 <?php 
 $cont=0;
 $cont1=0;
 $nombre = "";
 $equipo = "";
 ?>
 @foreach($asignaciones as $asignacion)
        @if($asignacion->equipment_licensings_id == $dat->id)
        <?php
        if($cont==0){
            $nombre = $asignacion->name." ".$asignacion->last_name;
            $equipo = $asignacion->observation;
        }
        $cont=$cont+1;
        ?>
        
        @endif
 @endforeach
        
  <tr  align="center">
    <td rowspan="{{ $cont }}" >{{ $dat->id }}</td>
    <td rowspan="{{ $cont }}" >{{ $dat->id_licensing }}</td>
    <td rowspan="{{ $cont }}" >{{ $dat->provider }}</td>
    <td rowspan="{{ $cont }}" >{{ $dat->version }}</td>
    <td style="font-size: 11px">{{ $nombre }}</td>
    <td style="font-size: 11px">{{ $equipo }}</td>
    
    <td rowspan="{{ $cont }}" >{{ $dat->available }} / {{ $dat->quantity }}</td>
  </tr>
 
 @foreach($asignaciones as $asignacion)
        @if($asignacion->equipment_licensings_id == $dat->id)
        @if($cont1>0)
        <tr align="center">
           <td style="font-size: 11px">{{ $asignacion->name }} {{ $asignacion->last_name }}</td>
           <td style="font-size: 11px">{{ $asignacion->observation }}</td>
        </tr>
        @endif
        <?php $cont1=$cont1+1; ?>
        @endif
   @endforeach
 @endforeach
</table>
 
</body>
 
</html>