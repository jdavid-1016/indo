    <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <i class="icon-reorder"></i> Ver Equipos
               </div>
               <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  
                  <li class="active"><a href="verequipos" >Ver Equipos</a></li>
                  <li class=""><a href="inactivos" >Sin Asignar</a></li>
                  <li class=""><a href="debaja" >De Baja</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
                  <div class="table-responsive">
                    <div>
                       <form class="form-inline" action="verequipos" method="get">
                          <div class="search-region">
                             <div class="form-group">
                                <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}" onkeyup="tabla_lista_equipos()">
                             </div>
                             <div class="form-group">
                                <select class="form-control input-medium select2me" name="applicant" id="applicant" onchange="tabla_lista_equipos()">
                                <option value="">Usuario</option> 
                                 @foreach($applicants as $applicant)
                                 @if($applicant->id == Input::get('applicant'))
                                    <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                                 @else
                                    <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                                 @endif  
                                 @endforeach
                                </select>
                             </div>
                             <div class="form-group">
                                <select class="form-control input-medium select2me" name="marca" id="marca" onchange="tabla_lista_equipos()">
                                <option value="">Marca</option> 
                                 @foreach($marcas as $marca)
                                 @if($marca->mark == Input::get('marca'))
                                    <option value="{{$marca->mark}}" selected="">{{$marca->mark}}</option>
                                 @else
                                    <option value="{{$marca->mark}}">{{$marca->mark}}</option>
                                 @endif  
                                 @endforeach
                                </select>
                             </div>
                             <div class="form-group">
                                <select class="form-control input-medium select2me" name="tipo_equipo" id="tipo_equipo" onchange="tabla_lista_equipos()">
                                <option value="">Tipo</option> 
                                 @foreach($tipo_equipos as $tipo_equipo)
                                 @if($tipo_equipo->id == Input::get('tipo_equipo'))
                                    <option value="{{$tipo_equipo->id}}" selected="">{{$tipo_equipo->type}}</option>
                                 @else
                                    <option value="{{$tipo_equipo->id}}">{{$tipo_equipo->type}}</option>
                                 @endif  
                                 @endforeach
                                </select>
                             </div>
                             <div class="form-group">
                                <select class="form-control input-medium select2me" name="grupo" id="grupo" onchange="tabla_lista_equipos()">
                                    <option value="">Grupo</option>
                                    @foreach($grupos as $grupo)
                                    @if($grupo->id == Input::get('grupo'))
                                       <option value="{{$grupo->id}}" selected="">{{$grupo->name_group}}</option>
                                    @else
                                       <option value="{{$grupo->id}}">{{$grupo->name_group}}</option>
                                    @endif 
                                    @endforeach
                                </select>
                             </div> 
                             <div class="form-group">
                                <input type="text" class="form-control" id="nombre_e" placeholder="Nombre Equipo" onkeyup="tabla_lista_equipos()">
                             </div>
                             <div class="form-group">
                                <input type="text" class="form-control" id="serial" name="serial" placeholder="Serial" value="{{Input::get('serial')}}">
                             </div>
                                 
                          </div>
                       </form> 
                    </div>
                    <div id="tabla_lista_equipos">
                        @include('dashboard.equipos.tabla_lista_equipos')
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                       
                        <!-- /.modal-dialog -->
            </div>
<script>
    function detalle(id, id_equipo){
        
        $.post('calluserprofile', {
            id:id, id_equipo:id_equipo
        } ,

        function(response){
            $('#basic').html(response);
            //var datos = JSON.parse(response);
        });
    }
    function tabla_lista_equipos(id,order_tipo,nombre){


      var code = $("#code").val();
      var applicant = $("#applicant").val();
      var marca = $("#marca").val();
      var f_compra = $("#f_compra").val();
      var serial = $("#serial").val();
      var grupo = $("#grupo").val();
      var tipo_equipo = $("#tipo_equipo").val();
      var nombre_e = $("#nombre_e").val();
      var order = order;



        $.get('verequipos', {
            id:id,
            code: code,
            applicant: applicant,
            marca: marca,
            f_compra: f_compra,
            serial: serial,
            grupo: grupo,
            tipo_equipo:tipo_equipo,
            order_tipo:order_tipo,
            order_nombre:nombre,
            nombre_e:nombre_e,
            nombre: 1
        } ,

        function(response){
            $('#tabla_lista_equipos').html(response);
            //var datos = JSON.parse(response);
        });
    }
</script>