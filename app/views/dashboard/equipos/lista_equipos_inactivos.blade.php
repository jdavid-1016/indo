    <div class="page-content">
      <div class="row">
         <div class="col-md-12">
            <div class="portlet">
               <div class="portlet-title">
                  <i class="icon-reorder"></i> Ver Equipos
               </div>
               <div class="portlet-body">
               <ul  class="nav nav-tabs">
                  
                  <li class=""><a href="verequipos" >Ver Equipos</a></li>
                  <li class="active"><a href="inactivos" >Sin Asignar</a></li>
                  <li class=""><a href="debaja" >De Baja</a></li>
                  <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                  
               </ul>
                  <div class="table-responsive">
                      <div>
                         <form class="form-inline" action="inactivos" method="get">
                            <div class="search-region">
                               <div class="form-group">
                                  <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                               </div>
                               
                               <div class="form-group">
                                  <select class="form-control input-medium" name="marca">
                                  <option value="">Marca</option> 
                                   @foreach($marcas as $marca)
                                   @if($marca->mark == Input::get('marca'))
                                      <option value="{{$marca->mark}}" selected="">{{$marca->mark}}</option>
                                   @else
                                      <option value="{{$marca->mark}}">{{$marca->mark}}</option>
                                   @endif  
                                   @endforeach
                                  </select>
                               </div>
                               <div class="form-group">
                                  <input type="text" class="form-control fecha_required" id="f_compra" name="f_compra" placeholder="Fecha Compra">
                               </div>
                               
                               <div class="form-group">
                                  <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                               </div>    
                            </div>
                         </form> 
                      </div>
                     <table class="table table-striped table-hover top-blue">
                        <thead>
                        <tr>
                              <th>Imagen</th>
                              <th>Tipo Equipo <a href="/indoamericana/intranet/listaEmpleados/last_name/asc"><i class="icon-sort"></i></a></th>
                              <th>F. Compra <a href="/indoamericana/intranet/listaEmpleados/last_name/asc"><i class="icon-sort"></i></a></th>
                              <th>Marca</th>
                              <th>Modelo <a href="/indoamericana/intranet/listaEmpleados/id_process/asc"><i class="icon-sort"></i></a> </th>
                              <th>Serial</th>
                              <th>ip</th>
                              
                           </tr>
                        </thead>
                        <tbody class="center">                         
                        @foreach ($administradores as $row)
                           <tr>
                           <td>                               
                           <a href="#basic" data-toggle="modal" class="btn btn-default" onclick="detalle_equipos_inactivos({{$row->id}})">
                            @if($row->image == "")
                              
                            <img src="../assets/img/equipos/-1.png" width="45"  height="40" >
                            @else
                              <img src="../{{$row->image}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                            @endif
                           </a>   
                           </td>
                           <td>{{$row->EquipmentTypes->type}}</td>
                           <td>{{$row->date_shopping}}</td>
                           <td>{{$row->mark}}</td>
                           <td>{{$row->model}}</td>
                           <td>{{$row->serial}}</td>
                           <td>{{$row->ip}}</td>
                           
                           
                           </tr>
                        @endforeach
                           
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
     
   </div>
</div>

            <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                       
                        <!-- /.modal-dialog -->
            </div>
<script>
    function detalle_equipos_inactivos(id){
        
        $.post('calluserprofile', {
            id:id
        } ,
        function(response){
            $('#basic').html(response);
            
        });
    }
</script>