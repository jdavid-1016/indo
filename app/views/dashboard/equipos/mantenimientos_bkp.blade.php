        <div class="page-content">
          <div class="row">
             <div class="col-md-12">
                <div class="portlet">
                   <div class="portlet-title">
                      <i class="icon-reorder"></i> Ingresar Mantenimientos
                   </div>
                   <div class="portlet-body">
                   <ul  class="nav nav-tabs">
                      
                      <li class="active"><a href="mantenimientos" >Ingresar Mantenimientos</a></li>
                      <li class=""><a href="vermantenimientos" >Ver Mantenimientos</a></li>
                      
                      <!-- <li class=""><a href="#tab_1_3" data-toggle="tab">Campos</a></li> -->
                      
                   </ul>
                      <div class="table-responsive">
                      <div>
                         <form class="form-inline" action="mantenimientos" method="get">
                            <div class="search-region">
                               <div class="form-group">
                                  <input type="text" class="form-control" id="code" name="code" placeholder="Código" value="{{Input::get('code')}}">
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-medium" name="applicant">
                                  <option value="">Usuario</option> 
                                   @foreach($applicants as $applicant)
                                   @if($applicant->id == Input::get('applicant'))
                                      <option value="{{$applicant->id}}" selected="">{{$applicant->name}} {{$applicant->last_name}}</option>
                                   @else
                                      <option value="{{$applicant->id}}">{{$applicant->name}} {{$applicant->last_name}}</option>
                                   @endif  
                                   @endforeach
                                  </select>
                               </div>
                               <div class="form-group">
                                  <select class="form-control input-medium" name="marca">
                                  <option value="">Marca</option> 
                                   @foreach($marcas as $marca)
                                   @if($marca->mark == Input::get('marca'))
                                      <option value="{{$marca->mark}}" selected="">{{$marca->mark}}</option>
                                   @else
                                      <option value="{{$marca->mark}}">{{$marca->mark}}</option>
                                   @endif  
                                   @endforeach
                                  </select>
                               </div>
                               <div class="form-group">
                                  <input type="text" class="form-control fecha_required" id="f_compra" name="f_compra" placeholder="Fecha Compra">
                               </div>
                               <div class="form-group">
                                  <input type="text" class="form-control" id="serial" name="serial" placeholder="Serial" value="{{Input::get('serial')}}">
                               </div>
                               <div class="form-group">
                                  <button type="submit" class="btn btn-primary form-control active" value="Buscar"><i class="icon-search"></i></button>
                               </div>    
                            </div>
                         </form> 
                      </div>
                         <table class="table table-striped table-hover top-blue">
                            <thead>
                            <tr>
                                  <th>Imagen</th>
                                  <th>Usuario <a href="/indoamericana/intranet/listaEmpleados/last_name/asc"><i class="icon-sort"></i></a></th>
                                  <th>tipo</th>
                                  <th>Marca</th>
                                  <th>Modelo <a href="/indoamericana/intranet/listaEmpleados/id_process/asc"><i class="icon-sort"></i></a> </th>
                                  <th>Serial</th>
                                  <th>ip</th>
                                  
                               </tr>
                            </thead>
                            <tbody class="center">                         
                            @foreach ($administradores as $row)
                               <tr>
                               <td>                               
                               <a href="#basic" data-toggle="modal" class="btn btn-default" onclick="detalle({{$row->id}},{{$row->id_user}})">
                                @if($row->image == "")
                                  
                                <img src="../assets/img/equipos/-1.png" width="45"  height="40" >
                                @else
                                  <img src="../{{$row->image}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;">
                                @endif
                               </a>   
                               </td>
                               <td>{{$row->last_name.' '.$row->last_name2.' '.$row->name.' '.$row->name2}}</td>
                               <td>{{$row->type}}</td>
                               <td>{{$row->mark}}</td>
                               <td>{{$row->model}}</td>
                               <td>{{$row->serial}}</td>
                               <td>{{$row->ip}}</td>
                               
                               </tr>
                            @endforeach
                               
                            </tbody>
                         </table>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </div>

                <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
                           
                            <!-- /.modal-dialog -->
                </div>
    <script>
        function detalle(id, id_user){
            
            $.post('frommantenimientos', {
                id:id, id_user:id_user
            } ,

            function(response){
                $('#basic').html(response);
                //var datos = JSON.parse(response);
            });
        }
    </script>