
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="{{ URL::to("assets/calendario/daterangepicker-bs3.css")}}" />
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js"></script>
<script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ URL::to("assets/calendario/moment.js")}}"></script>
<script type="text/javascript" src="{{ URL::to("assets/calendario/daterangepicker.js")}}"></script>

<style type="text/css">
            ${demo.css}
        </style>
<div class="page-content">
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1_2">
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Estadisticas
                    </div>
                </div>
                  
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class="active"><a href="#">Estadisticas</a></li>
                        <li class="pull-right">
                            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span></span> <b class="caret"></b>
                            </div>
                        </li>

                    </ul>
                    <div id="graficos">
                        <form>
                            <div class="form-body" id="area_de_graficos">
                                <div class="row">
                                    <div class="form-group has-success">
                                        
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                    <div id="container1" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="container2" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group has-success">
                                         
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                     <div id="container3" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="container4" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group has-success">
                                         
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <div class="col-md-6">
                                                     <div id="container5" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div id="container6" style="min-width: 300px; height: 400px; margin: 0 auto"></div>
                                                 </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
            </div>
        </div>
    </div> 
</div>

<div id="graficos_recargar">
                                    
</div>  

<script type="text/javascript">
    function grafico1(arreglo1) {
        Highcharts.setOptions({
            colors: ['#058DC7']
        });

        $('#container1').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Tipos de Equipos'
            },
            subtitle: {
                text: 'Código: <a href="http://192.168.0.18/indo/public/soporte/verequipos">Indoamericana</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            series: [{
                name: 'Population',
                data: arreglo1,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    }
</script>
<script type="text/javascript">
    function grafico2(arreglo2) {
        Highcharts.setOptions({
            colors: ['#64E572']
        });

        $('#container2').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Estados de los Equipos'
            },
            subtitle: {
                text: 'Código: <a href="http://192.168.0.18/indo/public/soporte/verequipos">Indoamericana</a>'
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Cantidad'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '<b>{point.y}</b>'
            },
            series: [{
                name: 'Population',
                data: arreglo2,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    format: '{point.y}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    }
</script>

<script type="text/javascript">
    function grafico4(arreglo4) {
        Highcharts.setOptions({
            colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
        });
        $('#container3').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Equipos por Proceso'
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        format: '<b>{point.name}</b>:{point.y}', // one decimal
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'N°',
                data: arreglo4
            }]
        });
    }
</script>
<script type="text/javascript">
    function grafico5(arreglo5) {
        Highcharts.setOptions({
            colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
        });
        $('#container4').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Impresoras por Proceso'
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        format: '<b>{point.name}</b>:{point.y}', // one decimal
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'N°',
                data: arreglo5
            }]
        });
    }
</script>
<script type="text/javascript">
    function grafico3(arreglo3) {
        Highcharts.setOptions({
            colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
        });
        $('#container5').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Equipos en Grupos'
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        format: '<b>{point.name}</b>:{point.y}', // one decimal
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'N°',
                data: arreglo3
            }]
        });
    }
</script>
<script type="text/javascript">
    function grafico6(arreglo6) {
        Highcharts.setOptions({
            colors: ['#058DC7', '#50B432', '#ED561B', '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
        });
        $('#container6').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: {
                text: 'Licencias por Producto'
            },
            tooltip: {
                // pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                pointFormat: '<b>{point.y}</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        format: '<b>{point.name}</b>:{point.y}', // one decimal
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'N°',
                data: arreglo6
            }]
        });
    }
</script>
<script type="text/javascript">
    
$(document).ready(function () {
    var arreglo1 = new Array();
    var arreglo2 = new Array();
    var arreglo3 = new Array();
    var arreglo4 = new Array();
    var arreglo5 = new Array();
    var arreglo6 = new Array();


    @foreach($data_tipos as $data_tipo)
        arreglo1.push(new Array('{{$data_tipo->type}}', {{$data_tipo->dato}}));
    @endforeach

    @foreach($data_estados as $data_estados)
        arreglo2.push(new Array('{{$data_estados->status}}', {{$data_estados->dato}}));
    @endforeach
    
    @foreach($data_grupos as $data_grupos)
        arreglo3.push(new Array('{{$data_grupos->name_group}}', {{$data_grupos->dato}}));
    @endforeach

    @foreach($data_procesos as $data_procesos)
        arreglo4.push(new Array('{{$data_procesos->acronyms}}', {{$data_procesos->dato}}));
    @endforeach

    @foreach($data_impresoras as $data_impresoras)
        arreglo5.push(new Array('{{$data_impresoras->acronyms}}', {{$data_impresoras->dato}}));
    @endforeach

    @foreach($data_licencias as $data_licencias)
        arreglo6.push(new Array('{{$data_licencias->product}}', {{$data_licencias->dato}}));
    @endforeach
    
    grafico1(arreglo1);
    grafico2(arreglo2);
    grafico3(arreglo3);
    grafico4(arreglo4);
    grafico5(arreglo5);
    grafico6(arreglo6);
});  

</script>
<script type="text/javascript">
    $(document).ready(function () {

        var cb = function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
            var fecha_inicio = start.format('YYYY-M-D');
            var fecha_fin = end.format('YYYY-M-D');                                

            $.ajax({
                type: "GET",
                url: "estadisticas",
                data: {fecha_inicio: fecha_inicio, fecha_fin: fecha_fin},
            })
            .done(function (data) {
                $("#graficos_recargar").html(data);
            });

            // document.getElementById("grafica").src = "changedateseguimientos?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica2").src = "changedateprogramas?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica3").src = "changedatecampanas?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica4").src = "changedatefuentes?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica5").src = "changedateestados?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica6").src = "changedatejornadas?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica7").src = "changedategeneros?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica8").src = "changedateciudad?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";
            // document.getElementById("grafica9").src = "changedatelocalidad?fecha_inicio=" + fecha_inicio + "&fecha_fin=" + fecha_fin + "";

            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            //alert("Callback has fired: [" + start.format('MMMM D, YYYY') + " to " + end.format('MMMM D, YYYY') + ", label = " + label + "]");
        }

        var optionSet1 = {
            startDate: moment().subtract(29, 'days'),
            endDate: moment(),
            minDate: '01/01/2012',
            maxDate: '12/31/2015',
            dateLimit: {days: 60},
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Mes Anterior': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'left',
            buttonClasses: ['btn btn-default'],
            applyClass: 'btn-small btn-primary',
            cancelClass: 'btn-small',
            format: 'MM/DD/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Aplicar',
                cancelLabel: 'Clear',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Personalizada',
                daysOfWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                firstDay: 1
            }
        };

        var optionSet2 = {
            startDate: moment().subtract(7, 'days'),
            endDate: moment(),
            opens: 'left',
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        };

        $('#reportrange span').html(moment().subtract(29, 'days').format('MMMM D, YYYY') + ' - ' + moment().format('MMMM D, YYYY'));

        $('#reportrange').daterangepicker(optionSet1, cb);

        $('#reportrange').on('show.daterangepicker', function () {
            console.log("show event fired");
        });
        $('#reportrange').on('hide.daterangepicker', function () {
            console.log("hide event fired");
        });
        $('#reportrange').on('apply.daterangepicker', function (ev, picker) {
            console.log("apply event fired, start/end dates are "
                    + picker.startDate.format('MMMM D, YYYY')
                    + " to "
                    + picker.endDate.format('MMMM D, YYYY')
                    );
        });
        $('#reportrange').on('cancel.daterangepicker', function (ev, picker) {
            console.log("cancel event fired");
        });

        $('#options1').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet1, cb);
        });

        $('#options2').click(function () {
            $('#reportrange').data('daterangepicker').setOptions(optionSet2, cb);
        });

        $('#destroy').click(function () {
            $('#reportrange').data('daterangepicker').remove();
        });

    });
</script>