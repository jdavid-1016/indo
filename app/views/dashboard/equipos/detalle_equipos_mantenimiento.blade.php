<div class="modal-dialog  modal-wide" ng-controller="help_deskCtrl" ng-app>
   <div class="modal-content">
   <form class="form" action="#" ng-submit="adminSupport()">
   
      <input type="hidden" ng-model="support_id" value="" id="support_id">
      <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
         <h4 class="modal-title">Mantenimientos de Equipos</h4>
      </div>
      <div class="modal-body form-body">
         

                <div class="portlet">
                   <div class="portlet-title">
                     <div class="caption">Información General</div>
                     <div class="tools">
                       <a href="javascript:;" class="expand"></a>
                     </div>
                   </div>
                   <div class="portlet-body display-hide">
                     <div class="row">
                       <div class="col-md-6">
                         <input type="hidden" value="1" name="tipo-equipo">
                         <table class="table middle well">
                           <tbody>
                             <tr>
                               <td>Funcionario</td>
                               <td>
                                 <span class="tag">
                                    <select name="usuario" id="" class="form-control input-sm clear-border" readonly="">
                                        <option>{{$users[0]->name}} {{$users[0]->name2}} {{$users[0]->last_name}} {{$users[0]->last_name2}}</option>
                                    </select>
                                 </span>
                                 <span class="test-img"><img class="inside-avatar" src="../{{$users[0]->img_min}}"></span>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>
                             <tr>
                                <td>Tipo de Equipo</td>
                                <td>
                                 {{$equipo->EquipmentTypes->type}}
                                </td>
                             </tr>
                             <tr>
                               <td>Imagen</td>
                               <td>
                                 <div class="fileupload fileupload-new" data-provides="fileupload">
                                   <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                        @if($equipo->image == "")
                                            <img src="../assets/img/equipos/-1.png" alt="">
                                        @else
                                            <img src="../{{$equipo->image}}" alt="">
                                        @endif
                                   </div>
                                   <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                   <div>
                                       <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="icon-trash"></i> Eliminar</a>
                                   </div>
                                 </div>
                               </td>
                             </tr>
                           </tbody>
                         </table>
                        @endif
                        @endforeach
                        </div>
                        <div class="col-md-6">
                        @foreach($equipos as $equipo)
                        @if($equipo->equipment_types_id == 7 || $equipo->equipment_types_id == 5 || $equipo->equipment_types_id == 2)
                         <table class="table middle well">
                           <tbody>  
                             <tr>
                               <td>Marca</td>
                               <td><input name="marca" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->mark}}"></td>
                             </tr>
                             <tr>
                               <td>Modelo</td>
                               <td><input name="modelo" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->model}}"></td>
                             </tr>
                             <tr>
                               <td>Serial</td>
                               <td><input name="serial" class="tag form-control clear-border input-medium" type="text" readonly="" value="{{$equipo->serial}}"></td>
                             </tr>
                             <tr>
                               <td>Entrega del Equipo</td>
                               <td><input name="entrega" class="tag form-control input-medium clear-border"  readonly="" value="{{$equipo->date_asign}}"></td>
                             </tr>
                             <tr>
                               <td>Estado</td>
                               <td><input name="estado" class="tag form-control clear-border input-medium" type="text"readonly=""  value="{{$equipo->EquipmentStatuses->status}}"></td>
                             </tr>
                           </tbody>
                         </table>
                         @endif
                         @endforeach
                       </div>
                     </div>
                   </div>
                </div>
                
                <div class="portlet">
                    <div class="portlet-title">
                        <div class="caption">Mantenimiento </div>

                        <div class="tools">
                            <a href="javascript:;" class="expand"></a>
                        </div>
                    </div>
                    <div class="portlet-body ">

                        @if(count($mantenimientos) != 0)
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <strong>Atención! </strong> Este elemento tiene un mantenimiento pendiente.
                                </div>
                                <table class="table  top-blue">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>FECHA</th>
                                            <th>DETALLE</th>
                                            <th>TIPO MANT</th>
                                            <th>ESTADO</th>
                                        </tr>
                                    </thead>
                                    <tbody class="center">                         
                                  
                                        @foreach ($mantenimientos as $mant)
                                        <tr >
                                            <td>{{'MANT' . str_pad($mant->id, 6, "0", STR_PAD_LEFT)}}</td> 
                                            <td>{{$mant->created_at}}</td> 
                                            <td>{{$mant->detalle}}</td> 
                                            <td>{{$mant->EquipmentTypeMaintenances->type_maintenance}}</td> 
                                            <td>{{$mant->EquipmentMaintenancesStatuses->status}}</td> 
                                            
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <div class="note note-info">
                            <h4 class="block"> Seleccione los Items del mantenimiento 
                            <span class="label label-sm label-success ">{{$mantenimientos[0]->EquipmentTypeMaintenances->type_maintenance}}</span></h4>
                            <div class="row">
                                <div class="col-md-4">
                                    @foreach($items as $tipo)
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <input type="checkbox" class="toggle" id="item_{{$tipo->id}}"> 
                                                    </span>
                                                    <label class="control-label" for="item_{{$tipo->id}}"> {{$tipo->item}}</label>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                    
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group has-success">
                                        <div class="col-md-12">
                                            <label class="control-label">Comentario</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                <textarea rows="2" class="form-control" name="" id="observation_mantenimiento"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success refresh" onclick="enviar_mantenimientos_final2({{$mantenimientos[0]->id}})">Ingresar</button>
                            </div>
                        </div>
                        @else
                        <div class="note-info">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="alert alert-info alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                         Este elemento No tiene ningún  mantenimiento pendiente.
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Seleccione Un Tipo de Mantenimiento</label>
                                            <select  class="form-control" id="id_tipo_mant_asignar" onchange="cargar_checkbox_items()">
                                                <option value=""></option>
                                                @foreach($tipo_mantenimientos as $tipo_mantenimiento)
                                                <option value="{{$tipo_mantenimiento->id}}">{{$tipo_mantenimiento->type_maintenance}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="items_mantenimientos">
                                <div class="col-md-12">
                                   <div class="form-group"> 
                                        <div class="col-md-6">
                                            @foreach($items_mantenimiento as $tipo)
                                            <div class="input-group" style="display:none" id="campo_item_{{$tipo->id}}">
                                                <input type="checkbox" class="toggle" id="item_{{$tipo->id}}"> {{$tipo->item}}
                                            </div>
                                            @endforeach
                                        </div>
                                   </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label class="control-label">Comentario</label>
                                            <div class="input-group">
                                                <span class="input-group-addon"><i class="icon-edit"></i></span>
                                                <textarea rows="2" class="form-control" name="" id="observation_mantenimiento"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <button type="button" class="btn btn-success refresh" onclick="enviar_mantenimientos_final({{$equipos_datos[0]->id}})">Ingresar</button>
                            </div>
                        </div>
                        @endif

                    </div>
                </div>
                
                
                
         
      </div>
      <div class="modal-footer">
         
         
         <button type="button" class="btn btn-default refresh" data-dismiss="modal" id="close_modal_mantenimiento">Cerrar</button>
      </div>
   
   </form>   
   </div>
</div>

<script src="/indo/public/js/upload.js"></script>
<script type="text/javascript">
    function cargar_checkbox_items(){
        var id_tipo = $("#id_tipo_mant_asignar").val();
        $.ajax({
            type: "GET",
            url:  "mostraritemsmantenimiento",
            data: { id_tipo: id_tipo }
        })
        .done(function(data) {
            $("#items_mantenimientos").html(data);
        });
    }
</script>
<script type="text/javascript">
    function enviar_mantenimientos_final2(id){ 
        var itmes = new Array();
        var i = 0;
        var ids = new Array();
        var estados = new Array();
        var descripcion = $("#observation_mantenimiento").val();
        var tipo_mant_asignar = $("#id_tipo_mant_asignar").val();

            @foreach($items as $tipo)

                if($("#item_{{$tipo->id}}").is(':checked')) {  
                    ids[i] = {{$tipo->id}};
                    
                } else {
                    @if($tipo->required == "1")
                        toastr.error('El Item {{$tipo->item}} es Requerido para este Tipo de Mantenimiento', 'Error'); 
                        return;        
                    @endif
                }
                i++;
            @endforeach
            var descripcion = $("#observation_mantenimiento").val();
            if (descripcion == "") {
                toastr.error('El campo Comentario es Requerido.', 'Error'); 
                return;
            }
            $.ajax({
                type: "POST",
                url:  "enviarmantenimientos2",
                data: { id_details: ids, descripcion:descripcion, id:id, tipo_mant_asignar:tipo_mant_asignar}
            })
            .done(function(data) {
                toastr.success('Se ingreso correctamente el Mantenimiento', 'Nuevo Mantenimiento');
                $("#close_modal_mantenimiento").click();
            });
        }
    </script>