

<a href="#" class="btn btn-default">Número de Registros {{$numero_equipos}}</a>
<table class="table  top-blue">
   <thead>
   <tr>
         <th>Imagen</th>
         <th>Fecha Mant</th>
         <th>Usuario</th>
         <th>Tipo</th>
         <th>Marca</th>
         <th>Grupo</th>
         <th>Mantenimiento</th>
         <th>Tecnico</th>
         <!-- <th><i class="icon-print"></i></th> -->

         
      </tr>
   </thead>
   <tbody class="center">                         
  
        @foreach ($data as $row)
        <tr class="{{$row['row']}}">
            <td>
                
                <a href="#basic" data-toggle="modal" class="btn btn-default" onclick="detalle({{$row['id_equipo']}}, {{$row['id_user']}}, {{$row['id_elemento']}})"><img src="../{{$row['img_equipo']}}" width="45" height="40" style="border-radius: 8px 8px 8px 8px;"></a>   
            </td>
            <td>{{$row['fecha_mant']}}</td>
            <td>
                <span class="label label-sm {{$row['label_user']}} ">{{$row['user']}}</span>
            </td>
            <td>{{$row['tipo']}}</td>
            <td>{{$row['marca']}}</td>
            <td>{{$row['grupo']}}</td>
            <td>{{$row['tipo_mant']}}</td>
            <td>
                <a href="#" class="nameimg" style="{{$row['style_mant']}}" title="{{$row['tecnico']}}">
                <img alt="" style="border-radius:50px;" width="50" height="50"  src="../{{$row['img_mant']}}">
                </a>
            </td>
            <!-- <td><a href="generarpdfequipo?id={{$row['id_user']}}&id_equipo={{$row['id_equipo']}}" class="btn btn-info" target="_blank" {{$row['disabled']}} ><i class="icon-print"></i></a></td> -->
        </tr>
        @endforeach
   </tbody>
</table>
<div class="pagination">
  {{$pag->appends(array("code" => Input::get('code'),"applicant" => Input::get('applicant'),"marca" => Input::get('marca'),"f_compra" => Input::get('f_compra'),"tipo_equipo" => Input::get('tipo_equipo'),"grupo" => Input::get('grupo')))->links()}}
</div>

