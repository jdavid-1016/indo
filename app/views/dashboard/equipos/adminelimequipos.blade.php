
<div class="page-content" id="adminhelpdesk">
    <div class="row">
        <div class="col-md-12">
           <div class="portlet">
                <div class="portlet-title">
                    <div class="caption"><i class="icon-reorder"></i>Administrador</div>
                </div>
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <!-- <li class=""><a href="#tab_1_1" data-toggle="tab">Tipos de Equipos</a></li> -->
                        <li class=""><a href="administrador">Tipos de Mantenimientos</a></li>
                        <li class=""><a href="adminitems">Items de Mantenimientos</a></li>
                        <li class=""><a href="admigrupos">Grupos de Equipos</a></li>
                        <li class=""><a href="adminaddequipos">Agregar Equipos A Un Grupo</a></li>
                        <li class="active"><a href="adminelimequipos">Eliminar Equipos De Un Grupo</a></li>

                        
                    </ul>
                    <div  class="tab-content">
                        
                        
                        <div class="tab-pane fade active in" id="tab_1_6">
                            <div class="table-responsive" id="table_admin_">
                                <div class="row">
                                    <div class="col-md-9">
                                        <div class="portlet">
                                            <div class="portlet-title">
                                                <div class="caption"><i class="icon-remove"></i>Eliminar Equipo De Un Grupo</div>
                                                <div class="tools">
                                                    <a href="javascript:;" class="collapse"></a>
                                                    <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                                    <a href="javascript:;" class="reload"></a>
                                                    <a href="javascript:;" class="remove"></a>
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <form>
                                                    <div class="form-body">
                                                       <div class="row">
                                                            <div class="form-group has-success">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <div class="col-md-6">
                                                                            <select  class="form-control input-lg  select2me" data-placeholder="Grupo..." id="id_grupo_asign_elim" onchange="cargar_equios_del_grupo2()">
                                                                                <option value=""></option>
                                                                                @foreach($grupos as $grupo)
                                                                                <option value="{{$grupo->id}}">{{$grupo->name_group}}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <!-- <div class="col-md-6" id="">
                                                                    <div class="form-group">
                                                                        <div class="col-md-6">
                                                                            <select  class="form-control input-lg  select2me" data-placeholder="Equipo..." id="id_equipo_asing">
                                                                                <option value=""></option>
                                                                                @foreach($equipos as $equipo)
                                                                                <option value="{{$equipo->id}}">
                                                                                    {{$equipo->id}} {{$equipo->mark}} @if($equipo->name != "") {{$equipo->name}} {{$equipo->last_name}} @else Sin asignar @endif

                                                                                </option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div> -->
                                                            </div>
                                                        </div>
                                                        <hr>
                                                        <div class="row">
                                                            <div id="multiselect_equipos_eliminar">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-actions right">                           
                                                       <a class="btn btn-success" onclick="eliminar_equipos_del_grupo()">Eliminar</a>                            
                                                    </div>
                                                </form> 
                                            </div>
                                        </div>                    
                                        
                                    </div>
                                    <!-- <div class="col-md-3">
                                        <div class="form-group"> 
                                           <label class="control-label">Documento</label>
                                           <div class="input-group">
                                              <span class="input-group-addon"><i class="icon-edit"></i></span>
                                              <input type="text" class="form-control" placeholder="" data-required="true" name="name_n" id="item_documento" value="">
                                           </div> 
                                        </div> 
                                        <a class="btn btn-success" onclick="crear_documento()"><i class="icon-ok"></i> Crear</a>
                                    </div>   -->
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
           </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function cargar_equios_del_grupo(){
        var id_grupo = $("#id_grupo_asign").val();
        var tipo_equipo  = $("#id_tipo_asign").val();
        $.ajax({
                type: "GET",
                url:  "cargarequiposdelgrupo",
                data: { id_grupo: id_grupo, tipo_equipo:tipo_equipo}
        })
        .done(function(data) {
            if (data != 1) {
                //toastr.success('Se Agrego correctamente el el Equipo al Grupo', 'Nuevo Asignación de Equipo');
                //$("#id_equipo_asing").val("");
                //$("#id_grupo_asign").val("");
                
                 $("#multiselect_equipos").html(data);
            }else{
                toastr.error('No ha sido posible ingresar el  Equipo al Grupo', 'Error');
            }
        });
    }
</script>

<script type="text/javascript">
    function cargar_equios_del_grupo2(){
        var id_grupo = $("#id_grupo_asign_elim").val();

        $.ajax({
                type: "GET",
                url:  "cargarequiposdelgrupo",
                data: { id_grupo: id_grupo, eliminar:1}
        })
        .done(function(data) {
            if (data != 1) {
                //toastr.success('Se Agrego correctamente el el Equipo al Grupo', 'Nuevo Asignación de Equipo');
                //$("#id_equipo_asing").val("");
                //$("#id_grupo_asign").val("");
                
                 $("#multiselect_equipos_eliminar").html(data);
            }else{
                toastr.error('No ha sido posible ingresar el  Equipo al Grupo', 'Error');
            }
        });
    }
</script>


<script type="text/javascript">
    function crear_tipo_mantenimiento2(){ 
        
        var i = 0;
        var ids = new Array();
        var mantenimiento = $("#tipo_mantenimiento").val();
        var fecha_limite = $("#fecha_limite").val();


        @foreach($grupos as $grupo)

            if($("#grupo_{{$grupo->id}}").is(':checked')) {  
                ids[i] = {{$grupo->id}};
                i++;  
            } 
        @endforeach
        
        if (mantenimiento == "") {
            toastr.error('El campo Tipo de Mantenimiento es Requerido.', 'Error'); 
            return;
        }
        $.ajax({
            type: "POST",
            url:  "creartipomantenimiento",
            data: { id_details: ids, mantenimiento:mantenimiento, fecha_limite:fecha_limite}
        })
        .done(function(data) {

            toastr.success('Se ingreso correctamente el Mantenimiento', 'Nuevo Mantenimiento');
            var mantenimiento = $("#tipo_mantenimiento").val("");
            var fecha_limite = $("#fecha_limite").val("");
            $("#tabla_tipo_mantenimientos").html(data);
        });
          
    }
</script>


