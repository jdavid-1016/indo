
<style>
    body {
        font-family: sans-serif;
        margin: 0;
        padding: 0;
    }
    #wrapper {
        padding: 15px;
    }
    #software_adicionales {
        width: 300px;
    }
</style>



<div class="page-content">
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1_2">
            <div class="portlet ">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-reorder"></i> Eliminar Asignación
                    </div>
                </div>
                  
                <div class="portlet-body">
                    <ul  class="nav nav-tabs">
                        <li class=""><a href="equipos">Ingresar Equipos</a></li>
                        <li class=""><a href="elementos">Asignar elementos a un Equipo</a></li>
                        <li class=""><a href="asignar">Asignar Equipos a usuarios</a></li>
                        <li class="active"><a href="eliminar">Eliminar Asignación</a></li>
                    </ul>
                    <form>
                        <div class="form-body">
                           <div class="row">
                                <div class="form-group has-success">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="col-md-6">
                                                <select  class="form-control input-lg  select2me" data-placeholder="Equipo..." id="id_equipo_asignar" onchange="tabla_eliminar_asignacion()">
                                                    <option value=""></option>
                                                    @foreach($equipos as $equipo)
                                                    <option value="{{$equipo['id_equipo']}}">{{$equipo['id_equipo']}} {{$equipo['usuarios']}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12" id="tabla_eliminar_asignacion">
                                    
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-actions right">                           
                           <a class="btn btn-success" onclick="asignar_equipos()">Eliminar Asignación</a>                            
                        </div> -->
                    </form>
                </div>
            </div>
        </div>
    </div> 
</div>

<script>
    function tabla_eliminar_asignacion(){
        var equipo = $("#id_equipo_asignar").val();
        

        
        $.ajax({
                type: "GET",
                url:  "tablaeliminarasignacion",
                data: { id_equipo: equipo}
        })
        .done(function(data) {
            if (data == 1) {
                alert('paila');
            }else{
                $("#tabla_eliminar_asignacion").html(data);
            }
        });
    }
    function eliminar_elemento(id_equipo){
        var equipo = id_equipo;
        

        
        $.ajax({
                type: "GET",
                url:  "eliminarasignacion",
                data: { id_equipo: equipo}
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se elimino el elemento correctamente.', 'Eliminacion de elemento')
                $("#id_elemento_"+equipo).hide('slow');
            }else{
                toastr.error('Error al eliminar la asignacion del Elemento.', 'Error')
            }
        });
    }
    function eliminar_asignacion(id_equipo){
        var equipo = id_equipo;
        

        
        $.ajax({
                type: "GET",
                url:  "eliminarasignacionusuario",
                data: { id_equipo: equipo}
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se elimino el Usuario correctamente.', 'Eliminacion de usuario')
                $("#id_usuario_"+equipo).hide('slow');
            }else{
                toastr.error('Error al eliminar la asignacion de Usuario.', 'Error')
            }
        });
    }
</script>
