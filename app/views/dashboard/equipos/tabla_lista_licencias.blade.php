<table class="table table-striped table-hover top-blue">
    <thead>
        <tr>
            <th>Detalle</th>
            <th>ID </th>
            <th>Producto</th>
            <th>Version</th>
            <th>Cantidad</th>
            <th>Disponibles</th>
            <th>Fecha de expiracion</th>
        </tr>
    </thead>
    <tbody class="center">                         
    @foreach ($licencias as $row)
        <tr>
            <td><a class=" btn btn-default" data-target="#ajax2" id="{{$row->id}}" data-toggle="modal" onclick='cargar_detalle_licencia($(this).attr("id"));return false;'><i class="icon-eye-open"></i></a></td>
            <td>{{$row->id_licensing}}</td>
            <td>{{$row->product}}</td>
            <td>{{$row->version}}</td>
            <td>{{$row->quantity}}</td>
            <td>{{$row->available}}</td>
            <td>{{$row->date_expiry}}</td>
        </tr>
    @endforeach
       
    </tbody>
</table>