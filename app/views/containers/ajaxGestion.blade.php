
<?php 
    $host= $_SERVER["HTTP_HOST"];
    $url= $_SERVER["REQUEST_URI"];
    $direccion = "http://" . $host . $url;
    
?>
@foreach($requests as $request)
<ul  class="nav nav-tabs">
   <li class="active"><a href="#tab_1_1_{{$request->id}}" data-toggle="tab">Gestionar PQR</a></li>
   <li class=""><a href="#tab_1_2_{{$request->id}}" data-toggle="tab"> ver Notas</a></li>
   <li class=""><a href="#tab_1_3_{{$request->id}}" data-toggle="tab">Ver pagos</a></li>
   <li class=""><a href="#tab_1_4_{{$request->id}}" data-toggle="tab">Ver PQRS antiguas</a></li>
</ul>
<div  class="tab-content">
   <div class="tab-pane fade active in" id="tab_1_1_{{$request->id}}">
      <div class="">
         <div class="portlet">

                <!--_____________________tabla____________________________________ -->
              <form action="pqrs/response" method="post" accept-charset="utf-8" name="document" enctype="multipart/form-data">

                <input type="hidden" name="users_coment" value="{{ Auth::user()->id }}">
                <input type="hidden" class="form-control" name="requests_id" value="{{$request->id}}">
                
                

                <?php date_default_timezone_set("America/Bogota"); ?>
                <input type="hidden" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}">
                

                <table class="table table-hover table-striped table-bordered">
                      <tr>
                         <th>MOTIVO:</th>
                         <th>{{$request->title}}</th>
                         <th>TIPO DE PQR:</th>
                         <td>{{$request->PqrsTypes->name}}</td>
                      </tr>
                      <tr>
                         <th>FECHA DE PUBLICAIÓN:</th>
                         <td>
                            <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                               {{$request->date}}
                            </div>
                         </td>
                         <th>TIPO DE USUARIO:</th>
                         <td>
                            <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                               {{$request->PqrsTypeUsers->name}}
                            </div>
                         </td>
                      </tr>
                      <tr>
                         <th>PROCESO INVOLUCRADO</th>
                         <td>
                            <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                               {{$request->Processes->name}}
                            </div>
                         </td>
                         <th>SOPORTE</th>
                         <td>
                         @if($request->support == 'Sin soporte')
                         Sin soporte
                         @else
                         <a href="{{$request->support}}" target="_blank" class="list-group-item"><i class="icon-paperclip"></i> soporte</a>
                         @endif


                         
                         </td>
                      </tr>
                  
                      
                      
                      <tr>
                         <th>DESCRIPCIÓN:</th>
                         <td colspan="3">
                            <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                            {{$request->description}}
                            </div>
                         </td>
                      </tr>



                         <script type="text/javascript">
                            /**
                             * Funcion que se ejecuta al seleccionar una opcion del primer select
                             */
                            function cargarSelect2_<?php echo $request->id; ?>(valor)
                            {
                                /**
                                 * Este array contiene los valores sel segundo select
                                 * Los valores del mismo son:
                                 *  - hace referencia al value del primer select. Es para saber que valores
                                 *  mostrar una vez se haya seleccionado una opcion del primer select
                                 *  - value que se asignara
                                 *  - testo que se asignara
                                 */
                                  

                                var arrayValores=new Array(
                            <?php foreach ($especifique as $key_especifique): ?>
                                  <?php echo 'new Array('.$key_especifique->pqrs_descriptions_id.",".$key_especifique->id.',"'.$key_especifique->name.'"),'?>
                            <?php endforeach ?>
                                    
                                    new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
                                );
                                if(valor==0)
                                {
                                    // desactivamos el segundo select
                                    document.getElementById("select2_<?php echo $request->id; ?>").options.length=0;
                                    document.getElementById("select2_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                                    document.getElementById("select2_<?php echo $request->id; ?>").disabled=true;

                                    document.getElementById("select3_<?php echo $request->id; ?>").options.length=0;
                                    document.getElementById("select3_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                                    document.getElementById("select3_<?php echo $request->id; ?>").disabled=true;

                                }else{
                                    // eliminamos todos los posibles valores que contenga el select2
                                    document.getElementById("select2_<?php echo $request->id; ?>").options.length=0;
                                    
                                    // añadimos los nuevos valores al select2
                                    document.getElementById("select2_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                                    for(i=0;i<arrayValores.length;i++)
                                    {
                                        // unicamente añadimos las opciones que pertenecen al id seleccionado
                                        // del primer select
                                        if(arrayValores[i][0]==valor)
                                        {
                                            document.getElementById("select2_<?php echo $request->id; ?>").options[document.getElementById("select2_<?php echo $request->id; ?>").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                                        }
                                    }
                                    
                                    // habilitamos el segundo select
                                    document.getElementById("select2_<?php echo $request->id; ?>").disabled=false;
                                }
                            }

                         
                            
                         </script>

                      <tr>
                         <th>TIPO DE PQR:</th>
                         <td colspan="">
                            <select  class="form-control" name="id_request_type" required="" id='select1' onchange='cargarSelect2_<?php echo $request->id; ?>(this.value);'>
                               <option value=''>Selecciona una opcion</option>
                               @foreach($TypeRequests as $typeRequest)
                               <option value='{{$typeRequest->id}}'>{{$typeRequest->name}}</option>

                               @endforeach
                            </select>
                         </td>
                         
                         <th>ESPECIFIQUE</th>
                         <td>
                            <select disabled="" class="form-control" name="specify" id='select2_{{$request->id}}'required="" onchange='cargarSelect3_<?php echo $request->id; ?>(this.value);'>
                                <option value=''>Selecciona una opcion</option>
                            </select>
                         </td>
                      </tr>

                         <script type="text/javascript">
                            /**
                             * Funcion que se ejecuta al seleccionar una opcion del primer select
                             */
                            function cargarSelect3_<?php echo $request->id; ?>(valor)
                            {
                                /**
                                 * Este array contiene los valores sel segundo select
                                 * Los valores del mismo son:
                                 *  - hace referencia al value del primer select. Es para saber que valores
                                 *  mostrar una vez se haya seleccionado una opcion del primer select
                                 *  - value que se asignara
                                 *  - testo que se asignara
                                 */
                                  

                                var arrayValores=new Array(
                            <?php foreach ($description as $descripcion): ?>
                                  <?php echo 'new Array('.$descripcion->pqrs_types_id .",".$descripcion->id.',"'.$descripcion->name.'"),'?>
                            <?php endforeach ?>
                                    
                                    new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
                                );
                                if(valor==0)
                                {
                                    // desactivamos el segundo select
                                    document.getElementById("select3_<?php echo $request->id; ?>").options.length=0;
                                    document.getElementById("select3_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                                    document.getElementById("select3_<?php echo $request->id; ?>").disabled=true;
                                }else{
                                    // eliminamos todos los posibles valores que contenga el select2
                                    document.getElementById("select3_<?php echo $request->id; ?>").options.length=0;
                                    
                                    // añadimos los nuevos valores al select2
                                    document.getElementById("select3_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                                    for(i=0;i<arrayValores.length;i++)
                                    {
                                        // unicamente añadimos las opciones que pertenecen al id seleccionado
                                        // del primer select
                                        if(arrayValores[i][0]==valor)
                                        {
                                            document.getElementById("select3_<?php echo $request->id; ?>").options[document.getElementById("select3_<?php echo $request->id; ?>").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                                        }
                                    }
                                    
                                    // habilitamos el segundo select
                                    document.getElementById("select3_<?php echo $request->id; ?>").disabled=false;
                                }
                            }

                         
                            
                         </script>

                      <tr>
                         <th>DESCRIPCION:</th>
                         <td colspan="">
                            <select  class="form-control" disabled="" name="specify_description" required="" id="select3_<?php echo $request->id; ?>">
                               <option value=''>Selecciona una opcion</option>
                               @foreach($description as $descripcion)
                               <option value='{{$descripcion->id}}'>{{$descripcion->name}}</option>

                               @endforeach
                            </select>
                         </td>
                         
                         <th>ESTADO DE LA PQR</th>
                         <td>
                            <select  class="form-control" name="state_request" id="state_request" required="" onchange="selecOp()">
                               <option value=''></option>
                               @foreach($state_requests as $state_request)
                               <option value="{{$state_request->id}}">{{$state_request->name}}</option>
                               @endforeach
                            </select>
                         </td>
                      </tr>
                      <tr>
                         <th>ASIGNAR PQR:</th>
                         <td colspan="2">
                            <select  class="form-control" name="user_asign" required="">
                               <option value=''></option>
                               @foreach($processes as $process)
                               <option value='{{$process->id}}'>{{$process->name}}-{{$process->name}}</option>
                               @endforeach
                            </select>
                         </td>
                         
                         
                      </tr>
                      <tr>
                         <th>COMENTARIO:</th>
                         <td colspan="3">
                         <script type="text/javascript"> 
                               function selecOp{{$request->id}}(){ 
                                  var op=document.getElementById("no_conformidad_{{$request->id}}"); 
                                  
                                  var tt2=document.getElementById("coment_{{$request->id}}"); 
                                  
                                  tt2.value=tt2.value=op.options[op.options.selectedIndex].value;; 

                               } 
                            </script> 
                            <textarea  name="coment" id="coment_{{$request->id}}" class="form-control" maxlength="200" rows="3" placeholder="Escriba un comentario. EJ: Se asigna solicitud al usuario..." required=""></textarea>
                            
                            <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_text">Agregar textos <span class=""><i class="icon-plus"></i></span></a>
                            
                            <select name="no_conformidad" id="no_conformidad_{{$request->id}}" onchange="selecOp{{$request->id}}()" style="width:200px" class="form-control"> 
                               <option value="">Selecciona una Opcion</option> 
                               @foreach($default_text as $default)
                               <option value='{{$default->text}}'>{{$default->text}}</option>

                               @endforeach
                            </select>
                             
                         </td>
                      </tr>
                      <tr>
                         <th>CONFIDENCIAL:</th>
                         <td colspan="3">
                            <script type="text/javascript"> 
                               function selecOp_{{$request->id}}(){ 
                                  var op=document.getElementById("no_confidential_{{$request->id}}"); 
                                  
                                  var tt2=document.getElementById("confidential_{{$request->id}}"); 
                                  
                                  tt2.value=tt2.value=op.options[op.options.selectedIndex].value;; 

                               } 
                            </script> 
                            <textarea id="confidential_{{$request->id}}" name="confidential" class="form-control" maxlength="200" rows="2" placeholder="Este campo es confidencial y solo puede ser visto por administrativos...." ></textarea>
                            <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_confidential">Agregar textos <span class=""><i class="icon-plus"></i></span></a>
                            
                            <select name="no_conformidad" id="no_confidential_{{$request->id}}" onchange="selecOp_{{$request->id}}()" style="width:200px" class="form-control"> 
                               <option value="">Selecciona una Opcion</option> 
                               @foreach($default_confidential as $default_conf)
                               <option value='{{$default_conf->text}}'>{{$default_conf->text}}</option>

                               @endforeach
                            </select>
                         </td>
                      </tr>
                      <tr>
                         <th>
                            <input type="submit" class="btn btn-success" value="Enviar"> 
                         </th> 
                      </tr>
                   
                </table>
             </form>
             </div>

          </div>




   </div>
   <div class="tab-pane fade" id="tab_1_2_{{$request->id}}">
      <p>ver notas {{$request->Users->name}} {{$request->Users->name2}} {{$request->Users->last_name}}</p>
      <table class="table table-bordered table-hover">
         <thead>
            <tr>
               <th>PERIODO</th>
               <th>MATERIA1</th>
               <th>MATERIA2</th>
               <th>MATERIA3</th>
               <th>MATERIA4</th>
            </tr>
         </thead>
         <tbody>
            <tr>
               <td>1</td>
               <td class="active">5.0</td>
               <td class="success">4.7</td>
               <td class="warning">4.3</td>
               <td class="danger">4.5</td>
            </tr>
            <tr>
               <td>2</td>
               <td class="active">5.0</td>
               <td class="success">4.3</td>
               <td class="warning">4.7</td>
               <td class="danger">4.5</td>
            </tr>
            <tr>
               <td>3</td>
               <td class="active">4.8</td>
               <td class="success">3.2</td>
               <td class="warning">3.5</td>
               <td class="danger">5.0</td>
            </tr>
            <tr>
               <td>4</td>
               <td class="active">5.0</td>
               <td class="success">5.0</td>
               <td class="warning">4.5</td>
               <td class="danger">4.3</td>
            </tr>
         </tbody>
      </table>
   </div>
   <div class="tab-pane fade" id="tab_1_3_{{$request->id}}">
      <p>ver pagos {{$request->Users->name}} {{$request->Users->name2}} {{$request->Users->last_name}}</p>
      <div class="invoice">
         <div class="row invoice-logo">
            <div class="col-xs-6 invoice-logo-space"><img src="" alt="" /> </div>
            <div class="col-xs-6">
               <p>#5652256 / 28 Feb 2013 <span class="muted">Consectetuer adipiscing elit</span></p>
            </div>
         </div>
         <hr/>
         <div class="row">
            <div class="col-xs-4">
               <h4>Usuario:</h4>
               <ul class="list-unstyled">
                  <li></li>
                  <li>{{$request->Users->username}}</li>
                  <li> {{$request->Users->name}} {{$request->Users->name2}} {{$request->Users->last_name}}</li>
                  <li>{{$request->Users->email_institutional}}</li>
                  
               </ul>
            </div>
            <div class="col-xs-4">
               <h4>About:</h4>
               <ul class="list-unstyled">
                  <li>Drem psum dolor sit amet</li>
                  <li>Laoreet dolore magna</li>
                  <li>Consectetuer adipiscing elit</li>
                  <li>Magna aliquam tincidunt erat volutpat</li>
                  <li>Olor sit amet adipiscing eli</li>
                  <li>Laoreet dolore magna</li>
               </ul>
            </div>
            <div class="col-xs-4 invoice-payment">
               <h4>Payment Details:</h4>
               <ul class="list-unstyled">
                  <li><strong>V.A.T Reg #:</strong> 542554(DEMO)78</li>
                  <li><strong>Account Name:</strong> FoodMaster Ltd</li>
                  <li><strong>SWIFT code:</strong> 45454DEMO545DEMO</li>
                  <li><strong>V.A.T Reg #:</strong> 542554(DEMO)78</li>
                  <li><strong>Account Name:</strong> FoodMaster Ltd</li>
                  <li><strong>SWIFT code:</strong> 45454DEMO545DEMO</li>
               </ul>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-12">
               <table class="table table-striped table-hover">
                  <thead>
                     <tr>
                        <th>#</th>
                        <th>Item</th>
                        <th class="hidden-480">Description</th>
                        <th class="hidden-480">Quantity</th>
                        <th class="hidden-480">Unit Cost</th>
                        <th>Total</th>
                     </tr>
                  </thead>
                  <tbody>
                     <tr>
                        <td>1</td>
                        <td>Hardware</td>
                        <td class="hidden-480">Server hardware purchase</td>
                        <td class="hidden-480">32</td>
                        <td class="hidden-480">$75</td>
                        <td>$2152</td>
                     </tr>
                     <tr>
                        <td>2</td>
                        <td>Furniture</td>
                        <td class="hidden-480">Office furniture purchase</td>
                        <td class="hidden-480">15</td>
                        <td class="hidden-480">$169</td>
                        <td>$4169</td>
                     </tr>
                     <tr>
                        <td>3</td>
                        <td>Foods</td>
                        <td class="hidden-480">Company Anual Dinner Catering</td>
                        <td class="hidden-480">69</td>
                        <td class="hidden-480">$49</td>
                        <td>$1260</td>
                     </tr>
                     <tr>
                        <td>3</td>
                        <td>Software</td>
                        <td class="hidden-480">Payment for Jan 2013</td>
                        <td class="hidden-480">149</td>
                        <td class="hidden-480">$12</td>
                        <td>$866</td>
                     </tr>
                  </tbody>
               </table>
            </div>
         </div>
         <div class="row">
            <div class="col-xs-4">
               <div class="well">
                  <address>
                     <strong>Loop, Inc.</strong><br />
                     795 Park Ave, Suite 120<br />
                     San Francisco, CA 94107<br />
                     <abbr title="Phone">P:</abbr> (234) 145-1810
                  </address>
                  <address>
                     <strong>Full Name</strong><br />
                     <a href="mailto:#">first.last@email.com</a>
                  </address>
               </div>
            </div>
            <div class="col-xs-8 invoice-block">
               <ul class="list-unstyled amounts">
                  <li><strong>Sub - Total amount:</strong> $9265</li>
                  <li><strong>Discount:</strong> 12.9%</li>
                  <li><strong>VAT:</strong> -----</li>
                  <li><strong>Grand Total:</strong> $12489</li>
               </ul>
               <br />
               <a class="btn btn-lg btn-info hidden-print" onclick="javascript:window.print();">Print <i class="icon-print"></i></a>
               
            </div>
         </div>
      </div>
   </div>
   <div class="tab-pane fade" id="tab_1_4_{{$request->id}}">
     
      <div>


         <div class="portlet">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i> pqrs anteriores {{$request->Users->name}} {{$request->Users->name2}} {{$request->Users->last_name}}</div>
               <div class="tools">
                  <a href="javascript:;" class="collapse"></a>
                  <a href="#portlet-config" data-toggle="modal" class="config"></a>
                  <a href="javascript:;" class="reload"></a>
                  <a href="javascript:;" class="remove"></a>
               </div>
            </div>
            <div class="portlet-body">
               <div class="panel-group accordion scrollable" id="accordion2">
               @foreach($previus_requests as $previus_request)
                  <div class="panel panel-default">
                     <div class="panel-heading">
                        <h4 class="panel-title">
                           <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_{{$previus_request->id}}">
                           <strong>PQR-00{{$previus_request->id}}: </strong>{{$previus_request->title}}-{{$previus_request->date}}
                           </a>
                        </h4>
                     </div>
                     <div id="collapse_{{$previus_request->id}}" class="panel-collapse collapse">
                        <div class="panel-body">
                           <table class="table table-hover table-striped table-bordered">
                                 <tr>
                                    <th>MOTIVO:</th>
                                    <th>{{$previus_request->title}}</th>
                                    <th>TIPO DE PQR:</th>
                                    <td>{{$previus_request->PqrsTypes->name}}</td>
                                 </tr>
                                 <tr>
                                    <th>FECHA DE PUBLICAIÓN:</th>
                                    <td>
                                       <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                                          {{$previus_request->date}}
                                       </div>
                                    </td>
                                    <th>TIPO DE USUARIO:</th>
                                    <td>
                                       <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                                          {{$previus_request->PqrsTypeUsers->name}}
                                       </div>
                                    </td>
                                 </tr>
                                 <tr>
                                    <th>PROCESO INVOLUCRADO</th>
                                    <td>
                                       <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                                          {{$previus_request->Processes->name}}
                                       </div>
                                    </td>
                                    <th>SOPORTE</th>
                                    <td>
                                    @if($previus_request->support == 'Sin soporte')
                                    Sin soporte
                                    @else
                                    <a href="{{$previus_request->support}}" target="_blank" class="list-group-item"><i class="icon-paperclip"></i> soporte</a>
                                    @endif


                                    
                                    </td>
                                 </tr>
                              
                                 
                                 
                                 <tr>
                                    <th>DESCRIPCIÓN:</th>
                                    <td colspan="3">
                                       <div style="padding:5px; border:1px solid #DDD; border-radius:5px">
                                       {{$previus_request->description}}
                                       </div>
                                    </td>
                                 </tr>
                           </table>
                           
                        </div>
                     </div>
                  </div>
               @endforeach
               </div>
            </div>
         </div>
      
      
      </div>
   </div>

   
</div>

@endforeach

<div class="modal fade" id="new_text" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title text-center">INGRESAR TEXTOS</h4>
         </div><!-- modal-header -->
         <div class="modal-body">
            <div class="tab-pane col-md-12" id="">
               <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>Ingrese un texto para usarlo posteriormente</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        
                      </div><!-- tools -->
                   </div><!-- portlet-title -->
                  <div>
                  <form class="form-horizontal " role="form" action="pqrs/text" method="post" enctype="multipart/form-data">
                     <table class="table table-hover table-striped table-bordered">
                  
                  
                  
                    
                     <?php date_default_timezone_set("America/Bogota"); ?>
                     <input type="hidden" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}">
                     <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <tr>
                           <td><strong>TEXTO:</strong> </td>
                           <td >
                                <textarea id="maxlength_textarea" name="text" class="form-control" maxlength="200" rows="2" placeholder="Ingrese un texto para usarlo posteriormente..." required=""></textarea>
                           </td>
                        </tr>
                   
                     <tr>
                        <th>
                            <div class="form-group">
                               <div class="col-md-6">
                                   <input type="submit" class="btn btn-success" value="Ingresar Texto"> 
                               </div><!-- col-md-6 -->
                            </div><!-- form-group -->
                        </th>
                     </tr>
                     
                     </table>
                    </form>
                  </div><!-- portlet-body form -->
               </div><!-- portlet -->
            </div><!-- tab-pane col-md-3 -->
         </div><!-- modal-body -->
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div><!-- modal-footer -->
      </div><!-- modal-content -->
   </div><!-- modal-dialog -->
</div><!-- modal fade -->



<div class="modal fade" id="new_confidential" tabindex="-1" role="basic" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title text-center">INGRESAR TEXTOS CONFIDENCIALES</h4>
         </div><!-- modal-header -->
         <div class="modal-body">
            <div class="tab-pane col-md-12" id="">
               <div class="portlet">
                  <div class="portlet-title">
                     <div class="caption"><i class="icon-reorder"></i>Ingrese un texto para usarlo posteriormente</div>
                     <div class="tools">
                        <a href="javascript:;" class="collapse"></a>
                        
                      </div><!-- tools -->
                   </div><!-- portlet-title -->
                  <div>
                  <form class="form-horizontal " role="form" action="pqrs/confidential" method="post" enctype="multipart/form-data">
                     <table class="table table-hover table-striped table-bordered">
                  
                  
                  
                    
                     <?php date_default_timezone_set("America/Bogota"); ?>
                     <input type="hidden" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}">
                     <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                        <tr>
                           <td><strong>TEXTO:</strong> </td>
                           <td >
                                <textarea id="maxlength_textarea" name="text" class="form-control" maxlength="200" rows="2" placeholder="Ingrese un texto para usarlo posteriormente..." required=""></textarea>
                           </td>
                        </tr>
                   
                     <tr>
                        <th>
                            <div class="form-group">
                               <div class="col-md-6">
                                   <input type="submit" class="btn btn-success" value="Ingresar Texto"> 
                               </div><!-- col-md-6 -->
                            </div><!-- form-group -->
                        </th>
                     </tr>
                     
                     </table>
                    </form>
                  </div><!-- portlet-body form -->
               </div><!-- portlet -->
            </div><!-- tab-pane col-md-3 -->
         </div><!-- modal-body -->
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         </div><!-- modal-footer -->
      </div><!-- modal-content -->
   </div><!-- modal-dialog -->
</div><!-- modal fade -->