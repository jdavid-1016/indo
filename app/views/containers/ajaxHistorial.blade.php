<thead>
   <tr>
      <th class="text-center" style="padding:10px">
         N° PQR 
      </th>
      <th class="text-center" style="min-width:150px"> Motivo</th>
      <th class="text-center hidden-xs" > Descripción</th>
      <th class="text-center" style="min-width:140px">
         Usuaro
      </th>
      <th class="text-center" style="width:110px"> 
            Estado   
      </th>
      <th class="text-center" style="min-width:140px">
         Proceso
      </th>
      <th class="text-center" style="min-width:140px"> Tipo de alumno</th>
      <th class="text-center" style="min-width:115px"> 
         Tipo PQR
      </th>
      
   </tr>
</thead>

<tbody >


@if($requests)
<!-- $requests es la variable enviada desde el controlador -->
@foreach($requests as $request)
   <tr>
      <td class="highlight" style="width:120px;">
        @if($request->stateRequests->name == 'Pendiente')
           <a class="btn btn-default" data-toggle="modal" href="#wide" onclick="realizaProcesodos({{$request->id}});return false;">
              <i class="icon-edit"></i> PQR-00{{$request->id}}
           </a>
        @else
           <a class="btn btn-default" data-toggle="modal" href="#wide" disabled="" onclick="realizaProcesodos({{$request->id}});return false;">
              <i class="icon-edit"></i> PQR-00{{$request->id}}
           </a>
        @endif
      </td>
      <td >{{$request->title}}</td>
      <td class="hidden-xs" style="padding:10px">{{$request->description}}</td>
      <td>
         {{$request->user->name}}
         {{$request->user->last_name}}
      </td>
      <td>
         @if($request->stateRequests->name == 'Pendiente')
         <span class="label label-danger">{{$request->stateRequests->name}}</span>
         @elseif($request->stateRequests->name == 'Finalizado')
         <span class="label label-success">{{$request->StateRequests->name}}</span>
         @else
         <span class="label label-info">{{$request->StateRequests->name}}</span>
         @endif
      </td>
      <td>{{$request->process->name}}</td>
      <td>{{$request->TypeUsers->name}}</td>
      <td>{{$request->TypeRequests->name}}</td>
      
   </tr>
@endforeach
@endif
</tbody> -->
<tr>
   <th colspan="8">
      <div style="margin:0 auto;">
      <?php echo $requests->links(); ?>
      </div>
   </th>
   
</tr>