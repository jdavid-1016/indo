<table class="table table-hover table-striped table-bordered">
  
  <?php date_default_timezone_set("America/Bogota"); ?>
  <input type="hidden" class="form-control" name="date" required="" value="{{date('Y-m-d-H:i:s')}}">
  <input type="hidden" name="users_coment" value="{{ Auth::user()->id }}">
     <script type="text/javascript">
        /**
         * Funcion que se ejecuta al seleccionar una opcion del primer select
         */
        function cargarSelect2_<?php echo $request->id; ?>(valor)
        {
            /**
             * Este array contiene los valores sel segundo select
             * Los valores del mismo son:
             *  - hace referencia al value del primer select. Es para saber que valores
             *  mostrar una vez se haya seleccionado una opcion del primer select
             *  - value que se asignara
             *  - testo que se asignara
             */
              

            var arrayValores=new Array(
        <?php foreach ($especifique as $key_especifique): ?>
              <?php echo 'new Array('.$key_especifique->type_request_id.",".$key_especifique->id.',"'.$key_especifique->detalle.'"),'?>
        <?php endforeach ?>
                
                new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
            );
            if(valor==0)
            {
                // desactivamos el segundo select
                document.getElementById("select2_<?php echo $request->id; ?>").options.length=0;
                document.getElementById("select2_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                document.getElementById("select2_<?php echo $request->id; ?>").disabled=true;

                document.getElementById("select3_<?php echo $request->id; ?>").options.length=0;
                document.getElementById("select3_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                document.getElementById("select3_<?php echo $request->id; ?>").disabled=true;

            }else{
                // eliminamos todos los posibles valores que contenga el select2
                document.getElementById("select2_<?php echo $request->id; ?>").options.length=0;
                
                // añadimos los nuevos valores al select2
                document.getElementById("select2_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                for(i=0;i<arrayValores.length;i++)
                {
                    // unicamente añadimos las opciones que pertenecen al id seleccionado
                    // del primer select
                    if(arrayValores[i][0]==valor)
                    {
                        document.getElementById("select2_<?php echo $request->id; ?>").options[document.getElementById("select2_<?php echo $request->id; ?>").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                    }
                }
                
                // habilitamos el segundo select
                document.getElementById("select2_<?php echo $request->id; ?>").disabled=false;
            }
        }

     
        
     </script>

  <tr>
     <th>TIPO DE PQR:</th>
     <td colspan="">
        <select  class="form-control" name="id_request_type" required="" id='select1' onchange='cargarSelect2_<?php echo $request->id; ?>(this.value);'>
           <option value=''>Selecciona una opcion</option>
           @foreach($TypeRequests as $typeRequest)
           <option value='{{$typeRequest->id}}'>{{$typeRequest->name}}</option>

           @endforeach
        </select>
     </td>
     
     <th>ESPECIFIQUE</th>
     <td>
        <select disabled="" class="form-control" name="specify" id='select2_<?php echo $request->id; ?>'required="" onchange='cargarSelect3_<?php echo $request->id; ?>(this.value);'>
            <option value=''>Selecciona una opcion</option>
        </select>
     </td>
  </tr>

     <script type="text/javascript">
        /**
         * Funcion que se ejecuta al seleccionar una opcion del primer select
         */
        function cargarSelect3_<?php echo $request->id; ?>(valor)
        {
            /**
             * Este array contiene los valores sel segundo select
             * Los valores del mismo son:
             *  - hace referencia al value del primer select. Es para saber que valores
             *  mostrar una vez se haya seleccionado una opcion del primer select
             *  - value que se asignara
             *  - testo que se asignara
             */
              

            var arrayValores=new Array(
        <?php foreach ($description as $descripcion): ?>
              <?php echo 'new Array('.$descripcion->especifique_id .",".$descripcion->id.',"'.$descripcion->descripcion.'"),'?>
        <?php endforeach ?>
                
                new Array(-1,4,"opcion3-4")//comodin para finalizar el arreglo
            );
            if(valor==0)
            {
                // desactivamos el segundo select
                document.getElementById("select3_<?php echo $request->id; ?>").options.length=0;
                document.getElementById("select3_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                document.getElementById("select3_<?php echo $request->id; ?>").disabled=true;
            }else{
                // eliminamos todos los posibles valores que contenga el select2
                document.getElementById("select3_<?php echo $request->id; ?>").options.length=0;
                
                // añadimos los nuevos valores al select2
                document.getElementById("select3_<?php echo $request->id; ?>").options[0]=new Option("Selecciona una opcion", "");
                for(i=0;i<arrayValores.length;i++)
                {
                    // unicamente añadimos las opciones que pertenecen al id seleccionado
                    // del primer select
                    if(arrayValores[i][0]==valor)
                    {
                        document.getElementById("select3_<?php echo $request->id; ?>").options[document.getElementById("select3_<?php echo $request->id; ?>").options.length]=new Option(arrayValores[i][2], arrayValores[i][1]);
                    }
                }
                
                // habilitamos el segundo select
                document.getElementById("select3_<?php echo $request->id; ?>").disabled=false;
            }
        }

     
        
     </script>

  <tr>
     <th>DESCRIPCION:</th>
     <td colspan="">
        <select  class="form-control" disabled="" name="specify_description" required="" id="select3_<?php echo $request->id; ?>">
           <option value=''>Selecciona una opcion</option>
           @foreach($description as $descripcion)
           <option value='{{$descripcion->id}}'>{{$descripcion->descripcion}}</option>

           @endforeach
        </select>
     </td>
     
     <th>ESTADO DE LA PQR</th>
     <td>
        <select  class="form-control" name="state_request" id="state_request" required="" onchange="selecOp()">
           <option value=''></option>
           @foreach($state_requests as $state_request)
           <option value="{{$state_request->id}}">{{$state_request->name}}</option>
           @endforeach
        </select>
     </td>
  </tr>
  <tr>
     <th>ASIGNAR PQR:</th>
     <td colspan="2">
        <select  class="form-control" name="user_asign" required="">
           <option value=''></option>
           @foreach($processes as $process)
           <option value='{{$process->id}}'>{{$process->processes->name}}-{{$process->name}}</option>
           @endforeach
        </select>
     </td>
     
     
  </tr>
  <tr>
     <th>COMENTARIO:</th>
     <td colspan="3">
     <script type="text/javascript"> 
           function selecOp{{$request->id}}(){ 
              var op=document.getElementById("no_conformidad_{{$request->id}}"); 
              
              var tt2=document.getElementById("coment_{{$request->id}}"); 
              
              tt2.value=tt2.value=op.options[op.options.selectedIndex].value;; 

           } 
        </script> 
        <textarea  name="coment" id="coment_{{$request->id}}" class="form-control" maxlength="200" rows="3" placeholder="Escriba un comentario. EJ: Se asigna solicitud al usuario..." required=""></textarea>
        
        <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_text">Agregar textos <span class=""><i class="icon-plus"></i></span></a>
        
        <select name="no_conformidad" id="no_conformidad_{{$request->id}}" onchange="selecOp{{$request->id}}()" style="width:200px" class="form-control"> 
           <option value="">Selecciona una Opcion</option> 
           @foreach($default_text as $default)
           <option value='{{$default->text}}'>{{$default->text}}</option>

           @endforeach
        </select>
         
     </td>
  </tr>
  <tr>
     <th>CONFIDENCIAL:</th>
     <td colspan="3">
        <script type="text/javascript"> 
           function selecOp_{{$request->id}}(){ 
              var op=document.getElementById("no_confidential_{{$request->id}}"); 
              
              var tt2=document.getElementById("confidential_{{$request->id}}"); 
              
              tt2.value=tt2.value=op.options[op.options.selectedIndex].value;; 

           } 
        </script> 
        <textarea id="confidential_{{$request->id}}" name="confidential" class="form-control" maxlength="200" rows="2" placeholder="Este campo es confidencial y solo puede ser visto por administrativos...." ></textarea>
        <a class="btn btn-default" style="border-radius:5px; margin:0 auto; float:right" data-toggle="modal" href="#new_confidential">Agregar textos <span class=""><i class="icon-plus"></i></span></a>
        
        <select name="no_conformidad" id="no_confidential_{{$request->id}}" onchange="selecOp_{{$request->id}}()" style="width:200px" class="form-control"> 
           <option value="">Selecciona una Opcion</option> 
           @foreach($default_confidential as $default_conf)
           <option value='{{$default_conf->text}}'>{{$default_conf->text}}</option>

           @endforeach
        </select>
     </td>
  </tr>
  <tr>
     <th>
        <input type="submit" class="btn btn-success" value="Enviar"> 
     </th> 
  </tr>
</table>