<?php
/**
 * 
 */
class ArchivoController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }

    public function getIngreso(){
        $user = Users::find(Auth::user()->id);

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.archivo.subir_archivo')
            ->with('user', $user)
            ->with('submenu_activo', 'Ingreso')
            ->with('menu_activo', 'Archivo');
        }

    }
    public function getConsulta(){
        $user = Users::find(Auth::user()->id);

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.archivo.consultar_archivo')
            ->with('user', $user)
            ->with('submenu_activo', 'Consulta')
            ->with('menu_activo', 'Archivo');
        }

    }
    
        //funcion para guardar un nuevo Proveedor
    public function postGuardararchivo() {
        
        include("assets/Excel/reader.php");
         
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($_FILES["archivo"]["tmp_name"]);
        
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            
        $sql = "SELECT*FROM archive_boxes WHERE id=".$data->sheets[0]['cells'][$i][1];
        $datos = DB::select($sql);
        
            if($datos){

                DB::table('archive_boxes')
                ->where('id', $data->sheets[0]['cells'][$i][1])
                ->update(array('name' => $data->sheets[0]['cells'][$i][2]));

            }else{

                $box = new ArchiveBoxes;
                $box->name = $data->sheets[0]['cells'][$i][2];
                $box->save();

            }

        }
        
        for ($i = 2; $i <= $data->sheets[1]['numRows']; $i++) {
            
        $sql = "SELECT*FROM archive_packages WHERE id=".$data->sheets[1]['cells'][$i][1];
        $datos = DB::select($sql);
        
            if($datos){

                DB::table('archive_packages')
                ->where('id', $data->sheets[1]['cells'][$i][1])
                ->update(array('name' => $data->sheets[1]['cells'][$i][2], 'archive_boxes_id' => $data->sheets[1]['cells'][$i][3]));

            }else{

                $box = new ArchivePackages;
                $box->name = $data->sheets[1]['cells'][$i][2];
                $box->archive_boxes_id = $data->sheets[1]['cells'][$i][3];
                $box->save();

            }

        }
        
        for ($i = 2; $i <= $data->sheets[3]['numRows']; $i++) {
            
        $sql = "SELECT*FROM archive_type_documents WHERE id=".$data->sheets[3]['cells'][$i][1];
        $datos = DB::select($sql);
        
            if($datos){

                DB::table('archive_type_documents')
                ->where('id', $data->sheets[3]['cells'][$i][1])
                ->update(array('name' => $data->sheets[3]['cells'][$i][2], 'description' => $data->sheets[3]['cells'][$i][3]));

            }else{

                $box = new ArchiveTypeDocuments;
                $box->name = $data->sheets[3]['cells'][$i][2];
                $box->description = $data->sheets[3]['cells'][$i][3];
                $box->save();

            }

        }
        
        for ($i = 2; $i <= $data->sheets[2]['numRows']; $i++) {
            
        $sql = "SELECT*FROM archive_documents WHERE id=".$data->sheets[2]['cells'][$i][1];
        $datos = DB::select($sql);
        
            if($datos){

                DB::table('archive_documents')
                ->where('id', $data->sheets[2]['cells'][$i][1])
                ->update(array('date' => $data->sheets[2]['cells'][$i][2], 'archive_type_documents_id' => $data->sheets[2]['cells'][$i][3], 'value' => $data->sheets[2]['cells'][$i][4], 'name' => $data->sheets[2]['cells'][$i][5], 'code' => $data->sheets[2]['cells'][$i][6], 'archive_packages_id' => $data->sheets[2]['cells'][$i][7], 'providers_id' => $data->sheets[2]['cells'][$i][8]));

            }else{

                $box = new ArchiveDocuments;
                $box->date = $data->sheets[2]['cells'][$i][2];
                $box->archive_type_documents_id = $data->sheets[2]['cells'][$i][3];
                $box->value = $data->sheets[2]['cells'][$i][4];
                $box->name = $data->sheets[2]['cells'][$i][5];
                $box->code = $data->sheets[2]['cells'][$i][6];
                $box->archive_packages_id = $data->sheets[2]['cells'][$i][7];
                $box->providers_id = $data->sheets[2]['cells'][$i][8];
                $box->save();

            }

        }
        
      return Redirect::away("ingreso?ok=2");

    }
    
    //funcion para exportar todos los proveedores
    public function getExportarprov() {
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT*FROM providers";

        $datos = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Proveedores")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'NIT')
                ->setCellValue('B1', 'Sucursal')
                ->setCellValue('C1', 'Usa Sucursal (1=verdadero, 0=Falso)')
                ->setCellValue('D1', 'Nombre')
                ->setCellValue('E1', 'Contacto')
                ->setCellValue('F1', 'Direccion')
                ->setCellValue('G1', 'Telefono 1')
                ->setCellValue('H1', 'Telefono 2')
                ->setCellValue('I1', 'Telefono 3')
                ->setCellValue('J1', 'Telefono 4')
                ->setCellValue('K1', 'Fax')
                ->setCellValue('L1', 'Apartado Aereo')
                ->setCellValue('M1', 'Email')
                ->setCellValue('N1', 'Sexo (1=Ninguno, 2=Masculino, 3=Femenino, 4=Empresa)')
                ->setCellValue('O1', 'Fecha de nacimiento')
                ->setCellValue('P1', 'Digito de verificacion')
                ->setCellValue('Q1', 'Identificacion tributaria')
                ->setCellValue('R1', 'Codigo del pais')
                ->setCellValue('S1', 'Codigo de la ciudad')
                ->setCellValue('T1', 'Clasificacion tributaria')
                ->setCellValue('U1', 'Actividad economica')
                ->setCellValue('V1', 'Tipo persona juridica')
                ->setCellValue('W1', 'Observaciones')
                ->setCellValue('X1', 'Usa agente retenedor')
                ->setCellValue('Y1', 'Es beneficiario RETEIVA')
                ->setCellValue('Z1', 'Es agente RETEICA')
                ->setCellValue('AA1', 'Es declarante')
                ->setCellValue('AB1', 'Usa retencion')
                ->setCellValue('AC1', 'Estado')
                ->setCellValue('AD1', 'Usa razon social')
                ->setCellValue('AE1', 'Codigo EAN')
                ->setCellValue('AF1', 'Primer nombre')
                ->setCellValue('AG1', 'Segundo nombre')
                ->setCellValue('AH1', 'Primer apellido')
                ->setCellValue('AI1', 'Segundo apellido')
                ->setCellValue('AJ1', 'Forma de pago')
                ->setCellValue('AK1', 'Calificacion')
                ->setCellValue('AL1', 'Porcentaje de descuento')
                ->setCellValue('AM1', 'Periodo de pago')
                ->setCellValue('AN1', 'Dias optimista')
                ->setCellValue('AO1', 'Dias pesimistas')
                ->setCellValue('AP1', 'identificacion extranjera')
                ->setCellValue('AQ1', 'Codigo identificacion fiscal')
                ->setCellValue('AR1', 'Tipo empresa')
                ->setCellValue('AS1', 'Autoriza reportar entidades crediticias')
                ->setCellValue('AT1', 'Tarifa diferencial RETEIVA compras')
                ->setCellValue('AU1', 'Tarifa diferencial RETEIVA ventas')
                ->setCellValue('AV1', 'Fecha de creacion')
                ->setCellValue('AW1', 'Fecha de actualizacion');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->nit)
                    ->setCellValue('B' . $j, $datos[$i]->branch_office)
                    ->setCellValue('C' . $j, $datos[$i]->branch_office_usa)
                    ->setCellValue('D' . $j, $datos[$i]->provider)
                    ->setCellValue('E' . $j, $datos[$i]->contact)
                    ->setCellValue('F' . $j, $datos[$i]->address)
                    ->setCellValue('G' . $j, $datos[$i]->phone1)
                    ->setCellValue('H' . $j, $datos[$i]->phone2)
                    ->setCellValue('I' . $j, $datos[$i]->phone3)
                    ->setCellValue('J' . $j, $datos[$i]->phone4)
                    ->setCellValue('K' . $j, $datos[$i]->fax)
                    ->setCellValue('L' . $j, $datos[$i]->air_section)
                    ->setCellValue('M' . $j, $datos[$i]->email)
                    ->setCellValue('N' . $j, $datos[$i]->genders_id)
                    ->setCellValue('O' . $j, $datos[$i]->birth)
                    ->setCellValue('P' . $j, $datos[$i]->digit_of_verification)
                    ->setCellValue('Q' . $j, $datos[$i]->tax_ids_id)
                    ->setCellValue('R' . $j, $datos[$i]->country_code)
                    ->setCellValue('S' . $j, $datos[$i]->city_code)
                    ->setCellValue('T' . $j, $datos[$i]->tax_classifications_id)
                    ->setCellValue('U' . $j, $datos[$i]->economic_activity)
                    ->setCellValue('V' . $j, $datos[$i]->legal_person_types_id)
                    ->setCellValue('W' . $j, $datos[$i]->observations)
                    ->setCellValue('X' . $j, $datos[$i]->withholding_agent)
                    ->setCellValue('Y' . $j, $datos[$i]->beneficiary_reteiva)
                    ->setCellValue('Z' . $j, $datos[$i]->agent_reteica)
                    ->setCellValue('AA' . $j, $datos[$i]->declarant)
                    ->setCellValue('AB' . $j, $datos[$i]->uses_retention)
                    ->setCellValue('AC' . $j, $datos[$i]->status)
                    ->setCellValue('AD' . $j, $datos[$i]->uses_social_reason)
                    ->setCellValue('AE' . $j, $datos[$i]->ean_code)
                    ->setCellValue('AF' . $j, $datos[$i]->first_name)
                    ->setCellValue('AG' . $j, $datos[$i]->middle_name)
                    ->setCellValue('AH' . $j, $datos[$i]->last_name)
                    ->setCellValue('AI' . $j, $datos[$i]->last_name2)
                    ->setCellValue('AJ' . $j, $datos[$i]->method_of_payment)
                    ->setCellValue('AK' . $j, $datos[$i]->rating)
                    ->setCellValue('AL' . $j, $datos[$i]->discount_percentage)
                    ->setCellValue('AM' . $j, $datos[$i]->payment_period)
                    ->setCellValue('AN' . $j, $datos[$i]->optimistic_days)
                    ->setCellValue('AO' . $j, $datos[$i]->pessimistic_days)
                    ->setCellValue('AP' . $j, $datos[$i]->foreign_identification)
                    ->setCellValue('AQ' . $j, $datos[$i]->tax_identification_code)
                    ->setCellValue('AR' . $j, $datos[$i]->company_type)
                    ->setCellValue('AS' . $j, $datos[$i]->authorizes_lenders_report)
                    ->setCellValue('AT' . $j, $datos[$i]->differential_rate_reteiva_shopping)
                    ->setCellValue('AU' . $j, $datos[$i]->differential_rate_reteiva_sales)
                    ->setCellValue('AV' . $j, $datos[$i]->created_at)
                    ->setCellValue('AW' . $j, $datos[$i]->updated_at);

            $j++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('Proveedores');
                
        // Creamos una nueva hoja llamada “Campos”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Campos');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Columna')
                ->setCellValue('B1', 'Campo')
                ->setCellValue('C1', 'Tipo')
                ->setCellValue('D1', 'Longitud');
        
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A2', "1")
                    ->setCellValue('B2', "NIT")
                    ->setCellValue('C2', "DECIMAL")
                    ->setCellValue('D2', "13")
                    ->setCellValue('A3', "2")
                    ->setCellValue('B3', "SUCURSAL")
                    ->setCellValue('C3', "ENTERO")
                    ->setCellValue('D3', "3")
                    ->setCellValue('A4', "3")
                    ->setCellValue('B4', "USA SUCURSAL (1=Verdadero, 0=Falso)")
                    ->setCellValue('C4', "ENTERO")
                    ->setCellValue('D4', "1")
                    ->setCellValue('A5', "4")
                    ->setCellValue('B5', "NOMBRE")
                    ->setCellValue('C5', "CARÁCTER")
                    ->setCellValue('D5', "250")
                    ->setCellValue('A6', "5")
                    ->setCellValue('B6', "CONTACTO")
                    ->setCellValue('C6', "CARÁCTER")
                    ->setCellValue('D6', "250")
                    ->setCellValue('A7', "6")
                    ->setCellValue('B7', "DIRECCION")
                    ->setCellValue('C7', "CARÁCTER")
                    ->setCellValue('D7', "250")
                    ->setCellValue('A8', "7")
                    ->setCellValue('B8', "TELEFONO 1")
                    ->setCellValue('C8', "CARÁCTER")
                    ->setCellValue('D8', "50")
                    ->setCellValue('A9', "8")
                    ->setCellValue('B9', "TELEFONO 2")
                    ->setCellValue('C9', "CARÁCTER")
                    ->setCellValue('D9', "50")
                    ->setCellValue('A10', "9")
                    ->setCellValue('B10', "TELEFONO 3")
                    ->setCellValue('C10', "CARÁCTER")
                    ->setCellValue('D10', "50")
                    ->setCellValue('A11', "10")
                    ->setCellValue('B11', "TELEFONO 4")
                    ->setCellValue('C11', "CARÁCTER")
                    ->setCellValue('D11', "50")
                    ->setCellValue('A12', "11")
                    ->setCellValue('B12', "FAX")
                    ->setCellValue('C12', "CARÁCTER")
                    ->setCellValue('D12', "50")
                    ->setCellValue('A13', "12")
                    ->setCellValue('B13', "APARTADO AEREO")
                    ->setCellValue('C13', "ENTERO")
                    ->setCellValue('D13', "6")
                    ->setCellValue('A14', "13")
                    ->setCellValue('B14', "EMAIL")
                    ->setCellValue('C14', "CARÁCTER")
                    ->setCellValue('D14', "50")
                    ->setCellValue('A15', "14")
                    ->setCellValue('B15', "SEXO (1=Ninguno, 2=Masculino, 3=Femenino , 4=Empresa)")
                    ->setCellValue('C15', "ENTERO")
                    ->setCellValue('D15', "6")
                    ->setCellValue('A16', "15")
                    ->setCellValue('B16', "FECHA DE NACIMIENTO")
                    ->setCellValue('C16', "FECHA")
                    ->setCellValue('D16', "MM/DD/AAAA")
                    ->setCellValue('A17', "16")
                    ->setCellValue('B17', "DIGITO DE VERIFICACION")
                    ->setCellValue('C17', "ENTERO")
                    ->setCellValue('D17', "1")
                    ->setCellValue('A18', "17")
                    ->setCellValue('B18', "IDENTIFICACION TRIBUTARIA (1=Ninguno, 2=Nit, 3=Cedula, 4=Ruc, 5=Código Identificación, 6=Pasaporte, 7=Cedula Extranjería, 8=Tarjeta de Identidad, 9=Registro Civil, 10=Tarjeta Extranjería, 11= Numero Único identificación Personal)")
                    ->setCellValue('C18', "ENTERO")
                    ->setCellValue('D18', "1")
                    ->setCellValue('A19', "18")
                    ->setCellValue('B19', "CODIGO DEL PAIS")
                    ->setCellValue('C19', "ENTERO")
                    ->setCellValue('D19', "3")
                    ->setCellValue('A20', "19")
                    ->setCellValue('B20', "CODIGO DE LA CIUDAD")
                    ->setCellValue('C20', "ENTERO")
                    ->setCellValue('D20', "3")
                    ->setCellValue('A21', "20")
                    ->setCellValue('B21', "CLASIFICACION TRIBUTARIA (1=Ninguno, 2=Gran Contribuyente, 3=Empresa Del Estado, 4=Régimen Común, 5=régimen Simplificado, 6=Régimen Simplificado No Residente País, 7=No Residente País, 8=No Responsable IVA)")
                    ->setCellValue('C21', "ENTERO")
                    ->setCellValue('D21', "1")
                    ->setCellValue('A22', "21")
                    ->setCellValue('B22', "ACTIVIDAD ECONOMICA")
                    ->setCellValue('C22', "ENTERO")
                    ->setCellValue('D22', "5")
                    ->setCellValue('A23', "22")
                    ->setCellValue('B23', "TIPO PERSONA JURIDICA (1=Ninguno, 2=Natural, 3=Jurídica)")
                    ->setCellValue('C23', "ENTERO")
                    ->setCellValue('D23', "1")
                    ->setCellValue('A24', "23")
                    ->setCellValue('B24', "OBSERVACIONES")
                    ->setCellValue('C24', "CARÁCTER")
                    ->setCellValue('D24', "50")
                    ->setCellValue('A25', "24")
                    ->setCellValue('B25', "USA AGENTE RETENEDOR (1=Verdadero, 0=Falso)")
                    ->setCellValue('C25', "ENTERO")
                    ->setCellValue('D25', "1")
                    ->setCellValue('A26', "25")
                    ->setCellValue('B26', "ES BENEFICIARO RETEIVA (1=Verdadero, 0=Falso)")
                    ->setCellValue('C26', "ENTERO")
                    ->setCellValue('D26', "1")
                    ->setCellValue('A27', "26")
                    ->setCellValue('B27', "ES AGENTE RETEICA (1=Verdadero, 0=Falso)")
                    ->setCellValue('C27', "ENTERO")
                    ->setCellValue('D27', "1")
                    ->setCellValue('A28', "27")
                    ->setCellValue('B28', "ES DECLARANTE (1=Verdadero, 0=Falso)")
                    ->setCellValue('C28', "ENTERO")
                    ->setCellValue('D28', "1")
                    ->setCellValue('A29', "28")
                    ->setCellValue('B29', "USA RETENCION (1=Verdadero, 0=Falso)")
                    ->setCellValue('C29', "ENTERO")
                    ->setCellValue('D29', "1")
                    ->setCellValue('A30', "29")
                    ->setCellValue('B30', "ACTIVO (1 = Verdadero, 0 = Falso)")
                    ->setCellValue('C30', "ENTERO")
                    ->setCellValue('D30', "1")
                    ->setCellValue('A31', "30")
                    ->setCellValue('B31', "USA RAZON SOCIAL (1 = Verdadero, 0 = Falso)")
                    ->setCellValue('C31', "ENTERO")
                    ->setCellValue('D31', "1")
                    ->setCellValue('A32', "31")
                    ->setCellValue('B32', "CODIGO EAN")
                    ->setCellValue('C32', "CARÁCTER")
                    ->setCellValue('D32', "13")
                    ->setCellValue('A33', "32")
                    ->setCellValue('B33', "PRIMER NOMBRE")
                    ->setCellValue('C33', "CARÁCTER")
                    ->setCellValue('D33', "100")
                    ->setCellValue('A34', "33")
                    ->setCellValue('B34', "SEGUNDO NOMBRE")
                    ->setCellValue('C34', "CARÁCTER")
                    ->setCellValue('D34', "100")
                    ->setCellValue('A35', "34")
                    ->setCellValue('B35', "PRIMER APELLIDO")
                    ->setCellValue('C35', "CARÁCTER")
                    ->setCellValue('D35', "100")
                    ->setCellValue('A36', "35")
                    ->setCellValue('B36', "SEGUNDO APELLIDO")
                    ->setCellValue('C36', "CARÁCTER")
                    ->setCellValue('D36', "100")
                    ->setCellValue('A37', "36")
                    ->setCellValue('B37', "FORMA DE PAGO")
                    ->setCellValue('C37', "ENTERO")
                    ->setCellValue('D37', "3")
                    ->setCellValue('A38', "37")
                    ->setCellValue('B38', "CALIFICACION")
                    ->setCellValue('C38', "ENTERO")
                    ->setCellValue('D38', "3")
                    ->setCellValue('A39', "38")
                    ->setCellValue('B39', "PORCENTAJE DE DESCUENTO")
                    ->setCellValue('C39', "DECIMAL")
                    ->setCellValue('D39', "3,2")
                    ->setCellValue('A40', "39")
                    ->setCellValue('B40', "PERIODO DE PAGO")
                    ->setCellValue('C40', "ENTERO")
                    ->setCellValue('D40', "3")
                    ->setCellValue('A41', "40")
                    ->setCellValue('B41', "DIAS OPTIMISTA")
                    ->setCellValue('C41', "ENTERO")
                    ->setCellValue('D41', "4")
                    ->setCellValue('A42', "41")
                    ->setCellValue('B42', "DIAS PESIMISTA")
                    ->setCellValue('C42', "ENTERO")
                    ->setCellValue('D42', "4")
                    ->setCellValue('A43', "42")
                    ->setCellValue('B43', "IDENTIFICACION EXTRANJERA")
                    ->setCellValue('C43', "DECIMAL")
                    ->setCellValue('D43', "12")
                    ->setCellValue('A44', "43")
                    ->setCellValue('B44', "CODIGO IDENTIFICACION FISCAL")
                    ->setCellValue('C44', "ENTERO")
                    ->setCellValue('D44', "1")
                    ->setCellValue('A45', "44")
                    ->setCellValue('B45', "TIPO EMPRESA")
                    ->setCellValue('C45', "ENTERO")
                    ->setCellValue('D45', "6")
                    ->setCellValue('A46', "45")
                    ->setCellValue('B46', "AUTORIZA REPORTAR ENTIDADES CREDITICIAS")
                    ->setCellValue('C46', "ENTERO")
                    ->setCellValue('D46', "1")
                    ->setCellValue('A47', "46")
                    ->setCellValue('B47', "TARIFA DIFERENCIAL RETEIVA COMPRAS")
                    ->setCellValue('C47', "DECIMAL")
                    ->setCellValue('D47', "2,2")
                    ->setCellValue('A48', "47")
                    ->setCellValue('B48', "TARIFA DIFERENCIAL RETEIVA VENTAS")
                    ->setCellValue('C48', "DECIMAL")
                    ->setCellValue('D48', "2,2");               

        $objPHPExcel->setActiveSheetIndex(0);


        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Proveedores_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    
    

}

?>
