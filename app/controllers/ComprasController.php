<?php
/**
 * 
 */
class ComprasController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }
    
    //funcion para saber la diferencia entre dos horas
    public function hourdiff($hour_1 , $hour_2 , $formated=false){
    
        $h1_explode = explode(":" , $hour_1);
        $h2_explode = explode(":" , $hour_2);

        $h1_explode[0] = (int) $h1_explode[0];
        $h1_explode[1] = (int) $h1_explode[1];
        $h2_explode[0] = (int) $h2_explode[0];
        $h2_explode[1] = (int) $h2_explode[1];
        

        $h1_to_minutes = ($h1_explode[0] * 60) + $h1_explode[1];
        $h2_to_minutes = ($h2_explode[0] * 60) + $h2_explode[1];

        
        if($h1_to_minutes > $h2_to_minutes){
        $subtraction = $h1_to_minutes - $h2_to_minutes;
        }
        else
        {
        $subtraction = $h2_to_minutes - $h1_to_minutes;
        }

        $result = $subtraction / 60;

        if(is_float($result) && $formated){
        
        $result = (string) $result;
          
        $result_explode = explode(".",$result);

        return $result_explode[0].":".(($result_explode[1]*60)/10);
        }
        else
        {
        return $result;
        }
    }
    
    public function interval_date3() {


        // Paso 1
        // Recorres las fechas desde la menor hasta la mayor y sumas 1 día. Si es sábado o domingo no lo sumas.
        // Paso 2
        // Operas con las horas. Si la segunda es mayor que la primera la restas y lo sumas a los días.
        // Si la mayor es la primera, haces la resta y le descuentas las horas a los días que te ha dado el paso 1.
        
    
        $ini = Input::get('fecha1');
        $fin = Input::get('fecha2');
        
        $dia_i = date('Y-m-d H:i:s', strtotime($ini));
        $dia_f = date('Y-m-d H:i:s', strtotime($fin));
        
        $consulta = DB::select('SELECT holiday FROM general_holidays ');        
        $festivos= array();
        
            foreach($consulta as $con){
            array_push($festivos, $con->holiday);
            }
            
        $total = 0;
        $cont = 0;
        $permiso=0;
        
        for($i=$dia_i;$i<=$dia_f;$i = date("Y-m-d", strtotime($i ."+ 1 day"))){
            
            echo $i . "<br />";
            $dia_valido =  date('l', strtotime($i));
                        
            if ($dia_valido == "Sunday") {
                $permiso=0;
                //echo "1";
            }elseif($dia_valido == "Saturday"){
                $permiso=1;
                //echo "2";
                $hora_limite = "13:00";
            }else{
             $permiso=1;
             //echo "3";
             $hora_limite = "17:30";
            }
            
            if(in_array(date('Y-m-d', strtotime($i)), $festivos)){
                $permiso=0;
            }
            
            if($permiso==1){
                
               // echo 'nueva fecha '.$nuevafecha."<br>";
                //echo date('Y-m-d', strtotime($i));
                //echo date('Y-m-d', strtotime($fin));
               if (date('Y-m-d', strtotime($i)) ==  date('Y-m-d', strtotime($fin))) { 
                   $hora_limite = date('H:i', strtotime($fin));                   
               }

               if($cont==0){
               $h = date('H:i', strtotime($ini));
               if($h<"07:30"){
                   $h = "07:30";
               }
               //echo $h;
               }else{
               $h = "07:30";
               //echo $h;
               }
               if($h>"07:29" && $h<$hora_limite){
               $total = $total + $this->hourdiff($h , $hora_limite);               
               }
                              
            }
            $cont=1;

        }
        
        $minutos = 0;
        $minutos2 = explode(".", $total);
        
        if(isset($minutos2[1])){
             $minutos2[1] = "0.".$minutos2[1];
            $minutos = (float)$minutos2[1]*60/100;
            $minutos = $minutos *100;
            
        }
        
        if($minutos2[0]<1){
        return round($minutos)." Minutos";
        }else{
        return $minutos2[0]." Horas y ".round($minutos)." Minutos";
        }
                
    }
    
    //funcion para consultar los datos del proveedor en cada cotizacion segun el NIT
    public function getComprobarprov()
	{
		//get POST data
                $nit = Input::get('nit');
                //$bandera = Users::find($correo_institucional);
                $bandera = DB::select('SELECT * FROM providers WHERE nit = ?', array($nit));
                if($bandera){
                    return $bandera;
                }else{
                    return 0;
                }
	}
        
    //funcion para consultar los datos del proveedor en cada cotizacion segun el Nombre
    public function getConsultarprov()
	{
		//get POST data
                $texto = Input::get('texto');
                
                $sql = "SELECT * FROM providers WHERE provider LIKE '%".$texto."%' ORDER BY provider DESC";

                $datos = DB::select($sql);
                
                if($datos){
                    for($i=0;count($datos)>$i;$i++)
                        {
                        ?>
                        <a onclick="agregarNit('<?php echo $datos[$i]->nit; ?>', '<?php echo $datos[$i]->provider; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                        <div class="display_box" align="left">
                        <div style="margin-right:6px;"><b><?php echo $datos[$i]->provider; ?></b></div></div>
                        </a>
                        <?php
                        }
                }else{
                    echo 0;
                }
                    
	}
        
    //funcion para consultar los datos del proveedor en cada cotizacion segun el Nombre
    public function getConsultarnit()
	{
		//get POST data
                $texto = Input::get('texto');
                
                $sql = "SELECT * FROM providers WHERE nit LIKE '%".$texto."%' ORDER BY nit DESC";

                $datos = DB::select($sql);
                
                if($datos){
                    for($i=0;count($datos)>$i;$i++)
                        {
                        ?>
                        <a onclick="agregarProv('<?php echo $datos[$i]->nit; ?>', '<?php echo $datos[$i]->provider; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                        <div class="display_box" align="left">
                        <div style="margin-right:6px;"><b><?php echo $datos[$i]->nit; ?></b></div></div>
                        </a>
                        <?php
                        }
                }else{
                    echo 0;
                }
                    
	}
        
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
//funcion para consultar los datos del proveedor en cada cotizacion segun el Nombre
    public function getConsultarprovedit()
	{
		//get POST data
                $name = Input::get('texto');
                
                $sql = "SELECT * FROM providers WHERE provider LIKE '%".$name."%' ORDER BY provider DESC";

                $datos = DB::select($sql);
                
                if($datos){
                    for($i=0;count($datos)>$i;$i++)
                        {
                        ?>
                        <a onclick="agregarProvp('<?php echo $datos[$i]->nit; ?>', '<?php echo $datos[$i]->provider; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                        <div class="display_box" align="left">
                        <div style="margin-right:6px;"><b><?php echo $datos[$i]->provider; ?></b></div></div>
                        </a>
                        <?php
                        }
                }else{
                    echo 0;
                }
                    
	}

//funcion para consultar los datos del proveedor en cada cotizacion segun el Nombre
    public function getConsultarnitedit()
	{
		//get POST data
                $nit = Input::get('texto');
                
                $sql = "SELECT * FROM providers WHERE nit LIKE '%".$nit."%' ORDER BY nit DESC";

                $datos = DB::select($sql);
                
                if($datos){
                    for($i=0;count($datos)>$i;$i++)
                        {
                        ?>
                        <a onclick="agregarProvp('<?php echo $datos[$i]->nit; ?>', '<?php echo $datos[$i]->provider; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                        <div class="display_box" align="left">
                        <div style="margin-right:6px;"><b><?php echo $datos[$i]->nit; ?></b></div></div>
                        </a>
                        <?php
                        }
                }else{
                    echo 0;
                }
                    
	}

//funcion para consultar los datos del proveedor en el formulario de editar
    public function getConsultarprovp()
	{
		//get POST data
                $nit = Input::get('nit');
                
                $sql = "SELECT * FROM providers WHERE nit = '".$nit."'";

                $datos = DB::select($sql);
                
                if($datos){
                    return $datos;
                }else{
                    echo 0;
                }
                    
	}
        
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    //funcion para mostrar la vista de crear Proveedores
    public function getGestionprov() {
        
        $user = Users::find(Auth::user()->id);

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.gestionproveedores')
            ->with('user', $user)
            ->with('submenu_activo', 'Gestionar Solicitudes')
            ->with('menu_activo', 'Compras');
        }
    }
    
    //funcion para guardar un nuevo Proveedor
    public function getGuardarprov() {
        
        $sql = "SELECT * FROM providers WHERE nit = '".Input::get('nit')."'";

                $datos = DB::select($sql);
                
                if($datos){
                    
                    
                DB::table('providers')
                ->where('nit', Input::get('nit'))
                ->update(array('branch_office' => Input::get('branch_office'), 
                               'branch_office_usa' => Input::get('branch_office_usa'),
                               'provider' => Input::get('provider'),
                               'contact' => Input::get('contact'),
                               'address' => Input::get('address'),
                               'phone1' => Input::get('phone1'),
                               'phone2' => Input::get('phone2'),
                               'phone3' => Input::get('phone3'),
                               'phone4' => Input::get('phone4'),
                               'fax' => Input::get('fax'),
                               'air_section' => Input::get('air_section'),
                               'email' => Input::get('email'),
                               'genders_id' => Input::get('genders_id'),
                               'birth' => Input::get('birth'),
                               'digit_of_verification' => Input::get('digit_of_verification'),
                               'tax_ids_id' => Input::get('tax_ids_id'),
                               'country_code' => Input::get('country_code'),
                               'city_code' => Input::get('city_code'),
                               'tax_classifications_id' => Input::get('tax_classifications_id'),
                               'economic_activity' => Input::get('economic_activity'),
                               'legal_person_types_id' => Input::get('legal_person_types_id'),
                               'observations' => Input::get('observations'),
                               'withholding_agent' => Input::get('withholding_agent'),
                               'beneficiary_reteiva' => Input::get('beneficiary_reteiva'),
                               'agent_reteica' => Input::get('agent_reteica'),
                               'declarant' => Input::get('declarant'),
                               'uses_retention' => Input::get('uses_retention'),
                               'status' => Input::get('status'),
                               'uses_social_reason' => Input::get('uses_social_reason'),
                               'ean_code' => Input::get('ean_code'),
                               'first_name' => Input::get('first_name'),
                               'middle_name' => Input::get('middle_name'),
                               'last_name' => Input::get('last_name'),
                               'last_name2' => Input::get('last_name2'),
                               'method_of_payment' => Input::get('method_of_payment'),
                               'rating' => Input::get('rating'),
                               'discount_percentage' => Input::get('discount_percentage'),
                               'payment_period' => Input::get('payment_period'),
                               'optimistic_days' => Input::get('optimistic_days'),
                               'pessimistic_days' => Input::get('pessimistic_days'),
                               'foreign_identification' => Input::get('foreign_identification'),
                               'tax_identification_code' => Input::get('tax_identification_code'),
                               'company_type' => Input::get('company_type'),
                               'authorizes_lenders_report' => Input::get('authorizes_lenders_report'),
                               'differential_rate_reteiva_shopping' => Input::get('differential_rate_reteiva_shopping'),
                               'differential_rate_reteiva_sales' => Input::get('differential_rate_reteiva_sales'),
                               'payments_methods_id' => Input::get('payments_methods_id'),
                               'bank' => Input::get('bank'),
                               'account_type' => Input::get('account_type'),
                               'account_number' => Input::get('account_number')));
                
                echo 1;
                    
                                        
                }else{
                    
                    $prov = new Providers;

                    $prov->nit = Input::get('nit');
                    $prov->branch_office = Input::get('branch_office');
                    $prov->branch_office_usa = Input::get('branch_office_usa');
                    $prov->provider = Input::get('provider');
                    $prov->contact = Input::get('contact');
                    $prov->address = Input::get('address');
                    $prov->phone1 = Input::get('phone1');
                    $prov->phone2 = Input::get('phone2');
                    $prov->phone3 = Input::get('phone3');
                    $prov->phone4 = Input::get('phone4');
                    $prov->fax = Input::get('fax');
                    $prov->air_section = Input::get('air_section');
                    $prov->email = Input::get('email');
                    $prov->genders_id = Input::get('genders_id');
                    $prov->birth = Input::get('birth');
                    $prov->digit_of_verification = Input::get('digit_of_verification');
                    $prov->tax_ids_id = Input::get('tax_ids_id');
                    $prov->country_code = Input::get('country_code');
                    $prov->city_code = Input::get('city_code');
                    $prov->tax_classifications_id = Input::get('tax_classifications_id');
                    $prov->economic_activity = Input::get('economic_activity');
                    $prov->legal_person_types_id = Input::get('legal_person_types_id');
                    $prov->observations = Input::get('observations');
                    $prov->withholding_agent = Input::get('withholding_agent');
                    $prov->beneficiary_reteiva = Input::get('beneficiary_reteiva');
                    $prov->agent_reteica = Input::get('agent_reteica');
                    $prov->declarant = Input::get('declarant');
                    $prov->uses_retention = Input::get('uses_retention');
                    $prov->status = Input::get('status');
                    $prov->uses_social_reason = Input::get('uses_social_reason');
                    $prov->ean_code = Input::get('ean_code');
                    $prov->first_name = Input::get('first_name');
                    $prov->middle_name = Input::get('middle_name');
                    $prov->first_name = Input::get('first_name');
                    $prov->last_name = Input::get('last_name');
                    $prov->last_name2 = Input::get('last_name2');
                    $prov->method_of_payment = Input::get('method_of_payment');
                    $prov->rating = Input::get('rating');
                    $prov->discount_percentage = Input::get('discount_percentage');
                    $prov->payment_period = Input::get('payment_period');
                    $prov->optimistic_days = Input::get('optimistic_days');
                    $prov->pessimistic_days = Input::get('pessimistic_days');
                    $prov->foreign_identification = Input::get('foreign_identification');
                    $prov->tax_identification_code = Input::get('tax_identification_code');
                    $prov->company_type = Input::get('company_type');
                    $prov->authorizes_lenders_report = Input::get('authorizes_lenders_report');
                    $prov->differential_rate_reteiva_shopping = Input::get('differential_rate_reteiva_shopping');
                    $prov->differential_rate_reteiva_sales = Input::get('differential_rate_reteiva_sales');
                    $prov->payments_methods_id = Input::get('payments_methods_id');
                    $prov->bank = Input::get('bank');
                    $prov->account_type = Input::get('account_type');
                    $prov->account_number = Input::get('account_number');

                    if($prov->save()){
                        echo "1";
                    }else{
                        echo "0";
                    }
                    
                }
        
    }
    
    //funcion para guardar un nuevo Proveedor
    public function postGuardararcprov() {
        
        include("assets/Excel/reader.php");
         
        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($_FILES["archivo"]["tmp_name"]);
        
        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {
            
        $sql = "SELECT*FROM providers WHERE nit=".$data->sheets[0]['cells'][$i][1];
        $datos = DB::select($sql);
        
            if($datos){

                DB::table('providers')
                ->where('nit', $data->sheets[0]['cells'][$i][1])
                ->update(array('branch_office' => $data->sheets[0]['cells'][$i][2], 
                               'branch_office_usa' => $data->sheets[0]['cells'][$i][3],
                               'provider' => $data->sheets[0]['cells'][$i][4],
                               'contact' => $data->sheets[0]['cells'][$i][5],
                               'address' => $data->sheets[0]['cells'][$i][6],
                               'phone1' => $data->sheets[0]['cells'][$i][7],
                               'phone2' => $data->sheets[0]['cells'][$i][8],
                               'phone3' => $data->sheets[0]['cells'][$i][9],
                               'phone4' => $data->sheets[0]['cells'][$i][10],
                               'fax' => $data->sheets[0]['cells'][$i][11],
                               'air_section' => $data->sheets[0]['cells'][$i][12],
                               'email' => $data->sheets[0]['cells'][$i][13],
                               'genders_id' => $data->sheets[0]['cells'][$i][14],
                               'birth' => $data->sheets[0]['cells'][$i][15],
                               'digit_of_verification' => $data->sheets[0]['cells'][$i][16],
                               'tax_ids_id' => $data->sheets[0]['cells'][$i][17],
                               'country_code' => $data->sheets[0]['cells'][$i][18],
                               'city_code' => $data->sheets[0]['cells'][$i][19],
                               'tax_classifications_id' => $data->sheets[0]['cells'][$i][20],
                               'economic_activity' => $data->sheets[0]['cells'][$i][21],
                               'legal_person_types_id' => $data->sheets[0]['cells'][$i][22],
                               'observations' => $data->sheets[0]['cells'][$i][23],
                               'withholding_agent' => $data->sheets[0]['cells'][$i][24],
                               'beneficiary_reteiva' => $data->sheets[0]['cells'][$i][25],
                               'agent_reteica' => $data->sheets[0]['cells'][$i][26],
                               'declarant' => $data->sheets[0]['cells'][$i][27],
                               'uses_retention' => $data->sheets[0]['cells'][$i][28],
                               'status' => $data->sheets[0]['cells'][$i][29],
                               'uses_social_reason' => $data->sheets[0]['cells'][$i][30],
                               'ean_code' => $data->sheets[0]['cells'][$i][31],
                               'first_name' => $data->sheets[0]['cells'][$i][32],
                               'middle_name' => $data->sheets[0]['cells'][$i][33],
                               'last_name' => $data->sheets[0]['cells'][$i][34],
                               'last_name2' => $data->sheets[0]['cells'][$i][35],
                               'method_of_payment' => $data->sheets[0]['cells'][$i][36],
                               'rating' => $data->sheets[0]['cells'][$i][37],
                               'discount_percentage' => $data->sheets[0]['cells'][$i][38],
                               'payment_period' => $data->sheets[0]['cells'][$i][39],
                               'optimistic_days' => $data->sheets[0]['cells'][$i][40],
                               'pessimistic_days' => $data->sheets[0]['cells'][$i][41],
                               'foreign_identification' => $data->sheets[0]['cells'][$i][42],
                               'tax_identification_code' => $data->sheets[0]['cells'][$i][43],
                               'company_type' => $data->sheets[0]['cells'][$i][44],
                               'authorizes_lenders_report' => $data->sheets[0]['cells'][$i][45],
                               'differential_rate_reteiva_shopping' => $data->sheets[0]['cells'][$i][46],
                               'differential_rate_reteiva_sales' => $data->sheets[0]['cells'][$i][47],
                               'payments_methods_id' => $data->sheets[0]['cells'][$i][48],
                               'bank' => $data->sheets[0]['cells'][$i][49],
                               'account_type' => $data->sheets[0]['cells'][$i][50],
                               'account_number' => $data->sheets[0]['cells'][$i][51]));

            }else{

                $prov = new Providers;

                $prov->nit = $data->sheets[0]['cells'][$i][1];
                $prov->branch_office = $data->sheets[0]['cells'][$i][2];
                $prov->branch_office_usa = $data->sheets[0]['cells'][$i][3];
                $prov->provider = $data->sheets[0]['cells'][$i][4];
                $prov->contact = $data->sheets[0]['cells'][$i][5];
                $prov->address = $data->sheets[0]['cells'][$i][6];
                $prov->phone1 = $data->sheets[0]['cells'][$i][7];
                $prov->phone2 = $data->sheets[0]['cells'][$i][8];
                $prov->phone3 = $data->sheets[0]['cells'][$i][9];
                $prov->phone4 = $data->sheets[0]['cells'][$i][10];
                $prov->fax = $data->sheets[0]['cells'][$i][11];
                $prov->air_section = $data->sheets[0]['cells'][$i][12];
                $prov->email = $data->sheets[0]['cells'][$i][13];
                $prov->genders_id = $data->sheets[0]['cells'][$i][14];
                $prov->birth = $data->sheets[0]['cells'][$i][15];
                $prov->digit_of_verification = $data->sheets[0]['cells'][$i][16];
                $prov->tax_ids_id = $data->sheets[0]['cells'][$i][17];
                $prov->country_code = $data->sheets[0]['cells'][$i][18];
                $prov->city_code = $data->sheets[0]['cells'][$i][19];
                $prov->tax_classifications_id = $data->sheets[0]['cells'][$i][20];
                $prov->economic_activity = $data->sheets[0]['cells'][$i][21];
                $prov->legal_person_types_id = $data->sheets[0]['cells'][$i][22];
                $prov->observations = $data->sheets[0]['cells'][$i][23];
                $prov->withholding_agent = $data->sheets[0]['cells'][$i][24];
                $prov->beneficiary_reteiva = $data->sheets[0]['cells'][$i][25];
                $prov->agent_reteica = $data->sheets[0]['cells'][$i][26];
                $prov->declarant = $data->sheets[0]['cells'][$i][27];
                $prov->uses_retention = $data->sheets[0]['cells'][$i][28];
                $prov->status = $data->sheets[0]['cells'][$i][29];
                $prov->uses_social_reason = $data->sheets[0]['cells'][$i][30];
                $prov->ean_code = $data->sheets[0]['cells'][$i][31];
                $prov->first_name = $data->sheets[0]['cells'][$i][32];
                $prov->middle_name = $data->sheets[0]['cells'][$i][33];
                $prov->last_name = $data->sheets[0]['cells'][$i][34];
                $prov->last_name2 = $data->sheets[0]['cells'][$i][35];
                $prov->method_of_payment = $data->sheets[0]['cells'][$i][36];
                $prov->rating = $data->sheets[0]['cells'][$i][37];
                $prov->discount_percentage = $data->sheets[0]['cells'][$i][38];
                $prov->payment_period = $data->sheets[0]['cells'][$i][39];
                $prov->optimistic_days = $data->sheets[0]['cells'][$i][40];
                $prov->pessimistic_days = $data->sheets[0]['cells'][$i][41];
                $prov->foreign_identification = $data->sheets[0]['cells'][$i][42];
                $prov->tax_identification_code = $data->sheets[0]['cells'][$i][43];
                $prov->company_type = $data->sheets[0]['cells'][$i][44];
                $prov->authorizes_lenders_report = $data->sheets[0]['cells'][$i][45];
                $prov->differential_rate_reteiva_shopping = $data->sheets[0]['cells'][$i][46];
                $prov->differential_rate_reteiva_sales = $data->sheets[0]['cells'][$i][47];
                $prov->payments_methods_id = $data->sheets[0]['cells'][$i][48];
                $prov->bank = $data->sheets[0]['cells'][$i][49];
                $prov->account_type = $data->sheets[0]['cells'][$i][50];
                $prov->account_number = $data->sheets[0]['cells'][$i][51];
                

                $prov->save();

            }

        }
        
      return Redirect::away("gestionprov?ok=2");

    }
    
    //funcion para exportar todos los proveedores
    public function getExportarprov() {
        include("assets/phpexcel/PHPExcel.php");

        $sql = "SELECT*FROM providers";

        $datos = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Proveedores")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'NIT')
                ->setCellValue('B1', 'Sucursal')
                ->setCellValue('C1', 'Usa Sucursal (1=verdadero, 0=Falso)')
                ->setCellValue('D1', 'Nombre')
                ->setCellValue('E1', 'Contacto')
                ->setCellValue('F1', 'Direccion')
                ->setCellValue('G1', 'Telefono 1')
                ->setCellValue('H1', 'Telefono 2')
                ->setCellValue('I1', 'Telefono 3')
                ->setCellValue('J1', 'Telefono 4')
                ->setCellValue('K1', 'Fax')
                ->setCellValue('L1', 'Apartado Aereo')
                ->setCellValue('M1', 'Email')
                ->setCellValue('N1', 'Sexo (1=Ninguno, 2=Masculino, 3=Femenino, 4=Empresa)')
                ->setCellValue('O1', 'Fecha de nacimiento')
                ->setCellValue('P1', 'Digito de verificacion')
                ->setCellValue('Q1', 'Identificacion tributaria')
                ->setCellValue('R1', 'Codigo del pais')
                ->setCellValue('S1', 'Codigo de la ciudad')
                ->setCellValue('T1', 'Clasificacion tributaria')
                ->setCellValue('U1', 'Actividad economica')
                ->setCellValue('V1', 'Tipo persona juridica')
                ->setCellValue('W1', 'Observaciones')
                ->setCellValue('X1', 'Usa agente retenedor')
                ->setCellValue('Y1', 'Es beneficiario RETEIVA')
                ->setCellValue('Z1', 'Es agente RETEICA')
                ->setCellValue('AA1', 'Es declarante')
                ->setCellValue('AB1', 'Usa retencion')
                ->setCellValue('AC1', 'Estado')
                ->setCellValue('AD1', 'Usa razon social')
                ->setCellValue('AE1', 'Codigo EAN')
                ->setCellValue('AF1', 'Primer nombre')
                ->setCellValue('AG1', 'Segundo nombre')
                ->setCellValue('AH1', 'Primer apellido')
                ->setCellValue('AI1', 'Segundo apellido')
                ->setCellValue('AJ1', 'Forma de pago')
                ->setCellValue('AK1', 'Calificacion')
                ->setCellValue('AL1', 'Porcentaje de descuento')
                ->setCellValue('AM1', 'Periodo de pago')
                ->setCellValue('AN1', 'Dias optimista')
                ->setCellValue('AO1', 'Dias pesimistas')
                ->setCellValue('AP1', 'identificacion extranjera')
                ->setCellValue('AQ1', 'Codigo identificacion fiscal')
                ->setCellValue('AR1', 'Tipo empresa')
                ->setCellValue('AS1', 'Autoriza reportar entidades crediticias')
                ->setCellValue('AT1', 'Tarifa diferencial RETEIVA compras')
                ->setCellValue('AU1', 'Tarifa diferencial RETEIVA ventas')
                ->setCellValue('AV1', 'ID Metodo de pago')
                ->setCellValue('AW1', 'Banco')
                ->setCellValue('AX1', 'Tipo de cuenta')
                ->setCellValue('AY1', '# de cuenta')
                ->setCellValue('AZ1', 'Fecha de creacion')
                ->setCellValue('BA1', 'Fecha de actualizacion');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->nit)
                    ->setCellValue('B' . $j, $datos[$i]->branch_office)
                    ->setCellValue('C' . $j, $datos[$i]->branch_office_usa)
                    ->setCellValue('D' . $j, $datos[$i]->provider)
                    ->setCellValue('E' . $j, $datos[$i]->contact)
                    ->setCellValue('F' . $j, $datos[$i]->address)
                    ->setCellValue('G' . $j, $datos[$i]->phone1)
                    ->setCellValue('H' . $j, $datos[$i]->phone2)
                    ->setCellValue('I' . $j, $datos[$i]->phone3)
                    ->setCellValue('J' . $j, $datos[$i]->phone4)
                    ->setCellValue('K' . $j, $datos[$i]->fax)
                    ->setCellValue('L' . $j, $datos[$i]->air_section)
                    ->setCellValue('M' . $j, $datos[$i]->email)
                    ->setCellValue('N' . $j, $datos[$i]->genders_id)
                    ->setCellValue('O' . $j, $datos[$i]->birth)
                    ->setCellValue('P' . $j, $datos[$i]->digit_of_verification)
                    ->setCellValue('Q' . $j, $datos[$i]->tax_ids_id)
                    ->setCellValue('R' . $j, $datos[$i]->country_code)
                    ->setCellValue('S' . $j, $datos[$i]->city_code)
                    ->setCellValue('T' . $j, $datos[$i]->tax_classifications_id)
                    ->setCellValue('U' . $j, $datos[$i]->economic_activity)
                    ->setCellValue('V' . $j, $datos[$i]->legal_person_types_id)
                    ->setCellValue('W' . $j, $datos[$i]->observations)
                    ->setCellValue('X' . $j, $datos[$i]->withholding_agent)
                    ->setCellValue('Y' . $j, $datos[$i]->beneficiary_reteiva)
                    ->setCellValue('Z' . $j, $datos[$i]->agent_reteica)
                    ->setCellValue('AA' . $j, $datos[$i]->declarant)
                    ->setCellValue('AB' . $j, $datos[$i]->uses_retention)
                    ->setCellValue('AC' . $j, $datos[$i]->status)
                    ->setCellValue('AD' . $j, $datos[$i]->uses_social_reason)
                    ->setCellValue('AE' . $j, $datos[$i]->ean_code)
                    ->setCellValue('AF' . $j, $datos[$i]->first_name)
                    ->setCellValue('AG' . $j, $datos[$i]->middle_name)
                    ->setCellValue('AH' . $j, $datos[$i]->last_name)
                    ->setCellValue('AI' . $j, $datos[$i]->last_name2)
                    ->setCellValue('AJ' . $j, $datos[$i]->method_of_payment)
                    ->setCellValue('AK' . $j, $datos[$i]->rating)
                    ->setCellValue('AL' . $j, $datos[$i]->discount_percentage)
                    ->setCellValue('AM' . $j, $datos[$i]->payment_period)
                    ->setCellValue('AN' . $j, $datos[$i]->optimistic_days)
                    ->setCellValue('AO' . $j, $datos[$i]->pessimistic_days)
                    ->setCellValue('AP' . $j, $datos[$i]->foreign_identification)
                    ->setCellValue('AQ' . $j, $datos[$i]->tax_identification_code)
                    ->setCellValue('AR' . $j, $datos[$i]->company_type)
                    ->setCellValue('AS' . $j, $datos[$i]->authorizes_lenders_report)
                    ->setCellValue('AT' . $j, $datos[$i]->differential_rate_reteiva_shopping)
                    ->setCellValue('AU' . $j, $datos[$i]->differential_rate_reteiva_sales)
                    ->setCellValue('AV' . $j, $datos[$i]->payments_methods_id)
                    ->setCellValue('AW' . $j, $datos[$i]->bank)
                    ->setCellValue('AX' . $j, $datos[$i]->account_type)
                    ->setCellValue('AY' . $j, $datos[$i]->account_number)
                    ->setCellValue('AZ' . $j, $datos[$i]->created_at)
                    ->setCellValue('BA' . $j, $datos[$i]->updated_at);

            $j++;
        }
        $objPHPExcel->getActiveSheet()->setTitle('Proveedores');
                
        // Creamos una nueva hoja llamada “Campos”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Campos');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Columna')
                ->setCellValue('B1', 'Campo')
                ->setCellValue('C1', 'Tipo')
                ->setCellValue('D1', 'Longitud');
        
        $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A2', "1")
                    ->setCellValue('B2', "NIT")
                    ->setCellValue('C2', "DECIMAL")
                    ->setCellValue('D2', "13")
                    ->setCellValue('A3', "2")
                    ->setCellValue('B3', "SUCURSAL")
                    ->setCellValue('C3', "ENTERO")
                    ->setCellValue('D3', "3")
                    ->setCellValue('A4', "3")
                    ->setCellValue('B4', "USA SUCURSAL (1=Verdadero, 0=Falso)")
                    ->setCellValue('C4', "ENTERO")
                    ->setCellValue('D4', "1")
                    ->setCellValue('A5', "4")
                    ->setCellValue('B5', "NOMBRE")
                    ->setCellValue('C5', "CARÁCTER")
                    ->setCellValue('D5', "250")
                    ->setCellValue('A6', "5")
                    ->setCellValue('B6', "CONTACTO")
                    ->setCellValue('C6', "CARÁCTER")
                    ->setCellValue('D6', "250")
                    ->setCellValue('A7', "6")
                    ->setCellValue('B7', "DIRECCION")
                    ->setCellValue('C7', "CARÁCTER")
                    ->setCellValue('D7', "250")
                    ->setCellValue('A8', "7")
                    ->setCellValue('B8', "TELEFONO 1")
                    ->setCellValue('C8', "CARÁCTER")
                    ->setCellValue('D8', "50")
                    ->setCellValue('A9', "8")
                    ->setCellValue('B9', "TELEFONO 2")
                    ->setCellValue('C9', "CARÁCTER")
                    ->setCellValue('D9', "50")
                    ->setCellValue('A10', "9")
                    ->setCellValue('B10', "TELEFONO 3")
                    ->setCellValue('C10', "CARÁCTER")
                    ->setCellValue('D10', "50")
                    ->setCellValue('A11', "10")
                    ->setCellValue('B11', "TELEFONO 4")
                    ->setCellValue('C11', "CARÁCTER")
                    ->setCellValue('D11', "50")
                    ->setCellValue('A12', "11")
                    ->setCellValue('B12', "FAX")
                    ->setCellValue('C12', "CARÁCTER")
                    ->setCellValue('D12', "50")
                    ->setCellValue('A13', "12")
                    ->setCellValue('B13', "APARTADO AEREO")
                    ->setCellValue('C13', "ENTERO")
                    ->setCellValue('D13', "6")
                    ->setCellValue('A14', "13")
                    ->setCellValue('B14', "EMAIL")
                    ->setCellValue('C14', "CARÁCTER")
                    ->setCellValue('D14', "50")
                    ->setCellValue('A15', "14")
                    ->setCellValue('B15', "SEXO (1=Ninguno, 2=Masculino, 3=Femenino , 4=Empresa)")
                    ->setCellValue('C15', "ENTERO")
                    ->setCellValue('D15', "6")
                    ->setCellValue('A16', "15")
                    ->setCellValue('B16', "FECHA DE NACIMIENTO")
                    ->setCellValue('C16', "FECHA")
                    ->setCellValue('D16', "MM/DD/AAAA")
                    ->setCellValue('A17', "16")
                    ->setCellValue('B17', "DIGITO DE VERIFICACION")
                    ->setCellValue('C17', "ENTERO")
                    ->setCellValue('D17', "1")
                    ->setCellValue('A18', "17")
                    ->setCellValue('B18', "IDENTIFICACION TRIBUTARIA (1=Ninguno, 2=Nit, 3=Cedula, 4=Ruc, 5=Código Identificación, 6=Pasaporte, 7=Cedula Extranjería, 8=Tarjeta de Identidad, 9=Registro Civil, 10=Tarjeta Extranjería, 11= Numero Único identificación Personal)")
                    ->setCellValue('C18', "ENTERO")
                    ->setCellValue('D18', "1")
                    ->setCellValue('A19', "18")
                    ->setCellValue('B19', "CODIGO DEL PAIS")
                    ->setCellValue('C19', "ENTERO")
                    ->setCellValue('D19', "3")
                    ->setCellValue('A20', "19")
                    ->setCellValue('B20', "CODIGO DE LA CIUDAD")
                    ->setCellValue('C20', "ENTERO")
                    ->setCellValue('D20', "3")
                    ->setCellValue('A21', "20")
                    ->setCellValue('B21', "CLASIFICACION TRIBUTARIA (1=Ninguno, 2=Gran Contribuyente, 3=Empresa Del Estado, 4=Régimen Común, 5=régimen Simplificado, 6=Régimen Simplificado No Residente País, 7=No Residente País, 8=No Responsable IVA)")
                    ->setCellValue('C21', "ENTERO")
                    ->setCellValue('D21', "1")
                    ->setCellValue('A22', "21")
                    ->setCellValue('B22', "ACTIVIDAD ECONOMICA")
                    ->setCellValue('C22', "ENTERO")
                    ->setCellValue('D22', "5")
                    ->setCellValue('A23', "22")
                    ->setCellValue('B23', "TIPO PERSONA JURIDICA (1=Ninguno, 2=Natural, 3=Jurídica)")
                    ->setCellValue('C23', "ENTERO")
                    ->setCellValue('D23', "1")
                    ->setCellValue('A24', "23")
                    ->setCellValue('B24', "OBSERVACIONES")
                    ->setCellValue('C24', "CARÁCTER")
                    ->setCellValue('D24', "50")
                    ->setCellValue('A25', "24")
                    ->setCellValue('B25', "USA AGENTE RETENEDOR (1=Verdadero, 0=Falso)")
                    ->setCellValue('C25', "ENTERO")
                    ->setCellValue('D25', "1")
                    ->setCellValue('A26', "25")
                    ->setCellValue('B26', "ES BENEFICIARO RETEIVA (1=Verdadero, 0=Falso)")
                    ->setCellValue('C26', "ENTERO")
                    ->setCellValue('D26', "1")
                    ->setCellValue('A27', "26")
                    ->setCellValue('B27', "ES AGENTE RETEICA (1=Verdadero, 0=Falso)")
                    ->setCellValue('C27', "ENTERO")
                    ->setCellValue('D27', "1")
                    ->setCellValue('A28', "27")
                    ->setCellValue('B28', "ES DECLARANTE (1=Verdadero, 0=Falso)")
                    ->setCellValue('C28', "ENTERO")
                    ->setCellValue('D28', "1")
                    ->setCellValue('A29', "28")
                    ->setCellValue('B29', "USA RETENCION (1=Verdadero, 0=Falso)")
                    ->setCellValue('C29', "ENTERO")
                    ->setCellValue('D29', "1")
                    ->setCellValue('A30', "29")
                    ->setCellValue('B30', "ACTIVO (1 = Verdadero, 0 = Falso)")
                    ->setCellValue('C30', "ENTERO")
                    ->setCellValue('D30', "1")
                    ->setCellValue('A31', "30")
                    ->setCellValue('B31', "USA RAZON SOCIAL (1 = Verdadero, 0 = Falso)")
                    ->setCellValue('C31', "ENTERO")
                    ->setCellValue('D31', "1")
                    ->setCellValue('A32', "31")
                    ->setCellValue('B32', "CODIGO EAN")
                    ->setCellValue('C32', "CARÁCTER")
                    ->setCellValue('D32', "13")
                    ->setCellValue('A33', "32")
                    ->setCellValue('B33', "PRIMER NOMBRE")
                    ->setCellValue('C33', "CARÁCTER")
                    ->setCellValue('D33', "100")
                    ->setCellValue('A34', "33")
                    ->setCellValue('B34', "SEGUNDO NOMBRE")
                    ->setCellValue('C34', "CARÁCTER")
                    ->setCellValue('D34', "100")
                    ->setCellValue('A35', "34")
                    ->setCellValue('B35', "PRIMER APELLIDO")
                    ->setCellValue('C35', "CARÁCTER")
                    ->setCellValue('D35', "100")
                    ->setCellValue('A36', "35")
                    ->setCellValue('B36', "SEGUNDO APELLIDO")
                    ->setCellValue('C36', "CARÁCTER")
                    ->setCellValue('D36', "100")
                    ->setCellValue('A37', "36")
                    ->setCellValue('B37', "FORMA DE PAGO")
                    ->setCellValue('C37', "ENTERO")
                    ->setCellValue('D37', "3")
                    ->setCellValue('A38', "37")
                    ->setCellValue('B38', "CALIFICACION")
                    ->setCellValue('C38', "ENTERO")
                    ->setCellValue('D38', "3")
                    ->setCellValue('A39', "38")
                    ->setCellValue('B39', "PORCENTAJE DE DESCUENTO")
                    ->setCellValue('C39', "DECIMAL")
                    ->setCellValue('D39', "3,2")
                    ->setCellValue('A40', "39")
                    ->setCellValue('B40', "PERIODO DE PAGO")
                    ->setCellValue('C40', "ENTERO")
                    ->setCellValue('D40', "3")
                    ->setCellValue('A41', "40")
                    ->setCellValue('B41', "DIAS OPTIMISTA")
                    ->setCellValue('C41', "ENTERO")
                    ->setCellValue('D41', "4")
                    ->setCellValue('A42', "41")
                    ->setCellValue('B42', "DIAS PESIMISTA")
                    ->setCellValue('C42', "ENTERO")
                    ->setCellValue('D42', "4")
                    ->setCellValue('A43', "42")
                    ->setCellValue('B43', "IDENTIFICACION EXTRANJERA")
                    ->setCellValue('C43', "DECIMAL")
                    ->setCellValue('D43', "12")
                    ->setCellValue('A44', "43")
                    ->setCellValue('B44', "CODIGO IDENTIFICACION FISCAL")
                    ->setCellValue('C44', "ENTERO")
                    ->setCellValue('D44', "1")
                    ->setCellValue('A45', "44")
                    ->setCellValue('B45', "TIPO EMPRESA")
                    ->setCellValue('C45', "ENTERO")
                    ->setCellValue('D45', "6")
                    ->setCellValue('A46', "45")
                    ->setCellValue('B46', "AUTORIZA REPORTAR ENTIDADES CREDITICIAS")
                    ->setCellValue('C46', "ENTERO")
                    ->setCellValue('D46', "1")
                    ->setCellValue('A47', "46")
                    ->setCellValue('B47', "TARIFA DIFERENCIAL RETEIVA COMPRAS")
                    ->setCellValue('C47', "DECIMAL")
                    ->setCellValue('D47', "2,2")
                    ->setCellValue('A48', "47")
                    ->setCellValue('B48', "TARIFA DIFERENCIAL RETEIVA VENTAS")
                    ->setCellValue('C48', "DECIMAL")
                    ->setCellValue('D48', "2,2")
                    ->setCellValue('A49', "48")
                    ->setCellValue('B49', "ID METODO DE PAGO")
                    ->setCellValue('C49', "ENTERO")
                    ->setCellValue('D49', "5")
                    ->setCellValue('A50', "49")
                    ->setCellValue('B50', "BANCO")
                    ->setCellValue('C50', "CARÁCTER")
                    ->setCellValue('D50', "50")
                    ->setCellValue('A51', "50")
                    ->setCellValue('B51', "TIPO DE CUENTA (1=Ninguno, 2=Cheque, 3=Transferencia, 4=Efectivo, 5=T.Credito)")
                    ->setCellValue('C51', "CARÁCTER")
                    ->setCellValue('D51', "50")
                    ->setCellValue('A52', "51")
                    ->setCellValue('B52', "# DE CUENTA")
                    ->setCellValue('C52', "ENTERO")
                    ->setCellValue('D52', "20");

        $objPHPExcel->setActiveSheetIndex(0);


        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Proveedores_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    //funcion para mostrar la vista de crear nuevas solicitudes
    public function getSolicitudes() {
        $user = Users::find(Auth::user()->id);

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            
            $historico = ShoppingCategories::all();
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.my_requisitions')
            ->with('user', $user)
            //->with('noti', $noti)
            ->with('submenu_activo', 'Solicitudes')
            ->with('menu_activo', 'Compras');
        }
    }
    
    //funcion para notificacion de nueva compra
    public function getNotpendingcompra() {

        $bandera = 0;
        $sql = "SELECT*FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.users_id = '".Auth::user()->id."' AND shoppings.id = '".Input::get('id')."'";
        $user_not = DB::select($sql);
        if(count($user_not)>0){
            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            $bandera = $notificaciones->last()->id;
        }
        echo $bandera;
    }    

    //funcion para notificacion de nueva compra
    public function getNotnewcompra() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 26;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
    
    //funcion para notificacion de nueva compra
    public function getNotnewshopping() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 34;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
    
    //funcion para notificacion de nueva compra
    public function getNotrefusecompra() {
                        
        $bandera = 0;
        $sql = "SELECT*FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.users_id = '".Auth::user()->id."' AND shoppings.id = '".Input::get('id')."'";
        $user_not = DB::select($sql);
        if(count($user_not)>0){
            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            $bandera = $notificaciones->last()->id;
        }
        echo $bandera;
    }
    
    //funcion para notificacion de nueva compra
    public function getNotgestcompra() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 27;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
        
    
    //funcion para notificacion de nueva compra
    public function getNotrefusegestcompra() {
                        
        $bandera = 0;
        $sql = "SELECT*FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.users_id = '".Auth::user()->id."' AND shoppings.id = '".Input::get('id')."'";
        $user_not = DB::select($sql);
        if(count($user_not)>0){
            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            $bandera = $notificaciones->last()->id;
        }
        echo $bandera;
    }
    
    
    //funcion para notificacion de nueva compra
    public function getNotadmincompra() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 26;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
        
    
    //funcion para notificacion de nueva compra
    public function getNotrefuseadmincompra() {
                        
        $bandera = 0;
        $sql = "SELECT*FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.users_id = '".Auth::user()->id."' AND shoppings.id = '".Input::get('id')."'";
        $user_not = DB::select($sql);
        if(count($user_not)>0){
            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
            $bandera = $notificaciones->last()->id;
        }
        echo $bandera;
    }
    
    //funcion para mostrar la vista de mis solicitudes de compra
    public function getMisolicitudescompras() {

        $permission = $this->permission_control("23");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

       $compras = Shoppings::where('shopping_statuses_id', '<>', '5')
       ->where('shopping_statuses_id', '<>', '7')
       ->where('users_id', Auth::user()->id)
       ->orderBy('id', 'DESC')
       ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);

            $data[$i]['row_color'] = '';
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }
            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4){
                $data[$i]['label'] = 'label-success';
                $data[$i]['row_color'] = 'green';
            }else if ($compras[$i]->shopping_statuses_id == 6)
                $data[$i]['label'] = 'label-default';

        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_admin_supports')
            ->with('supports', $data);
        } else {
            DB::table('notifications') ->where('user_id' ,'=', Auth::user()->id)->where('description' ,'=', 'Agrego una fecha de entrega para su solicitud de compra')->where('estate' ,'=', '1') ->update(array('estate' => 0));
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.mis_solicitudes_compras')
            ->with('shoppings', $data)
            ->with('submenu_activo', 'Solicitudes')
            ->with('menu_activo', 'Compras');
        }

    }

    //función para mostrar la vista de histórico de compras
    public function getHistoricocompras() {

        $permission = $this->permission_control("23");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

       $compras = Shoppings::where('shopping_statuses_id', '<>', '1')
       ->where('shopping_statuses_id', '<>', '2')
       ->where('shopping_statuses_id', '<>', '3')
       ->where('shopping_statuses_id', '<>', '4')
       ->where('shopping_statuses_id', '<>', '6')
       ->where('users_id', Auth::user()->id)
       ->orderBy('id', 'DESC')
       ->get();

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }
            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';
            else if ($compras[$i]->shopping_statuses_id == 6){
                $data[$i]['label'] = 'label-default';
                $data[$i]['row_color'] = 'green';
            }else if ($compras[$i]->shopping_statuses_id == 7)
                $data[$i]['label'] = 'label-success';

        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_admin_supports')
            ->with('supports', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.mi_historico_compras')
            ->with('shoppings', $data)
           
            ->with('submenu_activo', 'Solicitudes')
            ->with('menu_activo', 'Compras');
        }       
               
    }

    //funcion para mostrar las categorias de compras
    public function getCategorias() {
        $categorias = ShoppingCategories::all();
        
        $select = "<select name='categorias' id='categorias'>";
        foreach($categorias as $cat){
            $select .= "<option value='".$cat->id."'>".$cat->name."</option>";
        }
        $select .= "</select>";
        
        return $select;
    }

    //funcion para crear una nueva compra
    public function getNuevacompra(){

        $my_profile = Users::find(Auth::user()->id);
        foreach ($my_profile->Profiles as $perfil) {
            $my_process = $perfil->Processes->id;
        }
        
        $sql = "SELECT users.id FROM users JOIN administratives ON (users.id = administratives.user_id) JOIN profiles_users ON (users.id = profiles_users.users_id) JOIN profiles ON (profiles_users.profiles_id = profiles.id) WHERE profiles.processes_id = '".$my_process."' AND administratives.responsible = 1";
        $usuario_lider = DB::select($sql);

        $estado = 1;
         $lider = DB::table('administratives')
                ->select('responsible')
                ->where('user_id', Auth::user()->id)
                ->get();
        
        $lider = DB::table('administratives')
                ->select('responsible')
                ->where('user_id', Auth::user()->id)
                ->get();
        
        if($lider[0]->responsible==1){
         $estado=2;
        }
        
        $shoppings = new Shoppings;
       
        $shoppings->shopping_statuses_id = $estado;
        $shoppings->users_id = Auth::user()->id;
        $shoppings->justification = Input::get('justificacion');
        $items = Input::get('items');
        if($shoppings->save()){
            
            $last_id = Shoppings::Where("users_id", Auth::user()->id)->get();
            $last_id = $last_id->last()->id;
            
            for($i=0;count($items)>$i;$i++){
                
                $details = new ShoppingDetails;
                $details->date_required = $items[$i][0];
                $details->description = $items[$i][1];
                $details->quantity = $items[$i][2];
                $details->reference = $items[$i][3];
                $details->value = $items[$i][4];
                $details->shoppings_id = $last_id;
                $details->shopping_categories_id = $items[$i][5];
                $details->shopping_details_statuses_id = 1;
                $details->save();
                
            }
            
            if($estado==2){
            return 1;
            }else{
                
                $notification = new Notifications;

                        $notification->description = 'ha creado una nueva solicitud de compra';
                        $notification->estate = '1';
                        $notification->type_notifications_id = '1';
                        $notification->user_id = $usuario_lider[0]->id;
                        $notification->user_id1 = Auth::user()->id;
                        $notification->link = 'compras/aprobar';

                        $notification->save();
                
                $arreglo[0] = Auth::user();
                $arreglo[1] = $usuario_lider[0];
             return $arreglo;
            }
        }else{
            return 0;
        }
    }
    //funcion para mostrar la vista de administrar compras
    public function getAdministrar() {

        $permission = $this->permission_control("27");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        $compras = Shoppings::where('shopping_statuses_id', 3)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('users_id', $var, $signo)
        ->orderBy('id', 'ASC')
        ->paginate(15);


        $applicants = Users::all();
        
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["cid"] = $compras[$i]->id;
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;

            $cuotation = ShoppingTraces::where("shoppings_id", $compras[$i]->id)
                        ->where('status', 2)
                        ->get();

            $data[$i]["user2"] = $cuotation['0']->Users->name." ".$cuotation['0']->Users->last_name;
            

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }
            $data[$i]['row_color'] = '';
            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4){
                $data[$i]['label'] = 'label-success';
                $data[$i]['row_color'] = 'green';
            }
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';
        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.compras.table_admin_compras')
            ->with('shoppings', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.admin_compras')
            ->with('shoppings', $data)
            ->with('applicants', $applicants)
            ->with('pag', $compras)
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Compras');
        }
    }
    //funcion para mostrar la vista de historico administrar compras
    public function getAdministrarhist() {

        $permission = $this->permission_control("27");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

       $compras = Shoppings::where('shopping_statuses_id',"<>", 2)
       ->where('shopping_statuses_id',"<>", 1)
       ->where('shopping_statuses_id',"<>", 3)
       ->where('id', 'LIKE', '%' . $codigo . '%')
       ->where('users_id', $var, $signo)
       ->where('shopping_statuses_id', 'LIKE', '%' . $status . '%')
       ->orderBy('id', 'desc')
       ->paginate(15);
       $applicants = Users::all();

       $statuses = ShoppingStatuses::all();
        
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["cid"] = $compras[$i]->id;
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;
            
            $cuotation = ShoppingTraces::where("shoppings_id", $compras[$i]->id)
                        ->where('status', 2)
                        ->get();

            $data[$i]["user2"] = $cuotation['0']->Users->name." ".$cuotation['0']->Users->last_name;
            
            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            $data[$i]['row_color'] = '';

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }

            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4){
                $data[$i]['label'] = 'label-success';
                $data[$i]['row_color'] = 'green';
            }
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';
            else if ($compras[$i]->shopping_statuses_id == 6)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 7)
                $data[$i]['label'] = 'label-success';
        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_admin_supports')
            ->with('supports', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.admin_compras_hist')
            ->with('shoppings', $data)
            ->with('applicants', $applicants)
            ->with('statuses', $statuses)
            ->with('pag', $compras)
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Compras');
        }
    }
    //funcion para mostrar la vista de gestion de compras
    public function getGestion() {

        $permission = $this->permission_control("26");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }
        $compras = Shoppings::where('shopping_statuses_id', '<>', '5')
        ->where('shopping_statuses_id', '<>', '1')
        ->where('shopping_statuses_id', '<>', '6')
        ->where('shopping_statuses_id', '<>', '7')
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('users_id', $var, $signo)
        ->where('shopping_statuses_id', 'LIKE', '%' . $status . '%')
        ->orderBy('id','ASC')
        ->paginate(15);
        $applicants = Users::all();
        $statuses = ShoppingStatuses::all();
        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["cid"] = $compras[$i]->id;
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            // ******************************************************

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;

            $cuotation = ShoppingTraces::where("shoppings_id", $compras[$i]->id)
                        ->where('status', 2)
                        ->get();

            $data[$i]["user2"] = $cuotation['0']->Users->name." ".$cuotation['0']->Users->last_name;

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';
            //$data[$i]['hours'] = $this->calcula_time($fecha, $fecha_actual);
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }

            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4){
                $data[$i]['label'] = 'label-success';
                $data[$i]['row_color'] = 'green';
            }
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';

        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.compras.table_gestion_compras')
            ->with('shoppings', $data);
        } else {
            
            DB::table('notifications') ->where('user_id' ,'=', Auth::user()->id)->where('description' ,'=', 'Creo una nueva solicitud de compra')->where('estate' ,'=', '1') ->update(array('estate' => 0));
            DB::table('notifications') ->where('user_id' ,'=', Auth::user()->id)->where('description' ,'=', 'Aprobo una solicitud de compra')->where('estate' ,'=', '1') ->update(array('estate' => 0));
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.gestion_compras')
            ->with('shoppings', $data)
            ->with('applicants', $applicants)
            ->with('statuses', $statuses)
            ->with('pag', $compras)
            ->with('submenu_activo', 'Gestionar Solicitudes')
            ->with('menu_activo', 'Compras');
        }
        
    }
    //funcion para mostrar la vista de administrar compras
    public function getGestionhist() {

        $permission = $this->permission_control("26");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        $compras = Shoppings::where('shopping_statuses_id', '<>','1')
        ->where('shopping_statuses_id', '<>','1')
        ->where('shopping_statuses_id', '<>','2')
        ->where('shopping_statuses_id', '<>','3')
        ->where('shopping_statuses_id', '<>','4')
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('users_id', $var, $signo)
        ->where('shopping_statuses_id', 'LIKE', '%' . $status . '%')
        ->orderBy('id','DESC')
        ->paginate(15);

        $applicants = Users::all();
        $statuses = ShoppingStatuses::all();
        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["cid"] = $compras[$i]->id;
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            $cuotation = ShoppingTraces::where("shoppings_id", $compras[$i]->id)
                        ->where('status', 2)
                        ->get();
            
            $data[$i]["user2"] = $cuotation['0']->Users->name." ".$cuotation['0']->Users->last_name;
            
            // echo $compras[$i]->id;
            // exit;

            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }
            
            // ******************************************************

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }





            if ($compras[$i]->shopping_statuses_id == 6)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';

        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_gestion_compras_hist')
            ->with('supports', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.gestion_compras_hist')
            ->with('shoppings', $data)
            ->with('applicants', $applicants)
            ->with('statuses', $statuses)
            ->with('pag', $compras)
            ->with('submenu_activo', 'Gestionar Solicitudes')
            ->with('menu_activo', 'Compras');
        }
    }
    //funcion para hacer pruebas en el modulo de compras
    public function getPrueba(){

        //$compras = Shoppings::get();
        $compras = ShoppingDetailsStatuses::get();

        foreach ($compras as $key) {

            echo "compra: ".$key->name;
            //echo "--justficacion: ".$key->description."<br>";
            //echo "--fecha: ".$key->created_at;
            //echo "--estado: ".$key->ShoppingStatuses->name;
        }
    }

    //funcion para mostrar el modal de detalle de compras
    public function getDetalle() {
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();
        $time = $this->interval_date($shoppings_details[0]->Shoppings->created_at, $fecha_actual = date('Y-m-d H:i:s'));
        $technical = DB::table('users')
        ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
        ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
        ->select('users.id', 'users.name', 'users.last_name')
        ->where('profiles.processes_id', 7)
        ->where('users.status', 1)
        ->orderBy('name', 'asc')
        ->get();


 
        return View::make('dashboard.compras.admin_shoppings_edit')
        ->with('statuses', $status)
        ->with('time', $time)
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('shopping_details', $shoppings_details);
    }

    //funcion para mostrar el modal de detalle de compras con comentarios
    public function getDetalle2() {
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();
        $time = $this->interval_date($shoppings_details[0]->Shoppings->created_at, $fecha_actual = date('Y-m-d H:i:s'));
        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        return View::make('dashboard.compras.gestion_shoppings_edit')
                        ->with('statuses', $status)
                        ->with('time', $time)
                        ->with('categories', $categories)
                        ->with('technicals', $technical)
                        ->with('shopping_details', $shoppings_details);
    }

    //funcion para mostrar el modal del historico de una solicitud de compra
    public function getDetallehist() {
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();

        $technical = DB::table('users')
        ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
        ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
        ->select('users.id', 'users.name', 'users.last_name')
        ->where('profiles.processes_id', 7)
        ->where('users.status', 1)
        ->orderBy('name', 'asc')
        ->get();



        return View::make('dashboard.compras.gestion_shoppings_hist')
        ->with('statuses', $status)
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('shopping_details', $shoppings_details);
    }

    //funcion para mostrar el modal de historico en la vista del usuario
    public function getDetallehistorico() {
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();
        $time = $this->interval_date($shoppings_details[0]->Shoppings->created_at, $fecha_actual = date('Y-m-d H:i:s'));
        $technical = DB::table('users')
        ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
        ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
        ->select('users.id', 'users.name', 'users.last_name')
        ->where('profiles.processes_id', 7)
        ->where('users.status', 1)
        ->orderBy('name', 'asc')
        ->get();



        return View::make('dashboard.compras.gestion_shoppings_historico')
        ->with('statuses', $status)
        ->with('time', $time)
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('shopping_details', $shoppings_details);
    }

    //funcion para mostrar los comentarios en el chat de compras
    public function getCargarcomments() {

        $id = Input::get('id');

        $shopping_comments = ShoppingMessages::where('shoppings_id', $id)->get();

        // if (count($shopping_comments) == 0) {
        //     echo '<h2 style="text-align: center;">Inserte un comentario</h2>
        //                                                <img width="50px" src="../assets/img/cargando.gif" style="margin-left: auto; margin-right: auto; display:block; padding-top:10px;">';
        //     exit;
        // }

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();
        
        $technical = DB::table('users')
        ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
        ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
        ->select('users.id', 'users.name', 'users.last_name')
        ->where('profiles.processes_id', 7)
        ->where('users.status', 1)
        ->orderBy('name', 'asc')
        ->get();



        return View::make('dashboard.compras.comentario_compra')
        ->with('statuses', $status)
        
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('id', $id)
        ->with('shopping_comments', $shopping_comments);
    }
    //funcion para crear un nuevo mensaje en el chat de compras
    public function getNuevomsj() {
        $id_chat = Input::get('id');
        $my_id = Auth::user()->id;
        $you_id = "";
        
       

        

        
            $sql = "SELECT * FROM `shopping_messages` WHERE shoppings_id = '" . Input::get('id') . "' order by id desc limit 1";
            $messages = DB::select($sql);

            foreach ($messages as $message) {
                
                if ($my_id != $message->users_id) {
                    $you_id = $message->users_id;
                }else{
                    $you_id = $message->users_id1;
                }
            }
            if ($you_id == "") {
                $you_id = "194";
            }

            
            $msj = new ShoppingMessages;
            $msj->users_id = $my_id;
            $msj->users_id1 = $you_id;
            $msj->shoppings_id = Input::get('id');
            $msj->message = Input::get('mensaje');
            
            if($msj->save()){
                                
                $sql = "SELECT * FROM `shopping_messages` WHERE shoppings_id = '" . Input::get('id') . "' order by id desc limit 1";
                $messages = DB::select($sql);
                //return $messages;
                $arreglo = Array();
                $fecha = date("M d, Y H:i",strtotime($messages[0]->created_at));
                $arreglo[0] = $you_id;
                $arreglo[1] = $messages[0]->id;
                $arreglo[2] = $fecha;
                $arreglo[3] = $messages[0]->shoppings_id;
                $arreglo[4] = "qwe";
                $arreglo[5] = "asd";
                return $arreglo;
                
            }else{
                
                return 0;
                
            }
        
                                    
    }

    //funcion para mostrar el modal de solicitudes detalle en el hoistorico de compras
    public function getMisolicitudescomprasdetalle() {
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        $time = $this->interval_date($shoppings_details[0]->Shoppings->created_at, $fecha_actual = date('Y-m-d H:i:s'));

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();

        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', 7)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();



        return View::make('dashboard.compras.mis_solicitudes_detalle')
        ->with('statuses', $status)
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('time', $time)
        ->with('shopping_details', $shoppings_details);
    }

    //funcion para calcular el intervalo de timepo
    public function interval_date($init, $finish) {
        //formateamos las fechas a segundos tipo 1374998435
        $diferencia = strtotime($finish) - strtotime($init);

        //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
        //floor devuelve el número entero anterior, si es 5.7 devuelve 5
        if ($diferencia < 60) {
            $tiempo = floor($diferencia) . " Seg";
        } else if ($diferencia > 60 && $diferencia < 3600) {
            $tiempo = floor($diferencia / 60) . ' Min';
        } else if ($diferencia > 3600 && $diferencia < 86400) {
            $tiempo = floor($diferencia / 3600) . " Horas";
        } else if ($diferencia > 86400 && $diferencia < 2592000) {
            $tiempo = floor($diferencia / 86400) . " Días";
        } else if ($diferencia > 2592000 && $diferencia < 31104000) {
            $tiempo = floor($diferencia / 2592000) . " Meses";
        } else if ($diferencia > 31104000) {
            $tiempo = floor($diferencia / 31104000) . " Años";
        } else {
            $tiempo = "Error";
        }
        return $tiempo;
    }

    //funcion para calcular el tiempo transcurrido
    function calcula_time($init, $finish) {
        $diferencia = strtotime($finish) - strtotime($init);

        return $diferencia;
    }

    //funcion para gestionar una solicitud de compra
    public function postGestion() {
        
        $id     = Input::get('id_details');
        $status = Input::get('status');        

        $i = '0';
        foreach ($id as $key) {

            DB::table('shopping_details')
            ->where('id', $key)
            ->update(array('shopping_details_statuses_id' => $status[$i]));
            

            $i++;
        }


        $id_cot     = Input::get('id_cot');
        $status_cot = Input::get('status_cot');


        $i_cot = '0';
        foreach ($id_cot as $key_cot) {

            DB::table('shopping_quotations')
            ->where('id', $key_cot)
            ->update(array('status' => $status_cot[$i_cot]));
            

            
            $i_cot++;
        }

        $comment     = Input::get('comment');
        //$id_shopping     = Input::get('id_shopping');

        $id_shopping = e(Input::get('id_shopping'));
        
        $estado_gen = e(Input::get('estado_gen'));

        DB::update('update shoppings set shopping_statuses_id = ?  where id = ?', array($estado_gen, $id_shopping));


        $trace = new ShoppingTraces;

        $trace->comment = $comment;
        $trace->users_id = Auth::user()->id;
        $trace->shoppings_id = Input::get('id_shopping');
        $trace->status = $estado_gen;
        
        $trace->save();
                
        
        if(Input::get('estado_gen')==4){
        
            $user = Users::all();
            $submenu_required = 26;
            $bandera = 0;
            foreach ($user as $key) {
                foreach ($key->Profiles as $perfil) {
                    foreach ($perfil->submenus as $submenu) {
                        if ($submenu_required == $submenu->id) {
                            //echo $key->name." ".$key->last_name."<br>";
                            $notification = new Notifications;

                            $notification->description = 'Aprobo una solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $key->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'compras/gestion';

                            $notification->save();
                        }
                    }
                }
            }
        }else{
            
        $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.id = '".Input::get('id_shopping')."'";
        $user_not = DB::select($sql);
        
            $notification = new Notifications;

                            $notification->description = 'Rechazo su solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $user_not[0]->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'compras/historicocompras';

                            $notification->save();                                        
            
        }
        $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
            return $user_not;
       
    }

    //funcion para subir una cotizacion a una solicitud de compra
    public function postSubricotizacion() {
        
        $quotation = new ShoppingQuotation;


        
        
        $id_shopp       = "{$_POST['id_shopping']}";
        //$proveedor  = "{$_POST['nombre_pro']}";
        $id_proveedor   = "{$_POST['id_proveedor']}";
        $value          = "{$_POST['valor_cot']}";
        $recomendado    = "{$_POST['recomendado']}";
        $observation    = "{$_POST['observacion']}";
        $archivo        = "{$_POST['archivo']}";


        if ($archivo == 1) {

            $quotation->file         = "";
            
            $quotation->providers_id = $id_proveedor;
            $quotation->valor        = $value;
            $quotation->observacion  = $observation;
            $quotation->shoppings_id = $id_shopp;
            $quotation->recommended  = $recomendado;

            $quotation->save();

            
            exit;
        }


        $nombre = "Cot_".$id_shopp;
        $ruta = 'assets/files/cotizaciones/'; //Decalaramos una variable con la ruta en donde almacenaremos los archivos
        $mensage = '';//Declaramos una variable mensaje quue almacenara el resultado de las operaciones.
        foreach ($_FILES as $key) //Iteramos el arreglo de archivos
        {
            if($key['error'] == UPLOAD_ERR_OK )//Si el archivo se paso correctamente Ccontinuamos 
                {
                    $NombreOriginal = $nombre."_".utf8_decode($key['name']);//Obtenemos el nombre original del archivo
                    $temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
                    $Destino = $ruta.$NombreOriginal;   //Creamos una ruta de destino con la variable ruta y el nombre original del archivo 
                    
                    move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada

                    
                    $quotation->file         = $ruta.$nombre."_".$key['name'];
                    
                    $quotation->providers_id = $id_proveedor;
                    $quotation->valor        = $value;
                    $quotation->observacion  = $observation;
                    $quotation->shoppings_id = $id_shopp;
                    $quotation->recommended = $recomendado;

                    $quotation->save();
                }

            if ($key['error']=='') //Si no existio ningun error, retornamos un mensaje por cada archivo subido
                {
                    $mensage .= '';
                }
            if ($key['error']!='')//Si existio algún error retornamos un el error por cada archivo.
                {
                    $mensage .= '==> No se pudo subir el archivo <b>'.$NombreOriginal.'</b> debido al siguiente Error: \n'.$key['error']; 
                }
            
        }
        echo $mensage;// Regresamos los mensajes generados al cliente
    }

    //funcion para mostrar las cotizaciones subidas
    public function getMostrararchivos(){
        $id = Input::get('id_quotation');

        $cuotation = ShoppingQuotation::where("shoppings_id", $id)->get();
        $archivos = array();
        $classrow = "";
        
        foreach ($cuotation as $item) {
            if($item->recommended==1){
                $classrow = "green";
            }else{
                $classrow = "";
            }
            $style = "";
            if ($item->file == "") {
                $style = "display:none";
            }
            
            echo    '<tr class="'.$classrow.'">
                         <td>COT'.str_pad($item->id, 6, "0", STR_PAD_LEFT).'</td>
                         <td>'.$item->Providers->provider.'</td>
                         <td>'.$item->Providers->nit.'</td>
                         <td>$ '.$item->valor.'</td>
                         <td>'.$item->observacion.'</td>
                         <td><a class="btn btn-default" style="'.$style.'" id="" href="../'.$item->file.'" target="_blanc"><i class="icon-paper-clip"></i></a></td>
                         <td><a class="btn btn-danger" id="" ><i class="icon-trash"></i></a></td>
                    </tr>';   
            
        }
       
        //$directorio_escaneado = scandir('assets/files/cotizaciones/');
        //$archivos = array();
        // foreach ($directorio_escaneado as $item) {
        //     if ($item != '.' and $item != '..') {
        //         $archivos[] = $item;
        //     }
        // }
    }
    
    //funcion para subir una factura a una solicitud de compra
    public function postSubirfactura() {
        
         
        $id              = e(Input::get('id_shopping'));
        $id_cuotation    = e(Input::get('id_cuotation'));
        $comment_aproved = e(Input::get('comment_aproved'));
        $fecha_entrega   = e(Input::get('fecha_entrega'));
        $precio_final    = e(Input::get('precio_final'));
        $n_factura       = e(Input::get('no_factura'));
        $fecha_pago      = e(Input::get('fecha_pago'));
        $forma_pago      = e(Input::get('forma_pago'));
        $providers_id    = e(Input::get('provider_id'));        

        DB::update('update shoppings set shopping_statuses_id = 6, comment_aproved = ?, date_delivery = ?  where id = ?', array($comment_aproved, $fecha_entrega, $id));
            
            
        $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.id = '".Input::get('id_shopping')."'";
        $user_not = DB::select($sql);
        
                       
        $ruta = 'assets/files/facturas/'; //Declaramos una variable con la ruta en donde almacenaremos los archivos
        $mensage = '';//Declaramos una variable mensaje quue almacenara el resultado de las operaciones.
        foreach ($_FILES as $key) //Iteramos el arreglo de archivos
        {
            if($key['error'] == UPLOAD_ERR_OK )//Si el archivo se paso correctamente Ccontinuamos 
                {                    
                    $NombreOriginal = $id."_factura"."_".utf8_decode($key['name']);//Obtenemos el nombre original del archivo
                    $temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
                    $Destino = $ruta.$NombreOriginal;   //Creamos una ruta de destino con la variable ruta y el nombre original del archivo 
                    
                    move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada

                    $new_payment = new Payments;
                    
                    $new_payment->description = 'Pago de solicitud de compra';
                    $new_payment->value = $precio_final;
                    $new_payment->file = $Destino;
                    $new_payment->n_factura = $n_factura;
                    $new_payment->payment_date = $fecha_pago;
                    $new_payment->payments_statuses_id = 1;
                    $new_payment->payments_concepts_id = 1;
                    $new_payment->users_id = $user_not[0]->id;
                    $new_payment->shopping_quotation_id = $id_cuotation;
                    $new_payment->providers_id = $providers_id;
                    $new_payment->payments_methods_id = $forma_pago;

                    $new_payment->save();
                    
                }

            if ($key['error']=='') //Si no existio ningun error, retornamos un mensaje por cada archivo subido
                {
                    $mensage .= '';
                }
            if ($key['error']!='')//Si existio algún error retornamos un el error por cada archivo.
                {
                    $mensage .= '==> No se pudo subir el archivo <b>'.$NombreOriginal.'</b> debido al siguiente Error: \n'.$key['error']; 
                }
            
        }
        //echo $mensage;// Regresamos los mensajes generados al cliente
        
            $notification = new Notifications;

                            $notification->description = 'Agrego una fecha de entrega para su solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $user_not[0]->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'compras/misolicitudescompras';

                            $notification->save();

        $user = Users::all();
        $submenu_required = 27;
        $bandera = 0;
        foreach ($user as $key) {
            foreach ($key->Profiles as $perfil) {
                foreach ($perfil->submenus as $submenu) {
                    if ($submenu_required == $submenu->id) {
                        //echo $key->name." ".$key->last_name."<br>";
                        $notification = new Notifications;

                        $notification->description = 'Creo una nueva solicitud de pago';
                        $notification->estate = '1';
                        $notification->type_notifications_id = '3';
                        $notification->user_id = $key->id;
                        $notification->user_id1 = Auth::user()->id;
                        $notification->link = 'pagos/aprobar';

                        $notification->save();
                    }
                }
            }
        }
                            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
            return $user_not;                                                               
                       
    }

    //funcion para mostrar el historico de compras
    public function getComprashistorico(){
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();
        $time = $this->interval_date($shoppings_details[0]->Shoppings->created_at, $fecha_actual = date('Y-m-d H:i:s'));
        $technical = DB::table('users')
        ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
        ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
        ->select('users.id', 'users.name', 'users.last_name')
        ->where('profiles.processes_id', 7)
        ->where('users.status', 1)
        ->orderBy('name', 'asc')
        ->get();



        return View::make('dashboard.compras.admin_shoppings_edit')
        ->with('statuses', $status)
        ->with('time', $time)
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('shopping_details', $shoppings_details);
       
        //$directorio_escaneado = scandir('assets/files/cotizaciones/');
        //$archivos = array();
        // foreach ($directorio_escaneado as $item) {
        //     if ($item != '.' and $item != '..') {
        //         $archivos[] = $item;
        //     }
        // }
    }

    //funcion para cambiar el estado a una solicitud de compras compras
    public function getTerminarproceso(){
        //$id = Input::get('id_shopping');

        if (isset($_GET['comment_aproved'])) {
            $id                 = e(Input::get('id_shopping'));
            $comment_aproved    = e(Input::get('comment_aproved'));
            $fecha_entrega      = e(Input::get('fecha_entrega'));

            DB::update('update shoppings set shopping_statuses_id = 6, comment_aproved = ?, date_delivery = ?  where id = ?', array($comment_aproved, $fecha_entrega, $id));
            
            
        $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.id = '".Input::get('id_shopping')."'";
        $user_not = DB::select($sql);




        $ruta = 'assets/files/facturas/';
        foreach ($_FILES as $key) //Iteramos el arreglo de archivos
        {
            if($key['error'] == UPLOAD_ERR_OK )//Si el archivo se paso correctamente Ccontinuamos 
                {
                    $NombreOriginal = "factura"."_".utf8_decode($key['name']);//Obtenemos el nombre original del archivo
                    $temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
                    $Destino = $ruta.$NombreOriginal;   //Creamos una ruta de destino con la variable ruta y el nombre original del archivo 
                    
                    move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada


                    $new_payment = new Payments;
                    
                    $new_payment->concept = 'Pago de solicitud de compra';
                    $new_payment->value = '';
                    $new_payment->file = $ruta.$Destino."_".$key['name'];

                    $new_payment->save();


                    
                    // $quotation->file         = $ruta.$nombre."_".$key['name'];
                    
                    // $quotation->providers_id = $id_proveedor;
                    // $quotation->valor        = $value;
                    // $quotation->observacion  = $observation;
                    // $quotation->shoppings_id = $id_shopp;

                    // $quotation->save();
                }

            if ($key['error']=='') //Si no existio ningun error, retornamos un mensaje por cada archivo subido
                {
                    $mensage .= '';
                }
            if ($key['error']!='')//Si existio algún error retornamos un el error por cada archivo.
                {
                    $mensage .= '==> No se pudo subir el archivo <b>'.$NombreOriginal.'</b> debido al siguiente Error: \n'.$key['error']; 
                }
            
        }
        echo $mensage;// Regresamos los mensajes generados al cliente





        
            $notification = new Notifications;

                            $notification->description = 'Agrego una fecha de entrega para su solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $user_not[0]->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'compras/misolicitudescompras';

                            $notification->save();
                            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
            return $user_not;
                                    
        }elseif(isset($_GET['comentario_general'])){
            
            $id = e(Input::get('id_shopping'));
            $status = e(Input::get('status'));
            $comentario_general = Input::get('comentario_general');

            DB::update('update shoppings set shopping_statuses_id = ?  where id = ?', array($status, $id));

            $trace = new ShoppingTraces;

            $trace->comment = $comentario_general;
            $trace->users_id = Auth::user()->id;
            $trace->shoppings_id = Input::get('id_shopping');
            $trace->status = $status;
            
            $trace->save();
            
        $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.id = '".Input::get('id_shopping')."'";
        $user_not = DB::select($sql);
            
        if(Input::get('status')==3){
        
            $user = Users::all();
            $submenu_required = 27;
            $bandera = 0;
            foreach ($user as $key) {
                foreach ($key->Profiles as $perfil) {
                    foreach ($perfil->submenus as $submenu) {
                        if ($submenu_required == $submenu->id) {
                            //echo $key->name." ".$key->last_name."<br>";
                            $notification = new Notifications;

                            $notification->description = 'Creo una nueva solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $key->id;
                            $notification->user_id1 = $user_not[0]->id;
                            $notification->link = 'compras/administrar';

                            $notification->save();
                        }
                    }
                }
            }
        }else{
        
            $notification = new Notifications;

                            $notification->description = 'Rechazo su solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $user_not[0]->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'compras/historicocompras';

                            $notification->save();
                            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
        }
            
            return $user_not;
            
        }else{

            $id = e(Input::get('id_shopping'));

            DB::update('update shoppings set shopping_statuses_id = 3  where id = ?', array($id));
           
            echo "ok";
        }

    }
    //funcion para mostrar la vista de administrar compras
    public function getAprobarsol(){
        //$id = Input::get('id_shopping');

        $id = e(Input::get('id_shopping'));
        $estado = e(Input::get('estado'));
        $comentario = Input::get('comentario');

        DB::update('update shoppings set shopping_statuses_id = ? where id = ?', array($estado,$id));
        
        $trace = new ShoppingTraces;

        $trace->comment = $comentario;
        $trace->users_id = Auth::user()->id;
        $trace->shoppings_id = Input::get('id_shopping');
        $trace->status = $estado;
        
        
        $trace->save();
        
        $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users JOIN shoppings ON (users.id = shoppings.users_id) WHERE shoppings.id = '".Input::get('id_shopping')."'";
        $user_not = DB::select($sql);
            
        if(Input::get('estado')==2){
        
            $user = Users::all();
            $submenu_required = 26;
            $bandera = 0;
            foreach ($user as $key) {
                foreach ($key->Profiles as $perfil) {
                    foreach ($perfil->submenus as $submenu) {
                        if ($submenu_required == $submenu->id) {
                            //echo $key->name." ".$key->last_name."<br>";
                            $notification = new Notifications;

                            $notification->description = 'Creo una nueva solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $key->id;
                            $notification->user_id1 = $user_not[0]->id;
                            $notification->link = 'compras/gestion';

                            $notification->save();
                        }
                    }
                }
            }
        }else{
        
            $notification = new Notifications;

                            $notification->description = 'Rechazo su solicitud de compra';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '2';
                            $notification->user_id = $user_not[0]->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'compras/historicocompras';

                            $notification->save();
                            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
        }
       
        return $user_not;
    }
    //funcion para eliminar una cotizacion
    public function getDeletecuotation(){
        $id = Input::get('cuotation');

        $cuotation = ShoppingQuotation::find($id);

        


        if ($cuotation->delete()) {
            echo "ok";
        }
    }
    //funcion para aprobar una solicitud de compra por parte del lider de proceso
    public function getAprobar() {
        $my_id = Auth::user()->id;

        $my_profile = Users::find($my_id);
        foreach ($my_profile->Profiles as $perfil) {
            $my_process = $perfil->Processes->id;
        }

        $permission = $this->permission_control("30");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

       $sql = "SELECT shoppings.id, shopping_statuses.name as estado, shopping_statuses.description as description, shoppings.users_id,
        shoppings.shopping_statuses_id, shoppings.justification, shoppings.comment, shoppings.created_at, processes.name AS proceso,
        users.name, users.last_name FROM shoppings JOIN profiles_users ON (shoppings.users_id = profiles_users.users_id)
        JOIN shopping_statuses ON (shopping_statuses.id = shoppings.shopping_statuses_id)
        JOIN profiles ON (profiles_users.profiles_id = profiles.id)
        JOIN processes ON (processes.id = profiles.processes_id) JOIN users ON (users.id = shoppings.users_id) 
        WHERE shoppings.shopping_statuses_id = 1 AND processes.id='".$my_process."' ORDER BY shoppings.id ASC";
       $compras = DB::select($sql);

        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $compras[$i]->name . " " . $compras[$i]->last_name;

            

            $data[$i]["state"] = $compras[$i]->estado;
            $data[$i]["description"] = $compras[$i]->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['profil'] = $compras[$i]->proceso;
                
            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';

        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.compras.table_aprobar_compras')
            ->with('shoppings', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.aprobar')
            ->with('shoppings', $data)
            ->with('my_process', $my_process)
               
            ->with('submenu_activo', 'Aprobar Solicitudes')
            ->with('menu_activo', 'Compras');
        }

        

        
        // return View::make('dashboard.index')
        // ->with('container', 'dashboard.compras.admin_compras')
        // ->with('shopping', $compras)
           
        // ->with('submenu_activo', 'Gestionar Solicitudes')
        // ->with('menu_activo', 'Compras');
    }
    //funcion para aprobar un historico???????
    public function getAprobarhist() {
        $my_id = Auth::user()->id;

        $my_profile = Users::find($my_id);
        foreach ($my_profile->Profiles as $perfil) {
            $my_process = $perfil->Processes->id;
        }

        $permission = $this->permission_control("30");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }


        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        $compras = Shoppings::join('profiles_users', 'shoppings.users_id', '=', 'profiles_users.users_id')
            ->join('profiles', 'profiles_users.profiles_id', '=', 'profiles.id')
            ->join('processes', 'processes.id', '=', 'profiles.processes_id')
            ->select(
                    'shoppings.id',
                    'shoppings.users_id',
                    'shopping_statuses_id',
                    'shoppings.justification',
                    'shoppings.comment',
                    'shoppings.created_at',
                    'processes.name AS proceso'
                )

            ->where('shoppings.id', 'LIKE', '%' . $codigo . '%')
            ->where('shoppings.users_id', $var, $signo)
            ->where('shopping_statuses_id', 'LIKE', '%' . $status . '%')
            ->where('shopping_statuses_id', '<>', '1')
            ->where('processes.id', $my_process)
            ->orderBy('shoppings.id', 'desc')
            ->paginate(15);

        $applicants = Users::all();
        $statuses = ShoppingStatuses::all();
        //$supports = Supports::orderBy('id', 'desc')->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['profil'] = $compras[$i]->proceso;
            $data[$i]['row_color'] = '';

            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4){
                $data[$i]['label'] = 'label-success';
                $data[$i]['row_color'] = 'green';

            }
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';
            else if ($compras[$i]->shopping_statuses_id == 6)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 7)
                $data[$i]['label'] = 'label-success';

        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.helpDesk.table_admin_supports')
            ->with('supports', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.aprobarhist')
            ->with('shoppings', $data)
            ->with('my_process', $my_process)
            ->with('applicants', $applicants)
            ->with('statuses', $statuses)
            ->with('pag', $compras)
            ->with('submenu_activo', 'Aprobar Solicitudes')
            ->with('menu_activo', 'Compras');
        }

        

        
        // return View::make('dashboard.index')
        // ->with('container', 'dashboard.compras.admin_compras')
        // ->with('shopping', $compras)
           
        // ->with('submenu_activo', 'Gestionar Solicitudes')
        // ->with('menu_activo', 'Compras');
    }

    //funcion para detalle de parobar ?????
    public function getDetalleaprobar() {
        $id = Input::get('id');

        $shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();
        $time = $this->interval_date($shoppings_details[0]->Shoppings->created_at, $fecha_actual = date('Y-m-d H:i:s'));
        //$data = Supports::find($id);
        $status = ShoppingDetailsStatuses::get();
        $categories = SupportCategory::where('status', 1)->get();

        $technical = DB::table('users')
        ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
        ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
        ->select('users.id', 'users.name', 'users.last_name')
        ->where('profiles.processes_id', 7)
        ->where('users.status', 1)
        ->orderBy('name', 'asc')
        ->get();



        return View::make('dashboard.compras.detalle_aprobar')
        ->with('statuses', $status)
        ->with('time', $time)
        ->with('categories', $categories)
        ->with('technicals', $technical)
        ->with('shopping_details', $shoppings_details);
    }

    public function getStatistics() {
        $permission = $this->permission_control("31");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }
        
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.compras.statistics')
        
        ->with('submenu_activo', 'Estadísticas')
        ->with('menu_activo', 'Compras');
        
    }
    
    
    public function getAprobarpagos(){
        $permission = $this->permission_control("27");
        if ($permission == 0) {
            return View::make('dashboard.index')
            ->with('container', 'errors.access_denied_ad')
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Help Desk');
        }

        $codigo = Input::get('code');
        $applicant = Input::get('applicant');
        $responsible = Input::get('responsible');
        $status = Input::get('status');
        $priority = Input::get('priority');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        $compras = Shoppings::where('shopping_statuses_id', 3)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('users_id', $var, $signo)
        ->orderBy('id', 'desc')
        ->paginate(15);


        $applicants = Users::all();
        
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($compras); $i++) {
            $fecha = date($compras[$i]->created_at);


            $data[$i]["id"] = 'SC' . str_pad($compras[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["cid"] = $compras[$i]->id;
            $data[$i]["user"] = $compras[$i]->Users->name . " " . $compras[$i]->Users->last_name;

            

            $data[$i]["state"] = $compras[$i]->ShoppingStatuses->name;
            $data[$i]["description"] = $compras[$i]->ShoppingStatuses->description;
            $data[$i]["justification"] = $compras[$i]->justification;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["hid"] = $compras[$i]->id;
            

            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            
            foreach ($compras[$i]->Users->Profiles as $perfil) {
                $data[$i]['profil'] = $perfil->Processes->name;
            }
            $data[$i]['row_color'] = '';
            if ($compras[$i]->shopping_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($compras[$i]->shopping_statuses_id == 2)
                $data[$i]['label'] = 'label-default';
            else if ($compras[$i]->shopping_statuses_id == 3)
                $data[$i]['label'] = 'label-info';
            else if ($compras[$i]->shopping_statuses_id == 4){
                $data[$i]['label'] = 'label-success';
                $data[$i]['row_color'] = 'green';
            }
            else if ($compras[$i]->shopping_statuses_id == 5)
                $data[$i]['label'] = 'label-danger';
        }


        if (isset($_GET['nombre'])) {
            return View::make('dashboard.compras.table_admin_compras')
            ->with('shoppings', $data);
        } else {
            return View::make('dashboard.index')
            ->with('container', 'dashboard.compras.aprobar_pagos')
            ->with('shoppings', $data)
            ->with('applicants', $applicants)
            ->with('pag', $compras)
            ->with('submenu_activo', 'Administrar Solicitudes')
            ->with('menu_activo', 'Compras');
        }
        
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphicstatuses() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT shopping_statuses_id, shopping_statuses.name, COUNT( * )  as dato
                FROM shoppings
                join shopping_statuses on (shopping_statuses.id = shoppings.shopping_statuses_id)
                GROUP BY shopping_statuses_id
                ORDER BY dato DESC";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->name;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de los estados de las compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function getChangedatestatuses() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT shopping_statuses_id, shopping_statuses.name, COUNT( * )  as dato
                FROM shoppings
                join shopping_statuses on (shopping_statuses.id = shoppings.shopping_statuses_id)
                WHERE shoppings.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
                GROUP BY shopping_statuses_id
                ORDER BY dato DESC";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de los estados de las compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarstatuses() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT shopping_statuses_id, shopping_statuses.name, COUNT( * )  as dato
                FROM shoppings
                join shopping_statuses on (shopping_statuses.id = shoppings.shopping_statuses_id)
                WHERE shoppings.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                GROUP BY shopping_statuses_id
                ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT shoppings.justification, users.name, users.last_name, shoppings.created_at, shopping_statuses.name as estado
                FROM shoppings
                join shopping_statuses on (shopping_statuses.id = shoppings.shopping_statuses_id)
                join users on (users.id = shoppings.users_id)
                WHERE shoppings.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                GROUP BY shopping_statuses_id
                ORDER BY shoppings.id DESC";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Estados de las compras")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Estado')
                ->setCellValue('B1', 'Numero');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->name)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Estados');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->justification)
                    ->setCellValue('B' . $j, $detalle[$i]->estado)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphicusers() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT shoppings.users_id, processes.acronyms as name, COUNT( * ) AS dato 
        FROM shoppings JOIN users ON ( users.id = shoppings.users_id ) 
        join profiles_users on (users.id = profiles_users.users_id) 
        join profiles on (profiles.id = profiles_users.profiles_id) 
        join processes on (processes.id = profiles.processes_id)
        GROUP BY name
        ORDER BY dato DESC";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->name;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte del numero de compras por proceso");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function getChangedateusers() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT shoppings.users_id, processes.acronyms as name, COUNT( * ) AS dato 
        FROM shoppings JOIN users ON ( users.id = shoppings.users_id ) 
        join profiles_users on (users.id = profiles_users.users_id) 
        join profiles on (profiles.id = profiles_users.profiles_id) 
        join processes on (processes.id = profiles.processes_id)
        WHERE shoppings.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
        GROUP BY name
        ORDER BY dato DESC";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte del numero de compras por proceso");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarusers() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphicmoneyprocess() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT sum(payments.net_pay) as valortotal, processes.acronyms
				FROM payments
				JOIN users ON (payments.users_id = users.id)
                                JOIN profiles_users ON (profiles_users.users_id = users.id)
                                JOIN profiles ON (profiles_users.profiles_id = profiles.id)
                                JOIN processes ON (processes.id = profiles.processes_id)
                                WHERE payments.payments_statuses_id != 'NULL'
				GROUP BY processes_id
				ORDER BY valortotal DESC
                                LIMIT 0 , 10";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->valortotal;
            $datos1_2[] = $key->acronyms;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de dinero de compras por proceso");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function getChangedatemoneyprocess() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT sum(payments.net_pay) as valortotal, processes.acronyms
				FROM payments
				JOIN users ON (payments.users_id = users.id)
                                JOIN profiles_users ON (profiles_users.users_id = users.id)
                                JOIN profiles ON (profiles_users.profiles_id = profiles.id)
                                JOIN processes ON (processes.id = profiles.processes_id)
                                WHERE payments.payments_statuses_id != 'NULL'
                                AND payments.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY processes_id
				ORDER BY valortotal DESC
                                LIMIT 0 , 10";
        

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->valortotal;
                $datos1_2[] = $key->acronyms;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de procesos con mas compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarmoneyprocess() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphicproviders() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT providers.provider, providers.id, COUNT(*) AS dato
				FROM providers
				JOIN payments ON (payments.providers_id = providers.id)
                                WHERE payments.payments_statuses_id = '6'
                GROUP BY providers_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->provider;
        }

        $grafico1 = new Graph(600, 600);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de proveedores con mas compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function getChangedateproviders() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT providers.provider, providers.id, COUNT(*) AS dato
				FROM providers
				JOIN payments ON (payments.providers_id = providers.id)
                                WHERE payments.payments_statuses_id = '6'
                                AND payments.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
                                GROUP BY providers_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->provider;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de proveedores con mas compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarproviders() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphiccategories() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT shopping_categories.name, COUNT(*) AS dato
				FROM shopping_categories
				JOIN shopping_details ON (shopping_details.shopping_categories_id = shopping_categories.id)
                                JOIN shoppings ON (shopping_details.shoppings_id = shoppings.id)
                                JOIN shopping_quotations ON (shopping_quotations.shoppings_id = shoppings.id)
                                JOIN payments ON (shopping_quotations.id = payments.shopping_quotation_id)
                                WHERE payments.payments_statuses_id = '6'
                                GROUP BY shopping_categories.id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->name;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de articulos con mas compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function getChangedatecategories() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");
        
        $sql = "SELECT shopping_categories.name, COUNT(*) AS dato
				FROM shopping_categories
				JOIN shopping_details ON (shopping_details.shopping_categories_id = shopping_categories.id)
                                JOIN shoppings ON (shopping_details.shoppings_id = shoppings.id)
                                JOIN shopping_quotations ON (shopping_quotations.shoppings_id = shoppings.id)
                                JOIN payments ON (shopping_quotations.id = payments.shopping_quotation_id)
                                WHERE payments.payments_statuses_id = '6'
                                AND shoppings.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
                                GROUP BY shopping_categories.id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de articulos con mas compras");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarcategories() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphicporcentaje() {
        
        
        require_once ("jpgraph/src/jpgraph.php");
        require_once ("jpgraph/src/jpgraph_pie.php");
        
        $sql = "SELECT COUNT(*) AS total, ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =6 OR shopping_statuses_id =4  ) AS aprobadas, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =5 ) AS rechazadas, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =1 ) AS ingresadas, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =2 ) AS compras, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =3 ) AS pendientes
				FROM shoppings
                LIMIT 0 , 10";
        
        $cons_grafico = DB::select($sql);
        
        $aprobadas = $cons_grafico[0]->aprobadas * 100 / $cons_grafico[0]->total;
        $rechazadas = $cons_grafico[0]->rechazadas * 100 / $cons_grafico[0]->total;
        $ingresadas = $cons_grafico[0]->ingresadas * 100 / $cons_grafico[0]->total;
        $compras = $cons_grafico[0]->compras * 100 / $cons_grafico[0]->total;
        $pendientes = $cons_grafico[0]->pendientes * 100 / $cons_grafico[0]->total;

        // Se define el array de valores y el array de la leyenda
        $datos = array($aprobadas,$rechazadas, $ingresadas, $compras, $pendientes);
        $leyenda = array("Aprobadas","Rechazadas", "Ingresadas", "En compras", "Pendientes");

        //Se define el grafico
        $grafico = new PieGraph(450,450);

        //Definimos el titulo
        $grafico->title->Set("Reporte de porcentaje de estados");
        $grafico->title->SetFont(FF_FONT1,FS_BOLD);

        //Añadimos el titulo y la leyenda
        $p1 = new PiePlot($datos);
        $p1->SetLegends($leyenda);
        $p1->SetCenter(0.4);
        
        //Se muestra el grafico
        $grafico->Add($p1);
        $grafico->Stroke();

    }
    
    public function getChangedateporcentaje() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        require_once ("jpgraph/src/jpgraph.php");
        require_once ("jpgraph/src/jpgraph_pie.php");
        
        $sql = "SELECT COUNT(*) AS total, ( SELECT COUNT( id ) FROM shoppings WHERE (shopping_statuses_id =6 OR shopping_statuses_id =4)  AND (created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "')) AS aprobadas, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =5 AND (created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "')) AS rechazadas, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =1 AND (created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "')) AS ingresadas, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =2 AND (created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "')) AS compras, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE shopping_statuses_id =3 AND (created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "')) AS pendientes
				FROM shoppings
                                WHERE created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
                LIMIT 0 , 10";                
        
        $cons_grafico = DB::select($sql);

        if ($cons_grafico[0]->total > 0) {
                $aprobadas = $cons_grafico[0]->aprobadas * 100 / $cons_grafico[0]->total;
                $rechazadas = $cons_grafico[0]->rechazadas * 100 / $cons_grafico[0]->total;
                $ingresadas = $cons_grafico[0]->ingresadas * 100 / $cons_grafico[0]->total;
                $compras = $cons_grafico[0]->compras * 100 / $cons_grafico[0]->total;
                $pendientes = $cons_grafico[0]->pendientes * 100 / $cons_grafico[0]->total;

                // Se define el array de valores y el array de la leyenda
                $datos = array($aprobadas,$rechazadas, $ingresadas, $compras, $pendientes);
                $leyenda = array("Aprobadas","Rechazadas", "Ingresadas", "En compras", "Pendientes");
        } else {
            // Se define el array de valores y el array de la leyenda
            $datos = array(100);
            $leyenda = array("No hay datos");
        }
                

        //Se define el grafico
        $grafico = new PieGraph(450,450);

        //Definimos el titulo
        $grafico->title->Set("Reporte de porcentaje de estados");
        $grafico->title->SetFont(FF_FONT1,FS_BOLD);

        //Añadimos el titulo y la leyenda
        $p1 = new PiePlot($datos);
        $p1->SetLegends($leyenda);
        $p1->SetCenter(0.4);
        
        //Se muestra el grafico
        $grafico->Add($p1);
        $grafico->Stroke();
                
    }

    public function getExportarporcentaje() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function getGraphicmonth() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT COUNT(*) AS total, ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-1-1' AND '2015-1-31' ) AS enero, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-2-1' AND '2015-2-28' ) AS febrero, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-3-1' AND '2015-3-31' ) AS marzo, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-4-1' AND '2015-4-30' ) AS abril, 
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-5-1' AND '2015-5-31' ) AS mayo,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-6-1' AND '2015-6-30' ) AS junio,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-7-1' AND '2015-7-31' ) AS julio,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-8-1' AND '2015-8-31' ) AS agosto,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-9-1' AND '2015-9-30' ) AS septiembre,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-10-1' AND '2015-10-31' ) AS octubre,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-11-1' AND '2015-11-30' ) AS noviembre,
                                          ( SELECT COUNT( id ) FROM shoppings WHERE created_at BETWEEN '2015-12-1' AND '2015-12-31' ) AS diciembre
				FROM shoppings
                LIMIT 0 , 12";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        
        $datos1[0] = $cons_grafico[0]->enero;
        $datos1_2[0] = "Enero";
        $datos1[1] = $cons_grafico[0]->febrero;
        $datos1_2[1] = "Febrero";
        $datos1[2] = $cons_grafico[0]->marzo;
        $datos1_2[2] = "Marzo";
        $datos1[3] = $cons_grafico[0]->abril;
        $datos1_2[3] = "Abril";
        $datos1[4] = $cons_grafico[0]->mayo;
        $datos1_2[4] = "Mayo";
        $datos1[5] = $cons_grafico[0]->junio;
        $datos1_2[5] = "Junio";
        $datos1[6] = $cons_grafico[0]->julio;
        $datos1_2[6] = "Julio";
        $datos1[7] = $cons_grafico[0]->agosto;
        $datos1_2[7] = "Agosto";
        $datos1[8] = $cons_grafico[0]->septiembre;
        $datos1_2[8] = "Septiembre";
        $datos1[9] = $cons_grafico[0]->octubre;
        $datos1_2[9] = "Octubre";
        $datos1[10] = $cons_grafico[0]->noviembre;
        $datos1_2[10] = "Noviembre";
        $datos1[11] = $cons_grafico[0]->diciembre;
        $datos1_2[11] = "Diciembre";

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de compras por mes");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarmonth() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}

?>
