<?php

/**
 * Controlador Intranet	
 */
class BlogpublicController extends BaseController {

    public function __construct() {
        //$this->beforeFilter('auth'); //bloqueo de acceso
    }
    
    public function formatoFecha($fecha) {

        $transf = strtotime($fecha);
        $dia = date("d", $transf);
        $mes = date("F", $transf);
        if ($mes == "January")
            $mes = "Enero";
        if ($mes == "February")
            $mes = "Febrero";
        if ($mes == "March")
            $mes = "Marzo";
        if ($mes == "April")
            $mes = "Abril";
        if ($mes == "May")
            $mes = "Mayo";
        if ($mes == "June")
            $mes = "Junio";
        if ($mes == "July")
            $mes = "Julio";
        if ($mes == "August")
            $mes = "Agosto";
        if ($mes == "September")
            $mes = "Setiembre";
        if ($mes == "October")
            $mes = "Octubre";
        if ($mes == "November")
            $mes = "Noviembre";
        if ($mes == "December")
            $mes = "Diciembre";
        $ano = date("Y", $transf);

        return $dia . " de " . $mes . " de " . $ano;
    }

    public function getBlog() {
        
        if(Auth::user()){
            $url = "http://192.168.0.18/indo/public/blog/blog";
            return Redirect::away($url);
        }

        $blogs = Blogs::where('public', 1)->orderby('id', 'DESC')->paginate(5);
        $sql = "SELECT DISTINCT blog_tags.tag FROM blog_tags
                JOIN blogs ON (blogs.id = blog_tags.blogs_id)
               WHERE blogs.public = '1'";
        $tags_general = DB::select($sql);
        
        $sql = "SELECT * FROM blogs WHERE public = '1' ORDER BY views DESC limit 5";

        $tops = DB::select($sql);
        
        //construimos la vista
        return View::make('dashboard.blogpublic.blog')
                        ->with('blogs', $blogs)
                        ->with('pag', $blogs)
                        ->with('tops', $tops)
                        ->with('tags_general', $tags_general);
    }
    
    public function getDetallepost() {
        
        $id = Input::get('id');
        
        if(Auth::user()){
            $url = "http://192.168.0.18/indo/public/blog/detallepost?id=".$id;
            return Redirect::away($url);
        }
        
        //$blog = Blogs::find($id);
//        $blog = Blogs::where('public', 1)
//                       ->where('id', $id)
//                       ->get();
        $sql = "SELECT blogs.id, blogs.title, blogs.description, blogs.article, blogs.image_1, blogs.image_2, blogs.users_id, blogs.views, blogs.public, blogs.created_at
                FROM blogs WHERE blogs.id = '".$id."' AND blogs.public = 1";
        $blog = DB::select($sql);
        if(!$blog){
            $url = "http://192.168.0.18/indo/public/blog";
            return Redirect::away($url);
        }
        $sql = "SELECT DISTINCT blog_tags.tag FROM blog_tags
                JOIN blogs ON (blogs.id = blog_tags.blogs_id)
               WHERE blogs.public = '1'";
        $tags_general = DB::select($sql);
        $tags = BlogTags::where('blogs_id', $id)->get();
        $comentarios = BlogComments::where('blogs_id', $id)->get();
        $sql = "SELECT * FROM blogs WHERE public = '1' ORDER BY views DESC limit 5";
        $tops = DB::select($sql);
        
        $views = $blog[0]->views+1;
        
        DB::table('blogs')->where('id', $id)->update(array('views' => $views));
       
        return View::make('dashboard.blogpublic.detalle_blog')
                        ->with('blog', $blog)
                        ->with('tags', $tags)
                        ->with('tops', $tops)
                        ->with('tags_general', $tags_general)
                        ->with('comentarios', $comentarios);
    }
    
    public function getTag() {

        $tag = Input::get('tag');
        
        $sql = "SELECT blogs.id,blogs.title,blogs.description,blogs.image_1,blogs.created_at,blogs.users_id FROM blogs
                JOIN blog_tags ON (blogs.id = blog_tags.blogs_id)
               WHERE blog_tags.tag LIKE '%".$tag."%' AND blogs.public = '1'";
        $blogs = DB::select($sql);
        
        $blogs = DB::table('blogs')
                ->join('blog_tags', 'blogs.id', '=', 'blog_tags.blogs_id')
                ->select('blogs.id', 'blogs.title', 'blogs.description', 'blogs.image_1', 'blogs.users_id', 'blogs.created_at')
                ->where('blog_tags.tag','LIKE', '%'.$tag.'%')
                ->where('blogs.public', '1')
                ->orderby('id', 'DESC')
                ->paginate(5);
        
        $sql = "SELECT DISTINCT blog_tags.tag FROM blog_tags
                JOIN blogs ON (blogs.id = blog_tags.blogs_id)
               WHERE blogs.public = '1'";
        $tags_general = DB::select($sql);
        
        $sql = "SELECT * FROM blogs WHERE public = '1' ORDER BY views DESC limit 5";
        $tops = DB::select($sql);
       
        return View::make('dashboard.blogpublic.blog')
                        ->with('blogs', $blogs)
                        ->with('pag', $blogs)
                        ->with('tops', $tops)
                        ->with('tags_general', $tags_general);
    }
    
    public function getGuardarcomentario() {

        $comentario_user = Input::get('comentario_user');
        $id = Input::get('id');

                                $comment = new BlogComments;
                                $comment->comment = $comentario_user;
                                $comment->blogs_id = $id;
                                $comment->users_id = Auth::user()->id;
                                $comment->save();
                                
                echo '<div class="media">
                        <a href="#" class="pull-left">
                        <img alt="" src="../'.Auth::user()->img_min.'" class="media-object">
                        </a>
                        <div class="media-body">
                           <h4 class="media-heading">'.Auth::user()->name.' '.Auth::user()->last_name.'<span>Hace un momento</span></h4>
                           <p>'.$comentario_user.'</p>
                        </div>
                     </div>';

    }


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
?>