<?php 	
/**
* 	
*/
class PqrsController extends BaseController
{
	public function __construct()
	{
		$this->beforeFilter('auth'); //bloqueo de acceso
	}

	public function gestion(){
		$requests = Pqrs::where('state_requests_id', '==', 1 )->get();
		return View::make('dashboard.index')
		
		->with('requests',$requests)
		->with('container', 'dashboard.pqrs.gestion')
		->with('menu_activo', 'pqrs');		
	}
	public function getMypqrs()	{	
		$my_id = Auth::user()->id;

		
		$TypeRequests 	= DB::table('pqrs_types')->get();
		$process 		= DB::table('processes')->get();
		$TypeUsers 		= DB::table('pqrs_type_users')->get();
		$detalle 		= DB::table('pqrs_details')->get();

		$pqr = Pqrs::where('users_id', '=',$my_id )->get();
		$Traceabilitys = DB::table('pqrs_traces')->where('id', '<>', -2)->get();

		$data = array();
		for ($i=0; $i < count($pqr); $i++) { 
			$data[$i]['id_pqr'] 		= 'PQR' . str_pad($pqr[$i]->id, 6, "0", STR_PAD_LEFT);
			$data[$i]['title'] 			= $pqr[$i]->title;
			$data[$i]['description'] 	= $pqr[$i]->description;
			$data[$i]['usuario'] 		= $pqr[$i]->Users->name." ".$pqr[$i]->Users->last_name;
			$data[$i]['estado'] 		= $pqr[$i]->PqrsStatuses->name;
			$data[$i]['proceso'] 		= $pqr[$i]->Processes->name;
			$data[$i]['tipo_usuario'] 	= $pqr[$i]->PqrsTypeUsers->name;
			$data[$i]['tipo_pqr'] 		= $pqr[$i]->PqrsTypes->name;
			
			
		}

		//consulta trazabilidad


		if (count($pqr) == 0) {
			return View::make('dashboard.index')

			->with('error', 'Usted no ha ingresado nunguna PQR')

			->with('TypeRequests', $TypeRequests)
			->with('process', $process)
			->with('TypeUsers', $TypeUsers)
			->with('detalle', $detalle)
			->with('container', 'dashboard.pqrs.my_pqrs')
			->with('submenu_activo', 'Mis PQRS')
			->with('menu_activo', 'PQRS');


			
		}else{
			return View::make('dashboard.index')
			->with('traceabilitys', $Traceabilitys)
			->with('pqrs', $data)
			->with('TypeRequests', $TypeRequests)
			->with('process', $process)
			->with('TypeUsers', $TypeUsers)
			->with('detalle', $detalle)
			->with('container', 'dashboard.pqrs.my_pqrs')
			->with('submenu_activo', 'Mis PQRS')
			->with('menu_activo', 'PQRS');
		}
	}
	public function getGestion(){	
		$my_id = Auth::user()->id;
		$user = Users::find(Auth::user()->id);
		$pqrs = Pqrs::where('pqrs_statuses_id', '=', '1')->orderBy('id','desc')->get();

		$Type_pqrs 				= DB::table('pqrs_types')->get();
		$confidential_texts 	= DB::table('confidential_texts')->where('administrative_id', '=', $my_id)->get();
		$specify 				= DB::table('pqrs_specifys')->get();
		$descriptions 			= DB::table('pqrs_descriptions')->get();
		$states_pqrs 			= DB::table('pqrs_statuses')->get();
		$processes 				= processes::where('user_id', '=', 1)->get();
		$default_texts 			= DB::table('default_texts')->where('administrative_id', '=', $my_id)->get();
		

		return View::make('dashboard.index')
		->with('container', 'dashboard.pqrs.gestion')
		->with('requests',$pqrs)
		->with('user', $user)
		->with('TypeRequests', $Type_pqrs)
		->with('especifique', $specify)
		->with('description', $descriptions)
		->with('state_requests', $states_pqrs)
		->with('processes', $processes)
		->with('default_text',$default_texts)
		->with('default_confidential',$confidential_texts)
		->with('submenu_activo', 'Gestión')
		->with('menu_activo', 'PQRS');
	}
	public function responder()	{
		
		$my_id = Auth::user()->id;

		$requests = Requests::where('state_requests_id', '=', '2')->where('user_asign', '=', $my_id)->orderBy('id','desc')->get();

		$TypeRequests 			= DB::table('Type_requests')->get();
		$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
		$especifique 			= DB::table('especifique')->get();
		$descripcion 			= DB::table('descripcion')->get();
		$state_requests 		= DB::table('state_requests')->get();
		$processes 				= User::where('responsible', '=', 1)->get();
		$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();

		if (count($requests) == 0) {
			return View::make('dashboard.index')
			
			
			->with('container', 'dashboard.pqrs.gestion')
			->with('menu_activo', 'pqrs')
			->with('error', 'Usted no ha ingresado nunguna PQR');

		}else{
			return View::make('dashboard.index')
			->with('requests',$requests)
			->with('TypeRequests', $TypeRequests)
			->with('especifique', $especifique)
			->with('description', $descripcion)
			->with('state_requests', $state_requests)
			->with('processes', $processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('container', 'dashboard.pqrs.responder')
			->with('menu_activo', 'pqrs');

		}
	}
	public function historial(){
		$level = Auth::user()->rol_id;

		//control permissions only access administrator (ad)
		if($level==1)
		{
			$my_id = Auth::user()->id;
			$requests = Requests::where('state_requests_id', '<>', '0')->orderBy('id','desc')->paginate(10);
			$TypeRequests 			= DB::table('Type_requests')->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$descripcion 			= DB::table('descripcion')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();

			//$requests = DB::table('requests')->paginate(6);
			return View::make('dashboard.index')
			->with('requests',$requests)
			->with('TypeRequests', $TypeRequests)
			->with('especifique', $especifique)
			->with('description', $descripcion)
			->with('state_requests', $state_requests)
			->with('processes', $processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');
			
			
		}else{
			return View::make('dashboard.index')
			->with('container', 'errors.access_denied_ad')
			->with('menu_activo', 'administration');
		}
	}
	public function getDelete($user_id)	{
		//buscamos el usuario en la base de datos segun la id enviada
		$user = User::find($user_id);
		//eliminamos y redirigimos
		$user->delete();
		return Redirect::to('users')->with('status', 'ok_delete');
	}
	public function postNew(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		$nombre_supports = $_FILES['support']['name'];
		$ruta = $_FILES['support']['tmp_name'];

		if (isset($_FILES['support']) && $_FILES['support']['tmp_name'] == "") {
			$destino = 'Sin siporte';
		}else{
			$destino = 'assets/supports_pqrs/'.'soporte_'.$_POST['user_id'].$nombre_supports;
			copy($ruta, $destino);
		}

		$request = new Pqrs;

		$request->title 			= 		Input::get('title');
		$request->description 		= 		Input::get('description');
		$request->suport_file		=		$destino;
		$request->pqrs_types_id 	= 		Input::get('type_requests_id');
		$request->processes_id 		= 		Input::get('process_id');
		$request->users_id 			= 		Input::get('user_id');
		$request->pqrs_specifys_id  = 		Input::get('pqrs_specifys_id');
		$request->pqrs_specifys_id  = 		Input::get('type_users_id');
		$request->pqrs_type_users_id= 		1;
		
		$request->save();
		
		//redirigimos a my requets
		return Redirect::to('pqrs/mypqrs');




	}
	public function postIngresartexto(){
		$id_user = Auth::user()->id;
		
		$DefaultText = new DefaultText;

		$DefaultText->text 				= 		Input::get('text');
		$DefaultText->administrative_id	= 		$id_user;
		
		if ($DefaultText->save()) {
			echo "1";
		}else{
			echo "2";
		}
	}


	public function getCargartextosusuario(){
		$textos = DefaultText::where('administrative_id', Auth::user()->id)->get();
		echo '<option value="">Selecciona una Opcion</option> ';
		foreach ($textos as $key) {
			echo '<option value="'.$key->text.'">'.$key->text.'</option> ';
		}

	}
	public function postIngresartextoconfidencial(){
		
		$DefaultText = new DefaultConfidential;

		$DefaultText->text 			= 		Input::get('text');
		$DefaultText->id_user 		= 		Input::get('user_id');
		$DefaultText->date 			= 		Input::get('date');
		//guardamos
		$DefaultText->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postReply(){
		//capturamos el id del request
		$data = Input::get('requests_id');
		
		//actualizamos los datos en la tabla requests
		Requests::where('id', '=', $data)->update(
			array(
				'state_requests_id'   =>Input::get('state_request'),
				'user_asign' 		  =>Input::get('user_asign')


			)
		);
		
		//ingresar registros en la tabla traceabilitys
		$traceability = new Traceabilitys;

		$traceability->coment 		= Input::get('coment');
		$traceability->date 		= Input::get('date');
		$traceability->users_coment = Input::get('users_coment');
		$traceability->confidential = Input::get('confidential');
		$traceability->user_id 		= Input::get('user_asign');
		$traceability->requests_id 	= Input::get('requests_id');

		//guardamos
		$traceability->save();
		
		//redirigimos a my requets
		return Redirect::to('responder')->with('status', 'ok_create');
	}
	public function postResponse(){
		//capturamos el id del request
		$data = Input::get('requests_id');
		
		//actualizamos los datos en la tabla requests
		Requests::where('id', '=', $data)->update(
			array(
				'state_requests_id'   =>Input::get('state_request'),
				'type_request2' 	  =>Input::get('state_request'),
				'specify' 			  =>Input::get('specify'),
				'specify_description' =>Input::get('specify_description'),
				'user_asign' 		  =>Input::get('user_asign')


			)
		);
		//ingresar registros en la tabla traceabilitys
		$traceability = new Traceabilitys;

		$traceability->coment 		= Input::get('coment');
		$traceability->date 		= Input::get('date');
		$traceability->users_coment = Input::get('users_coment');
		$traceability->confidential = Input::get('confidential');
		$traceability->user_id 		= Input::get('user_asign');
		$traceability->requests_id 	= Input::get('requests_id');

		//guardamos
		$traceability->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postMassive(){
		$data_request = array(
				'request_id' => Input::get('chck')
			);
		foreach ($data_request['request_id'] as $key) {
			echo $key;

			Requests::where('id', '=', $key)->update(
				array(
					'state_requests_id' 	=>Input::get('state_request'),
					'type_request2' 		=>Input::get('state_request'),
					'specify' 				=>Input::get('specify'),
					'specify_description' 	=>Input::get('specify_description'),
					'user_asign'		 	=>Input::get('user_asign')


				)
			);

			$traceability = new Traceabilitys;

			$traceability->coment 		= Input::get('coment');
			$traceability->date 		= Input::get('date');
			$traceability->users_coment = Input::get('users_coment');
			$traceability->confidential = Input::get('confidential');
			$traceability->user_id 		= Input::get('user_asign');
			$traceability->requests_id 	= $key;

			//guardamos
			$traceability->save();
		}


		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function historialAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];

			$requests = Requests::where('id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}elseif (isset($_POST['valorCaja2']) && $_POST['valorCaja2'] != 'Proceso' && $_POST['valorCaja2'] != '' ) {

			$item = $_POST['valorCaja2'];

			$requests = Requests::where('process_id', '=', "$item")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}elseif (isset($_POST['valorCaja3']) && $_POST['valorCaja3'] != 'Estado' && $_POST['valorCaja3'] != '' ) {

			$item = $_POST['valorCaja3'];
		

			$requests = Requests::where('state_requests_id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}elseif (isset($_POST['valorCaja4']) && $_POST['valorCaja4'] != 'Tipo PQR' && $_POST['valorCaja4'] != '' ) {

			$item = $_POST['valorCaja4'];
			

			$requests = Requests::where('type_requests_id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}
		else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}
	}
	public function getDetallepqr(){
		if (Input::get('id_pqr')) {

			$id_pqr 		= Input::get('id_pqr');
			$my_id 			= Auth::user()->id;
			$pqrs 			= Pqrs::where('id', $id_pqr)->get();
			$tipo_pqrs 		= DB::table('pqrs_types')->get();
			$tipo_usuarios  = DB::table('pqrs_type_users')->get();
			$procesos 		= DB::table('processes')->get();
			$textos  = DB::table('default_texts')->where('administrative_id', '=', $my_id)->get();


			return View::make('dashboard.pqrs.detalle_pqr')
			->with('pqrs',$pqrs)
			->with('tipo_usuarios',$tipo_usuarios)
			->with('textos',$textos)
			->with('procesos',$procesos)
			->with('tipo_pqrs',$tipo_pqrs);
			
			

		}
	}
	public function responderAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];
			$my_id = Auth::user()->id;
			$requests = Requests::where('id', '=', $item)->paginate(10);
			foreach ($requests as $key) {
				$item_last = $key->user->id;
			}

			$previus_requests = Requests::where('user_id', '=', $item_last)->get();
			$TypeRequests 			= DB::table('Type_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$descripcion 			= DB::table('descripcion')->get();



			return View::make('containers.ajaxResponder')
			->with('requests',$requests)
			->with('previus_requests',$previus_requests)
			->with('TypeRequests',$TypeRequests)
			->with('processes',$processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('especifique',$especifique)
			->with('state_requests',$state_requests)
			->with('description',$descripcion)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->get();
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}
	}
	public function respMasivo(){
		$item = $_POST['id'];
		echo $item;
		
		echo "hola";
	}

	public function getSubirnotas(){
		
		$my_id = Auth::user()->id;
		$my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get(); 

		$cursos = CoursesSubjects::where('courses_instructors_id', '=',$my_idins[0]->id)
		->get();

		return View::make('dashboard.index')
		->with('container', 'dashboard.continuada.subir_notas')
		->with('cursos', $cursos)
		->with('submenu_activo', 'Mis PQRS')
		->with('menu_activo', 'Educacion Continuada');
	}
	public function getSubirfallas(){
		
		$my_id = Auth::user()->id;
		$my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get(); 

		$cursos = CoursesSubjects::where('courses_instructors_id', '=',$my_idins[0]->id)
		
		->get();

		return View::make('dashboard.index')
		->with('container', 'dashboard.continuada.subir_fallas')
		->with('cursos', $cursos)
		->with('submenu_activo', 'Mis PQRS')
		->with('menu_activo', 'Educacion Continuada');
	}
	public function getCargaralumnosnotas(){
		
		$alumnos = DB::table('courses_has_courses_students')->where('courses_id', Input::get('id_curso'))
		->join('courses_students', 'courses_students.id', '=', 'courses_has_courses_students.courses_students_id')
		->join('users', 'users.id', '=', 'courses_students.users_id')
		->select('users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'courses_students.id', 'courses_has_courses_students.id as id_asig')
		->get();

		$data = array(); 

		$notas = DB::table('courses_notes')->where('courses_has_courses_students_id', $alumnos[0]->id_asig)->get();

		for ($i=0; $i < count($alumnos); $i++) { 
			$notas = DB::table('courses_notes')->where('courses_has_courses_students_id', $alumnos[$i]->id_asig)->get();
			$j = 0;
			foreach ($notas as $key) {
				
				$data[$i]['id'] = $key->id;

				if ($key->note == "") {
					
					$data[$i]['valor'] = 100;
					
				}else{
					
					$data[$i]['valor'] = $key->note;
				}

				$j++;
			}
		}

		return View::make('dashboard.continuada.subir_notas_alumnos')
		->with('curso', Input::get('id_curso'))
		->with('notas', $data)
		->with('notas2', $notas)
		->with('alumnos', $alumnos);
		
		
	}
	public function getCargaralumnosfallas(){
		
		$alumnos = DB::table('courses_has_courses_students')->where('courses_id', Input::get('id_curso'))
		->join('courses_students', 'courses_students.id', '=', 'courses_has_courses_students.courses_students_id')
		->join('users', 'users.id', '=', 'courses_students.users_id')
		->select('users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'courses_students.id', 'courses_has_courses_students.id as id_asig')
		->get(); 

		$data = array();
		
		$fechas = DB::table('courses_fails')->where('courses_has_courses_students_id', $alumnos[0]->id_asig)->get();


		for ($i=0; $i < count($alumnos); $i++) { 
			$fallas = DB::table('courses_fails')->where('courses_has_courses_students_id', $alumnos[$i]->id_asig)->get();
			$j = 0;
			foreach ($fallas as $key) {
				
				$data[$i][$j]['id'] = $key->id;
				if ($key->fail == 1) {
					$data[$i][$j]['falla'] = 'icon-ok';
					$data[$i][$j]['valor'] = 0;
					
				}else{
					$data[$i][$j]['falla'] = 'icon-remove';
					$data[$i][$j]['valor'] = 1;
				}

				$j++;
			}
		}
			

		return View::make('dashboard.continuada.subir_fallas_alumnos')
		->with('fecha_nueva', Input::get('fecha'))
		->with('curso', Input::get('id_curso'))
		->with('fechas', $fechas)
		->with('fallas', $data)
		->with('alumnos', $alumnos);
		
		
	}
	public function postGuardarfallas(){

		if (Input::get('fecha') && Input::get('id_alum') && Input::get('valor') && Input::get('curso')) {
			$id_alum = Input::get('id_alum');
			$valor 	 = Input::get('valor');
			$j = 0;
			$my_id = Auth::user()->id;
			$my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get();

			foreach ($id_alum as $key) {
				$alumnos = DB::table('courses_has_courses_students')
				->where('courses_id', Input::get('curso'))
				->where('courses_students_id', $key)
				->get();

			   
			    DB::table('courses_fails')->insert(array(
			        array('fail' => $valor[$j], 'date_faill' => Input::get('fecha'), 'courses_has_courses_students_id' => $alumnos[0]->id, 'courses_instructors_id' => $my_idins[0]->id),
			                
			    ));
			        
			    $j++;
			}
			echo "1";
		}else{
			echo "2";
		}
		
		
		
	}
	public function postGuardarnotas(){

		if (Input::get('id_alum') && Input::get('valor') && Input::get('curso')) {

			$id_alum = Input::get('id_alum');
			$valor 	 = Input::get('valor');
			$j = 0;
			$my_id = Auth::user()->id;
			$my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get();

			foreach ($id_alum as $key) {

				$alumnos = DB::table('courses_has_courses_students')
				->where('courses_id', Input::get('curso'))
				->where('courses_students_id', $key)
				->get();

				// var_dump($alumnos);
				// exit();
			   
			    DB::table('courses_notes')->insert(array(
			        array('note' => $valor[$j], 'courses_has_courses_students_id' => $alumnos[0]->id, 'courses_instructors_id' => $my_idins[0]->id),
			                
			    ));
			        
			    $j++;
			}
			echo "1";
		}else{
			echo "2";
		}
		
		
		
	}
	public function getCambiarfalla(){
		
		DB::table('courses_fails')
		->where('id', Input::get('id'))
		->update(array('fail' => Input::get('valor')));
		echo "1";
	}
	public function getCambiarnota(){
		
		DB::table('courses_notes')
		->where('id', Input::get('id'))
		->update(array('note' => Input::get('valor')));
		echo "1";
	}
	public function getFormentrada(){


		return View::make('dashboard.index')
		// return View::make('dashboard.continuada.form_entrada')
		->with('container', 'dashboard.continuada.form_entrada')
		->with('submenu_activo', 'Formulario de Entrada')
		->with('menu_activo', 'Educacion Continuada');	


		
	}
	public function getFormsalida(){


		return View::make('dashboard.index')
		
		->with('container', 'dashboard.continuada.form_salida')
		->with('submenu_activo', 'Formulario de Salida')
		->with('menu_activo', 'Educacion Continuada');	


		
	}
	public function getIngresarhoras(){

		$nueva_hora = new CoursesControlClasses;

		$nueva_hora->date					= Input::get('horas_fecha');
		$nueva_hora->hour					= Input::get('horas_hora');
		$nueva_hora->courses_subjects_id	= Input::get('horas_intructor');
		$nueva_hora->general_classrooms_id	= Input::get('horas_aula');

		if ($nueva_hora->save()) {

			$materias = CoursesSubjects::Where("courses_id", Input::get('id_curso'))->get();

			$subject = array();
			$i = 0;
			foreach ($materias as $key) {
			    $subject[$i] = $key->id;
			    $i++;
			}

			$horas = CoursesControlClasses::whereIn('courses_subjects_id', $subject)->get();


			

			return View::make('dashboard.continuada.body_horas')
			->with('horas', $horas);	

		}else{
			echo "1";
		}
		


		
	}
	public function getControl(){
		$my_id = Auth::user()->id;
		$my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get();

		$cursos = CoursesSubjects::where('courses_instructors_id', '=',$my_idins[0]->id)
        ->get();

		return View::make('dashboard.index')
		
		->with('container', 'dashboard.continuada.control_clase')
		->with('submenu_activo', 'Cargar Control Clase')
		->with('cursos', $cursos)
		->with('menu_activo', 'Educacion Continuada');	


		
	}
	public function getCargarhorascurso(){
		
		$horas = CoursesControlClasses::where('courses_subjects_id', Input::get('id_curso'))->get();

		return View::make('dashboard.continuada.body_control_clase')
		->with('horas', $horas);
		
		
	}
	public function getGuardarcontrolclase(){
		$tema_control = Input::get('tema_control');
		$observacion_control = Input::get('observacion_control');
		$id_control = Input::get('id_control');


		$horas = CoursesControlClasses::find($id_control);

		$horas->theme = $tema_control;
		$horas->observation_teacher = $observacion_control;
		$horas->dictated = 1;

		if($horas->save()){
			echo 1;
		}else{
		    echo 2;
		}



	}
	public function getGuardarverificacionclase(){
		
		$observacion_control = Input::get('observacion_control');
		$id_control = Input::get('id_control');


		$horas = CoursesControlClasses::find($id_control);

		
		$horas->observation_adm = $observacion_control;
		$horas->verified = 1;

		if($horas->save()){
			echo 1;
		}else{
		    echo 2;
		}



	}
//    public function getPrueba(){
//        $estudiantes = DB::table('prueba')->get();
//        foreach ($estudiantes as $key) {
//            DB::update('update courses set courses_inspectors_id  = ? where id = ?', array($key->documento ,$key->id_user));
//        }
//        echo "1";
//    }
}

?>