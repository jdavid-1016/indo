<?php
/**
 * 
 */
class RegistroController extends BaseController {

    public function __construct() {
     
    }
    
    
    public function formulario($codigo){

        $aspirante = AspirantsProspects::where('code', '=', $codigo)
                ->get();

        if(count($aspirante) > 0){
         
        $programas = AspirantsPrograms::all();
        $typedoc = AspirantsTypeDocuments::all();
        $nivelf = AspirantsTrainings::all();
        $estadocivil = AspirantsMaritalStatuses::all();
        $lugarnac = AspirantsBirthPlaces::all();
        $localidades = AspirantsLocations::all();
        $genero = AspirantsGenders::all();
        $periodo = AspirantsPeriods::all();       
        $jornadas = AspirantsTimes::all();
        $tipo_sangre = DB::table('general_blood_groups')->get();
        $countries = DB::table('countries')->get();
        $fuentes_inf = DB::table('aspirants_sources')->get();
        
        $name = explode(" ", $aspirante[0]->name);
        $last_name = explode(" ", $aspirante[0]->last_name);
        
        return View::make('dashboard.aspirantes.registro')
        ->with('aspirante', $aspirante)
        ->with('name', $name)
        ->with('last_name', $last_name)
        ->with('programas', $programas)
        ->with('periodo', $periodo)
        ->with('typedoc', $typedoc)
        ->with('estadocivil', $estadocivil)
        ->with('genero', $genero)
        ->with('jornadas', $jornadas)
        ->with('lugarnac', $lugarnac)
        ->with('localidades', $localidades)
        ->with('tipo_sangre', $tipo_sangre)
        ->with('countries', $countries)
        ->with('fuentes_inf', $fuentes_inf)
        ->with('id_prospecto', $aspirante[0]->id)
        ->with('nivelf', $nivelf);
        
        }else{
            
        return View::make('dashboard.aspirantes.pagerror');
            
        }
                
    }
    
    public function formularionuevo(){
        
        $programas = AspirantsPrograms::all();

        return View::make('dashboard.aspirantes.registronuevo')
        ->with('programas', $programas);
                
    }
    
    public function getGenerarformulario(){
        
        $id = Input::get('id');
        $observation_edit = Input::get('observation_edit');                		
        $email = Input::get('email');
        $codigo = rand().$id;
        $codigo = md5($codigo);
        
        DB::table('aspirants_prospects')
                ->where('id', Input::get('id'))
                ->update(array('name' => Input::get('name'), 
                               'email' => Input::get('email'),
                               'phone' => Input::get('phone'),
                               'city' => Input::get('city'),
                               'code' => $codigo,
                               'aspirants_status_prospects_id' => Input::get('status'),
                               'aspirants_programs_id' => Input::get('programs')));

        if($observation_edit != ""){

            $traces = new AspirantsTraces;

            $traces->coment = $observation_edit;
            $traces->aspirants_status_prospects_id = Input::get('status');
            $traces->aspirants_prospects_id = $id;
            $traces->users_id = Auth::user()->id;

            $traces->save();
        }

                    $fromEmail = "Indoamericana@indoamericana.edu.co";
                    $fromName = "Indoamericana";

                    $data= array(
                        'link' => 'http://192.168.0.18/indo/public/registro/'.$codigo,
                        'name' => Input::get('name')
                    );

                Mail::send('emails.formulario', $data, function ($message) use ($fromName, $fromEmail){
                $message->subject('Formulario de registro de Aspirantes');
                $message->to(Input::get('email'));
                $message->from($fromEmail, $fromName);
                });
                
                echo 1;
        
    }
    
    public function guardarnuevoaspirante(){

        $imagen_alumno = Input::get('imagen_alumno');
        $id_programa = Input::get('id_programa');
        $periodo = Input::get('periodo');
        $type_aspirant = Input::get('type_aspirant');
        $jornada = Input::get('jornada');
        $name = Input::get('name');
        $middle_name = Input::get('middle_name');
        $last_name = Input::get('last_name');
        $last_name2 = Input::get('last_name2');
        $type_document = Input::get('type_document');
        $document = Input::get('document');
        $nivel_formacion = Input::get('nivel_formacion');
        $libreta_militar = Input::get('libreta_militar');
        $fecha_nacimiento = Input::get('fecha_nacimiento');
        $estado_civil = Input::get('estado_civil');
        $genero = Input::get('genero');
        $grupo_sanguineo = Input::get('grupo_sanguineo');
        $nacionalidad = Input::get('nacionalidad');
        $direccion = Input::get('direccion');
        $localidad = Input::get('localidad');
        $barrio = Input::get('barrio');
        $ciudad = Input::get('ciudad');
        $telefono = Input::get('telefono');
        $celular = Input::get('celular');
        $email = Input::get('email');
        $lugar_nacimiento = Input::get('lugar_nacimiento');
        $observaciones = Input::get('observaciones');
        $empresa = Input::get('empresa');
        $empresa_cargo = Input::get('empresa_cargo');
        $empresa_telefono = Input::get('empresa_telefono');
        $empresa_direccion = Input::get('empresa_direccion');
        $padre_nombre = Input::get('padre_nombre');
        $padre_ocupacion = Input::get('padre_ocupacion');
        $padre_direccion = Input::get('padre_direccion');
        $padre_telefono = Input::get('padre_telefono');
        $madre_nombre = Input::get('madre_nombre');
        $madre_ocupacion = Input::get('madre_ocupacion');
        $madre_direccion = Input::get('madre_direccion');
        $madre_telefono = Input::get('madre_telefono');
        $acudiente_nombre = Input::get('acudiente_nombre');
        $acudiente_ocupacion = Input::get('acudiente_ocupacion');
        $acudiente_direccion = Input::get('acudiente_direccion');
        $acudiente_telefono = Input::get('acudiente_telefono');
        $acudiente_celular = Input::get('acudiente_celular');
        $acudiente_email = Input::get('acudiente_email');
        $acudiente_barrio = Input::get('acudiente_barrio');
        $estudios_institucion = Input::get('estudios_institucion');
        $estudios_universidad = Input::get('estudios_universidad');
        $estudios_carrera = Input::get('estudios_carrera');
        $estudios_otros = Input::get('estudios_otros');
        $motivo_estudio = Input::get('motivo_estudio');
        $fuentes_informacion = Input::get('fuentes_informacion');
        $fuentes_especifique = Input::get('fuentes_especifique');

        $estrato = Input::get('estrato');
        $eps = Input::get('eps');
        $sisben = Input::get('sisben');
        $desplazado = Input::get('desplazado');
        $indigena = Input::get('indigena');
        $reinsertado = Input::get('reinsertado');
        $discapacidad = Input::get('discapacidad');
        $cual_discapacidad = Input::get('cual_discapacidad');
        $cabeza_familia = Input::get('cabeza_familia');
        $frontera = Input::get('frontera');
        $afro = Input::get('afro');

        $estatura = Input::get('estatura');
        $id_prospecto = Input::get('id_prospecto');
        $expedicion = Input::get('expedicion');

        if (Input::file('imagen_alumno')) {
            
            $destinationPath = 'assets/img/aspirantes/';

            $observaciones      = Input::get('observaciones');
            $id     = Input::get('id');
           
            $filename = date("YmdHis")."_".Input::file('imagen_alumno')->getClientOriginalName();

            $uploadSuccess = Input::file('imagen_alumno')->move($destinationPath, $filename);

            if ($uploadSuccess) {
                
                $new_user = new Users;
        
                $new_user->name                         =$name;
                $new_user->name2                        =$middle_name;
                $new_user->last_name                    =$last_name;
                $new_user->last_name2                   =$last_name2;
                $new_user->img = $destinationPath.$filename;
                $new_user->img_profile = $destinationPath.$filename;
                $new_user->img_min = $destinationPath.$filename;
                $new_user->email_institutional          =$email;
                $new_user->email                        =$email;
                $new_user->cell_phone                   =$celular;
                $new_user->address                      =$direccion;
                $new_user->phone                        =$telefono;
                $new_user->type_document                =$type_document;
                $new_user->document                     =$document;
                $new_user->city_id                      =$ciudad;
                $new_user->general_genres_id            =$genero;
                $new_user->general_blood_group_id       =$grupo_sanguineo;
                $new_user->countries_id                 =$nacionalidad;
                $new_user->general_marital_status_id    =$estado_civil;
                
                
                        if ($new_user->save()) {

                        $last_user  = Users::all();
                        $id_user    = $last_user->last()->id;

                        $new_aspirant_inf = new AspirantsInformations;



                        $new_aspirant_inf->first_name                       =$name;
                        $new_aspirant_inf->middle_name                      =$middle_name;
                        $new_aspirant_inf->last_name                        =$last_name;
                        $new_aspirant_inf->last_name2                       =$last_name2;
                        $new_aspirant_inf->aspirants_type_documents_id      =$type_document;
                        $new_aspirant_inf->document                         =$document;
                        $new_aspirant_inf->issued                           =$expedicion;
                        $new_aspirant_inf->aspirants_programs_id                =$id_programa;
                        $new_aspirant_inf->aspirants_periods_id             =$periodo;
                        $new_aspirant_inf->aspirants_times_id               =$jornada;
                        $new_aspirant_inf->aspirants_trainings_id           =$nivel_formacion;
                        $new_aspirant_inf->military_card                    =$libreta_militar;
                        $new_aspirant_inf->birthday                         =$fecha_nacimiento;
                        $new_aspirant_inf->aspirants_marital_statuses_id    =$estado_civil;
                        $new_aspirant_inf->aspirants_genders_id             =$genero;
                        $new_aspirant_inf->blood_group                      =$grupo_sanguineo;
                        $new_aspirant_inf->countries_id                      =$nacionalidad;
                        $new_aspirant_inf->address                          =$direccion;
                        $new_aspirant_inf->aspirants_locations_id           =$localidad;
                        $new_aspirant_inf->neighborhood                     =$barrio;
                        $new_aspirant_inf->aspirants_cities_id              =$ciudad;
                        $new_aspirant_inf->phone                            =$telefono;
                        $new_aspirant_inf->cell_phone                       =$celular;
                        $new_aspirant_inf->email                            =$email;
                        $new_aspirant_inf->aspirants_birth_places_id        =$lugar_nacimiento;
                        $new_aspirant_inf->observations                     =$observaciones;
                        $new_aspirant_inf->height                           =$estatura;
                        $new_aspirant_inf->image                            =$destinationPath.$filename;
                        $new_aspirant_inf->aspirants_sources_id             =$fuentes_informacion;
                        $new_aspirant_inf->source_details                   =$fuentes_especifique;
                        $new_aspirant_inf->aspirants_prospects_id           =$id_prospecto;
                        $new_aspirant_inf->aspirants_status_managements_id  =3;
                        $new_aspirant_inf->users_id                         =$id_user;
                        $new_aspirant_inf->general_type_students_id         =1;

                        if ($new_aspirant_inf->save()) {

                            $last_aspirant_inf  = AspirantsInformations::all();
                            $id_aspirant_inf    = $last_aspirant_inf->last()->id;


                            $new_aspirant_fam = new AspirantsFamilies;


                            $new_aspirant_fam->name_father                  = $padre_nombre;
                            $new_aspirant_fam->occupation_father            = $padre_ocupacion;
                            $new_aspirant_fam->address_father               = $padre_direccion;
                            $new_aspirant_fam->phone_father                 = $padre_telefono;
                            $new_aspirant_fam->name_mother                  = $madre_nombre;
                            $new_aspirant_fam->occupation_mother            = $madre_ocupacion;
                            $new_aspirant_fam->address_mother               = $madre_direccion;
                            $new_aspirant_fam->phone_mother                 = $madre_telefono;
                            $new_aspirant_fam->name_keeper                  = $acudiente_nombre;
                            $new_aspirant_fam->occupation_keeper            = $acudiente_ocupacion;
                            $new_aspirant_fam->address_keeper               = $acudiente_direccion;
                            $new_aspirant_fam->phone_keeper                 = $acudiente_telefono;
                            $new_aspirant_fam->cellphone_keeper             = $acudiente_celular;
                            $new_aspirant_fam->email_keeper                 = $acudiente_email;
                            $new_aspirant_fam->neighborhood                 = $acudiente_barrio;
                            $new_aspirant_fam->aspirants_informations_id    = $id_aspirant_inf;

                            $new_aspirant_fam->save();

                            $aspirants_studies = new AspirantsStudies;


                            $aspirants_studies->school                     = $estudios_institucion;
                            $aspirants_studies->university                 = $estudios_universidad;
                            $aspirants_studies->career                     = $estudios_carrera;
                            $aspirants_studies->others                     = $estudios_carrera;
                            $aspirants_studies->motivation                 = $estudios_otros;
                            $aspirants_studies->aspirants_informations_id  = $id_aspirant_inf;

                            $aspirants_studies->save();

                            $aspirants_works = new AspirantsWorkings;


                            $aspirants_works->company                       = $empresa;
                            $aspirants_works->occupation                    = $empresa_cargo;
                            $aspirants_works->phone                         = $empresa_telefono;
                            $aspirants_works->address                       = $empresa_direccion;
                            $aspirants_works->aspirants_informations_id     = $id_aspirant_inf;

                            $aspirants_works->save();
                            
                            if($nacionalidad==50){
                                $documentos = AspirantsDocuments::whereIn('id', [1, 2, 3, 5, 6])->get();
                            }else{
                                $documentos = AspirantsDocuments::whereIn('id', [1, 3, 4, 7, 8])->get();
                            }

                            foreach ($documentos as $doc){

                                $doc_user = new AspirantsDocumentsHasAspirantsInformations;

                                $doc_user->aspirants_documents_id = $doc->id;
                                $doc_user->aspirants_informations_id = $id_aspirant_inf;
                                $doc_user->status = 1;
                                $doc_user->date_commitment = 0000-00-00;

                                $doc_user->save();

                            }

                                $examen = new AspirantsResults;

                                $examen->date_knowledge = 0000-00-00;
                                $examen->score_knowledge = 0;
                                $examen->date_interview = 0000-00-00;
                                $examen->score_interview = 0;
                                $examen->aspirants_status_results_id = 1;
                                $examen->score_status = 0;
                                $examen->discount = 0;
                                $examen->score_discount = 0;
                                $examen->observations = " ";
                                $examen->aspirants_informations_id = $id_aspirant_inf;

                                $examen->save();

                                DB::table('aspirants_prospects')
                                ->where('id', $id_prospecto)
                                ->update(array('status_inscription' =>1, 'aspirants_status_managements_id' => 3));

                            echo "1";

                        }

                    }
                                 
            } 
        } else {
            //echo "2";
            echo Input::file('imagen_alumno');
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
                              
    }

}
?>