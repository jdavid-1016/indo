<?php
/**
 * 
 */
class ContinuadaController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }
    public function getNuevocurso() {
        
        $user = Users::find(Auth::user()->id);
        $categories = CoursesCategories::all();
        $instructors = CoursesInstructors::all();
        $inspectores = CoursesInspectors::all();
        $nombres = Courses::distinct()->select('name_curso')->get(); 

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.continuada.nuevocurso')
            ->with('user', $user)
            ->with('nombres', $nombres)
            ->with('categories', $categories)
            ->with('instructors', $instructors)
            ->with('inspectores', $inspectores)
            ->with('submenu_activo', 'Cursos')
            ->with('menu_activo', 'Educacion Continuada');
        }
    }
    public function getCursos() {

        $ocsa    = Input::get('ocsa');
        $cod    = Input::get('cod');
        $curso  = Input::get('curso');
        $cate   = Input::get('cate');

        $user = Users::find(Auth::user()->id);
        $categories = CoursesCategories::all();
        $instructors = CoursesInstructors::all();

        $cursos = Courses::Where("status", 1)
        ->where('name_curso', 'LIKE', '%' . Input::get('curso') . '%')
        ->where('courses_categories_id', 'LIKE', '%' . Input::get('cate') . '%')
        ->where('coding', 'LIKE', '%' . Input::get('cod') . '%')
        ->where('ocsa', 'LIKE', '%' . Input::get('ocsa') . '%')

        ->paginate(20);

        $num = Courses::Where("status", 1)
        ->where('name_curso', 'LIKE', '%' . Input::get('curso') . '%')
        ->where('courses_categories_id', 'LIKE', '%' . Input::get('cate') . '%')
        ->where('coding', 'LIKE', '%' . Input::get('cod') . '%')
        ->where('ocsa', 'LIKE', '%' . Input::get('ocsa') . '%')
        ->get();

        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.continuada.table_cursos')
            ->with('user', $user)
            ->with('categories', $categories)
            ->with('instructors', $instructors)
            ->with('cursos', $cursos)
            ->with('num', $num)
            ->with('pag', $cursos);
            exit();
        }
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.cursos')
        ->with('user', $user)
        ->with('categories', $categories)
        ->with('instructors', $instructors)
        ->with('cursos', $cursos)
        ->with('pag', $cursos)
        ->with('num', $num)
        ->with('submenu_activo', 'Cursos')
        ->with('menu_activo', 'Educacion Continuada');
        
    }
    public function getCursosinactivos() {
        $ocsa    = Input::get('ocsa');
        $emp    = Input::get('emp');
        $curso  = Input::get('curso');
        $cate   = Input::get('cate');

        $user = Users::find(Auth::user()->id);
        $categories = CoursesCategories::all();
        $instructors = CoursesInstructors::all();

        $cursosina = Courses::Where("status", 0)
        ->where('name_curso', 'LIKE', '%' . Input::get('curso') . '%')
        ->where('courses_categories_id', 'LIKE', '%' . Input::get('cate') . '%')
        ->where('coding', 'LIKE', '%' . Input::get('cod') . '%')
        ->where('ocsa', 'LIKE', '%' . Input::get('ocsa') . '%')
        ->orderBy('id', 'desc')
        ->paginate(20);

        $num = Courses::Where("status", 0)
        ->where('name_curso', 'LIKE', '%' . Input::get('curso') . '%')
        ->where('courses_categories_id', 'LIKE', '%' . Input::get('cate') . '%')
        ->where('coding', 'LIKE', '%' . Input::get('cod') . '%')
        ->where('ocsa', 'LIKE', '%' . Input::get('ocsa') . '%')
        ->get();

        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.continuada.table_cursos_inactivos')
            ->with('user', $user)
            ->with('categories', $categories)
            ->with('instructors', $instructors)
            ->with('cursosina', $cursosina)
            ->with('num', $num)
            ->with('pag', $cursosina);
            exit();
        }
            
        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.cursosinactivos')
        ->with('user', $user)
        ->with('categories', $categories)
        ->with('instructors', $instructors)
        ->with('cursosina', $cursosina)
        ->with('pag', $cursosina)
        ->with('num', $num)
        ->with('submenu_activo', 'Cursos')
        ->with('menu_activo', 'Educacion Continuada');
        
    }
    public function getGenerarocsa(){
        include('assets/fpdf17/fpdf.php');

        
        $instructor = Courses::where('id', Input::get('id'))->get();

        $alumnos = DB::table('courses_has_courses_students')
        ->join('courses_students', 'courses_students.id', '=', 'courses_students_id')
        ->join('users', 'users.id', '=', 'courses_students.users_id')
        ->where('courses_has_courses_students.courses_id', Input::get('id'))
        ->get();

        $profesores = DB::table('courses_subjects')
        ->join('courses_instructors', 'courses_instructors.id', '=', 'courses_instructors_id')
        ->join('users', 'users.id', '=', 'courses_instructors.users_id')
        ->where('courses_subjects.courses_id', Input::get('id'))
        ->get();

        $data = array();
        $data_alum = array();
        $data_prof = "";
        for ($i=0; $i < count($instructor); $i++) { 




            $data[$i]['ocsa']               = utf8_decode('N° '.$instructor[$i]->ocsa);
            $data[$i]['nombre_curso']       = utf8_decode($instructor[$i]->name_curso);
            $data[$i]['horario']            = utf8_decode($instructor[$i]->schedule);
            $data[$i]['horas']              = utf8_decode($instructor[$i]->time_intensity);
            $data[$i]['inspector']          = utf8_decode($instructor[$i]->CoursesInspectors->name);
            $data[$i]['l_teorica']          = utf8_decode($instructor[$i]->theorist_places);
            $data[$i]['d_teorica']          = utf8_decode($instructor[$i]->theorist_dates);
            $data[$i]['l_practica']         = utf8_decode($instructor[$i]->practice_places);
            $data[$i]['d_practica']         = utf8_decode($instructor[$i]->practice_dates);
        }
        for ($i=0; $i < count($alumnos); $i++) { 
            $data_alum[$i]['nombre']    = $alumnos[$i]->last_name." ".$alumnos[$i]->last_name2." ".$alumnos[$i]->name." ".$alumnos[$i]->name2;
            $data_alum[$i]['documento']    = $alumnos[$i]->document;
        }
        for ($i=0; $i < count($profesores); $i++) { 
            $data_prof = $data_prof.$profesores[$i]->name." ".$profesores[$i]->name2." ".$profesores[$i]->last_name." ".$profesores[$i]->last_name2.", ";
        }

        $pdf = new FPDF('P', 'pt', 'letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        /* Encabezado */
        $rutaImagen = 'assets/img/logo2(2).png';
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Cell(100, 60, $pdf->Image($rutaImagen, 35, 35, 0, 50, ''), 1, 0, 'L');
        $pdf->Cell(300, 60, 'INFORME OCSA-06', 1, 0, 'C');
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(150, 20, 'CODIGO: GA3-REG018', 1, 2);
        $pdf->Cell(150, 20, 'FECHA ACTUALIZACION: 26/02/2013', 1, 2);
        
        $pdf->Cell(150, 20, 'REVISION:1', 1, 0);
        $pdf->Ln();

        $pdf->Cell(550, 30, 'CONSECUTIVO', 1, 0, 'C');
        $pdf->SetXY(360, 93);
        $pdf->Cell(150, 20, $data[0]['ocsa'], 1, 2, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('CENTRO DE INSTRUCCIÓN'), 1, 0, 'C');
        $pdf->Cell(350, 17, utf8_decode('CORPORACIÓN EDUCATIVA INDOAMERICANA'), 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('CERTIFICADO UAEAC'), 1, 0, 'C');
        $pdf->Cell(350, 17, 'CCI-022', 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('ÁREA UAEAC'), 1, 0, 'C');
        $pdf->Cell(350, 17, utf8_decode('GRUPO DE INSPECCIÓN DE AERONAVEGABILIDAD'), 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('INSPECTOR ASIGNADO'), 1, 0, 'C');
        $pdf->Cell(350, 17, $data[0]['inspector'], 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('PROGRAMA ENTRENAMIENTO'), 1, 0, 'C');
        $pdf->Cell(350, 17, $data[0]['nombre_curso'], 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('EMPRESAS PARTICIPANTES'), 1, 0, 'C');
        $pdf->Cell(350, 17, '', 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('INSTRUCTOR'), 1, 0, 'C');
        $pdf->Cell(350, 17, $data_prof, 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('LICENCIAS DEL INSTRUCTOR'), 1, 0, 'C');
        $pdf->Cell(350, 17, '', 1, 0, 'C');
        $pdf->Ln();

        

        $pdf->Cell(200, 17, utf8_decode('LUGAR INSTRUCCIÓN TEÓRICA'), 1, 0, 'C');
        $pdf->Cell(200, 17, $data[0]['l_teorica'], 1, 0, 'C');
        $pdf->Cell(60, 17, 'FECHA', 1, 0, 'C');
        $pdf->Cell(90, 17, $data[0]['d_teorica'], 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('LUGAR INSTRUCCIÓN PRÁCTICA'), 1, 0, 'C');
        $pdf->Cell(200, 17, $data[0]['l_practica'], 1, 0, 'C');
        $pdf->Cell(60, 17, 'FECHA', 1, 0, 'C');
        $pdf->Cell(90, 17, $data[0]['d_practica'], 1, 0, 'C');
        $pdf->Ln();

        $pdf->Cell(200, 17, utf8_decode('HORARIO'), 1, 0, 'C');
        $pdf->Cell(200, 17, $data[0]['horario'], 1, 0, 'C');
        $pdf->Cell(60, 17, 'HORAS', 1, 0, 'C');
        $pdf->Cell(90, 17, $data[0]['horas'], 1, 0, 'C');
        $pdf->Ln(24);

        $pdf->Cell(30, 16, utf8_decode('N°'), 1, 0, 'C');
        $pdf->Cell(270, 16, 'PARTICIPANTES', 1, 0, 'C');
        $pdf->Cell(250, 16, utf8_decode('IDENTIFICACIÓN'), 1, 0, 'C');
        $pdf->Ln();
        
        for ($i=0; $i < count($data_alum); $i++) { 
            $pdf->Cell(30, 14, utf8_decode($i+1), 1, 0, 'C');
            $pdf->Cell(270, 14, utf8_decode($data_alum[$i]['nombre']), 1, 0, 'C');
            $pdf->Cell(250, 14, $data_alum[$i]['documento'], 1, 0, 'C');
            $pdf->Ln();
        }

        $pdf->Cell(550, 15, utf8_decode('TOTAL PARTICIPANTES: '.count($data_alum)), 1, 0, 'C');
        $pdf->Ln();
        
        $pdf->Cell(550, 13, utf8_decode('OBSERVACIONES'), 1, 0, 'C');
        $pdf->Ln();

        
        $pdf->Cell(550, 20,'', 1, 0, 'C');
        $pdf->Ln(30);

        $pdf->Cell(550, 30,utf8_decode('_______________________________________'), 0, 0, 'C');
        $pdf->Ln(10);
        $pdf->Cell(550, 30,utf8_decode('JOSELYN ZÁRATE GIRALDO'), 0, 0, 'C');
        $pdf->Ln(10);
        $pdf->Cell(550, 30,utf8_decode('DIRECTOR ACADÉMICO'), 0, 0, 'C');
        $pdf->Ln();

        // foreach ($data as $key) {

        //     $pdf->SetFont('Arial', 'B', 15);
        //     $pdf->Cell(160, 30, '', 0);
        //     $pdf->Cell(1, 30, 'Informacion Alumno ', 0);
        //     $pdf->SetFont('Arial', '', 10);

        //     $pdf->ln();
        //     $pdf->Cell(1, 30, '', 0);
        //     $pdf->Cell(200, 30, $key['nombre'], 0);
        //     $pdf->ln();


        //     $pdf->Image($key['img_profile'], 500, 120, 60);

        //     $pdf->Cell(1, 20, '', 0);
        //     //$pdf->Cell(1, 20, 'Puesto Ciclo1: ' . $pCiclo1, 0);
        //     //$pdf->ln();
        //     $pdf->Cell(1, 30, '', 0);
        //     //$pdf->Cell(1, 30, 'Puesto Ciclo2: ' . $pCiclo2, 0);
        //     $pdf->ln(10);
        //     $pdf->SetFont('Arial', 'B', 12);
        //     $pdf->Cell(20, 10, '', 0);
        //     $pdf->Ln(1);
        //     $pdf->Cell(1, 50, 'Informacion Personal', 0);
        //     $pdf->SetFont('Arial', '', 9.5);
        //     $pdf->SetFillColor(241, 241, 241);
        //     $pdf->Ln();
        //     $pdf->Cell(120, 16, 'Numero Identificacion :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['documento'], 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Tipo Identificacion :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['tipo_documento'], 0, '', 'L');
        //     $pdf->Ln(25);
        //     /* $pdf->Ln();
        //       $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //       $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Ln(); */
        //     $pdf->Cell(120, 16, 'Lugar De Nacimiento :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['lugar_nacimiento'], 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Fecha De Nacimiento :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['f_nacimiento'], 0, '', 'L');
        //     $pdf->Ln(25);
        //     /* $pdf->Ln();
        //       $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //       $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Ln(); */
        //     $pdf->Cell(120, 16, 'Estado Civil :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['estado_civil'], 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Genero :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['genero'], 0, '', 'L');
        //     $pdf->Ln(25);
        //     /* $pdf->Ln();
        //       $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //       $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Ln(); */
        //     $pdf->Cell(120, 16, 'Grupo Sanguineo :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['grupo_sanguineo'], 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Nacionalidad :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['nacionalidad'], 0, '', 'L');
        //     $pdf->Ln(25);
        //     /* $pdf->Ln();
        //       $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //       $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Ln(); */
        //     $pdf->Cell(120, 16, 'Direccion Residencia :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, ucfirst(mb_strtolower($key['direccion'])), 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Telefono :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['telefono'], 0, '', 'L');
        //     $pdf->Ln(25);
        //     /* $pdf->Ln();
        //       $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //       $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Ln(); */
        //     $pdf->Cell(120, 16, 'Celular :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['celular'], 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Email :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['email'], 0, '', 'L');
        //     $pdf->Ln(25);
        //     /* $pdf->Ln();
        //       $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //       $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
        //       $pdf->Ln(); */
        //     $pdf->Cell(120, 16, 'Ciudad Residencia :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, $key['ciudad'], 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Numero Hijos :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16,$key['hijos'], 0, '', 'L');
        //     $pdf->Ln(25);



        //     $pdf->Cell(120, 16, 'Nombre Del Conyuge :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, " ", 0, '', 'L');
        //     $pdf->Cell(160, 16, 'Ocupacion Conyuge :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, " ", 0, '', 'L');
        //     $pdf->Ln(25);

        //     $pdf->Cell(120, 16, 'Telefono Oficina Conyuge :', 0, '', 'R', FALSE);
        //     $pdf->Cell(160, 16, " ", 0, '', 'L');
        //     $pdf->Ln(25);
        //     $pdf->Cell(120, 16, 'Celular Conyuge :', 0, '', 'R', FALSE);
        //     $pdf->Cell(120, 16, " ", 0, '', 'L');

        //     $pdf->Ln(25);
        //     $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
        //     $pdf->Ln(1);


        //     $pdf->SetFont('Arial', 'B', 12);
        //     //$pdf->Cell(20, 10, '', 0);
        //     //$pdf->Ln();
        //     $pdf->Cell(1, 50, 'Informacion Academica', 0);
            
        // }
        $pdf->Ln();

        $pdf->Output();
    }
    public function getGuardarcurso() {
        
                $curso = new Courses;
                $curso->name_curso = Input::get('name_curso');
                $curso->start_date = Input::get('start_date');
                $curso->end_date = Input::get('end_date');
                $curso->coding = Input::get('coding');
                $curso->open = Input::get('open');
                $curso->courses_categories_id = Input::get('courses_categories_id');
                $curso->quota = Input::get('cupo');
                $curso->time_intensity = Input::get('time_intensity');
                $curso->schedule = Input::get('schedule');
                $curso->status = Input::get('status');
                $curso->ocsa = Input::get('ocsa');
                $curso->courses_inspectors_id = Input::get('inspector');
                $curso->theorist_dates = Input::get('fecha_teorica');
                $curso->practice_dates = Input::get('fecha_practica');
                $curso->theorist_places = Input::get('inst_teorica');
                $curso->practice_places = Input::get('inst_practica');
                if($curso->save()){
                    echo 1;
                }else{
                    echo 2;
                }
    }
    
    public function getEditarcurso() { 
        
                $id = Input::get('id');
                $curso = Courses::find($id);
                
                $curso->name_curso = Input::get('name_curso');
                $curso->courses_categories_id = Input::get('courses_categories_id');
                $curso->time_intensity = Input::get('time_intensity');
                $curso->schedule = Input::get('schedule');
                $curso->open = Input::get('open');
                
                $curso->courses_inspectors_id = Input::get('inspector');
                $curso->theorist_places = Input::get('inst_teorica');
                $curso->theorist_dates = Input::get('fecha_teorica');
                $curso->practice_places = Input::get('inst_practica');
                $curso->practice_dates = Input::get('fecha_practica');
                $curso->start_date = Input::get('start_date');
                $curso->end_date = Input::get('end_date');
                if($curso->save()){
                echo 1;
                }else{
                    echo 2;
                }
    }
    
    public function getEliminarmateria(){
    
        $id = Input::get('id');
        
        $filasAfectadas = CoursesSubjects::where('id', '=', $id)->delete();
        
        echo 1;
        
    }
    
    public function getEliminarasigancionest(){
    
        $id = Input::get('id');
        
        $filasAfectadas = CoursesHasCoursesStudents::where('id', '=', $id)->delete();
        
        echo 1;
        
    }        
    
    public function getDuplicarcurso() {
        
        $materias = CoursesSubjects::Where("courses_id", Input::get('id'))->get();
        
                $curso = new Courses;
                $curso->name_curso = Input::get('name_curso');
                $curso->courses_categories_id = Input::get('courses_categories_id');
                $curso->time_intensity = Input::get('time_intensity');
                $curso->schedule = Input::get('schedule');
                $curso->open = Input::get('open');
                $curso->inspector = Input::get('inspector');
                $curso->theorist_places = Input::get('inst_teorica');
                $curso->theorist_dates = Input::get('fecha_teorica');
                $curso->practice_places = Input::get('inst_practica');
                $curso->practice_dates = Input::get('fecha_practica');
                $curso->start_date = Input::get('start_date');
                $curso->end_date = Input::get('end_date');
                $curso->status = "1";
                if($curso->save()){
                    
                    $cursos = Courses::Where("id", Input::get('id'))->get();
                    foreach($materias as $mate){
                    $materia = new CoursesSubjects;
                    $materia->subject = $mate->subject;
                    $materia->courses_id = $cursos[0]->id;
                    $materia->courses_instructors_id = $mate->courses_instructors_id;
                    $materia->save();
                    }
                echo 1;
                }else{
                    echo 2;
                }
    }
    public function getGuardarcategory() {
        
                $categoria = new CoursesCategories;
                $categoria->category = Input::get('category');
                if($categoria->save()){
                    echo 1;
                }else{
                    echo 2;
                }
    }
    public function getGuardarmateria() {
        
                $materia = new CoursesSubjects;
                $materia->subject = "NULL";
                $materia->courses_id = Input::get('id');
                $materia->courses_instructors_id = Input::get('instructor');
                $materia->price = Input::get('price');
                if($materia->save()){
                    echo 1;
                }else{
                    echo 2;
                }
    }
    public function getDetallecurso() {
        
        $id = Input::get('id');

        //$shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        $curso = Courses::find($id);
        $materias = CoursesSubjects::Where("courses_id", $id)->get();
        $categories = CoursesCategories::all();
        
        $inspectores = DB::table('courses_inspectors')->get();

        //$instructors = CoursesInstructors::all();
        
        $instructors = DB::table('courses_instructors')
                ->join('users', 'users.id', '=', 'courses_instructors.users_id')
                ->select('users.name', 'users.last_name', 'courses_instructors.id')
                ->orderby('users.last_name')
                ->get();

        //$status = ShoppingDetailsStatuses::get();
        $estudiantes = DB::table('users')
                ->join('courses_students', 'users.id', '=', 'courses_students.users_id')
                ->join('courses_has_courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.updated_at', 'courses_students.id as id_est', 'courses_has_courses_students.id as id_relacion')
                ->where('courses_has_courses_students.courses_id', $id)
                ->get();
        $notas_alum = array();

        $cont = -1;

        for ($i=0; $i < count($estudiantes); $i++) { 
            $notas = DB::table('courses_notes')->where('courses_has_courses_students_id', $estudiantes[$i]->id_relacion)->get();

            $nota = 0;
            for ($j=0; $j < count($notas); $j++) { 
                $cont = count($notas);
                $nota = $nota+$notas[$j]->note;

            }
            $notas_alum[$i]['nota_f']    = number_format($nota / $cont,1);
            $notas_alum[$i]['id_student']= $estudiantes[$i]->id_relacion;
            
            


        }
        $subject = array();
        $i = 0;
        foreach ($materias as $key) {
            $subject[$i] = $key->id;
            $i++;
        }

        $horas = CoursesControlClasses::whereIn('courses_subjects_id', $subject)->get();


        
        if(Input::get('nombre')){
            
            return View::make('dashboard.continuada.body_materias')
                        ->with('materias', $materias);
            
        }else{

        return View::make('dashboard.continuada.detalle_curso')
                        ->with('curso', $curso)
                        ->with('inspectores', $inspectores)
                        ->with('instructors', $instructors)
                        ->with('categories', $categories)
                        ->with('estudiantes', $estudiantes)
                        ->with('notas', $notas_alum)
                        ->with('cont', $cont)
                        ->with('horas', $horas)
                        ->with('materias', $materias);
        }
    }
    
//     public function getPrueba(){
//
//         $estudiantes = DB::table('prueba')->get();
//         foreach ($estudiantes as $key) {
//                                      
//             DB::update('update prueba set password2  = ? where id = ?', array(base64_encode($key->password) ,$key->id));
//
//         }
//         echo "1";
//
//     }
     
    public function getAsignaralumnocurso() {
        
        $id = Input::get('id');

        //$shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        $curso = Courses::find($id);
        $materias = CoursesSubjects::Where("courses_id", $id)->get();
        $categories = CoursesCategories::all();

        $instructors = CoursesInstructors::all();
        $empresas = CoursesCompanies::all();
        
        $estudiantes = DB::table('users')
                ->join('courses_students', 'users.id', '=', 'courses_students.users_id')
                ->join('courses_has_courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'courses_students.id as id_s')
                ->where('courses_has_courses_students.courses_id', $id)
                ->get();
        
        $inscritos = array();
        $i = 0;
        foreach ($estudiantes as $key) {
            $inscritos[$i] = $key->id_s;
            $i++;
        }

        $alumnos = DB::table('users')
        ->join('courses_students', 'users.id', '=', 'courses_students.users_id')
        //->join('courses_has_courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
        ->select('courses_students.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2')
        // ->where('courses_has_courses_students.courses_id', $id)
        //->whereNotIn('courses_has_courses_students.courses_students_id', array($inscritos))
        ->orderby('users.last_name')
        ->get();
        
        if(Input::get('nombre')){
            
            return View::make('dashboard.continuada.body_materias')
                        ->with('materias', $materias);
            
        }else{

        return View::make('dashboard.continuada.asignar_alumno_curso')
        ->with('curso', $curso)
        ->with('instructors', $instructors)
        ->with('categories', $categories)
        ->with('empresas', $empresas)
        ->with('estudiantes', $estudiantes)
        ->with('alumnos', $alumnos)
        ->with('materias', $materias);
        }
    }
    public function getGuardaralumno(){
        $id = Input::get('id');
        $id_alumno = Input::get('id_alumno');
        $id_empresa = Input::get('id_empresa');
        $disponibles = Input::get('disponibles');

        $total = $disponibles - 1;

        DB::update('update courses set available  = ? where id = ?', array($total ,$id));

        DB::table('courses_has_courses_students')->insert(array(
            array('courses_id' => $id, 'courses_students_id' => $id_alumno, 'courses_companies_id' => $id_empresa, 'status' => 1),
        ));

        echo "1";


    }
    public function getDetallecursoina() {
        
        $id = Input::get('id');

        $curso = Courses::find($id);
        $materias = CoursesSubjects::Where("courses_id", $id)->get();
        $instructors = CoursesInstructors::all();
        $categories = CoursesCategories::all();
        
         $estudiantes = DB::table('users')
                ->join('courses_students', 'users.id', '=', 'courses_students.users_id')
                ->join('courses_has_courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.updated_at')
                ->where('courses_has_courses_students.courses_id', $id)
                ->get();
        
        if(Input::get('nombre')){
            
            return View::make('dashboard.continuada.body_materias')
                        ->with('materias', $materias);
            
        }else{

        return View::make('dashboard.continuada.detalle_curso_ina')
                        ->with('curso', $curso)
                        ->with('instructors', $instructors)
                        ->with('categories', $categories)
                        ->with('estudiantes', $estudiantes)
                        ->with('materias', $materias);
        }
        
    }
    
    public function getEvaluacionentrada() {
        
                $idalumno = Input::get('idalumno');
                $idcurso = Input::get('idcurso');
                
                $relacion = DB::table('courses_has_courses_students')
                ->select('courses_has_courses_students.id')
                ->where('courses_id', $idcurso)
                ->where('courses_students_id', $idalumno)
                ->get();
        
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta1');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 1;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta2');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 2;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta3');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 3;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta4');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 4;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta5');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 5;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta23');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 23;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta24');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 24;
                $respuestas->save();
                                
                    echo 1;
    }
    
    public function getEvaluacionsalida() {
        
                $idalumno = Input::get('idalumno');
                $idcurso = Input::get('idcurso');
                
                $relacion = DB::table('courses_has_courses_students')
                ->select('courses_has_courses_students.id')
                ->where('courses_id', $idcurso)
                ->where('courses_students_id', $idalumno)
                ->get();
                
                $materias = CoursesSubjects::Where("courses_id", $idcurso)->get();
                
                $notasprofesores = Input::get('notasprofesores');
                $indicador = 8;
                $i=0;
                $cont = 6;
                foreach($materias as $mat){
                    
                    for($i;$indicador>$i;$i++){
                        
                        $respuestas = new CoursesAnswers;
                        $respuestas->note = $notasprofesores[$i];
                        $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                        $respuestas->courses_instructors_id = $mat->CoursesInstructors->id;
                        $respuestas->courses_questions_id = $cont;
                        $respuestas->save();
                
                        $cont++;
                    }
                    $cont = 6;
                    $indicador = $indicador+8;
                    
                }
        
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta14not');
                $respuestas->observation = Input::get('pregunta14obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 14;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta15not');
                $respuestas->observation = Input::get('pregunta15obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 15;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta16not');
                $respuestas->observation = Input::get('pregunta16obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 16;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta17not');
                $respuestas->observation = Input::get('pregunta17obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 17;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta18not');
                $respuestas->observation = Input::get('pregunta18obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 18;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta19not');
                $respuestas->observation = Input::get('pregunta19obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 19;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->note = Input::get('pregunta20not');
                $respuestas->observation = Input::get('pregunta20obs');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 20;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;                
                $respuestas->observation = Input::get('pregunta21');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 21;
                $respuestas->save();
                
                $respuestas = new CoursesAnswers;
                $respuestas->observation = Input::get('pregunta22');
                $respuestas->courses_has_courses_students_id = $relacion[0]->id;
                $respuestas->courses_questions_id = 22;
                $respuestas->save();
                                
                    echo 1;
    }
    
    public function getDetalleinstructor(){

        $instructor = CoursesInstructors::where('id', Input::get('id'))->get();


        $estudios = DB::table('courses_instructors_studies')->where('courses_instructors_id', Input::get('id'))->get();
        
        $licencias = DB::table('courses_instructors_has_courses_technical_licenses')
                        ->where('courses_instructors_id', Input::get('id'))->get();

        $cursos = DB::table('courses_subjects')
            ->join('courses_instructors', 'courses_instructors.id', '=', 'courses_subjects.courses_instructors_id')
            ->join('courses', 'courses.id', '=', 'courses_subjects.courses_id')
            ->where('courses_subjects.courses_instructors_id', Input::get('id'))
            ->get();



        
        $tipos_licencias = DB::table('courses_technical_licenses')->get();

        $sql = "SELECT * FROM `courses_instructors_type_studies`";
        

        $tipo_estudios = DB::select($sql);

        $paises             = DB::table('countries')->get();
        $grupo_sanguineo    = DB::table('general_blood_groups')->get();
        $estado_civil       = DB::table('general_marital_statuses')->get();
        $ciudades           = DB::table('cities')->get();
        $generos            = DB::table('general_genres')->get();
        
        return View::make('dashboard.continuada.detalle_instructor')
        ->with('tipo_estudios', $tipo_estudios)
        ->with('paises', $paises)
        ->with('grupo_sanguineo', $grupo_sanguineo)
        ->with('estado_civil', $estado_civil)
        ->with('ciudades', $ciudades)
        ->with('generos', $generos)
        ->with('estudios', $estudios)
        ->with('licencias', $licencias)
        ->with('cursos', $cursos)
        ->with('tipos_licencias', $tipos_licencias)
        ->with('instructor', $instructor);
        
    }
    
    public function getEditarinstructor(){

        $idu = Input::get('idu');
        $idi = Input::get('idi');
        $nombre1 = Input::get('nombre1');
        $nombre2 = Input::get('nombre2');
        $apellido1 = Input::get('apellido1');
        $apellido2 = Input::get('apellido2');
        $tipo_identificacion = Input::get('tipo_identificacion');
        $n_documento = Input::get('n_documento');
        $lugar_nacimiento = Input::get('lugar_nacimiento');
        $fecha_nacimiento = Input::get('fecha_nacimiento');
        $estado_civil = Input::get('estado_civil');
        $genero = Input::get('genero');
        $grupo_sanguineo = Input::get('grupo_sanguineo');
        $direccion = Input::get('direccion');
        $telefono = Input::get('telefono');
        $celular = Input::get('celular');
        $email = Input::get('email');
        $ciudad_residencia = Input::get('ciudad_residencia');
        $numero_hijos = Input::get('numero_hijos');
                    
            DB::update('update users set name  = ?, name2 = ?, last_name = ?, last_name2 = ?, type_document = ?, document = ?, countries_id = ?, birthday = ?, general_marital_status_id = ?, general_genres_id = ?, general_blood_group_id = ?, address = ?, phone = ?, cell_phone = ?, email_institutional = ?, city_id = ? where id = ?', 
                    array($nombre1 ,$nombre2,$apellido1,$apellido2,$tipo_identificacion,$n_documento,$lugar_nacimiento,$fecha_nacimiento,$estado_civil,$genero,
                        $grupo_sanguineo,$direccion,$telefono,$celular,$email,$ciudad_residencia, $idu));
            
            DB::update('update courses_instructors set number_of_children  = ? where id = ?', 
                    array($numero_hijos ,$idi));
            
            echo "1";        
    }
    
    public function getCambiarpassinst() {
        
        $idu = Input::get('idu');
                
        $user = Users::find($idu);
            $user->password = Hash::make(Input::get('nueva'));
            if ($user->save()) {
                echo "1";
            } else {
                echo "3";
            }
    }
    
    public function getCambiarpassest() {
        
        $idu = Input::get('idu');
                
        $user = Users::find($idu);
            $user->password = Hash::make(Input::get('nueva'));
            if ($user->save()) {
                echo "1";
            } else {
                echo "3";
            }
    }
    
    public function getInstructores(){
        
        $sql = "SELECT * FROM `courses_instructors_type_studies`";
        

        $tipo_estudios = DB::select($sql);

        $paises             = DB::table('countries')->get();
        $grupo_sanguineo    = DB::table('general_blood_groups')->get();
        $estado_civil       = DB::table('general_marital_statuses')->get();
        $ciudades           = DB::table('cities')->get();
        $generos            = DB::table('general_genres')->get();
        
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.instructores')
        ->with('tipo_estudios', $tipo_estudios)
        ->with('paises', $paises)
        ->with('grupo_sanguineo', $grupo_sanguineo)
        ->with('estado_civil', $estado_civil)
        ->with('ciudades', $ciudades)
        ->with('generos', $generos)
        ->with('submenu_activo', 'Instructores')
        ->with('menu_activo', 'Educacion Continuada');
        
    }
    public function postIngresarnuevoinstructor(){
        if (Input::file('imagen_ins')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/instructores/';

            $nombre1                = Input::get('nombre1');
            $nombre2                = Input::get('nombre2');
            $apellido1              = Input::get('apellido1');
            $apellido2              = Input::get('apellido2');
            $tipo_identificacion    = Input::get('tipo_identificacion');
            $n_documento            = Input::get('n_documento');
            $lugar_nacimiento       = Input::get('lugar_nacimiento');
            $fecha_nacimiento       = Input::get('fecha_nacimiento');
            $estado_civil           = Input::get('estado_civil');
            $genero                 = Input::get('genero');
            $grupo_sanguineo        = Input::get('grupo_sanguineo');
            $nacionalidad           = Input::get('nacionalidad');
            $direccion              = Input::get('direccion');
            $telefono               = Input::get('telefono');
            $celular                = Input::get('celular');
            $email                  = Input::get('email');
            $ciudad_residencia      = Input::get('ciudad_residencia');
            $numero_hijos           = Input::get('numero_hijos');


            $ids                    = Input::get('ids');
            $titulos                = Input::get('titulos');
            $instituciones          = Input::get('instituciones');
            $fechas                 = Input::get('fechas');

            

            $ids =    explode(',', $ids);
            $titulos =    explode(',', $titulos);
            $instituciones =  explode(',', $instituciones);
            $fechas =     explode(',', $fechas);


           
            
            $filename = $n_documento."_".Input::file('imagen_ins')->getClientOriginalName();
            

            if (Input::get('n_documento')) {
                
                $uploadSuccess = Input::file('imagen_ins')->move($destinationPath, $filename);

                if ($uploadSuccess) {

                    $user = new Users;

                    
                    $user->name                         = Input::get('nombre1');
                    $user->name2                        = Input::get('nombre2');
                    $user->last_name                    = Input::get('apellido1');
                    $user->last_name2                   = Input::get('apellido2');
                    $user->email_institutional          = Input::get('email');
                    $user->email                        = Input::get('email');
                    $user->cell_phone                   = Input::get('celular');
                    $user->address                      = Input::get('direccion');
                    
                    $user->phone                        = Input::get('telefono');
                    $user->img                          = $destinationPath.$filename;
                    $user->img_profile                  = $destinationPath.$filename;
                    $user->img_min                      = $destinationPath.$filename;
                    
                    $user->type_document                = Input::get('tipo_identificacion');
                    $user->document                     = Input::get('n_documento');
                    $user->city_id                      = Input::get('ciudad_residencia');
                    $user->birthday                     = Input::get('fecha_nacimiento');

                    
                    
                    
                    $user->general_genres_id            = Input::get('genero');
                    $user->general_blood_group_id       = Input::get('grupo_sanguineo');
                    $user->countries_id                 = Input::get('lugar_nacimiento');
                    $user->general_marital_status_id    = Input::get('estado_civil');

                    
                    $user->save();
                    $last_user  = Users::all();
                    $id_user    = $last_user->last()->id;

                    $nuevo_inst = new CoursesInstructors;

                    $nuevo_inst->number_of_children = $numero_hijos;
                    $nuevo_inst->users_id           = $id_user;

                    $nuevo_inst->save();

                    $inst  = CoursesInstructors::all();
                    $inst  = $inst->last()->id;

                    $i = 0;
                    foreach ($ids as $key) {
                        
                        DB::table('courses_instructors_studies')->insert(array(
                        array('obtained_title' => $titulos[$i], 'institution' => $instituciones[$i], 'end_date' => $fechas[$i], 'courses_instructors_type_studies_id' => $key, 'courses_instructors_id' => $inst),
                        
                        ));

                        $i++;
                    }

                ;}
            }
            echo "1";

        } else {
            echo "2";
            
        }

    }
    public function getVerinstructores(){
        
        $cod = Input::get('cod');
        $nomb = Input::get('nomb');
        $doc = Input::get('doc');

        
        $instructores = DB::table('courses_instructors')
                ->join('users', 'users.id', '=', 'courses_instructors.users_id')
                ->select('users.name','users.last_name','users.document','courses_instructors.id', 'users.img', 'courses_instructors.created_at')
                ->where('courses_instructors.id', 'LIKE', '%' . Input::get('cod') . '%')
                ->Where(function($query)
                            {
                                $query->where('users.name', 'LIKE', '%' . Input::get('nomb') . '%')
                                      ->orwhere('users.last_name', 'LIKE', '%' . Input::get('nomb') . '%');
                            })
                ->where('users.document', 'LIKE', '%' . Input::get('doc') . '%')
                ->get();

        $nombres = CoursesInstructors::get();
        $documento = CoursesInstructors::get();


        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.continuada.table_ver_instructores')
            ->with('instructores', $instructores)
            ->with('nombres', $nombres)
            ->with('documento', $documento);
            exit();
        }

        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.ver_instructores')
        ->with('instructores', $instructores)
        ->with('nombres', $nombres)
        ->with('documento', $documento)
        ->with('submenu_activo', 'Instructores')
        ->with('menu_activo', 'Educacion Continuada');
        
    }
    public function getPdfinstructor(){
        
        include('assets/fpdf17/fpdf.php');

        // $pdf = new FPDF();
        // $pdf->AliasNbPages();
        // $pdf->AddPage();
        $instructor = CoursesInstructors::where('id', Input::get('id'))->get();

        $data = array();
        for ($i=0; $i < count($instructor); $i++) { 
            $data[$i]['nombre']             = $instructor[$i]->Users->name." ".$instructor[$i]->Users->name2." ".$instructor[$i]->Users->last_name." ".$instructor[$i]->Users->last_name2;
            $data[$i]['img_profile']        = $instructor[$i]->Users->img;
            $data[$i]['documento']          = $instructor[$i]->Users->document;
            $data[$i]['tipo_documento']     = $instructor[$i]->Users->type_document;
            $data[$i]['lugar_nacimiento']   = $instructor[$i]->Users->Countries->country;
            $data[$i]['f_nacimiento']       = $instructor[$i]->Users->birthday;
            $data[$i]['estado_civil']       = utf8_decode($instructor[$i]->Users->GeneralMaritalStatus->marital_status);;
            $data[$i]['genero']             = $instructor[$i]->Users->GeneralGenres->gender;
            $data[$i]['grupo_sanguineo']    = $instructor[$i]->Users->GeneralBloodGroup->group;
            $data[$i]['nacionalidad']       = $instructor[$i]->Users->Countries->country;
            $data[$i]['direccion']          = $instructor[$i]->Users->address;
            $data[$i]['telefono']           = $instructor[$i]->Users->phone;
            $data[$i]['celular']            = $instructor[$i]->Users->cell_phone;
            $data[$i]['email']              = $instructor[$i]->Users->email_institutional;
            $data[$i]['ciudad']             =  utf8_decode($instructor[$i]->Users->City->name);
            $data[$i]['hijos']              = $instructor[$i]->number_of_children;
            
            
        }

        $pdf = new FPDF('P', 'pt', 'letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        /* Encabezado */
        $rutaImagen = 'assets/img/logo2(2).png';
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Cell(100, 60, $pdf->Image($rutaImagen, 35, 35, 0, 50, ''), 1, 0, 'L');
        $pdf->Cell(300, 60, 'HOJA DE VIDA INSTRUCTORES', 1, 0, 'C');
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(150, 20, 'CODIGO: GA3-REG042', 1, 2);
        $pdf->Cell(150, 20, 'FECHA ACTUALIZACION: 02/10/2013', 1, 2);
        
        $pdf->Cell(150, 20, 'REVISION:1', 1, 0);
        $pdf->Ln(30);

        foreach ($data as $key) {

            $pdf->SetFont('Arial', 'B', 15);
            $pdf->Cell(160, 30, '', 0);
            $pdf->Cell(1, 30, 'Informacion Docente ', 0);
            $pdf->SetFont('Arial', '', 10);

            $pdf->ln();
            $pdf->Cell(1, 30, '', 0);
            $pdf->Cell(200, 30, $key['nombre'], 0);
            $pdf->ln();


            $pdf->Image($key['img_profile'], 500, 120, 60);

            $pdf->Cell(1, 20, '', 0);
            //$pdf->Cell(1, 20, 'Puesto Ciclo1: ' . $pCiclo1, 0);
            //$pdf->ln();
            $pdf->Cell(1, 30, '', 0);
            //$pdf->Cell(1, 30, 'Puesto Ciclo2: ' . $pCiclo2, 0);
            $pdf->ln(10);
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(20, 10, '', 0);
            $pdf->Ln(1);
            $pdf->Cell(1, 50, 'Informacion Personal', 0);
            $pdf->SetFont('Arial', '', 9.5);
            $pdf->SetFillColor(241, 241, 241);
            $pdf->Ln();
            $pdf->Cell(120, 16, 'Numero Identificacion :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['documento'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Tipo Identificacion :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['tipo_documento'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Lugar De Nacimiento :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['lugar_nacimiento'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Fecha De Nacimiento :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['f_nacimiento'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Estado Civil :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['estado_civil'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Genero :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['genero'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Grupo Sanguineo :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['grupo_sanguineo'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Nacionalidad :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['nacionalidad'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Direccion Residencia :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, ucfirst(mb_strtolower($key['direccion'])), 0, '', 'L');
            $pdf->Cell(160, 16, 'Telefono :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['telefono'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Celular :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['celular'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Email :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['email'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Ciudad Residencia :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['ciudad'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Numero Hijos :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16,$key['hijos'], 0, '', 'L');
            $pdf->Ln(25);



            $pdf->Cell(120, 16, 'Nombre Del Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, " ", 0, '', 'L');
            $pdf->Cell(160, 16, 'Ocupacion Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, " ", 0, '', 'L');
            $pdf->Ln(25);

            $pdf->Cell(120, 16, 'Telefono Oficina Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(160, 16, " ", 0, '', 'L');
            $pdf->Ln(25);
            $pdf->Cell(120, 16, 'Celular Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, " ", 0, '', 'L');

            $pdf->Ln(25);
            $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
            $pdf->Ln(1);


            $pdf->SetFont('Arial', 'B', 12);
            //$pdf->Cell(20, 10, '', 0);
            //$pdf->Ln();
            $pdf->Cell(1, 50, 'Informacion Academica', 0);
            
        }

        $pdf->Ln();

        $pdf->Output();
        
        
    }
    public function getAlumnos(){

        $programas = DB::table('courses')->where('status', 1)->get();
        $typedoc = AspirantsTypeDocuments::all();
        
        $estadocivil = AspirantsMaritalStatuses::all();
        $lugarnac = AspirantsBirthPlaces::all();
        $localidades = AspirantsLocations::all();
        $genero = AspirantsGenders::all();
        $estudios = DB::table('courses_students_studies')->get();
        $licencias = DB::table('courses_technical_licenses')->get();
        $nacionalidades = DB::table('countries')->get();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.alumnos')
        
        ->with('programas',$programas)
        ->with('typedoc',$typedoc)
        
        ->with('estadocivil',$estadocivil)
        ->with('lugarnac',$lugarnac)
        ->with('localidades',$localidades)
        ->with('estudios', $estudios)
        ->with('licencias', $licencias)
        ->with('nacionalidades', $nacionalidades)
        ->with('genero',$genero)
        
        ->with('submenu_activo', 'Alumnos')
        ->with('menu_activo', 'Educacion Continuada');
    }
    public function getVeralumnos(){
        $cod = Input::get('cod');
        $nomb = Input::get('nomb');
        $doc = Input::get('doc');

        $alumnos = DB::table('courses_students')
                ->join('users', 'users.id', '=', 'courses_students.users_id')
                ->select('users.name','users.name2','users.last_name','users.last_name2','users.document','courses_students.id', 'users.img_min', 'courses_students.created_at')
                ->where('courses_students.id', 'LIKE', '%' . Input::get('cod') . '%')
                ->Where(function($query)
                            {
                                $query->where('users.name', 'LIKE', '%' . Input::get('nomb') . '%')
                                      ->orwhere('users.last_name', 'LIKE', '%' . Input::get('nomb') . '%');
                            })
                ->where('users.document', 'LIKE', '%' . Input::get('doc') . '%')
                ->paginate(20);


        $count = DB::table('courses_students')
                ->join('users', 'users.id', '=', 'courses_students.users_id')
                ->select('users.name','users.name2','users.last_name','users.last_name2','users.document','courses_students.id', 'users.img_min', 'courses_students.created_at')
                ->where('courses_students.id', 'LIKE', '%' . Input::get('cod') . '%')
                ->Where(function($query)
                            {
                                $query->where('users.name', 'LIKE', '%' . Input::get('nomb') . '%')
                                      ->orwhere('users.last_name', 'LIKE', '%' . Input::get('nomb') . '%');
                            })
                ->where('users.document', 'LIKE', '%' . Input::get('doc') . '%')
                ->get();

        // $alumnos = CoursesStudents::all();
        $programas = DB::table('courses')->get();

        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.continuada.table_alumnos')
            ->with('programas',$programas)
            ->with('alumnos',$alumnos)
            ->with('num',$count)
            ->with('pag', $alumnos);
            exit();
        }

        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.ver_alumnos')
        
        ->with('programas',$programas)
        ->with('alumnos',$alumnos)
        ->with('pag', $alumnos)
        ->with('num',$count)
        ->with('submenu_activo', 'Alumnos')
        ->with('menu_activo', 'Educacion Continuada');
    }
    public function getDetallealumno() {
        
        $id = Input::get('id');
        $idcurso = Input::get('idcurso');
        
        $cursos = CoursesStudents::find($id)->Courses;
        $detallealumno = CoursesStudents::where('id', $id)->get();
        $cursos = CoursesStudents::find($id)->Courses;
        $instructors = CoursesInstructors::all();
        $categories = CoursesCategories::all();

        $typedoc = AspirantsTypeDocuments::all();
        $estadocivil = AspirantsMaritalStatuses::all();
        $lugarnac = City::orderBy('name', 'asc')->get();
        $localidades = AspirantsLocations::all();
        $materias = CoursesSubjects::Where("courses_id", $idcurso)->get();
        // $genero = GenderGoncept::all();
        $genero = DB::table('general_genres')->get();
        $estado_civil = DB::table('general_marital_statuses')->get();
        $nacionalidades = DB::table('countries')->get();

        $tipo_estudios = DB::table('courses_students_studies')->get();
        $tipos_licencias = DB::table('courses_technical_licenses')->get();

        $estudios = DB::table('courses_students_has_courses_students_studies')->where('courses_students_id', $id)->get();
        $licencias = DB::table('courses_technical_licenses_has_courses_students')->where('courses_students_id', $id)->get();


        $cod_asignacion = DB::table('courses_has_courses_students')
        ->where('courses_id', $idcurso)
        ->where('courses_students_id', $id)
        ->get();


        $preguntas = DB::table('courses_questions')
        ->where('courses_type_questions_id', 1)
        ->get();

        $data = array();
        $ocultar_btn                = "";

        if (count($cod_asignacion) != 0) {
            
            for ($i=0; $i < count($preguntas); $i++) { 

                $data[$i]["pregunta"] = $preguntas[$i]->question;
                $data[$i]["id_pregunta"] = $preguntas[$i]->id;

                $respuestas = DB::table('courses_answers')
                ->where('courses_questions_id', $preguntas[$i]->id)
                ->where('courses_has_courses_students_id', $cod_asignacion[0]->id)
                ->get();
                if (count($respuestas) == 0) {
                    
                    $data[$i]["respuesta"]      = "";
                    $data[$i]["id_respuesta"]   = "";
                    $data[$i]["funcion"]        = "";
                    $data[$i]["funcion2"]        = "";

                    $ocultar_btn                = "";
                }else{
                    $data[$i]["respuesta"]      = $respuestas[0]->observation;
                    $data[$i]["id_respuesta"]   = $respuestas[0]->id;
                    $data[$i]["funcion"]        = 'onblur="editar_respuesta_entrada('.$respuestas[0]->id.', '.$preguntas[$i]->id.')"';
                    $data[$i]["funcion2"]       = 'onfocus="capturar_valor_respuesta_entrada('.$respuestas[0]->id.', '.$preguntas[$i]->id.')"';
                    $ocultar_btn                = "display:none";
                }
            }
        }

        $preguntas2 = DB::table('courses_questions')
        ->where('courses_type_questions_id', 2)
        ->where('part', 2)
        ->get();

        $data_2 = array();
        $data_profesores = array();
        $data_profesores_resp = array();
        for ($j=0; $j < count($preguntas2); $j++) { 

            $data_2[$j]["pregunta"] = $preguntas2[$j]->question;
            $data_2[$j]["id_pregunta"] = $preguntas2[$j]->id;


            for ($k=0; $k < count($materias); $k++) { 
                $data_profesores[$k]['profesor'] = $materias[$k]->CoursesInstructors->Users->name." ".$materias[$k]->CoursesInstructors->Users->last_name; 
                $data_profesores[$k]['id_profesor'] = $materias[$k]->CoursesInstructors->id; 
                


                
                
            }


                // var_dump($data_profesores_resp[2]);
                // exit();

        }
        $respuestas2 = array();
        if (count($cod_asignacion) != 0) {
                $respuestas2 = DB::table('courses_answers')
                // ->where('courses_questions_id', $preguntas2[$j]->id)
                ->where('courses_has_courses_students_id', $cod_asignacion[0]->id)
                
                ->get();
        }


        return View::make('dashboard.continuada.detalle_alumno')
        ->with('cursos', $cursos)
        ->with('detallealumno', $detallealumno)
        ->with('instructors', $instructors)
        ->with('categories', $categories)


        ->with('typedoc',$typedoc)
        ->with('estadocivil',$estadocivil)
        ->with('lugarnac',$lugarnac)
        ->with('expedido',$lugarnac)
        ->with('localidades',$localidades)
        ->with('estudios', $estudios)
        ->with('nacionalidades', $nacionalidades)
        ->with('estado_civil', $estado_civil)
        ->with('generos',$genero)
        ->with('idcurso',$idcurso)
        ->with('tipo_estudios', $tipo_estudios)
        ->with('tipos_licencias', $tipos_licencias)
        ->with('licencias', $licencias)
        ->with('preguntas_entrada', $data)
        ->with('ocultar_btn', $ocultar_btn)
        ->with('preguntas_salida1', $data_2)
        ->with('preguntas_profesores', $data_profesores)
        ->with('preguntas_profesores_resp', $respuestas2)
        
        
        ->with('materias', $materias);

    }
    public function getEditarrespuestaentrada(){
        if (Input::get('texto_editar')) {
            
            $texto_editar = Input::get('texto_editar');
            $texto = Input::get('texto');
            DB::update('update courses_answers set observation = ? where id = ?', array($texto, $texto_editar));

            echo "1";
        }
    }
    public function getActualizardatosalumno(){

        $id_alumno = Input::get('id_alumno');
        $id_user = Input::get('id_user');
        $nombre1_edit = Input::get('nombre1_edit');
        $nombre2_edit = Input::get('nombre2_edit');
        $apellido1_edit = Input::get('apellido1_edit');
        $apellido2_edit = Input::get('apellido2_edit');
        $genero_edit = Input::get('genero_edit');
        $estado_civil_edit = Input::get('estado_civil_edit');
        $f_nacimiento_edit = Input::get('f_nacimiento_edit');
        $nacionalidad_edit = Input::get('nacionalidad_edit');
        $lugar_nac_edit = Input::get('lugar_nac_edit');
        $profesion_edit = Input::get('profesion_edit');
        $fecha_inscipcion_edit = Input::get('fecha_inscipcion_edit');
        $email_edit = Input::get('email_edit');
        $tipo_documento_edit = Input::get('tipo_documento_edit');
        $n_documento_edit = Input::get('n_documento_edit');
        $lugar_expedicion_edit = Input::get('lugar_expedicion_edit');
        $direccion_edit = Input::get('direccion_edit');
        $telefono_edit = Input::get('telefono_edit');
        $empresa_edit = Input::get('empresa_edit');
        $empresa_telefono_edit = Input::get('empresa_telefono_edit');
        $cargo_edit = Input::get('cargo_edit');
        $tiempo_experiencia_edit = Input::get('tiempo_experiencia_edit');
        $n_libreta_militar_edit = Input::get('n_libreta_militar_edit');
        $distrito_militar_edit = Input::get('distrito_militar_edit');


        // echo $id_alumno;
        // exit();
        DB::update('update users set    

                name = ?,
                name2 = ?,
                last_name = ?,
                last_name2 = ?,
                email_institutional = ?,
                email = ?,
                cell_phone = ?,
                address = ?,
                phone = ?,
                type_document = ?,
                document = ?,
                city_id = ?,
                birthday = ?,
                general_genres_id = ?,
                countries_id = ?,
                general_marital_status_id = ? 
                where id  = ?', array(
                $nombre1_edit,
                $nombre2_edit,
                $apellido1_edit,
                $apellido2_edit,
                $email_edit,
                $email_edit,
                $telefono_edit,
                $direccion_edit,
                $telefono_edit,
                $tipo_documento_edit,
                $n_documento_edit,
                $lugar_nac_edit,
                $f_nacimiento_edit,
                $genero_edit,
                $nacionalidad_edit,
                $estado_civil_edit,
                $id_user
        ));

        DB::update('update courses_students set 
                profession = ?,
                date_inscription = ?,
                residence_address = ?,
                employment_business = ?,
                employment_business_phone = ?,
                current_position = ?,
                time_experience = ?,
                birthplace = ?,
                military_card = ?,
                military_district = ?
                
                where id = ?', array(

                $profesion_edit,
                $fecha_inscipcion_edit,
                $direccion_edit,
                $empresa_edit,
                $empresa_telefono_edit,
                $cargo_edit,
                $tiempo_experiencia_edit,
                $lugar_expedicion_edit,
                $n_libreta_militar_edit,
                $distrito_militar_edit,
                $id_alumno

        ));

        echo "1";

    }
    public function getActualizarestudiosalumno(){
        $id_alumno = Input::get('id_alumno');
        $id_user = Input::get('id_user');
        $tipo_estudios = Input::get('tipo_estudios');
        $institucion_estudios = Input::get('institucion_estudios');
        $titulo_estudios = Input::get('titulo_estudios');


        DB::table('courses_students_has_courses_students_studies')->insert(
            array('courses_students_id' => $id_alumno, 'courses_students_studies_id' => $tipo_estudios, 'institution' => $institucion_estudios, 'title' => $titulo_estudios)
        );

        $estudios = DB::table('courses_students_has_courses_students_studies')->where('courses_students_id', $id_alumno)->get();

        $tipo_estudios2 = DB::table('courses_students_studies')->get();

        foreach ($estudios as $estudio) {
            echo '

                <tr class="center">
                    <td>
                        <select id="tipo_estudios" name="estudios" class="form-control">';

                            foreach($tipo_estudios2 as $tipo_estudio){
                                if($tipo_estudio->id == $estudio->courses_students_studies_id){
                                    echo  '<option value="'.$tipo_estudio->id.'" selected="">'.$tipo_estudio->course.'</option>';
                                }else{
                                    echo '<option value="'.$tipo_estudio->id.'" >'.$tipo_estudio->course.'</option>';
                                }
                            }   
                            
                 echo ' </select>
                        
                    </td>
                    <td>'.$estudio->institution.'</td>
                    <td>'.$estudio->title.'</td>
                    
                </tr>

            ';
        }

        
        
    }
    public function getActualizarlicenciasalumno(){
        $id_alumno = Input::get('id_alumno');
        $id_user = Input::get('id_user');
        $tipo_licencia = Input::get('tipo_licencia');
        $licencia_numero = Input::get('licencia_numero');
        $licencia_vencimiento = Input::get('licencia_vencimiento');



        DB::table('courses_technical_licenses_has_courses_students')->insert(
            array('courses_students_id' => $id_alumno, 'courses_technical_licenses_id' => $tipo_licencia, 'number_licence' => $licencia_numero, 'expiration_date' => $licencia_vencimiento)
        );

        $licencias = DB::table('courses_technical_licenses_has_courses_students')->where('courses_students_id', $id_alumno)->get();

        $tipo_licencias = DB::table('courses_technical_licenses')->get();

        foreach ($licencias as $licencia) {
            echo '

                <tr class="center">
                    <td>
                        <select id="tipo_estudios" name="estudios" class="form-control">';

                            foreach($tipo_licencias as $tipo_licencia){
                                if($tipo_licencia->id == $licencia->courses_technical_licenses_id){
                                    echo  '<option value="'.$tipo_licencia->id.'" selected="">'.$tipo_licencia->programs.'</option>';
                                }else{
                                    echo '<option value="'.$tipo_licencia->id.'" >'.$tipo_licencia->programs.'</option>';
                                }
                            }   
                            
                 echo ' </select>
                        
                    </td>
                    <td>'.$licencia->number_licence.'</td>
                    <td>'.$licencia->expiration_date.'</td>
                    
                </tr>

            ';
        }

        
        
    }
    public function getPdfalumno(){
        
        include('assets/fpdf17/fpdf.php');

        // $pdf = new FPDF();
        // $pdf->AliasNbPages();
        // $pdf->AddPage();
        $instructor = CoursesStudents::where('id', Input::get('id'))->get();

        $data = array();
        for ($i=0; $i < count($instructor); $i++) { 
            $data[$i]['nombre']             = utf8_decode($instructor[$i]->Users->name." ".$instructor[$i]->Users->name2." ".$instructor[$i]->Users->last_name." ".$instructor[$i]->Users->last_name2);
            $data[$i]['img_profile']        = $instructor[$i]->Users->img;
            $data[$i]['documento']          = $instructor[$i]->Users->document;
            $data[$i]['tipo_documento']     = utf8_decode($instructor[$i]->Users->type_document);
            $data[$i]['lugar_nacimiento']   = utf8_decode($instructor[$i]->Users->Countries->country);
            $data[$i]['f_nacimiento']       = $instructor[$i]->Users->birthday;
            $data[$i]['estado_civil']       = utf8_decode($instructor[$i]->Users->GeneralMaritalStatus->marital_status);;
            $data[$i]['genero']             = $instructor[$i]->Users->GeneralGenres->gender;
            $data[$i]['grupo_sanguineo']    = $instructor[$i]->Users->GeneralBloodGroup->group;
            $data[$i]['nacionalidad']       = utf8_decode($instructor[$i]->Users->Countries->country);
            $data[$i]['direccion']          = utf8_decode($instructor[$i]->Users->address);
            $data[$i]['telefono']           = $instructor[$i]->Users->phone;
            $data[$i]['celular']            = $instructor[$i]->Users->cell_phone;
            $data[$i]['email']              = $instructor[$i]->Users->email_institutional;
            $data[$i]['ciudad']             =  utf8_decode($instructor[$i]->Users->City->name);
            $data[$i]['hijos']              = $instructor[$i]->number_of_children;
            
            
        }

        $pdf = new FPDF('P', 'pt', 'letter');
        $pdf->AliasNbPages();
        $pdf->AddPage();

        /* Encabezado */
        $rutaImagen = 'assets/img/logo2(2).png';
        $pdf->SetFont('Arial', 'B', 11);
        $pdf->Cell(100, 60, $pdf->Image($rutaImagen, 35, 35, 0, 50, ''), 1, 0, 'L');
        $pdf->Cell(300, 60, 'HOJA DE VIDA ALUMNOS', 1, 0, 'C');
        $pdf->SetFont('Arial', 'B', 8);
        $pdf->Cell(150, 20, 'CODIGO: GA3-REG042', 1, 2);
        $pdf->Cell(150, 20, 'FECHA ACTUALIZACION: 02/10/2013', 1, 2);
        
        $pdf->Cell(150, 20, 'REVISION:1', 1, 0);
        $pdf->Ln(30);

        foreach ($data as $key) {

            $pdf->SetFont('Arial', 'B', 15);
            $pdf->Cell(160, 30, '', 0);
            $pdf->Cell(1, 30, 'Informacion Alumno ', 0);
            $pdf->SetFont('Arial', '', 10);

            $pdf->ln();
            $pdf->Cell(1, 30, '', 0);
            $pdf->Cell(200, 30, $key['nombre'], 0);
            $pdf->ln();


            $pdf->Image($key['img_profile'], 500, 120, 60);

            $pdf->Cell(1, 20, '', 0);
            //$pdf->Cell(1, 20, 'Puesto Ciclo1: ' . $pCiclo1, 0);
            //$pdf->ln();
            $pdf->Cell(1, 30, '', 0);
            //$pdf->Cell(1, 30, 'Puesto Ciclo2: ' . $pCiclo2, 0);
            $pdf->ln(10);
            $pdf->SetFont('Arial', 'B', 12);
            $pdf->Cell(20, 10, '', 0);
            $pdf->Ln(1);
            $pdf->Cell(1, 50, 'Informacion Personal', 0);
            $pdf->SetFont('Arial', '', 9.5);
            $pdf->SetFillColor(241, 241, 241);
            $pdf->Ln();
            $pdf->Cell(120, 16, 'Numero Identificacion :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['documento'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Tipo Identificacion :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['tipo_documento'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Lugar De Nacimiento :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['lugar_nacimiento'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Fecha De Nacimiento :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['f_nacimiento'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Estado Civil :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['estado_civil'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Genero :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['genero'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Grupo Sanguineo :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['grupo_sanguineo'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Nacionalidad :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['nacionalidad'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Direccion Residencia :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, ucfirst(mb_strtolower($key['direccion'])), 0, '', 'L');
            $pdf->Cell(160, 16, 'Telefono :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['telefono'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Celular :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['celular'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Email :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['email'], 0, '', 'L');
            $pdf->Ln(25);
            /* $pdf->Ln();
              $pdf->Cell(120, 16, '', 0, '', 'L', TRUE);
              $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
              $pdf->Cell(160, 16, '', 0, '', 'L', TRUE);
              $pdf->Ln(); */
            $pdf->Cell(120, 16, 'Ciudad Residencia :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, $key['ciudad'], 0, '', 'L');
            $pdf->Cell(160, 16, 'Numero Hijos :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16,$key['hijos'], 0, '', 'L');
            $pdf->Ln(25);



            $pdf->Cell(120, 16, 'Nombre Del Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, " ", 0, '', 'L');
            $pdf->Cell(160, 16, 'Ocupacion Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, " ", 0, '', 'L');
            $pdf->Ln(25);

            $pdf->Cell(120, 16, 'Telefono Oficina Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(160, 16, " ", 0, '', 'L');
            $pdf->Ln(25);
            $pdf->Cell(120, 16, 'Celular Conyuge :', 0, '', 'R', FALSE);
            $pdf->Cell(120, 16, " ", 0, '', 'L');

            $pdf->Ln(25);
            $pdf->Cell(120, 16, '', 0, '', 'L', FALSE);
            $pdf->Ln(1);


            $pdf->SetFont('Arial', 'B', 12);
            //$pdf->Cell(20, 10, '', 0);
            //$pdf->Ln();
            $pdf->Cell(1, 50, 'Informacion Academica', 0);
            
        }      
       

        $pdf->Ln();

        $pdf->Output();
        
        
    }
    public function postIngresarnuevoalumno(){
        if (Input::file('imagen_alumno')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/alumnos_continuada/';

            $nombre1                    = Input::get('nombre1');
            $nombre2                    = Input::get('nombre2');
            $segundo_nombre1            = Input::get('segundo_nombre1');
            $segundo_nombre2            = Input::get('segundo_nombre2');
            $email                      = Input::get('email');
            $genero                     = Input::get('genero');
            $estado_civil               = Input::get('estado_civil');
            $f_nacimiento               = Input::get('f_nacimiento');
            $nacionalidad               = Input::get('nacionalidad');
            $lugar_nacimiento           = Input::get('lugar_nacimiento');
            $profesion                  = Input::get('profesion');
            $curso_realizar             = Input::get('curso_realizar');
            $fecha_inscipcion           = Input::get('fecha_inscipcion');
            $tipo_documento             = Input::get('tipo_documento');
            $n_documento                = Input::get('n_documento');
            $lugar_expedicion           = Input::get('lugar_expedicion');
            $direccion                  = Input::get('direccion');
            $telefono                   = Input::get('telefono');
            $empresa                    = Input::get('empresa');
            $empresa_telefono           = Input::get('empresa_telefono');
            $cargo                      = Input::get('cargo');
            $tiempo_experiencia         = Input::get('tiempo_experiencia');
            $estudios                   = Input::get('estudios');
            $tecnico_institucion        = Input::get('tecnico_institucion');
            $tecnico_titulo             = Input::get('tecnico_titulo');
            $tecnologico_institucion    = Input::get('tecnologico_institucion');
            $tecnologico_titulo         = Input::get('tecnologico_titulo');
            $profesional_institucion    = Input::get('profesional_institucion');
            $profesional_titulo         = Input::get('profesional_titulo');
            $postgrado_institucion      = Input::get('postgrado_institucion');
            $profesional_titulo         = Input::get('profesional_titulo');
            $especializacion_institucion= Input::get('especializacion_institucion');
            $especializacion_titulo     = Input::get('especializacion_titulo');
            $seminarios_institucion     = Input::get('seminarios_institucion');
            $seminarios_titulo          = Input::get('seminarios_titulo');
            $tla_numero                 = Input::get('tla_numero');
            $tlh_numero                 = Input::get('tlh_numero');
            $term_numero                = Input::get('term_numero');
            $teei_numero                = Input::get('teei_numero');
            $teh_numero                 = Input::get('teh_numero');
            $temc_numero                = Input::get('temc_numero');
            $ait_numero                 = Input::get('ait_numero');
            $faa_numero                 = Input::get('faa_numero');
            $tesh_numero                = Input::get('tesh_numero');
            $otras_numero               = Input::get('otras_numero');
            
            $id_curso                   = Input::get('id_curso');
            $institucion                = Input::get('institucion');
            $titulo                     = Input::get('titulo');
            $id_licencia                = Input::get('id_licencia');
            $numerolic                  = Input::get('numerolic');
            $vencimiento                = Input::get('vencimiento');

            $id_curso       = explode(",", $id_curso);

            $institucion    = explode(",", $institucion);
            $titulo         = explode(",", $titulo);
            $id_licencia    = explode(",", $id_licencia);
            $numerolic      = explode(",", $numerolic);
            $vencimiento    = explode(",", $vencimiento);


            
            
            $filename = $n_documento."_".Input::file('imagen_alumno')->getClientOriginalName();
            

            if (Input::get('n_documento')) {
                
                $uploadSuccess = Input::file('imagen_alumno')->move($destinationPath, $filename);

                if ($uploadSuccess) {



                    $new_user = new Users;


                    $new_user->name                 = $nombre1;
                    $new_user->name2                = $nombre2;
                    $new_user->last_name            = $segundo_nombre1;
                    $new_user->last_name2           = $segundo_nombre2;
                    $new_user->email_institutional  = $email;
                    $new_user->email                = $email;
                    $new_user->cell_phone           = $telefono;
                    $new_user->address              = $direccion;
                    $new_user->phone                = $telefono;
                    $new_user->img                  = $destinationPath.$filename;
                    $new_user->img_profile          = "assets/img/profile/default.jpg";
                    $new_user->img_min              = "assets/img/avatar_small/default.jpg";
                    $new_user->status               = 1;
                    $new_user->type_document        = $tipo_documento;
                    $new_user->document             = $n_documento;
                    $new_user->city_id              = 8;
                    $new_user->tutorial             = 0;
                    $new_user->general_genres_id    = $genero;
                    $new_user->general_marital_status_id             = $estado_civil;
                    $new_user->birthday                   = $f_nacimiento;
                    $new_user->countries_id               = $nacionalidad;

                    $new_user->save();

                    $users = Users::get();
                    $bandera = $users->last()->id;

                    
                    $new_alumno = new CoursesStudents;

                    
                    // $new_alumno->marital_status             = $estado_civil;
                    $new_alumno->birthday                   = $f_nacimiento;
                    $new_alumno->countries_id               = $nacionalidad;
                    $new_alumno->profession                 = $profesion;
                    $new_alumno->date_inscription           = $fecha_inscipcion;
                    $new_alumno->residence_address          = $direccion;
                    $new_alumno->residence_phone            = $telefono;
                    $new_alumno->employment_business        = $empresa;
                    $new_alumno->employment_business_phone  = $empresa_telefono;
                    $new_alumno->current_position           = $cargo;
                    $new_alumno->time_experience            = $tiempo_experiencia;
                    $new_alumno->birthplace                 = $lugar_nacimiento;
                    $new_alumno->birthplace                 = $lugar_nacimiento;
                    $new_alumno->general_aviation_schools_id = -1;
                    
                    $new_alumno->users_id                   = $bandera;

                    $new_alumno->save();

                    $alumno = CoursesStudents::get();
                    $bandera2 = $alumno->last()->id;

                    $i = 0;
                    $j = 0;

                    foreach ($id_curso as $key) {
                        if ($institucion[$i] != "") {
                            DB::table('courses_students_has_courses_students_studies')->insert(array(
                                array('courses_students_id' => $bandera2, 'courses_students_studies_id' => $key, 'institution' => $institucion[$i], 'title' => $titulo[$i]),
                                    
                            ));
                            
                        }


                        $i++;
                    }

                    foreach ($id_licencia as $key2) {
                        if ($numerolic[$j] != "") {
                            DB::table('courses_technical_licenses_has_courses_students')->insert(array(
                                array('courses_students_id' => $bandera2, 'courses_technical_licenses_id' => $key2, 'number_licence' => $numerolic[$j], 'expiration_date' => $vencimiento[$j]),
                                    
                            ));
                            
                        }


                        $j++;
                    }


                    echo "1";
                    
                }
            }

        } else {
            echo "2";
            
        }
    }
    public function getEmpresas(){
        
        $companies = CoursesCompanies::all();        

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($companies); $i++) {
            $fecha = date($companies[$i]->created_at);
            $data[$i]["id"] = 'AGE' . str_pad($companies[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["name"] = $companies[$i]->name;
            $data[$i]["name_contact"] = $companies[$i]->name_contact;
            $data[$i]["address"] = $companies[$i]->address;
            $data[$i]["phone"] = $companies[$i]->phone;
            $data[$i]["email"] = $companies[$i]->email;
            $data[$i]["observations"] = $companies[$i]->observations;
            $data[$i]["date_fair"] = $companies[$i]->date_fair;
            $data[$i]["observation"] = $companies[$i]->observations;

            $data[$i]["hid"] = $companies[$i]->id;

        }
        
        if (isset($_GET['nombre'])) {
               $hola = View::make('dashboard.aspirantes.tabla_directorio')
                                ->with('datos_agendas', $data);
              
               //return PDF::load($hola, 'A4', 'portrait')->show();
            } else { 
                return View::make('dashboard.index')
                ->with('container', 'dashboard.continuada.empresas')
                ->with('datos_agendas', $data)
                ->with('submenu_activo', 'Empresas')
                ->with('menu_activo', 'Educacion Continuada');
            }
        
    }
    public function getDetalleempresa(){

        $detalle_empresa  =  CoursesCompanies::where('id', Input::get('id'))->get();

        return View::make('dashboard.continuada.detalle_empresa')
        ->with('detalle_empresa', $detalle_empresa);

    }
    public function getSeguimientosempresa(){
        
        $seg_empresa  = CoursesCompaniesTraces::where('courses_companies_id', Input::get('id'))->get();

        return View::make('dashboard.continuada.seguimientos_empresa')
        ->with('id_empresa', Input::get('id'))
        ->with('detalle_empresas', $seg_empresa);

    }
    public function getIngresarempresa(){

        $nombre = Input::get('nombre');
        $contacto = Input::get('contacto');
        $direccion = Input::get('direccion');
        $telefono = Input::get('telefono');
        $email = Input::get('email');
        $observaciones = Input::get('observaciones');
            
        if (Input::get('id')) {
            $id = Input::get('id');
            DB::update('update courses_companies set name  = ?, name_contact = ?, address = ?, phone = ?, email = ?, observations = ?, date_fair = ?, name_rector = ? where id = ?', array($nombre ,$contacto,$direccion,$telefono,$email,$observaciones,$feria,$rector,$id));
            echo "1";
        }else{
            
            $nueva_empresa  = new CoursesCompanies;

            $nueva_empresa->name = $nombre;
            $nueva_empresa->name_contact = $contacto;
            $nueva_empresa->address = $direccion;
            $nueva_empresa->phone = $telefono;
            $nueva_empresa->email = $email;
            $nueva_empresa->observations = $observaciones;
            $nueva_empresa->users_id = Auth::user()->id;

            if($nueva_empresa->save()){
                echo "1";
            }else{
                echo "2";
            }
        }
    }
    public function getIngresarseguimientos(){

        $observaciones = Input::get('observaciones');
        $id = Input::get('id');

            $nuevo_seg  = new CoursesCompaniesTraces;

            $nuevo_seg->observation = $observaciones;
            $nuevo_seg->courses_companies_id = $id;
            $nuevo_seg->users_id = Auth::user()->id;

            if($nuevo_seg->save()){
                echo "1";
            }else{
                echo "2";
            }
        
    }
    public function getEstadisticas() {
            
        $sql_estados = "SELECT status, COUNT( * ) AS dato 
        FROM `courses` GROUP BY status ORDER BY dato DESC";
        
        $sql_cantidad = "SELECT courses_has_courses_students.courses_id, courses.name_curso, COUNT( * ) AS dato 
        FROM `courses_has_courses_students` 
        JOIN courses on (courses.id = courses_has_courses_students.courses_id) 
        GROUP BY courses.name_curso ORDER BY dato DESC";

        $sql_tipos = "SELECT equipment_types.id, equipment_types.type , COUNT( * ) AS dato 
        FROM `equipment_inventories` join equipment_types on (equipment_types.id = equipment_inventories.equipment_types_id) 
        WHERE equipment_types.id NOT IN (8,9)
        GROUP BY equipment_types.id ORDER BY dato DESC";

        $sql_grupos = "SELECT equipment_groups.id, equipment_groups.name_group , COUNT( * ) AS dato FROM `equipment_groups` 
        join equipment_groups_has_equipment_inventories on (equipment_groups.id = equipment_groups_has_equipment_inventories.equipment_groups_id) 
        GROUP BY equipment_groups.id ORDER BY dato DESC";

        $sql_procesos = "SELECT processes.acronyms,COUNT( * ) AS dato FROM `equipments`
        join equipment_inventories_users on (equipment_inventories_users.equipments_id = equipments.id)
        join users on (equipment_inventories_users.users_id = users.id)
        join profiles_users on (profiles_users.users_id = users.id)
        join profiles on (profiles.id = profiles_users.profiles_id)
        join processes on (processes.id = profiles.processes_id)
        GROUP BY processes.acronyms ORDER BY dato DESC";

        $sql_impresoras = "SELECT processes.acronyms,COUNT( * ) AS dato FROM equipment_inventories
        join equipments_has_equipment_inventories on (equipments_has_equipment_inventories.equipment_inventories_id = equipment_inventories.id)
        join equipments on (equipments_has_equipment_inventories.equipments_id = equipments.id)
        join equipment_inventories_users on (equipment_inventories_users.equipments_id = equipments.id)
        join users on (equipment_inventories_users.users_id = users.id)
        join profiles_users on (profiles_users.users_id = users.id)
        join profiles on (profiles.id = profiles_users.profiles_id)
        join processes on (processes.id = profiles.processes_id)
        where equipment_inventories.equipment_types_id = 3
        GROUP BY processes.acronyms ORDER BY dato DESC";

        $sql_licencias = "SELECT equipment_type_licenses.product,COUNT( * ) AS dato FROM `equipment_licensings` 
        join equipment_type_licenses on (equipment_type_licenses.id = equipment_licensings.equipment_type_licenses_id)
        GROUP BY equipment_type_licenses.product ORDER BY dato DESC";


        
        if (Input::get('fecha_inicio')) {
                $sql_tipos = "SELECT equipment_types.id, equipment_types.type , COUNT( * ) AS dato 
                FROM `equipment_inventories` join equipment_types on (equipment_types.id = equipment_inventories.equipment_types_id) 
                GROUP BY equipment_types.id ORDER BY id DESC";
        }

        $cons_estados = DB::select($sql_estados);
        $cons_cantidad = DB::select($sql_cantidad);
        $cons_grupos = DB::select($sql_grupos);
        $cons_procesos = DB::select($sql_procesos);
        $cons_impresoras = DB::select($sql_impresoras);
        $cons_licencias = DB::select($sql_licencias);

        if (Input::get('fecha_inicio')) {
            return View::make('dashboard.equipos.scrip_graficos')
            // ->with('container', 'dashboard.equipos.estadisticas')
            ->with('data_estados', $cons_estados)
            ->with('data_tipos', $cons_tipos)
            ->with('data_grupos', $cons_grupos)
            ->with('data_procesos', $cons_procesos)
            ->with('data_impresoras', $cons_impresoras)
            ->with('data_licencias', $cons_licencias);
            // ->with('submenu_activo', 'Estadísticas')
            // ->with('menu_activo', 'Equipos'); 
            exit();   
        }

        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.estadisticas')
        ->with('data_estados', $cons_estados)
        ->with('data_cantidad', $cons_cantidad)
        ->with('data_grupos', $cons_grupos)
        ->with('data_procesos', $cons_procesos)
        ->with('data_impresoras', $cons_impresoras)
        ->with('data_licencias', $cons_licencias)
        ->with('submenu_activo', 'Estadísticas')
        ->with('menu_activo', 'Educacion Continuada');      
    }
    //funcion para generar pdf de informes
    public function getGenerardiplomacont(){
    
        $id_alumno = Input::get('id_alumno');
        $id_curso = Input::get('id_curso');

               $datos       = CoursesStudents::where('id', $id_alumno)->get();
               $datos_curso = Courses::where('id', $id_curso)->get();

                
                
                include('assets/fpdf17/fpdf.php');
        
                $pdf = new FPDF("L");
				$pdf->AddPage();
				$pdf->SetFont('Arial','',18);
				$pdf->Image('assets/img/continuada/diploma.jpg' , 1, 1, 295, 208,'JPG');
				$pdf->SetXY(110,80);
				$pdf->Write(5,$datos[0]->Users->name." ".$datos[0]->Users->name2." ".$datos[0]->Users->last_name." ".$datos[0]->Users->last_name2);
				$pdf->SetFont('Arial','',12);
                                $pdf->SetXY(141,98);
				$pdf->Write(5,$datos[0]->Users->document);
				$pdf->SetXY(210,98);
				$pdf->Write(5,"Bogota");
                                $pdf->SetFont('Arial','',18);
                                $pdf->SetXY(135,125);
				$pdf->Write(5,$datos_curso[0]->name_curso);				
                                $pdf->SetFont('Arial','',12);
                                $pdf->SetXY(138,144);
				$pdf->Write(5,"el dia 05 de mayo de 2015");
                                $pdf->SetFont('Arial','',10);
                                $pdf->SetXY(159,166);
				$pdf->Write(5,"90");
				$pdf->Output("diploma.pdf","I");

    }
    
    public function getInformes() {

        $permission = $this->permission_control("52");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }

        $user = Users::find(Auth::user()->id);
        $estados = AspirantsStatusManagements::all();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.continuada.informes')
                            ->with('user', $user)
                            ->with('estados', $estados)
                            ->with('submenu_activo', 'Informes')
                            ->with('menu_activo', 'Educacion Continuada');
        }
    }
    
    //funcion para generar pdf de informes
    public function getGenerarpdfcont(){ 
    
        $id = Input::get('id');
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        $cont = 0;
        switch ($id)
        {
            
            case "1":

                $sql = "SELECT status, COUNT( * ) AS dato
                        FROM `courses`
                        
                        GROUP BY status ORDER BY dato DESC";

                $datos = DB::select($sql);

                $html= View::make('dashboard.continuada.informes.pdfestados')
                            ->with('datos', $datos)
                            ->with('cont', $cont);
                    PDF::load($html, 'A4', 'portrait')->show();
                
            break;
        
            case "2":
                
                $sql = "SELECT courses_has_courses_students.courses_id, courses.name_curso, COUNT( * ) AS dato 
                        FROM `courses_has_courses_students` 
                        JOIN courses on (courses.id = courses_has_courses_students.courses_id) 
                        GROUP BY courses.name_curso ORDER BY dato DESC";
                                

                $datos = DB::select($sql);

                $html= View::make('dashboard.continuada.informes.pdfestudiantes')
                            ->with('datos', $datos)
                            ->with('cont', $cont);
                    PDF::load($html, 'A4', 'portrait')->show();
                
            break;
        
            case "3":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_programs_id, aspirants_programs.name, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_programs_id, aspirants_programs.name, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC";
                    
                }

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdfprogramas')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "4":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC";
                    
                }                                

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdfcampanas')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "5":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC";
                    
                }                   

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdfestados')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "6":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC";
                    
                }                   

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdffuentes')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "7":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                    
                }

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdfjornadas')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "8":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                    
                }
                
                    $sql = "SELECT id, YEAR(CURDATE())-YEAR(birthday) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(birthday,'%m-%d'), 0, -1) AS edad_actual
                            FROM aspirants_informations
                            WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

                    $datos = DB::select($sql);
                    
                    $rango1=0;
                    $rango2=0;
                    $rango3=0;
                    $rango4=0;
                    
                    foreach ($datos as $dat){
                        
                        if($dat->edad_actual<18){
                            $rango1=$rango1+1;
                        }elseif($dat->edad_actual>17 && $dat->edad_actual<26){ 
                            $rango2=$rango2+1;
                        }elseif($dat->edad_actual>25 && $dat->edad_actual<31){
                            $rango3=$rango3+1;
                        }elseif($dat->edad_actual>30){
                            $rango4=$rango4+1;
                        }
                        
                    }
                    

                    $html= View::make('dashboard.aspirantes.informes.pdfedades')
                                ->with('rango1', $rango1)
                                ->with('rango2', $rango2)
                                ->with('rango3', $rango3)
                                ->with('rango4', $rango4)
                                ->with('cont', count($datos));
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "9":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC";
                    
                }

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdfgenero')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
            
            case "10":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC";
                    
                }

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdfciudad')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
        
            case "11":
                
                if($estado != 0){
                    
                    $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC";
                    
                }else{
                    
                    $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC";
                    
                }

                    $datos = DB::select($sql);

                    $html= View::make('dashboard.aspirantes.informes.pdflocalidad')
                                ->with('datos', $datos)
                                ->with('cont', $cont);
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
            
        }

    }
    
    public function getCertificadodescargado(){
        
        $id = Input::get('id');

        DB::update('update courses_has_courses_students set status_certification = ? where id = ?', array(0 ,$id));
        
        DB::update('update courses_request_certificates set status = ? where courses_has_courses_students_id = ?', array(5 ,$id));

        echo "1";

    }
    
    public function getGestioncertificado(){
        $id = Input::get('id');
        $libro = Input::get('libro');
        $folio = Input::get('folio');
        $status = Input::get('status');

        DB::update('update courses_has_courses_students set status_certification = ? where id = ?', array($status ,$id));
        
        DB::update('update courses_request_certificates set status = ?, book = ?, folio = ? where courses_has_courses_students_id = ?', array($status, $libro, $folio, $id));

        echo "1";

    }
    
    public function getCertificados() {
        
        $user = Users::find(Auth::user()->id);
        $categories = CoursesCategories::all();
        $instructors = CoursesInstructors::all();
        $detallealumno = CoursesStudents::where('users_id', Auth::user()->id)->get();
        $cursos = DB::table('courses')
                ->join('courses_has_courses_students', 'courses_has_courses_students.courses_id', '=', 'courses.id')
                ->join('courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
                ->join('courses_request_certificates', 'courses_request_certificates.courses_has_courses_students_id', '=', 'courses_has_courses_students.id')
                ->select('courses.id', 'courses.name_curso', 'courses.start_date', 'courses.end_date', 'courses_students.id as id_alumno', 'courses_has_courses_students.id as id_asignacion', 'courses_has_courses_students.status_certification')
                ->where('courses_request_certificates.status', 1)
                ->get();
        //$cursos = CoursesStudents::find($detallealumno[0]->id)->Courses;
        //$cursos = Courses::Where("status", 1)->get();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.continuada.certificados')
            ->with('user', $user)
            ->with('categories', $categories)
            ->with('instructors', $instructors)
            ->with('cursos', $cursos)
            ->with('submenu_activo', 'Certificados')
            ->with('menu_activo', 'Educacion Continuada');
        }
    }
    
    public function getSolicitudcertificado(){
        $id = Input::get('id');        

        DB::update('update courses_has_courses_students set status_certification = ? where id = ?', array(1 ,$id));

        DB::table('courses_request_certificates')->insert(array(
            array('courses_has_courses_students_id' => $id, 'status' => 1),
        ));

        echo "1";

    }
    
    public function getMiscursos() {
        
        $user = Users::find(Auth::user()->id);
        $categories = CoursesCategories::all();
        $instructors = CoursesInstructors::all();
        $detallealumno = CoursesStudents::where('users_id', Auth::user()->id)->get();
        $cursos = DB::table('courses')
                ->join('courses_has_courses_students', 'courses_has_courses_students.courses_id', '=', 'courses.id')
                ->join('courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
                ->select('courses.id', 'courses.name_curso', 'courses.start_date', 'courses.end_date', 'courses_students.id as id_alumno', 'courses_has_courses_students.id as id_asignacion', 'courses_has_courses_students.status_certification')
                ->where('courses_students.id', $detallealumno[0]->id)
                ->get();
        //$cursos = CoursesStudents::find($detallealumno[0]->id)->Courses;
        //$cursos = Courses::Where("status", 1)->get();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.continuada.mis_cursos')
            ->with('user', $user)
            ->with('categories', $categories)
            ->with('instructors', $instructors)
            ->with('cursos', $cursos)
            ->with('submenu_activo', 'Mis Cursos')
            ->with('menu_activo', 'Educacion Continuada');
        }
    }
    
    public function getDetallemicurso() {
        
        $id = Input::get('id');

        //$shoppings_details = ShoppingDetails::where('shoppings_id', $id)->get();

        $curso = Courses::find($id);
        $materias = CoursesSubjects::Where("courses_id", $id)->get();
        $categories = CoursesCategories::all();

        $instructors = CoursesInstructors::all();
        //$status = ShoppingDetailsStatuses::get();
        $estudiantes = DB::table('users')
                ->join('courses_students', 'users.id', '=', 'courses_students.users_id')
                ->join('courses_has_courses_students', 'courses_has_courses_students.courses_students_id', '=', 'courses_students.id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2')
                ->where('courses_has_courses_students.courses_id', $id)
                ->get();
        
        if(Input::get('nombre')){
            
            return View::make('dashboard.continuada.body_materias')
                        ->with('materias', $materias);
            
        }else{

        return View::make('dashboard.continuada.detalle_mis_cursos')
                        ->with('curso', $curso)
                        ->with('instructors', $instructors)
                        ->with('categories', $categories)
                        ->with('estudiantes', $estudiantes)
                        ->with('materias', $materias);
        }
    }
    
    public function getGenerarmidiplomacont(){
    
        $id = Input::get('id');

            $nombre = strtoupper(Auth::user()->name." ".Auth::user()->name2." ".Auth::user()->last_name." ".Auth::user()->last_name2);
            $documento = strtoupper(Auth::user()->document);

        
                $sql = "SELECT*FROM `users`
                        GROUP BY status";

                $datos = DB::select($sql);
                
                                include('assets/fpdf17/fpdf.php');
        
                                $pdf = new FPDF("L");
                $pdf->AddPage();
                $pdf->SetFont('Arial','',18);
                $pdf->Image('assets/img/continuada/diploma.jpg' , 1, 1, 295, 208,'JPG');
                $pdf->SetXY(110,80);
                $pdf->Write(5,$nombre);
                $pdf->SetFont('Arial','',12);
                                $pdf->SetXY(141,98);
                $pdf->Write(5,$documento);
                $pdf->SetXY(210,98);
                $pdf->Write(5,"Bogota");
                                $pdf->SetFont('Arial','',18);
                                $pdf->SetXY(135,125);
                $pdf->Write(5,"Curso virtual");             
                                $pdf->SetFont('Arial','',12);
                                $pdf->SetXY(138,144);
                $pdf->Write(5,"el dia 05 de mayo de 2015");
                                $pdf->SetFont('Arial','',10);
                                $pdf->SetXY(159,166);
                $pdf->Write(5,"90");
                $pdf->Output("diploma.pdf","I");

    }
    //funcion para consultar los datos del curso segun el codigo
    public function getConsultarcodigoedit()
    {
    		//get POST data
                    $texto = Input::get('texto');
                    
                    $sql = "SELECT * FROM courses WHERE coding LIKE '%".$texto."%' ORDER BY coding DESC";

                    $datos = DB::select($sql);
                    
                    if($datos){
                        for($i=0;count($datos)>$i;$i++)
                            {
                            ?>
                            <a onclick="agregarCursoe('<?php echo $datos[$i]->coding; ?>', '<?php echo $datos[$i]->name_curso; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                            <div class="display_box" align="left">
                            <div style="margin-right:6px;"><b><?php echo $datos[$i]->coding; ?></b></div></div>
                            </a>
                            <?php
                            }
                    }else{
                        echo 0;
                    }
                        
    }
    //funcion para consultar los datos del curso segun el Nombre
    public function getConsultarnombredit()
	{
		//get POST data
                $name = Input::get('texto');
                
                $sql = "SELECT * FROM courses WHERE name_curso LIKE '%".$name."%' ORDER BY name_curso DESC";

                $datos = DB::select($sql);
                
                if($datos){
                    for($i=0;count($datos)>$i;$i++)
                        {
                        ?>
                        <a onclick="agregarCursoe('<?php echo $datos[$i]->coding; ?>', '<?php echo $datos[$i]->name_curso; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                        <div class="display_box" align="left">
                        <div style="margin-right:6px;"><b><?php echo $datos[$i]->name_curso; ?></b></div></div>
                        </a>
                        <?php
                        }
                }else{
                    echo 0;
                }

	}
    //funcion para consultar los datos del proveedor en el formulario de editar
    public function getConsultarcursose()
	{
		//get POST data
                $cod = Input::get('codigo');
                
                $sql = "SELECT * FROM courses WHERE coding = '".$cod."'";

                $datos = DB::select($sql);
                
                if($datos){
                    return $datos;
                }else{
                    echo 0;
                }
                    
	}
    public function getGestionblog()
    {
        
        return View::make('dashboard.index')
            ->with('container', 'dashboard.continuada.gestion_blog')
            ->with('submenu_activo', 'Gestion Blog')
            ->with('menu_activo', 'Educacion Continuada');           
    }
    public function getSubirnotas(){
        
        $my_id = Auth::user()->id;
        $my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get(); 

        $cursos = CoursesSubjects::where('courses_instructors_id', '=',$my_idins[0]->id)
        ->get();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.subir_notas')
        ->with('cursos', $cursos)
        ->with('submenu_activo', 'Gerstión Cursos')
        ->with('menu_activo', 'Educacion Continuada');
    }
    public function getSubirfallas(){
        
        $my_id = Auth::user()->id;
        $my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get(); 

        $cursos = CoursesSubjects::where('courses_instructors_id', '=',$my_idins[0]->id)
        
        ->get();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.continuada.subir_fallas')
        ->with('cursos', $cursos)
        ->with('submenu_activo', 'Gerstión Cursos')
        ->with('menu_activo', 'Educacion Continuada');
    }
    public function getCargaralumnosnotas(){
        
        $alumnos = DB::table('courses_has_courses_students')->where('courses_id', Input::get('id_curso'))
        ->join('courses_students', 'courses_students.id', '=', 'courses_has_courses_students.courses_students_id')
        ->join('users', 'users.id', '=', 'courses_students.users_id')
        ->select('users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'courses_students.id', 'courses_has_courses_students.id as id_asig')
        ->get();

        $data = array(); 

        $notas = DB::table('courses_notes')->where('courses_has_courses_students_id', $alumnos[0]->id_asig)->get();

        for ($i=0; $i < count($alumnos); $i++) { 
            $notas = DB::table('courses_notes')->where('courses_has_courses_students_id', $alumnos[$i]->id_asig)->get();
            $j = 0;
            foreach ($notas as $key) {
                
                $data[$i]['id'] = $key->id;

                if ($key->note == "") {
                    
                    $data[$i]['valor'] = 100;
                    
                }else{
                    
                    $data[$i]['valor'] = $key->note;
                }

                $j++;
            }
        }

        return View::make('dashboard.continuada.subir_notas_alumnos')
        ->with('curso', Input::get('id_curso'))
        ->with('notas', $data)
        ->with('notas2', $notas)
        ->with('alumnos', $alumnos);
        
        
    }
    public function getCargaralumnosfallas(){
        
        $alumnos = DB::table('courses_has_courses_students')->where('courses_id', Input::get('id_curso'))
        ->join('courses_students', 'courses_students.id', '=', 'courses_has_courses_students.courses_students_id')
        ->join('users', 'users.id', '=', 'courses_students.users_id')
        ->select('users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'courses_students.id', 'courses_has_courses_students.id as id_asig')
        ->get(); 

        $data = array();
        
        $fechas = DB::table('courses_fails')->where('courses_has_courses_students_id', $alumnos[0]->id_asig)->get();


        for ($i=0; $i < count($alumnos); $i++) { 
            $fallas = DB::table('courses_fails')->where('courses_has_courses_students_id', $alumnos[$i]->id_asig)->get();
            $j = 0;
            foreach ($fallas as $key) {
                
                $data[$i][$j]['id'] = $key->id;
                if ($key->fail == 1) {
                    $data[$i][$j]['falla'] = 'icon-ok';
                    $data[$i][$j]['valor'] = 0;
                    
                }else{
                    $data[$i][$j]['falla'] = 'icon-remove';
                    $data[$i][$j]['valor'] = 1;
                }

                $j++;
            }
        }
            

        return View::make('dashboard.continuada.subir_fallas_alumnos')
        ->with('fecha_nueva', Input::get('fecha'))
        ->with('curso', Input::get('id_curso'))
        ->with('fechas', $fechas)
        ->with('fallas', $data)
        ->with('alumnos', $alumnos);
        
        
    }
    public function postGuardarfallas(){

        if (Input::get('fecha') && Input::get('id_alum') && Input::get('valor') && Input::get('curso')) {
            $id_alum = Input::get('id_alum');
            $valor   = Input::get('valor');
            $j = 0;
            $my_id = Auth::user()->id;
            $my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get();

            foreach ($id_alum as $key) {
                $alumnos = DB::table('courses_has_courses_students')
                ->where('courses_id', Input::get('curso'))
                ->where('courses_students_id', $key)
                ->get();

               
                DB::table('courses_fails')->insert(array(
                    array('fail' => $valor[$j], 'date_faill' => Input::get('fecha'), 'courses_has_courses_students_id' => $alumnos[0]->id, 'courses_instructors_id' => $my_idins[0]->id),
                            
                ));
                    
                $j++;
            }
            echo "1";
        }else{
            echo "2";
        }
        
        
        
    }
    public function postGuardarnotas(){


        if (Input::get('id_alum') && Input::get('valor') && Input::get('curso')) {

            $id_alum = Input::get('id_alum');
            $valor   = Input::get('valor');
            $j = 0;
            $my_id = Auth::user()->id;
            $my_idins = DB::table('courses_instructors')->where('users_id', $my_id)->select('id')->get();

            foreach ($id_alum as $key) {

                $alumnos = DB::table('courses_has_courses_students')
                ->where('courses_id', Input::get('curso'))
                ->where('courses_students_id', $key)
                ->get();

                // var_dump($alumnos);
                // exit();
               
                DB::table('courses_notes')->insert(array(
                    array('note' => $valor[$j], 'courses_has_courses_students_id' => $alumnos[0]->id, 'courses_instructors_id' => $my_idins[0]->id),
                            
                ));
                    
                $j++;
            }
            echo "1";
        }else{
            echo "2";

        }
        
        
        
    }
    public function postGuardarnotas2(){


        if (Input::get('id_alum') && Input::get('valor') && Input::get('curso') && Input::get('id_prof')) {

            $id_alum = Input::get('id_alum');
            $id_prof = Input::get('id_prof');
            $valor   = Input::get('valor');
            



            foreach ($id_prof as $key2) {
                $j = 0;

                foreach ($id_alum as $key) {

                    
                    DB::table('courses_notes')->insert(array(
                        array('note' => $valor[$j], 'courses_has_courses_students_id' => $key, 'courses_instructors_id' => $key2),
                                
                    ));
                        
                    
                    $j++;
                }
                
            }
                echo "1";

        }else{
            echo "2";

        }
        
        
        
    }
    public function getCambiarfalla(){
        
        DB::table('courses_fails')
        ->where('id', Input::get('id'))
        ->update(array('fail' => Input::get('valor')));
        echo "1";
    }
    public function getCambiarnota(){
        
        DB::table('courses_notes')
        ->where('id', Input::get('id'))
        ->update(array('note' => Input::get('valor')));
        echo "1";
    }
    public function postEditarlicenciasinstructor(){
        
        if (Input::get('id_asignacion') && Input::get('valor_tipo') && Input::get('valor_numero') && Input::get('valor_fecha')) {
            $id_asignacion = Input::get('id_asignacion');
            $valor_tipo = Input::get('valor_tipo');
            $valor_numero = Input::get('valor_numero');
            $valor_fecha = Input::get('valor_fecha');
            $j = 0;

            // var_dump($id_asignacion);
            // exit();

            foreach ($id_asignacion as $key) {
                
                DB::table('courses_instructors_has_courses_technical_licenses')
                ->where('id', $key)
                ->update(array('courses_technical_licenses_id' => $valor_tipo[$j], 'number_licence' => $valor_numero[$j], 'expiration_date' => $valor_fecha[$j]));

                $j++;
                
            }


            echo "1";
        }else{

            echo "2";
        }
    }
    public function postEditarestudiosinstructor(){
        
        if (Input::get('id_asignacion') && Input::get('titulo') && Input::get('institucion') && Input::get('fecha_finalizacion')) {
            $id_asignacion = Input::get('id_asignacion');
            $titulo = Input::get('titulo');
            $institucion = Input::get('institucion');
            $fecha_finalizacion = Input::get('fecha_finalizacion');
            $j = 0;

            // var_dump($id_asignacion);
            // exit();

            foreach ($id_asignacion as $key) {
                
                DB::table('courses_instructors_studies')
                ->where('id', $key)
                ->update(array('obtained_title' => $titulo[$j], 'institution' => $institucion[$j], 'end_date' => $fecha_finalizacion[$j]));

                $j++;
                
            }


            echo "1";
        }else{

            echo "2";
        }
    } 
    public function getFormentrada(){


        return View::make('dashboard.index')
        // return View::make('dashboard.continuada.form_entrada')
        ->with('container', 'dashboard.continuada.form_entrada')
        ->with('submenu_activo', 'Formulario de Entrada')
        ->with('menu_activo', 'Educacion Continuada');  


        
    }
    public function getFormsalida(){


        return View::make('dashboard.index')
        
        ->with('container', 'dashboard.continuada.form_salida')
        ->with('submenu_activo', 'Formulario de Salida')
        ->with('menu_activo', 'Educacion Continuada');  


        
    }     
    public function getCrearinspector(){




            $nombre = Input::get('nombre');
            

            $nuevo_item = new CoursesInspectors;
            $nuevo_item->name   = $nombre;
            
            
            if ($nuevo_item->save()) {
                $inspectores = CoursesInspectors::orderBy('id', 'asc')->get();
                
                
                foreach ($inspectores as $key) {
                    $select = "";
                    if ($key->name == $nombre) {
                        $select = 'selected';
                    }
                    echo '<option value="'.$key->id.'"'.$select.'>'.$key->name.'</option>';
                }
            }else{
                
            }
        
    }  


}

?>
