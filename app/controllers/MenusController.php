<?php 	
/**
* 	
*/
class MenusController extends BaseController
{
	public function __construct()
	{
		$this->beforeFilter('auth'); //bloqueo de acceso
	}
        
        public function traerMenus()
	{
            $bandera="";
               $user = Users::find(Auth::user()->id);
               

               if(count($user->Profiles) == 0){
                echo "el Usuario no tiene ningun perfil";
               }else{

                        return View::make('dashboard.index')
                        ->with('user', $user)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.container')
                        ->with('menu_activo', 'none')
                        ->with('submenu_activo', 'none');
               }
	}

	public function getIndex()
	{
		$user = User::find(Auth::user()->id);
		if (count($user->requests) == 0 ) {
			return View::make('dashboard.index')
			->with('error','El usuario no tiene nungun menu activo')
			->with('container', 'dashboard.administration.menus')
			->with('menu_activo', 'administration');
			echo "El usuario no tiene nungun menu";
		}else{
			return View::make('dashboard.index')
				->with('menus',$user)
				->with('container', 'dashboard.administration.menus')
				->with('menu_activo', 'administration');
		}

	}
	
	public function postCreate()
	{
		//validamos reglas inputs
		$rules = array(
			'name' => 'required|max:50',
			'last_name' => 'required|max:50',
			'email' => 'required|email|unique:users',
			'address' => 'required|max:50',
			'phone' => 'required|numeric|min:5',
			'username' => 'required|max:50',
			'password' => 'required|min:5',
			);
		$validation = Validator::make(Input::all(), $rules);
		if ($validation->fails())
		{
			return Redirect::back()->with_input()->with_errors($validation);
		}
		//si todo esta bien guardamos
		$password = Input::get('password');

		$user = new User;
		$user->name= Input::get('name');
		$user->last_name= Input::get('last_name');
		$user->email= Input::get('email');
		$user->address= Input::get('address');
		$user->phone= Input::get('phone');
		$user->username= Input::get('username');
		$user->level= Input::get('level');
		$user->password = Hash::make($password);
		//guardamos
		$user->save();

		//rediigimos a usuarios
		return Redirect::to('users')->with('status', 'ok_create');

	}
	public function getDelete($user_id)
	{
		//buscamos el usuario en la base de datos segun la id enviada
		$user = User::find($user_id);
		//eliminamos y redirigimos
		$user->delete();
		return Redirect::to('users')->with('status', 'ok_delete');
	}
}

?>