<?php
/**
 * 
 */
class EquiposController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    

    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }
    public function getEquipos2(){
        $user = Users::find(Auth::user()->id);

        
        
        $tipos = EquipmentTypes::get();
        $applicants = Users::all();
        

        $ubicaciones = EquipmentOffices::get();

        $software = DB::table('equipment_inventories')->distinct('software_additional')->select('software_additional')->get();
        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.equipos')
            ->with('user', $user)
            ->with('tipos', $tipos)
            ->with('usuarios', $applicants)
            ->with('programas', $software)

            ->with('ubicaciones', $ubicaciones)
            ->with('submenu_activo', 'Gestión de Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getEquipos(){
        $user = Users::find(Auth::user()->id);

        
        
        $tipos = EquipmentTypes::get();
        $applicants = Users::all();
        

        $ubicaciones = EquipmentOffices::get();

        $software = DB::table('equipment_inventories')->distinct('software_additional')->select('software_additional')->get();
        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.ingresar_equipos')
            ->with('user', $user)
            ->with('tipos', $tipos)
            ->with('usuarios', $applicants)
            ->with('programas', $software)

            ->with('ubicaciones', $ubicaciones)
            ->with('submenu_activo', 'Gestión de Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getCargarinfouser(){
        $funcionarios1 = DB::table('users')
            ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
            ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
            ->join('processes', 'profiles.processes_id', '=', 'processes.id')
            ->join('administratives', 'users.id', '=', 'administratives.user_id')
            ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
            
            ->where('users.id', Input::get('usuario_id'))
            ->orderBy('last_name', 'asc')
            ->get();
        // var_dump($funcionarios1);
        echo '<a href="#" class="nameimg">
                <img alt="" style="border-radius:50px;" width="50" height="50"  src="../'.$funcionarios1[0]->img_min.'">
                </a>';
        echo $funcionarios1[0]->name." ".$funcionarios1[0]->last_name;
    }
    public function getCargarinfoequipo(){
        
        $usuarios    = DB::table("equipment_inventories_users")
                        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id','=', 'equipment_inventories_users.equipments_id')
                        ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipments_id')
                        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id')
                        ->select('users.name', 'users.last_name', 'equipment_inventories.image')
                        ->where('equipment_inventories_users.equipments_id', Input::get('id_equipo_asignar'))
                        ->get();
        if ($usuarios[0]->image == "") {
            $imagen = "assets/img/equipos/-1.png";
        }else{
            $imagen = $usuarios[0]->image;
        }
        if ( $usuarios[0]->name != "") {
            $nombre = $usuarios[0]->name." ".$usuarios[0]->last_name;
        }else{
            $nombre = "Sin asignar";
        }



        echo '<a href="#" class="nameimg">
                <img alt="" style="border-radius:50px;" width="50" height="50"  src="../'.$imagen.'">
                </a>';
        echo $nombre;
    }
    public function getElementos(){
        $user       = Users::find(Auth::user()->id);
        $equipos    = Equipments::all(); 
        $elementos  = EquipmentInventories::get();
        $data = array();
        for ($i=0; $i < count($equipos); $i++) { 
            $data[$i]['id_equipo'] = $equipos[$i]->id;

            $usuarios    = DB::table("equipment_inventories_users")
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id')
                            ->select('users.name', 'users.last_name')
                            ->where('equipment_inventories_users.equipments_id', $equipos[$i]->id)
                            ->where('equipment_inventories_users.status', 1)
                            ->get();

            if (count($usuarios) == 0) {
                $texto = "Sin Asignar"; 
            }else{
                $texto = "";
                foreach ($usuarios as $key) {
                    if($texto==""){
                    $texto .= $key->name." ".$key->last_name;
                    }else{
                        $texto .= ", ".$key->name." ".$key->last_name;
                    }
                }
            }

            $data[$i]['usuarios'] = $texto;
        }
        

        
        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.asignar_elementos')
            ->with('equipos', $data)
            ->with('elementos', $elementos)
            ->with('submenu_activo', 'Gestión de Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getAsignar(){
        $user           = Users::find(Auth::user()->id);
        $usuarios2     = Users::orderBy('last_name', 'asc')->get();
        $equipos    = Equipments::all(); 
        $data = array();
        for ($i=0; $i < count($equipos); $i++) { 
            $data[$i]['id_equipo'] = $equipos[$i]->id;

            $usuarios    = DB::table("equipment_inventories_users")
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id')
                            ->select('users.name', 'users.last_name')
                            ->where('equipment_inventories_users.equipments_id', $equipos[$i]->id)
                            ->where('equipment_inventories_users.status', 1)
                            ->get();

            if (count($usuarios) == 0) {
                $texto = "Sin Asignar"; 
            }else{
                $texto = "";
                foreach ($usuarios as $key) {
                    if($texto==""){
                    $texto .= $key->name." ".$key->last_name;
                    }else{
                        $texto .= ", ".$key->name." ".$key->last_name;
                    }
                }
            }

            $data[$i]['usuarios'] = $texto;
        }

        
        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.asignar_equipos')
            ->with('user', $user)
            ->with('usuarios', $usuarios2)
            ->with('equipos', $data)
            ->with('submenu_activo', 'Gestión de Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getEliminar(){
        $user       = Users::find(Auth::user()->id);
        $equipos    = Equipments::all(); 
        $data = array();

        for ($i=0; $i < count($equipos); $i++) { 
            $data[$i]['id_equipo'] = $equipos[$i]->id;

            $usuarios    = DB::table("equipment_inventories_users")
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id')
                            ->select('users.name', 'users.last_name')
                            ->where('equipment_inventories_users.equipments_id', $equipos[$i]->id)
                            ->get();

            if (count($usuarios) == 0) {
                $texto = "Sin Asignar"; 
            }else{
                $texto = "";
                foreach ($usuarios as $key) {
                    if($texto==""){
                    $texto .= $key->name." ".$key->last_name;
                    }else{
                        $texto .= ", ".$key->name." ".$key->last_name;
                    }
                }
            }

            $data[$i]['usuarios'] = $texto;
        }

        
        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.eliminar_asignacion')
            ->with('user', $user)
            ->with('equipos', $data)
            ->with('submenu_activo', 'Gestión de Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getAsignarequipo(){
        $id_equipo = Input::get('id_equipo');
        $id_user = Input::get('id_user');

        DB::table('equipments')
        ->where('id', $id_equipo)
        ->update(array('status' => 1));

        DB::table('equipment_inventories_users')->insert(array(
            array('users_id' => $id_user, 'equipments_id' => $id_equipo, 'status' => "1"),
            
        ));
        echo "1";
    }
    public function getAsignarelemento(){
        $elemento = Input::get('elemento');
        $equipo = Input::get('equipo');

        DB::table('equipment_inventories')
        ->where('id', $elemento)
        ->update(array('equipment_statuses_id' => 1));

        DB::table('equipments_has_equipment_inventories')->insert(array(
            array('equipments_id' => $equipo, 'equipment_inventories_id' => $elemento, 'status' => "1"),
            
        ));
        echo "1";
    }
    public function getAdministrador(){
        
        $tipo_equipos = DB::table('equipment_types')->get();
        $tipo_mantenimientos = DB::table('equipment_type_maintenances')->where('status' , 1)->get();
        $item_mantenimientos = EquipmentItemMaintenances::orderBy('id', 'asc')->get();
        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();



        $grupos = DB::table('equipment_groups') 
        ->distinct('equipment_groups.id')
        ->leftjoin('equipment_maintenance_schedules', 'equipment_maintenance_schedules.equipment_groups_id', '=', 'equipment_groups.id')
        ->select('equipment_groups.id', 'name_group')
        // ->where('equipment_type_maintenances_id', '')
        ->get();
        
        $funcionarios = DB::table('equipment_inventories')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
        ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name')
        // ->where('equipment_groups_has_equipment_inventories.equipment_groups_id', $id_grupo)
        ->get();

        $applicants = Users::all();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.administrador')
        ->with('tipo_equipos', $tipo_equipos)
        ->with('tipo_mantenimientos', $tipo_mantenimientos)
        ->with('item_mantenimientos', $item_mantenimientos)
        ->with('grupos', $grupos)
        ->with('equipos', $funcionarios)
        ->with('usuarios', $applicants)
        ->with('submenu_activo', 'Administrador')
        ->with('menu_activo', 'Equipos');
    }
    public function postIngresarmonitor(){


        if (Input::file('imagen_monitor')) {

            $my_id = Auth::user()->id;

            

            $monitor_marca      = Input::get('monitor_marca');
            $monitor_modelo     = Input::get('monitor_modelo');
            $monitor_serial     = Input::get('monitor_serial');
            $monitor_factura = Input::get('monitor_factura');
            $monitor_asignacion = Input::get('monitor_asignacion');
            $monitor_ubicacion = Input::get('monitor_ubicacion');
            $monitor_compra = Input::get('monitor_compra');

            $user_asign  = Input::get('user_asign');

            
            $filename = $monitor_serial."_".Input::file('imagen_monitor')->getClientOriginalName();
            $destinationPath = 'assets/img/equipos/';

            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo =1;
            }

           

            $uploadSuccess = Input::file('imagen_monitor')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                $nuevo_equipo = new EquipmentInventories;

                $nuevo_equipo->mark = $monitor_marca;
                $nuevo_equipo->model = $monitor_modelo;
                $nuevo_equipo->serial = $monitor_serial;
                $nuevo_equipo->equipment_types_id = 1;
                $nuevo_equipo->equipment_statuses_id = 1;
                $nuevo_equipo->invoice = $monitor_factura;
                $nuevo_equipo->date_asign = $monitor_asignacion;
                $nuevo_equipo->equipment_offices_id = $monitor_ubicacion;
                $nuevo_equipo->date_shopping = $monitor_compra;
                $nuevo_equipo->image = $destinationPath.$filename;


                $nuevo_equipo->equipment_statuses_id = $estado_equipo;
                 
                $nuevo_equipo->save();




                if ($estado_equipo == "1") {

                    $equipo = EquipmentInventories::all();
                    $bandera = $equipo->last()->id;
                    DB::table('equipment_inventories_users')->insert(array(
                        array('users_id' => $user_asign, 'equipment_inventories_id' => $bandera, 'status' => "1"),
                        
                    ));
                }
                

                
                echo "1";
            }



        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function postIngresartelefono(){


        if (Input::file('imagen_telefono')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/equipos/';

            $telefono_marca      = Input::get('telefono_marca');
            $telefono_modelo     = Input::get('telefono_modelo');
            $telefono_serial     = Input::get('telefono_serial');
            $telefono_factura     = Input::get('telefono_factura');
            $telefono_asignacion  = Input::get('telefono_asignacion');
            $telefono_ubicacion   = Input::get('telefono_ubicacion');
            $telefono_compra         = Input::get('telefono_compra');
            
            $user_asign  = Input::get('user_asign');

            $filename = $telefono_serial."_".Input::file('imagen_telefono')->getClientOriginalName();
            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo =1;
            }
           

            $uploadSuccess = Input::file('imagen_telefono')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                 $nuevo_equipo = new EquipmentInventories;

                 $nuevo_equipo->mark = $telefono_marca;
                 $nuevo_equipo->model = $telefono_modelo;
                 $nuevo_equipo->serial = $telefono_serial;
                 $nuevo_equipo->equipment_types_id = 5;
                 $nuevo_equipo->equipment_statuses_id = 1;
                 $nuevo_equipo->invoice = $telefono_factura;
                 $nuevo_equipo->date_asign = $telefono_asignacion;
                 $nuevo_equipo->equipment_offices_id = $telefono_ubicacion;
                 $nuevo_equipo->date_shopping = $telefono_compra;
                 $nuevo_equipo->equipment_statuses_id = $estado_equipo;
                 $nuevo_equipo->image = $destinationPath.$filename;

                 
                $nuevo_equipo->save();

                if ($estado_equipo == "1") {
                    
                    $equipo = EquipmentInventories::all();
                    $bandera = $equipo->last()->id;
                    DB::table('equipment_inventories_users')->insert(array(
                        array('users_id' => $user_asign, 'equipment_inventories_id' => $bandera, 'status' => "1"),
                        
                    ));
                }

                
                echo "1";
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function postIngresarimpresora(){


        if (Input::file('imagen_impresora')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/equipos/';

            $impresora_marca      = Input::get('impresora_marca');
            $impresora_modelo     = Input::get('impresora_modelo');
            $impresora_serial     = Input::get('impresora_serial');
            $impresora_factura = Input::get('impresora_factura');  
            $impresora_asignacion = Input::get('impresora_asignacion');   
            $impresora_ubicacion = Input::get('impresora_ubicacion');
            $impresora_compra = Input::get('impresora_compra');
            
            $user_asign  = Input::get('user_asign');
            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo =1;
            }
           
            $filename = $impresora_serial."_".Input::file('imagen_impresora')->getClientOriginalName();

            $uploadSuccess = Input::file('imagen_impresora')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                 $nuevo_equipo = new EquipmentInventories;

                 $nuevo_equipo->mark = $impresora_marca;
                 $nuevo_equipo->model = $impresora_modelo;
                 $nuevo_equipo->serial = $impresora_serial;
                 $nuevo_equipo->equipment_types_id = 7;
                 $nuevo_equipo->equipment_statuses_id = 1;
                 $nuevo_equipo->invoice = $impresora_factura;
                 $nuevo_equipo->date_asign = $impresora_asignacion;
                 $nuevo_equipo->equipment_offices_id = $impresora_ubicacion;
                 $nuevo_equipo->date_shopping = $impresora_compra;    
                 $nuevo_equipo->equipment_statuses_id = $estado_equipo;
                 $nuevo_equipo->image = $destinationPath.$filename;

                 
                $nuevo_equipo->save();

                if ($estado_equipo == "1") {
                    
                    $equipo = EquipmentInventories::all();
                    $bandera = $equipo->last()->id;
                    DB::table('equipment_inventories_users')->insert(array(
                        array('users_id' => $user_asign, 'equipment_inventories_id' => $bandera, 'status' => "1"),
                        
                    ));
                }

                
                echo "1";
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function getIngresarteclado(){


        if (Input::get('teclado_marca')) {
            $teclado_marca = Input::get('teclado_marca');
            $teclado_factura = Input::get('teclado_factura');
            $teclado_asignacion = Input::get('teclado_asignacion');
            $teclado_ubicacion = Input::get('teclado_ubicacion');
            $teclado_compra = Input::get('teclado_compra');

            $user_asign  = Input::get('user_asign');
            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo =1;
            }

            $my_id = Auth::user()->id;
            

            $nuevo_equipo = new EquipmentInventories;
            $nuevo_equipo->mark = $teclado_marca;
            $nuevo_equipo->equipment_types_id = 2;
            $nuevo_equipo->equipment_statuses_id = 1;
            $nuevo_equipo->invoice = $teclado_factura;
            $nuevo_equipo->date_asign = $teclado_asignacion;
            $nuevo_equipo->equipment_offices_id = $teclado_ubicacion;
            $nuevo_equipo->date_shopping = $teclado_compra;
            $nuevo_equipo->equipment_statuses_id = $estado_equipo;


            $nuevo_equipo->save();

            if ($estado_equipo == "1") {
                    
                    $equipo = EquipmentInventories::all();
                    $bandera = $equipo->last()->id;
                    DB::table('equipment_inventories_users')->insert(array(
                        array('users_id' => $user_asign, 'equipment_inventories_id' => $bandera, 'status' => "1"),
                        
                    ));
                }

                
            echo "1";
            
        } else {
            echo "2";
            
        }
    }
    public function getIngresarmouse(){


        if (Input::get('mouse_marca')) {

            $my_id = Auth::user()->id;
            $mouse_marca      = Input::get('mouse_marca');
            $mouse_factura  = Input::get('mouse_factura');
            $mouse_asignacion   = Input::get('mouse_asignacion');
            $mouse_ubicacion    = Input::get('mouse_ubicacion');
            $mouse_compra = Input::get('mouse_compra');

            $user_asign  = Input::get('user_asign');
            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo =1;
            }

            $nuevo_equipo = new EquipmentInventories;
            $nuevo_equipo->mark = $mouse_marca;
            $nuevo_equipo->equipment_types_id = 3;
            $nuevo_equipo->equipment_statuses_id = 1;
            $nuevo_equipo->invoice = $mouse_factura;
            $nuevo_equipo->date_asign = $mouse_asignacion;
            $nuevo_equipo->equipment_offices_id = $mouse_ubicacion;
            $nuevo_equipo->date_shopping = $mouse_compra;
            $nuevo_equipo->equipment_statuses_id = $estado_equipo;


            $nuevo_equipo->save();

            if ($estado_equipo == "1") {
                    
                    $equipo = EquipmentInventories::all();
                    $bandera = $equipo->last()->id;
                    DB::table('equipment_inventories_users')->insert(array(
                        array('users_id' => $user_asign, 'equipment_inventories_id' => $bandera, 'status' => "1"),
                        
                    ));
                }

                
            echo "1";
            
        } else {
            echo "2";
            
        }
    }
    public function postIngresarcpu(){


        if (Input::file('imagen_cpu')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/equipos/';

            $cpu_marca = Input::get('cpu_marca');
            $cpu_modelo = Input::get('cpu_modelo');
            $cpu_serial = Input::get('cpu_serial');
            
            $cpu_ip = Input::get('cpu_ip');
            $cpu_licencia_so = Input::get('cpu_licencia_so');
            $cpu_vesion_so = Input::get('cpu_vesion_so');
            $cpu_mac = Input::get('cpu_mac');
            $cpu_memoria_ram = Input::get('cpu_memoria_ram');
            $cpu_disco_duro = Input::get('cpu_disco_duro');
            $cpu_procesador = Input::get('cpu_procesador');
            $cpu_so = Input::get('cpu_so');
            $cpu_factura = Input::get('cpu_factura');
            $cpu_asignacion = Input::get('cpu_asignacion');
            $cpu_ubicacion = Input::get('cpu_ubicacion');
            $cpu_compra = Input::get('cpu_compra');
                
            
            
            $user_asign  = Input::get('user_asign');


            $filename = $cpu_serial."_".Input::file('imagen_cpu')->getClientOriginalName();
            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo =1;
            }

           

            $uploadSuccess = Input::file('imagen_cpu')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                 $nuevo_equipo = new EquipmentInventories;

                 $nuevo_equipo->mark = $cpu_marca;
                 $nuevo_equipo->model = $cpu_modelo;
                 $nuevo_equipo->serial = $cpu_serial;
                 
                 
                 
                 
                 $nuevo_equipo->ip = $cpu_ip;
                 $nuevo_equipo->mac = $cpu_mac;
                 $nuevo_equipo->memory = $cpu_memoria_ram;
                 $nuevo_equipo->hard_drive = $cpu_disco_duro;
                 $nuevo_equipo->processor = $cpu_procesador;
                 $nuevo_equipo->operating_system = $cpu_so;
                 
                 
                 
                 $nuevo_equipo->invoice = $cpu_factura;
                 $nuevo_equipo->date_asign = $cpu_asignacion;
                 $nuevo_equipo->equipment_offices_id = $cpu_ubicacion;
                 $nuevo_equipo->date_shopping = $cpu_compra;
                 $nuevo_equipo->equipment_statuses_id = $estado_equipo;
                 $nuevo_equipo->image = $destinationPath.$filename;

                 
                 $nuevo_equipo->equipment_types_id = 4;
                 $nuevo_equipo->equipment_statuses_id = 1;
                 
                $nuevo_equipo->save();

                if ($estado_equipo == "1") {
                    
                    $equipo = EquipmentInventories::all();
                    $bandera = $equipo->last()->id;
                    DB::table('equipment_inventories_users')->insert(array(
                        array('users_id' => $user_asign, 'equipment_inventories_id' => $bandera, 'status' => "1"),
                        
                    ));
                }

                
                echo "1";
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function getProgramas(){
        

        $applicants = Users::orderBy('name', 'asc')->get();
        $software = DB::table('equipment_inventories')->distinct('software_additional')->select('software_additional')->get();
        
        return $software;
    }
    public function getVerequipos() {
        
        $user = Users::find(Auth::user()->id);

        //funciones para filtros
        $f_compra       = Input::get('f_compra');
        $serial         = Input::get('serial');
        $codigo         = Input::get('code');
        $marca          = Input::get('marca');
        $applicant      = Input::get('applicant');
        $grupo          = Input::get('grupo');
        $tipo_equipo    = Input::get('tipo_equipo');
        $observation    = Input::get('nombre_e');

        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'equipment_inventories_users.users_id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'equipment_inventories.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
            $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
            $var_grupo = '=';
            $signo_grupo = $grupo;
        } else {
            $campo_grupo = 'equipment_inventories.id';
            $var_grupo = '<>';
            $signo_grupo = '-2';
        }
        if (isset($_GET['nombre_e']) && !empty($_GET['nombre_e'])) {
            $campo_nomb = 'equipment_inventories.observation';
            $var_nomb = '=';
            $signo_nomb = $observation;
        } else {
            $campo_nomb = 'equipment_inventories.id';
            $var_nomb = '<>';
            $signo_nomb = '-4';
        }


        if (Input::get('order_tipo') == 0) {
            $orden_tipo = 'equipment_types.type';
            $tipo_orden_tipo = 'ASC';
        }elseif(Input::get('order_tipo') == 1){
            $orden_tipo = 'equipment_types.type';
            $tipo_orden_tipo = 'DESC';
        }else{
            $orden_tipo = 'equipment_types.type';
            $tipo_orden_tipo = 'ASC';
        }

        if (Input::get('order_nombre') == 0) {
            $orden_nombre = 'equipment_inventories.observation';
            $tipo_orden_nombre = 'DESC';
        }elseif(Input::get('order_nombre') == 1){
            $orden_nombre = 'equipment_inventories.observation';
            $tipo_orden_nombre = 'ASC';
        }else{
            $orden_nombre = 'equipment_inventories.observation';
            $tipo_orden_nombre = 'DESC';
        }


        

        $tipo_equipos = DB::table('equipment_types')->distinct()->get();

        $applicants = DB::table('users')->get();
        
        $marcas  = DB::table('equipment_inventories')->distinct()->select('mark')->get();
        

        $funcionarios = DB::table('equipments')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
        ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
        ->leftJoin('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
        ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
        ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
        ->leftJoin('equipment_offices', 'equipment_offices.id', '=', 'equipment_inventories.equipment_offices_id')
        ->select('users.id as id_user','users.name', 'users.last_name', 'users.img_min', 'equipments.id as id_equipo', 'equipment_inventories.mark',
            'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_inventories.id as id_elemento',
            'equipment_inventories.observation', 'equipment_offices.sede as ubicacion',
            'equipment_type_maintenances.type_maintenance', 'equipment_inventories.image', 'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance')
        ->where($campo_grupo, $var_grupo, $signo_grupo )
        ->where($campo_user, $var_user, $signo_user)
        ->where($campo_nomb, $var_nomb, $signo_nomb)
        // ->where($campo_mant, $var_mant, $signo_mant)
        ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')
        // ->where('equipment_inventories.observation', 'LIKE', '%' . $observation . '%')
        ->where('equipment_inventories.equipment_types_id', '<>', 8)
        ->where('equipment_inventories.equipment_types_id', '<>', 9)
        ->where('equipment_inventories_users.status', 1)
        
        ->orderBy($orden_nombre, $tipo_orden_nombre)
        ->distinct('equipment_inventories.id')
        ->paginate(15);



        // $funcionarios = DB::table('equipments')
        // ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
        // ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
        // ->leftJoin('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
        // ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        // ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        // ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        // ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
        // ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
        // ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
        // ->leftJoin('equipment_offices', 'equipment_offices.id', '=', 'equipment_inventories.equipment_offices_id')
        // ->select('users.id as id_user','users.name', 'users.last_name', 'users.img_min', 'equipments.id as id_equipo', 'equipment_inventories.mark',
        //     'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_inventories.id as id_elemento',
        //     'equipment_type_maintenances.type_maintenance', 'equipment_inventories.image', 'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance',
        //     'equipment_inventories.observation', 'equipment_offices.sede as ubicacion')
        // ->where($campo_grupo, $var_grupo, $signo_grupo )
        // ->where($campo_user, $var_user, $signo_user)
        // // ->where($campo_mant, $var_mant, $signo_mant)
        // ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        // ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')

        // ->where('equipment_inventories.equipment_types_id', '<>', 8)
        // ->where('equipment_inventories.equipment_types_id', '<>', 9)
        // ->where('equipment_inventories_users.status', 1)
        // ->orderBy($orden, $tipo_orden)
        // ->paginate(500);




        $numero_equipos = count($funcionarios);
        $data = array();
        $data_filtros = array();

        if(isset($_GET['id'])){

            if($_GET['id']==1){

            if (isset($_GET['order_tipo']) && $_GET['order_tipo'] == 1) {

                $data_filtros['icon_order_tipo'] = 'icon-arrow-down';
                $data_filtros['num_order_tipo'] = '0';

                
                if($_GET['order_nombre']==1){
                    $data_filtros['num_order_nombre'] = $_GET['order_nombre'];
                    $data_filtros['icon_order_nombre'] = 'icon-arrow-up';
                }elseif($_GET['order_nombre']==0){
                    $data_filtros['num_order_nombre'] = $_GET['order_nombre'];
                    $data_filtros['icon_order_nombre'] = 'icon-arrow-down';
                }
                
            }else{

                $data_filtros['icon_order_tipo'] = 'icon-arrow-up';
                $data_filtros['num_order_tipo'] = '1';

                if($_GET['order_nombre']==1){
                    $data_filtros['num_order_nombre'] = $_GET['order_nombre'];
                    $data_filtros['icon_order_nombre'] = 'icon-arrow-up';
                }elseif($_GET['order_nombre']==0){
                    $data_filtros['num_order_nombre'] = $_GET['order_nombre'];
                    $data_filtros['icon_order_nombre'] = 'icon-arrow-down';
                }


            }

        }elseif($_GET['id']==2){

            if(isset($_GET['order_nombre']) && $_GET['order_nombre'] == 1){

                $data_filtros['icon_order_nombre'] = 'icon-arrow-down';
                $data_filtros['num_order_nombre'] = '0';

                if($_GET['order_tipo']==1){
                    $data_filtros['num_order_tipo'] = $_GET['order_tipo'];
                    $data_filtros['icon_order_tipo'] = 'icon-arrow-up';
                }elseif($_GET['order_tipo']==0){
                    $data_filtros['num_order_tipo'] = $_GET['order_tipo'];
                    $data_filtros['icon_order_tipo'] = 'icon-arrow-down';
                }


            }else{

                $data_filtros['icon_order_nombre'] = 'icon-arrow-up';
                $data_filtros['num_order_nombre'] = '1';

                if($_GET['order_tipo']==1){
                    $data_filtros['num_order_tipo'] = $_GET['order_tipo'];
                    $data_filtros['icon_order_tipo'] = 'icon-arrow-up';
                }elseif($_GET['order_tipo']==0){
                    $data_filtros['num_order_tipo'] = $_GET['order_tipo'];
                    $data_filtros['icon_order_tipo'] = 'icon-arrow-down';
                }
            }
        }
            

        }else{

            $data_filtros['icon_order_tipo'] = 'icon-arrow-up';
                $data_filtros['num_order_tipo'] = '1';

                $data_filtros['icon_order_nombre'] = 'icon-arrow-up';
                $data_filtros['num_order_nombre'] = '1';
        }
        for ($i=0; $i < count($funcionarios); $i++) { 
            $data[$i]['id_user'] = $funcionarios[$i]->id_user;
            $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
            $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
            $data[$i]['id_elemento'] = $funcionarios[$i]->id_elemento;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['tipo'] = $funcionarios[$i]->type;
            $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
            $data[$i]['sede'] = $funcionarios[$i]->ubicacion;
            if($funcionarios[$i]->observation == ""){
                $data[$i]['name_equipo'] = 'N/A';
                
            }else{
                $data[$i]['name_equipo'] = $funcionarios[$i]->observation;
            }

            if ($funcionarios[$i]->image != "") {
                $data[$i]['img_equipo'] = $funcionarios[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }
            if (Input::get('order') == 0) {
                $data[$i]['icon_order'] = 'icon-arrow-up';
                $data[$i]['num_order'] = 'tabla_lista_equipos(1)';
            }else{
                $data[$i]['icon_order'] = 'icon-arrow-down';
                $data[$i]['num_order'] = 'tabla_lista_equipos(0)';
            }

            if ($funcionarios[$i]->name != "") {
                $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                $data[$i]['disabled'] = 'false';


            }else{
                $data[$i]['id_user'] = '-1';
                $data[$i]['id_equipo'] = $funcionarios[$i]->id_elemento;
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            if ($funcionarios[$i]->name_group != "") {
                $data[$i]['grupo'] = $funcionarios[$i]->name_group;
            }else{
                $data[$i]['grupo'] = "Sin Grupo";
            }

            if ($funcionarios[$i]->type_maintenance != "") {
                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                $data[$i]['row'] = 'green';
            }else{
                $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                $data[$i]['row'] = "red";
            }
        }
        if (Input::get('nombre') && Input::get('nombre') != "") {
            return View::make('dashboard.equipos.tabla_lista_equipos')
            
            // ->with('administradores', $funcionarios2)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('numero_equipos', $numero_equipos)
            ->with('data', $data)
            ->with('data_filtros', $data_filtros)
            ->with('grupos', $grupos)
            ->with('tipo_equipos', $tipo_equipos)
            ->with('pag', $funcionarios);
            
            exit();
        }                     
        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.lista_equipos')
        // ->with('administradores', $funcionarios2)
        ->with('marcas', $marcas)
        ->with('applicants', $applicants)
        ->with('numero_equipos', $numero_equipos)
        ->with('pag', $funcionarios)
        ->with('data', $data)
        ->with('data_filtros', $data_filtros)
        ->with('grupos', $grupos)
        ->with('tipo_equipos', $tipo_equipos)
        ->with('submenu_activo', 'Ver Equipos')
        ->with('menu_activo', 'Equipos');
    }
    public function getInactivos() {
        
        $user = Users::find(Auth::user()->id);

        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');

        if (isset($_GET['serial']) && !empty($_GET['serial'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }
        
        $applicants = Users::orderBy('name', 'asc')->get();
        $marcas = EquipmentInventories::distinct()->select('mark')->where('equipment_statuses_id', 2)->get();

        $funcionarios = EquipmentInventories::where('equipment_statuses_id', 2)
        ->where('equipment_inventories.id', 'LIKE', '%' . $codigo . '%')
        ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        ->where('equipment_inventories.date_shopping', 'LIKE', '%' . $f_compra . '%')
        
        
        ->get();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.lista_equipos_inactivos')
            ->with('administradores', $funcionarios)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)

            ->with('submenu_activo', 'Ver Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getDebaja() {
        
        $user = Users::find(Auth::user()->id);
        $applicants = Users::orderBy('name', 'asc')->get();
        $marcas = EquipmentInventories::distinct()->select('mark')->where('equipment_statuses_id', 3)->get();

        $funcionarios2 = DB::table('equipment_inventories')
        // ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipment_inventories_id', '=', 'equipment_inventories.id') 
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
        ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name')
        ->where('equipment_inventories.equipment_statuses_id','<>', 3)
        ->get();



        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');

        if (isset($_GET['serial']) && !empty($_GET['serial'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }


        $funcionarios = EquipmentInventories::where('equipment_statuses_id', 3)
        ->where('equipment_inventories.id', 'LIKE', '%' . $codigo . '%')
        ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        ->where('equipment_inventories.date_shopping', 'LIKE', '%' . $f_compra . '%')
        ->get();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.lista_equipos_baja')
            ->with('administradores', $funcionarios)
            ->with('equipos', $funcionarios2)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('submenu_activo', 'Ver Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function postCalluserprofile() {
        if (Input::get('id') != -1) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'));
            

            $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                
                ->where('users.id', Input::get('id'))
                ->orderBy('last_name', 'asc')
                ->get();

            // $equipos = Users::find(Input::get('id'))->EquipmentInventories;

            $equipos = DB::table("equipment_inventories")
                    ->leftJoin('equipment_types','equipment_types.id', '=', 'equipment_inventories.equipment_types_id') 
                    ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                    ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                    ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                    ->select('equipment_inventories.id as id_equipo','equipment_inventories.equipment_types_id','equipment_inventories.model','equipment_inventories.observation'
                    ,'equipment_inventories.serial','equipment_inventories.image' , 'equipment_inventories.mark', 'equipment_inventories.date_asign'
                    ,'users.id as id_user'
                    ,'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_inventories.ip', 'equipment_inventories.mac'
                    ,'equipment_inventories.memory', 'equipment_inventories.hard_drive', 'equipment_inventories.processor'
                    ,'equipment_inventories.operating_system','equipments_has_equipment_inventories.equipments_id'
                    ,'equipment_inventories.license_of'
                    ,'equipment_inventories.version_op'
                    ,'equipment_inventories.no_license_op'
                    ,'equipment_inventories_users.id as id_asignacion', 'equipments_has_equipment_inventories.id as id_asignacion_equipo'
                    ,'equipments_has_equipment_inventories.status')
                    ->where('equipments_has_equipment_inventories.equipments_id',Input::get('id_equipo'))
                    ->groupBy('equipment_inventories.id')
                    ->paginate(15); 

            $fecha_actual = date('Y-m-d H:i:s');
            $data = array();
            $mantenimientos = EquipmentMaintenances::where('equipment_inventories_id', Input::get('id_equipo'))->get();
            return View::make('dashboard.equipos.detalle_equipos')
            ->with('users', $funcionarios)
            ->with('equipos', $equipos)
            ->with('data', $data)
            ->with('mantenimientos', $mantenimientos );

            

            exit();

        }else{

               //consultamos los datos del usuario administrativo
               $user = Users::find(Input::get('id'));
               

               $funcionarios = DB::table('users')
                   ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                   ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                   ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                   ->join('administratives', 'users.id', '=', 'administratives.user_id')
                   ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                   ->where('status', 1)
                   ->where('users.id', Input::get('id'))
                   ->orderBy('last_name', 'asc')
                   ->get();

               // $equipos = Users::find(Input::get('id'))->EquipmentInventories;

               $equipos = DB::table("equipment_inventories")
                       ->leftJoin('equipment_types','equipment_types.id', '=', 'equipment_inventories.equipment_types_id') 
                       ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                       ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                       ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                       ->select('equipment_inventories.id as id_equipo','equipment_inventories.equipment_types_id','equipment_inventories.model','equipment_inventories.observation'
                       ,'equipment_inventories.serial','equipment_inventories.image' , 'equipment_inventories.mark', 'equipment_inventories.date_asign'
                       ,'users.id as id_user'
                       ,'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_inventories.ip', 'equipment_inventories.mac'
                       ,'equipment_inventories.memory', 'equipment_inventories.hard_drive', 'equipment_inventories.processor'
                       ,'equipment_inventories.operating_system','equipments_has_equipment_inventories.equipments_id'
                       ,'equipment_inventories.license_of'
                       ,'equipment_inventories.version_op'
                       ,'equipment_inventories.no_license_op'
                       ,'equipment_inventories_users.id as id_asignacion', 'equipments_has_equipment_inventories.id as id_asignacion_equipo'
                       ,'equipments_has_equipment_inventories.status')
                       ->where('equipments_has_equipment_inventories.equipments_id',Input::get('id_equipo'))
                       ->groupBy('equipment_inventories.id')
                       ->paginate(15); 

               $fecha_actual = date('Y-m-d H:i:s');
               $data = array();
               $mantenimientos = EquipmentMaintenances::where('equipment_inventories_id', Input::get('id_equipo'))->get();
               return View::make('dashboard.equipos.detalle_equipos_inactivos')
               ->with('users', $funcionarios)
               ->with('equipos', $equipos)
               ->with('data', $data)
               ->with('mantenimientos', $mantenimientos );

               
               
               

               

               exit();

           
        }
    }
    public function postDetallemantenimientos() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'));

            $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->where('users.id', Input::get('id'))
                ->orderBy('last_name', 'asc')
                ->get();

            $equipos = Users::find(Input::get('id'))->EquipmentInventories;

            // $mantenimientos = EquipmentInventories::find(Input::get('id_equipo'))->EquipmentMaintenances;


            $fecha_actual = date('Y-m-d H:i:s');
            $data = array();

            
            $mantenimientos = Db::table('equipment_maintenances')
            ->join('equipment_maintenances_details', 'equipment_maintenances_id', '=', 'equipment_maintenances.id')
            ->join('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances_details.equipment_inventories_id')
            ->join('users', 'users.id', '=', 'equipment_maintenances.users_id')
            ->join('equipment_inventories_users', 'equipment_inventories_users.equipment_inventories_id', '=', 'equipment_inventories.id')
            ->select('details', 'equipment_maintenances.users_id', 'equipment_maintenances.created_at','equipment_inventories.mark', 'users.name', 'users.last_name','equipment_maintenances.id as id_mantenimiento')
            ->distinct()
            ->where('equipment_inventories_users.users_id', Input::get('id'))->get();

                // SELECT DISTINCT details, users_id FROM `equipment_maintenances` join equipment_maintenances_details on (equipment_maintenances_id = equipment_maintenances.id)

            


            return View::make('dashboard.equipos.detalle_mantenimientos')
            ->with('users', $funcionarios)
            ->with('equipos', $equipos)
            ->with('data', $data)
            ->with('mantenimientos', $mantenimientos );

            

            exit();

        }
    }
    public function postDetalleinactivos() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'));

            $equipos = EquipmentInventories::where('id', Input::get('id'))->get();

            

            





            return View::make('dashboard.equipos.detalle_equipos_inactivos')
            ->with('equipos', $equipos);
            
            

            

            exit();

        }
    }
    public function postFrommantenimientos() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id_user'));

            $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->where('users.id', Input::get('id_user'))
                ->orderBy('last_name', 'asc')
                ->get();

            $equipos = Equipments::find(Input::get('id'))->EquipmentInventories;

            $equipos_datos = EquipmentInventories::where('id', Input::get('id'))->get();

            $mantenimientos = EquipmentMaintenances::where('equipment_inventories_id', Input::get('id'))
            ->where('equipment_type_maintenances_id', Input::get('id_mant_pendiente'))
            ->where('equipment_maintenances_statuses_id', 1)
            ->get();

            // $items = EquipmentItemMaintenances::where('equipment_type_maintenances_id', Input::get('id_mant_pendiente'))
            // ->get();

            $items = DB::table('equipment_item_maintenances_has_equipment_type_maintenances') 
            ->join('equipment_item_maintenances','equipment_item_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_item_maintenances_id') 
            ->join('equipment_type_maintenances','equipment_type_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_type_maintenances_id')
            ->select('equipment_type_maintenances.type_maintenance'
                , 'equipment_item_maintenances.item'
                , 'equipment_item_maintenances.id'
                , 'equipment_item_maintenances.created_at'
                , 'equipment_item_maintenances.status'
                , 'equipment_item_maintenances.required'
            )
            ->where('equipment_type_maintenances_id', Input::get('id_mant_pendiente'))
            ->get();
            
            $tipo_mantenimiento = DB::table('equipment_type_maintenances')->where('status', 1)->get();
            $tipos = EquipmentItemMaintenances::get();


            


            return View::make('dashboard.equipos.detalle_equipos_mantenimiento')
            ->with('users', $funcionarios)
            ->with('equipos', $equipos)
            ->with('equipos_datos', $equipos_datos)
            ->with('mantenimientos', $mantenimientos)
            ->with('items', $items)
            ->with('items_mantenimiento', $tipos)
            ->with('tipo_mantenimientos', $tipo_mantenimiento );
            

            

            exit();

        }
    }
    public function postFrommantenimientosgestion() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id_user'));

            $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->where('users.id', Input::get('id_user'))
                ->orderBy('last_name', 'asc')
                ->get();

            $equipos = Equipments::find(Input::get('id'))->EquipmentInventories;

            $equipos_datos = EquipmentInventories::where('id', Input::get('id'))->get();

            $mantenimientos = EquipmentMaintenances::where('equipment_inventories_id', Input::get('id_elemento'))
            ->get();

            $tipo_mantenimiento = DB::table('equipment_type_maintenances')->where('status', 1)->get();
            $tipos = EquipmentItemMaintenances::get();


            


            return View::make('dashboard.equipos.detalle_equipos_mantenimiento_gestion')
            ->with('users', $funcionarios)
            ->with('equipos', $equipos)
            ->with('equipos_datos', $equipos_datos)
            ->with('mantenimientos', $mantenimientos )
            ->with('items_mantenimiento', $tipos)
            ->with('tipo_mantenimientos', $tipo_mantenimiento );
            

            

            exit();

        }
    }
    public function getMantenimientos() {

        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        $grupo = Input::get('grupo');
        $tipo_equipo = Input::get('tipo_equipo');
        $mantenimiento = Input::get('mantenimiento');

        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'equipment_inventories_users.users_id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'equipment_inventories.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
            $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
            $var_grupo = '=';
            $signo_grupo = $grupo;
        } else {
            $campo_grupo = 'equipment_inventories.id';
            $var_grupo = '<>';
            $signo_grupo = '-2';
        }

        if (isset($_GET['mantenimiento']) && !empty($_GET['mantenimiento'])) {
            $campo_mant = 'equipment_type_maintenances.id';
            $var_mant = '=';
            $signo_mant = $mantenimiento;

            $datos = DB::table('equipment_maintenance_schedules')
            ->select('equipment_groups_id')
            ->where('equipment_type_maintenances_id', $mantenimiento)
            ->get();

            
            $i =0;
            $grupos_array = array();
            foreach ($datos as $key) {
                $grupos_array= $key->equipment_groups_id;
                $i++;
            }
            $campos_grupos_mant ='equipment_groups_has_equipment_inventories.equipment_groups_ids';
            $campos_var_mant = '=';
            $grupos_where = "whereBetween";
            // ->whereBetween('id', array('1', '4'))

        } else {
            $grupos_array = '-4';
            $campo_mant = 'equipment_inventories.id';
            $var_mant = '<>';
            $signo_mant = '-3';
            $campos_grupos_mant ='equipment_inventories.id';

            $grupos_where = "where";
            $grupos_where2 = "'equipment_inventories.id','<>',  $grupos_array";
            $campos_var_mant = '<>';
        }
        $tipos_mant = DB::table('equipment_type_maintenances')->where('status', 1)->get();
        $tipo_equipos = DB::table('equipment_types')->get();

        $applicants = DB::table('users')
                // ->join('users', 'users.id', '=', 'equipment_inventories_users.users_id')
                // ->join('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_inventories_users.equipment_inventories_id')
                // ->distinct()
                // ->select('users.id','users.name', 'users.name2', 'users.last_name', 'users.last_name2')
                // ->where('equipment_inventories.id', 'LIKE', '%' . $codigo . '%')
                // ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
                // ->where('equipment_inventories.date_shopping', 'LIKE', '%' . $f_compra . '%')
                // ->where('equipment_inventories.serial', 'LIKE', '%' . $serial . '%')
                // ->where('users.status', 1)
                
                // ->where('equipment_types_id', 4)
                ->orderBy('last_name', 'asc')
                ->get();
        
        $marcas  = DB::table('equipment_inventories')
                // ->join('users', 'users.id', '=', 'equipment_inventories_users.users_id')
                // ->join('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_inventories_users.equipment_inventories_id')
                // ->distinct()
                ->select('equipment_inventories.mark')
                // ->where('equipment_inventories.id', 'LIKE', '%' . $codigo . '%')
                
                // ->where('equipment_inventories.date_shopping', 'LIKE', '%' . $f_compra . '%')
                // ->where('equipment_inventories.serial', 'LIKE', '%' . $serial . '%')
                // ->where('users.status', 1)
                // ->where('users_id', $var_user, $signo_user)
                // ->where('equipment_types_id', 4)
                // ->orderBy('last_name', 'asc')
                ->distinct()
                ->get();
        $funcionarios = DB::table('equipments') 

            ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
            ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
            ->leftJoin('equipment_types', 'equipment_inventories.equipment_types_id', '=', 'equipment_types.id') 
            ->leftJoin('equipment_statuses', 'equipment_statuses.id', '=', 'equipment_inventories.equipment_statuses_id')
            ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id')
            ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id')
            ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
            ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
            ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
            ->leftJoin('equipment_maintenances_statuses', 'equipment_maintenances_statuses.id', '=', 'equipment_maintenances.equipment_maintenances_statuses_id')
            ->leftJoin('equipment_type_maintenances', 'equipment_maintenances.equipment_type_maintenances_id', '=', 'equipment_type_maintenances.id')
            ->leftJoin('users as user_mant', 'users.id', '=', 'equipment_maintenances.users_id')
            ->select(
                'users.name','users.last_name','users.id as id_user','users.img_min','equipment_inventories.mark','equipment_inventories.id as id_elemento','equipments.id as id_equipo_completo','equipment_types.type',
                'equipment_maintenances.created_at as ultimo_mant','equipment_type_maintenances.type_maintenance','equipment_inventories.image','equipment_groups.name_group',
                'equipment_maintenances.equipment_maintenances_statuses_id as estado_mant','equipment_maintenances.detalle as desc_mant','equipment_type_maintenances.id as id_mant',
                'users.name as nom_mant','users.last_name as ap_mant'
                )
            
            ->where($campo_grupo, $var_grupo, $signo_grupo )
            ->where($campo_user, $var_user, $signo_user)
            ->where($campo_mant, $var_mant, $signo_mant)
            ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
            ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')
            ->whereNotIn('equipment_inventories.equipment_types_id', array(4, 6, 8, 9))
            
            ->paginate(15);




        $numero_equipos = count($funcionarios);
        $data = array();

        for ($i=0; $i < count($funcionarios); $i++) { 
            $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
            $data[$i]['id_user'] = $funcionarios[$i]->id_user;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo_completo;
            $data[$i]['id_elemento'] = $funcionarios[$i]->id_elemento;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['tipo'] = $funcionarios[$i]->type;
            $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
            if ($funcionarios[$i]->id_mant == "") {
                $data[$i]['id_mant'] = '0';
            }else{
                $data[$i]['id_mant'] = $funcionarios[$i]->id_mant;
            }
            if ($funcionarios[$i]->image != "") {
                $data[$i]['img_equipo'] = $funcionarios[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }

            if ($funcionarios[$i]->name != "") {
                $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                $data[$i]['disabled'] = 'false';

            }else{
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            if ($funcionarios[$i]->name_group != "") {
                $data[$i]['grupo'] = $funcionarios[$i]->name_group;
            }else{
                $data[$i]['grupo'] = "Sin Grupo";
            }

            if ($funcionarios[$i]->type_maintenance != "") {
                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                
                if ($funcionarios[$i]->estado_mant == 1 ) {
                    $data[$i]['row'] = 'red';
                    $data[$i]['tecnico'] = "N/A";
                    $data[$i]['img_mant'] = "assets/img/avatar_small/default.jpg";
                    $data[$i]['style_mant'] = "display:none";
                }else{
                    $data[$i]['row'] = 'green';
                    $data[$i]['img_mant'] = "assets/img/avatar_small/default.jpg";//imagen de usuario mantenimiento
                    $data[$i]['style_mant'] = "";
                    $data[$i]['tecnico'] = $funcionarios[$i]->nom_mant." ".$funcionarios[$i]->ap_mant;//nomres de usuario de mantenimiento
                }
                $data[$i]['fecha_mant'] = $funcionarios[$i]->ultimo_mant;
                $data[$i]['descripcion'] = $funcionarios[$i]->desc_mant;

                

                
            }else{
                $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                $data[$i]['row'] = "red";
                $data[$i]['fecha_mant'] = 'N/A';
                $data[$i]['tecnico'] = 'N/A';
                $data[$i]['descripcion'] = 'N/A';
                $data[$i]['img_mant'] = 'N/A';
                $data[$i]['style_mant'] = "display:none";
            }
           
        }



        if (Input::get('nombre') && Input::get('nombre') != "") {
            return View::make('dashboard.equipos.tabla_mantenimientos')
            
            // ->with('administradores', $funcionarios2)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('numero_equipos', $numero_equipos)
            ->with('data', $data)
            ->with('grupos', $grupos)
            ->with('tipo_equipos', $tipo_equipos)
            ->with('tipos_mant', $tipos_mant)
            ->with('pag', $funcionarios);
            
            exit();
                                 
        } 
        if (Input::get('grupo')) {
             return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.mantenimientos')
            
            // ->with('administradores', $funcionarios2)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('numero_equipos', $numero_equipos)
            ->with('data', $data)
            ->with('grupos', $grupos)
            ->with('tipo_equipos', $tipo_equipos)
            ->with('tipos_mant', $tipos_mant)
            ->with('pag', $funcionarios)
            ->with('submenu_activo', 'Mantenimientos')
            ->with('menu_activo', 'Equipos');

            exit();
         } 

            $funcionarios[] = "";
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.mantenimientos')
            
            // ->with('administradores', $funcionarios2)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('numero_equipos', $numero_equipos)
            ->with('pag', $funcionarios)
            ->with('data', $data)
            ->with('grupos', $grupos)
            ->with('tipos_mant', $tipos_mant)
            ->with('tipo_equipos', $tipo_equipos)
            ->with('submenu_activo', 'Mantenimientos')
            ->with('menu_activo', 'Equipos');
    }
    public function getMostraritemsmantenimiento(){
        $tipo_mant = Input::get("id_tipo");

        //$tipos = EquipmentItemMaintenances::where('equipment_type_maintenances_id', $tipo_mant)->where('status',1)->get();
        $tipos = DB::table('equipment_item_maintenances_has_equipment_type_maintenances') 
        ->join('equipment_item_maintenances','equipment_item_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_item_maintenances_id') 
        ->join('equipment_type_maintenances','equipment_type_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_type_maintenances_id')
        ->select('equipment_type_maintenances.type_maintenance'
            , 'equipment_item_maintenances.item'
            , 'equipment_item_maintenances.id'
            , 'equipment_item_maintenances.created_at'
            , 'equipment_item_maintenances.status'
            , 'equipment_item_maintenances.required'
        )
        ->where('equipment_type_maintenances_id', $tipo_mant)
        ->get();

        return View::make('dashboard.equipos.item_mantenimientos')
        ->with('tipos', $tipos);
    }
    public function getMostraritemsmantenimientogestion(){
        $tipo_mant = Input::get("id_tipo");

        $tipos = EquipmentItemMaintenances::where('equipment_type_maintenances_id', $tipo_mant)->where('status',1)->get();

        return View::make('dashboard.equipos.item_mantenimientos_gestion')
        ->with('tipos', $tipos);
    }
    public function getVermantenimientos() {
        
        $user = Users::find(Auth::user()->id);


        //funciones para filtros
        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        $applicants = Users::orderBy('name', 'asc')->get();
        $marcas = EquipmentInventories::distinct()->select('mark')->get();


        $tipo_mantenimientos_soft = DB::table('equipment_type_maintenances')->get();
       
        $mantenimientos = EquipmentMaintenances::get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($mantenimientos); $i++) {
            $fecha = date($mantenimientos[$i]->created_at);
            $data[$i]["id"] = 'MAN' . str_pad($mantenimientos[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["tecnico"] = $mantenimientos[$i]->Users->name." ". $mantenimientos[$i]->Users->last_name;
            $data[$i]["img"] = $mantenimientos[$i]->Users->img_min;
            $data[$i]["fecha"] = $mantenimientos[$i]->created_at;
            $data[$i]["observaciones"] = $mantenimientos[$i]->detalle;
            $data[$i]["tipo"] = $mantenimientos[$i]->id;

            $variable2 = "";
            $equipos = EquipmentMaintenances::find($mantenimientos[$i]->id)->EquipmentItemMaintenances;
            $equipo = EquipmentMaintenances::find($mantenimientos[$i]->id)->EquipmentInventories;
            
            $data[$i]["users"] = $mantenimientos[$i]->Users->name." ". $mantenimientos[$i]->Users->last_name;
            foreach ($equipos as $key) {
                $texto = $key->item."<br>";
                $variable2 = $variable2.$texto;
                $variable3 = $key->EquipmentTypeMaintenances->type_maintenance;
            }
            $user = DB::table('equipment_inventories_users')
            ->join('users', 'users.id','=',  'equipment_inventories_users.users_id')
            ->where('equipment_inventories_users.equipment_inventories_id',$equipo[0]->id)
            ->get();
            
            $data[$i]["traces"] = $variable2;
            $data[$i]["tipo"] = $variable3;
            $data[$i]["equipo"] = $equipo[0]->EquipmentTypes->type;



            $data[$i]["asign"] = $user[0]->name." ".$user[0]->last_name;

            
        }



                               
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.ver_mantenimientos')
            ->with('mantenimientos', $data)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('tipos', $tipo_mantenimientos_soft )
            ->with('submenu_activo', 'Mantenimientos')
            ->with('menu_activo', 'Equipos');
    }
    public function getGestion() {
        
        $user = Users::find(Auth::user()->id);

        //funciones para filtros
        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        $grupo = Input::get('grupo');
        $tipo_equipo = Input::get('tipo_equipo');
        $mantenimiento = Input::get('mantenimiento');

        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'equipment_inventories_users.users_id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'equipment_inventories.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
            $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
            $var_grupo = '=';
            $signo_grupo = $grupo;
        } else {
            $campo_grupo = 'equipment_inventories.id';
            $var_grupo = '<>';
            $signo_grupo = '-2';
        }

        if (isset($_GET['mantenimiento']) && !empty($_GET['mantenimiento'])) {
            $campo_mant = 'equipment_type_maintenances.id';
            $var_mant = '=';
            $signo_mant = $mantenimiento;

            $datos = DB::table('equipment_maintenance_schedules')
            ->select('equipment_groups_id')
            ->where('equipment_type_maintenances_id', $mantenimiento)
            ->get();

            
            $i =0;
            $grupos_array = array();
            foreach ($datos as $key) {
                $grupos_array= $key->equipment_groups_id;
                $i++;
            }
            $campos_grupos_mant ='equipment_groups_has_equipment_inventories.equipment_groups_ids';
            $campos_var_mant = '=';
            $grupos_where = "whereBetween";
            // ->whereBetween('id', array('1', '4'))

        } else {
            $grupos_array = '-4';
            $campo_mant = 'equipment_inventories.id';
            $var_mant = '<>';
            $signo_mant = '-3';
            $campos_grupos_mant ='equipment_inventories.id';

            $grupos_where = "where";
            $grupos_where2 = "'equipment_inventories.id','<>',  $grupos_array";
            $campos_var_mant = '<>';
        }
        $tipos_mant = DB::table('equipment_type_maintenances')->where('status', 1)->get();
        $tipo_equipos = DB::table('equipment_types')->get();

        $applicants = DB::table('users')
                // ->join('users', 'users.id', '=', 'equipment_inventories_users.users_id')
                // ->join('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_inventories_users.equipment_inventories_id')
                // ->distinct()
                // ->select('users.id','users.name', 'users.name2', 'users.last_name', 'users.last_name2')
                // ->where('equipment_inventories.id', 'LIKE', '%' . $codigo . '%')
                // ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
                // ->where('equipment_inventories.date_shopping', 'LIKE', '%' . $f_compra . '%')
                // ->where('equipment_inventories.serial', 'LIKE', '%' . $serial . '%')
                // ->where('users.status', 1)
                
                // ->where('equipment_types_id', 4)
                ->orderBy('last_name', 'asc')
                ->get();
        
        $marcas  = DB::table('equipment_inventories')
                // ->join('users', 'users.id', '=', 'equipment_inventories_users.users_id')
                // ->join('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_inventories_users.equipment_inventories_id')
                // ->distinct()
                // ->select('equipment_inventories.mark')
                // ->where('equipment_inventories.id', 'LIKE', '%' . $codigo . '%')
                
                // ->where('equipment_inventories.date_shopping', 'LIKE', '%' . $f_compra . '%')
                // ->where('equipment_inventories.serial', 'LIKE', '%' . $serial . '%')
                // ->where('users.status', 1)
                // ->where('users_id', $var_user, $signo_user)
                // ->where('equipment_types_id', 4)
                // ->orderBy('last_name', 'asc')
                ->get();
        

        $funcionarios = DB::table("equipment_maintenances")
                // ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                // ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipment_inventories_id', '=', 'equipment_inventories.id') 
                // ->leftJoin('equipment_maintenances_details', 'equipment_maintenances_details.equipment_inventories_id', '=', 'equipment_inventories.id') 
                // ->leftJoin('equipment_item_maintenances', 'equipment_item_maintenances.id', '=', 'equipment_maintenances_details.equipment_item_maintenances_id')
                ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
                ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 

                ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                ->leftJoin('equipment_type_maintenances', 'equipment_maintenances.equipment_type_maintenances_id', '=', 'equipment_type_maintenances.id')
                ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                ->leftJoin('users as user_mant', 'equipment_maintenances.users_id', '=', 'user_mant.id') 
                ->leftJoin('equipment_groups', 'equipment_groups_has_equipment_inventories.equipment_groups_id', '=', 'equipment_groups.id')
                ->join('equipment_types', 'equipment_inventories.equipment_types_id', '=', 'equipment_types.id') 
                ->select('equipment_inventories.id as id_elemento','equipment_inventories.image' , 'equipment_inventories.mark', 'users.id as id_user',
                'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_maintenances.equipment_maintenances_statuses_id as estado_mant',
                'equipment_maintenances.detalle as desc_mant','equipments.id as id_equipo',
                'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance',
                'user_mant.name as nom_mant', 'user_mant.last_name as ap_mant', 'user_mant.img_min as img_mant')
                ->where($campo_grupo, $var_grupo, $signo_grupo )
                ->where($campo_user, $var_user, $signo_user)
                ->where($campo_mant, $var_mant, $signo_mant)
                // ->whereIn('equipment_groups_has_equipment_inventories.equipment_groups_id', array(var_dump($grupos_array)))


                ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
                ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')
                ->where('equipment_inventories_users.status', 1)
                ->groupBy('equipment_maintenances.id')
                ->paginate(15);

        $numero_equipos = count($funcionarios);
        $data = array();

        for ($i=0; $i < count($funcionarios); $i++) { 
            $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['id_user'] = $funcionarios[$i]->id_user;
            $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
            $data[$i]['id_elemento'] = $funcionarios[$i]->id_elemento;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['tipo'] = $funcionarios[$i]->type;
            $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;

            if ($funcionarios[$i]->image != "") {
                $data[$i]['img_equipo'] = $funcionarios[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }

            if ($funcionarios[$i]->name != "") {
                $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                $data[$i]['disabled'] = 'false';

            }else{
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            if ($funcionarios[$i]->name_group != "") {
                $data[$i]['grupo'] = $funcionarios[$i]->name_group;
            }else{
                $data[$i]['grupo'] = "Sin Grupo";
            }

            if ($funcionarios[$i]->type_maintenance != "") {
                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                
                if ($funcionarios[$i]->estado_mant == 1 ) {
                    $data[$i]['row'] = 'red';
                    $data[$i]['tecnico'] = "N/A";
                    $data[$i]['img_mant'] = "assets/img/avatar_small/default.jpg";
                    $data[$i]['style_mant'] = "display:none";
                }else{
                    $data[$i]['row'] = 'green';
                    $data[$i]['img_mant'] = $funcionarios[$i]->img_mant;
                    $data[$i]['style_mant'] = "";
                    $data[$i]['tecnico'] = $funcionarios[$i]->nom_mant." ".$funcionarios[$i]->ap_mant;
                }
                $data[$i]['fecha_mant'] = $funcionarios[$i]->ultimo_mant;
                $data[$i]['descripcion'] = $funcionarios[$i]->desc_mant;

                

                
            }else{
                $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                $data[$i]['row'] = "red";
                $data[$i]['fecha_mant'] = 'N/A';
                $data[$i]['tecnico'] = 'N/A';
                $data[$i]['descripcion'] = 'N/A';
                $data[$i]['img_mant'] = 'N/A';
                $data[$i]['style_mant'] = "display:none";
            }
           
        }


        if (Input::get('nombre') && Input::get('nombre') != "") {
            return View::make('dashboard.equipos.tabla_gestion_mantenimientos')
            
            // ->with('administradores', $funcionarios2)
            ->with('marcas', $marcas)
            ->with('applicants', $applicants)
            ->with('numero_equipos', $numero_equipos)
            ->with('data', $data)
            ->with('grupos', $grupos)
            ->with('tipo_equipos', $tipo_equipos)
            ->with('tipos_mant', $tipos_mant)
            ->with('pag', $funcionarios);
            
            exit();
                                 
        }                     
            
        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.gestion_mantenimientos')
        // ->with('administradores', $funcionarios2)
        ->with('marcas', $marcas)
        ->with('applicants', $applicants)
        ->with('numero_equipos', $numero_equipos)
        ->with('pag', $funcionarios)
        ->with('data', $data)
        ->with('grupos', $grupos)
        ->with('tipos_mant', $tipos_mant)
        ->with('tipo_equipos', $tipo_equipos)
        ->with('submenu_activo', 'Mantenimientos')
        ->with('menu_activo', 'Equipos');
    }
    public function getConsultarprov(){
        //get POST data
        $texto = Input::get('texto');
                
        // $sql = "SELECT * FROM Users WHERE provider LIKE '%".$texto."%' ORDER BY provider DESC";
        $sql = "SELECT * FROM `users` WHERE `name` LIKE '%".$texto."' or `name2` LIKE '%".$texto."%' or `last_name` LIKE '%".$texto."%' or `last_name2` LIKE '%".$texto."%' ORDER BY last_name DESC";

        $datos = DB::select($sql);
                
        if($datos){
            for($i=0;count($datos)>$i;$i++){
                ?>
                <a onclick="agregarNit('<?php echo $datos[$i]->name; ?>', '<?php echo $datos[$i]->name2; ?>', '<?php echo $datos[$i]->last_name; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                <div class="display_box" align="left">
                <div style="margin-right:6px;"><b><?php echo $datos[$i]->name." ".$datos[$i]->name2." ".$datos[$i]->last_name; ?></b></div></div>
                </a>
                <?php
            }
        }else{
            echo 0;
        }
    }
    public function postCalluserperifericos() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'));
            $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->where('users.id', Input::get('id'))
                ->orderBy('last_name', 'asc')
                ->get();

            $equipos = Users::find(Input::get('id'))->EquipmentInventories;


            return View::make('dashboard.equipos.detalle_equipos_mantenimiento')
            ->with('users', $funcionarios)
            ->with('equipos', $equipos);

            

            exit();

        }
    }
    public function postEnviarmantenimientos() {
        $id     = Input::get('id_details');
        $descripcion = Input::get('descripcion');        
        
        $id_user = Auth::user()->id;
        $i = '0';
        $item = Input::get('id');
        $tipo_mant_asignar = Input::get('tipo_mant_asignar');

        $mantenimiento = New EquipmentMaintenances;

        
        $mantenimiento->users_id = $id_user;
        $mantenimiento->detalle = $descripcion;
        $mantenimiento->equipment_inventories_id = $item;
        $mantenimiento->equipment_type_maintenances_id = $tipo_mant_asignar;
        $mantenimiento->equipment_maintenances_statuses_id = 2;
        $mantenimiento->save();

        $nuevo_mant = EquipmentMaintenances::Where("users_id", $id_user)->get();
        $bandera = $nuevo_mant->last()->id;

        foreach($id as $key) {

            
            DB::table('equipment_maintenances_details')->insert(array(
                array('equipment_maintenances_id' => $bandera, 'equipment_item_maintenances_id' => $key),
                
            ));
            

            $i++;
        }


        echo "2";
    }
    public function postEnviarmantenimientos2() {
        $id     = Input::get('id_details');
        $descripcion = Input::get('descripcion');        
        
        $id_user = Auth::user()->id;
        $i = '0';
        $item = Input::get('id');
        $tipo_mant_asignar = Input::get('tipo_mant_asignar');
        DB::table('equipment_maintenances')
        ->where('id', $item)
        ->update(array('equipment_maintenances_statuses_id' => 2, 'detalle' => $descripcion, 'users_id' => $id_user));

        foreach($id as $key) {

            
            DB::table('equipment_maintenances_details')->insert(array(
                array('equipment_maintenances_id' => $item, 'equipment_item_maintenances_id' => $key),
                
            ));
            

            $i++;
        }


        echo "2";
    }
    public function getActualizardatosperifericos() {
        $ids    = Input::get('ids');
        $marcas = Input::get('marcas');
        $modelos    = Input::get('modelos');
        $seriales   = Input::get('seriales');
                
        
        
        $i = '0';
        
        

        foreach($ids as $key) {

            
            DB::table('equipment_inventories')
            ->where('id', $key)
            ->update(array('mark' => $marcas[$i],  'model' => $modelos[$i], 'serial' => $seriales[$i]));
            
            $i++;
        }


        echo "1";
    }
    public function postActualizarmantenimientos() {
        $id     = Input::get('id_details');
        $descripcion = Input::get('descripcion');        
        
        $id_user = Auth::user()->id;
        $i = '0';
        $item = Input::get('id');
        $tipo_mant_asignar = Input::get('tipo_mant_asignar');

        $mantenimiento = New EquipmentMaintenances;

        $mantenimiento->date = $date;
        $mantenimiento->users_id = $id_user;
        
        $mantenimiento->equipment_inventories_id = $item;
        $mantenimiento->equipment_type_maintenances_id = $tipo_mant_asignar;
        $mantenimiento->equipment_maintenances_statuses_id = 2;
        $mantenimiento->save();

        $nuevo_mant = EquipmentMaintenances::Where("users_id", $id_user)->get();
        $bandera = $nuevo_mant->last()->id;

        foreach($id as $key) {

            
            DB::table('equipment_maintenances_details')->insert(array(
                array('equipment_maintenances_id' => $bandera, 'equipment_item_maintenances_id' => $key),
                
            ));
            

            $i++;
        }


        echo "2";
    }
    public function getIngresoavion(){
        $user = Users::find(Auth::user()->id);

        $applicants = Users::orderBy('name', 'asc')->get();
        
        $tipos = EquipmentTypes::get();
        $providers = Providers::get();
        $payments_methods = PaymentsMethods::get();
        $software = DB::table('equipment_inventories')->distinct('software_additional')->select('software_additional')->get();
        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.ingreso_avion')
            ->with('user', $user)
            ->with('tipos', $tipos)
            ->with('usuarios', $applicants)
            ->with('programas', $software)
            ->with('providers', $providers)
            ->with('payments_methods', $payments_methods)

            ->with('submenu_activo', 'Equipos')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getCreartipoequipo(){


        if (Input::get('tipo_equipo')) {
            $tipo_equipo = Input::get('tipo_equipo');
            $nuevo_tipo = new EquipmentTypes;

            $nuevo_tipo->type = $tipo_equipo;

            if ($nuevo_tipo->save()) {
                $tipos = EquipmentTypes::get();

                foreach ($tipos as $item) {
                    $style = "";
                    if ($item->status == "1") {
                        $btn = '<button type="submit" class="btn btn-danger" onclick="desactivar_tipo('.$item->id.', 0)"><i class="icon-remove"></i> Desactivar</button>';
                    }elseif($item->status == "0"){
                        $style = "red";
                        $btn = '<button type="submit" class="btn btn-success" onclick="desactivar_tipo('.$item->id.', 1)"><i class="icon-ok"></i> Activar</button>';
                    }
                    
                    echo    '<tr class="'.$style.'">
                                 <td>'.$item->id.'</td>
                                 <td>'.$item->type.'</td>
                                 <td>'.$btn.'</td>
                            </tr>';   
                    
                }
            }else{
                echo "2";
            }    
        }elseif (Input::get('desactivar_id')) {


            $desactivar_id = Input::get('desactivar_id');
            $status = Input::get('status');
            DB::update('update equipment_types set status = ? where id = ?', array($status, $desactivar_id));

            $tipos = EquipmentTypes::get();

            foreach ($tipos as $item) {
                $style = "";
                if ($item->status == "1") {
                    $btn = '<button type="submit" class="btn btn-danger" onclick="desactivar_tipo('.$item->id.', 0)"><i class="icon-remove"></i> Desactivar</button>';
                }elseif($item->status == "0"){
                    $style = "red";
                    $btn = '<button type="submit" class="btn btn-success" onclick="desactivar_tipo('.$item->id.', 1)"><i class="icon-ok"></i> Activar</button>';
                }
                
                echo    '<tr class="'.$style.'">
                             <td>'.$item->id.'</td>
                             <td>'.$item->type.'</td>
                             <td>'.$btn.'</td>
                        </tr>';   
                
            }
            
        }
    }
    public function getCreargrupoequipos(){


        if (Input::get('grupo')) {
            $grupo = Input::get('grupo');
            $nuevo_tipo = new EquipmentGroups;

            $nuevo_tipo->name_group = $grupo;

            if ($nuevo_tipo->save()) {
                $tipos = EquipmentGroups::where('status', 1)->get();

                    
                foreach ($tipos as $item) {
                    if($item->status == "0"){
                        $style = "red";
                        $status = "1";
                    }else{
                        $style = "";
                        $status = "0";
                    }
                    
                    echo    '
                        <tr class="'.$style.'" >
                            <td>'.$item->id.'</td>
                            <td>

                                 
                                 <div class="input-group" id="id_grupo_'.$item->id.'">
                                     <input type="text" class="form-control" value="'.$item->name_group.'" id="texto_'.$item->id.'" id="texto_'.$item->id.'" onblur="editar_grupo('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                     <span class="input-group-btn">
                                     
                                     <button class="btn btn-danger" type="button" onclick="eliminar_grupo('.$item->id.', '.$status.')">
                                         <i class="icon-trash"></i>
                                     </button>
                                     
                                     </span>
                                 </div>
                                 

                            </td>
                            
                        </tr>


                    ';   
                    
                }
            }else{
                echo "2";
            }    
        }elseif (Input::get('desactivar_id')) {


            $desactivar_id = Input::get('desactivar_id');
            $status = Input::get('status');
            DB::update('update equipment_groups set status = ? where id = ?', array($status, $desactivar_id));

            $tipos = EquipmentGroups::where('status', 1)->get();

                
            foreach ($tipos as $item) {
                if($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }else{
                    $style = "";
                    $status = "0";
                }
                
                echo    '
                    <tr class="'.$style.'" >
                        <td>'.$item->id.'</td>
                        <td>

                             
                             <div class="input-group" id="id_grupo_'.$item->id.'">
                                 <input type="text" class="form-control" value="'.$item->name_group.'" id="texto_'.$item->id.'" id="texto_'.$item->id.'" onblur="editar_grupo('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                 <span class="input-group-btn">
                                 
                                 <button class="btn btn-danger" type="button" onclick="eliminar_grupo('.$item->id.', '.$status.')">
                                     <i class="icon-trash"></i>
                                 </button>
                                 
                                 </span>
                             </div>
                             

                        </td>
                        
                    </tr>


                ';   
                
            }
            
        }elseif (Input::get('texto_editar')) {
            $texto_editar = Input::get('texto_editar');
            $texto = Input::get('texto');
            DB::update('update equipment_groups set name_group = ? where id = ?', array($texto, $texto_editar));

            echo "1";
        }elseif (Input::get('estado_grupos')) {
            if (Input::get('estado_grupos') == 1) {
                $tipos = EquipmentGroups::where('status', 1)->get();
            }else{
                $tipos = EquipmentGroups::get();
            }

                
            foreach ($tipos as $item) {
                if($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }else{
                    $style = "";
                    $status = "0";
                }
                
                echo    '
                    <tr class="'.$style.'" >
                        <td>'.$item->id.'</td>
                        <td>

                             
                             <div class="input-group" id="id_grupo_'.$item->id.'">
                                 <input type="text" class="form-control" value="'.$item->name_group.'" id="texto_'.$item->id.'" id="texto_'.$item->id.'" onblur="editar_grupo('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                 <span class="input-group-btn">
                                 
                                 <button class="btn btn-danger" type="button" onclick="eliminar_grupo('.$item->id.', '.$status.')">
                                     <i class="icon-trash"></i>
                                 </button>
                                 
                                 </span>
                             </div>
                             

                        </td>
                        
                    </tr>


                ';   
                
            }
        }else{
            echo "2";
        }
    }
    
    public function getCreartipomantenimiento(){


        if (Input::get('desactivar_id')) {


            $desactivar_id = Input::get('desactivar_id');
            $status = Input::get('status');
            DB::update('update equipment_type_maintenances set status = ? where id = ?', array($status, $desactivar_id));

            
            $tipos = EquipmentTypeMaintenances::where('status', 1)->get();

            foreach ($tipos as $item) {

                if ($item->status == "1") {
                    $style = "";
                    $status = "0";
                }elseif($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }
                
                
                echo    '<tr class="'.$style.'" >
                            <td>'.$item->id.'</td>
                            <td style="min-width:150px">'.$item->created_at.'</td>
                            <td>
                                <div class="input-group" id="id_mantenimiento_'.$item->id.'">
                                    <input type="text" class="form-control" value="'.$item->type_maintenance.'" id="texto_'.$item->id.'" onblur="editar_mantenimiento('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                    <span class="input-group-btn">
                                    
                                    <button class="btn btn-danger" type="button" onclick="eliminar_mantenimiento('.$item->id.', '.$status.')">
                                        <i class="icon-trash"></i>
                                    </button>
                                    
                                    </span>
                                </div>
                            </td>
                        </tr>';   
                
            }
            
        }elseif (Input::get('texto_editar')) {
            $texto_editar = Input::get('texto_editar');
            $texto = Input::get('texto');
            DB::update('update equipment_type_maintenances set type_maintenance = ? where id = ?', array($texto, $texto_editar));

            echo "1";
        }elseif (Input::get('estado_mantenimientos')) {
            if (Input::get('estado_mantenimientos') == 1) {
                $tipos = EquipmentTypeMaintenances::where('status', 1)->get();
            }else{
                $tipos = EquipmentTypeMaintenances::get();
            }

                
            foreach ($tipos as $item) {
                if($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }else{
                    $style = "";
                    $status = "0";
                }
                
                echo    '
                    <tr class="'.$style.'" >
                        <td>'.$item->id.'</td>
                        <td style="min-width:150px">'.$item->created_at.'</td>
                        <td>
                            <div class="input-group" id="id_mantenimiento_'.$item->id.'">
                                <input type="text" class="form-control" value="'.$item->type_maintenance.'" id="texto_'.$item->id.'" onblur="editar_mantenimiento('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                <span class="input-group-btn">
                                
                                <button class="btn btn-danger" type="button" onclick="eliminar_mantenimiento('.$item->id.', '.$status.')">
                                    <i class="icon-trash"></i>
                                </button>
                                
                                </span>
                            </div>
                        </td>
                    </tr>


                ';   
                
            }
        }
        if (Input::get('tipo_mantenimiento')) {
            $tipo_equipo = Input::get('tipo_mantenimiento');

            $nuevo_tipo = new EquipmentTypeMaintenances;
            $nuevo_tipo->type_maintenance  = $tipo_equipo;
            $nuevo_tipo->status             = 1;  

            if ($nuevo_tipo->save()) {
                $tipos = EquipmentTypeMaintenances::get();

                foreach ($tipos as $item) {
                    $style = "";
                    if ($item->status == "1") {
                        $btn = '<button type="submit" class="btn btn-danger" onclick="gestionar_tipo_mantenimientos('.$item->id.', 0)"><i class="icon-remove"></i> Desactivar</button>';
                    }elseif($item->status == "0"){
                        $style = "red";
                        $btn = '<button type="submit" class="btn btn-success" onclick="gestionar_tipo_mantenimientos('.$item->id.', 1)"><i class="icon-ok"></i> Activar</button>';
                    }
                    
                    echo    '<tr class="'.$style.'">
                                 <td>'.$item->id.'</td>
                                 <td>'.$item->type_maintenance.'</td>
                                 <td>'.$item->created_at.'</td>
                                 <td>'.$btn.'</td>
                            </tr>';   
                    
                }
            }else{
                echo "2";
            }    
        }
        if (Input::get('item_mantenimiento')) {
            $item = Input::get('item_mantenimiento');
            $tipo = Input::get('tipo');
            


            
            $data = DB::table('equipment_item_maintenances_has_equipment_type_maintenances')
            ->where('equipment_item_maintenances_id', $item)
            ->where('equipment_type_maintenances_id', $tipo)
            ->get();

            if (count($data) != 0) {
                echo "1";
                exit();
            }
            

            DB::table('equipment_item_maintenances_has_equipment_type_maintenances')->insert(array(
                array('equipment_item_maintenances_id' => $item, 'equipment_type_maintenances_id' => $tipo),
            ));

            $tipos = DB::table('equipment_item_maintenances_has_equipment_type_maintenances') 
            ->join('equipment_item_maintenances','equipment_item_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_item_maintenances_id') 
            ->join('equipment_type_maintenances','equipment_type_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_type_maintenances_id')
            ->select('equipment_type_maintenances.type_maintenance'
                , 'equipment_item_maintenances.item'
                , 'equipment_item_maintenances.id'
                , 'equipment_item_maintenances.created_at'
                , 'equipment_item_maintenances.status'
                , 'equipment_item_maintenances.required'
            )
            ->where('equipment_type_maintenances_id', Input::get('tipo'))
            ->get();
              

            
                

                foreach ($tipos as $item) {
                    if($item->status == "0"){
                        $style = "red";
                        $status = "1";
                    }else{
                        $style = "";
                        $status = "0";
                    }
                    if($item->required == "1"){
                        
                        $required = "Si";
                    }else{
                        $required = "No";
                    }
                    echo '
                        <tr class="'.$style.'" >
                            <td>'.$item->id.'</td>
                            <td style="min-width:150px">'.$item->type_maintenance.'</td>
                            <td>
                                <div class="input-group" id="id_mant_'.$item->id.'">
                                    <input type="text" class="form-control" value="'.$item->item.'" id="texto_'.$item->id.'">
                                    <span class="input-group-btn">
                                    
                                    <button class="btn btn-danger" type="button" onclick="eliminar_items_mant('.$item->id.', '.$status.','.$item->id.')">
                                        <i class="icon-trash"></i>
                                    </button>
                                    <button class="btn btn-success" type="button" onclick="editar_items_mant('.$item->id.', '.$item->id.')">
                                        <i class="icon-edit"></i>
                                    </button>
                                    </span>
                                </div>
                            </td>
                            <td style="min-width:150px">'.$required.'</td>
                            <td style="min-width:150px">'.$item->created_at.'</td>

                        </tr>
                    ';
                }
              
        }elseif (Input::get('desactivar_id_item')) {


            $desactivar_id = Input::get('desactivar_id_item');
            $status = Input::get('status');
            DB::update('update equipment_item_maintenances set status = ? where id = ?', array($status, $desactivar_id));

            
            $tipos = EquipmentItemMaintenances::get();

            foreach ($tipos as $item) {
                $style = "";
                if ($item->status == "1") {
                    $btn = '<button type="submit" class="btn btn-danger" onclick="gestionar_item_mantenimientos('.$item->id.', 0)"><i class="icon-remove"></i></button>';
                }elseif($item->status == "0"){
                    $style = "red";
                    $btn = '<button type="submit" class="btn btn-success" onclick="gestionar_item_mantenimientos('.$item->id.', 1)"><i class="icon-ok"></i></button>';
                }
                
                echo    '<tr class="'.$style.'">
                             <td>'.$item->id.'</td>
                             <td>'.$item->item.'</td>
                             <td>'.$item->EquipmentTypeMaintenances->type_maintenance.'</td>
                             <td>'.$item->required.'</td>
                             <td>'.$item->created_at.'</td>
                             <td>'.$btn.'</td>
                        </tr>'; 
                }  
            
        }elseif (Input::get('tipo_mant')) {
            
            $tipos = DB::table('equipment_item_maintenances_has_equipment_type_maintenances') 
            ->join('equipment_item_maintenances','equipment_item_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_item_maintenances_id') 
            ->join('equipment_type_maintenances','equipment_type_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_type_maintenances_id')
            ->select('equipment_type_maintenances.type_maintenance'
                , 'equipment_type_maintenances.id as id_tipo'
                , 'equipment_item_maintenances.item'
                , 'equipment_item_maintenances.id'
                , 'equipment_item_maintenances.created_at'
                , 'equipment_item_maintenances.status'
                , 'equipment_item_maintenances.required'
                )
            ->where('equipment_type_maintenances_id', Input::get('tipo_mant'))
            ->get();

            foreach ($tipos as $item) {
                if($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }else{
                    $style = "";
                    $status = "0";
                }
                if($item->required == "1"){
                    
                    $required = "Si";
                }else{
                    $required = "No";
                }
                echo '
                    <tr class="'.$style.'" >
                        <td>'.$item->id.'</td>
                        <td style="min-width:150px">'.$item->type_maintenance.'</td>
                        <td>
                            <div class="input-group" id="id_mant_'.$item->id.'">
                                <input type="text" class="form-control" value="'.$item->item.'" id="texto_'.$item->id.'" onblur="editar_items_mant('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                <span class="input-group-btn">
                                
                                <button class="btn btn-danger" type="button" onclick="eliminar_items_mant('.$item->id.', '.$status.','.$item->id_tipo.')">
                                    <i class="icon-trash"></i>
                                </button>
                                
                                </span>
                            </div>
                        </td>
                        <td style="min-width:150px">'.$required.'</td>
                        <td style="min-width:150px">'.$item->created_at.'</td>

                    </tr>
                ';
            }
        }
    }
    public function getGestionaritemmant(){
        if (Input::get('desactivar_id')) {


            $desactivar_id  = Input::get('desactivar_id');
            $status         = Input::get('status');
            DB::update('update equipment_item_maintenances set status = ? where id = ?', array($status, $desactivar_id));

            
            //$tipos = EquipmentItemMaintenances::where('equipment_type_maintenances_id', Input::get('tipo'))->get();


            $tipos = DB::table('equipment_item_maintenances_has_equipment_type_maintenances') 
            ->join('equipment_item_maintenances','equipment_item_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_item_maintenances_id') 
            ->join('equipment_type_maintenances','equipment_type_maintenances.id', '=', 'equipment_item_maintenances_has_equipment_type_maintenances.equipment_type_maintenances_id')
            ->select('equipment_type_maintenances.type_maintenance'
                , 'equipment_type_maintenances.id as id_tipo'
                , 'equipment_item_maintenances.item'
                , 'equipment_item_maintenances.id'
                , 'equipment_item_maintenances.created_at'
                , 'equipment_item_maintenances.status'
                , 'equipment_item_maintenances.required'
                )
            ->where('equipment_type_maintenances_id', Input::get('tipo'))
            ->get();

            foreach ($tipos as $item) {

                if ($item->status == "1") {
                    $style = "";
                    $status = "0";
                }elseif($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }
                if($item->required == "1"){
                    
                    $required = "Si";
                }else{
                    $required = "No";
                }
                
                
                echo    '
                            <tr class="'.$style.'" >
                                <td>'.$item->id.'</td>
                                <td style="min-width:150px">'.$item->type_maintenance.'</td>
                                <td>
                                    <div class="input-group" id="id_mant_'.$item->id.'">
                                        <input type="text" class="form-control" value="'.$item->item.'" id="texto_'.$item->id.'" onblur="editar_items_mant('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                        <span class="input-group-btn">
                                        
                                        <button class="btn btn-danger" type="button" onclick="eliminar_items_mant('.$item->id.', '.$status.','.$item->id_tipo.')">
                                            <i class="icon-trash"></i>
                                        </button>
                                        
                                        </span>
                                    </div>
                                </td>
                                <td style="min-width:150px">'.$required.'</td>
                                <td style="min-width:150px">'.$item->created_at.'</td>

                            </tr>
                        ';   
                
            }
            
        }elseif (Input::get('texto_editar')) {
            $texto_editar   = Input::get('texto_editar');
            $texto          = Input::get('texto');

            DB::update('update equipment_item_maintenances set item = ? where id = ?', array($texto, $texto_editar));

            echo "1";
        } 
    }
    public function postCreartipomantenimiento(){

        $id          = Input::get('id_details');
        $descripcion = Input::get('mantenimiento');        
        $date        = Input::get('fecha_limite');
       
        $nuevo_tipo = new EquipmentTypeMaintenances;
        $nuevo_tipo->type_maintenance  = $descripcion;
        $nuevo_tipo->status             = 1;  

        if ($nuevo_tipo->save()) {
            $nuevo_mant = EquipmentTypeMaintenances::get();
            $bandera = $nuevo_mant->last()->id;

            if ($id != "") {
                
                foreach($id as $key) {
                    DB::table('equipment_maintenance_schedules')->insert(array(
                        array('equipment_groups_id' => $key, 'equipment_type_maintenances_id' => $bandera),
                    ));


                    $datos = DB::table('equipment_groups_has_equipment_inventories')
                    ->where('equipment_groups_id', $key)
                    ->get();

                
                    $i =0;
                    $grupos_array = array();
                    foreach ($datos as $dato) {

                        $mantenimiento = New EquipmentMaintenances;

                        $mantenimiento->detalle = "pendiente";
                        
                        $mantenimiento->equipment_inventories_id = $dato->equipment_inventories_id;
                        $mantenimiento->equipment_type_maintenances_id = $bandera;
                        $mantenimiento->equipment_maintenances_statuses_id = 1;
                        $mantenimiento->save();

                       
                    }
                }
                $i =0;
                $grupos_array = array();
                foreach ($datos as $key) {
                    $grupos_array= $key->equipment_groups_id;
                    $i++;
                }
            }

            $tipos = EquipmentTypeMaintenances::get();

            foreach ($tipos as $item) {

                    if($item->status == "0"){
                        $style = "red";
                        $status = "1";
                    }else{
                        $style = "";
                        $status = "0";
                    }
                    if($item->required == "1"){
                        
                        $required = "Si";
                    }else{
                        $required = "No";
                    }
                    echo '
                        <tr class="'.$style.'" >
                            <td>'.$item->id.'</td>
                            <td style="min-width:150px">'.$item->created_at.'</td>
                            <td>
                                <div class="input-group" id="id_mant_'.$item->id.'">
                                    <input type="text" class="form-control" value="'.$item->type_maintenance.'" id="texto_'.$item->id.'" onblur="editar_mantenimiento('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                    <span class="input-group-btn">
                                    
                                    <button class="btn btn-danger" type="button" onclick="eliminar_items_mant('.$item->id.', '.$status.','.$item->id.')">
                                        <i class="icon-trash"></i>
                                    </button>
                                    
                                    </span>
                                </div>
                            </td>
                            
                            

                        </tr>
                    ';
                  
                
            }


            
            
        }
        echo "2"; 
    }
    public function postAsignarmantagrupo(){

        $id          = Input::get('id_details');
        $descripcion = Input::get('mantenimiento');        
        
       
         

        if ($descripcion != "") {
            $nuevo_mant = EquipmentTypeMaintenances::get();
            $bandera = $nuevo_mant->last()->id;

            if ($id != "") {
                
                foreach($id as $key) {
                    DB::table('equipment_maintenance_schedules')->insert(array(
                        array('equipment_groups_id' => $key, 'equipment_type_maintenances_id' => $descripcion),
                    ));


                    $datos = DB::table('equipment_groups_has_equipment_inventories')
                    ->where('equipment_groups_id', $key)
                    ->get();

                
                    $i =0;
                    $grupos_array = array();
                    foreach ($datos as $dato) {

                        $mantenimiento = New EquipmentMaintenances;

                        $mantenimiento->detalle = "pendiente";
                        
                        $mantenimiento->equipment_inventories_id = $dato->equipment_inventories_id;
                        $mantenimiento->equipment_type_maintenances_id = $descripcion;
                        $mantenimiento->equipment_maintenances_statuses_id = 1;
                        $mantenimiento->save();

                       
                    }
                }
                $i =0;
                $grupos_array = array();
                foreach ($datos as $key) {
                    $grupos_array= $key->equipment_groups_id;
                    $i++;
                }
            }

            $tipos = EquipmentTypeMaintenances::get();

            foreach ($tipos as $item) {

                if($item->status == "0"){
                    $style = "red";
                    $status = "1";
                }else{
                    $style = "";
                    $status = "0";
                }
                if($item->required == "1"){
                    
                    $required = "Si";
                }else{
                    $required = "No";
                }
                
                
                echo    '
                        <tr class="'.$style.'" >
                            <td>'.$item->id.'</td>
                            <td style="min-width:150px">'.$item->type_maintenance.'</td>
                            <td>
                                <div class="input-group" id="id_mant_'.$item->id.'">
                                    <input type="text" class="form-control" value="'.$item->item.'" id="texto_'.$item->id.'" onblur="editar_mantenimiento('.$item->id.')" onfocus="capturar_valor('.$item->id.')">
                                    <span class="input-group-btn">
                                    
                                    <button class="btn btn-danger" type="button" onclick="eliminar_items_mant('.$item->id.', '.$status.','.$item->id.')">
                                        <i class="icon-trash"></i>
                                    </button>
                                    
                                    </span>
                                </div>
                            </td>
                            <td style="min-width:150px">'.$required.'</td>
                            <td style="min-width:150px">'.$item->created_at.'</td>

                        </tr>
                        ';   
                
            }


            
            
        }
        echo "2"; 
    }
    public function postMantenimientosequipos() {
        $bandera = "";
        
        $user = Users::find(Auth::user()->id);

        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $var = '=';
            $signo = $applicant;
        } else {
            $var = 'LIKE';
            $signo = '%' . $applicant . '%';
        }

        //consultamos los usuarios administrativos
        return $mantenimientosequipos = DB::table('equipment_inventories_users')
                ->join('users', 'users.id', '=', 'equipment_inventories_users.users_id')
                ->join('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_inventories_users.equipment_inventories_id')
                ->join('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'equipment_inventories.mark', 'equipment_types.type','equipment_inventories.image', 'equipment_inventories.model', 'equipment_inventories.serial', 'users.email_institutional', 'equipment_inventories.ip')
                ->where('users.status', 1)
                ->where('equipment_types_id', '<>', 1)
                ->where('equipment_types_id', '<>', 2)
                ->where('equipment_types_id', '<>', 3)
                ->orderBy('last_name', 'asc')
                ->get();
    }
    public function getLicencias(){
        
        $code = Input::get('code');
        $producto = Input::get('producto');
        $version = Input::get('version');
        $f_compra = Input::get('f_compra');
        $code   = Input::get('code');

        if (isset($_GET['producto']) && !empty($_GET['producto'])) {
            $campo_producto = 'equipment_type_licenses.product';
            $var_producto = '=';
            $signo_producto = $producto;
        } else {
            $campo_producto = 'equipment_licensings.id';
            $var_producto = '<>';
            $signo_producto = '-1';
        }

        $user = Users::find(Auth::user()->id);
        $productos = DB::table('equipment_type_licenses')->distinct('product')->select('product', 'id')->get();
        $versiones = DB::table('equipment_licensings')->distinct('version')->select('version')->orderBY('version', 'asc')->get();

        // $licencias = EquipmentLicensings::get();
        $licencias = DB::table('equipment_licensings')
                ->join('equipment_type_licenses', 'equipment_licensings.equipment_type_licenses_id', '=', 'equipment_type_licenses.id')
                ->select('equipment_licensings.available','equipment_licensings.id', 'equipment_licensings.id_licensing', 'equipment_licensings.provider', 'equipment_licensings.version', 'equipment_licensings.quantity', 'equipment_licensings.date_shopping', 'equipment_licensings.date_expiry', 'equipment_type_licenses.product')
                ->where($campo_producto, $var_producto, $signo_producto)
                ->where('equipment_licensings.version', 'LIKE', '%' . $version . '%')
                ->where('equipment_licensings.id_licensing', 'LIKE', '%' . $code . '%')
                ->orderBy('equipment_licensings.id_licensing')
                ->get();

        $tipos = EquipmentTypeLicenses::get(); 



        if (Input::get('nombre') && Input::get('nombre') != "") {
            return View::make('dashboard.equipos.tabla_lista_licencias')
            
            ->with('user', $user)
            ->with('tipos', $tipos)
            ->with('licencias', $licencias)
            ->with('productos', $productos)
            ->with('versiones', $versiones);
            
            
        } else {
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.licencias')
            ->with('user', $user)
            ->with('tipos', $tipos)
            ->with('licencias', $licencias)
            ->with('productos', $productos)
            ->with('versiones', $versiones)
            ->with('submenu_activo', 'Licencias')
            ->with('menu_activo', 'Equipos');
        }
    }
    public function getLicenciasequipos(){
        
        //funciones para filtros
                $f_compra = Input::get('f_compra');
                $serial = Input::get('serial');
                $codigo = Input::get('code');
                $marca = Input::get('marca');
                $applicant = Input::get('applicant');
                if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
                    $var = '=';
                    $signo = $applicant;
                } else {
                    $var = 'LIKE';
                    $signo = '%' . $applicant . '%';
                }

                $applicants = Users::orderBy('name', 'asc')->get();
                $marcas = EquipmentInventories::distinct()->select('mark')->get();


                $tipo_mantenimientos_soft = DB::table('equipment_type_maintenances')->get();
               
                $mantenimientos = EquipmentMaintenances::get();

                $fecha_actual = date('Y-m-d H:i:s');
                $data = array();
                for ($i = 0; $i < count($mantenimientos); $i++) {
                    $fecha = date($mantenimientos[$i]->created_at);
                    $data[$i]["id"] = 'MAN' . str_pad($mantenimientos[$i]->id, 6, "0", STR_PAD_LEFT);
                    $data[$i]["tecnico"] = $mantenimientos[$i]->Users->name." ". $mantenimientos[$i]->Users->last_name;
                    $data[$i]["img"] = $mantenimientos[$i]->Users->img_min;
                    $data[$i]["fecha"] = $mantenimientos[$i]->created_at;
                    $data[$i]["observaciones"] = $mantenimientos[$i]->detalle;
                    $data[$i]["tipo"] = $mantenimientos[$i]->id;

                    $variable2 = "";
                    $equipos = EquipmentMaintenances::find($mantenimientos[$i]->id)->EquipmentItemMaintenances;
                    $equipo = EquipmentMaintenances::find($mantenimientos[$i]->id)->EquipmentInventories;
                    
                    $data[$i]["users"] = $mantenimientos[$i]->Users->name." ". $mantenimientos[$i]->Users->last_name;
                    foreach ($equipos as $key) {
                        $texto = $key->item."<br>";
                        $variable2 = $variable2.$texto;
                        $variable3 = $key->EquipmentTypeMaintenances->type_maintenance;
                    }
                    $user = DB::table('equipment_inventories_users')
                    ->join('users', 'users.id','=',  'equipment_inventories_users.users_id')
                    ->where('equipment_inventories_users.equipment_inventories_id',$equipo[0]->id)
                    ->get();
                    
                    $data[$i]["traces"] = $variable2;
                    $data[$i]["tipo"] = $variable3;
                    $data[$i]["equipo"] = $equipo[0]->EquipmentTypes->type;



                    $data[$i]["asign"] = $user[0]->name." ".$user[0]->last_name;

                    
                }
                return View::make('dashboard.index')
                    ->with('container', 'dashboard.equipos.licencias_equipos')
                    ->with('mantenimientos', $data)
                    ->with('marcas', $marcas)
                    ->with('applicants', $applicants)
                    ->with('tipos', $tipo_mantenimientos_soft )
                    ->with('submenu_activo', 'Licencias')
                    ->with('menu_activo', 'Equipos');
    }
    public function getDetallelicencia(){
        
        $seriales = EquipmentKeyLicenses::where('equipment_licensings_id', Input::get('id'))->get();

        $disponibles = EquipmentLicensings::where('id', Input::get('id'))->select('available')->get();
        $equipos = EquipmentInventories::where('equipment_types_id', 7)->get();

        $funcionarios = DB::table('equipments')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
        ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
        ->leftJoin('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
        ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
        ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
        ->leftJoin('equipment_offices', 'equipment_offices.id', '=', 'equipment_inventories.equipment_offices_id')
        ->select('users.id as id_user','users.name', 'users.last_name', 'users.img_min', 'equipments.id as id_equipo', 'equipment_inventories.mark',
            'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_inventories.id as id_elemento','equipment_inventories.serial',
            'equipment_inventories.observation', 'equipment_offices.sede as ubicacion',
            'equipment_type_maintenances.type_maintenance', 'equipment_inventories.image', 'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance')
        ->where('equipment_inventories.equipment_types_id', 7)
        ->where('equipment_inventories_users.status', 1)
        ->distinct('equipment_inventories.id')
        ->get();

        $asignados = DB::table('equipment_licensings')
        ->leftJoin('equipment_key_licenses', 'equipment_key_licenses.equipment_licensings_id', '=', 'equipment_licensings.id')
        ->leftjoin('equipment_inventories_equipment_key_licenses', 'equipment_key_licenses_id', '=', 'equipment_key_licenses.id')
        ->leftjoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_inventories_equipment_key_licenses.equipment_inventories_id')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        // ->leftjoin('equipment_key_licenses', 'equipment_key_licenses.id', '=', 'equipment_inventories_equipment_key_licenses.equipment_key_licenses_id')
        ->select('equipment_inventories.image', 'equipment_inventories.id as id_equipo','equipment_inventories.mark', 'equipment_key_licenses.id as id_licensing',
            'equipment_key_licenses.serial', 'equipment_inventories_equipment_key_licenses.id as id_asignacion','users.name','users.last_name')
        ->where('equipment_licensings.id', Input::get('id'))
        ->where('equipment_inventories_equipment_key_licenses.status', 1)
        ->where('equipment_inventories_users.status',1)
        ->get();

        $numero = count($asignados);

        return View::make('dashboard.equipos.detalle_licencia')
        ->with('seriales', $seriales)
        ->with('equipos', $funcionarios)
        ->with('disponibles', $disponibles)
        ->with('asignados', $asignados)
        ->with('disp', $disponibles);
    }
    public function getNuevalicencia(){
        
        $licencia = new EquipmentLicensings;
       
        $licencia->id_licensing = Input::get('idlicencia');
        $licencia->provider = Input::get('proveedor');
        $licencia->version = Input::get('version');
        $licencia->quantity = Input::get('cantidad');
        $licencia->available = Input::get('cantidad');
        $licencia->status = 1;
        $licencia->equipment_type_licenses_id = Input::get('producto');
        $licencia->date_shopping = Input::get('fecha_compra');
        $licencia->date_expiry = Input::get('fecha_expiracion');
        $seriales = Input::get('serial');
        
        if($licencia->save()){
            
            $last_id = EquipmentLicensings::all();
            $last_id = $last_id->last()->id;
            
            for($i=0;count($seriales)>$i;$i++){
                
                $details = new EquipmentKeyLicenses;
                $details->product = $seriales[$i][0];
                $details->serial = $seriales[$i][1];
                $details->equipment_licensings_id = $last_id;
                $details->save();                    
                
            }    
            return 1;
        }else{
            return 0;
        }
    }
    public function getAsignarseriales(){
        $serial     = Input::get('equipo_asignar_id');
        $licencia   = Input::get('serial_asignar_id');
        $disp       = Input::get('disp');
        $id_licencia = Input::get('id_licencia');

        $total = $disp - 1;

        DB::table('equipment_inventories_equipment_key_licenses')->insert(
            array('equipment_inventories_id' => $serial, 'equipment_key_licenses_id' => $licencia, 'status' => 1)
        );

        DB::update('update equipment_licensings set available  = ? where id = ?', array($total ,$id_licencia));

        echo "1";
    }
    public function getEliminarseriales(){
        $serial     = Input::get('id_asignacion');
        $licencia   = Input::get('serial_asignar_id');
        $disp       = Input::get('disp');
        $id_licencia = Input::get('id_licencia');

        $total = $disp + 1;

        
        DB::update('update equipment_inventories_equipment_key_licenses set status  = 0 where id = ?', array($serial));

        DB::update('update equipment_licensings set available  = ? where id = ?', array($total ,$id_licencia));

        echo "1";
    }
    public function getAgregarmantenimientos() {
            $comentario = Input::get('comentario');
            $permisos_a = Input::get('permisos_a');
            $id_equipo = Input::get('id_equipo');
            
            // foreach ($permisos_a as $per) {
                
            //     DB::table('profiles_submenus')->insert(
            //         array('profiles_id' => $perfil, 'submenus_id' => $per)
            //     );     
                
            // }
            // __________________________________
            $id     = Input::get('permisos_a');
            $comentario = Input::get('comentario');        
            
            $id_user = Auth::user()->id;
            


            $mantenimiento = New EquipmentMaintenances;

            $mantenimiento->details = $comentario;
            $mantenimiento->users_id = $id_user;
            $mantenimiento->save();

            $nuevo_mant = EquipmentMaintenances::Where("users_id", $id_user)->get();
            $bandera = $nuevo_mant->last()->id;

            foreach($id as $key) {

                DB::table('equipment_maintenances_details')->insert(array(
                    array('equipment_inventories_id' => $id_equipo, 'equipment_maintenances_id' => $bandera, 'type_maintenances_id' => $key),
                    
                ));

                
            }


            echo 1;
    }
    public function getGenerarpdfequipo(){
    
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'))->Profiles;

            $funcionarios1 = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->where('users.id', Input::get('id'))
                ->orderBy('last_name', 'asc')
                ->get();

            $equipos = EquipmentInventories::where('equipment_inventories.id', Input::get('id_elemento'))->get();

            $perifericos = Equipments::find(Input::get('id_equipo'))->EquipmentInventories()->where('equipment_types_id', '<>', 7)->get();

            $variable2 = "";
            $mantenimientos = EquipmentMaintenances::where('equipment_inventories_id', Input::get('id_equipo'))->get();
                if (count($mantenimientos) == 0) {

                    $i = 0;
                    $data_m[$i]["id"]               = "N/A";
                    $data_m[$i]["tecnico"]          = "N/A";
                    $data_m[$i]["tipo_mant"]        = "N/A";
                    $data_m[$i]["marca"]            = "N/A";
                    $data_m[$i]["observaciones"]    = "N/A";
                    $data_m[$i]["f_mantenimiento"]  = "N/A";

                    $data_last_m[$i]['ultimo'] = "N/A";


                }else{
            
                    for ($i = 0; $i < count($mantenimientos); $i++) {
                        $data_m[$i]["id"] = 'MAN' . str_pad($mantenimientos[$i]->equipment_maintenances_id, 6, "0", STR_PAD_LEFT);
                        $data_m[$i]["tecnico"]        = $mantenimientos[$i]->id;
                        $data_m[$i]["tipo_mant"]      = $mantenimientos[$i]->id;
                        $data_m[$i]["marca"]          = $mantenimientos[$i]->id;
                        $data_m[$i]["observaciones"]  = $mantenimientos[$i]->id;
                        $data_m[$i]["f_mantenimiento"] = $mantenimientos[$i]->id;

                        $data_last_m[$i]['ultimo'] = $mantenimientos->last()->created_at;
                    }

                        
                }

            $fecha_actual = date('Y-m-d H:i:s');
            $data = array();
            for ($i = 0; $i < count($equipos); $i++) {
                
                if ($equipos[0]->equipment_types_id == 7) {
                    $data[$i]["num_equipo"] = $equipos[$i]->id;
                    $data[$i]["nomb_equipo"] = $equipos[$i]->id." ".$equipos[$i]->model;
                    $data[$i]["mac"] = $equipos[$i]->mac;
                    $data[$i]["date_asign"] = $equipos[$i]->date_asign;
                    $data[$i]["procesador"] = $equipos[$i]->processor;
                    $data[$i]["ram"] = $equipos[$i]->memory;
                    $data[$i]["disco_duro"] = $equipos[$i]->hard_drive;
                    $data[$i]["sitema_o"] = $equipos[$i]->operating_system;
                    $data[$i]["version_op"] = $equipos[$i]->version_op;
                    $data[$i]["license_op"] = $equipos[$i]->license_op;
                    $data[$i]["img_equipo"] = $equipos[$i]->image;
                    
                    $data[$i]["id"] = 'EQ' . str_pad($equipos[$i]->id_equipo, 6, "0", STR_PAD_LEFT);
                    $data[$i]["tecnico"]        = $equipos[$i]->name." ". $equipos[$i]->last_name;
                    $data[$i]["img"]            = $equipos[$i]->img_min;
                    $data[$i]["fecha"]          = $equipos[$i]->created_at;
                    $data[$i]["observaciones"]  = $equipos[$i]->id_equipo;
                    $data[$i]["marca"]          = $equipos[$i]->mark;
                    $data[$i]["tipo"]           = $equipos[$i]->type;
                    $data[$i]["f_mantenimiento"]= $equipos[$i]->mark;
                    
                }
                
                
            }


            $vista = View::make('dashboard.equipos.pdf_detalle_equipos')
            ->with('users', $funcionarios1)
            ->with('equipos', $equipos)
            ->with('data', $data)
            ->with('data_per', $perifericos)
            ->with('datam', $data_m)
            ->with('user', $user)
            ->with('data_last_m', $data_last_m)
            ->with('mantenimientos', $mantenimientos);
            ;
        }
        PDF::load($vista, 'A4', 'portrait')->show();
    }
    public function getCargarequiposdelgrupo(){
        $id_grupo = Input::get('id_grupo');
        $tipo = Input::get('tipo_equipo');
        $id_proceso = Input::get('id_process');

        if (Input::get('eliminar')) {

            $datos = DB::table('equipment_inventories')
            
            ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
            ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
            ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
            ->join('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
            ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name', 'equipment_inventories.observation')
            ->where('equipment_groups_has_equipment_inventories.equipment_groups_id', $id_grupo)
            ->get();

            $vista  = "multiselect_eliminar_equipos";

            return View::make('dashboard.equipos.multiselect_eliminar_equipos')
            ->with('datos', $datos);
            exit();
        }

        

        if (isset($_GET['id_grupo']) && !empty($_GET['id_grupo'])) {
            $cons_grupo = "where (equipment_groups_has_equipment_inventories.equipment_groups_id <> '$id_grupo'
            or `equipment_groups_has_equipment_inventories`.`id` is null)";
        } else {
            $cons_grupo = "Where equipment_inventories.id <> -1";
        }


        if (isset($_GET['tipo_equipo']) && !empty($_GET['tipo_equipo'])) {
            $cons_tipo = " and equipment_inventories.equipment_types_id = '$tipo'";
        } else {
            $cons_tipo = " and equipment_inventories.id <> -1";
        }

        if (isset($_GET['id_process']) && !empty($_GET['id_process'])) {
            $cons_proceso = " and profiles.processes_id = '$id_proceso'";
        } else {
            $cons_proceso = " and equipment_inventories.id <> -1";
        
        }
        

        





        $sql = "SELECT `equipment_inventories`.`id`, `equipment_inventories`.`mark`, `users`.`name`, `users`.`last_name`, `equipment_inventories`.`observation` 
                from `equipment_inventories` 
                left join `equipment_groups_has_equipment_inventories` on `equipment_inventories_id` = `equipment_inventories`.`id` 
                left join `equipments_has_equipment_inventories` on `equipments_has_equipment_inventories`.`equipment_inventories_id` = `equipment_inventories`.`id` 
                left join `equipments` on `equipments`.`id` = `equipments_has_equipment_inventories`.`equipments_id` 
                left join `equipment_inventories_users` on `equipment_inventories_users`.`equipments_id` = `equipments_has_equipment_inventories`.`equipments_id` 
                left join `users` on `equipment_inventories_users`.`users_id` = `users`.`id` 
                left join `profiles_users` on `profiles_users`.`users_id` = `users`.`id` 
                left join `profiles` on `profiles`.`id` = `profiles_users`.`profiles_id` 
                ".$cons_grupo." ".$cons_tipo." ".$cons_proceso."";

        $sql = DB::select($sql);



        

        return View::make('dashboard.equipos.multiselect_agregar_equipos')
        ->with('datos', $sql);
    }
    public function getAgregarequiposalgrupo() {
            
            $grupo = Input::get('grupo'); 

            $permisos_a = Input::get('permisos_eq');                                    
            
            foreach ($permisos_a as $per) {
                
            

                $cons = DB::table('equipment_groups_has_equipment_inventories')
                ->where('equipment_groups_id', $grupo)
                ->where('equipment_inventories_id', $per)
                ->get();

                if (count($cons) == 0) {
                    DB::table('equipment_groups_has_equipment_inventories')->insert(
                        array('equipment_groups_id' => $grupo, 'equipment_inventories_id' => $per)
                    );     
                }
            }
            
            echo 1;
    }
    public function getEliminarequiposdelgrupo() {
            
            $grupo = Input::get('grupo'); 

            $permisos_a = Input::get('permisos_eq');                                    
            
            foreach ($permisos_a as $per) {
                
            
                DB::table('equipment_groups_has_equipment_inventories')
                ->where('equipment_groups_id', $grupo)
                ->where('equipment_inventories_id', $per)
                ->delete();    
                
            }
            
            echo 1;
    }
    public function getEstadisticas() {
            
        $sql_estados = "SELECT equipment_statuses.id, equipment_statuses.status , COUNT( * ) AS dato 
        FROM `equipment_inventories` join equipment_statuses on (equipment_statuses.id = equipment_inventories.equipment_statuses_id) 
        GROUP BY equipment_statuses.id ORDER BY dato DESC";


        $sql_tipos = "SELECT equipment_types.id, equipment_types.type , COUNT( * ) AS dato 
        FROM `equipment_inventories` join equipment_types on (equipment_types.id = equipment_inventories.equipment_types_id)
         WHERE equipment_types.id NOT IN (8,9) 
        GROUP BY equipment_types.id ORDER BY dato DESC";

        $sql_grupos = "SELECT equipment_groups.id, equipment_groups.name_group , COUNT( * ) AS dato FROM `equipment_groups` 
        join equipment_groups_has_equipment_inventories on (equipment_groups.id = equipment_groups_has_equipment_inventories.equipment_groups_id) 
        GROUP BY equipment_groups.id ORDER BY dato DESC";

        $sql_procesos = "SELECT processes.acronyms,COUNT( * ) AS dato FROM `equipments`
        join equipment_inventories_users on (equipment_inventories_users.equipments_id = equipments.id)
        join users on (equipment_inventories_users.users_id = users.id)
        join profiles_users on (profiles_users.users_id = users.id)
        join profiles on (profiles.id = profiles_users.profiles_id)
        join processes on (processes.id = profiles.processes_id)
        GROUP BY processes.acronyms ORDER BY dato DESC";

        $sql_impresoras = "SELECT processes.acronyms,COUNT( * ) AS dato FROM equipment_inventories
        join equipments_has_equipment_inventories on (equipments_has_equipment_inventories.equipment_inventories_id = equipment_inventories.id)
        join equipments on (equipments_has_equipment_inventories.equipments_id = equipments.id)
        join equipment_inventories_users on (equipment_inventories_users.equipments_id = equipments.id)
        join users on (equipment_inventories_users.users_id = users.id)
        join profiles_users on (profiles_users.users_id = users.id)
        join profiles on (profiles.id = profiles_users.profiles_id)
        join processes on (processes.id = profiles.processes_id)
        where equipment_inventories.equipment_types_id = 3
        GROUP BY processes.acronyms ORDER BY dato DESC";

        $sql_licencias = "SELECT equipment_type_licenses.product,COUNT( * ) AS dato FROM `equipment_licensings` 
        join equipment_type_licenses on (equipment_type_licenses.id = equipment_licensings.equipment_type_licenses_id)
        GROUP BY equipment_type_licenses.product ORDER BY dato DESC";


        
        if (Input::get('fecha_inicio')) {
                $sql_tipos = "SELECT equipment_types.id, equipment_types.type , COUNT( * ) AS dato 
                FROM `equipment_inventories` join equipment_types on (equipment_types.id = equipment_inventories.equipment_types_id) 
                GROUP BY equipment_types.id ORDER BY id DESC";
        }

        $cons_estados = DB::select($sql_estados);
        $cons_tipos = DB::select($sql_tipos);
        $cons_grupos = DB::select($sql_grupos);
        $cons_procesos = DB::select($sql_procesos);
        $cons_impresoras = DB::select($sql_impresoras);
        $cons_licencias = DB::select($sql_licencias);

        if (Input::get('fecha_inicio')) {
            return View::make('dashboard.equipos.scrip_graficos')
            // ->with('container', 'dashboard.equipos.estadisticas')
            ->with('data_estados', $cons_estados)
            ->with('data_tipos', $cons_tipos)
            ->with('data_grupos', $cons_grupos)
            ->with('data_procesos', $cons_procesos)
            ->with('data_impresoras', $cons_impresoras)
            ->with('data_licencias', $cons_licencias);
            // ->with('submenu_activo', 'Estadísticas')
            // ->with('menu_activo', 'Equipos'); 
            exit();   
        }

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.estadisticas')
        ->with('data_estados', $cons_estados)
        ->with('data_tipos', $cons_tipos)
        ->with('data_grupos', $cons_grupos)
        ->with('data_procesos', $cons_procesos)
        ->with('data_impresoras', $cons_impresoras)
        ->with('data_licencias', $cons_licencias)
        ->with('submenu_activo', 'Estadísticas')
        ->with('menu_activo', 'Equipos');      
    }
    public function getTablaeliminarasignacion(){
        if (Input::get('id_equipo') == "") {
            return " ";
            exit();
        }

        $equipos = DB::table("equipment_inventories")
                ->leftJoin('equipment_types','equipment_types.id', '=', 'equipment_inventories.equipment_types_id') 
                ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                ->select('equipment_inventories.id as id_equipo','equipment_inventories.image' , 'equipment_inventories.mark', 'users.id as id_user', 'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type'
                ,'equipment_inventories_users.id as id_asignacion', 'equipments_has_equipment_inventories.id as id_asignacion_equipo','equipments_has_equipment_inventories.status')
                ->where('equipments_has_equipment_inventories.equipments_id',Input::get('id_equipo'))
                ->groupBy('equipment_inventories.id')
                ->paginate(15);

        $Usuarios = DB::table("equipment_inventories")
                ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                ->select('equipment_inventories_users.id as id_asignacion_usuario','users.id as id_user', 'users.name', 'users.last_name', 'users.img_min'
                ,'equipment_inventories_users.id as id_asignacion', 'equipment_inventories_users.status')
                ->where('equipments_has_equipment_inventories.equipments_id',Input::get('id_equipo'))
                ->groupBy('users.id')
                ->paginate(15);
        

        for ($i=0; $i < count($equipos); $i++) { 
            $data[$i]['user'] = $equipos[$i]->name." ".$equipos[$i]->last_name;
            $data[$i]['marca'] = $equipos[$i]->mark;
            $data[$i]['id_user'] = $equipos[$i]->id_user;
            $data[$i]['id_equipo'] = $equipos[$i]->id_equipo;
            $data[$i]['id_equipo_asignacion'] = $equipos[$i]->id_asignacion_equipo;
            
            $data[$i]['marca'] = $equipos[$i]->mark;
            $data[$i]['tipo'] = $equipos[$i]->type;
            $data[$i]['mantenimiento'] = 'mantenimiento';
            $data[$i]['tipo_mant'] = 'tipo_mant';

            if ($equipos[$i]->image != "") {
                $data[$i]['img_equipo'] = $equipos[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }

            if ($equipos[$i]->name != "") {
                $data[$i]['user'] = $equipos[$i]->name." ".$equipos[$i]->last_name;
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $equipos[$i]->img_min;
                $data[$i]['disabled'] = 'false';

            }else{
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            
            $data[$i]['tipo_mant'] = "Sin Mantenimientos";
            if ($equipos[$i]->status == 1) {
                $data[$i]['row'] = "green";
                $data[$i]['status'] = "Activo";

            }else{
                $data[$i]['row'] = "red";
                $data[$i]['status'] = "Inactivo";
            }
            
           
        }


        return View::make('dashboard.equipos.tabla_eliminar_asignacion')
        ->with('usuarios', $Usuarios)
        ->with('data', $data);
    }
    public function getEliminarasignacion(){
        // DB::table('equipments_has_equipment_inventories')->where('id', Input::get('id_equipo'))->delete();

        DB::table('equipments_has_equipment_inventories')
        ->where('id', Input::get('id_equipo'))
        ->update(array('status' => 0));

        echo "1";
    }
    public function getEliminarasignacionusuario(){
        // DB::table('equipment_inventories_users')->where('id', Input::get('id_equipo'))->delete();

        DB::table('equipment_inventories_users')
        ->where('id', Input::get('id_equipo'))
        ->update(array('status' => 0));

        echo "1";
    }
    public function getActualizardatosequipos(){
        $id_equipo = Input::get('id_equipo');

        $equipo_marca_editar = Input::get('equipo_marca_editar');
        $equipo_modelo_editar = Input::get('equipo_modelo_editar');
        $equipo_serial_editar = Input::get('equipo_serial_editar');
        $equipo_fechaengrega_editar = Input::get('equipo_fechaengrega_editar');
        // $equipo_estado_editar = Input::get('equipo_estado_editar');
        $equipo_ip_editar = Input::get('equipo_ip_editar');
        $equipo_mac_editar = Input::get('equipo_mac_editar');
        $equipo_ram_editar = Input::get('equipo_ram_editar');
        $equipo_disco_editar = Input::get('equipo_disco_editar');
        $equipo_procesador_editar = Input::get('equipo_procesador_editar');
        $id_equipo_edit = Input::get('id_equipo_edit');
        $equipo_nombre_editar = Input::get('equipo_nombre_editar');

        DB::update('update equipment_inventories set observation = ?, mark  = ?, model = ?, serial = ?, date_asign = ?,ip = ?, mac = ?, memory = ?, hard_drive = ?, processor = ?  where id = ?', array($equipo_nombre_editar, $equipo_marca_editar, $equipo_modelo_editar, $equipo_serial_editar, $equipo_fechaengrega_editar, $equipo_ip_editar, $equipo_mac_editar, $equipo_ram_editar, $equipo_disco_editar, $equipo_procesador_editar, $id_equipo_edit));


        echo "1";
    }
    public function postActualizardatosequipos(){
        if (Input::file('imagen_monitor_editar')) {
            $destinationPath = 'assets/img/equipos/';
            $equipo_marca_editar = Input::get('equipo_marca_editar');
            $equipo_modelo_editar = Input::get('equipo_modelo_editar');
            $equipo_serial_editar = Input::get('equipo_serial_editar');
            $equipo_fechaengrega_editar = Input::get('equipo_fechaengrega_editar');
            $equipo_ip_editar = Input::get('equipo_ip_editar');
            $equipo_mac_editar = Input::get('equipo_mac_editar');
            $equipo_ram_editar = Input::get('equipo_ram_editar');
            $equipo_disco_editar = Input::get('equipo_disco_editar');
            $equipo_procesador_editar = Input::get('equipo_procesador_editar');
            $id_equipo_edit = Input::get('id_equipo_edit');
            $equipo_nombre_editar = Input::get('equipo_nombre_editar');
            
            $filename = $equipo_serial_editar."_".Input::file('imagen_monitor_editar')->getClientOriginalName();
            $imagen_equipo =  $destinationPath.$filename;
            $uploadSuccess = Input::file('imagen_monitor_editar')->move($destinationPath, $filename);

            if ($uploadSuccess) {
                DB::update('update equipment_inventories set observation = ?, image = ?, mark  = ?, model = ?, serial = ?, date_asign = ?,ip = ?, mac = ?, memory = ?, hard_drive = ?, processor = ?  where id = ?', array($equipo_nombre_editar, $imagen_equipo, $equipo_marca_editar, $equipo_modelo_editar, $equipo_serial_editar, $equipo_fechaengrega_editar, $equipo_ip_editar, $equipo_mac_editar, $equipo_ram_editar, $equipo_disco_editar, $equipo_procesador_editar, $id_equipo_edit));
                    echo "1";
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function postDarequipodebaja(){


        if (Input::file('archivo_dar_de_baja')) {
            $destinationPath = 'assets/img/equipos/equipos_de_baja/';
            $id_equipo = Input::get('id_equipo');
            
            $filename = $id_equipo."_".Input::file('archivo_dar_de_baja')->getClientOriginalName();

            $imagen_equipo =  $destinationPath.$filename;
            $uploadSuccess = Input::file('archivo_dar_de_baja')->move($destinationPath, $filename);

            if ($uploadSuccess) {
                DB::update('update equipment_inventories set equipment_statuses_id = 3, decommissioned = ?  where id = ?', array($imagen_equipo, $id_equipo));
                echo "1";
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function getInformes(){

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.informes')
        
        ->with('submenu_activo', 'Informes')
        ->with('menu_activo', 'Equipos');   
    }
    public function getGenerarinformeequipo(){
                
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        $cont = 0;
        
        $id_informe = Input::get('id_grafico');
        switch ($id_informe) {
            case '1':
                    $user = Users::find(Auth::user()->id);

                    //funciones para filtros
                    $f_compra = Input::get('f_compra');
                    $serial = Input::get('serial');
                    $codigo = Input::get('code');
                    $marca = Input::get('marca');
                    $applicant = Input::get('applicant');
                    $grupo = Input::get('grupo');
                    $tipo_equipo = Input::get('tipo_equipo');

                    // $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

                    if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
                        $campo_user = 'equipment_inventories_users.users_id';
                        $var_user = '=';
                        $signo_user = $applicant;
                    } else {
                        $campo_user = 'equipment_inventories.id';
                        $var_user = '<>';
                        $signo_user = '-1';
                    }

                    if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
                        $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
                        $var_grupo = '=';
                        $signo_grupo = $grupo;
                    } else {
                        $campo_grupo = 'equipment_inventories.id';
                        $var_grupo = '<>';
                        $signo_grupo = '-2';
                    }

                    $funcionarios = DB::table("equipment_inventories")
                            // ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipment_inventories_id', '=', 'equipment_inventories.id')
                            // ->leftJoin('equipment_types','equipment_types.id', '=', 'equipment_inventories.equipment_types_id') 
                            ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                            ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id')  
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                            // ->leftJoin('equipment_maintenances_details', 'equipment_maintenances_details.equipment_inventories_id', '=', 'equipment_inventories.id') 
                            ->leftJoin('equipment_maintenances', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                            // ->leftJoin('equipment_item_maintenances', 'equipment_item_maintenances.id', '=', 'equipment_maintenances_details.equipment_item_maintenances_id')
                            ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
                            ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('equipment_groups', 'equipment_groups_has_equipment_inventories.equipment_groups_id', '=', 'equipment_groups.id')
                            ->join('equipment_types', 'equipment_inventories.equipment_types_id', '=', 'equipment_types.id') 
                            ->select('equipment_inventories.id as id_equipo','equipment_inventories.image' , 'equipment_inventories.mark', 'users.id as id_user', 'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_maintenances.id as ultimo_mant',
                            'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance')
                            ->where($campo_grupo, $var_grupo, $signo_grupo )
                            ->where($campo_user, $var_user, $signo_user)
                            ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
                            ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')
                            ->groupBy('equipment_inventories.id')
                            ->paginate(335);

                    $numero_equipos = count($funcionarios);
                    $data = array();

                    for ($i=0; $i < count($funcionarios); $i++) { 
                        $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                        $data[$i]['marca'] = $funcionarios[$i]->mark;
                        $data[$i]['id_user'] = $funcionarios[$i]->id_user;
                        $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
                        $data[$i]['marca'] = $funcionarios[$i]->mark;
                        $data[$i]['tipo'] = $funcionarios[$i]->type;
                        $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
                        $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;

                        if ($funcionarios[$i]->image != "") {
                            $data[$i]['img_equipo'] = $funcionarios[$i]->image;
                        }else{
                            $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
                        }

                        if ($funcionarios[$i]->name != "") {
                            $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                            $data[$i]['label_user'] = "label-success";
                            $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                            $data[$i]['disabled'] = 'false';

                        }else{
                            $data[$i]['user'] = "Sin Asignar";
                            $data[$i]['label_user'] = "label-danger";
                            $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                            $data[$i]['disabled'] = 'disabled';
                        }
                        if ($funcionarios[$i]->name_group != "") {
                            $data[$i]['grupo'] = $funcionarios[$i]->name_group;
                        }else{
                            $data[$i]['grupo'] = "Sin Grupo";
                        }

                        if ($funcionarios[$i]->type_maintenance != "") {
                            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                            $data[$i]['row'] = 'green';
                        }else{
                            $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                            $data[$i]['row'] = "red";
                        }
                       
                    }
                    $vista = View::make('dashboard.equipos.informes.informe_equipos')
                    // ->with('administradores', $funcionarios2)
                    // ->with('marcas', $marcas)
                    // ->with('applicants', $applicants)
                    // ->with('numero_equipos', $numero_equipos)
                    ->with('data', $data)
                    // ->with('grupos', $grupos)
                    // ->with('tipo_equipos', $tipo_equipos)
                    ->with('pag', $funcionarios);
                    PDF::load($vista, 'A4', 'portrait')->show();
                break;
            case '3':
                    if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
                        $campo_user = 'equipment_inventories_users.users_id';
                        $var_user = '=';
                        $signo_user = $applicant;
                    } else {
                        $campo_user = 'equipment_inventories.id';
                        $var_user = '<>';
                        $signo_user = '-1';
                    }

                    if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
                        $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
                        $var_grupo = '=';
                        $signo_grupo = $grupo;
                    } else {
                        $campo_grupo = 'equipment_inventories.id';
                        $var_grupo = '<>';
                        $signo_grupo = '-2';
                    }

                    if (isset($_GET['mantenimiento']) && !empty($_GET['mantenimiento'])) {
                        $campo_mant = 'equipment_type_maintenances.id';
                        $var_mant = '=';
                        $signo_mant = $mantenimiento;

                        $datos = DB::table('equipment_maintenance_schedules')
                        ->select('equipment_groups_id')
                        ->where('equipment_type_maintenances_id', $mantenimiento)
                        ->get();

                        
                        $i =0;
                        $grupos_array = array();
                        foreach ($datos as $key) {
                            $grupos_array= $key->equipment_groups_id;
                            $i++;
                        }
                        $campos_grupos_mant ='equipment_groups_has_equipment_inventories.equipment_groups_ids';
                        $campos_var_mant = '=';
                        $grupos_where = "whereBetween";
                        // ->whereBetween('id', array('1', '4'))

                    } else {
                        $grupos_array = '-4';
                        $campo_mant = 'equipment_inventories.id';
                        $var_mant = '<>';
                        $signo_mant = '-3';
                        $campos_grupos_mant ='equipment_inventories.id';

                        $grupos_where = "where";
                        $grupos_where2 = "'equipment_inventories.id','<>',  $grupos_array";
                        $campos_var_mant = '<>';
                    }

                    $funcionarios = DB::table("equipment_maintenances")
                            // ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                            // ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipment_inventories_id', '=', 'equipment_inventories.id') 
                            ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                            ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
                            ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                            // ->leftJoin('equipment_maintenances_details', 'equipment_maintenances_details.equipment_inventories_id', '=', 'equipment_inventories.id') 
                            // ->leftJoin('equipment_item_maintenances', 'equipment_item_maintenances.id', '=', 'equipment_maintenances_details.equipment_item_maintenances_id')
                            ->leftJoin('equipment_type_maintenances', 'equipment_maintenances.equipment_type_maintenances_id', '=', 'equipment_type_maintenances.id')
                            ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('users as user_mant', 'equipment_maintenances.users_id', '=', 'user_mant.id') 
                            ->leftJoin('equipment_groups', 'equipment_groups_has_equipment_inventories.equipment_groups_id', '=', 'equipment_groups.id')
                            ->join('equipment_types', 'equipment_inventories.equipment_types_id', '=', 'equipment_types.id') 
                            ->select('equipment_inventories.id as id_equipo','equipment_inventories.image' , 'equipment_inventories.mark', 'users.id as id_user',
                            'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_maintenances.equipment_maintenances_statuses_id as estado_mant',
                            'equipment_maintenances.detalle as desc_mant',
                            'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance',
                            'user_mant.name as nom_mant', 'user_mant.last_name as ap_mant', 'user_mant.img_min as img_mant')
                            ->where('equipment_maintenances.equipment_maintenances_statuses_id', 2)
                            ->groupBy('equipment_maintenances.id')
                            ->paginate(1000);
                            $data = array();

                            for ($i=0; $i < count($funcionarios); $i++) { 
                                $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                                $data[$i]['marca'] = $funcionarios[$i]->mark;
                                $data[$i]['id_user'] = $funcionarios[$i]->id_user;
                                $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
                                $data[$i]['marca'] = $funcionarios[$i]->mark;
                                $data[$i]['tipo'] = $funcionarios[$i]->type;
                                $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
                                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;

                                if ($funcionarios[$i]->image != "") {
                                    $data[$i]['img_equipo'] = $funcionarios[$i]->image;
                                }else{
                                    $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
                                }

                                if ($funcionarios[$i]->name != "") {
                                    $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                                    $data[$i]['label_user'] = "label-success";
                                    $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                                    $data[$i]['disabled'] = 'false';

                                }else{
                                    $data[$i]['user'] = "Sin Asignar";
                                    $data[$i]['label_user'] = "label-danger";
                                    $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                                    $data[$i]['disabled'] = 'disabled';
                                }
                                if ($funcionarios[$i]->name_group != "") {
                                    $data[$i]['grupo'] = $funcionarios[$i]->name_group;
                                }else{
                                    $data[$i]['grupo'] = "Sin Grupo";
                                }

                                if ($funcionarios[$i]->type_maintenance != "") {
                                    $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                                    
                                    if ($funcionarios[$i]->estado_mant == 1 ) {
                                        $data[$i]['row'] = 'red';
                                        $data[$i]['tecnico'] = "N/A";
                                        $data[$i]['img_mant'] = "assets/img/avatar_small/default.jpg";
                                        $data[$i]['style_mant'] = "display:none";
                                    }else{
                                        $data[$i]['row'] = 'green';
                                        $data[$i]['img_mant'] = $funcionarios[$i]->img_mant;
                                        $data[$i]['style_mant'] = "";
                                        $data[$i]['tecnico'] = $funcionarios[$i]->nom_mant." ".$funcionarios[$i]->ap_mant;
                                    }
                                    $data[$i]['fecha_mant'] = $funcionarios[$i]->ultimo_mant;
                                    $data[$i]['descripcion'] = $funcionarios[$i]->desc_mant;

                                    

                                    
                                }else{
                                    $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                                    $data[$i]['row'] = "red";
                                    $data[$i]['fecha_mant'] = 'N/A';
                                    $data[$i]['tecnico'] = 'N/A';
                                    $data[$i]['descripcion'] = 'N/A';
                                    $data[$i]['img_mant'] = 'N/A';
                                    $data[$i]['style_mant'] = "display:none";
                                }
                               
                            }

                            $vista = View::make('dashboard.equipos.informes.informe_mantenimientos_realizados')
                            ->with('data', $data)
                            ->with('pag', $funcionarios);
                            PDF::load($vista, 'A4', 'portrait')->show();
                break;
            case '4':
                    if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
                        $campo_user = 'equipment_inventories_users.users_id';
                        $var_user = '=';
                        $signo_user = $applicant;
                    } else {
                        $campo_user = 'equipment_inventories.id';
                        $var_user = '<>';
                        $signo_user = '-1';
                    }

                    if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
                        $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
                        $var_grupo = '=';
                        $signo_grupo = $grupo;
                    } else {
                        $campo_grupo = 'equipment_inventories.id';
                        $var_grupo = '<>';
                        $signo_grupo = '-2';
                    }

                    if (isset($_GET['mantenimiento']) && !empty($_GET['mantenimiento'])) {
                        $campo_mant = 'equipment_type_maintenances.id';
                        $var_mant = '=';
                        $signo_mant = $mantenimiento;

                        $datos = DB::table('equipment_maintenance_schedules')
                        ->select('equipment_groups_id')
                        ->where('equipment_type_maintenances_id', $mantenimiento)
                        ->get();

                        
                        $i =0;
                        $grupos_array = array();
                        foreach ($datos as $key) {
                            $grupos_array= $key->equipment_groups_id;
                            $i++;
                        }
                        $campos_grupos_mant ='equipment_groups_has_equipment_inventories.equipment_groups_ids';
                        $campos_var_mant = '=';
                        $grupos_where = "whereBetween";
                        // ->whereBetween('id', array('1', '4'))

                    } else {
                        $grupos_array = '-4';
                        $campo_mant = 'equipment_inventories.id';
                        $var_mant = '<>';
                        $signo_mant = '-3';
                        $campos_grupos_mant ='equipment_inventories.id';

                        $grupos_where = "where";
                        $grupos_where2 = "'equipment_inventories.id','<>',  $grupos_array";
                        $campos_var_mant = '<>';
                    }

                    $funcionarios = DB::table("equipment_maintenances")
                            // ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                            // ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipment_inventories_id', '=', 'equipment_inventories.id') 
                            ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipment_maintenances.equipment_inventories_id')
                            ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
                            ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                            // ->leftJoin('equipment_maintenances_details', 'equipment_maintenances_details.equipment_inventories_id', '=', 'equipment_inventories.id') 
                            // ->leftJoin('equipment_item_maintenances', 'equipment_item_maintenances.id', '=', 'equipment_maintenances_details.equipment_item_maintenances_id')
                            ->leftJoin('equipment_type_maintenances', 'equipment_maintenances.equipment_type_maintenances_id', '=', 'equipment_type_maintenances.id')
                            ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('users as user_mant', 'equipment_maintenances.users_id', '=', 'user_mant.id') 
                            ->leftJoin('equipment_groups', 'equipment_groups_has_equipment_inventories.equipment_groups_id', '=', 'equipment_groups.id')
                            ->join('equipment_types', 'equipment_inventories.equipment_types_id', '=', 'equipment_types.id') 
                            ->select('equipment_inventories.id as id_equipo','equipment_inventories.image' , 'equipment_inventories.mark', 'users.id as id_user',
                            'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_maintenances.equipment_maintenances_statuses_id as estado_mant',
                            'equipment_maintenances.detalle as desc_mant',
                            'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance',
                            'user_mant.name as nom_mant', 'user_mant.last_name as ap_mant', 'user_mant.img_min as img_mant')
                            ->where('equipment_maintenances.equipment_maintenances_statuses_id', 1)

                            ->groupBy('equipment_maintenances.id')
                            ->paginate(1000);
                            $data = array();

                            for ($i=0; $i < count($funcionarios); $i++) { 
                                $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                                $data[$i]['marca'] = $funcionarios[$i]->mark;
                                $data[$i]['id_user'] = $funcionarios[$i]->id_user;
                                $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
                                $data[$i]['marca'] = $funcionarios[$i]->mark;
                                $data[$i]['tipo'] = $funcionarios[$i]->type;
                                $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
                                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;

                                if ($funcionarios[$i]->image != "") {
                                    $data[$i]['img_equipo'] = $funcionarios[$i]->image;
                                }else{
                                    $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
                                }

                                if ($funcionarios[$i]->name != "") {
                                    $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
                                    $data[$i]['label_user'] = "label-success";
                                    $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                                    $data[$i]['disabled'] = 'false';

                                }else{
                                    $data[$i]['user'] = "Sin Asignar";
                                    $data[$i]['label_user'] = "label-danger";
                                    $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                                    $data[$i]['disabled'] = 'disabled';
                                }
                                if ($funcionarios[$i]->name_group != "") {
                                    $data[$i]['grupo'] = $funcionarios[$i]->name_group;
                                }else{
                                    $data[$i]['grupo'] = "Sin Grupo";
                                }

                                if ($funcionarios[$i]->type_maintenance != "") {
                                    $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                                    
                                    if ($funcionarios[$i]->estado_mant == 1 ) {
                                        $data[$i]['row'] = 'red';
                                        $data[$i]['tecnico'] = "N/A";
                                        $data[$i]['img_mant'] = "assets/img/avatar_small/default.jpg";
                                        $data[$i]['style_mant'] = "display:none";
                                    }else{
                                        $data[$i]['row'] = 'green';
                                        $data[$i]['img_mant'] = $funcionarios[$i]->img_mant;
                                        $data[$i]['style_mant'] = "";
                                        $data[$i]['tecnico'] = $funcionarios[$i]->nom_mant." ".$funcionarios[$i]->ap_mant;
                                    }
                                    $data[$i]['fecha_mant'] = $funcionarios[$i]->ultimo_mant;
                                    $data[$i]['descripcion'] = $funcionarios[$i]->desc_mant;

                                    

                                    
                                }else{
                                    $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                                    $data[$i]['row'] = "red";
                                    $data[$i]['fecha_mant'] = 'N/A';
                                    $data[$i]['tecnico'] = 'N/A';
                                    $data[$i]['descripcion'] = 'N/A';
                                    $data[$i]['img_mant'] = 'N/A';
                                    $data[$i]['style_mant'] = "display:none";
                                }
                               
                            }

                            $vista = View::make('dashboard.equipos.informes.informe_mantenimientos_pendientes')
                            ->with('data', $data)
                            ->with('pag', $funcionarios);
                            PDF::load($vista, 'A4', 'portrait')->show();
                break;
                
                case "5":
                    
                    $sql = "SELECT*FROM equipment_licensings";

                    $datos = DB::select($sql);
                    
                    // $sql = "SELECT users.name, users.last_name, equipment_inventories.observation, equipment_key_licenses.equipment_licensings_id  FROM equipment_inventories_equipment_key_licenses
                    //         LEFT JOIN equipment_key_licenses ON (equipment_key_licenses.id = equipment_inventories_equipment_key_licenses.equipment_key_licenses_id)
                    //         LEFT JOIN equipment_inventories ON (equipment_inventories_equipment_key_licenses.equipment_inventories_id = equipment_inventories.id)
                    //         LEFT JOIN equipments_has_equipment_inventories ON (equipments_has_equipment_inventories.equipment_inventories_id = equipment_inventories.id)
                    //         LEFT JOIN equipments ON (equipments_has_equipment_inventories.equipments_id = equipments.id)
                    //         LEFT JOIN equipment_inventories_users ON (equipment_inventories_users.equipments_id = equipments.id)
                    //         LEFT JOIN users ON (equipment_inventories_users.users_id = users.id)
                    //         where equipment_inventories_users.status = 1
                    //         where equipment_inventories_equipment_key_licenses.status = 1
                    //             ";



                    $asignaciones   = DB::table('equipment_inventories_equipment_key_licenses')
                            ->leftJoin('equipment_key_licenses', 'equipment_key_licenses.id', '=', 'equipment_inventories_equipment_key_licenses.equipment_key_licenses_id')
                            ->leftJoin('equipment_inventories', 'equipment_inventories_equipment_key_licenses.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
                            ->leftJoin('equipments', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
                            ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id')
                            ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id')
                            ->where('equipment_inventories_users.status', '=', '1')
                            ->where('equipment_inventories_equipment_key_licenses.status', '=', '1')
                            ->select('users.name', 'users.last_name', 'equipment_inventories.observation', 'equipment_key_licenses.equipment_licensings_id')
                            ->get();
                                

                    // $asignaciones = DB::select($sql);

                    $html= View::make('dashboard.equipos.informes.informe_licencias')
                                ->with('datos', $datos)
                                ->with('asignaciones', $asignaciones);
                                
                        PDF::load($html, 'A4', 'portrait')->show();

            break;
             
            default:
                 
                break;
         } 
                     
    }
    public function getGenerarinformeequipo2(){
    
        $user = Users::find(Auth::user()->id);

        //funciones para filtros
        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        $grupo = Input::get('grupo');
        $tipo_equipo = Input::get('tipo_equipo');

        // $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'equipment_inventories_users.users_id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'equipment_inventories.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
            $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
            $var_grupo = '=';
            $signo_grupo = $grupo;
        } else {
            $campo_grupo = 'equipment_inventories.id';
            $var_grupo = '<>';
            $signo_grupo = '-2';
        }

        $funcionarios = DB::table('equipments')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
        ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
        ->leftJoin('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
        ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
        ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
        ->leftJoin('equipment_offices', 'equipment_offices.id', '=', 'equipment_inventories.equipment_offices_id')
        ->select('users.id as id_user','users.name', 'users.last_name', 'users.img_min', 'equipments.id as id_equipo', 'equipment_inventories.mark',
            'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_inventories.id as id_elemento',
            'equipment_type_maintenances.type_maintenance', 'equipment_inventories.image','equipment_inventories.observation AS name_equipo', 'equipment_groups.name_group',
            'equipment_type_maintenances.type_maintenance', 'equipment_offices.sede')
        ->where($campo_grupo, $var_grupo, $signo_grupo )
        ->where($campo_user, $var_user, $signo_user)
        // ->where($campo_mant, $var_mant, $signo_mant)
        ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')

        ->where('equipment_inventories.equipment_types_id', '<>', 8)
        ->where('equipment_inventories.equipment_types_id', '<>', 9)
        ->where('equipment_inventories_users.status', 1)
        ->orderBy('equipments.id', 'asc')
        ->get();

        $numero_equipos = count($funcionarios);
        $data = array();

        for ($i=0; $i < count($funcionarios); $i++) { 
            $data[$i]['user'] = $funcionarios[$i]->name." ".$funcionarios[$i]->last_name;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['id_user'] = $funcionarios[$i]->id_user;
            $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['tipo'] = $funcionarios[$i]->type;
            $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
            $data[$i]['name_equipo'] = $funcionarios[$i]->name_equipo;
            $data[$i]['sede'] = $funcionarios[$i]->sede;

            if ($funcionarios[$i]->image != "") {
                $data[$i]['img_equipo'] = $funcionarios[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }

            if ($funcionarios[$i]->name != "") {
                $data[$i]['user'] = utf8_decode($funcionarios[$i]->name." ".$funcionarios[$i]->last_name);
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                $data[$i]['disabled'] = 'false';

            }else{
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            if ($funcionarios[$i]->name_group != "") {
                $data[$i]['grupo'] = $funcionarios[$i]->name_group;
            }else{
                $data[$i]['grupo'] = "Sin Grupo";
            }

            if ($funcionarios[$i]->type_maintenance != "") {
                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                $data[$i]['row'] = 'green';
            }else{
                $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                $data[$i]['row'] = "red";
            }
           
        }

        include('assets/fpdf17/fpdf.php');

        $pdf = new FPDF();
        $pdf->AliasNbPages();
        


        

            
           $pdf->AddPage();
           $contador=0;
           // La variable contador lleva el control de las iteraciones 

           $pdf->Image('assets/img/logo2(2).png',5,8,33);
           $pdf->SetFont('Arial','B',15);
           $pdf->SetY(15);
           $pdf->SetX(50);
           $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
           $pdf->SetFont('Arial','B',14);
           $pdf->SetY(21);
           $pdf->SetX(50);
           $pdf->Cell(130,5,'LISTADO DE INVENTARIO',0,0,'C');

           // Se imprime el aula y sede
           $pdf->SetY(40);
           $pdf->SetX(8);
           $pdf->SetFont('Arial','',12);
           $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
           $pdf->SetX(8);
           $pdf->SetY(44);
           // $pdf->Cell(100,5,'Aula: 1',0,'L');
           $pdf->SetX(8);
           $pdf->SetY(48);
           $fecha=date('yy-mm-dd');
           $parte = explode("-",$fecha);
           $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
           // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

           $Y_Fields_Name_position = 55;
           $Y_Table_Position = 61;
           //////////////////////////////////////////////////////////////193
           //////////////////////////////////////////////////////////////190
           // Genero el encabezado de la tabla.
           $pdf->SetFillColor(232,232,232);
           $pdf->SetFont('Arial','B',8);
           $pdf->SetY($Y_Fields_Name_position);
           $pdf->SetX(10);
           $pdf->Cell(8,6,'ID.',1,0,'C',1);
           $pdf->SetX(18);
           $pdf->Cell(30,6,'TIPO',1,0,'C',1);
           $pdf->SetX(48);
           $pdf->Cell(50,6,' NOMBRE ',1,0,'C',1);
           $pdf->SetX(98);
           $pdf->Cell(30,6,' RESPONSABLE ',1,0,'C',1);
           $pdf->SetX(128);
           $pdf->Cell(30,6,'UBICACION',1,0,'C',1);
           $pdf->SetX(158);
           $pdf->Cell(30,6,'F. verif',1,0,'C',1);

           $pdf->SetFont('Arial','',8);
           $pdf->SetY($Y_Table_Position);
           
           foreach ($data as $key) {
               
               $pdf->Cell(8,6,  $key['id_equipo'] ,1);
               $pdf->Cell(30,6, $key['tipo'] ,1,'L');
               $pdf->Cell(50,6, $key['name_equipo'] ,1,'R');
               $pdf->Cell(30,6, $key['user'] ,1,'R');
               $pdf->Cell(30,6, $key['sede'] ,1,'R');
               $pdf->Cell(30,6, "" ,1,'R');

               $pdf->Ln();
           }


          
           

          


           $pdf->Output();

        // $pdf=new FPDF();
        // $pdf->AddPage();
        // $pdf->SetFont('Arial','B',16);
        // $pdf->Cell(40,10,'¡Mi primera página pdf con FPDF!');
        // $pdf->Output();
        
    }
    public function postIngresarnuevoequipo(){
        $var = 0;

        if (Input::file('equipo_imagen')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/equipos/';

            $form_equipo_marca       = Input::get('form_equipo_marca');
            $form_equipo_modelo      = Input::get('form_equipo_modelo');
            $form_equipo_serial      = Input::get('form_equipo_serial');
            $form_equipo_ip          = Input::get('form_equipo_ip');
            $form_equipo_mac         = Input::get('form_equipo_mac');
            $form_equipo_memoria_ram = Input::get('form_equipo_memoria_ram');
            $form_equipo_disco_duro  = Input::get('form_equipo_disco_duro');
            $form_equipo_procesador  = Input::get('form_equipo_procesador');
            $form_equipo_so          = Input::get('form_equipo_so');
            $form_equipo_factura     = Input::get('form_equipo_factura');
            $form_equipo_asignacion  = Input::get('form_equipo_asignacion');
            $form_equipo_compra      = Input::get('form_equipo_compra');
            $form_equipo_ubicacion   = Input::get('form_equipo_ubicacion');
            $form_equipo_imagen      = Input::get('form_equipo_imagen');
    
            $teclado_marca           = Input::get('teclado_marca');
            $teclado_factura         = Input::get('teclado_factura');
            $teclado_asignacion      = Input::get('teclado_asignacion');
            $teclado_compra          = Input::get('teclado_compra');
            $teclado_ubicacion       = Input::get('teclado_ubicacion');

            $mouse_marca             = Input::get('mouse_marca');
            $mouse_factura           = Input::get('mouse_factura');
            $mouse_ubicacion         = Input::get('mouse_ubicacion');
            $mouse_asignacion        = Input::get('monitor_asignacion');
            $mouse_compra            = Input::get('monitor_compra');

            $user_asign              = Input::get('usuario_id');
            $tipo_equipo             = Input::get('tipo_equipo'); 

            $tipo = $tipo_equipo;

            if($tipo_equipo == 1) {
                $tipo = 7;
            }
            

            $filename = $form_equipo_serial."_".Input::file('equipo_imagen')->getClientOriginalName();

            if ($user_asign == "") {
                $estado_equipo = 2;
                
            }else{
                $estado_equipo = 1;
            }

            if (Input::get('form_equipo_marca')) {
                
                $uploadSuccess = Input::file('equipo_imagen')->move($destinationPath, $filename);

                if ($uploadSuccess) {

                    $nuevo_equipo = new EquipmentInventories;

                    $nuevo_equipo->mark                     = $form_equipo_marca;
                    $nuevo_equipo->model                    = $form_equipo_modelo;
                    $nuevo_equipo->serial                   = $form_equipo_serial;
                    $nuevo_equipo->ip                       = $form_equipo_ip;
                    $nuevo_equipo->mac                      = $form_equipo_mac;
                    $nuevo_equipo->memory                   = $form_equipo_memoria_ram;
                    $nuevo_equipo->hard_drive               = $form_equipo_disco_duro;
                    $nuevo_equipo->processor                = $form_equipo_procesador;
                    $nuevo_equipo->operating_system         = $form_equipo_so;
                    $nuevo_equipo->invoice                  = $form_equipo_factura;
                    $nuevo_equipo->date_asign               = $form_equipo_asignacion;
                    $nuevo_equipo->equipment_offices_id     = $form_equipo_ubicacion;
                    $nuevo_equipo->date_shopping            = $form_equipo_compra;
                    $nuevo_equipo->equipment_statuses_id    = $estado_equipo;
                    $nuevo_equipo->image                    = $destinationPath.$filename;
                    $nuevo_equipo->equipment_types_id       = $tipo;
                    
                     
                    $nuevo_equipo->save();

                    if ($tipo == 1 || $tipo == 2 || $tipo == 5 || $tipo == 7 ) {

                        DB::table('equipments')->insert(array(
                            array('status' => 1),
                                       
                        ));
                        
                        $equipo = Equipments::all(); 
                        $bandera = $equipo->last()->id;

                        $elemento = EquipmentInventories::all();
                        $bandera2 = $elemento->last()->id;

                        DB::table('equipments_has_equipment_inventories')->insert(array(
                            array('equipments_id' => $bandera, 'equipment_inventories_id' => $bandera2, 'status' => "1"),
                                
                        ));

                    }
                    if ($user_asign != "") {
                        DB::table('equipment_inventories_users')->insert(array(
                            array('equipments_id' => $bandera, 'users_id' => $user_asign, 'status' => "1"),
                                
                        ));
                    }



                    

                    
                    $var = 2;
                }
            }
            if (Input::get('teclado_marca')) {
                
                $nuevo_equipo = new EquipmentInventories;

                $nuevo_equipo->mark                     = $teclado_marca;
                $nuevo_equipo->invoice                  = $teclado_factura;
                $nuevo_equipo->date_asign               = $teclado_asignacion;
                $nuevo_equipo->equipment_offices_id     = $teclado_ubicacion;
                $nuevo_equipo->date_shopping            = $teclado_compra;
                $nuevo_equipo->equipment_statuses_id    = $estado_equipo;
                $nuevo_equipo->equipment_types_id       = 8;
                    
                $nuevo_equipo->save();


                if ($tipo == 1 || $tipo == 2 || $tipo == 5 || $tipo == 7) {

                    // DB::table('equipments')->insert(array(
                    //     array('status' => 1),
                                   
                    // ));
                    
                    $equipo = Equipments::all(); 
                    $bandera = $equipo->last()->id;

                    $elemento = EquipmentInventories::all();
                    $bandera2 = $elemento->last()->id;

                    DB::table('equipments_has_equipment_inventories')->insert(array(
                        array('equipments_id' => $bandera, 'equipment_inventories_id' => $bandera2, 'status' => "1"),
                            
                    ));

                    // DB::table('equipment_inventories_users')->insert(array(
                    //     array('equipments_id' => $bandera, 'users_id' => $user_asign, 'status' => "1"),
                            
                    // ));
                }

                $var = 2;
            }
            if (Input::get('mouse_marca')) {
                
                $nuevo_equipo = new EquipmentInventories;

                $nuevo_equipo->mark                     = $mouse_marca;
                $nuevo_equipo->invoice                  = $mouse_factura;
                $nuevo_equipo->date_asign               = $mouse_asignacion;
                $nuevo_equipo->equipment_offices_id     = $mouse_ubicacion;
                $nuevo_equipo->date_shopping            = $mouse_compra;
                $nuevo_equipo->equipment_statuses_id    = $estado_equipo;
                $nuevo_equipo->equipment_types_id       = 9;
                    
                $nuevo_equipo->save();
                
                $var = 2;

                if ($tipo == 1 || $tipo == 2 || $tipo == 5 || $tipo == 7) {

                    // DB::table('equipments')->insert(array(
                    //     array('status' => 1),
                                   
                    // ));
                    
                    $equipo = Equipments::all(); 
                    $bandera = $equipo->last()->id;

                    $elemento = EquipmentInventories::all();
                    $bandera2 = $elemento->last()->id;

                    DB::table('equipments_has_equipment_inventories')->insert(array(
                        array('equipments_id' => $bandera, 'equipment_inventories_id' => $bandera2, 'status' => "1"),
                            
                    ));

                    // DB::table('equipment_inventories_users')->insert(array(
                    //     array('equipments_id' => $bandera, 'users_id' => $user_asign, 'status' => "1"),
                            
                    // ));
                }
            }

            echo "1";

        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function postIngresarnuevomonitor(){
        $var = 0;

        if (Input::file('monitor_imagen')) {

            $my_id = Auth::user()->id;

            
            $destinationPath = 'assets/img/equipos/';

            $monitor_marca = Input::get('monitor_marca');
            $monitor_modelo = Input::get('monitor_modelo');
            $monitor_serial = Input::get('monitor_serial');
            $monitor_factura = Input::get('monitor_factura');
            $monitor_asignacion = Input::get('monitor_asignacion');
            $monitor_compra = Input::get('monitor_compra');
            $monitor_ubicacion = Input::get('monitor_ubicacion');
            
            

            $user_asign              = Input::get('usuario_id');
            $tipo_equipo             = Input::get('tipo_equipo'); 


            $filename = $monitor_serial."_".Input::file('monitor_imagen')->getClientOriginalName();
            if ($user_asign == "") {
                $estado_equipo = 2;
            }else{
                $estado_equipo = 1;
            }

            if (Input::get('monitor_marca')) {
                
                $uploadSuccess = Input::file('monitor_imagen')->move($destinationPath, $filename);

                if ($uploadSuccess) {

                    $nuevo_equipo = new EquipmentInventories;

                    $nuevo_equipo->mark                     = $monitor_marca;
                    $nuevo_equipo->model                    = $monitor_modelo;
                    $nuevo_equipo->serial                   = $monitor_serial;
                    
                    $nuevo_equipo->invoice                  = $monitor_factura;
                    $nuevo_equipo->date_asign               = $monitor_asignacion;
                    $nuevo_equipo->date_shopping            = $monitor_compra;
                    $nuevo_equipo->equipment_offices_id     = $monitor_ubicacion;
                    $nuevo_equipo->equipment_statuses_id    = $estado_equipo;
                    $nuevo_equipo->image                    = $destinationPath.$filename;
                    $nuevo_equipo->equipment_types_id       = 6;
                    
                     
                    $nuevo_equipo->save();

                    if ($estado_equipo == "1") {

                        $equipo = Equipments::all(); 
                        $bandera = $equipo->last()->id;

                        $elemento = EquipmentInventories::all();
                        $bandera2 = $elemento->last()->id;

                        DB::table('equipments_has_equipment_inventories')->insert(array(
                            array('equipments_id' => $bandera, 'equipment_inventories_id' => $bandera2, 'status' => "1"),
                                
                        ));
                    }

                    
                    $var = 2;
                }
            }
            echo "1";

        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function getPdflistaequipos(){
                
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        $cont = 0;
        
        $user = Users::find(Auth::user()->id);

        //funciones para filtros
        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        $grupo = Input::get('grupo');
        $tipo_equipo = Input::get('tipo_equipo');

        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'equipment_inventories_users.users_id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'equipment_inventories.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
            $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
            $var_grupo = '=';
            $signo_grupo = $grupo;
        } else {
            $campo_grupo = 'equipment_inventories.id';
            $var_grupo = '<>';
            $signo_grupo = '-2';
        }

        $tipo_equipos = DB::table('equipment_types')->distinct()->get();

        $applicants = DB::table('users')->get();
        
        $marcas  = DB::table('equipment_inventories')->distinct()->select('mark')->get();
        

        if (Input::get('order_tipo') == 0) {
            $orden = 'equipment_types.type';
            $tipo_orden = 'ASC';
        }else{
            $orden = 'equipment_types.type';
            $tipo_orden = 'DESC';
        }


        $funcionarios = DB::table('equipments')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
        ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
        ->leftJoin('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
        ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
        ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
        ->leftJoin('equipment_offices', 'equipment_offices.id', '=', 'equipment_inventories.equipment_offices_id')
        ->select('users.id as id_user','users.name', 'users.last_name', 'users.img_min', 'equipments.id as id_equipo', 'equipment_inventories.mark',
            'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_inventories.id as id_elemento',
            'equipment_type_maintenances.type_maintenance', 'equipment_inventories.image', 'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance',
            'equipment_inventories.observation', 'equipment_offices.sede as ubicacion')
        ->where($campo_grupo, $var_grupo, $signo_grupo )
        ->where($campo_user, $var_user, $signo_user)
        // ->where($campo_mant, $var_mant, $signo_mant)
        ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')

        ->where('equipment_inventories.equipment_types_id', '<>', 4)
        ->where('equipment_inventories.equipment_types_id', '<>', 6)
        ->where('equipment_inventories.equipment_types_id', '<>', 8)
        ->where('equipment_inventories.equipment_types_id', '<>', 9)
        ->where('equipment_inventories_users.statusj', 1)
        ->orderBy($orden, $tipo_orden)
        ->distinct()
        ->paginate(500);

        $numero_equipos = count($funcionarios);
        $data = array();

        for ($i=0; $i < count($funcionarios); $i++) { 
            $data[$i]['user'] = utf8_encode($funcionarios[$i]->name." ".$funcionarios[$i]->last_name);
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['id_user'] = $funcionarios[$i]->id_user;
            $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['tipo'] = $funcionarios[$i]->type;
            $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
            $data[$i]['sede'] = $funcionarios[$i]->ubicacion;
            if($funcionarios[$i]->observation == ""){
                $data[$i]['name_equipo'] = 'N/A';
                
            }else{
                $data[$i]['name_equipo'] = $funcionarios[$i]->observation;
            }

            if ($funcionarios[$i]->image != "") {
                $data[$i]['img_equipo'] = $funcionarios[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }

            if ($funcionarios[$i]->name != "") {
                $data[$i]['user'] = utf8_decode($funcionarios[$i]->name." ".$funcionarios[$i]->last_name);
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                $data[$i]['disabled'] = 'false';

            }else{
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            if ($funcionarios[$i]->name_group != "") {
                $data[$i]['grupo'] = $funcionarios[$i]->name_group;
            }else{
                $data[$i]['grupo'] = "Sin Grupo";
            }

            if ($funcionarios[$i]->type_maintenance != "") {
                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                $data[$i]['row'] = 'green';
            }else{
                $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                $data[$i]['row'] = "red";
            }
           
        }
        // $vista = View::make('dashboard.equipos.informes.informe_equipos')
        // // ->with('administradores', $funcionarios2)
        // // ->with('marcas', $marcas)
        // // ->with('applicants', $applicants)
        // // ->with('numero_equipos', $numero_equipos)
        // ->with('data', $data)
        // // ->with('grupos', $grupos)
        // // ->with('tipo_equipos', $tipo_equipos)
        // ->with('pag', $funcionarios);
        // PDF::load($vista, 'A4', 'portrait')->show();
                
         
        
        include('assets/fpdf17/fpdf.php');

        $pdf = new FPDF();
        $pdf->AliasNbPages();
        


        

            
           $pdf->AddPage();
           $contador=0;
           // La variable contador lleva el control de las iteraciones 

           $pdf->Image('assets/img/logo2(2).png',5,8,33);
           $pdf->SetFont('Arial','B',15);
           $pdf->SetY(15);
           $pdf->SetX(50);
           $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
           $pdf->SetFont('Arial','B',14);
           $pdf->SetY(21);
           $pdf->SetX(50);
           $pdf->Cell(130,5,'LISTADO DE INVENTARIO',0,0,'C');

           // Se imprime el aula y sede
           $pdf->SetY(40);
           $pdf->SetX(8);
           $pdf->SetFont('Arial','',12);
           $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
           $pdf->SetX(8);
           $pdf->SetY(44);
           // $pdf->Cell(100,5,'Aula: 1',0,'L');
           $pdf->SetX(8);
           $pdf->SetY(48);
           $fecha=date('yy-mm-dd');
           $parte = explode("-",$fecha);
           $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
           // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

           $Y_Fields_Name_position = 55;
           $Y_Table_Position = 61;
           //////////////////////////////////////////////////////////////193
           //////////////////////////////////////////////////////////////190
           // Genero el encabezado de la tabla.


           $pdf->SetFillColor(232,232,232);
           $pdf->SetFont('Arial','B',8);
           $pdf->SetY($Y_Fields_Name_position);
           $pdf->SetX(10);
           $pdf->Cell(8,6,'ID.',1,0,'C',1);
           $pdf->SetX(18);
           $pdf->Cell(30,6,'TIPO',1,0,'C',1);
           $pdf->SetX(48);
           $pdf->Cell(50,6,' NOMBRE ',1,0,'C',1);
           $pdf->SetX(98);
           $pdf->Cell(30,6,' RESPONSABLE ',1,0,'C',1);
           $pdf->SetX(128);
           $pdf->Cell(30,6,'UBICACION',1,0,'C',1);
           $pdf->SetX(158);
           $pdf->Cell(30,6,'F. verif',1,0,'C',1);

           $pdf->SetFont('Arial','',8);
           $pdf->SetY($Y_Table_Position);
           
           foreach ($data as $key) {
               
               $pdf->Cell(8,6,  $key['id_equipo'] ,1);
               $pdf->Cell(30,6, $key['tipo'] ,1,'L');
               $pdf->Cell(50,6, $key['name_equipo'] ,1,'R');
               $pdf->Cell(30,6, $key['user'] ,1,'R');
               $pdf->Cell(30,6, $key['sede'] ,1,'R');
               $pdf->Cell(30,6, "" ,1,'R');

               $pdf->Ln();
           }


          
           

          


           $pdf->Output();

        
        


            // $vista = View::make('dashboard.equipos.informes.informe_equipos')
            // // ->with('administradores', $funcionarios2)
            // // ->with('marcas', $marcas)
            // // ->with('applicants', $applicants)
            // // ->with('numero_equipos', $numero_equipos)
            // ->with('data', $data)
            // // ->with('grupos', $grupos)
            // // ->with('tipo_equipos', $tipo_equipos)
            // ->with('pag', $funcionarios);
            // PDF::load($vista, 'A4', 'portrait')->show();
    }
    public function getPdflistaqrs(){
                
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        $cont = 0;
        
        $user = Users::find(Auth::user()->id);

        //funciones para filtros
        $f_compra = Input::get('f_compra');
        $serial = Input::get('serial');
        $codigo = Input::get('code');
        $marca = Input::get('marca');
        $applicant = Input::get('applicant');
        $grupo = Input::get('grupo');
        $tipo_equipo = Input::get('tipo_equipo');

        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'equipment_inventories_users.users_id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'equipment_inventories.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['grupo']) && !empty($_GET['grupo'])) {
            $campo_grupo = 'equipment_groups_has_equipment_inventories.equipment_groups_id';
            $var_grupo = '=';
            $signo_grupo = $grupo;
        } else {
            $campo_grupo = 'equipment_inventories.id';
            $var_grupo = '<>';
            $signo_grupo = '-2';
        }

        $tipo_equipos = DB::table('equipment_types')->distinct()->get();

        $applicants = DB::table('users')->get();
        
        $marcas  = DB::table('equipment_inventories')->distinct()->select('mark')->get();
        

        if (Input::get('order_tipo') == 0) {
            $orden = 'equipment_types.type';
            $tipo_orden = 'ASC';
        }else{
            $orden = 'equipment_types.type';
            $tipo_orden = 'DESC';
        }


        $funcionarios = DB::table('equipments')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipments_id', '=', 'equipments.id')
        ->leftJoin('equipment_inventories', 'equipment_inventories.id', '=', 'equipments_has_equipment_inventories.equipment_inventories_id')
        ->leftJoin('equipment_types', 'equipment_types.id', '=', 'equipment_inventories.equipment_types_id')
        ->leftJoin('equipment_inventories_users', 'equipment_inventories_users.equipments_id', '=', 'equipments.id' )
        ->leftJoin('users', 'users.id', '=', 'equipment_inventories_users.users_id') 
        ->leftJoin('equipment_groups_has_equipment_inventories', 'equipment_groups_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_groups', 'equipment_groups.id', '=', 'equipment_groups_has_equipment_inventories.equipment_groups_id')
        ->leftJoin('equipment_maintenances', 'equipment_maintenances.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipment_type_maintenances', 'equipment_type_maintenances.id', '=', 'equipment_maintenances.equipment_type_maintenances_id')
        ->leftJoin('equipment_offices', 'equipment_offices.id', '=', 'equipment_inventories.equipment_offices_id')
        ->select('users.id as id_user','users.name', 'users.last_name', 'users.img_min', 'equipments.id as id_equipo', 'equipment_inventories.mark',
            'equipment_types.type', 'equipment_maintenances.created_at as ultimo_mant','equipment_inventories.id as id_elemento',
            'equipment_type_maintenances.type_maintenance', 'equipment_inventories.image', 'equipment_groups.name_group', 'equipment_type_maintenances.type_maintenance',
            'equipment_inventories.observation', 'equipment_offices.sede as ubicacion')
        ->where($campo_grupo, $var_grupo, $signo_grupo )
        ->where($campo_user, $var_user, $signo_user)
        // ->where($campo_mant, $var_mant, $signo_mant)
        ->where('equipment_inventories.mark', 'LIKE', '%' . $marca . '%')
        ->where('equipment_inventories.equipment_types_id', 'LIKE', '%' . $tipo_equipo . '%')

        ->where('equipment_inventories.equipment_types_id', '<>', 8)
        ->where('equipment_inventories.equipment_types_id', '<>', 9)
        ->where('equipment_inventories_users.status', 1)
        ->orderBy($orden, $tipo_orden)
        ->paginate(500);

        $numero_equipos = count($funcionarios);
        $data = array();

        for ($i=0; $i < count($funcionarios); $i++) { 
            $data[$i]['user'] = utf8_encode($funcionarios[$i]->name." ".$funcionarios[$i]->last_name);
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['id_user'] = $funcionarios[$i]->id_user;
            $data[$i]['id_equipo'] = $funcionarios[$i]->id_equipo;
            $data[$i]['marca'] = $funcionarios[$i]->mark;
            $data[$i]['tipo'] = $funcionarios[$i]->type;
            $data[$i]['mantenimiento'] = $funcionarios[$i]->ultimo_mant;
            $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
            $data[$i]['sede'] = $funcionarios[$i]->ubicacion;
            if($funcionarios[$i]->observation == ""){
                $data[$i]['name_equipo'] = 'N/A';
                
            }else{
                $data[$i]['name_equipo'] = $funcionarios[$i]->observation;
            }

            if ($funcionarios[$i]->image != "") {
                $data[$i]['img_equipo'] = $funcionarios[$i]->image;
            }else{
                $data[$i]['img_equipo'] = "assets/img/equipos/-1.png";
            }

            if ($funcionarios[$i]->name != "") {
                $data[$i]['user'] = utf8_decode($funcionarios[$i]->name." ".$funcionarios[$i]->last_name);
                $data[$i]['label_user'] = "label-success";
                $data[$i]['img_user'] = $funcionarios[$i]->img_min;
                $data[$i]['disabled'] = 'false';

            }else{
                $data[$i]['user'] = "Sin Asignar";
                $data[$i]['label_user'] = "label-danger";
                $data[$i]['img_user'] = "assets/img/avatar_small/default.jpg";
                $data[$i]['disabled'] = 'disabled';
            }
            if ($funcionarios[$i]->name_group != "") {
                $data[$i]['grupo'] = $funcionarios[$i]->name_group;
            }else{
                $data[$i]['grupo'] = "Sin Grupo";
            }

            if ($funcionarios[$i]->type_maintenance != "") {
                $data[$i]['tipo_mant'] = $funcionarios[$i]->type_maintenance;
                $data[$i]['row'] = 'green';
            }else{
                $data[$i]['tipo_mant'] = "Sin Mantenimientos";
                $data[$i]['row'] = "red";
            }
           
        }
        // $vista = View::make('dashboard.equipos.informes.informe_equipos')
        // // ->with('administradores', $funcionarios2)
        // // ->with('marcas', $marcas)
        // // ->with('applicants', $applicants)
        // // ->with('numero_equipos', $numero_equipos)
        // ->with('data', $data)
        // // ->with('grupos', $grupos)
        // // ->with('tipo_equipos', $tipo_equipos)
        // ->with('pag', $funcionarios);
        // PDF::load($vista, 'A4', 'portrait')->show();
                
        
        
        include('assets/fpdf17/fpdf.php');

        $pdf = new FPDF();
        $pdf->AliasNbPages();
        


        

            
           $pdf->AddPage();
           $contador=0;
           // La variable contador lleva el control de las iteraciones 

           $pdf->Image('assets/img/logo2(2).png',5,8,33);
           $pdf->SetFont('Arial','B',15);
           $pdf->SetY(15);
           $pdf->SetX(50);
           $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
           $pdf->SetFont('Arial','B',14);
           $pdf->SetY(21);
           $pdf->SetX(50);
           $pdf->Cell(130,5,'LISTADO DE CODIGOS PARA INVENTARIO',0,0,'C');

           // Se imprime el aula y sede
           $pdf->SetY(40);
           $pdf->SetX(8);
           $pdf->SetFont('Arial','',12);
           $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
           $pdf->SetX(8);
           $pdf->SetY(44);
           // $pdf->Cell(100,5,'Aula: 1',0,'L');
           $pdf->SetX(8);
           $pdf->SetY(48);
           $fecha=date('yy-mm-dd');
           $parte = explode("-",$fecha);
           $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
           // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

           $Y_Fields_Name_position = 55;
           $Y_Table_Position = 61;
           //////////////////////////////////////////////////////////////193
           //////////////////////////////////////////////////////////////190
           // Genero el encabezado de la tabla.


           $pdf->SetFillColor(232,232,232);
           $pdf->SetFont('Arial','B',8);
           $pdf->SetY($Y_Fields_Name_position);
           $pdf->SetX(10);
           $pdf->Cell(8,6,'ID.',1,0,'C',1);
           $pdf->SetX(18);
           $pdf->Cell(70,6,'IMAGEN',1,0,'C',1);
           $pdf->SetX(90);
           $pdf->Cell(8,6,' ID ',1,0,'C',1);
           $pdf->SetX(98);
           $pdf->Cell(70,6,' IMAGEN ',1,0,'C',1);
           $pdf->SetX(128);
           // $pdf->Cell(30,6,'UBICACION',1,0,'C',1);
           // $pdf->SetX(158);
           // $pdf->Cell(30,6,'F. verif',1,0,'C',1);

           $pdf->SetFont('Arial','',8);
        
            $cont = 65;
            $num = 1;
            $num2 = 6;
           
           $pdf->SetY($Y_Table_Position);

           foreach ($data as $key) {

                $Base64Img = base64_encode(QrCode::format("png")->size(100)->generate(URL::to("soporte/equiposqr2?id=".$key['id_user']."&id_equipo=".$key['id_equipo']."")));
                $file = fopen("qrs/imagen_".$key['id_equipo'].".png", "w+");
                fwrite($file, base64_decode($Base64Img));



               $pdf->Cell(8,35,  $key['id_equipo'] ,1);
               $pdf->Cell(70,35,  "" ,1);
               $pdf->Image('assets/img/logo2(2).png',20, $cont+5);
               $pdf->Image('qrs/imagen_'.$key['id_equipo'].'.png',50, $cont);


               $pdf->SetX(90);
               $pdf->Cell(8,35,   $key['id_equipo'] +1,1);
               $pdf->Cell(70,35,  "" ,1);
               $pdf->Image('assets/img/logo2(2).png',100, $cont+5);
               $pdf->Image('qrs/imagen_'.$key['id_equipo'].'.png',130, $cont);



               $pdf->Ln();
               if ($num == $num2) {
                $cont = 15;
                $num = 1;
                $num2 = 7;
               }else{
                    $cont = $cont + 35;

               $num++;
               }

           }


          
           

          


           $pdf->Output();

        
        


            // $vista = View::make('dashboard.equipos.informes.informe_equipos')
            // // ->with('administradores', $funcionarios2)
            // // ->with('marcas', $marcas)
            // // ->with('applicants', $applicants)
            // // ->with('numero_equipos', $numero_equipos)
            // ->with('data', $data)
            // // ->with('grupos', $grupos)
            // // ->with('tipo_equipos', $tipo_equipos)
            // ->with('pag', $funcionarios);
            // PDF::load($vista, 'A4', 'portrait')->show();
    }
    
    public function getEquiposqr2(){
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'));
            

            $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_min', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->where('users.id', Input::get('id'))
                ->orderBy('last_name', 'asc')
                ->get();

            // $equipos = Users::find(Input::get('id'))->EquipmentInventories;

            $equipos = DB::table("equipment_inventories")
                    ->leftJoin('equipment_types','equipment_types.id', '=', 'equipment_inventories.equipment_types_id') 
                    ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                    ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                    ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                    ->select('equipment_inventories.id as id_equipo','equipment_inventories.equipment_types_id','equipment_inventories.model','equipment_inventories.observation'
                    ,'equipment_inventories.serial','equipment_inventories.image' , 'equipment_inventories.mark', 'equipment_inventories.date_asign'
                    ,'users.id as id_user'
                    ,'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_inventories.ip', 'equipment_inventories.mac'
                    ,'equipment_inventories.memory', 'equipment_inventories.hard_drive', 'equipment_inventories.processor'
                    ,'equipment_inventories.operating_system','equipments_has_equipment_inventories.equipments_id'
                    ,'equipment_inventories.license_of'
                    ,'equipment_inventories.version_op'
                    ,'equipment_inventories.no_license_op'
                    ,'equipment_inventories_users.id as id_asignacion', 'equipments_has_equipment_inventories.id as id_asignacion_equipo'
                    ,'equipments_has_equipment_inventories.status')
                    ->where('equipments_has_equipment_inventories.equipments_id',Input::get('id_equipo'))
                    ->groupBy('equipment_inventories.id')
                    ->paginate(15); 

            $fecha_actual = date('Y-m-d H:i:s');
            $data = array();
            $mantenimientos = EquipmentMaintenances::where('equipment_inventories_id', Input::get('id_equipo'))->get();
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.detalle_equipos_qr')
            ->with('users', $funcionarios)
            ->with('equipos', $equipos)
            ->with('data', $data)
            ->with('mantenimientos', $mantenimientos )
            ->with('submenu_activo', '')
            ->with('menu_activo', 'Equipos');

            

            exit();

        }
    }
    public function getEquiposqr() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $user = Users::find(Input::get('id'));

            $equipos = EquipmentInventories::where('id', Input::get('id'))->get();

            

            





            // return View::make('dashboard.equipos.detalle_equipos_inactivos')
            // ->with('equipos', $equipos);
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.equipos.detalle_equipos_qr')
            ->with('equipos', $equipos)
            ->with('submenu_activo', '')
            ->with('menu_activo', 'Equipos');

            

            exit();

        }
    }
    public function getAdminitems() {
        $tipo_equipos = DB::table('equipment_types')->get();
        $tipo_mantenimientos = DB::table('equipment_type_maintenances')->where('status', 1)->get();
        $item_mantenimientos = EquipmentItemMaintenances::orderBy('id', 'asc')->get();
        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();
        
        $funcionarios = DB::table('equipment_inventories')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
        ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name')
        // ->where('equipment_groups_has_equipment_inventories.equipment_groups_id', $id_grupo)
        ->get();

        $applicants = Users::all();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.adminitems')
        ->with('tipo_equipos', $tipo_equipos)
        ->with('tipo_mantenimientos', $tipo_mantenimientos)
        ->with('item_mantenimientos', $item_mantenimientos)
        ->with('grupos', $grupos)
        ->with('equipos', $funcionarios)
        ->with('usuarios', $applicants)
        ->with('submenu_activo', 'Administrador')
        ->with('menu_activo', 'Equipos');
    }
    public function getAdmigrupos() {
        $tipo_equipos = DB::table('equipment_types')->get();
        $tipo_mantenimientos = DB::table('equipment_type_maintenances')->get();
        $item_mantenimientos = EquipmentItemMaintenances::orderBy('id', 'asc')->get();
        $grupos = EquipmentGroups::where('status', 1)->orderBy('id', 'asc')->get();
        
        $funcionarios = DB::table('equipment_inventories')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
        ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name')
        // ->where('equipment_groups_has_equipment_inventories.equipment_groups_id', $id_grupo)
        ->get();

        $applicants = Users::all();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.admigrupos')
        ->with('tipo_equipos', $tipo_equipos)
        ->with('tipo_mantenimientos', $tipo_mantenimientos)
        ->with('item_mantenimientos', $item_mantenimientos)
        ->with('grupos', $grupos)
        ->with('equipos', $funcionarios)
        ->with('usuarios', $applicants)
        ->with('submenu_activo', 'Administrador')
        ->with('menu_activo', 'Equipos');
    }
    public function getAdminaddequipos() {
        $tipo_equipos           = DB::table('equipment_types')->get();
        $tipo_mantenimientos    = DB::table('equipment_type_maintenances')->get();
        $item_mantenimientos    = EquipmentItemMaintenances::orderBy('id', 'asc')->get();
        $grupos                 = EquipmentGroups::orderBy('id', 'asc')->get();
        $processes              = processes::get();
        
        $funcionarios = DB::table('equipment_inventories')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
        ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name')
        // ->where('equipment_groups_has_equipment_inventories.equipment_groups_id', $id_grupo)
        ->get();

        $applicants = Users::all();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.adminaddequipos')
        ->with('tipo_equipos', $tipo_equipos)
        ->with('tipo_mantenimientos', $tipo_mantenimientos)
        ->with('item_mantenimientos', $item_mantenimientos)
        ->with('grupos', $grupos)
        ->with('equipos', $funcionarios)
        ->with('usuarios', $applicants)
        ->with('processes', $processes)
        ->with('submenu_activo', 'Administrador')
        ->with('menu_activo', 'Equipos');
    }
    public function getAdminelimequipos() {
        $tipo_equipos = DB::table('equipment_types')->get();
        $tipo_mantenimientos = DB::table('equipment_type_maintenances')->get();
        $item_mantenimientos = EquipmentItemMaintenances::orderBy('id', 'asc')->get();
        $grupos = EquipmentGroups::orderBy('id', 'asc')->get();
        
        $funcionarios = DB::table('equipment_inventories')
        ->leftJoin('equipments_has_equipment_inventories', 'equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id')
        ->leftJoin('equipments', 'equipments.id', '=', 'equipments_has_equipment_inventories.equipments_id')
        ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
        ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
        ->select('equipment_inventories.id', 'equipment_inventories.mark', 'users.name', 'users.last_name')
        // ->where('equipment_groups_has_equipment_inventories.equipment_groups_id', $id_grupo)
        ->get();

        $applicants = Users::all();

        return View::make('dashboard.index')
        ->with('container', 'dashboard.equipos.adminelimequipos')
        ->with('tipo_equipos', $tipo_equipos)
        ->with('tipo_mantenimientos', $tipo_mantenimientos)
        ->with('item_mantenimientos', $item_mantenimientos)
        ->with('grupos', $grupos)
        ->with('equipos', $funcionarios)
        ->with('usuarios', $applicants)
        ->with('submenu_activo', 'Administrador')
        ->with('menu_activo', 'Equipos');
    }
    public function getNuevoitemmant(){
        $texto = Input::get('texto');
        $requerido = Input::get('campo_requerido');

        $nuevo_item = new EquipmentItemMaintenances;
        $nuevo_item->item   = $texto;
        $nuevo_item->status = 1;  
        $nuevo_item->tipo = 1;  
        $nuevo_item->required = $requerido;  
        
        if ($nuevo_item->save()) {
            $item_mantenimientos = EquipmentItemMaintenances::orderBy('id', 'asc')->get();
            
            echo '<option value=""></otpion>';
            foreach ($item_mantenimientos as $key) {
                echo '<option value="'.$key->id.'">'.$key->item.'</option>';
            }
        }else{
            
        }
    }

    

}
?>