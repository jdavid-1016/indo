<?php 
/**
* 
*/
class getuserController extends BaseController
{
	public function postData()
	{
		$user_id = Input::get('user');

		$user = User::find($user_id);

		$data = array(
			'success' => true,
			'id' => $user->id,
			'name' => $user->name,
			'last_name' => $user->last_name, 
			'email' => $user->email,
			'date' => $user->date,
			'phone' => $user->phone,
			'username' => $user->username,
			'rol_id' => $user->rol_id

			);
		return Response::json($data);
	}
}
?>