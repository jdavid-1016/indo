<?php 	
/**
* 	
*/
class SistemaController extends BaseController
{
	public function __construct()
	{
		$this->beforeFilter('auth'); //bloqueo de acceso
	}
	public function permission_control($menu_id){
		$user = Users::find(Auth::user()->id);
		$submenu_required = $menu_id;
		$bandera = 0;
		foreach ($user->Profiles as $perfil){
			foreach($perfil->submenus as $submenu){
			    if ($submenu_required == $submenu->id){
			        $bandera = "1";                       
			    }
			}	                
		}
		return $bandera;

	}
	public function getIndex(){
		$requests = Requests::where('state_requests_id', '==', 1 )->get();
		return View::make('dashboard.index')
		
		->with('requests',$requests)
		->with('container', 'dashboard.pqrs.gestion')
		->with('menu_activo', 'pqrs');		
		
		
	}
    public function usuarios(){
    	$permission = $this->permission_control("19");
    	if ($permission == 0) {
    		return View::make('dashboard.index')


    		->with('container', 'errors.access_denied_ad')
    		->with('submenu_activo', 'Administrar Solicitudes')
    		->with('menu_activo', 'Help Desk');
    		
    	}

        $user = Users::find(Auth::user()->id);
        $ciudades = City::get();
        $processes = processes::get();
        $submenus = Submenus::get();
        $menus = Menus::get();
        $profiles = Profiles::get();
               
        if(count($user->profiles) == 0){
            echo "el Usuario no tiene ningun perfil";
        }else{
            return View::make('dashboard.index')

            ->with('ciudades', $ciudades)
            ->with('container', 'dashboard.users.users')
            ->with('user', $user)
            ->with('processes', $processes)
            ->with('submenus', $submenus)
            ->with('menus', $menus)
            ->with('profiles', $profiles)
            ->with('submenu_activo', 'Administrar Usuarios')
            ->with('menu_activo', 'Sistema');
        }
		
		
	}
	public function tareas(){
    	$permission = $this->permission_control("19");
    	if ($permission == 0) {
    		return View::make('dashboard.index')


    		->with('container', 'errors.access_denied_ad')
    		->with('submenu_activo', 'Administrar Solicitudes')
    		->with('menu_activo', 'Help Desk');
    		
    	}

        $user = Users::find(Auth::user()->id);
        $ciudades = City::get();
        $processes = processes::get();
        $submenus = Submenus::get();
        $menus = Menus::get();
        $profiles = Profiles::get();
               
        if(count($user->profiles) == 0){
            echo "el Usuario no tiene ningun perfil";
        }else{
            return View::make('dashboard.index')

            ->with('ciudades', $ciudades)
            ->with('container', 'dashboard.sistema.tareas')
            ->with('user', $user)
            ->with('processes', $processes)
            ->with('submenus', $submenus)
            ->with('menus', $menus)
            ->with('profiles', $profiles)
            ->with('submenu_activo', 'Tareas')
            ->with('menu_activo', 'Sistema');
        }
		
		
	}
        
	public function my_pqrs(){
		$my_id = Auth::user()->id;

		$requests = Requests::where('user_id', '=',$my_id )->paginate(5);
		
		$TypeRequests 	= DB::table('Type_requests')->get();
		$process 		= DB::table('processes')->get();
		$TypeUsers 		= DB::table('type_users')->get();
		$detalle 		= DB::table('detalle')->get();

		//consulta trazabilidad
		$Traceabilitys = Traceabilitys::where('id', '<>', -2)->get();


		if (count($requests) == 0) {
			return View::make('dashboard.index')

			->with('error', 'Usted no ha ingresado nunguna PQR')

			->with('TypeRequests', $TypeRequests)
			->with('process', $process)
			->with('TypeUsers', $TypeUsers)
			->with('detalle', $detalle)
			->with('container', 'dashboard.pqrs.my_pqrs')
			->with('menu_activo', 'pqrs');


			
		}else{
			return View::make('dashboard.index')
			->with('traceabilitys', $Traceabilitys)
			->with('requests', $requests)
			->with('TypeRequests', $TypeRequests)
			->with('process', $process)
			->with('TypeUsers', $TypeUsers)
			->with('detalle', $detalle)
			->with('container', 'dashboard.pqrs.my_pqrs')
			->with('menu_activo', 'pqrs');
		}
	}

	public function gestion()
	{
		$my_id = Auth::user()->id;

		$requests = Requests::where('state_requests_id', '=', '1')->orderBy('id','desc')->get();

		$TypeRequests 			= DB::table('Type_requests')->get();
		$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
		$especifique 			= DB::table('especifique')->get();
		$descripcion 			= DB::table('descripcion')->get();
		$state_requests 		= DB::table('state_requests')->get();
		$processes 				= User::where('responsible', '=', 1)->get();
		$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();
		

		return View::make('dashboard.index')
		->with('requests',$requests)
		->with('TypeRequests', $TypeRequests)
		->with('especifique', $especifique)
		->with('description', $descripcion)
		->with('state_requests', $state_requests)
		->with('processes', $processes)
		->with('default_text',$default_text)
		->with('default_confidential',$default_confidential)
		->with('container', 'dashboard.pqrs.gestion')
		->with('menu_activo', 'pqrs');

	}

	public function responder()	{
		
		$my_id = Auth::user()->id;

		$requests = Requests::where('state_requests_id', '=', '2')->where('user_asign', '=', $my_id)->orderBy('id','desc')->get();

		$TypeRequests 			= DB::table('Type_requests')->get();
		$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
		$especifique 			= DB::table('especifique')->get();
		$descripcion 			= DB::table('descripcion')->get();
		$state_requests 		= DB::table('state_requests')->get();
		$processes 				= User::where('responsible', '=', 1)->get();
		$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();

		if (count($requests) == 0) {
			return View::make('dashboard.index')
			
			
			->with('container', 'dashboard.pqrs.gestion')
			->with('menu_activo', 'pqrs')
			->with('error', 'Usted no ha ingresado nunguna PQR');

		}else{
			return View::make('dashboard.index')
			->with('requests',$requests)
			->with('TypeRequests', $TypeRequests)
			->with('especifique', $especifique)
			->with('description', $descripcion)
			->with('state_requests', $state_requests)
			->with('processes', $processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('container', 'dashboard.pqrs.responder')
			->with('menu_activo', 'pqrs');

		}
	}

	public function historial(){
		$level = Auth::user()->rol_id;

		//control permissions only access administrator (ad)
		if($level==1)
		{
			$my_id = Auth::user()->id;
			$requests = Requests::where('state_requests_id', '<>', '0')->orderBy('id','desc')->paginate(10);
			$TypeRequests 			= DB::table('Type_requests')->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$descripcion 			= DB::table('descripcion')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();

			//$requests = DB::table('requests')->paginate(6);
			return View::make('dashboard.index')
			->with('requests',$requests)
			->with('TypeRequests', $TypeRequests)
			->with('especifique', $especifique)
			->with('description', $descripcion)
			->with('state_requests', $state_requests)
			->with('processes', $processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');
			
			
		}else{
			return View::make('dashboard.index')
			->with('container', 'errors.access_denied_ad')
			->with('menu_activo', 'administration');
		}
	}

	public function getDelete($user_id)	{
		//buscamos el usuario en la base de datos segun la id enviada
		$user = User::find($user_id);
		//eliminamos y redirigimos
		$user->delete();
		return Redirect::to('users')->with('status', 'ok_delete');
	}

	public function postNew(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		$nombre_supports = $_FILES['support']['name'];
		$ruta = $_FILES['support']['tmp_name'];

		if (isset($_FILES['support']) && $_FILES['support']['tmp_name'] == "") {
			$destino = 'Sin siporte';
		}else{
			$destino = 'assets/supports_pqrs/'.'soporte_'.$_POST['user_id'].$nombre_supports;
			copy($ruta, $destino);
		}

		$request = new Requests;

		$request->title 			= 		Input::get('title');
		$request->description 		= 		Input::get('description');
		$request->type_requests_id 	= 		Input::get('type_requests_id');
		$request->date 				= 		Input::get('date');
		// $request->detalle 			=		Input::get('detalle');
		$request->user_id 			= 		Input::get('user_id');
		$request->process_id 		= 		Input::get('process_id');
		$request->type_users_id 	= 		Input::get('type_users_id');
		$request->support 			=		$destino;


		//guardamos
		$request->save();
		
		//redirigimos a my requets
		return Redirect::to('my_pqrs')->with('status', 'ok_create');
	}
	public function postText(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		

		$DefaultText = new DefaultText;

		$DefaultText->text 			= 		Input::get('text');
		$DefaultText->id_user 		= 		Input::get('user_id');
		$DefaultText->date 			= 		Input::get('date');
		//guardamos
		$DefaultText->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postConfidential(){
		//validamos los inputs
		

		
		//si todo esta bien guardamos
		

		$DefaultText = new DefaultConfidential;

		$DefaultText->text 			= 		Input::get('text');
		$DefaultText->id_user 		= 		Input::get('user_id');
		$DefaultText->date 			= 		Input::get('date');
		//guardamos
		$DefaultText->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postReply(){
		//capturamos el id del request
		$data = Input::get('requests_id');
		
		//actualizamos los datos en la tabla requests
		Requests::where('id', '=', $data)->update(
			array(
				'state_requests_id'   =>Input::get('state_request'),
				'user_asign' 		  =>Input::get('user_asign')


			)
		);
		
		//ingresar registros en la tabla traceabilitys
		$traceability = new Traceabilitys;

		$traceability->coment 		= Input::get('coment');
		$traceability->date 		= Input::get('date');
		$traceability->users_coment = Input::get('users_coment');
		$traceability->confidential = Input::get('confidential');
		$traceability->user_id 		= Input::get('user_asign');
		$traceability->requests_id 	= Input::get('requests_id');

		//guardamos
		$traceability->save();
		
		//redirigimos a my requets
		return Redirect::to('responder')->with('status', 'ok_create');
	}
	public function postResponse(){
		//capturamos el id del request
		$data = Input::get('requests_id');
		
		//actualizamos los datos en la tabla requests
		Requests::where('id', '=', $data)->update(
			array(
				'state_requests_id'   =>Input::get('state_request'),
				'type_request2' 	  =>Input::get('state_request'),
				'specify' 			  =>Input::get('specify'),
				'specify_description' =>Input::get('specify_description'),
				'user_asign' 		  =>Input::get('user_asign')


			)
		);
		//ingresar registros en la tabla traceabilitys
		$traceability = new Traceabilitys;

		$traceability->coment 		= Input::get('coment');
		$traceability->date 		= Input::get('date');
		$traceability->users_coment = Input::get('users_coment');
		$traceability->confidential = Input::get('confidential');
		$traceability->user_id 		= Input::get('user_asign');
		$traceability->requests_id 	= Input::get('requests_id');

		//guardamos
		$traceability->save();
		
		//redirigimos a my requets
		return Redirect::to('gestion')->with('status', 'ok_create');
	}
	public function postMassive(){
		$data_request = array(
				'request_id' => Input::get('chck')
			);
		foreach ($data_request['request_id'] as $key) {
			echo $key;

			Requests::where('id', '=', $key)->update(
				array(
					'state_requests_id' 	=>Input::get('state_request'),
					'type_request2' 		=>Input::get('state_request'),
					'specify' 				=>Input::get('specify'),
					'specify_description' 	=>Input::get('specify_description'),
					'user_asign'		 	=>Input::get('user_asign')


				)
			);

			$traceability = new Traceabilitys;

			$traceability->coment 		= Input::get('coment');
			$traceability->date 		= Input::get('date');
			$traceability->users_coment = Input::get('users_coment');
			$traceability->confidential = Input::get('confidential');
			$traceability->user_id 		= Input::get('user_asign');
			$traceability->requests_id 	= $key;

			//guardamos
			$traceability->save();
		}


		return Redirect::to('gestion')->with('status', 'ok_create');
	}


	public function historialAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];

			$requests = Requests::where('id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}elseif (isset($_POST['valorCaja2']) && $_POST['valorCaja2'] != 'Proceso' && $_POST['valorCaja2'] != '' ) {

			$item = $_POST['valorCaja2'];

			$requests = Requests::where('process_id', '=', "$item")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}elseif (isset($_POST['valorCaja3']) && $_POST['valorCaja3'] != 'Estado' && $_POST['valorCaja3'] != '' ) {

			$item = $_POST['valorCaja3'];
		

			$requests = Requests::where('state_requests_id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}elseif (isset($_POST['valorCaja4']) && $_POST['valorCaja4'] != 'Tipo PQR' && $_POST['valorCaja4'] != '' ) {

			$item = $_POST['valorCaja4'];
			

			$requests = Requests::where('type_requests_id', 'LIKE', "%$item%")->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests);
			
		}
		else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->paginate(10);
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}

	}
	public function gestionAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];
			$my_id = Auth::user()->id;
			$requests = Requests::where('id', '=', $item)->paginate(10);
			foreach ($requests as $key) {
				$item_last = $key->user->id;
			}

			$previus_requests = Requests::where('user_id', '=', $item_last)->get();
			$TypeRequests 			= DB::table('Type_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$descripcion 			= DB::table('descripcion')->get();



			return View::make('containers.ajaxGestion')
			->with('requests',$requests)
			->with('previus_requests',$previus_requests)
			->with('TypeRequests',$TypeRequests)
			->with('processes',$processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('especifique',$especifique)
			->with('state_requests',$state_requests)
			->with('description',$descripcion)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->get();
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}
	}
	public function responderAjax(){
		if (isset($_POST['valorCaja1']) && $_POST['valorCaja1'] != '') {

			$item = $_POST['valorCaja1'];
			$my_id = Auth::user()->id;
			$requests = Requests::where('id', '=', $item)->paginate(10);
			foreach ($requests as $key) {
				$item_last = $key->user->id;
			}

			$previus_requests = Requests::where('user_id', '=', $item_last)->get();
			$TypeRequests 			= DB::table('Type_requests')->get();
			$processes 				= User::where('responsible', '=', 1)->get();
			$default_text 			= DB::table('default_texts')->where('id_user', '=', $my_id)->get();
			$default_confidential 	= DB::table('default_confidentials')->where('id_user', '=', $my_id)->get();
			$especifique 			= DB::table('especifique')->get();
			$state_requests 		= DB::table('state_requests')->get();
			$descripcion 			= DB::table('descripcion')->get();


			return View::make('containers.ajaxResponder')
			->with('requests',$requests)
			->with('previus_requests',$previus_requests)
			->with('TypeRequests',$TypeRequests)
			->with('processes',$processes)
			->with('default_text',$default_text)
			->with('default_confidential',$default_confidential)
			->with('especifique',$especifique)
			->with('state_requests',$state_requests)
			->with('description',$descripcion)

			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}else{
			
			$requests = Requests::where('state_requests_id', '<>', '0')->get();
			return View::make('containers.ajaxHistorial')
			->with('requests',$requests)
			->with('container', 'dashboard.pqrs.historial')
			->with('menu_activo', 'pqrs');

		}
	}
	public function respMasivo(){
		$item = $_POST['id'];
		echo $item;
		
		echo "hola";
	}

	public function mail_masivo(){

		ini_set('max_execution_time', 240); //240 segundos = 4 minutos
		$users = Users::all();

		foreach ($users as $key) {

			if ($key->id == 174 or $key->id == 1940 or $key->id == 1320) {
				
				$supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
				->where('supports.support_status_id', '<>', '1')
				->where('supports.support_status_id', '<>', '4')
				->where('supports.support_status_id', '<>', '5')
				->where('supports.support_status_id', '<>', '6')
				->where('supports.users_id', '=', $key->id)
				->where('rating_status', '=', 0)
				->orderBy('id', 'desc')
				->get();

				$supports_asign = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
						->where('supports.support_status_id', '<>', '2')
				        ->where('supports.support_status_id', '<>', '3')
				        ->where('supports.support_status_id', '<>', '6')
				        ->where('user_id', '=', $key->id)
				        ->get();

				$tareas = DB::table('tasks_has_users')
				->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
				->join('users', 'users.id', '=', 'tasks.users_id')
				->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
				->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
				->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
				->select('tasks.id', 'users.name', 'users.last_name', 'users.img_min','tasks.tasks', 'tasks.description', 'tasks_periodicities.periodicity',
				    'tasks.start_date', 'tasks_statuses.status','tasks.progress','tasks_categories.category', 'tasks_has_users.id as id_asig', 'tasks.tasks_statuses_id')
				->where('tasks_has_users.users_id', $key->id)
				->where('tasks_has_users.status', 1)
				->where('tasks.tasks_statuses_id', '<>', 2)
				->where('tasks.tasks_statuses_id', '<>', 4)
				->get();

				$tareas_ingresadas = DB::table('tasks_has_users')
				->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
				->join('users', 'users.id', '=', 'tasks.users_id')
				->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
				->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
				->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
				->select('tasks.id', 'users.name', 'users.last_name', 'users.img_min','tasks.tasks', 'tasks.description', 'tasks_periodicities.periodicity',
				    'tasks.start_date', 'tasks_statuses.status','tasks.progress','tasks_categories.category', 'tasks_has_users.id as id_asig', 'tasks.tasks_statuses_id')
				
				->where('tasks.users_id', $key->id)
				->where('tasks_has_users.status', 1)
				->where('tasks.tasks_statuses_id', '<>', 2)
				->where('tasks.tasks_statuses_id', '<>', 4)
				->get();

				if (count($supports) != 0 or count($supports_asign) != 0 or count($tareas) != 0 or count($tareas_ingresadas) != 0) {



					$fromEmail = "Indoamericana@indoamericana.edu.co";
					$fromName = "Indoamericana";
					    
					$correo_envio = $key->email_institutional;
					$data= array(
					    'supports2' 		=> $supports_asign,
					    'supports' 			=> $supports,
					    'tareas_ingresadas' => $tareas_ingresadas,
					    'user' 				=> $key,
					    'tareas' 			=> $tareas

					);
					    
						Mail::send('emails.masivo', $data, function ($message) use ($fromName, $fromEmail, $correo_envio){
							$message->subject('Resumen diario SGI');
							$message->to($correo_envio);
							$message->from($fromEmail, $fromName);
						});
					// return View::make('emails.masivo')
					// ->with('data', $supports)
					// ->with('data_asign', $supports_asign);

					return View::make('emails.masivo')
					
					->with('supports', $supports_asign)
					->with('supports2', $supports)
					->with('tareas', $tareas)
					->with('tareas_ingresadas', $tareas_ingresadas)
					->with('user', $key)
					->with('menu_activo', 'pqrs')
					->with('submenu_activo', 'pqrs');


					
				}
			}
		}
		
	}
	public function mail_masivo_semanal(){

		ini_set('max_execution_time', 240); //240 segundos = 4 minutos
		$users = Users::all();
		$fecha_actual = date('Y-m-d');
		$nuevafecha = date('Y-m-d h:i:s', strtotime("$fecha_actual - 7 day"));

		foreach ($users as $key) {

			if ($key->id == 1740 or $key->id == 194 or $key->id == 1320) {
				


				$ingresos = DB::table('income_statistics')
							->where('created_at', '>=', $nuevafecha)
							->where('users_id', $key->id)
							->get();

				$archivos = DB::table('files_statistics')
							->where('created_at', '>=', $nuevafecha)
							->where('users_id', $key->id)
							->get();

				$soportes_atendidos = DB::table('support_traces')
							->where('updated_at', '>=', $nuevafecha)
							->where('user_id', $key->id)
							->get();

				$soportes_ingresados = DB::table('supports')
							->where('created_at', '>=', $nuevafecha)
							->where('users_id', $key->id)
							->get();


				$calificaciones = DB::table('support_traces')
							->select('users_id', 'users.name','users.last_name', DB::raw('avg(support_ratings.value) AS dato '))
							->join('supports',  'supports.id',  '=',  'support_traces.supports_id') 
							->join('support_ratings',  'support_ratings.support_id',  '=',  'supports.id') 
							->join('users',  'users.id',  '=',  'support_traces.user_id' ) 
							->where('supports.support_status_id', '2')
							->where('support_traces.updated_at', '>=', $nuevafecha)
							->where('support_traces.user_id', $key->id)
							->groupBy('user_id')
							->get();

				

				$actividades_atendidas = DB::table('tasks_has_users')
							->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
							->where('tasks_has_users.users_id', $key->id)
							->where('tasks.tasks_statuses_id', '<>', 1)
							->where('tasks.tasks_statuses_id', '<>', 3)
							->where('tasks.created_at', '>=', $nuevafecha)
							->get();

				$actividades_ingresadas = DB::table('tasks')
							->where('tasks.users_id', $key->id)
							->where('tasks.created_at', '>=', $nuevafecha)
							->get();


				
				


				if (count($ingresos) != 0) {



					$fromEmail = "Indoamericana@indoamericana.edu.co";
					$fromName = "Indoamericana";
					    
					$correo_envio = $key->email_institutional;
					$data= array(
					    'ingresos' 				=> $ingresos,
					    'archivos' 				=> $archivos,
					    'soportes_atendidos' 	=> $soportes_atendidos,
					    'soportes_ingresados' 	=> $soportes_ingresados,
					    'calificaciones' 		=> $calificaciones,
					    'actividades_atendidas' => $actividades_atendidas,
					    'actividades_ingresadas'=> $actividades_ingresadas,
					    'user' 					=> $key
					    

					);
					    
//						Mail::send('emails.masivo_semanal', $data, function ($message) use ($fromName, $fromEmail, $correo_envio){
//							$message->subject('Resumen Semanal SGI');
//							$message->to($correo_envio);
//							$message->from($fromEmail, $fromName);
//						});
					// return View::make('emails.masivo')
					// ->with('data', $supports)
					// ->with('data_asign', $supports_asign);

					return View::make('emails.masivo_semanal')
					
					->with('ingresos', $ingresos)
					->with('archivos', $archivos)
					->with('soportes_atendidos', $soportes_atendidos)
					->with('soportes_ingresados', $soportes_ingresados)
					->with('calificaciones', $calificaciones)
					->with('actividades_atendidas', $actividades_atendidas)
					->with('actividades_ingresadas', $actividades_ingresadas)
					->with('user', $key)
					->with('menu_activo', 'pqrs')
					->with('submenu_activo', 'pqrs');


					
				}
			}
		}
		
	}

		
}

?>