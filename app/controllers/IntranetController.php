<?php

/**
 * Controlador Intranet	
 */
class IntranetController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }
    
    public function Blog() {

        //construimos la vista
        return View::make('dashboard.index')
                        //->with('administradores', $administradores)
                        //->with('user', $user)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.intranet.blog')
                        ->with('menu_activo', 'Educacion Continuada')
                        ->with('submenu_activo', 'Blog');
    }

    /*
     * Funcion para armar la vista de informacion general
     */

    public function informacionGeneral() {

        /* $administradores = DB::table('users')
          ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
          ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
          ->join('processes', 'profiles.processes_id', '=', 'processes.id')
          ->join('administratives', 'users.id', '=', 'administratives.user_id')
          ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
          ->where('status', 1)
          ->orderBy('last_name', 'asc')
          ->get(); */

        //construimos la vista
        return View::make('dashboard.index')
                        //->with('administradores', $administradores)
                        //->with('user', $user)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.intranet.informacion_general')
                        ->with('menu_activo', 'Intranet')
                        ->with('submenu_activo', 'Información General');
    }

    /*
     * Funcion que recibe la peticion de FunCtrl() en Angular para mostrar la lista de Funcionarios
     */

    public function funcionarios() {
        $bandera = "";

        //consultamos los usuarios administrativos
        return $funcionarios = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img', 'profiles.profile', 'processes.acronyms', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('status', 1)
                ->orderBy('last_name', 'asc')
                ->get();
    }

    /*
     * Funcion que recibe la peticion de FunCtrl() en Angular para mostrar el detalle de cada funcionario
     */

    public function callUserProfile() {
        if (Input::get('id')) {
            //consultamos los datos del usuario administrativo
            $data = DB::table('users')
                    ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                    ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                    ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                    ->join('administratives', 'users.id', '=', 'administratives.user_id')
                    ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_profile', 'profiles.profile', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                    ->where('users.id', Input::get('id'))
                    ->get();

            //devolvemos el modal
            echo'<div class="modal-dialog">
            <div class="modal-content">
             <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
               <h3 class="modal-title">' . $data[0]->name . ' ' . $data[0]->name2 . ' ' . $data[0]->last_name . ' ' . $data[0]->last_name2 . '</h3>
             </div>
             <div class="modal-body">
                <div class="row">
                   <div class="col-md-5">
                      <img src="' . URL::to($data[0]->img_profile) . '" class="img-responsive img-profile" style=" width: 193px; height: 160px;" alt="">
                   </div>
                   <div class="col-md-7">
                     <h3>' . $data[0]->profile . '</h3>
                     <br/>
                     <h4><i class="icon-headphones"></i>' . $data[0]->extension . '</h4>
                     <h4><i class="icon-envelope"></i>' . $data[0]->email_institutional . '</h4>
                     <h4><i class="icon-phone"></i>' . $data[0]->cell_phone . '</h4>
                   </div>
                </div>
             </div>
          </div>
          </div>';

            //header('Content-type: text/plain');
            //echo json_encode($response);
        }
    }

    /*
     * Funcion para armar la vista de cada proceso
     */

    public function Proceso($id) {

        //consultamos los usuarios administrativos
        $proceso = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_profile', 'users.img', 'profiles.profile', 'processes.name as proceso', 'administratives.responsible', 'processes.acronyms', 'processes.description', 'processes.id as id_proceso', 'administratives.extension', 'users.email_institutional', 'users.cell_phone', 'users.linked', 'users.google', 'users.facebook', 'users.twitter', 'users.skype')
                ->where('processes.id', $id)
                ->where('status', 1)
                ->orderBy('name', 'asc')
                ->get();

        //consultamos las carpetas 
        $carpetas = DB::table('folders')
                ->where('process_id', $id)
                ->get();

        //consultamos las galerias
        $galerias = DB::table('galleries')
                ->where('process_id', $id)
                ->get();

        //consultamos las imagenes
        $imagenes = DB::table('images')
                ->join('galleries', 'galleries.id', '=', 'images.gallery_id')
                ->select('galleries.name', 'galleries.name_gallery', 'images.name_image', 'images.gallery_id', 'images.id', 'images.likes')
                ->where('galleries.process_id', $id)
                ->get();
        
        
        //get POST data        

            $procesos = new ProcessStatistics;
            $procesos->users_id = Auth::user()->id;
            $procesos->processes_id = $id;

            $procesos->save();

        $submenu = "";
        $vista = "dashboard.intranet.proceso";
        if ($id == 9) {
            //$vista = "dashboard.intranet.proceso2";
            $submenu = "Gestión seguridad operacional";
        } else {
            $submenu = $proceso[0]->proceso;
        }

        //verificamos si el usuario logueado tiene permisos para administrar el proceso
        $permisos = 0;
        foreach ($proceso as $row) {
            if ($row->responsible == 1) {
                if ($row->email_institutional == Auth::user()->email_institutional) {
                    $permisos = 1;
                }
            }
        }

        //construimos la vista
        return View::make('dashboard.index')
                        //->with('user', $user)
                        ->with('proceso', $proceso)
                        ->with('permisos', $permisos)
                        ->with('carpetas', $carpetas)
                        ->with('galerias', $galerias)
                        ->with('imagenes', $imagenes)
                        ->with('actual', "none")
                        ->with('content', 'container')
                        ->with('container', $vista)
                        ->with('menu_activo', 'Intranet')
                        ->with('submenu_activo', $submenu);
    }

    /*
     * Funcion que recibe la peticion de documentacion.js en Javascript para cargar los documentos de cada proceso por carpeta
     */

    public function cargarDocumentos() {
        //recibimos el id de la carpeta
        $id = Input::get('id');

        //consultamos los archivos de la carpeta
        $archivos = DB::table('folders')
                ->join('processes', 'processes.id', '=', 'folders.process_id')
                ->join('files', 'files.folder_id', '=', 'folders.id')
                ->select('folders.name_folder', 'folders.name', 'files.name as archivo', 'files.name_file', 'files.id', 'processes.acronyms')
                ->where('folders.id', $id)
                ->get();

        //devolvemos los archivos en una lista
        $print = "";
        $print .= "<ul>";
        foreach ($archivos as $arc) {
            $time = time();
            $print .= '<li dir="../assets/files/' . $arc->acronyms . '/' . $arc->name_folder . '/' . $arc->name_file . '" id="' . $arc->id . '" class="file"><i class="icon-file-text"></i>' . $arc->archivo . '</li>';
        }
        $print .="</ul>";
        echo $print;
    }

    public function Perfil() {
        $user = Users::find(Auth::user()->id);
        //$proceso = Profiles::where('processes_id', '=', $id)->get();
        $perfil = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->join('processes', 'profiles.processes_id', '=', 'processes.id')
                ->join('administratives', 'users.id', '=', 'administratives.user_id')
                ->select('users.id', 'users.name', 'users.name2', 'users.last_name', 'users.last_name2', 'users.img_profile', 'profiles.profile', 'processes.name as proceso', 'processes.id as id_proceso', 'administratives.responsible', 'processes.acronyms', 'processes.description', 'administratives.extension', 'users.email_institutional', 'users.cell_phone')
                ->where('users.id', Auth::user()->id)
                ->where('status', 1)
                ->orderBy('name', 'asc')
                ->get();                

        // $equipos = Users::find(Auth::user()->id)->EquipmentInventories;

        $equipos = DB::table("equipment_inventories")
                ->leftJoin('equipment_types','equipment_types.id', '=', 'equipment_inventories.equipment_types_id') 
                ->leftJoin('equipments_has_equipment_inventories','equipments_has_equipment_inventories.equipment_inventories_id', '=', 'equipment_inventories.id') 
                ->leftJoin('equipment_inventories_users','equipment_inventories_users.equipments_id', '=', 'equipments_has_equipment_inventories.equipments_id') 
                ->leftJoin('users', 'equipment_inventories_users.users_id', '=', 'users.id') 
                ->select('equipment_inventories.id as id_equipo','equipment_inventories.equipment_types_id','equipment_inventories.model'
                ,'equipment_inventories.serial','equipment_inventories.image' , 'equipment_inventories.mark', 'equipment_inventories.date_asign'
                ,'users.id as id_user'
                ,'users.name', 'users.last_name', 'users.img_min', 'equipment_types.type', 'equipment_inventories.ip', 'equipment_inventories.mac'
                , 'equipment_inventories.memory', 'equipment_inventories.hard_drive', 'equipment_inventories.processor'
                ,'equipment_inventories.operating_system'
                ,'equipment_inventories.license_of'
                ,'equipment_inventories.version_op'
                ,'equipment_inventories.no_license_op'
                ,'equipment_inventories_users.id as id_asignacion', 'equipments_has_equipment_inventories.id as id_asignacion_equipo'
                ,'equipments_has_equipment_inventories.status')
                ->where('users.id',Auth::user()->id)
                ->groupBy('equipment_inventories.id')
                ->paginate(15);

        
        $supports = SupportTraces::join('supports', 'supports.id', '=', 'supports_id')
                ->where('supports.support_status_id', '<>', '1')
                ->where('supports.support_status_id', '<>', '4')
                ->where('supports.support_status_id', '<>', '5')
                ->where('supports.users_id', '=', Auth::user()->id)
                ->orderBy('rating_status', 'asc')
                ->orderBy('id', 'asc')
                ->get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($supports); $i++) {
            $fecha = date($supports[$i]->Supports->created_at);
            $fecha_close = date($supports[$i]->Supports->updated_at);


            $data[$i]["id"] = 'HD' . str_pad($supports[$i]->Supports->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $supports[$i]->Supports->Users->name . " " . $supports[$i]->Supports->Users->last_name;
            $data[$i]["state"] = $supports[$i]->Supports->SupportStatus->name;
            $data[$i]["user_priority"] = $supports[$i]->Supports->user_priority;
            $data[$i]["message"] = $supports[$i]->Supports->message;
            $data[$i]["created_at"] = $fecha;
            $data[$i]["closed_at"] = $fecha_close;
            $data[$i]["hid"] = $supports[$i]->Supports->id;
            $data[$i]["category"] = $supports[$i]->Supports->SupportCategory->name;
            $data[$i]["observation"] = $supports[$i]->observation;
            $data[$i]['time'] = $this->interval_date($fecha, $fecha_close);

            $data[$i]['rating_status'] = $supports[$i]->rating_status;

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible'] = 'Sin Asignar';
            else {
                $user = Users::find($supports[$i]->user_id);
                $data[$i]['responsible'] = $user->name . ' ' . $user->last_name;
                $data[$i]['responsible_img'] = $user->img_min;
            }

            if ($supports[$i]->user_id == '')
                $data[$i]['responsible_color'] = 'label-danger';
            
                else($data[$i]['responsible_color'] = 'label-success');

            if ($supports[$i]->Supports->user_priority == 5) {
                $data[$i]["class_priority"] = 'badge-info';
            } else {
                $data[$i]["class_priority"] = 'badge-important';
            }
            if ($supports[$i]->Supports->support_status_id == 2) {

                if ($supports[$i]->scale == 1) {
                    $data[$i]['ocult'] = 'ocult' . $data[$i]["hid"];
                    $data[$i]['display'] = 'display:none';
                    $data[$i]['id_plus'] = '';
                    $data[$i]['scaled'] = 'icon-long-arrow-up';
                } else {

                    if ($supports[$i]->consecutive == 1) {
                        $data[$i]['scaled'] = 'icon-plus-sign';
                        $data[$i]['id_plus'] = $data[$i]["hid"];
                    } else {
                        $data[$i]['display'] = 'display:nne';
                        $data[$i]['scaled'] = '';
                    }

                    $data[$i]['display'] = '';
                    $data[$i]['ocult'] = '';
                    $data[$i]['id_plus'] = $data[$i]["hid"];
                }
            } else {
                $data[$i]['scaled'] = '';
                $data[$i]['id_plus'] = '';
                $data[$i]['display'] = 'hola2';
                $data[$i]['ocult'] = '';
            }
            $data[$i]['row_color'] = 'red';

            
            if ($supports[$i]->Supports->user_priority == 1)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 2)
                $data[$i]['class_priority'] = 'badge-important';
            else if ($supports[$i]->Supports->user_priority == 3)
                $data[$i]['class_priority'] = 'badge-warning';
            else if ($supports[$i]->Supports->user_priority == 4)
                $data[$i]['class_priority'] = 'badge-info';
            else if ($supports[$i]->Supports->user_priority == 5)
                $data[$i]['class_priority'] = 'badge-info';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($supports[$i]->Supports->support_status_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($supports[$i]->Supports->support_status_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($supports[$i]->Supports->support_status_id == 5)
                $data[$i]['label'] = 'label-warning';
            else if ($supports[$i]->Supports->support_status_id == 6)
                $data[$i]['label'] = 'label-warning';

            if ($supports[$i]->Supports->support_status_id == 1)
                $data[$i]['row_color'] = 'red';

            if ($supports[$i]->Supports->support_status_id == 2)
                $data[$i]['row_color'] = 'green';

        }
        
        
        return View::make('dashboard.index')
                        ->with('equipos', $equipos)
                        ->with('perfiles', $perfil)
                        ->with('supports', $data)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.intranet.perfil')
                        ->with('menu_activo', 'none')
                        ->with('submenu_activo', 'none');
    }
    public function interval_date($init, $finish) {
        //formateamos las fechas a segundos tipo 1374998435
        $diferencia = strtotime($finish) - strtotime($init);

        //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
        //floor devuelve el número entero anterior, si es 5.7 devuelve 5
        if ($diferencia < 60) {
            $tiempo = floor($diferencia) . " Seg";
        } else if ($diferencia > 60 && $diferencia < 3600) {
            $tiempo = floor($diferencia / 60) . ' Min';
        } else if ($diferencia > 3600 && $diferencia < 86400) {
            $tiempo = floor($diferencia / 3600) . " Horas";
        } else if ($diferencia > 86400 && $diferencia < 2592000) {
            $tiempo = floor($diferencia / 86400) . " Días";
        } else if ($diferencia > 2592000 && $diferencia < 31104000) {
            $tiempo = floor($diferencia / 2592000) . " Meses";
        } else if ($diferencia > 31104000) {
            $tiempo = floor($diferencia / 31104000) . " Años";
        } else {
            $tiempo = "Error";
        }
        return $tiempo;
    }
    public function actualizarperfil() {
        //get POST data

        $user = Users::find(Auth::user()->id);

        $user->cell_phone = Input::get('celular');
        $user->phone = Input::get('telefono');
        $user->email = Input::get('email');
        $user->address = Input::get('direccion');
        if ($user->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function redes() {
        //get POST data

        $user = Users::find(Auth::user()->id);

        $user->linked = Input::get('linked');
        $user->google = Input::get('google');
        $user->facebook = Input::get('facebook');
        $user->twitter = Input::get('twitter');
        $user->skype = Input::get('skype');
        if ($user->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function resena() {
        //get POST data              

        $proceso = Processes::find(Input::get('id'));

        $proceso->description = Input::get('editor');
        if ($proceso->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function crearDir() {

        $name = Input::get('nombre');
        $id = Input::get('id');
        $proceso = Input::get('proceso');
        $name_folder = str_replace(" ", "_", $name);

        if (mkdir('assets/files/' . $proceso . '/' . $name_folder, 0777)) {
            $folder = new Folders;

            $folder->name = $name;
            $folder->name_folder = $name_folder;
            $folder->process_id = $id;

            if ($folder->save()) {
                $folder = Folders::all();
                $array = array(
                    "id" => $folder->last()->id,
                    "name" => $name,
                );
                return $array;
            } else {
                echo "2";
            }
        }
    }

    public function guardarArc() {

        if (Input::hasFile('arc')) {

            $folder = DB::table('folders')
                    ->join('processes', 'folders.process_id', '=', 'processes.id')
                    ->select('folders.id', 'folders.name_folder', 'processes.acronyms')
                    ->where('folders.id', Input::get('carpetas'))
                    ->get();

            $filename = Input::file('arc')->getClientOriginalName();
            $name = Input::get('nombre');
            $id_folder = Input::get('carpetas');
            $destinationPath = 'assets/files/' . $folder[0]->acronyms . '/' . $folder[0]->name_folder . '/';

            $uploadSuccess = Input::file('arc')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                $file = new Files;

                $file->name = $name;
                $file->name_file = $filename;
                $file->folder_id = $id_folder;

                $file->save();
                $url = Input::get('url') . "?embed=../" . $destinationPath . $filename . "#tab_1_3";
                return Redirect::away($url);
            }
        } else {
            echo "no";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    
    public function estadocumentos() {
                
        //get POST data        

            $file = new FilesStatistics;            
            $file->users_id = Auth::user()->id;
            $file->files_id = Input::get('id');

            if ($file->save()) {
                echo "1";
            } else {
                echo "2";
            }
    }

    public function cambiarpass() {
        //get POST data              
        $pass_actual = Hash::make(Input::get('actual'));
        $user = Users::find(Auth::user()->id);
        if (Hash::check(Input::get('actual'), $user->password)) {
            $user->password = Hash::make(Input::get('nueva'));
            if ($user->save()) {
                echo "1";
            } else {
                echo "3";
            }
        } else {
            echo "2";
        }
    }

    public function estadisticas() {

        $permission = $this->permission_control("28");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }

        $user = Users::find(Auth::user()->id);

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.intranet.statistics')
                            ->with('user', $user)
                            ->with('submenu_activo', 'Estadísticas')
                            ->with('menu_activo', 'Intranet');
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function graphic_ingresos() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM users
				JOIN income_statistics ON (income_statistics.users_id = users.id)
				GROUP BY users_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->name . " " . $key->last_name;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Ingresos por Usuario");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function changedate() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM users
				JOIN income_statistics ON (income_statistics.users_id = users.id)
                                WHERE income_statistics.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
				GROUP BY users_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name . " " . $key->last_name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Ingresos por Usuario");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function exportar() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM users
				JOIN income_statistics ON (income_statistics.users_id = users.id)
                                WHERE income_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY users_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT users_id, users.name,users.last_name, income_statistics.ip, income_statistics.created_at
				FROM users
				JOIN income_statistics ON (income_statistics.users_id = users.id)
                                WHERE income_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				ORDER BY users_id DESC";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Ingresos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->name . " " . $datos[$i]->last_name)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de ingresos');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'IP')
                ->setCellValue('C1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('B' . $j, $detalle[$i]->ip)
                    ->setCellValue('C' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Ingresos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
        
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function graphic_files() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT files.name AS archivo, users.name,users.last_name, COUNT( * ) AS dato
				FROM files
				JOIN files_statistics ON (files_statistics.files_id = files.id)
                                JOIN users ON (files_statistics.users_id = users.id)
				GROUP BY files_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->archivo;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de documentos mas vistos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function changedatefiles() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");       
        
        $sql = "SELECT files.name AS archivo, users.name,users.last_name, COUNT( * ) AS dato
				FROM files
				JOIN files_statistics ON (files_statistics.files_id = files.id)
                                JOIN users ON (files_statistics.users_id = users.id)
                                WHERE files_statistics.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
				GROUP BY files_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->archivo;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de documentos mas vistos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function exportarfiles() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT files.name AS archivo, users.name,users.last_name, COUNT( * ) AS dato
				FROM files
				JOIN files_statistics ON (files_statistics.files_id = files.id)
                                JOIN users ON (files_statistics.users_id = users.id)
                                WHERE files_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY files_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT files.name AS archivo, files_statistics.created_at, users.name, users.last_name
				FROM files
				JOIN files_statistics ON (files_statistics.files_id = files.id)
                                JOIN users ON (files_statistics.users_id = users.id)
                                WHERE files_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";        

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Documento')
                ->setCellValue('B1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->archivo)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Documento')
                ->setCellValue('B1', 'Usuario')
                ->setCellValue('C1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->archivo)
                    ->setCellValue('B' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('C' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Documentos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function graphic_process() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT processes.acronyms, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
				GROUP BY processes_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->acronyms;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de procesos mas visitados");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }
    
    public function changedateprocess() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");                       
        
        $sql = "SELECT processes.acronyms, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
				GROUP BY processes_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->acronyms;                
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de procesos mas visitados");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function exportarprocess() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, COUNT( * ) AS dato
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "' 
				GROUP BY processes_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);
        
        $sql = "SELECT processes.acronyms, processes.name AS proceso, users.name,users.last_name, process_statistics.created_at
				FROM processes
				JOIN process_statistics ON (process_statistics.processes_id = processes.id)
                                JOIN users ON (process_statistics.users_id = users.id)
                                WHERE process_statistics.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Documentos mas vistos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Vistas');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->proceso)
                    ->setCellValue('B' . $j, $datos[$i]->acronyms)
                    ->setCellValue('C' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de Vistas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Proceso')
                ->setCellValue('B1', 'Siglas')
                ->setCellValue('C1', 'Usuario')
                ->setCellValue('D1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)                    
                    ->setCellValue('A' . $j, $detalle[$i]->proceso)
                    ->setCellValue('B' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('C' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('D' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Procesos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function likeImage() {
        //get POST data              

        $images = Images::find(Input::get('id'));
        $images->likes = Input::get('cont');
        if ($images->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function actIngresos() {
        //get POST data        
        $sql = "SELECT*FROM income_statistics WHERE users_id ='" . Auth::user()->id . "' AND str_to_date(created_at,'%Y-%m-%d') = '" . date("Y-m-d") . "'";
        $datos = DB::select($sql);
        if ($datos) {
            
        } else {

            $income = new IncomeStatistics;
            $income->ip = Request::getClientIp();
            $income->users_id = Auth::user()->id;
            $income->save();
        }
        
    }
    
    public function actCalificaciones() {
        //get POST data
        $sql = "SELECT*FROM supports WHERE users_id ='" . Auth::user()->id . "' AND rating_status = '0' AND (support_status_id = '2' or support_status_id = '3')";
        $datos = DB::select($sql);
        if($datos){
         $sql = "SELECT*FROM pending_ratings WHERE users_id ='" . Auth::user()->id . "' AND str_to_date(created_at,'%Y-%m-%d') = '" . date("Y-m-d") . "'";
         $datos = DB::select($sql);
         
               if ($datos) {

                   return 0;
               } else {                  
                   
                   $pending = new PendingRatings;
                   $pending->ip = Request::getClientIp();
                   $pending->users_id = Auth::user()->id;
                   $pending->save();
                   
                   return 1;
               }
        }else{
            return 0;
        }
        
    }       

    public function actTutorial() {
        //get POST data

        $user = Users::find(Auth::user()->id);

        $user->tutorial = Input::get('cont');
        if ($user->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    /*
     * Funcion para armar la vista de mensajes
     */

    public function mensajes() {

        $my_id = Auth::user()->id;

        $sql2 = "SELECT chat_id, count(*) as dato FROM `messages` WHERE users_id = '$my_id' or users_id1 = '$my_id' group by chat_id order by id desc";
        $chats = DB::select($sql2);

        //construimos la vista
        return View::make('dashboard.index')
                        ->with('chats', $chats)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.intranet.mensajes')
                        ->with('menu_activo', 'Intranet')
                        ->with('submenu_activo', '');
    }

    /*
     * Funcion que recibe el id de la conversacion para mostrar todos los mensajes
     */

    public function conversacion() {

        $my_id = Auth::user()->id;

        $sql = "SELECT * FROM `messages` WHERE chat_id = '" . Input::get('id') . "' order by id";
        $messages = DB::select($sql);

        foreach ($messages as $message) {
            $user = Users::find($message->users_id);
            $user_id = $user->id;
            if ($user_id == Auth::user()->id) {
                $user = Users::find($message->users_id1);
            }
        }
            //construimos la vista
            return View::make('dashboard.intranet.conversacion')
                            ->with('mensajes', $messages)
                            ->with('nombre', $user->name." ".$user->last_name)
                            ->with('imagen', $user->img_min);
    }


    public function conversacion_new_chat() {

        
        $user = Users::find(Input::get('id'));
            //construimos la vista
            return View::make('dashboard.intranet.create_new_chat')
            ->with('user_name', $user);
            
            
    }
        
    public function nuevomsj() {
        $id_chat = Input::get('id');
        $my_id = Auth::user()->id;
        $you_id = "";
        
        if ($id_chat == "0") {
            $sql = "SELECT * FROM `chats` order by id desc limit 1";  
            $messages = DB::select($sql);

            $new_chat = $messages[0]->id+1;//capturo el id que va a tener el nuevo chat

            $chat = new Chat;
            $chat->id  = $new_chat;

            $chat->save(); //guardo el nuevo chat


            $you_id = Input::get('id_user_chat');
            $userchat = Users::find($you_id);
            $msj = new Messages;
            $msj->users_id = $my_id;
            $msj->users_id1 = $you_id;
            $msj->chat_id = $new_chat;
            $msj->message = Input::get('mensaje');
            
            if($msj->save()){
                                
                $sql = "SELECT * FROM `messages` WHERE chat_id = '" . $new_chat . "' order by id desc limit 1";
                $messages = DB::select($sql);
                //return $messages;
                $arreglo = Array();
                $fecha = date("M d, Y H:i",strtotime($messages[0]->created_at));
                $arreglo[0] = $you_id;
                $arreglo[1] = $messages[0]->id;
                $arreglo[2] = $fecha;
                $arreglo[3] = $messages[0]->chat_id;
                $arreglo[4] = $userchat->img_min;
                $arreglo[5] = $userchat->name." ".$userchat->last_name;
                return $arreglo;
                
            }else{
                
                return 0;
                
            }
            

        }else{

        

        
            $sql = "SELECT * FROM `messages` WHERE chat_id = '" . Input::get('id') . "' order by id desc limit 1";
            $messages = DB::select($sql);

            foreach ($messages as $message) {
                
                if ($my_id != $message->users_id) {
                    $you_id = $message->users_id;
                }else{
                    $you_id = $message->users_id1;
                }
            }
            
            $msj = new Messages;
            $msj->users_id = $my_id;
            $msj->users_id1 = $you_id;
            $msj->chat_id = Input::get('id');
            $msj->message = Input::get('mensaje');
            
            if($msj->save()){
                                
                $sql = "SELECT * FROM `messages` WHERE chat_id = '" . Input::get('id') . "' order by id desc limit 1";
                $messages = DB::select($sql);
                //return $messages;
                $arreglo = Array();
                $fecha = date("M d, Y H:i",strtotime($messages[0]->created_at));
                $arreglo[0] = $you_id;
                $arreglo[1] = $messages[0]->id;
                $arreglo[2] = $fecha;
                $arreglo[3] = $messages[0]->chat_id;
                $arreglo[4] = "qwe";
                $arreglo[5] = "asd";
                return $arreglo;
                
            }else{
                
                return 0;
                
            }
        }
                                    
    }
    
    public function newmessage() {
        
        $sql = "SELECT * FROM `messages` WHERE id = '" . Input::get('id') . "' order by id desc limit 1";
        $messages = DB::select($sql);

                //return $messages;
                $fecha = date("M d, Y H:i",strtotime($messages[0]->created_at));
                $arreglo = Array();
                $arreglo[0] = $fecha;
                $arreglo[1] = $messages[0]->message;
                $arreglo[2] = $messages[0]->chat_id;
                return $arreglo;
                                    
    }
    
    // public function nuevomsj() {

    //     $my_id = Auth::user()->id;
    //     $you_id = "";
    //     $sql = "SELECT * FROM `messages` WHERE chat_id = '" . Input::get('id') . "' order by id desc limit 1";
    //     $messages = DB::select($sql);

    //     foreach ($messages as $message) {
            
    //         if ($my_id != $message->users_id) {
    //             $you_id = $message->users_id;
    //         }else{
    //             $you_id = $message->users_id1;
    //         }
    //     }
            
    //         $msj = new Messages;
    //         $msj->users_id = $my_id;
    //         $msj->users_id1 = $you_id;
    //         $msj->chat_id = Input::get('id');
    //         $msj->message = Input::get('mensaje');
            
    //         if($msj->save()){
                                
    //             $sql = "SELECT * FROM `messages` WHERE chat_id = '" . Input::get('id') . "' order by id desc limit 1";
    //             $messages = DB::select($sql);
    //             return $messages;
                
    //         }else{
                
    //             return 0;
                
    //         }
                                    
    //     }
    
        
    // }


    public function search_users() {
        $my_id = Auth::user()->id;
        if(Input::get('palabra')){

            $q=Input::get('palabra');//se recibe la cadena que queremos buscar
            $sql = "select * from users where name like '%$q%' and status = 1";
            $sql_res = DB::select($sql);

            foreach ($sql_res as $row) {
               
                $sql2 = "SELECT chat_id, count(*) as dato FROM `messages` WHERE users_id = '$my_id'and users_id1 = '$row->id' or users_id = '$row->id' and users_id1 = '$my_id' group by chat_id";
                $chats = DB::select($sql2);
                
                   if(count($chats) == 0){
                        $function = "create_new_chat(".$row->id.")";
                    }else{
                        $function = "conversacion(".$chats[0]->chat_id.")";
                    }
                
                

            
                $id=$row->id;
                $nombre=$row->name." ".$row->last_name;
                $direc=$row->cell_phone;
                $foto=$row->img_min;
                echo '<a style="text-decoration:none;" onclick="'.$function.'">
                    <div class="display_box" align="left">
                    <div style="float:left; margin-right:6px;"><img src="'.$foto.'" width="60" height="60" /></div> 
                    <div style="margin-right:6px;"><b>'.$nombre.'</b></div>
                    <div style="margin-right:6px; font-size:14px;" class="desc">'.$direc.'</div></div>
                    </a>
                ';

            
            }

        }else{
            echo "2";
        }
    
        
    }
}
    
?>