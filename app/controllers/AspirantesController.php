<?php

/**
 * 
 */
class AspirantesController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    public function formatoFecha($fecha) {

        $transf = strtotime($fecha);
        $dia = date("d", $transf);
        $mes = date("F", $transf);
        if ($mes == "January")
            $mes = "Enero";
        if ($mes == "February")
            $mes = "Febrero";
        if ($mes == "March")
            $mes = "Marzo";
        if ($mes == "April")
            $mes = "Abril";
        if ($mes == "May")
            $mes = "Mayo";
        if ($mes == "June")
            $mes = "Junio";
        if ($mes == "July")
            $mes = "Julio";
        if ($mes == "August")
            $mes = "Agosto";
        if ($mes == "September")
            $mes = "Setiembre";
        if ($mes == "October")
            $mes = "Octubre";
        if ($mes == "November")
            $mes = "Noviembre";
        if ($mes == "December")
            $mes = "Diciembre";
        $ano = date("Y", $transf);

        return $dia . " de " . $mes . " de " . $ano;
    }

    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }

    public function getInformacion() {

        $inscrito = AspirantsInformations::where('users_id', Auth::user()->id)->get();
        $aspirantes = AspirantsProspects::where('id', $inscrito[0]->aspirants_prospects_id)->get();
        $work = AspirantsWorkings::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $family = AspirantsFamilies::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $studies = AspirantsStudies::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $resultados = AspirantsResults::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $estados_resultados = AspirantsStatusResults::all();
        $estados = AspirantsStatusProspects::all();
        $programas = AspirantsPrograms::all();
        $jornadas = AspirantsTimes::all();
        $periodos = AspirantsPeriods::all();
        $tipos_documentos = AspirantsTypeDocuments::all();
        $formacion = AspirantsTrainings::all();
        $estado_civil = AspirantsMaritalStatuses::all();
        $generos = AspirantsGenders::all();
        $locations = AspirantsLocations::all();
        $fuentes = AspirantsSources::all();
        $documentos = AspirantsDocuments::where('status', 1)->get();
        $traces = AspirantsTraces::where('aspirants_prospects_id', Input::get('id'))->orderBy('id', 'desc')->get();
        $aspirants_documents = DB::table('aspirants_informations')->where('aspirants_prospects_id', Input::get('id'))->get();
        $documentosasp = DB::table('aspirants_documents_has_aspirants_informations')->where('aspirants_informations_id', $inscrito[0]->id)->get();

        $sql = "SELECT *
                                        FROM aspirants_allocation_tests
                                        JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)
                                        WHERE aspirants_allocation_tests.aspirants_informations_id = '" . $inscrito[0]->id . "' ORDER BY aspirants_type_tests.id";

        $citaciones = DB::select($sql);

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.informacion')
                        ->with('aspirantes', $aspirantes)
                        ->with('inscrito', $inscrito)
                        ->with('work', $work)
                        ->with('family', $family)
                        ->with('studies', $studies)
                        ->with('resultados', $resultados)
                        ->with('citaciones', $citaciones)
                        ->with('estados', $estados)
                        ->with('estados_resultados', $estados_resultados)
                        ->with('traces', $traces)
                        ->with('documentos', $documentos)
                        ->with('documentosasp', $documentosasp)
                        ->with('id_documentos', $aspirants_documents)
                        ->with('programas', $programas)
                        ->with('jornadas', $jornadas)
                        ->with('periodos', $periodos)
                        ->with('tipos_documentos', $tipos_documentos)
                        ->with('formacion', $formacion)
                        ->with('estado_civil', $estado_civil)
                        ->with('generos', $generos)
                        ->with('locations', $locations)
                        ->with('fuentes', $fuentes)
                        ->with('submenu_activo', 'Información')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getGaleriasferias() {

        //consultamos las imagenes
        $imagenes = DB::table('aspirants_school_traces_photos')->where('aspirants_school_fairs_id', Input::get('id'))->get();


        //construimos la vista
        return View::make('dashboard.index')
                        //->with('user', $user)
                        ->with('imagenes', $imagenes)
                        ->with('idgal', Input::get('id'))
                        ->with('actual', "none")
                        ->with('content', 'container')
                        ->with('container', 'dashboard.aspirantes.galerias')
                        ->with('menu_activo', 'Aspirantes')
                        ->with('submenu_activo', 'informacion');
    }

    public function getRemoveadmin() {

        $id = Input::get('id');
        $tipo = Input::get('tipo');

        switch ($tipo) {

            case "1":

                DB::table('aspirants_inscription_methods')->where('id', $id)->update(array('status' => 0));

                break;

            case "2":

                DB::table('aspirants_campaigns')->where('id', $id)->update(array('status' => 0));

                break;

            case "3":

                DB::table('aspirants_sources')->where('id', $id)->update(array('status' => 0));

                break;

            case "4":

                DB::table('aspirants_status_prospects')->where('id', $id)->update(array('state' => 0));

                break;

            case "5":

                DB::table('aspirants_documents')->where('id', $id)->update(array('status' => 0));

                break;

            case "6":

                DB::table('aspirants_convocations')->where('id', $id)->update(array('status' => 0));

                break;

            case "7":

                DB::table('aspirants_classrooms')->where('id', $id)->update(array('status' => 0));

                break;

            case "8":

                DB::table('aspirants_convocations_has_aspirants_classrooms')->where('id', $id)->update(array('status' => 0));

                break;
        }
    }

    //funcion para consultar los datos del proveedor en cada cotizacion segun el NIT    
    public function getImprimircitacion() {

        include('assets/fpdf17/fpdf.php');

        $id = Input::get('id');
        $inscrito = AspirantsInformations::where('id', $id)->get();
        $bandera = 0;

        $sql = "SELECT *
                                        FROM aspirants_allocation_tests
                                        JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                        JOIN aspirants_periods ON (aspirants_convocations.aspirants_periods_id = aspirants_periods.id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)
                                        WHERE aspirants_allocation_tests.aspirants_informations_id = '" . $id . "' ORDER BY aspirants_type_tests.id";

        $citaciones = DB::select($sql);

        if ($citaciones) {

            $programa = $inscrito[0]->AspirantsPrograms->name;
            if (strlen($inscrito[0]->AspirantsPrograms->name) > 36) {
                $programa = $inscrito[0]->AspirantsPrograms->acronyms;
            }

            $pdf = new FPDF();
            $pdf->AddPage();
            $pdf->SetFont('Arial', '', 12);
            $pdf->SetXY(55, 139);
            $pdf->Write(5, "Apreciado(a), ");
            $pdf->SetFont('Arial', 'B', 13);
            $pdf->SetXY(82, 139);
            $pdf->Write(5, utf8_decode(strtoupper($inscrito[0]->first_name . " " . $inscrito[0]->middle_name . " " . $inscrito[0]->last_name . " " . $inscrito[0]->last_name2)));
            $pdf->SetXY(52, 150);
            $pdf->SetFont('Arial', '', 12);
            $pdf->Write(5, utf8_decode("Bienvenido(a) al proceso de Admisión para ingreso a la"));
            $pdf->SetXY(68, 155);
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Write(5, utf8_decode("CORPORACIÓN EDUCATIVA"));
            $pdf->SetXY(80, 160);
            $pdf->SetFont('Arial', 'B', 14);
            $pdf->Write(5, "INDOAMERICANA");
            $pdf->SetXY(48, 170);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, "Programa:              " . utf8_decode($programa));
            $pdf->SetXY(48, 175);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, "Jornada:                 " . utf8_decode($inscrito[0]->AspirantsTimes->day_trip));
            $pdf->SetXY(48, 180);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, utf8_decode("Fecha Citación:"));
            $pdf->SetXY(81, 180);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Write(5, $this->formatoFecha($citaciones[0]->date));
            $pdf->SetXY(48, 185);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, utf8_decode("Hora:"));
            $pdf->SetXY(81, 185);
            $pdf->SetFont('Arial', 'B', 11);
            $pdf->Write(5, $citaciones[0]->time);
            $pdf->SetXY(48, 190);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, "_______________________________________________________");
            $pdf->SetXY(68, 195);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, $citaciones[0]->convocation . " - " . $citaciones[0]->period);
            $pdf->SetXY(80, 200);
            $pdf->SetFont('Arial', '', 11);
            $pdf->Write(5, utf8_decode("Fecha de impresión: " . date("d/m/Y")));
            $pdf->Output();

            exit;

//            $html = View::make('dashboard.aspirantes.citaciones.pdfcitacion')
//                    ->with('inscrito', $inscrito)
//                    ->with('citaciones', $citaciones);
//            PDF::load($html, 'A4', 'landscape')->show();
        } else {

            $sql = "SELECT aspirants_convocations_has_aspirants_classrooms.id, aspirants_classrooms.quota
                                        FROM aspirants_convocations_has_aspirants_classrooms
                                        JOIN aspirants_classrooms_has_aspirants_programs ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_classrooms_has_aspirants_programs.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        WHERE aspirants_classrooms_has_aspirants_programs.aspirants_programs_id = '" . $inscrito[0]->AspirantsPrograms->id . "' AND aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id=1";

            $datos = DB::select($sql);

            if ($datos) {

                foreach ($datos as $dat) {


                    $sql = "SELECT aspirants_allocation_tests.id, COUNT( * ) AS dato
                                        FROM aspirants_allocation_tests
                                        WHERE aspirants_convocations_has_aspirants_classrooms_id =" . $dat->id;

                    $registros = DB::select($sql);

                    if ($bandera == 0 && ($registros[0]->dato < $dat->quota)) {

                        $tests = new AspirantsAllocationTests();

                        $tests->aspirants_informations_id = $id;
                        $tests->aspirants_convocations_has_aspirants_classrooms_id = $dat->id;

                        if ($tests->save()) {
                            $bandera = 1;
                        }
                    }
                }
            } else {
                echo "no hay salones con el programa " . $inscrito[0]->AspirantsPrograms->name;
                exit;
            }

            $bandera = 0;

            $sql = "SELECT aspirants_convocations_has_aspirants_classrooms.id, aspirants_classrooms.quota
                                        FROM aspirants_convocations_has_aspirants_classrooms
                                        JOIN aspirants_classrooms_has_aspirants_programs ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_classrooms_has_aspirants_programs.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        WHERE aspirants_classrooms_has_aspirants_programs.aspirants_programs_id = '" . $inscrito[0]->AspirantsPrograms->id . "' AND aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id=2";

            $datos = DB::select($sql);

            if ($datos) {

                foreach ($datos as $dat) {

                    $sql = "SELECT aspirants_allocation_tests.id, COUNT( * ) AS dato
                                        FROM aspirants_allocation_tests
                                        WHERE aspirants_convocations_has_aspirants_classrooms_id =" . $dat->id;

                    $registros = DB::select($sql);

                    if ($bandera == 0 && ($registros[0]->dato < $dat->quota)) {

                        $tests = new AspirantsAllocationTests();

                        $tests->aspirants_informations_id = $id;
                        $tests->aspirants_convocations_has_aspirants_classrooms_id = $dat->id;

                        if ($tests->save()) {
                            $bandera = 1;
                        }
                    }
                }

                $sql = "SELECT *
                                        FROM aspirants_allocation_tests
                                        JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)
                                        WHERE aspirants_allocation_tests.aspirants_informations_id = '" . $id . "' ORDER BY aspirants_type_tests.id";

                $citaciones = DB::select($sql);

                $pdf = new FPDF();
                $pdf->AddPage();
                $pdf->SetFont('Arial', '', 16);
                $pdf->SetXY(48, 70);
                $pdf->Write(5, "Apreciado(a), ");
                $pdf->SetXY(48, 70);
                $pdf->SetFont('Arial', '', 16);
                $pdf->Write(5, "Bienvenido(a) al proceso de Adminsion para ingreso a la");
                $pdf->SetXY(48, 70);
                $pdf->SetFont('Arial', '', 18);
                $pdf->Write(5, "CORPORACION EDUCATIVA");
                $pdf->SetXY(48, 70);
                $pdf->SetFont('Arial', '', 18);
                $pdf->Write(5, "INDOAMERICANA");
                $pdf->Output();

//                $html = View::make('dashboard.aspirantes.citaciones.pdfcitacion')
//                        ->with('inscrito', $inscrito)
//                        ->with('citaciones', $citaciones);
//                PDF::load($html, 'A4', 'landscape')->show();
            } else {
                echo "no hay salones con el programa " . $inscrito[0]->AspirantsPrograms->name;
                exit;
            }
        }
    }

    //funcion para consultar los datos del proveedor en cada cotizacion segun el NIT
    public function getEliminarcitacion() {

        $id = Input::get('id');
        $inscrito = AspirantsInformations::where('id', $id)->get();
        $bandera = 0;

        $sql = "SELECT *
                                                FROM aspirants_allocation_tests
                                                JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id)
                                                JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                                JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                                JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)
                                                WHERE aspirants_allocation_tests.aspirants_informations_id = '" . $id . "' ORDER BY aspirants_type_tests.id";

        $citaciones = DB::select($sql);

        foreach ($citaciones as $cit) {
            $filasAfectadas = AspirantsAllocationTests::where('aspirants_informations_id', '=', $cit->aspirants_informations_id)->delete();
        }
    }

    //funcion para generar pdf de citas
    public function getGenerarpdfcit() {

        $sql = "SELECT aspirants_classrooms.classroom, aspirants_convocations.convocation, aspirants_convocations.date, aspirants_convocations_has_aspirants_classrooms.id, aspirants_type_tests.type
                                FROM aspirants_classrooms
				JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)                           JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)                
                                WHERE aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = '" . Input::get('id') . "'";

        $salones = DB::select($sql);

        $sql = "SELECT aspirants_programs.acronyms, aspirants_informations.id,aspirants_informations.first_name,aspirants_informations.last_name,aspirants_informations.middle_name,aspirants_informations.last_name2,aspirants_informations.document,aspirants_convocations_has_aspirants_classrooms.id as conv
				FROM aspirants_informations
				JOIN aspirants_allocation_tests ON (aspirants_informations.id = aspirants_allocation_tests.aspirants_informations_id)
                                JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id = aspirants_convocations_has_aspirants_classrooms.id)
                                JOIN aspirants_programs ON (aspirants_informations.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = '" . Input::get('id') . "'";

        $datos = DB::select($sql);

        $html = View::make('dashboard.aspirantes.citaciones.pdfcitaciones')
                ->with('datos', $datos)
                ->with('salones', $salones);
        PDF::load($html, 'A4', 'portrait')->show();
    }

    //funcion para generar pdf de informes
    public function getGenerarpdfasp() {

        $id = Input::get('id');
        $estado = Input::get('estado');
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);
        $cont = 0;
        switch ($id) {

            case "1":

                if ($estado != 0) {

                    $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
                                        FROM aspirants_traces
                                        JOIN users ON (aspirants_traces.users_id = users.id)
                                        JOIN aspirants_prospects ON (aspirants_prospects.id = aspirants_traces.aspirants_prospects_id)
                                        WHERE aspirants_traces.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                        AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
                                        GROUP BY users_id
                                        ORDER BY dato DESC";
                } else {

                    $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
                                        FROM aspirants_traces
                                        JOIN users ON (aspirants_traces.users_id = users.id)
                                        WHERE aspirants_traces.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                        GROUP BY users_id
                                        ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfseguimientos')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "2":

                if ($estado != 0) {

                    $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
                                        FROM aspirants_traces
                                        JOIN users ON (aspirants_traces.users_id = users.id)
                                        WHERE aspirants_traces.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                        GROUP BY users_id
                                        ORDER BY dato DESC";
                } else {

                    $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
                                        FROM aspirants_traces
                                        JOIN users ON (aspirants_traces.users_id = users.id)
                                        WHERE aspirants_traces.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                        GROUP BY users_id
                                        ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfseguimientos')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "3":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_programs_id, aspirants_programs.name, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_programs_id, aspirants_programs.name, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfprogramas')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "4":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfcampanas')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "5":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfestados')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "6":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_prospects.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdffuentes')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "7":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfjornadas')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "8":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";
                }

                $sql = "SELECT id, YEAR(CURDATE())-YEAR(birthday) + IF(DATE_FORMAT(CURDATE(),'%m-%d') > DATE_FORMAT(birthday,'%m-%d'), 0, -1) AS edad_actual
                            FROM aspirants_informations
                            WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'";

                $datos = DB::select($sql);

                $rango1 = 0;
                $rango2 = 0;
                $rango3 = 0;
                $rango4 = 0;

                foreach ($datos as $dat) {

                    if ($dat->edad_actual < 18) {
                        $rango1 = $rango1 + 1;
                    } elseif ($dat->edad_actual > 17 && $dat->edad_actual < 26) {
                        $rango2 = $rango2 + 1;
                    } elseif ($dat->edad_actual > 25 && $dat->edad_actual < 31) {
                        $rango3 = $rango3 + 1;
                    } elseif ($dat->edad_actual > 30) {
                        $rango4 = $rango4 + 1;
                    }
                }


                $html = View::make('dashboard.aspirantes.informes.pdfedades')
                        ->with('rango1', $rango1)
                        ->with('rango2', $rango2)
                        ->with('rango3', $rango3)
                        ->with('rango4', $rango4)
                        ->with('cont', count($datos));
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "9":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfgenero')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "10":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdfciudad')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;

            case "11":

                if ($estado != 0) {

                    $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                AND aspirants_informations.aspirants_status_managements_id = '" . $estado . "'
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC";
                } else {

                    $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC";
                }

                $datos = DB::select($sql);

                $html = View::make('dashboard.aspirantes.informes.pdflocalidad')
                        ->with('datos', $datos)
                        ->with('cont', $cont);
                PDF::load($html, 'A4', 'portrait')->show();

                break;
        }
    }

    //funcion para mostrar vista de Gestion de Emails
    public function getGestionemail() {

        $periodos = DB::table('aspirants_periods')->get();
        $programas = AspirantsPrograms::all();
        $convocatorias = AspirantsConvocations::all();
        $estados = AspirantsStatusManagements::all();

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.gestionemail')
                        ->with('convocatorias', $convocatorias)
                        ->with('periodos', $periodos)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('submenu_activo', 'Gestión Emails')
                        ->with('menu_activo', 'Aspirantes');
    }

    //funcion para mostrar vista de Gestion de Emails
    public function postEnviaremail() {

        $convocatoria = Input::get('convocatoria_id');
        $programa = Input::get('programa_id');
        $estado = Input::get('estado_id');
        $asunto = Input::get('asunto');
        $mensaje = Input::get('mensaje');
        $verificacion = 0;

        if (isset($_POST['programa_id']) && !empty($_POST['programa_id'])) {
            $campo = "aspirants_programs_id";
            $signo = '=';
            $valor = $programa;
        } else {
            $campo = "id";
            $signo = '<>';
            $valor = '-1';
        }

        if (isset($_POST['estado_id']) && !empty($_POST['estado_id'])) {
            $campo2 = "aspirants_status_managements_id";
            $signo2 = '=';
            $valor2 = $estado;
        } else {
            $campo2 = "id";
            $signo2 = '<>';
            $valor2 = '-2';
        }

        $correos = AspirantsProspects::where('aspirants_convocations_id', $convocatoria)
                ->where($campo, $signo, $valor)
                ->where($campo2, $signo2, $valor2)
                ->get();

        $fromEmail = "Indoamericana@indoamericana.edu.co";
        $fromName = "Indoamericana";


        if (Input::file('file_email')) {

            $destinationPath = 'assets/img/adjuntos_comercial/';

            $filename = date("YmdHis") . "_" . Input::file('file_email')->getClientOriginalName();

            $uploadSuccess = Input::file('file_email')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                $name_original = Input::file('file_email')->getClientOriginalName();

                foreach ($correos as $cor) {
                    Mail::send('emails.emailcomercial', array('mensaje' => $mensaje), function ($message) use ($fromName, $fromEmail, $cor, $asunto, $name_original, $destinationPath, $filename) {
                        $message->subject($asunto);
                        $message->to($cor->email);
                        $message->from($fromEmail, $fromName);
                        $message->attach("http://192.168.0.18/indo/public/" . $destinationPath . $filename, array('as' => $name_original));
                    });
                    return Redirect::to('aspirantes/gestionemail');
                }
            } else {
                echo "no se pudo subir el archivo";
            }
        } else {

            foreach ($correos as $cor) {
                Mail::send('emails.emailcomercial', array('mensaje' => $mensaje), function ($message) use ($fromName, $fromEmail, $cor, $asunto) {
                    $message->subject($asunto);
                    $message->to($cor->email);
                    $message->from($fromEmail, $fromName);
                });
                return Redirect::to('aspirantes/gestionemail');
            }
        }
    }

    public function getBuscar() {

        $emailasp = Input::get('emailasp');
        $nameasp = Input::get('nameasp');
        $nodoc = Input::get('nodoc');
        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = '<>';
            $signo = '-1';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('name', 'LIKE', '%' . $nameasp . '%')
                ->where('email', 'LIKE', '%' . $emailasp . '%')
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('document', 'LIKE', '%' . $nodoc . '%')
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();

            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos > 0) {
                $bandera2 = $bandera->Users->name . " " . $bandera->Users->last_name;
                $bandera = $bandera->Users->img_min;
            } else {
                $bandera = "assets/img/avatar_small/default.jpg";
                $bandera2 = "Sin Usuario";
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["img_min"] = $bandera;
            $data[$i]["user_seg"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        $campanas = AspirantsCampaigns::all();
        $metodos = AspirantsInscriptionMethods::all();

        if (isset($_GET['nombre'])) {
            return View::make('dashboard.aspirantes.tabla_buscar')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('metodos', $metodos);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.buscar')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('metodos', $metodos)
                            ->with('submenu_activo', 'Prospectos')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getBuscador() {

        $emailasp = Input::get('emailasp');
        $nameasp = Input::get('nameasp');
        $nodoc = Input::get('nodoc');
        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = '<>';
            $signo = '-1';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('name', 'LIKE', '%' . $nameasp . '%')
                ->where('email', 'LIKE', '%' . $emailasp . '%')
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('document', 'LIKE', '%' . $nodoc . '%')
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();

            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos > 0) {
                $bandera2 = $bandera->Users->name . " " . $bandera->Users->last_name;
                $bandera = $bandera->Users->img_min;
            } else {
                $bandera = "assets/img/avatar_small/default.jpg";
                $bandera2 = "Sin Usuario";
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["img_min"] = $bandera;
            $data[$i]["user_seg"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        $campanas = AspirantsCampaigns::all();
        $metodos = AspirantsInscriptionMethods::all();

        if (isset($_GET['nombre'])) {
            return View::make('dashboard.aspirantes.tabla_buscar')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('metodos', $metodos);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.buscador')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('metodos', $metodos)
                            ->with('submenu_activo', 'Inscritos')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getProspectos() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }

        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->whereNotIn('aspirants_status_prospects_id', [6, 7, 11, 18, 3])
                ->whereNotIn('aspirants_status_managements_id', [3, 4, 5, 6])
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();

            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos > 0) {
                $bandera2 = $bandera->Users->name . " " . $bandera->Users->last_name;
                $bandera = $bandera->Users->img_min;
            } else {
                $bandera = "assets/img/avatar_small/default.jpg";
                $bandera2 = "Sin Usuario";
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["img_min"] = $bandera;
            $data[$i]["user_seg"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        $campanas = AspirantsCampaigns::all();
        $metodos = AspirantsInscriptionMethods::all();
        $sources = AspirantsSources::all();
        $convocatorias = AspirantsConvocations::all();

        if (isset($_GET['nombre'])) {
            return View::make('dashboard.aspirantes.tabla_prospectos')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('convocatorias', $convocatorias)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('sources', $sources)
                            ->with('metodos', $metodos);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.prospectos')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('convocatorias', $convocatorias)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('metodos', $metodos)
                            ->with('sources', $sources)
                            ->with('submenu_activo', 'Prospectos')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getInscritos() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }

        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('status_inscription', 1)
                ->where('aspirants_status_managements_id', 3)
                ->get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.inscritos')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Prospectos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getRadicados() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('aspirants_status_managements_id', 4)
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.radicados')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Inscritos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getRadico() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('aspirants_status_managements_id', 4)
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.radico')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Prospectos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getExamen() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('aspirants_status_managements_id', 5)
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.examen')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Inscritos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getExamenf() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');


        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('aspirants_status_managements_id', 5)
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.examenf')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Prospectos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getPago() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');

        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('aspirants_status_managements_id', 6)
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.pago')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Inscritos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getPagos() {

        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        $estados = DB::table('aspirants_status_prospects')
                ->join('aspirants_prospects', 'aspirants_status_prospects.id', '=', 'aspirants_status_prospects_id')
                ->select('aspirants_status_prospects.id', 'aspirants_status_prospects.status')
                ->distinct()
                ->get('');

        $programas = DB::table('aspirants_programs')
                ->join('aspirants_prospects', 'aspirants_programs.id', '=', 'aspirants_prospects.aspirants_programs_id')
                ->select('aspirants_programs.id', 'aspirants_programs.name')
                ->distinct()
                ->get('');

        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $aspirantes = AspirantsProspects::where('id', 'LIKE', '%' . $codigo . '%')
                ->where('aspirants_status_prospects_id', $var, $signo)
                ->where('aspirants_programs_id', 'LIKE', '%' . $program . '%')
                ->where('aspirants_status_managements_id', 6)
                ->get();
        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
                $bandera2 = "assets/img/avatar_small/default.jpg";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = $aspirantes[$i]->AspirantsStatusProspects->status;
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';

            if ($aspirantes[$i]->aspirants_status_prospects_id == 21) {
                $data[$i]['row_color'] = 'green';
            } elseif ($seguimientos > 0) {
                $data[$i]['row_color'] = 'yellow';
            }
        }
        $programas = AspirantsPrograms::all();
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.pagos')
                        ->with('aspirantes', $data)
                        ->with('programas', $programas)
                        ->with('estados', $estados)
                        ->with('programas', $programas)
                        ->with('submenu_activo', 'Prospectos')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getPendientes() {

        //funciones para filtros
        $applicant = Input::get('applicant');
        $documento = Input::get('documento');

        if (isset($_GET['applicant']) && !empty($_GET['applicant'])) {
            $campo_user = 'aspirants_prospects.id';
            $var_user = '=';
            $signo_user = $applicant;
        } else {
            $campo_user = 'aspirants_prospects.id';
            $var_user = '<>';
            $signo_user = '-1';
        }

        if (isset($_GET['documento']) && !empty($_GET['documento'])) {
            $campo_doc = 'aspirants_documents.id';
            $var_doc = '=';
            $signo_doc = $documento;
        } else {
            $campo_doc = 'aspirants_documents.id';
            $var_doc = '<>';
            $signo_doc = '-1';
        }

        if (isset($_GET['f_pendiente']) && !empty($_GET['f_pendiente'])) {

            $f_pendiente1 = Input::get('f_pendiente');
            $f_pendiente2 = Input::get('f_pendiente');
        } else {
            $f_pendiente1 = "0000-00-00";
            $f_pendiente2 = "3000-01-01";
        }

        $aspirantes = DB::table('aspirants_documents_has_aspirants_informations')
                ->join('aspirants_informations', 'aspirants_informations.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_informations_id')
                ->join('aspirants_prospects', 'aspirants_prospects.id', '=', 'aspirants_informations.aspirants_prospects_id')
                ->join('aspirants_documents', 'aspirants_documents.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id')
                ->select('aspirants_documents_has_aspirants_informations.date_commitment', 'aspirants_documents.document as documento', 'aspirants_prospects.name', 'aspirants_prospects.email', 'aspirants_prospects.phone', 'aspirants_prospects.phone2', 'aspirants_prospects.city', 'aspirants_prospects.document', 'aspirants_prospects.id', 'aspirants_prospects.created_at', 'aspirants_informations.id as idinformation')
                ->where('aspirants_documents_has_aspirants_informations.status', '<>', '2')
                ->where($campo_user, $var_user, $signo_user)
                ->where($campo_doc, $var_doc, $signo_doc)
                ->whereBetween('aspirants_documents_has_aspirants_informations.date_commitment', array($f_pendiente1, $f_pendiente2))
                ->orderBy('aspirants_documents_has_aspirants_informations.date_commitment')
                ->get('');

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();
        for ($i = 0; $i < count($aspirantes); $i++) {
            $fecha = date($aspirantes[$i]->created_at);
            $data[$i]["id"] = 'ASP' . str_pad($aspirantes[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["user"] = $aspirantes[$i]->name . " " . $aspirantes[$i]->last_name;
            $data[$i]["email"] = $aspirantes[$i]->email;
            $data[$i]["phone"] = $aspirantes[$i]->phone;
            $data[$i]["city"] = $aspirantes[$i]->city;
            $data[$i]["status_view"] = $aspirantes[$i]->status_view;

//            $sql = "SELECT*FROM aspirants_documents_has_aspirants_informations
//                    JOIN aspirants_documents ON (aspirants_documents.id = aspirants_documents_has_aspirants_informations.aspirants_documents_id)
//                                        WHERE aspirants_informations_id = '".$aspirantes[$i]->idinformation."' AND (aspirants_documents_has_aspirants_informations.status = '3' OR aspirants_documents_has_aspirants_informations.status = '0') ORDER BY date_commitment";
//
//            $fechas = DB::select($sql);
//
//            if ($fechas) {
//                $data[$i]["fecha_pendiente"] = $fechas[0]->date_commitment;
//            }else{
//                $data[$i]["fecha_pendiente"] = "No hay fecha programada";
//            }


            $data[$i]["fecha_pendiente"] = $aspirantes[$i]->date_commitment;

            $data[$i]["documento"] = $aspirantes[$i]->documento;

            $variable2 = "";
            $nuevo_asp = AspirantsTraces::where('aspirants_prospects_id', $aspirantes[$i]->id)->get();
            foreach ($nuevo_asp as $key) {
                $texto = $key->Users->name . " " . $key->Users->last_name . "---" . $key->coment . "<br>";
                $variable2 = $variable2 . $texto;
            }

            $data[$i]["traces"] = $variable2;

            $bandera = $nuevo_asp->last();
            $seguimientos = DB::table('aspirants_traces')->where('aspirants_prospects_id', $aspirantes[$i]->id)->count();
            if ($seguimientos == 0) {
                $bandera = "Sin Usuario";
            } else {
                $bandera2 = $bandera->Users->img_min;
                $bandera = $bandera->Users->name . " " . $bandera->Users->last_name;
            }

            $data[$i]["state"] = "sin estado";
            $data[$i]["seg"] = $seguimientos;

            $data[$i]["user_seg"] = $bandera;
            $data[$i]["img_min"] = $bandera2;

            $data[$i]["created_at"] = $fecha;

            $data[$i]["hid"] = $aspirantes[$i]->id;
            $data[$i]['tiempo'] = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['row_color'] = '';
        }

        $nombres = DB::table('aspirants_documents_has_aspirants_informations')
                ->join('aspirants_informations', 'aspirants_informations.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_informations_id')
                ->join('aspirants_prospects', 'aspirants_prospects.id', '=', 'aspirants_informations.aspirants_prospects_id')
                ->join('aspirants_documents', 'aspirants_documents.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id')
                ->select('aspirants_prospects.name', 'aspirants_prospects.id')
                ->where('aspirants_documents_has_aspirants_informations.status', '<>', '2')
                ->distinct()
                ->orderBy('aspirants_prospects.name')
                ->get('');

        $documentos = DB::table('aspirants_documents_has_aspirants_informations')
                ->join('aspirants_informations', 'aspirants_informations.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_informations_id')
                ->join('aspirants_prospects', 'aspirants_prospects.id', '=', 'aspirants_informations.aspirants_prospects_id')
                ->join('aspirants_documents', 'aspirants_documents.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id')
                ->select('aspirants_documents.document', 'aspirants_documents.id')
                ->where('aspirants_documents_has_aspirants_informations.status', '<>', '2')
                ->distinct()
                ->orderBy('aspirants_documents.id')
                ->get('');

        if (isset($_GET['nombre'])) {
            return View::make('dashboard.aspirantes.tabla_pendientes')
                            ->with('aspirantes', $data);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.pendientes')
                            ->with('aspirantes', $data)
                            ->with('nombres', $nombres)
                            ->with('documentos', $documentos)
                            ->with('submenu_activo', 'Prospectos')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getAdministrador() {

        $campanas = DB::table('aspirants_campaigns')->get();
        $estados = DB::table('aspirants_status_prospects')->get();
        $metodos = DB::table('aspirants_inscription_methods')->get();
        $fuentes = DB::table('aspirants_sources')->get();
        $periodos = DB::table('aspirants_periods')->get();
        $programas = AspirantsPrograms::all();
        $documentos = AspirantsDocuments::where('status', 1)->get();
        $convocatorias = AspirantsConvocations::all();
        $salones = DB::table('aspirants_classrooms')->get();
        $tipos = DB::table('aspirants_type_tests')->get();
        $asignaciones = DB::table('aspirants_convocations_has_aspirants_classrooms')
                ->join('aspirants_convocations', 'aspirants_convocations.id', '=', 'aspirants_convocations_id')
                ->join('aspirants_classrooms', 'aspirants_classrooms.id', '=', 'aspirants_classrooms_id')
                ->join('aspirants_type_tests', 'aspirants_type_tests.id', '=', 'aspirants_type_tests_id')
                ->select('aspirants_convocations.convocation', 'aspirants_convocations.date', 'aspirants_classrooms.classroom', 'aspirants_classrooms.quota', 'aspirants_type_tests.type', 'aspirants_convocations_has_aspirants_classrooms.time', 'aspirants_convocations_has_aspirants_classrooms.id')
                ->distinct()
                ->get('');

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.Administrador')
                        ->with('campanas', $campanas)
                        ->with('estados', $estados)
                        ->with('metodos', $metodos)
                        ->with('fuentes', $fuentes)
                        ->with('periodos', $periodos)
                        ->with('documentos', $documentos)
                        ->with('convocatorias', $convocatorias)
                        ->with('salones', $salones)
                        ->with('asignaciones', $asignaciones)
                        ->with('tipos', $tipos)
                        ->with('submenu_activo', 'Administrador')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getRefreshconvocatorias() {

        $asignaciones = DB::table('aspirants_convocations_has_aspirants_classrooms')
                ->join('aspirants_convocations', 'aspirants_convocations.id', '=', 'aspirants_convocations_id')
                ->join('aspirants_classrooms', 'aspirants_classrooms.id', '=', 'aspirants_classrooms_id')
                ->join('aspirants_type_tests', 'aspirants_type_tests.id', '=', 'aspirants_type_tests_id')
                ->select('aspirants_convocations.convocation', 'aspirants_convocations.date', 'aspirants_classrooms.classroom', 'aspirants_classrooms.quota', 'aspirants_type_tests.type', 'aspirants_convocations_has_aspirants_classrooms.time', 'aspirants_convocations_has_aspirants_classrooms.id')
                ->where('aspirants_convocations.id', Input::get('convocatoria'))
                ->distinct()
                ->get('');

        $retorno = "";


        if ($asignaciones) {
            foreach ($asignaciones as $asig) {

                $retorno .='<tr style="text-align:center;">
                                                    
                                                    <td>' . $asig->id . '</td>
                                                    <td>' . $asig->convocation . '</td>
                                                    <td>' . $asig->classroom . '</td>
                                                    <td>' . $asig->type . '</td>
                                                    <td>' . $asig->date . '</td>
                                                    <td>' . $asig->time . '</td>
                                                    
                                                    <td>
                                                        <a class=" btn btn-default" data-target="#ajax1" id="' . $asig->id . '" data-toggle="modal" onclick="cargar_programas_asignacion(' . $asig->id . ')"><i class="icon-eye-open"></i></a>
                                                        <a class="btn btn-danger"><i class="icon-remove"></i></a></td>
                                                </tr>';
            }
        } else {
            $retorno = 1;
        }

        return $retorno;
    }

    public function getConvocatorias() {

        $convocatorias = AspirantsConvocations::all();

        if (isset($_GET['nombre'])) {
            return View::make('dashboard.aspirantes.tabla_prospectos')
                            ->with('aspirantes', $data)
                            ->with('programas', $programas)
                            ->with('estados', $estados)
                            ->with('campanas', $campanas)
                            ->with('metodos', $metodos);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.convocatorias')
                            ->with('convocatorias', $convocatorias)
                            ->with('submenu_activo', 'Convocatorias')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getArchivos() {

        $carpetas = DB::table('aspirants_folders')->get();

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.archivos')
                        ->with('carpetas', $carpetas)
                        ->with('submenu_activo', 'Archivos')
                        ->with('menu_activo', 'Aspirantes');
    }

    /*
     * Funcion que recibe la peticion de documentacion.js en Javascript para cargar los documentos de Aspirantes por carpeta
     */

    public function getCargardocumentos() {
        //recibimos el id de la carpeta
        $id = Input::get('id');

        //consultamos los archivos de la carpeta
        $archivos = DB::table('aspirants_folders')
                ->join('aspirants_files', 'aspirants_files.aspirants_folders_id', '=', 'aspirants_folders.id')
                ->select('aspirants_folders.name_folder', 'aspirants_folders.dir_folder', 'aspirants_files.name_format as archivo', 'aspirants_files.file_format', 'aspirants_files.id')
                ->where('aspirants_folders.id', $id)
                ->get();

        //devolvemos los archivos en una lista
        $print = "";
        $print .= "<ul>";
        foreach ($archivos as $arc) {
            $print .= '<li dir="../assets/files/documentacion_aspirantes/' . $arc->dir_folder . '/' . $arc->file_format . '" id="' . $arc->id . '" class="fileasp"><i class="icon-file-text"></i>' . $arc->archivo . '</li>';
        }
        $print .="</ul>";
        echo $print;
    }

    public function getCreardir() {

        $name = Input::get('nombre');
        $name_folder = str_replace(" ", "_", $name);

        if (mkdir('assets/files/documentacion_aspirantes/' . $name_folder, 0777)) {

            $folder = new AspirantsFolders;

            $folder->name_folder = $name;
            $folder->dir_folder = $name_folder;

            if ($folder->save()) {
                $folder = AspirantsFolders::all();
                $array = array(
                    "id" => $folder->last()->id,
                    "name" => $name,
                );
                return $array;
            } else {
                echo "2";
            }
        }
    }

    public function postGuardararc() {

        if (Input::hasFile('arc')) {

            $folder = DB::table('aspirants_folders')
                    ->where('id', Input::get('carpetas'))
                    ->get();

            $filename = Input::file('arc')->getClientOriginalName();
            $name = Input::get('nombre');
            $id_folder = Input::get('carpetas');
            $destinationPath = 'assets/files/documentacion_aspirantes/' . $folder[0]->dir_folder . '/';

            $uploadSuccess = Input::file('arc')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                $file = new AspirantsFiles;

                $file->name_format = $name;
                $file->file_format = $filename;
                $file->aspirants_folders_id = $id_folder;

                $file->save();
                $url = Input::get('url') . "?embed=../" . $destinationPath . $filename . "#tab_1_3";
                return Redirect::away($url);
            }
        } else {
            echo "no";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');
        }
    }

    public function postGuardarimgasp() {

        if (Input::hasFile('img_galeria')) {

//            $folder = DB::table('aspirants_folders')
//                    ->where('id', Input::get('carpetas'))
//                    ->get();

            for ($i = 0; count($_FILES["img_galeria"]["name"]) > $i; $i++) {
                $filename = $_FILES["img_galeria"]["tmp_name"][$i];
                $name = $_FILES["img_galeria"]["name"][$i];
                $destinationPath = 'assets/img/ferias/' . $name;

                $uploadSuccess = move_uploaded_file($filename, $destinationPath);

                if ($uploadSuccess) {

                    $img = new AspirantsSchoolTracesPhotos;

                    $img->photo = $destinationPath;
                    $img->aspirants_school_fairs_id = Input::get('idgal');

                    $img->save();
                } else {
                    echo "error";
                }
            }

            $url = "http://192.168.0.18/indo/public/aspirantes/galeriasferias?id=" . Input::get('idgal');
            return Redirect::away($url);
        } else {
            echo "no";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');
        }
    }

    public function getActualizaraspview() {


        DB::table('aspirants_prospects')
                ->where('id', Input::get('id'))
                ->update(array('status_view' => 0,));
    }

    public function getDetalleprospecto() {

        $aspirantes = AspirantsProspects::where('id', Input::get('id'))->get();
        $estados = AspirantsStatusProspects::all();
        $programas = AspirantsPrograms::where('status', 1)->get();
        $traces = AspirantsTraces::where('aspirants_prospects_id', Input::get('id'))->orderBy('id', 'desc')->get();
        $convocatorias = AspirantsConvocations::all();

        DB::table('aspirants_prospects')
                ->where('id', Input::get('id'))
                ->update(array('status_view' => 1,));

        return View::make('dashboard.aspirantes.detalle_prospecto')
                        ->with('aspirantes', $aspirantes)
                        ->with('estados', $estados)
                        ->with('convocatorias', $convocatorias)
                        ->with('traces', $traces)
                        ->with('programas', $programas);
    }

    public function getDetalleinscrito() {

        $aspirantes = AspirantsProspects::where('id', Input::get('id'))->get();
        $inscrito = AspirantsInformations::where('aspirants_prospects_id', Input::get('id'))->get();
        $work = AspirantsWorkings::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $family = AspirantsFamilies::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $studies = AspirantsStudies::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $sociocultural = AspirantsSocioculturalInformations::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $resultados = AspirantsResults::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $convocatorias = AspirantsConvocations::all();
        $imagen = DB::table('users')
                ->join('aspirants_informations', 'users.id', '=', 'aspirants_informations.users_id')
                ->where('aspirants_informations.id', '=', $inscrito[0]->id)
                ->get();
        $estados_resultados = AspirantsStatusResults::all();
        $estados = AspirantsStatusProspects::all();
        $programas = AspirantsPrograms::where('status', 1)->get();
        $jornadas = AspirantsTimes::all();
        $periodos = AspirantsPeriods::all();
        $tipos_documentos = AspirantsTypeDocuments::all();
        $formacion = AspirantsTrainings::all();
        $estado_civil = AspirantsMaritalStatuses::all();
        $generos = AspirantsGenders::all();
        $locations = AspirantsLocations::all();
        $fuentes = AspirantsSources::all();
        $documentos = AspirantsDocuments::where('status', 1)->get();
        $traces = AspirantsTraces::where('aspirants_prospects_id', Input::get('id'))->orderBy('id', 'desc')->get();
        $aspirants_documents = DB::table('aspirants_informations')->where('aspirants_prospects_id', Input::get('id'))->get();
        $documentosasp = DB::table('aspirants_documents_has_aspirants_informations')
                        ->select('aspirants_documents.document', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id', 'aspirants_documents_has_aspirants_informations.date_commitment', 'aspirants_documents_has_aspirants_informations.status')
                        ->join('aspirants_documents', 'aspirants_documents.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id')
                        ->where('aspirants_documents_has_aspirants_informations.aspirants_informations_id', $inscrito[0]->id)->get();

        $sql = "SELECT *
                                        FROM aspirants_allocation_tests
                                        JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)
                                        WHERE aspirants_allocation_tests.aspirants_informations_id = '" . $inscrito[0]->id . "' ORDER BY aspirants_type_tests.id";

        $citaciones = DB::select($sql);


        return View::make('dashboard.aspirantes.detalle_inscritos')
                        ->with('aspirantes', $aspirantes)
                        ->with('inscrito', $inscrito)
                        ->with('work', $work)
                        ->with('imagen', $imagen)
                        ->with('family', $family)
                        ->with('convocatorias', $convocatorias)
                        ->with('sociocultural', $sociocultural)
                        ->with('studies', $studies)
                        ->with('resultados', $resultados)
                        ->with('citaciones', $citaciones)
                        ->with('estados', $estados)
                        ->with('estados_resultados', $estados_resultados)
                        ->with('traces', $traces)
                        ->with('documentos', $documentos)
                        ->with('documentosasp', $documentosasp)
                        ->with('id_documentos', $aspirants_documents)
                        ->with('programas', $programas)
                        ->with('jornadas', $jornadas)
                        ->with('periodos', $periodos)
                        ->with('tipos_documentos', $tipos_documentos)
                        ->with('formacion', $formacion)
                        ->with('estado_civil', $estado_civil)
                        ->with('generos', $generos)
                        ->with('locations', $locations)
                        ->with('fuentes', $fuentes);
    }

    public function getDetalleinscrito2() {

        $aspirantes = AspirantsProspects::where('id', Input::get('id'))->get();
        $inscrito = AspirantsInformations::where('aspirants_prospects_id', Input::get('id'))->get();
        $work = AspirantsWorkings::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $family = AspirantsFamilies::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $sociocultural = AspirantsSocioculturalInformations::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $studies = AspirantsStudies::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $resultados = AspirantsResults::where('aspirants_informations_id', $inscrito[0]->id)->get();
        $convocatorias = AspirantsConvocations::all();
        $imagen = DB::table('users')
                ->join('aspirants_informations', 'users.id', '=', 'aspirants_informations.users_id')
                ->where('aspirants_informations.id', '=', $inscrito[0]->id)
                ->get();
        $estados_resultados = AspirantsStatusResults::all();
        $estados = AspirantsStatusProspects::all();
        $programas = AspirantsPrograms::where('status', 1)->get();
        $jornadas = AspirantsTimes::all();
        $periodos = AspirantsPeriods::all();
        $tipos_documentos = AspirantsTypeDocuments::all();
        $formacion = AspirantsTrainings::all();
        $estado_civil = AspirantsMaritalStatuses::all();
        $generos = AspirantsGenders::all();
        $locations = AspirantsLocations::all();
        $fuentes = AspirantsSources::all();
        $documentos = AspirantsDocuments::where('status', 1)->get();
        $traces = AspirantsTraces::where('aspirants_prospects_id', Input::get('id'))->orderBy('id', 'desc')->get();
        $aspirants_documents = DB::table('aspirants_informations')->where('aspirants_prospects_id', Input::get('id'))->get();
        $documentosasp = DB::table('aspirants_documents_has_aspirants_informations')
                        ->select('aspirants_documents.document', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id', 'aspirants_documents_has_aspirants_informations.date_commitment', 'aspirants_documents_has_aspirants_informations.status')
                        ->join('aspirants_documents', 'aspirants_documents.id', '=', 'aspirants_documents_has_aspirants_informations.aspirants_documents_id')
                        ->where('aspirants_documents_has_aspirants_informations.aspirants_informations_id', $inscrito[0]->id)->get();

        $sql = "SELECT *
                                        FROM aspirants_allocation_tests
                                        JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.id = aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id)
                                        JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                        JOIN aspirants_classrooms ON (aspirants_convocations_has_aspirants_classrooms.aspirants_classrooms_id = aspirants_classrooms.id)
                                        JOIN aspirants_type_tests ON (aspirants_convocations_has_aspirants_classrooms.aspirants_type_tests_id = aspirants_type_tests.id)
                                        WHERE aspirants_allocation_tests.aspirants_informations_id = '" . $inscrito[0]->id . "' ORDER BY aspirants_type_tests.id";

        $citaciones = DB::select($sql);


        return View::make('dashboard.aspirantes.detalle_inscritos2')
                        ->with('aspirantes', $aspirantes)
                        ->with('inscrito', $inscrito)
                        ->with('work', $work)
                        ->with('imagen', $imagen)
                        ->with('family', $family)
                        ->with('convocatorias', $convocatorias)
                        ->with('studies', $studies)
                        ->with('sociocultural', $sociocultural)
                        ->with('resultados', $resultados)
                        ->with('citaciones', $citaciones)
                        ->with('estados', $estados)
                        ->with('estados_resultados', $estados_resultados)
                        ->with('traces', $traces)
                        ->with('documentos', $documentos)
                        ->with('documentosasp', $documentosasp)
                        ->with('id_documentos', $aspirants_documents)
                        ->with('programas', $programas)
                        ->with('jornadas', $jornadas)
                        ->with('periodos', $periodos)
                        ->with('tipos_documentos', $tipos_documentos)
                        ->with('formacion', $formacion)
                        ->with('estado_civil', $estado_civil)
                        ->with('generos', $generos)
                        ->with('locations', $locations)
                        ->with('fuentes', $fuentes);
    }

    public function postGuardardocumentos() {

        $id = Input::get('id_details');
        $status = Input::get('status');
        $fechas = Input::get('fechas');
        $id_informacion = Input::get('id_informacion');

        $i = '0';
        foreach ($id as $key) {

            DB::table('aspirants_documents_has_aspirants_informations')
                    ->where('aspirants_documents_id', $key)
                    ->where('aspirants_informations_id', $id_informacion)
                    ->update(array('date_commitment' => $fechas[$i], 'status' => $status[$i]));

//            DB::table('aspirants_documents_has_aspirants_informations')->insert(array(
//                array('aspirants_documents_id' => $key, 'aspirants_informations_id' => $id_informacion, 'date_commitment' =>$fechas[$i], 'status' => $status[$i]),
//                
//            ));

            $i++;
        }
        echo "1";
    }

    public function getGuardarcambios() {

        $id = Input::get('id');
        $observation_edit = Input::get('observation_edit');

        DB::table('aspirants_prospects')
                ->where('id', Input::get('id'))
                ->update(array('name' => Input::get('name'),
                    'last_name' => Input::get('last_name'),
                    'email' => Input::get('email'),
                    'phone' => Input::get('phone'),
                    'document' => Input::get('document'),
                    'city' => Input::get('city'),
                    'aspirants_status_prospects_id' => Input::get('status'),
                    'aspirants_programs_id' => Input::get('programs')));


        if ($observation_edit != "") {

            $traces = new AspirantsTraces;

            $traces->coment = $observation_edit;
            $traces->aspirants_status_prospects_id = Input::get('status');
            $traces->aspirants_prospects_id = $id;
            $traces->users_id = Auth::user()->id;

            $traces->save();
        }

        echo 1;
    }

    public function getGuardarresultados() {

        $id_informacion = Input::get('id_informacion');
        $estado = "";

        DB::table('aspirants_results')
                ->where('aspirants_informations_id', $id_informacion)
                ->update(array('date_knowledge' => Input::get('date_knowledge'),
                    'score_knowledge' => Input::get('score_knowledge'),
                    'date_interview' => Input::get('date_interview'),
                    'score_interview' => Input::get('score_interview'),
                    'aspirants_status_results_id' => Input::get('aspirants_status_results_id'),
                    'score_status' => Input::get('score_status'),
                    'discount' => Input::get('discount'),
                    'score_discount' => Input::get('score_discount'),
                    'observations' => Input::get('observations')));

        if (Input::get('aspirants_status_results_id') == 1) {

            DB::table('aspirants_prospects')
                    ->join('aspirants_informations', 'aspirants_informations.aspirants_prospects_id', '=', 'aspirants_prospects.id')
                    ->where('aspirants_informations.id', $id_informacion)
                    ->update(array('aspirants_prospects.aspirants_status_managements_id' => 5, 'aspirants_informations.aspirants_status_managements_id' => 5));
        }


        echo 1;
    }

    public function getGuardarmatricula() {

        $id_informacion = Input::get('id_informacion');
        $creditos = Input::get('creditos');
        $programs_final = Input::get('programs_final');
        $observations_final = Input::get('observations_final');
        $estado = "";

        $verificacion = DB::table('icaft_students')
                ->where('aspirants_informations_id', $id_informacion)
                ->get();

        if (!$verificacion) {

            DB::table('icaft_students')->insert(array(
                array('aspirants_informations_id' => $id_informacion, 'credits' => $creditos, 'observations' => $observations_final, 'aspirants_programs_id' => $programs_final, 'status' => 1),
            ));

            DB::table('aspirants_prospects')
                    ->join('aspirants_informations', 'aspirants_informations.aspirants_prospects_id', '=', 'aspirants_prospects.id')
                    ->where('aspirants_informations.id', $id_informacion)
                    ->update(array('aspirants_prospects.aspirants_status_managements_id' => 6, 'aspirants_informations.aspirants_status_managements_id' => 6));

            $id_estudiante = DB::table('icaft_students')
                    ->where('aspirants_informations_id', $id_informacion)
                    ->get();


            $materias = DB::table('icaft_pensums')
                    ->join('aspirants_programs', 'icaft_pensums.aspirants_programs_id', '=', 'aspirants_programs.id')
                    ->join('icaft_pensums_icaft_subjects', 'icaft_pensums.id', '=', 'icaft_pensums_icaft_subjects.icaft_pensums_id')
                    ->select('icaft_pensums_icaft_subjects.id')
                    ->where('icaft_pensums.aspirants_programs_id', $programs_final)
                    ->where('icaft_pensums.status', 1)
                    ->get();

            foreach ($materias as $mat) {

                DB::table('icaft_subjects_icaft_students')->insert(array(
                    array('icaft_pensums_icaft_subjects_id' => $mat->id, 'icaft_students_id' => $id_estudiante[0]->id, 'icaft_status_id' => 1),
                ));
            }

            $user = DB::table('aspirants_informations')
                    ->where('id', $id_informacion)
                    ->get();

            DB::table('profiles_users')->insert(array(
                array('users_id' => $user[0]->users_id, 'profiles_id' => 68),
            ));
        }
        echo 1;
    }

    public function getGuardarnextdate() {

        $id = Input::get('id');
        $next_date = Input::get('next_date');
        $next_observation = Input::get('observation');

        $traces = new AspirantsDiaryTraces;

        $traces->next_date = $next_date;
        $traces->observation = $next_observation;
        $traces->aspirants_prospects_id = $id;
        $traces->users_id = Auth::user()->id;

        if ($traces->save()) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function getGenerarformulario() {

        $id = Input::get('id');
        $observation_edit = Input::get('observation_edit');
        $email = Input::get('email');
        $codigo = rand() . $id;
        $codigo = md5($codigo);

        DB::table('aspirants_prospects')
                ->where('id', Input::get('id'))
                ->update(array('name' => Input::get('name'),
                    'email' => Input::get('email'),
                    'phone' => Input::get('phone'),
                    'city' => Input::get('city'),
                    'code' => $codigo,
                    'aspirants_status_prospects_id' => 21,
                    'aspirants_status_managements_id' => 2,
                    'aspirants_programs_id' => Input::get('programs')));


        if ($observation_edit != "") {

            $traces = new AspirantsTraces;

            $traces->coment = $observation_edit;
            $traces->aspirants_status_prospects_id = Input::get('status');
            $traces->aspirants_prospects_id = $id;
            $traces->users_id = Auth::user()->id;

            $traces->save();
        }

        $fromEmail = "indoamericana@indoamericana.edu.co";
        $fromName = "Indoamericana";

        $data = array(
            'link' => 'http://192.168.0.18/indo/public/registro/' . $codigo,
            'name' => Input::get('name')
        );

        Mail::send('emails.formulario', $data, function ($message) use ($fromName, $fromEmail) {
            $message->subject('Formulario de registro de Aspirantes');
            $message->to(Input::get('email'));
            $message->from($fromEmail, $fromName);
        });

        echo 1;
    }

    public function getNuevoaspirante() {

        $asp = new AspirantsProspects;

        $asp->name = Input::get('name');
        $asp->email = Input::get('email');
        $asp->phone = Input::get('phone');
        $asp->last_name = Input::get('last_name');
        $asp->city = Input::get('city');
        $asp->document = Input::get('document');
        $asp->aspirants_sources_id = Input::get('fuentes');
        $asp->aspirants_campaigns_id = Input::get('campaign');
        $asp->aspirants_inscription_methods_id = Input::get('method');
        $asp->aspirants_status_prospects_id = 1;
        $asp->aspirants_status_managements_id = 1;
        $asp->aspirants_programs_id = Input::get('programs');

        $asp->save();

        $nuevo_asp = AspirantsProspects::all();
        $bandera = $nuevo_asp->last()->id;

        $traces = new AspirantsTraces;

        $traces->coment = "Se ingreso el aspirante al sistema";
        $traces->aspirants_status_prospects_id = 1;
        $traces->aspirants_prospects_id = $bandera;
        $traces->users_id = Auth::user()->id;

        $traces->save();

        echo 1;
    }

    public function postCargamasivaasp() {

        $campaign_carga = Input::get('campaign_carga');

        include("assets/Excel/reader.php");

        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($_FILES["file_aspirantes"]["tmp_name"]);

        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {

            $sql = "SELECT*FROM aspirants_prospects WHERE document=" . $data->sheets[0]['cells'][$i][3];
            $datos = DB::select($sql);

            if ($datos) {
                
            } else {

                $prov = new AspirantsProspects;

                $prov->name = $data->sheets[0]['cells'][$i][1];
                $prov->email = $data->sheets[0]['cells'][$i][2];
                $prov->document = $data->sheets[0]['cells'][$i][3];
                $prov->phone = $data->sheets[0]['cells'][$i][4];
                $prov->aspirants_campaigns_id = $campaign_carga;
                $prov->aspirants_inscription_methods_id = 3;
                $prov->aspirants_status_prospects_id = 1;
                $prov->aspirants_status_managements_id = 1;
                $prov->aspirants_programs_id = 1;
                $prov->save();
            }
        }

        echo 1;
    }

    public function postCargamasivares() {

        include("assets/Excel/reader.php");

        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($_FILES["file_resultados"]["tmp_name"]);

        for ($i = 3; $i <= $data->sheets[0]['numRows']; $i++) {


            DB::table('aspirants_results')
                    ->where('aspirants_informations_id', $data->sheets[0]['cells'][$i][1])
                    ->update(array('score_knowledge' => $data->sheets[0]['cells'][$i][10],
                        'score_interview' => $data->sheets[0]['cells'][$i][12],
                        'aspirants_status_results_id' => $data->sheets[0]['cells'][$i][13],
                        'score_status' => $data->sheets[0]['cells'][$i][14]));
        }
        echo 1;
    }

    public function interval_date($init, $finish) {
        //formateamos las fechas a segundos tipo 1374998435
        $diferencia = strtotime($finish) - strtotime($init);

        //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
        //floor devuelve el número entero anterior, si es 5.7 devuelve 5
        if ($diferencia < 60) {
            $tiempo = floor($diferencia) . " Seg";
        } else if ($diferencia > 60 && $diferencia < 3600) {
            $tiempo = floor($diferencia / 60) . ' Min';
        } else if ($diferencia > 3600 && $diferencia < 86400) {
            $tiempo = floor($diferencia / 3600) . " Horas";
        } else if ($diferencia > 86400 && $diferencia < 2592000) {
            $tiempo = floor($diferencia / 86400) . " Días";
        } else if ($diferencia > 2592000 && $diferencia < 31104000) {
            $tiempo = floor($diferencia / 2592000) . " Meses";
        } else if ($diferencia > 31104000) {
            $tiempo = floor($diferencia / 31104000) . " Años";
        } else {
            $tiempo = "Error";
        }
        return $tiempo;
    }

    public function getComprobaremail() {
        //get POST data
        $correo = Input::get('email');
        //$bandera = Users::find($correo_institucional);
        $bandera = DB::select('SELECT * FROM aspirants_prospects WHERE email = ?', array($correo));
        if ($bandera) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function getComprobarphone() {
        //get POST data
        $telefono = Input::get('phone');
        //$bandera = Users::find($correo_institucional);
        $bandera = DB::select('SELECT * FROM aspirants_prospects WHERE phone = ?', array($telefono));
        if ($bandera) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function getComprobardocumento() {
        //get POST data
        $doc = Input::get('document');
        //$bandera = Users::find($correo_institucional);
        $bandera = DB::select('SELECT * FROM aspirants_prospects WHERE document = ?', array($doc));
        if ($bandera) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function getEnviaremailnuevo() {

        $tipo = Input::get('tipo');
        $id = Input::get('id');
        $email = Input::get('email');

        $traces = new AspirantsTraces;

        $traces->coment = "Envio de email";
        $traces->aspirants_status_prospects_id = 22;
        $traces->aspirants_prospects_id = $id;
        $traces->users_id = Auth::user()->id;

        $traces->save();

        $fromEmail = "Indoamericana@indoamericana.edu.co";
        $fromName = "Indoamericana";

        $data = array();

        switch ($tipo) {
            case "1":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/INFORMACION_PROGRAMAS_GENERAL.pdf", array('as' => "INFORMACION_PROGRAMAS_GENERAL.pdf"));
                });
                break;
            case "2":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/ASA-I-2016.docx", array('as' => "ASA-I-2016.docx"));
                });
                break;
            case "3":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/ATA-I-206.docx", array('as' => "ATA-I-206.docx"));
                });
                break;
            case "4":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/DPA-I-2016.docx", array('as' => "DPA-I-2016.docx"));
                });
                break;
            case "5":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/TEEI-I-2016.docx", array('as' => "TEEI-I-2016.docx"));
                });
                break;
            case "6":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/TLA-I-2016.docx", array('as' => "TLA-I-2016.docx"));
                });
                break;
            case "7":
                Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
                    $message->subject('Proceso de Admision');
                    $message->to($email);
                    $message->from($fromEmail, $fromName);
                    $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/TLH-I-2016.docx", array('as' => "TLH-I-2016.docx"));
                });
                break;
        }

//        if ($tipo == 1) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/INFORMACION_PROGRAMAS_GENERAL.pdf", array('as' => "INFORMACION_PROGRAMAS_GENERAL.pdf"));
//            });
//        } elseif ($tipo == 2) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/ASA-I-2016.docx", array('as' => "ASA-I-2016.docx"));
//            });
//        } elseif ($tipo == 3) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/ATA-I-206.docx", array('as' => "ATA-I-206.docx"));
//            });
//        } elseif ($tipo == 4) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/DPA-I-2016.docx", array('as' => "DPA-I-2016.docx"));
//            });
//        } elseif ($tipo == 5) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/TEEI-I-2016.docx", array('as' => "TEEI-I-2016.docx"));
//            });
//        } elseif ($tipo == 6) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/TLA-I-2016.docx", array('as' => "TLA-I-2016.docx"));
//            });
//        } elseif ($tipo == 7) {
//            Mail::send('emails.email1', $data, function ($message) use ($fromName, $fromEmail, $email) {
//                $message->subject('Proceso de Admision');
//                $message->to($email);
//                $message->from($fromEmail, $fromName);
//                $message->attach("http://192.168.0.18/indo/public/assets/files/comercial/TLH-I-2016.docx", array('as' => "TLH-I-2016.docx"));
//            });
//        }

        echo 1;
    }

    public function getIngresaritems() {

        $metodo = Input::get('metodo');
        $campania = Input::get('campania');
        $fuente = Input::get('fuente');
        $estado = Input::get('estado');
        $documento = Input::get('documento');
        $convocation = Input::get('convocation');
        $date = Input::get('date');
        $period_c = Input::get('period_c');
        $classroom = Input::get('classroom');
        $quota = Input::get('quota');
        $convocation_a = Input::get('convocation_a');
        $classroom_a = Input::get('classroom_a');
        $type_a = Input::get('type_a');
        $time_a = Input::get('time_a');


        if ($metodo != "") {

            DB::table('aspirants_inscription_methods')->insert(array(
                array('inscription_method' => $metodo, 'status' => 1),
            ));
            echo "1";
        } elseif ($campania != "") {
            DB::table('aspirants_campaigns')->insert(array(
                array('campaign' => $campania, 'status' => 1),
            ));
            echo "1";
        } elseif ($fuente != "") {
            DB::table('aspirants_sources')->insert(array(
                array('source' => $fuente, 'status' => 1),
            ));
            echo "1";
        } elseif ($estado != "") {
            DB::table('aspirants_status_prospects')->insert(array(
                array('status' => $estado, 'state' => 1),
            ));
            echo "1";
        } elseif ($documento != "") {
            DB::table('aspirants_documents')->insert(array(
                array('document' => $documento, 'status' => 1),
            ));
            echo "1";
        } elseif ($convocation != "" && $date != "") {
            DB::table('aspirants_convocations')->insert(array(
                array('convocation' => $convocation, 'aspirants_periods_id' => $period_c, 'date' => $date),
            ));
            echo "1";
        } elseif ($classroom != "" && $quota != "") {
            DB::table('aspirants_classrooms')->insert(array(
                array('classroom' => $classroom, 'quota' => $quota),
            ));
            echo "1";
        } elseif ($convocation_a != "" && $classroom_a != "" && $type_a != "" && $time_a != "") {
            DB::table('aspirants_convocations_has_aspirants_classrooms')->insert(array(
                array('aspirants_convocations_id' => $convocation_a, 'aspirants_classrooms_id' => $classroom_a, 'aspirants_type_tests_id' => $type_a, 'time' => $time_a),
            ));
            echo "1";
        } else {
            echo "2";
        }
    }

    public function getProgramasasignacion() {

        $programas = AspirantsPrograms::all();

        $asignaciones_programas = DB::table('aspirants_classrooms_has_aspirants_programs')
                ->join('aspirants_programs', 'aspirants_programs.id', '=', 'aspirants_classrooms_has_aspirants_programs.aspirants_programs_id')
                ->where('aspirants_convocations_has_aspirants_classrooms_id', Input::get('id'))
                ->get();

        /* $archivos = DB::table('aspirants_folders')
          ->join('aspirants_files', 'aspirants_files.aspirants_folders_id', '=', 'aspirants_folders.id')
          ->select('aspirants_folders.name_folder', 'aspirants_folders.dir_folder', 'aspirants_files.name_format as archivo', 'aspirants_files.file_format', 'aspirants_files.id')
          ->where('aspirants_folders.id', Input::get('id'))
          ->get(); */

        return View::make('dashboard.aspirantes.programas_asignaciones')
                        ->with('id_asignacion', Input::get('id'))
                        ->with('programas', $programas)
                        ->with('asignaciones_programas', $asignaciones_programas);
    }

    public function getAsignarprogramas() {
        $programa_asignar = Input::get('programa_asignar');
        $id = Input::get('id');

        DB::table('aspirants_classrooms_has_aspirants_programs')->insert(
                array('aspirants_convocations_has_aspirants_classrooms_id' => $id, 'aspirants_programs_id' => $programa_asignar)
        );

        echo "1";
    }

    public function getDirectorio() {
        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $agendas = AspirantsSchoolAgendas::where('id', 'LIKE', '%' . $codigo . '%')
                ->get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($agendas); $i++) {
            $fecha = date($agendas[$i]->created_at);
            $data[$i]["id"] = 'AGE' . str_pad($agendas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["name"] = $agendas[$i]->name;
            $data[$i]["name_contact"] = $agendas[$i]->name_contact;
            $data[$i]["address"] = $agendas[$i]->address;
            $data[$i]["phone"] = $agendas[$i]->phone;
            $data[$i]["email"] = $agendas[$i]->email;
            $data[$i]["observations"] = $agendas[$i]->observations;
            $data[$i]["date_fair"] = $agendas[$i]->date_fair;
            $data[$i]["observation"] = $agendas[$i]->observations;

            $data[$i]["hid"] = $agendas[$i]->id;
        }

        if (isset($_GET['nombre'])) {
            $hola = View::make('dashboard.aspirantes.tabla_directorio')
                    ->with('datos_agendas', $data);

            //return PDF::load($hola, 'A4', 'portrait')->show();
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.agenda_colegios')
                            ->with('datos_agendas', $data)
                            ->with('submenu_activo', 'Directorio')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getFeriascolegios() {
        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');

        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }

        $ferias = AspirantsSchoolFairs::where('id', 'LIKE', '%' . $codigo . '%')
                ->get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($ferias); $i++) {
            $fecha = date($ferias[$i]->created_at);
            $data[$i]["id"] = 'AGE' . str_pad($ferias[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["name"] = $ferias[$i]->AspirantsSchoolAgendas->name;
            $data[$i]["name_contact"] = $ferias[$i]->AspirantsSchoolAgendas->name_contact;
            $data[$i]["address"] = $ferias[$i]->AspirantsSchoolAgendas->address;
            $data[$i]["phone"] = $ferias[$i]->AspirantsSchoolAgendas->phone;
            $data[$i]["email"] = $ferias[$i]->AspirantsSchoolAgendas->email;
            $data[$i]["observations"] = $ferias[$i]->observacion;
            $data[$i]["date_fair"] = $ferias[$i]->date_fair;
            $data[$i]["observation"] = $ferias[$i]->observacion;

            $data[$i]["hid"] = $ferias[$i]->id;
        }

        if (isset($_GET['nombre'])) {
            $hola = View::make('dashboard.aspirantes.tabla_directorio')
                    ->with('datos_agendas', $data);

            //return PDF::load($hola, 'A4', 'portrait')->show();
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.ferias_directorio')
                            ->with('datos_agendas', $data)
                            ->with('submenu_activo', 'Directorio')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getDirectorioferias() {
        $codigo = Input::get('code');
        $status = Input::get('status');
        $program = Input::get('programa');




        if (isset($_GET['status']) && !empty($_GET['status'])) {
            $var = '=';
            $signo = $status;
        } else {
            $var = 'LIKE';
            $signo = '%' . $status . '%';
        }


        $agendas = AspirantsSchoolAgendas::where('id', 'LIKE', '%' . $codigo . '%')->orderBy('id', 'asc')->get();

        $fecha_actual = date('Y-m-d H:i:s');
        $data = array();

        for ($i = 0; $i < count($agendas); $i++) {
            $fecha = date($agendas[$i]->created_at);
            $data[$i]["id"] = 'AGE' . str_pad($agendas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]["name"] = $agendas[$i]->name;
            $data[$i]["name_contact"] = $agendas[$i]->name_contact;
            $data[$i]["address"] = $agendas[$i]->address;
            $data[$i]["phone"] = $agendas[$i]->phone;
            $data[$i]["email"] = $agendas[$i]->email;
            $data[$i]["observations"] = $agendas[$i]->observations;
            $data[$i]["date_fair"] = $agendas[$i]->date_fair;
            $data[$i]["observation"] = $agendas[$i]->observations;

            $data[$i]["hid"] = $agendas[$i]->id;
        }

        if (isset($_GET['nombre'])) {
            return View::make('dashboard.aspirantes.tabla_directorio')
                            ->with('datos_agendas', $data);
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.directorioferias')
                            ->with('datos_agendas', $data)
                            ->with('submenu_activo', 'Directorio')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getIngresaragenda() {

        $nombre = Input::get('nombre');
        $contacto = Input::get('contacto');
        $direccion = Input::get('direccion');
        $telefono = Input::get('telefono');
        $email = Input::get('email');
        $feria = Input::get('feria');
        $rector = Input::get('rector');
        $observaciones = Input::get('observaciones');

        if (Input::get('id')) {
            $id = Input::get('id');
            DB::update('update aspirants_school_agendas set name  = ?, name_contact = ?, address = ?, phone = ?, email = ?, observations = ?, date_fair = ?, name_rector = ? where id = ?', array($nombre, $contacto, $direccion, $telefono, $email, $observaciones, $feria, $rector, $id));
            echo "1";
        } else {

            $nueva_agenda = new AspirantsSchoolAgendas;

            $nueva_agenda->name = $nombre;
            $nueva_agenda->name_contact = $contacto;
            $nueva_agenda->address = $direccion;
            $nueva_agenda->phone = $telefono;
            $nueva_agenda->email = $email;
            $nueva_agenda->observations = $observaciones;
            $nueva_agenda->date_fair = $feria;
            $nueva_agenda->name_rector = $rector;
            $nueva_agenda->users_id = Auth::user()->id;

            if ($nueva_agenda->save()) {
                echo "1";
            } else {
                echo "2";
            }
        }
    }

    public function getDetalleagenda() {




        $detalle_agenda = AspirantsSchoolAgendas::where('id', Input::get('id'))->get();

        return View::make('dashboard.aspirantes.detalle_agenda')
                        ->with('detalle_agendas', $detalle_agenda);
    }

    public function getSeguimientosagenda() {


        $ferias_agenda = AspirantsSchoolFairs::where('aspirants_school_agendas_id', Input::get('id'))->get();
        $campanas = AspirantsCampaigns::all();

        return View::make('dashboard.aspirantes.ferias_agenda')
                        ->with('id_agenda', Input::get('id'))
                        ->with('campanas', $campanas)
                        ->with('ferias_agenda', $ferias_agenda);
    }

    public function getSeguimientosferias() {


        $seg_agenda = AspirantsSchoolTraces::where('aspirants_shool_fairs_id', Input::get('id'))->get();
        $campanas = AspirantsCampaigns::all();

        return View::make('dashboard.aspirantes.seguimientos_agenda')
                        ->with('id_agenda', Input::get('id'))
                        ->with('campanas', $campanas)
                        ->with('detalle_agendas', $seg_agenda);
    }

    function formatDate($date) {
        return strtotime(substr($date, 6, 4) . "-" . substr($date, 3, 2) . "-" . substr($date, 0, 2) . " " . substr($date, 10, 6)) * 1000;
    }

    public function getIngresarferias() {
        $fecha_ini = str_replace('/', '-', Input::get('fecha_ini'));
        $fecha_fin = str_replace('/', '-', Input::get('fecha_fin'));
        ;
        $Observacion_feria = Input::get('Observacion_feria');




        $newDate1 = date("Y-m-d H:i:s", strtotime($fecha_ini));
        $newDate2 = date("Y-m-d H:i:s", strtotime($fecha_fin));


        $start = $this->formatDate($fecha_ini);
        $end = $this->formatDate($fecha_fin);


        $feria = new AspirantsSchoolFairs;

        $feria->observacion = Input::get('Observacion_feria');
        $feria->date_fair = $newDate1;
        $feria->date_fair_end = $newDate2;
        $feria->start = $start;
        $feria->end = $end;
        $feria->aspirants_school_agendas_id = Input::get('id');

        if ($feria->save()) {
            echo "1";
        } else {
            echo "2";
        }
    }

    public function postIngresarseguimientos() {

//        $observaciones = Input::get('observaciones');
//        $id = Input::get('id');
//       
//            $nueva_agenda  = new AspirantsSchoolTraces;
//
//            
//            $nueva_agenda->observation = $observaciones;
//            $nueva_agenda->aspirants_school_agenda_id = $id;
//            $nueva_agenda->users_id = Auth::user()->id;            
//            
//
//            if($nueva_agenda->save()){
//                echo "1";
//            }else{
//                echo "2";
//            }


        $campaign_carga = Input::get('campaign_carga');

        include("assets/Excel/reader.php");

        $data = new Spreadsheet_Excel_Reader();
        $data->setOutputEncoding('CP1251');
        $data->read($_FILES["file_estudiantes"]["tmp_name"]);

        for ($i = 2; $i <= $data->sheets[0]['numRows']; $i++) {

            $sql = "SELECT*FROM aspirants_prospects WHERE document=" . $data->sheets[0]['cells'][$i][3];
            $datos = DB::select($sql);

            if ($datos) {
                
            } else {

                $prov = new AspirantsProspects;

                $prov->name = $data->sheets[0]['cells'][$i][1];
                $prov->email = $data->sheets[0]['cells'][$i][2];
                $prov->document = $data->sheets[0]['cells'][$i][3];
                $prov->phone = $data->sheets[0]['cells'][$i][4];
                $prov->aspirants_campaigns_id = $campaign_carga;
                $prov->aspirants_inscription_methods_id = 3;
                $prov->aspirants_status_prospects_id = 1;
                $prov->aspirants_status_managements_id = 1;
                $prov->aspirants_programs_id = 1;
                $prov->save();
            }
        }


        if (Input::file('file_estudiantes')) {

            $my_id = Auth::user()->id;


            $destinationPath = 'assets/files/colegios_aspirantes/';

            $observaciones = Input::get('observaciones');
            $id = Input::get('id');



            $filename = date("YmdHis") . "_" . Input::file('file_estudiantes')->getClientOriginalName();

            $uploadSuccess = Input::file('file_estudiantes')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                $nueva_agenda = new AspirantsSchoolTraces;


                $nueva_agenda->observation = $observaciones;
                $nueva_agenda->file = $destinationPath . $filename;
                $nueva_agenda->aspirants_shool_fairs_id = $id;
                $nueva_agenda->users_id = Auth::user()->id;


                if ($nueva_agenda->save()) {
                    echo $destinationPath . $filename;
                } else {
                    echo "2";
                }
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }

    public function actAspirantes() {
        //get POST data
        $sql = "SELECT aspirants_diary_traces.next_date, aspirants_diary_traces.observation, aspirants_prospects.id, aspirants_prospects.name FROM aspirants_diary_traces JOIN aspirants_prospects ON (aspirants_prospects.id = aspirants_diary_traces.aspirants_prospects_id) WHERE users_id ='" . Auth::user()->id . "' AND str_to_date(next_date,'%Y-%m-%d') = '" . date("Y-m-d") . "'";
        $datos = DB::select($sql);
        if ($datos) {
            $sql = "SELECT*FROM pending_aspirants WHERE users_id ='" . Auth::user()->id . "' AND str_to_date(created_at,'%Y-%m-%d') = '" . date("Y-m-d") . "'";
            $datos2 = DB::select($sql);

            if ($datos2) {

                return 0;
            } else {

                $pending = new PendingAspirants;
                $pending->ip = Request::getClientIp();
                $pending->users_id = Auth::user()->id;
                $pending->save();

                $contenido = '<table class="table top-blue" data-target="soporte/callSupport/"><thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nombre</th>
                                                    <th>Observacion</th>
                                                    <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>';

                for ($i = 0; count($datos) > $i; $i++) {

                    $id = 'ASP' . str_pad($datos[$i]->id, 6, "0", STR_PAD_LEFT);

                    $contenido .= "<tr style='text-align:center;'><td>" . $id . "</td><td>" . $datos[$i]->name . "</td><td>" . $datos[$i]->observation . "</td><td>" . $datos[$i]->next_date . "</td></tr>";
                }

                $contenido .= "
                                            </tbody>
                                        </table>";

                return $contenido;
            }
        } else {
            return 0;
        }
    }

    public function actPendingfechas() {
        //get POST data
        $sql = "SELECT*FROM aspirants_documents_has_aspirants_informations
                JOIN  aspirants_informations ON (aspirants_informations.id = aspirants_documents_has_aspirants_informations.aspirants_informations_id)
                                        WHERE status = '3' AND str_to_date(date_commitment,'%Y-%m-%d') = '" . date("Y-m-d") . "' ORDER BY str_to_date(date_commitment,'%Y-%m-%d') = '" . date("Y-m-d") . "'";

        $datos = DB::select($sql);
        if ($datos) {
            $sql = "SELECT*FROM pending_aspirants WHERE users_id ='" . Auth::user()->id . "' AND str_to_date(created_at,'%Y-%m-%d') = '" . date("Y-m-d") . "'";
            $datos2 = DB::select($sql);

            if ($datos2) {

                return 0;
            } else {

                $pending = new PendingAspirants;
                $pending->ip = Request::getClientIp();
                $pending->users_id = Auth::user()->id;
                $pending->save();

                $contenido = '<table class="table top-blue" data-target="soporte/callSupport/"><thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Nombre</th>
                                                    <th>Fecha</th>
                                                </tr>
                                            </thead>
                                            <tbody>';

                for ($i = 0; count($datos) > $i; $i++) {

                    $id = 'ASP' . str_pad($datos[$i]->aspirants_informations_id, 6, "0", STR_PAD_LEFT);

                    $contenido .= "<tr style='text-align:center;'><td>" . $id . "</td><td>" . $datos[$i]->first_name . " " . $datos[$i]->last_name . "</td><td>" . $datos[$i]->date_commitment . "</td></tr>";
                }

                $contenido .= "
                                            </tbody>
                                        </table>";

                return $contenido;
            }
        } else {
            return 0;
        }
    }

    public function getInformes() {

        $permission = $this->permission_control("52");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }

        $user = Users::find(Auth::user()->id);
        $estados = AspirantsStatusManagements::all();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {
            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.informes')
                            ->with('user', $user)
                            ->with('estados', $estados)
                            ->with('submenu_activo', 'Informes')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    public function getEstadisticas() {

        $permission = $this->permission_control("51");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Administrar Solicitudes')
                            ->with('menu_activo', 'Help Desk');
        }

        $user = Users::find(Auth::user()->id);

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM aspirants_traces
				JOIN users ON (aspirants_traces.users_id = users.id)
				GROUP BY users_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $seguimientos_grafica = DB::select($sql);

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {

            return View::make('dashboard.index')
                            ->with('container', 'dashboard.aspirantes.statistics')
                            ->with('user', $user)
                            ->with('seguimientos_grafica', $seguimientos_grafica)
                            ->with('submenu_activo', 'Estadísticas')
                            ->with('menu_activo', 'Aspirantes');
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicseguimientos() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM aspirants_traces
				JOIN users ON (aspirants_traces.users_id = users.id)
				GROUP BY users_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->name . " " . $key->last_name;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Seguimientos por Usuario");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedateseguimientos() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM aspirants_traces
				JOIN users ON (aspirants_traces.users_id = users.id)
                                WHERE aspirants_traces.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
				GROUP BY users_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->name . " " . $key->last_name;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de Seguimientos por Usuario");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarseguimientos() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM aspirants_traces
				JOIN users ON (aspirants_traces.users_id = users.id)
                                WHERE aspirants_traces.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY users_id
				ORDER BY dato DESC";


        $datos = DB::select($sql);

        $sql = "SELECT users_id, users.name,users.last_name,aspirants_traces.coment,aspirants_traces.created_at,aspirants_status_prospects.status,aspirants_prospects.name as prospecto
				FROM aspirants_traces
				JOIN users ON (aspirants_traces.users_id = users.id)
                                JOIN aspirants_status_prospects ON (aspirants_traces.aspirants_status_prospects_id = aspirants_status_prospects.id)
                                JOIN aspirants_prospects ON (aspirants_traces.aspirants_prospects_id = aspirants_prospects.id)
                                WHERE aspirants_traces.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY users_id";


        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Seguimientos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Seguimientos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->name . " " . $datos[$i]->last_name)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Seguimientos');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Estado')
                ->setCellValue('C1', 'Comentario')
                ->setCellValue('D1', 'Prospecto')
                ->setCellValue('E1', 'Fecha');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name . " " . $detalle[$i]->last_name)
                    ->setCellValue('B' . $j, $detalle[$i]->status)
                    ->setCellValue('C' . $j, $detalle[$i]->coment)
                    ->setCellValue('D' . $j, $detalle[$i]->prospecto)
                    ->setCellValue('E' . $j, $detalle[$i]->created_at);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Seguimientos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicprogramas() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_programs_id, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->acronyms;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de programas de mas interes");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedateprogramas() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_programs_id, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->acronyms;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de programas de mas interes");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarprogramas() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_programs_id, aspirants_programs.name, aspirants_programs.acronyms, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_programs_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_prospects
                                JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_prospects.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Programas")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Cantidad');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->name)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Programas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->phone2)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Programas_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphiccampanas() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;


        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->campaign;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de campañas con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedatecampanas() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "' 
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->campaign;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de campañas con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarcampanas() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_campaigns_id, aspirants_campaigns.campaign, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_campaigns_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_prospects
                                JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_campaigns ON (aspirants_prospects.aspirants_campaigns_id = aspirants_campaigns.id)
                                JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_prospects.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Campanas")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Campaña')
                ->setCellValue('B1', 'Cantidad');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->campaign)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Campañas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa')
                ->setCellValue('H1', 'Campaña');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->phone2)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('H' . $j, $detalle[$i]->campaign);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Campanas_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicfuentes() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->source;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de fuentes de informacion con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedatefuentes() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->source;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de fuentes de informacion con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarfuentes() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_sources.source, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_sources_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_prospects
                                JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_sources ON (aspirants_prospects.aspirants_sources_id = aspirants_sources.id)
                                JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_prospects.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Fuentes de informacion")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Fuente')
                ->setCellValue('B1', 'Cantidad');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->source)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Fuentes');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa')
                ->setCellValue('H1', 'Fuente');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->phone2)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('H' . $j, $detalle[$i]->source);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Fuentes_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicestados() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->status;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de estados de los prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedateestados() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->status;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de estados de los prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarestados() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_status_managements_id, aspirants_status_managements.status, COUNT( * ) AS dato
				FROM aspirants_prospects
				JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_prospects.aspirants_status_managements_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_prospects
                                JOIN aspirants_programs ON (aspirants_prospects.aspirants_programs_id = aspirants_programs.id)                                
                                JOIN aspirants_status_managements ON (aspirants_prospects.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_prospects.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_prospects.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Estados")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Ingresos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->status)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Estados');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->phone2)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Estados_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicjornadas() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->day_trip;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de jornadas con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedatejornadas() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->day_trip;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de jornadas con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarjornadas() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_times.day_trip, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_times_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_informations
                                JOIN aspirants_programs ON (aspirants_informations.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                JOIN aspirants_status_managements ON (aspirants_informations.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_informations.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Jornadas")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Ingresos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->day_trip)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Jornadas');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa')
                ->setCellValue('H1', 'Jornada');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->cell_phone)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('H' . $j, $detalle[$i]->day_trip);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Jornadas_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicgeneros() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->aspirants_gender;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de generos con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedategeneros() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->aspirants_gender;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de generos con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportargeneros() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_genders.aspirants_gender, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_genders_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_informations
                                JOIN aspirants_programs ON (aspirants_informations.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_genders ON (aspirants_informations.aspirants_genders_id = aspirants_genders.id)
                                JOIN aspirants_status_managements ON (aspirants_informations.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_informations.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Generos")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Ingresos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->aspirants_gender)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de ingresos');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa')
                ->setCellValue('H1', 'Genero');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->cell_phone)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('H' . $j, $detalle[$i]->aspirants_gender);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Generos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphicciudad() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->city;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de ciudades con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedateciudad() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->city;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de ciudades con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarciudad() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_cities.city, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_cities_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_informations
                                JOIN aspirants_programs ON (aspirants_informations.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_cities ON (aspirants_informations.aspirants_cities_id = aspirants_cities.id)
                                JOIN aspirants_status_managements ON (aspirants_informations.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_informations.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ciudades")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Ingresos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->city)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de ingresos');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa')
                ->setCellValue('H1', 'Ciudad');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->cell_phone)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('H' . $j, $detalle[$i]->city);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Ciudades_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getGraphiclocalidad() {

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";

        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        foreach ($cons_grafico as $key) {
            $datos1[] = $key->dato;
            $datos1_2[] = $key->location;
        }

        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de localidades con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getChangedatelocalidad() {

        $fecha = Input::get("fecha_fin");
        $nuevafecha = strtotime('+1 day', strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);

        $grafico1 = array();

        include("jpgraph/src/jpgraph.php");
        include("jpgraph/src/jpgraph_bar.php");

        $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . Input::get("fecha_inicio") . "' AND '" . $nuevafecha . "'
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";


        $cons_grafico = DB::select($sql);

        $total1 = count($cons_grafico);
        $grafico1 = $cons_grafico;

        if ($total1 > 0) {
            foreach ($cons_grafico as $key) {
                $datos1[] = $key->dato;
                $datos1_2[] = $key->location;
            }
        } else {
            $datos1[] = 0;
            $datos1_2[] = "No hay datos";
        }
        $grafico1 = new Graph(500, 500);
        $grafico1->SetScale("textlin");
        $grafico1->title->Set("Reporte de localidades con mas prospectos");

        $grafico1->xaxis->SetTickLabels($datos1_2);
        $grafico1->xaxis->SetLabelAngle(50);

        $barplot1 = new BarPlot($datos1);
        $barplot1->SetColor("#F781BE@0.5");
        //UN GRADIANTE HORIZONTAL ROJO A AZUL
        $barplot1->SetFillGradient('#64D571@0.5', '#64D571@0.5', GRAD_HOR);
        //25 PIXELES DE ANCHO PARA CADA BARRA
        $barplot1->SetWidth(25);
        $grafico1->Add($barplot1);
        $barplot1->value->SetFormat("%d");
        $barplot1->value->Show();
        $grafico1->Stroke();
    }

    public function getExportarlocalidad() {
        include("assets/phpexcel/PHPExcel.php");
        $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
        $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
        $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
        $fecha_fin = date('Y-m-j', $nuevafecha);

        $sql = "SELECT aspirants_locations.location, COUNT( * ) AS dato
				FROM aspirants_informations
				JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
				GROUP BY aspirants_informations.aspirants_locations_id
				ORDER BY dato DESC";

        $datos = DB::select($sql);

        $sql = "SELECT*FROM aspirants_informations
                                JOIN aspirants_programs ON (aspirants_informations.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_locations ON (aspirants_informations.aspirants_locations_id = aspirants_locations.id)
                                JOIN aspirants_status_managements ON (aspirants_informations.aspirants_status_managements_id = aspirants_status_managements.id)
                                WHERE aspirants_informations.created_at BETWEEN '" . $fecha_in . "' AND '" . $fecha_fin . "'
                                ORDER BY aspirants_informations.id";

        $detalle = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Localidades")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Usuario')
                ->setCellValue('B1', 'Ingresos');

        $j = 2;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->location)
                    ->setCellValue('B' . $j, $datos[$i]->dato);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Numero de ingresos');
        // Creamos una nueva hoja llamada “Detalle”
        $myWorkSheet = new PHPExcel_Worksheet($objPHPExcel, 'Detalle');
        $objPHPExcel->addSheet($myWorkSheet, 1);

        $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', 'Nombre')
                ->setCellValue('B1', 'Email')
                ->setCellValue('C1', 'Telefono')
                ->setCellValue('D1', 'Telefono2')
                ->setCellValue('E1', 'Documento')
                ->setCellValue('F1', 'Estado')
                ->setCellValue('G1', 'Programa')
                ->setCellValue('H1', 'Localidad');

        $j = 2;
        for ($i = 0; $i < count($detalle); $i++) {

            $objPHPExcel->setActiveSheetIndex(1)
                    ->setCellValue('A' . $j, $detalle[$i]->name)
                    ->setCellValue('B' . $j, $detalle[$i]->email)
                    ->setCellValue('C' . $j, $detalle[$i]->phone)
                    ->setCellValue('D' . $j, $detalle[$i]->cell_phone)
                    ->setCellValue('E' . $j, $detalle[$i]->document)
                    ->setCellValue('F' . $j, $detalle[$i]->status)
                    ->setCellValue('G' . $j, $detalle[$i]->acronyms)
                    ->setCellValue('H' . $j, $detalle[$i]->location);

            $j++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Localidades_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public function getCambiargrafica() {

        $sql = "SELECT users_id, users.name,users.last_name, COUNT( * ) AS dato
				FROM aspirants_traces
				JOIN users ON (aspirants_traces.users_id = users.id)
				GROUP BY users_id
				ORDER BY dato DESC
                                LIMIT 0 , 10";
        $seguimientos_grafica = DB::select($sql);

        return View::make('dashboard.aspirantes.hola')
                        ->with('seguimientos_grafica', $seguimientos_grafica);
    }

    public function getExcelconvocatorias() {
        include("assets/phpexcel/PHPExcel.php");


        $sql = "SELECT aspirants_results.aspirants_status_results_id,aspirants_results.score_status, aspirants_results.score_interview, aspirants_results.score_knowledge, aspirants_times.day_trip, aspirants_convocations.date, aspirants_programs.acronyms,aspirants_informations.id,aspirants_informations.first_name,aspirants_informations.last_name,aspirants_informations.middle_name,aspirants_informations.last_name2,aspirants_informations.document
				FROM aspirants_informations
				JOIN aspirants_allocation_tests ON (aspirants_informations.id = aspirants_allocation_tests.aspirants_informations_id)
                                JOIN aspirants_convocations_has_aspirants_classrooms ON (aspirants_allocation_tests.aspirants_convocations_has_aspirants_classrooms_id = aspirants_convocations_has_aspirants_classrooms.id)
                                JOIN aspirants_convocations ON (aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = aspirants_convocations.id)
                                JOIN aspirants_programs ON (aspirants_informations.aspirants_programs_id = aspirants_programs.id)
                                JOIN aspirants_times ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                JOIN aspirants_results ON (aspirants_informations.aspirants_times_id = aspirants_times.id)
                                WHERE aspirants_convocations_has_aspirants_classrooms.aspirants_convocations_id = '" . Input::get('id') . "'
				group by aspirants_informations.id";

        $datos = DB::select($sql);

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Convocatoria")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'En la columna resultado por favor digite:')
                ->setCellValue('B1', '1 = ADMITIDO')
                ->setCellValue('D1', '2 = TUTORIA')
                ->setCellValue('F1', '3 = NO ADMITIDO')
                ->setCellValue('H1', '4 = COMUNICARSE')
                ->setCellValue('J1', 'POR FAVOR NO BORRE NINGUN CAMPO SOLO MODIFIQUE LAS ULTIMAS 6 COLUMNAS')
                ->setCellValue('A2', 'Id')
                ->setCellValue('B2', 'Primer Nombre')
                ->setCellValue('C2', 'Segundo Nombre')
                ->setCellValue('D2', 'Primer Apellido')
                ->setCellValue('E2', 'Segundo Apellido')
                ->setCellValue('F2', 'Documento')
                ->setCellValue('G2', 'Programa')
                ->setCellValue('H2', 'Jornada')
                ->setCellValue('I2', 'Fecha Examen')
                ->setCellValue('J2', 'Concepto Examen')
                ->setCellValue('K2', 'Fecha Entrevista')
                ->setCellValue('L2', 'Concepto Entrevista')
                ->setCellValue('M2', 'Resultado')
                ->setCellValue('N2', 'Concepto');

        $j = 3;
        for ($i = 0; $i < count($datos); $i++) {

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $datos[$i]->id)
                    ->setCellValue('B' . $j, $datos[$i]->first_name)
                    ->setCellValue('C' . $j, $datos[$i]->middle_name)
                    ->setCellValue('D' . $j, $datos[$i]->last_name)
                    ->setCellValue('E' . $j, $datos[$i]->last_name2)
                    ->setCellValue('F' . $j, $datos[$i]->document)
                    ->setCellValue('G' . $j, $datos[$i]->acronyms)
                    ->setCellValue('H' . $j, $datos[$i]->day_trip)
                    ->setCellValue('I' . $j, $datos[$i]->date)
                    ->setCellValue('J' . $j, $datos[$i]->score_knowledge)
                    ->setCellValue('K' . $j, $datos[$i]->date)
                    ->setCellValue('L' . $j, $datos[$i]->score_interview)
                    ->setCellValue('N' . $j, $datos[$i]->score_status)
                    ->setCellValue('M' . $j, $datos[$i]->aspirants_status_results_id);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Convocatoria');
        $objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Convocatoria_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function getUpdatedatospros() {

        $first_name = Input::get('first_name');
        $middle_name = Input::get('middle_name');
        $last_name = Input::get('last_name');
        $last_name2 = Input::get('last_name2');
        $aspirants_type_documents_id = Input::get('aspirants_type_documents_id');
        $document = Input::get('document');
        $issued = Input::get('issued');
        $aspirants_programs_id = Input::get('aspirants_programs_id');
        $aspirants_periods_id = Input::get('aspirants_periods_id');
        $aspirants_times_id = Input::get('aspirants_times_id');
        $aspirants_trainings_id = Input::get('aspirants_trainings_id');
        $military_card = Input::get('military_card');
        $birthday = Input::get('birthday');
        $aspirants_marital_statuses_id = Input::get('aspirants_marital_statuses_id');
        $aspirants_genders_id = Input::get('aspirants_genders_id');
        $blood_group = Input::get('blood_group');
        $nationality = Input::get('nationality');
        $address = Input::get('address');
        $aspirants_locations_id = Input::get('aspirants_locations_id');
        $neighborhood = Input::get('neighborhood');
        $aspirants_cities_id = Input::get('aspirants_cities_id');
        $phone = Input::get('phone');
        $cell_phone = Input::get('cell_phone');
        $email = Input::get('email');
        $aspirants_birth_places_id = Input::get('aspirants_birth_places_id');
        $observations = Input::get('observations');
        $height = Input::get('height');
        $image = Input::get('image');
        $aspirants_sources_id = Input::get('aspirants_sources_id');
        $source_details = Input::get('source_details');
        $aspirants_prospects_id = Input::get('aspirants_prospects_id');
        $aspirants_status_managements_id = Input::get('aspirants_status_managements_id');
        $name_father = Input::get('name_father');
        $occupation_father = Input::get('occupation_father');
        $address_father = Input::get('address_father');
        $phone_father = Input::get('phone_father');
        $name_mother = Input::get('name_mother');
        $occupation_mother = Input::get('occupation_mother');
        $address_mother = Input::get('address_mother');
        $phone_mother = Input::get('phone_mother');
        $name_keeper = Input::get('name_keeper');
        $occupation_keeper = Input::get('occupation_keeper');
        $address_keeper = Input::get('address_keeper');
        $phone_keeper = Input::get('phone_keeper');
        $cellphone_keeper = Input::get('cellphone_keeper');
        $email_keeper = Input::get('email_keeper');
        $neighborhood_keeper = Input::get('neighborhood_keeper');

        $school = Input::get('school');
        $university = Input::get('university');
        $career = Input::get('career');
        $others = Input::get('others');
        $motivation = Input::get('motivation');

        $company = Input::get('company');
        $phone = Input::get('phone');
        $address = Input::get('address');

        DB::table('aspirants_informations')
                ->where('id', $id_informacion)
                ->update(array('first_name' => $first_name, 'middle_name' => $middle_name, 'first_name' => $first_name, 'middle_name' => $middle_name));

        echo "1";
    }

    public function getCalendar() {


        $query = DB::table('aspirants_school_fairs')->get();


        $out = array();
        foreach ($query as $row) {
            $out[] = array(
                'id' => $row->id,
                'title' => $row->observacion,
                'url' => '#',
                'class' => $row->class,
                'start' => $row->start,
                'end' => $row->end
            );
        }


        // $query = $this->db->get('events');
        if (count($query) > 0) {
            echo json_encode(
                    array(
                        "success" => 1,
                        "result" => $out
                    )
            );
        }
    }

    public function getCalendario() {


        return View::make('dashboard.index')
                        ->with('container', 'dashboard.aspirantes.calendario')
                        ->with('submenu_activo', 'Calendario')
                        ->with('menu_activo', 'Aspirantes');
    }

    public function getDetalleagendacalendar() {




        $detalle_agenda = AspirantsSchoolFairs::where('id', Input::get('id'))->get();

        return View::make('dashboard.aspirantes.detalle_calendar')
                        ->with('detalle_agendas', $detalle_agenda);
    }
    public function getGaleria(){


          require_once ('google-api-php-client/src/Google_Client.php');
          require_once ('google-api-php-client/src/contrib/Google_YouTubeService.php');

          /* Set $DEVELOPER_KEY to the "API key" value from the "Access" tab of the
          Google APIs Console <http://code.google.com/apis/console#access>
          Please ensure that you have enabled the YouTube Data API for your project. */
          $DEVELOPER_KEY = 'AIzaSyDOkg-u9jnhP-WnzX5WPJyV1sc5QQrtuyc';

          $client = new Google_Client();
          $client->setDeveloperKey($DEVELOPER_KEY);

          $youtube = new Google_YoutubeService($client);

          try {
            $searchResponse = $youtube->search->listSearch('id,snippet', array(
              // 'q' => $_GET['q'],
              // 'maxResults' => $_GET['maxResults'],
              'q' => 'Corporacion Indoamericana',
              'maxResults' => 10,
            ));

            $videos = '';
            $channels = '';
            // var_dump($searchResponse);
            // exit();
            foreach ($searchResponse['items'] as $searchResult) {
              switch ($searchResult['id']['kind']) {
                case 'youtube#video':
                  // $videos .= sprintf('<li>%s (%s)</li>', $searchResult['snippet']['title'],
                  //   $searchResult['id']['videoId']."<a href=http://www.youtube.com/watch?v=".$searchResult['id']['videoId']." target=_blank>   Watch This Video</a>");
                  // <iframe width="420" height="315" src="https://www.youtube.com/embed/8QjPTcGI6UA" frameborder="0" allowfullscreen></iframe>
                  $videos .= sprintf('<div class="col-md-3 col-sm-4 mix 1 mix_all" style="display: block;  opacity: 1;"><div class="mix-inner"><h3>'.$searchResult['snippet']['thumbnails']['default']['url'].'</h3><iframe width="320" height="215" src="https://www.youtube.com/embed/'.$searchResult['id']['videoId'].'" frameborder="0" fullscreen></iframe><br><spam>'.$searchResult['snippet']['publishedAt'].'</spam></div></div> ');
                  break;
                case 'youtube#channel':
                  $channels .= sprintf('<li>%s (%s)</li>', $searchResult['snippet']['title'],
                    $searchResult['id']['channelId']);
                  break;
               }
            }

           } catch (Google_ServiceException $e) {
            $htmlBody .= sprintf('<p>A service error occurred: <code>%s</code></p>',
              htmlspecialchars($e->getMessage()));
          } catch (Google_Exception $e) {
            $htmlBody .= sprintf('<p>An client error occurred: <code>%s</code></p>',
              htmlspecialchars($e->getMessage()));
          }
        // }
        

        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.aspirantes.vistas_aspirantes.galeria')
        ->with('videos', $videos)
        ->with('searchResponse', $searchResponse)
        ->with('submenu_activo', 'Galerías')
        ->with('menu_activo', 'Aspirantes');
    }

}

?>