<?php
/**
 * 
 */
class TareasController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }
    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }
    public function getConsultausuario(){
        $user = Users::where('id', Input::get('responsable'))->get();
        if (count($user) == 0) {
            echo "1";
            exit();
        }

        foreach ($user as $key) {
            $random = rand(1,1000);
            
            echo '
                <tr id="id_usuario_'.$random.'">
                    <td id="id_usuario_'.$key->id.'"><img alt="" style="border-radius:30px;" width="30" height="30" src="../'.$key->img_min.'"></td>
                    <td>' .$key->name.' '.$key->last_name.'</td>
                    <td><a class="btn" onclick="eliminar_usuario('.$random.', '.$key->id.')"><i class="icon-remove"></a></td>
                </tr>

            ';
        }
    }
    public function getCrear(){
        $user           = Users::find(Auth::user()->id);

        $tareas         = Tasks::get();
        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $procesos       = Processes::get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.crear_tareas')
        ->with('tareas', $tareas)
        ->with('periodicidad', $periodicidad)
        ->with('procesos', $procesos)
        ->with('categorias', $categories)
        ->with('submenu_activo', 'Crear Actividad')
        ->with('menu_activo', 'Actividades');
        
    }
    public function getItems(){
        $user = Users::find(Auth::user()->id);

        $tareas         = Tasks::where('users_id', Auth::user()->id)->get();
        $periodicidad   = TasksPeriodicity::get();
        $procesos       = Processes::get();
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.items_tareas')
        ->with('tareas', $tareas)
        ->with('periodicidad', $periodicidad)
        ->with('procesos', $procesos)
        ->with('submenu_activo', 'Crear Actividad')
        ->with('menu_activo', 'Actividades');
        
    }
    public function getCargarresponsable(){
        $process = Input::get('id_proceso');
        //echo $profile;

        $technical = DB::table('users')
                ->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
                ->join('profiles', 'profiles.id', '=', 'profiles_users.profiles_id')
                ->select('users.id', 'users.name', 'users.last_name')
                ->where('profiles.processes_id', $process)
                ->where('users.status', 1)
                ->orderBy('name', 'asc')
                ->get();

        foreach ($technical as $key) {
            echo "<option value='$key->id'>$key->name $key->last_name</option>";
        }
    }
    public function getIngresartarea(){
        $tarea_tarea        = Input::get('tarea');
        $tarea_desc         = Input::get('tarea_desc');
        $tarea_per          = Input::get('tarea_per');
        $tarea_fecha_ini    = Input::get('tarea_fecha_ini');
        $tarea_fecha_venci  = Input::get('tarea_fecha_venci');
        $noti_correo        = Input::get('noti_correo');

        $responsable        = Input::get('responsable');
        $items              = Input::get('items');
        $prioridad          = Input::get('prioridad');
        $categoria          = Input::get('categoria');
        $fecha_actual       = date('Y-m-d H:i:s');

        $tarea = new Tasks;

        $tarea->tasks               = $tarea_tarea;
        $tarea->description         = $tarea_desc;
        $tarea->tasks_periodicity_id= $tarea_per;
        $tarea->start_date          = $tarea_fecha_ini;
        $tarea->end_date            = $tarea_fecha_venci;
        $tarea->email               = $noti_correo;
        $tarea->users_id            = Auth::user()->id;
        $tarea->tasks_statuses_id   = 1;
        $tarea->priority            = $prioridad;
        $tarea->tasks_categories_id = $categoria;
        

        if($tarea->save()){
            $cons = Tasks::Where("users_id", Auth::user()->id)->get();
            $bandera = $cons->last()->id;
             
            
            foreach ($responsable as $key) {
                
                DB::table('tasks_has_users')->insert(array(
                    array('users_id' => $key, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                
                ));


                $notification = new Notifications;

                $notification->description = 'Le Asigno Una Nueva Actividad';
                $notification->estate = '1';
                $notification->type_notifications_id = '5';
                $notification->user_id = $key;
                $notification->user_id1 = Auth::user()->id;
                $notification->link = 'tareas/asignadas';

                $notification->save();
            }

            if (count($items) != 0) {
                
                foreach ($items as $key2) {
                    
                    DB::table('tasks_items')->insert(array(
                        array('item' => $key2, 'status' => 1, 'required' => 1),
                    
                    ));

                    $cons_item = TasksItems::get();
                    $last_item = $cons_item->last()->id;

                    DB::table('tasks_items_has_tasks')->insert(array(
                        array('tasks_items_id' => $last_item, 'tasks_id' => $bandera),
                    
                    ));             

                    
                }
            }

            $arreglo = array();
            $arreglo[0] = Auth::user();
            $arreglo[1] = $bandera;
            return $arreglo;

        }else{
            echo "2";
        }
    }
    public function getIngresaritem(){
        
        if (Input::get('id_tarea') == "") {
            echo "";
            exit();
        }
        $cons = Tasks::where("id", Input::get('id_tarea'))->get();
        $bandera = $cons->last()->id;
        $fecha_actual = date('Y-m-d H:i:s');
        if (Input::get('item')) {
            
            DB::table('tasks_items')->insert(array(
                array('item' => Input::get('item'), 'status' => 1, 'required' => 1),
            ));

            $cons2 = TasksItems::get();
            $bandera2 = $cons2->last()->id;
            

            DB::table('tasks_items_has_tasks')->insert(array(
                array('tasks_items_id' => $bandera2, 'tasks_id' => $bandera, 'status' => 1),
            ));

        }
            $cons3 = DB::table('tasks_items_has_tasks')
            ->join('tasks_items', 'tasks_items.id', '=', 'tasks_items_has_tasks.tasks_items_id')
            ->where('tasks_items_has_tasks.tasks_id', Input::get('id_tarea'))
            ->where('tasks_items.status',1)
            ->select('tasks_items.item','tasks_items.id')
            ->get();
        foreach ($cons3 as $key) {
            echo '
                 

                <div class="col-md-6"> 
                    <div class="input-group input-medium">
                        <input type="text" class="form-control" value="'.$key->item.'" id="texto_'.$key->id.'">
                        <span class="input-group-btn">
                        
                        <button class="btn btn-danger" type="button" onclick="eliminar_item('.$key->id.')">
                            <i class="icon-trash"></i>
                        </button>
                        <button class="btn btn-success" type="button" onclick="editar_item('.$key->id.')">
                            <i class="icon-edit"></i>
                        </button>
                        </span>
                    </div>
                </div>
                       
            ';  
        }
    }
    public function getDetalletarea(){
        $user = Users::find(Auth::user()->id);

        if (Input::get('id_tarea') == "") {
            echo "";
            exit();
        }

        $tareas = Tasks::where('id', Input::get('id_tarea'))->get();

        if (Input::get('item')) {
            
            echo '
                <div class="col-md-6">
                    <div class="form-group has-success note note-warning">
                        <div class="portlet-body">
                            <div class="form-group"> 
                                <label class="control-label">Descripción de la tarea</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="icon-edit"></i></span>
                                    <textarea rows="3" class="form-control" id="tarea_desc" value="" disabled="">'.$tareas[0]->description.'</textarea>
                                </div> 
                            </div> 
                            
                        </div>
                    </div>
                </div>
                
            ';
            
            exit();
        }
        $responsables = TasksHasUsers::where('tasks_id', Input::get('id_tarea'))->get();
        
        $items = DB::table('tasks_items_has_tasks')
        ->join('tasks_items', 'tasks_items.id', '=', 'tasks_items_has_tasks.tasks_items_id')
        ->leftjoin('tasks_traces', 'tasks_traces.tasks_items_has_tasks_id', '=', 'tasks_items_has_tasks.id')
        ->where('tasks_items_has_tasks.tasks_id', Input::get('id_tarea'))
        ->where('tasks_items.status',1)
        ->whereNull('tasks_traces.observation')
        ->select('tasks_items.item','tasks_items.id', 'tasks_items_has_tasks.id as id_asig')
        ->get();


        $data_item = array();
        for ($i=0; $i < count($items); $i++) { 
            $data_item[$i]['item']      = $items[$i]->item;
            $data_item[$i]['id']        = $items[$i]->id;
            $data_item[$i]['id_asig']   = $items[$i]->id_asig;

            $cons3 = DB::table('tasks_traces')
            ->where('tasks_items_has_tasks_id',$items[$i]->id_asig)
            ->select('tasks_traces.observation','tasks_traces.status')
            ->get();

            if (count($cons3) == "") {
                $data_item[$i]['check']     = '';
                $data_item[$i]['display']   = 'display:none';
                $data_item[$i]['text']      = '';
                $data_item[$i]['success']   = '';
            }else{

                $data_item[$i]['check']     = 'checked';
                $data_item[$i]['display']   = '';
                $data_item[$i]['text']      = $cons3[0]->observation;
                $data_item[$i]['success']   = 'has-success';
            }

        }


        $items2 = DB::table('tasks_items_has_tasks')
        ->join('tasks_items', 'tasks_items.id', '=', 'tasks_items_has_tasks.tasks_items_id')
        ->join('tasks_traces', 'tasks_traces.tasks_items_has_tasks_id', '=', 'tasks_items_has_tasks.id')
        ->where('tasks_items_has_tasks.tasks_id', Input::get('id_tarea'))
        ->where('tasks_items.status',1)
        ->select('tasks_items.item','tasks_items.id', 'tasks_items_has_tasks.id as id_asig')
        ->get();


        $data_realizados = array();
        for ($i=0; $i < count($items2); $i++) { 
            $data_realizados[$i]['item']      = $items2[$i]->item;
            $data_realizados[$i]['id']        = $items2[$i]->id;
            $data_realizados[$i]['id_asig']   = $items2[$i]->id_asig;

            $cons3 = DB::table('tasks_traces')
            ->join('users', 'users.id', '=', 'tasks_traces.users_id')
            ->where('tasks_items_has_tasks_id',$items2[$i]->id_asig)
            ->select('tasks_traces.observation','tasks_traces.status', 'users.name', 'users.last_name')
            ->get();

            if (count($cons3) == "") {
                $data_realizados[$i]['check']     = '';
                $data_realizados[$i]['display']   = 'display:none';
                $data_realizados[$i]['text']      = '';
                $data_realizados[$i]['success']   = '';
                $data_realizados[$i]['estado']    = 'Pendiente';
                $data_realizados[$i]['row_class'] = 'red';
                $data_realizados[$i]['user']      = 'N/A';
            }else{

                $data_realizados[$i]['check']     = 'checked';
                $data_realizados[$i]['display']   = '';
                $data_realizados[$i]['text']      = $cons3[0]->observation;
                $data_realizados[$i]['success']   = 'has-success';
                $data_realizados[$i]['estado']    = 'Finalizado';
                $data_realizados[$i]['row_class'] = 'green';
                $data_realizados[$i]['user']      = $cons3[0]->name." ".$cons3[0]->last_name;
            }

        }



        $procesos       = Processes::get();
        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->Users->name." ".$tareas[$i]->Users->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->Users->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->TasksPeriodicity->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
        }
        
        
        return View::make('dashboard.tareas.detalle_tarea')
        ->with('tareas', $tareas)
        ->with('items', $items)
        ->with('data_item', $data_item)
        ->with('data_realizados', $data_realizados)
        ->with('procesos', $procesos)
        ->with('id_asig', Input::get('id_asig'))
        ->with('responsables', $responsables);
    }
    public function getEliminaritem(){
        $item = TasksItems::find(Input::get('item'));
        $item->status = 0;
        $item->save();
        echo "1";
    }
    public function postConfirmatend(){
        $comentario_rechazado   = Input::get('comentario_rechazado');
        $responsable            = Input::get('responsable');
        $id_gestion             = Input::get('id_gestion');
        $id_tarea               = Input::get('id_tarea');
        $id_asignacion          = Input::get('id_asignacion');
        $fecha_actual           = date('Y-m-d H:i:s');

        if ($id_gestion == 1) {
            $tasks = Tasks::find($id_tarea);
            $tasks->tasks_statuses_id = 3;
            $tasks->save();


            $tasks = new TasksComents;
            $tasks->comment     = "Atendiendo Actividad";
            $tasks->date        = $fecha_actual;
            $tasks->tasks_id    = $id_tarea;
            $tasks->users_id    = Auth::user()->id;
            $tasks->save();
            $status = 0;

        }elseif ($id_gestion == 2) {
            $tasks = new TasksComents;
            $tasks->comment     = $comentario_rechazado;
            $tasks->date        = $fecha_actual;
            $tasks->tasks_id    = $id_tarea;
            $tasks->users_id    = Auth::user()->id;
            $tasks->save();
            $status = 0;

            DB::update('update tasks_has_users set status  = ? where id = ?', array($status ,$id_asignacion));
            
        }elseif ($id_gestion == 3) {
            $status = 0;

            $tasks = new TasksComents;
            $tasks->comment     = "Actividad escalada";
            $tasks->date        = $fecha_actual;
            $tasks->tasks_id    = $id_tarea;
            $tasks->users_id    = Auth::user()->id;
            $tasks->save();
            $status = 0;


            DB::update('update tasks_has_users set status  = ? where id = ?', array($status ,$id_asignacion));


            DB::table('tasks_has_users')->insert(array(
                array('users_id' => $responsable, 'tasks_id' => $id_tarea, 'date_sign' => $fecha_actual),
            ));

            $notification = new Notifications;

            $notification->description = 'Le Escalo Una Nueva Actividad';
            $notification->estate = '1';
            $notification->type_notifications_id = '5';
            $notification->user_id = $responsable;
            $notification->user_id1 = Auth::user()->id;
            $notification->link = 'tareas/asignadas';

            $notification->save();
        }

        echo "1";
    }
    public function getDetalletareaadmin(){
        $user = Users::find(Auth::user()->id);
        $procesos       = Processes::get();
        if (Input::get('id_tarea') == "") {
            echo "";
            exit();
        }

        $tareas = Tasks::where('id', Input::get('id_tarea'))->get();
        $responsables = TasksHasUsers::where('tasks_id', Input::get('id_tarea'))->get();
        $estados = TasksStatuses::get();


        $items = DB::table('tasks_items_has_tasks')
        ->join('tasks_items', 'tasks_items.id', '=', 'tasks_items_has_tasks.tasks_items_id')
        ->where('tasks_items_has_tasks.tasks_id', Input::get('id_tarea'))
        ->where('tasks_items.status',1)
        ->select('tasks_items.item','tasks_items.id', 'tasks_items_has_tasks.id as id_asig')
        ->get();


        $data_item = array();
        for ($i=0; $i < count($items); $i++) { 
            $data_item[$i]['item']      = $items[$i]->item;
            $data_item[$i]['id']        = $items[$i]->id;
            $data_item[$i]['id_asig']   = $items[$i]->id_asig;

            $cons3 = DB::table('tasks_traces')
            ->join('users', 'users.id', '=', 'tasks_traces.users_id')
            ->where('tasks_items_has_tasks_id',$items[$i]->id_asig)
            ->select('tasks_traces.observation','tasks_traces.status', 'users.name', 'users.last_name')
            ->get();

            if (count($cons3) == "") {
                $data_item[$i]['check']     = '';
                $data_item[$i]['display']   = 'display:none';
                $data_item[$i]['text']      = '';
                $data_item[$i]['success']   = '';
                $data_item[$i]['estado']    = 'Pendiente';
                $data_item[$i]['row_class'] = 'red';
                $data_item[$i]['user']      = 'N/A';
            }else{

                $data_item[$i]['check']     = 'checked';
                $data_item[$i]['display']   = '';
                $data_item[$i]['text']      = $cons3[0]->observation;
                $data_item[$i]['success']   = 'has-success';
                $data_item[$i]['estado']    = 'Finalizado';
                $data_item[$i]['row_class'] = 'green';
                $data_item[$i]['user']      = $cons3[0]->name." ".$cons3[0]->last_name;
            }

        }

        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->Users->name." ".$tareas[$i]->Users->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->Users->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->TasksPeriodicity->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
        }
        
        
        return View::make('dashboard.tareas.detalle_tarea_admin')
        ->with('tareas', $tareas)
        ->with('items', $items)
        ->with('data_item', $data_item)
        ->with('estados', $estados)
        ->with('procesos', $procesos)
        ->with('id_tarea', Input::get('id_tarea'))
        ->with('responsables', $responsables);
    }
    public function getEditaritems(){
        $item = TasksItems::find(Input::get('item'));
        $item->item = Input::get('texto');
        $item->save();
        echo "1";
    }
    public function getVer(){
        $user           = Users::find(Auth::user()->id);
        $users          = $tareas = DB::table('tasks_has_users')
                            ->join('users', 'users.id', '=', 'tasks_has_users.users_id')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->select('users.name', 'users.last_name', 'users.img_min',
                             'users.id')
                            ->where('tasks.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 2)
                            ->where('tasks.tasks_statuses_id', '<>', 4)
                            ->orderBy('users.name')
                            ->distinct()
                            ->get();

        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        $progresos      = DB::table('tasks')
                            ->select('tasks.progress')
                            ->where('tasks.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 2)
                            ->where('tasks.tasks_statuses_id', '<>', 4)
                            ->orderBy('tasks.progress', 'asc')
                            ->distinct()
                            ->get();

        $responsable = Input::get('responsable');
        if (isset($_GET['responsable']) && !empty($_GET['responsable'])) {
            $responsable_campo = 'tasks_has_users.users_id';
            $responsable_signo = '=';
            $responsable_var   = $responsable;
        } else {
            $responsable_campo = 'tasks.id';
            $responsable_signo = '<>';
            $responsable_var = '-1';
        }
        

        
        $tareas = DB::table('tasks')
        ->join('users', 'users.id', '=', 'tasks.users_id')
        ->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
        ->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
        ->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
        ->join('tasks_has_users', 'tasks_has_users.tasks_id', '=', 'tasks.id')
        ->select('users.name', 'users.last_name', 'users.img_min',
         'tasks.id','tasks.tasks', 'priority', 'description', 'start_date', 'progress', 'category', 'periodicity', 'tasks_statuses.status', 'tasks.tasks_statuses_id')
        ->where($responsable_campo, $responsable_signo, $responsable_var)
        ->where('tasks.id', 'LIKE', '%' . Input::get('code') . '%')
        ->where('tasks.tasks_periodicity_id', 'LIKE', '%' . Input::get('periodicidad') . '%')
        ->where('tasks.tasks_categories_id', 'LIKE', '%' . Input::get('categoria') . '%')
        ->where('tasks.progress', 'LIKE', '%' . Input::get('progreso') . '%')
        ->where('tasks.users_id', Auth::user()->id)
        ->where('tasks.tasks_statuses_id', '<>', 2)
        ->where('tasks.tasks_statuses_id', '<>', 4)
        ->orderBy('tasks.start_date', Input::get('orden'))
        ->distinct()
        ->get();



        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->status;
            $data[$i]['categoria']      = $tareas[$i]->category;

            $comentarios = TasksComents::where('tasks_id', $tareas[$i]->id)->get();
            
            $data[$i]['comentarios']      = count($comentarios);
            
            $usuarios = TasksHasUsers::where('tasks_id', $tareas[$i]->id)->get();
            $texto = "";

            $variable2 = "";
            
            
            foreach ($usuarios as $key) {
                
                $texto = $key->Users->name." ".$key->Users->last_name." - ";
                $variable2 = $variable2.$texto;
            }
            $data[$i]['tecnico']      = $variable2;
            $data[$i]['row_color'] = '';
            if ( $tareas[$i]->tasks_statuses_id == 3 ) $data[$i]['row_color'] = 'yellow';
            if ( $tareas[$i]->progress == 100 ) $data[$i]['row_color'] = 'green';

            if ( $tareas[$i]->progress <= 35 ) $data[$i]['progress_class'] = 'progress-bar-danger';
            if ( $tareas[$i]->progress > 35 &&  $tareas[$i]->progress <= 70 ) $data[$i]['progress_class'] = 'progress-bar-warning';
            if ( $tareas[$i]->progress > 70 ) $data[$i]['progress_class'] = 'progress-bar-success';
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
        }
        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.tareas.tabla_ver_tareas')
            ->with('tareas', $data)
            ->with('usuarios', $users);
            exit();
        }
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.ver_tareas')
        ->with('tareas', $data)
        ->with('usuarios', $users)
        ->with('periodicidad', $periodicidad)
        ->with('categories', $categories)
        ->with('progresos', $progresos)
        ->with('submenu_activo', 'Ver Actividades')
        ->with('menu_activo', 'Actividades');
    }
    public function getAdmin(){
        $user           = Users::find(Auth::user()->id);
        $users          = $tareas = DB::table('tasks_has_users')
                            ->join('users', 'users.id', '=', 'tasks_has_users.users_id')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->select('users.name', 'users.last_name', 'users.img_min',
                             'users.id')
                            ->where('tasks.tasks_statuses_id', '<>', 2)
                            ->where('tasks.tasks_statuses_id', '<>', 4)
                            ->orderBy('users.name')
                            ->distinct()
                            ->get();

        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        $progresos      = DB::table('tasks')
                            ->select('tasks.progress')
                            ->where('tasks.tasks_statuses_id', '<>', 2)
                            ->where('tasks.tasks_statuses_id', '<>', 4)
                            ->orderBy('tasks.progress', 'asc')
                            ->distinct()
                            ->get();

        $responsable = Input::get('responsable');
        $categoria = Input::get('categoria');
        if (isset($_GET['responsable']) && !empty($_GET['responsable'])) {
            $responsable_campo = 'tasks_has_users.users_id';
            $responsable_signo = '=';
            $responsable_var   = $responsable;
        } else {
            $responsable_campo = 'tasks.id';
            $responsable_signo = '<>';
            $responsable_var = '-1';
        }

        if (isset($_GET['categoria']) && !empty($_GET['categoria'])) {
            $categoria_campo = 'tasks.tasks_categories_id';
            $categoria_signo = '=';
            $categoria_var   = $categoria;
        } else {
            $categoria_campo = 'tasks.id';
            $categoria_signo = '<>';
            $categoria_var = '-2';
        }
        

        
        $tareas = DB::table('tasks')
        ->join('users', 'users.id', '=', 'tasks.users_id')
        ->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
        ->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
        ->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
        ->join('tasks_has_users', 'tasks_has_users.tasks_id', '=', 'tasks.id')
        ->select('users.name', 'users.last_name', 'users.img_min',
         'tasks.id','tasks.tasks', 'priority', 'description', 'start_date', 'progress', 'category', 'periodicity', 'tasks_statuses.status', 'tasks.tasks_statuses_id')
        ->where($responsable_campo, $responsable_signo, $responsable_var)
        ->where('tasks.id', 'LIKE', '%' . Input::get('code') . '%')
        ->where('tasks.tasks_periodicity_id', 'LIKE', '%' . Input::get('periodicidad') . '%')
        ->where($categoria_campo, $categoria_signo, $categoria_var)
        ->where('tasks.progress', 'LIKE', '%' . Input::get('progreso') . '%')
        // ->where('tasks.users_id', Auth::user()->id)
        ->where('tasks.tasks_statuses_id', '<>', 2)
        ->where('tasks.tasks_statuses_id', '<>', 4)
        ->orderBy('tasks.start_date', Input::get('orden'))
        ->distinct()
        ->get();



        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->status;
            $data[$i]['categoria']      = $tareas[$i]->category;


            $comentarios = TasksComents::where('tasks_id', $tareas[$i]->id)->get();
            
            $data[$i]['comentarios']      = count($comentarios);
            
            $usuarios = TasksHasUsers::where('tasks_id', $tareas[$i]->id)->get();
            $texto = "";

            $variable2 = "";
            
            
            foreach ($usuarios as $key) {
                
                $texto = $key->Users->name." ".$key->Users->last_name." - ";
                $variable2 = $variable2.$texto;
            }
            $data[$i]['tecnico']      = $variable2;
            $data[$i]['row_color'] = '';
            if ( $tareas[$i]->tasks_statuses_id == 3 ) $data[$i]['row_color'] = 'yellow';
            if ( $tareas[$i]->progress == 100 ) $data[$i]['row_color'] = 'green';

            if ( $tareas[$i]->progress <= 35 ) $data[$i]['progress_class'] = 'progress-bar-danger';
            if ( $tareas[$i]->progress > 35 &&  $tareas[$i]->progress <= 70 ) $data[$i]['progress_class'] = 'progress-bar-warning';
            if ( $tareas[$i]->progress > 70 ) $data[$i]['progress_class'] = 'progress-bar-success';
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
        }
        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.tareas.tabla_ver_tareas')
            ->with('tareas', $data)
            ->with('usuarios', $users);
            exit();
        }
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.admin_tareas')
        ->with('tareas', $data)
        ->with('usuarios', $users)
        ->with('periodicidad', $periodicidad)
        ->with('categories', $categories)
        ->with('progresos', $progresos)
        ->with('submenu_activo', 'Admin de Actividades')
        ->with('menu_activo', 'Actividades');
    }
    public function getHistorico(){
        $user           = Users::find(Auth::user()->id);
        $users          = $tareas = DB::table('tasks_has_users')
                            ->join('users', 'users.id', '=', 'tasks_has_users.users_id')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->select('users.name', 'users.last_name', 'users.img_min',
                             'users.id')
                            ->where('tasks.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 1)
                            ->where('tasks.tasks_statuses_id', '<>', 3)
                            ->orderBy('users.name')
                            ->distinct()
                            ->get();

        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        $progresos      = DB::table('tasks')
                            ->select('tasks.progress')
                            ->where('tasks.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 1)
                            ->where('tasks.tasks_statuses_id', '<>', 3)
                            ->orderBy('tasks.progress', 'asc')
                            ->distinct()
                            ->get();

        $responsable = Input::get('responsable');
        if (isset($_GET['responsable']) && !empty($_GET['responsable'])) {
            $responsable_campo = 'tasks_has_users.users_id';
            $responsable_signo = '=';
            $responsable_var   = $responsable;
        } else {
            $responsable_campo = 'tasks.id';
            $responsable_signo = '<>';
            $responsable_var = '-1';
        }
        

        
        $tareas = DB::table('tasks')
        ->join('users', 'users.id', '=', 'tasks.users_id')
        ->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
        ->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
        ->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
        ->join('tasks_has_users', 'tasks_has_users.tasks_id', '=', 'tasks.id')
        ->select('users.name', 'users.last_name', 'users.img_min', 'tasks.rating',
         'tasks.id','tasks.tasks', 'priority', 'description', 'start_date', 'progress', 'category', 'periodicity', 'tasks_statuses.status', 'tasks.tasks_statuses_id')
        ->where($responsable_campo, $responsable_signo, $responsable_var)
        ->where('tasks.id', 'LIKE', '%' . Input::get('code') . '%')
        ->where('tasks.tasks_periodicity_id', 'LIKE', '%' . Input::get('periodicidad') . '%')
        ->where('tasks.tasks_categories_id', 'LIKE', '%' . Input::get('categoria') . '%')
        ->where('tasks.progress', 'LIKE', '%' . Input::get('progreso') . '%')
        ->where('tasks.users_id', Auth::user()->id)
        ->where('tasks.tasks_statuses_id', '<>', 1)
        ->where('tasks.tasks_statuses_id', '<>', 3)
        ->orderBy('tasks.start_date', Input::get('orden'))
        ->distinct()
        ->get();



        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->status;
            $data[$i]['categoria']      = $tareas[$i]->category;
            $data[$i]['rating']         = $tareas[$i]->rating;

            $comentarios = TasksComents::where('tasks_id', $tareas[$i]->id)->get();
            
            $data[$i]['comentarios']      = count($comentarios);
            
            $usuarios = TasksHasUsers::where('tasks_id', $tareas[$i]->id)->get();
            $texto = "";

            $variable2 = "";
            
            
            foreach ($usuarios as $key) {
                
                $texto = $key->Users->name." ".$key->Users->last_name." - ";
                $variable2 = $variable2.$texto;
            }
            $data[$i]['tecnico']      = $variable2;
            $data[$i]['row_color'] = '';
            if ( $tareas[$i]->tasks_statuses_id == 3 ) $data[$i]['row_color'] = 'yellow';
            if ( $tareas[$i]->progress == 100 ) $data[$i]['row_color'] = 'green';

            if ( $tareas[$i]->progress <= 35 ) $data[$i]['progress_class'] = 'progress-bar-danger';
            if ( $tareas[$i]->progress > 35 &&  $tareas[$i]->progress <= 70 ) $data[$i]['progress_class'] = 'progress-bar-warning';
            if ( $tareas[$i]->progress > 70 ) $data[$i]['progress_class'] = 'progress-bar-success';
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
        }
        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.tareas.tabla_ver_tareas_hist')
            ->with('tareas', $data)
            ->with('usuarios', $users)
            ->with('periodicidad', $periodicidad)
            ->with('categories', $categories)
            ->with('progresos', $progresos);
            exit();
        }
        
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.historico_tareas')
        ->with('tareas', $data)
        ->with('usuarios', $users)
        ->with('periodicidad', $periodicidad)
        ->with('categories', $categories)
        ->with('progresos', $progresos)
        ->with('submenu_activo', 'Ver Actividades')
        ->with('menu_activo', 'Actividades'); 
    }
    public function getAdminhistorico(){
        $user           = Users::find(Auth::user()->id);
        $users          = DB::table('tasks_has_users')
                            ->join('users', 'users.id', '=', 'tasks_has_users.users_id')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->select('users.name', 'users.last_name', 'users.img_min',
                             'users.id')
                            
                            ->where('tasks.tasks_statuses_id', '<>', 1)
                            ->where('tasks.tasks_statuses_id', '<>', 3)
                            ->orderBy('users.name')
                            ->distinct()
                            ->get();

        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        $progresos      = DB::table('tasks')
                            ->select('tasks.progress')
                            ->where('tasks.tasks_statuses_id', '<>', 1)
                            ->where('tasks.tasks_statuses_id', '<>', 3)
                            ->orderBy('tasks.progress', 'asc')
                            ->distinct()
                            ->get();

        $responsable = Input::get('responsable');
        $categoria = Input::get('categoria');
        if (isset($_GET['responsable']) && !empty($_GET['responsable'])) {
            $responsable_campo = 'tasks_has_users.users_id';
            $responsable_signo = '=';
            $responsable_var   = $responsable;
        } else {
            $responsable_campo = 'tasks.id';
            $responsable_signo = '<>';
            $responsable_var = '-1';
        }
        if (isset($_GET['categoria']) && !empty($_GET['categoria'])) {
            $categoria_campo = 'tasks.tasks_categories_id';
            $categoria_signo = '=';
            $categoria_var   = $categoria;
        } else {
            $categoria_campo = 'tasks.id';
            $categoria_signo = '<>';
            $categoria_var = '-2';
        }
        

        
        $tareas = DB::table('tasks')
        ->join('users', 'users.id', '=', 'tasks.users_id')
        ->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
        ->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
        ->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
        ->join('tasks_has_users', 'tasks_has_users.tasks_id', '=', 'tasks.id')
        ->select('users.name', 'users.last_name', 'users.img_min', 'tasks.rating',
         'tasks.id','tasks.tasks', 'priority', 'description', 'start_date', 'progress', 'category', 'periodicity', 'tasks_statuses.status', 'tasks.tasks_statuses_id')
        ->where($responsable_campo, $responsable_signo, $responsable_var)
        ->where('tasks.id', 'LIKE', '%' . Input::get('code') . '%')
        ->where('tasks.tasks_periodicity_id', 'LIKE', '%' . Input::get('periodicidad') . '%')
        ->where($categoria_campo, $categoria_signo, $categoria_var)
        ->where('tasks.progress', 'LIKE', '%' . Input::get('progreso') . '%')
        // ->where('tasks.users_id', Auth::user()->id)
        ->where('tasks.tasks_statuses_id', '<>', 1)
        ->where('tasks.tasks_statuses_id', '<>', 3)
        ->orderBy('tasks.start_date', Input::get('orden'))
        ->distinct()
        ->get();



        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->status;
            $data[$i]['categoria']      = $tareas[$i]->category;
            $data[$i]['rating']         = $tareas[$i]->rating;

            $comentarios = TasksComents::where('tasks_id', $tareas[$i]->id)->get();
            
            $data[$i]['comentarios']      = count($comentarios);
            
            $usuarios = TasksHasUsers::where('tasks_id', $tareas[$i]->id)->get();
            $texto = "";

            $variable2 = "";
            
            
            foreach ($usuarios as $key) {
                
                $texto = $key->Users->name." ".$key->Users->last_name." - ";
                $variable2 = $variable2.$texto;
            }
            $data[$i]['tecnico']      = $variable2;
            $data[$i]['row_color'] = '';
            if ( $tareas[$i]->tasks_statuses_id == 3 ) $data[$i]['row_color'] = 'yellow';
            if ( $tareas[$i]->progress == 100 ) $data[$i]['row_color'] = 'green';

            if ( $tareas[$i]->progress <= 35 ) $data[$i]['progress_class'] = 'progress-bar-danger';
            if ( $tareas[$i]->progress > 35 &&  $tareas[$i]->progress <= 70 ) $data[$i]['progress_class'] = 'progress-bar-warning';
            if ( $tareas[$i]->progress > 70 ) $data[$i]['progress_class'] = 'progress-bar-success';
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
            // $data[$i]['tarea']          = $tarea->tasks;
        }
        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.tareas.tabla_ver_tareas_hist')
            ->with('tareas', $data)
            ->with('usuarios', $users)
            ->with('periodicidad', $periodicidad)
            ->with('categories', $categories)
            ->with('progresos', $progresos);
            exit();
        }
        
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.admin_historico_tareas')
        ->with('tareas', $data)
        ->with('usuarios', $users)
        ->with('periodicidad', $periodicidad)
        ->with('categories', $categories)
        ->with('progresos', $progresos)
        ->with('submenu_activo', 'Admin de Actividades')
        ->with('menu_activo', 'Actividades'); 
    }
    public function getAsignadas(){
        $user = Users::find(Auth::user()->id);

        $users          = DB::table('tasks_has_users')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->join('users', 'users.id', '=', 'tasks.users_id')
                            ->select('users.name', 'users.last_name', 'users.id')
                            ->where('tasks_has_users.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 2)
                            ->where('tasks.tasks_statuses_id', '<>', 4)
                            ->orderBy('users.name', 'asc')
                            ->distinct()
                            ->get();

        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        $progresos      = DB::table('tasks_has_users')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->select('tasks.progress')
                            ->where('tasks_has_users.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 2)
                            ->where('tasks.tasks_statuses_id', '<>', 4)
                            ->orderBy('tasks.progress', 'asc')
                            ->distinct()
                            ->get();

        $responsable = Input::get('responsable');
        if (isset($_GET['responsable']) && !empty($_GET['responsable'])) {
            $responsable_campo = 'tasks.users_id';
            $responsable_signo = '=';
            $responsable_var   = $responsable;
        } else {
            $responsable_campo = 'tasks.id';
            $responsable_signo = '<>';
            $responsable_var = '-1';
        }

        $tareas = DB::table('tasks_has_users')
        ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
        ->join('users', 'users.id', '=', 'tasks.users_id')
        ->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
        ->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
        ->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
        ->select('tasks.id', 'users.name', 'users.last_name', 'users.img_min','tasks.tasks', 'tasks.description', 'tasks_periodicities.periodicity',
            'tasks.start_date', 'tasks_statuses.status','tasks.progress','tasks_categories.category', 'tasks_has_users.id as id_asig', 'tasks.tasks_statuses_id')
        ->where('tasks_has_users.users_id', Auth::user()->id)
        ->where('tasks_has_users.status', 1)
        ->where('tasks.tasks_statuses_id', '<>', 2)
        ->where('tasks.tasks_statuses_id', '<>', 4)
        ->where('tasks.id', 'LIKE', '%' . Input::get('code') . '%')
        ->where('tasks.tasks_periodicity_id', 'LIKE', '%' . Input::get('periodicidad') . '%')
        ->where('tasks.tasks_categories_id', 'LIKE', '%' . Input::get('categoria') . '%')
        ->where('tasks.progress', 'LIKE', '%' . Input::get('progreso') . '%')
        ->where($responsable_campo, $responsable_signo, $responsable_var)
        ->get();


        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->status;
            $data[$i]['categoria']      = $tareas[$i]->category;
            $data[$i]['id_asig']        = $tareas[$i]->id_asig;

            $comentarios = TasksComents::where('tasks_id', $tareas[$i]->id)->get();
            
            $data[$i]['comentarios']      = count($comentarios);

            $data[$i]['row_color'] = '';
            if ( $tareas[$i]->tasks_statuses_id == 3 ) $data[$i]['row_color'] = 'yellow';
            if ( $tareas[$i]->progress == 100 ) $data[$i]['row_color'] = 'green';
            if ( $tareas[$i]->progress <= 35 ) $data[$i]['progress_class'] = 'progress-bar-danger';
            if ( $tareas[$i]->progress > 35 &&  $tareas[$i]->progress <= 70 ) $data[$i]['progress_class'] = 'progress-bar-warning';
            if ( $tareas[$i]->progress > 70 ) $data[$i]['progress_class'] = 'progress-bar-success';
        }



        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.tareas.tabla_mis_tareas')
            ->with('tareas', $data);
            
            exit();
        }
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.mis_tareas')
        ->with('tareas', $data)
        ->with('usuarios', $users)
        ->with('periodicidad', $periodicidad)
        ->with('categories', $categories)
        ->with('progresos', $progresos)
        ->with('submenu_activo', 'Mis Actividades')
        ->with('menu_activo', 'Actividades');
    }
    public function getCargarseguimiento(){
        $user = Users::find(Auth::user()->id);

        $tareas = Tasks::where('id', Input::get('id_tarea'))->get();
        $responsables = TasksHasUsers::where('tasks_id', Input::get('id_tarea'))->get();

        $items = DB::table('tasks_items_has_tasks')
        ->join('tasks_items', 'tasks_items.id', '=', 'tasks_items_has_tasks.tasks_items_id')
        ->where('tasks_items_has_tasks.tasks_id', Input::get('id_tarea'))
        ->where('tasks_items.status',1)
        ->select('tasks_items.item','tasks_items.id', 'tasks_items_has_tasks.id as id_asig')
        ->get();

        $comentarios = TasksComents::where('tasks_id', Input::get('id_tarea'))->get();
        



        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->Users->name." ".$tareas[$i]->Users->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->Users->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->TasksPeriodicity->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->tasks_statuses_id;

        }

        $data_c = array();
        for ($i=0; $i < count($comentarios); $i++) { 
            $data_c[$i]['codigo']   = 'TR' . str_pad($comentarios[$i]->id, 6, "0", STR_PAD_LEFT);
            $data_c[$i]['comment']  = $comentarios[$i]->comment;
            $data_c[$i]['date']     = $comentarios[$i]->date;
            $data_c[$i]['img_user'] = $comentarios[$i]->Users->img_min;
            $data_c[$i]['user']     = $comentarios[$i]->Users->name." ".$comentarios[$i]->Users->last_name;
            
            if ($comentarios[$i]->users_id != Auth::user()->id) {
                $data_c[$i]['align']    = 'in';    
            }else{
                $data_c[$i]['align']    = 'out';    
            }
            

        }
        
        
        return View::make('dashboard.tareas.seguimiento_tarea')
        ->with('tareas', $data)
        ->with('items', $items)
        ->with('comentarios', $data_c)
        
        ->with('responsables', $responsables);
    }
    public function postGuardarseguimiento(){
        $id_item        = Input::get('id_item');
        $descripciones  = Input::get('descripciones');
        $progress       = Input::get('progress');
        $id             = Input::get('id');

        $i = 0;

        if (count($id_item) != 0) {
            foreach ($id_item as $key) {
                
                DB::table('tasks_traces')->insert(array(
                    array('observation' => $descripciones[$i], 'status' => 1, 'tasks_items_has_tasks_id' => $key, 'users_id' => Auth::user()->id),
                ));

                $i++;
            }
            
        }


        $tasks = Tasks::find(Input::get('id'));
        $tasks->progress = $progress;

        if ($tasks->save()) {

            if ($progress == 100) {
                // if ($tasks->email == 1) {
                    
                    // funcion para enviar email al usuario
                    $fromEmail = "Indoamericana@indoamericana.edu.co";
                    $fromName = "Indoamericana";
                        
                    $correo_envio = $tasks->Users->email_institutional;
                    $data= array(
                        'tareas'            => $tasks
                        

                    );
                    
                    Mail::send('dashboard.tareas.correo_cierre_actividad', $data, function ($message) use ($fromName, $fromEmail, $correo_envio){
                        $message->subject('Cierre de Actividad');
                        $message->to($correo_envio);
                        $message->from($fromEmail, $fromName);
                    });
                // }

                 //fin envio email
                    
                $notification = new Notifications;

                $notification->description = 'Ha Finalizado una Actividad';
                $notification->estate = '1';
                $notification->type_notifications_id = '5';
                $notification->user_id = $tasks->users_id;
                $notification->user_id1 = Auth::user()->id;
                $notification->link = 'tareas/ver';

                $notification->save();

                $arreglo = array();
                $arreglo[0] = Auth::user();
                $arreglo[1] = 1;
                $arreglo[2] = $id;
                return $arreglo;





            }else{
                $notification = new Notifications;

                $notification->description = 'Ha Hecho un avance de Actividad';
                $notification->estate = '1';
                $notification->type_notifications_id = '5';
                $notification->user_id = $tasks->users_id;
                $notification->user_id1 = Auth::user()->id;
                $notification->link = 'tareas/ver';

                $notification->save();


                $arreglo = array();
                $arreglo[0] = Auth::user();
                $arreglo[1] = 2;
                $arreglo[2] = $id;
                return $arreglo;
                
            }
        }else{
            echo "2";
        }
        
    }
    public function postNuevocomentario(){
        $id_item        = Input::get('id_item');
        $observacion    = Input::get('observacion');

        $fecha_actual = date('Y-m-d H:i:s');
        
        $tasks = new TasksComents;
        $tasks->comment     = $observacion;
        $tasks->date        = $fecha_actual;
        $tasks->tasks_id    = $id_item;
        $tasks->users_id    = Auth::user()->id;
        $tasks->save();

        echo "1";
    }
    public function getMihistorico(){
        
        $user = Users::find(Auth::user()->id);

        $users          = DB::table('tasks_has_users')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->join('users', 'users.id', '=', 'tasks.users_id')
                            ->select('users.name', 'users.last_name', 'users.id')
                            ->where('tasks_has_users.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 1)
                            ->where('tasks.tasks_statuses_id', '<>', 3)
                            ->orderBy('users.name', 'asc')
                            ->distinct()
                            ->get();

        $periodicidad   = TasksPeriodicity::orderby('periodicity', 'asc')->get();
        $categories     = TasksCategories::orderby('category', 'asc')->get();
        $progresos      = DB::table('tasks_has_users')
                            ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
                            ->select('tasks.progress')
                            ->where('tasks_has_users.users_id', Auth::user()->id)
                            ->where('tasks.tasks_statuses_id', '<>', 1)
                            ->where('tasks.tasks_statuses_id', '<>', 3)
                            ->orderBy('tasks.progress', 'asc')
                            ->distinct()
                            ->get();

        $responsable = Input::get('responsable');
        if (isset($_GET['responsable']) && !empty($_GET['responsable'])) {
            $responsable_campo = 'tasks.users_id';
            $responsable_signo = '=';
            $responsable_var   = $responsable;
        } else {
            $responsable_campo = 'tasks.id';
            $responsable_signo = '<>';
            $responsable_var = '-1';
        }

        $tareas = DB::table('tasks_has_users')
        ->join('tasks', 'tasks.id', '=', 'tasks_has_users.tasks_id')
        ->join('users', 'users.id', '=', 'tasks.users_id')
        ->join('tasks_statuses', 'tasks_statuses.id', '=', 'tasks.tasks_statuses_id')
        ->join('tasks_periodicities', 'tasks_periodicities.id', '=', 'tasks.tasks_periodicity_id')
        ->join('tasks_categories', 'tasks_categories.id', '=', 'tasks.tasks_categories_id')
        ->select('tasks.id', 'users.name', 'users.last_name', 'users.img_min','tasks.tasks', 'tasks.description', 'tasks_periodicities.periodicity', 'tasks.rating',
            'tasks.start_date', 'tasks_statuses.status','tasks.progress','tasks_categories.category', 'tasks_has_users.id as id_asig', 'tasks.tasks_statuses_id')
        ->where('tasks_has_users.users_id', Auth::user()->id)
        // ->where('tasks_has_users.status', 0)
        ->where('tasks.tasks_statuses_id', '<>', 1)
        ->where('tasks.tasks_statuses_id', '<>', 3)
        ->where('tasks.id', 'LIKE', '%' . Input::get('code') . '%')
        ->where('tasks.tasks_periodicity_id', 'LIKE', '%' . Input::get('periodicidad') . '%')
        ->where('tasks.tasks_categories_id', 'LIKE', '%' . Input::get('categoria') . '%')
        ->where('tasks.progress', 'LIKE', '%' . Input::get('progreso') . '%')
        ->where($responsable_campo, $responsable_signo, $responsable_var)
        ->get();


        $data = array();
        for ($i=0; $i < count($tareas); $i++) { 
            $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
            $data[$i]['id']             = $tareas[$i]->id;
            $data[$i]['usuario']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['tecnico']        = $tareas[$i]->name." ".$tareas[$i]->last_name;
            $data[$i]['img_usuario']    = $tareas[$i]->img_min;
            $data[$i]['tarea']          = $tareas[$i]->tasks;
            $data[$i]['descripcion']    = $tareas[$i]->description;
            $data[$i]['periodicidad']   = $tareas[$i]->periodicity;
            $data[$i]['f_inicio']       = $tareas[$i]->start_date;
            $data[$i]['progreso']       = $tareas[$i]->progress;
            $data[$i]['estado']         = $tareas[$i]->status;
            $data[$i]['categoria']      = $tareas[$i]->category;
            $data[$i]['id_asig']        = $tareas[$i]->id_asig;
            $data[$i]['rating']         = $tareas[$i]->rating;

            $comentarios = TasksComents::where('tasks_id', $tareas[$i]->id)->get();
            
            $data[$i]['comentarios']      = count($comentarios);

            $data[$i]['row_color'] = '';
            if ( $tareas[$i]->tasks_statuses_id == 3 ) $data[$i]['row_color'] = 'yellow';
            if ( $tareas[$i]->progress == 100 ) $data[$i]['row_color'] = 'green';
            if ( $tareas[$i]->progress <= 35 ) $data[$i]['progress_class'] = 'progress-bar-danger';
            if ( $tareas[$i]->progress > 35 &&  $tareas[$i]->progress <= 70 ) $data[$i]['progress_class'] = 'progress-bar-warning';
            if ( $tareas[$i]->progress > 70 ) $data[$i]['progress_class'] = 'progress-bar-success';
        }
        if (Input::get('filtro') && Input::get('filtro') == 1) {
            return View::make('dashboard.tareas.tabla_ver_tareas_hist')
            ->with('tareas', $data);
            
            exit();
        }
        
        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.mi_historico_tareas')
        ->with('tareas', $data)
        ->with('usuarios', $users)
        ->with('periodicidad', $periodicidad)
        ->with('categories', $categories)
        ->with('progresos', $progresos)
        ->with('submenu_activo', 'Mis Actividades')
        ->with('menu_activo', 'Actividades'); 
    }
    public function getEstadisticas() {
            
        $sql_usuarios = "SELECT users.name, users.last_name, COUNT( * ) AS dato FROM `tasks` 
        join users on (users.id = tasks.users_id)
        GROUP BY users.name ORDER BY dato DESC";


        $sql_estados = "SELECT tasks_statuses.status, COUNT( * ) AS dato FROM `tasks` 
        join tasks_statuses on (tasks_statuses.id = tasks.tasks_statuses_id)
        GROUP BY status ORDER BY dato DESC";

        $sql_periodicities = "SELECT periodicity, COUNT( * ) AS dato FROM `tasks` 
        join tasks_periodicities on (tasks_periodicities.id = tasks.tasks_periodicity_id)
        GROUP BY periodicity ORDER BY dato DESC";

        $sql_procesos = "SELECT processes.acronyms,COUNT( * ) AS dato FROM `equipments`
        join equipment_inventories_users on (equipment_inventories_users.equipments_id = equipments.id)
        join users on (equipment_inventories_users.users_id = users.id)
        join profiles_users on (profiles_users.users_id = users.id)
        join profiles on (profiles.id = profiles_users.profiles_id)
        join processes on (processes.id = profiles.processes_id)
        GROUP BY processes.acronyms ORDER BY dato DESC";

        $sql_impresoras = "SELECT processes.acronyms,COUNT( * ) AS dato FROM equipment_inventories
        join equipments_has_equipment_inventories on (equipments_has_equipment_inventories.equipment_inventories_id = equipment_inventories.id)
        join equipments on (equipments_has_equipment_inventories.equipments_id = equipments.id)
        join equipment_inventories_users on (equipment_inventories_users.equipments_id = equipments.id)
        join users on (equipment_inventories_users.users_id = users.id)
        join profiles_users on (profiles_users.users_id = users.id)
        join profiles on (profiles.id = profiles_users.profiles_id)
        join processes on (processes.id = profiles.processes_id)
        where equipment_inventories.equipment_types_id = 3
        GROUP BY processes.acronyms ORDER BY dato DESC";

        $sql_licencias = "SELECT equipment_type_licenses.product,COUNT( * ) AS dato FROM `equipment_licensings` 
        join equipment_type_licenses on (equipment_type_licenses.id = equipment_licensings.equipment_type_licenses_id)
        GROUP BY equipment_type_licenses.product ORDER BY dato DESC";


        
        if (Input::get('fecha_inicio')) {
                $sql_tipos = "SELECT equipment_types.id, equipment_types.type , COUNT( * ) AS dato 
                FROM `equipment_inventories` join equipment_types on (equipment_types.id = equipment_inventories.equipment_types_id) 
                GROUP BY equipment_types.id ORDER BY id DESC";
        }

        $cons_usuarios = DB::select($sql_usuarios);
        $cons_estados = DB::select($sql_estados);
        $cons_periodicities = DB::select($sql_periodicities);
        $cons_procesos = DB::select($sql_procesos);
        $cons_impresoras = DB::select($sql_impresoras);
        $cons_licencias = DB::select($sql_licencias);

        if (Input::get('fecha_inicio')) {
            return View::make('dashboard.equipos.scrip_graficos')
            // ->with('container', 'dashboard.equipos.estadisticas')
            ->with('data_usuarios', $cons_usuarios)
            ->with('data_estados', $cons_estados)
            ->with('data_periodicities', $cons_periodicities)
            ->with('data_procesos', $cons_procesos)
            ->with('data_impresoras', $cons_impresoras)
            ->with('data_licencias', $cons_licencias);
            // ->with('submenu_activo', 'Estadísticas')
            // ->with('menu_activo', 'Equipos'); 
            exit();   
        }

        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.estadisticas')
        ->with('data_usuarios', $cons_usuarios)
        ->with('data_estados', $cons_estados)
        ->with('data_periodicities', $cons_periodicities)
        ->with('data_procesos', $cons_procesos)
        ->with('data_impresoras', $cons_impresoras)
        ->with('data_licencias', $cons_licencias)
        ->with('submenu_activo', 'Estadísticas')
        ->with('menu_activo', 'Actividades');      
    }
    public function getInformes(){

        return View::make('dashboard.index')
        ->with('container', 'dashboard.tareas.informes')
        
        ->with('submenu_activo', 'Informes')
        ->with('menu_activo', 'Actividades');   
    }
    public function getCalendar(){


            $query = DB::table('events')->get();

            // $query = $this->db->get('events');
            if(count($query) > 0)
            {
                echo json_encode(
                    array(
                        "success" => 1,
                        "result" => $query
                    )
                );
            }
    }
    public function getGenerarinformes(){
        include('assets/fpdf17/fpdf.php');

        switch (Input::get('id')) {
            case '1':

                $tareas = Tasks::get();
                $data = array();
                for ($i=0; $i < count($tareas); $i++) { 
                    $data[$i]['codigo']         = 'TR' . str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT);
                    $data[$i]['id']             = $tareas[$i]->id;
                    $data[$i]['usuario']        = $tareas[$i]->Users->name." ".$tareas[$i]->Users->last_name;
                    $data[$i]['img_usuario']    = $tareas[$i]->Users->img_min;
                    $data[$i]['tarea']          = utf8_decode($tareas[$i]->tasks);
                    $data[$i]['descripcion']    = $tareas[$i]->description;
                    $data[$i]['periodicidad']   = $tareas[$i]->TasksPeriodicity->periodicity;
                    $data[$i]['f_inicio']       = $tareas[$i]->start_date;
                    $data[$i]['progreso']       = $tareas[$i]->progress;
                }



                
                $pdf = new FPDF();
                $pdf->AliasNbPages();
                
                $pdf->AddPage();
                $contador=0;
                // La variable contador lleva el control de las iteraciones 

                $pdf->Image('assets/img/logo2(2).png',5,8,33);
                $pdf->SetFont('Arial','B',15);
                $pdf->SetY(15);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(21);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'LISTADO DE TAREAS',0,0,'C');

                // Se imprime el aula y sede
                $pdf->SetY(40);
                $pdf->SetX(8);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(44);
                // $pdf->Cell(100,5,'Aula: 1',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(48);
                $fecha=date('yy-mm-dd');
                $parte = explode("-",$fecha);
                $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
                // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

                $Y_Fields_Name_position = 55;
                $Y_Table_Position = 61;
                //////////////////////////////////////////////////////////////193
                //////////////////////////////////////////////////////////////190
                // Genero el encabezado de la tabla.
                $pdf->SetFillColor(232,232,232);
                $pdf->SetFont('Arial','B',8);
                $pdf->SetY($Y_Fields_Name_position);
                $pdf->SetX(10);
                $pdf->Cell(15,6,'ID.',1,0,'C',1);
                $pdf->SetX(25);
                $pdf->Cell(60,6,'Tarea',1,0,'C',1);
                $pdf->SetX(85);
                $pdf->Cell(30,6,' Usuario ',1,0,'C',1);
                $pdf->SetX(115);
                $pdf->Cell(30,6,' Periodicidad ',1,0,'C',1);
                $pdf->SetX(145);
                $pdf->Cell(30,6,'Progreso',1,0,'C',1);
                $pdf->SetX(175);
                $pdf->Cell(30,6,'Inicio',1,0,'C',1);

                $pdf->SetFont('Arial','',8);
                $pdf->SetY($Y_Table_Position);
                
                foreach ($data as $key) {
                    
                    $pdf->Cell(15,6,  $key['codigo'] ,1);
                    $pdf->Cell(60,6, $key['tarea'] ,1,'L');
                    $pdf->Cell(30,6, $key['usuario'] ,1,'R');
                    $pdf->Cell(30,6, $key['periodicidad'] ,1,'R');
                    $pdf->Cell(30,6, $key['progreso']."%" ,1,'R');
                    $pdf->Cell(30,6, $key['f_inicio'] ,1,'R');

                    $pdf->Ln();
                }
                $pdf->Output();







                break;
            case '2':
                $tareas = Tasks::get();

                $sql_usuarios = "SELECT users.name, users.last_name, COUNT( * ) AS dato FROM `tasks` 
                join users on (users.id = tasks.users_id)
                GROUP BY users.name ORDER BY dato DESC";
                $cons_usuarios = DB::select($sql_usuarios);


                $data = array();
                for ($i=0; $i < count($cons_usuarios); $i++) { 
                    $data[$i]['usuario']     = $cons_usuarios[$i]->name.' '.$cons_usuarios[$i]->last_name;
                    $data[$i]['cantidad']   = $cons_usuarios[$i]->dato;
                    
                }



                
                $pdf = new FPDF();
                $pdf->AliasNbPages();
                
                $pdf->AddPage();
                $contador=0;
                // La variable contador lleva el control de las iteraciones 

                $pdf->Image('assets/img/logo2(2).png',5,8,33);
                $pdf->SetFont('Arial','B',15);
                $pdf->SetY(15);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(21);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'LISTADO DE TAREAS POR USUARIOS',0,0,'C');

                // Se imprime el aula y sede
                $pdf->SetY(40);
                $pdf->SetX(8);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(44);
                // $pdf->Cell(100,5,'Aula: 1',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(48);
                $fecha=date('yy-mm-dd');
                $parte = explode("-",$fecha);
                $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
                // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

                $Y_Fields_Name_position = 55;
                $Y_Table_Position = 61;
                //////////////////////////////////////////////////////////////193
                //////////////////////////////////////////////////////////////190
                // Genero el encabezado de la tabla.
                $pdf->SetFillColor(232,232,232);
                $pdf->SetFont('Arial','B',8);
                $pdf->SetY($Y_Fields_Name_position);
                $pdf->SetX(10);
                $pdf->Cell(80,6,'Usuario.',1,0,'C',1);
                $pdf->SetX(90);
                $pdf->Cell(80,6,'Cantidad',1,0,'C',1);
                $pdf->SetX(85);
                

                $pdf->SetFont('Arial','',8);
                $pdf->SetY($Y_Table_Position);
                
                foreach ($data as $key) {
                    
                    $pdf->Cell(80,6, $key['usuario'] ,1,0,'C');
                    $pdf->Cell(80,6, $key['cantidad'] ,1,0,'C');
                    

                    $pdf->Ln();
                }
                $pdf->Output();
                break;
            case '3':
                $tareas = Tasks::get();

                $sql_estados = "SELECT tasks_statuses.status, COUNT( * ) AS dato FROM `tasks` 
                join tasks_statuses on (tasks_statuses.id = tasks.tasks_statuses_id)
                GROUP BY status ORDER BY dato DESC";
                $cons_estados = DB::select($sql_estados);


                $data = array();
                for ($i=0; $i < count($cons_estados); $i++) { 
                    $data[$i]['estado']     = $cons_estados[$i]->status;
                    $data[$i]['cantidad']   = $cons_estados[$i]->dato;
                    
                }



                
                $pdf = new FPDF();
                $pdf->AliasNbPages();
                
                $pdf->AddPage();
                $contador=0;
                // La variable contador lleva el control de las iteraciones 

                $pdf->Image('assets/img/logo2(2).png',5,8,33);
                $pdf->SetFont('Arial','B',15);
                $pdf->SetY(15);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(21);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'LISTADO DE TAREAS POR ESTADOS',0,0,'C');

                // Se imprime el aula y sede
                $pdf->SetY(40);
                $pdf->SetX(8);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(44);
                // $pdf->Cell(100,5,'Aula: 1',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(48);
                $fecha=date('yy-mm-dd');
                $parte = explode("-",$fecha);
                $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
                // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

                $Y_Fields_Name_position = 55;
                $Y_Table_Position = 61;
                //////////////////////////////////////////////////////////////193
                //////////////////////////////////////////////////////////////190
                // Genero el encabezado de la tabla.
                $pdf->SetFillColor(232,232,232);
                $pdf->SetFont('Arial','B',8);
                $pdf->SetY($Y_Fields_Name_position);
                $pdf->SetX(10);
                $pdf->Cell(80,6,'Estado.',1,0,'C',1);
                $pdf->SetX(90);
                $pdf->Cell(80,6,'Cantidad',1,0,'C',1);
                $pdf->SetX(85);
               

                $pdf->SetFont('Arial','',8);
                $pdf->SetY($Y_Table_Position);
                
                foreach ($data as $key) {
                    
                    $pdf->Cell(80,6, $key['estado'] ,1,0,'C');
                    $pdf->Cell(80,6, $key['cantidad'] ,1,0,'C');
                    

                    $pdf->Ln();
                }
                $pdf->Output();
                break;
            case '4':
                $tareas = Tasks::get();

                $sql_periodicities = "SELECT periodicity, COUNT( * ) AS dato FROM `tasks` 
                join tasks_periodicities on (tasks_periodicities.id = tasks.tasks_periodicity_id)
                GROUP BY periodicity ORDER BY dato DESC";
                $cons_periodicities = DB::select($sql_periodicities);


                $data = array();
                for ($i=0; $i < count($cons_periodicities); $i++) { 
                    $data[$i]['periodicity']     = $cons_periodicities[$i]->periodicity;
                    $data[$i]['cantidad']   = $cons_periodicities[$i]->dato;
                    
                }



                
                $pdf = new FPDF();
                $pdf->AliasNbPages();
                
                $pdf->AddPage();
                $contador=0;
                // La variable contador lleva el control de las iteraciones 

                $pdf->Image('assets/img/logo2(2).png',5,8,33);
                $pdf->SetFont('Arial','B',15);
                $pdf->SetY(15);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(21);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'LISTADO DE TAREAS POR ESTADOS',0,0,'C');

                // Se imprime el aula y sede
                $pdf->SetY(40);
                $pdf->SetX(8);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(44);
                // $pdf->Cell(100,5,'Aula: 1',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(48);
                $fecha=date('yy-mm-dd');
                $parte = explode("-",$fecha);
                $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
                // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

                $Y_Fields_Name_position = 55;
                $Y_Table_Position = 61;
                //////////////////////////////////////////////////////////////193
                //////////////////////////////////////////////////////////////190
                // Genero el encabezado de la tabla.
                $pdf->SetFillColor(232,232,232);
                $pdf->SetFont('Arial','B',8);
                $pdf->SetY($Y_Fields_Name_position);
                $pdf->SetX(10);
                $pdf->Cell(80,6,'Periodicidad.',1,0,'C',1);
                $pdf->SetX(90);
                $pdf->Cell(80,6,'Cantidad',1,0,'C',1);
                $pdf->SetX(85);
               

                $pdf->SetFont('Arial','',8);
                $pdf->SetY($Y_Table_Position);
                
                foreach ($data as $key) {
                    
                    $pdf->Cell(80,6, $key['periodicity'] ,1,0,'C');
                    $pdf->Cell(80,6, $key['cantidad'] ,1,0,'C');
                    

                    $pdf->Ln();
                }
                $pdf->Output();
                break;
            case '5':
                $tareas = Tasks::get();

                $sql_categories = "SELECT category, COUNT( * ) AS dato FROM `tasks` 
                join tasks_categories on (tasks_categories.id = tasks.tasks_categories_id)
                GROUP BY category ORDER BY dato DESC";
                $cons_categories = DB::select($sql_categories);


                $data = array();
                for ($i=0; $i < count($cons_categories); $i++) { 
                    $data[$i]['category']     = $cons_categories[$i]->category;
                    $data[$i]['cantidad']   = $cons_categories[$i]->dato;
                    
                }



                
                $pdf = new FPDF();
                $pdf->AliasNbPages();
                
                $pdf->AddPage();
                $contador=0;
                // La variable contador lleva el control de las iteraciones 

                $pdf->Image('assets/img/logo2(2).png',5,8,33);
                $pdf->SetFont('Arial','B',15);
                $pdf->SetY(15);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(21);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'LISTADO DE TAREAS POR CATEGORIAS',0,0,'C');

                // Se imprime el aula y sede
                $pdf->SetY(40);
                $pdf->SetX(8);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(44);
                // $pdf->Cell(100,5,'Aula: 1',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(48);
                $fecha=date('yy-mm-dd');
                $parte = explode("-",$fecha);
                $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
                // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

                $Y_Fields_Name_position = 55;
                $Y_Table_Position = 61;
                //////////////////////////////////////////////////////////////193
                //////////////////////////////////////////////////////////////190
                // Genero el encabezado de la tabla.
                $pdf->SetFillColor(232,232,232);
                $pdf->SetFont('Arial','B',8);
                $pdf->SetY($Y_Fields_Name_position);
                $pdf->SetX(10);
                $pdf->Cell(80,6,'Categoria.',1,0,'C',1);
                $pdf->SetX(90);
                $pdf->Cell(80,6,'Cantidad',1,0,'C',1);
                $pdf->SetX(85);
               

                $pdf->SetFont('Arial','',8);
                $pdf->SetY($Y_Table_Position);
                
                foreach ($data as $key) {
                    
                    $pdf->Cell(80,6, $key['category'] ,1,0,'C');
                    $pdf->Cell(80,6, $key['cantidad'] ,1,0,'C');
                    

                    $pdf->Ln();
                }
                $pdf->Output();
                break;
            case '6':
                $tareas = Tasks::get();

                $sql_tecnicos = "SELECT users.name, users.last_name, COUNT( * ) AS dato FROM `tasks` 
                join tasks_has_users on (tasks_has_users.tasks_id = tasks.id)
                join users on (users.id = tasks_has_users.users_id)
                GROUP BY users.name ORDER BY dato DESC";
                $cons_tecnicos = DB::select($sql_tecnicos);


                $data = array();
                for ($i=0; $i < count($cons_tecnicos); $i++) { 
                    $data[$i]['tecnico']     = $cons_tecnicos[$i]->name." ".$cons_tecnicos[$i]->last_name;
                    $data[$i]['cantidad']   = $cons_tecnicos[$i]->dato;
                    
                }



                
                $pdf = new FPDF();
                $pdf->AliasNbPages();
                
                $pdf->AddPage();
                $contador=0;
                // La variable contador lleva el control de las iteraciones 

                $pdf->Image('assets/img/logo2(2).png',5,8,33);
                $pdf->SetFont('Arial','B',15);
                $pdf->SetY(15);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'CORPORACION EDUCATIVA INDOAMERICANA LTDA',0,0,'C');
                $pdf->SetFont('Arial','B',14);
                $pdf->SetY(21);
                $pdf->SetX(50);
                $pdf->Cell(130,5,'LISTADO DE TAREAS POR TECNICOS',0,0,'C');

                // Se imprime el aula y sede
                $pdf->SetY(40);
                $pdf->SetX(8);
                $pdf->SetFont('Arial','',12);
                $pdf->Cell(100,5,'  Fecha: '.date('Y-m-d H:i:s').'',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(44);
                // $pdf->Cell(100,5,'Aula: 1',0,'L');
                $pdf->SetX(8);
                $pdf->SetY(48);
                $fecha=date('yy-mm-dd');
                $parte = explode("-",$fecha);
                $nueva_fecha=$parte[2]."/".$parte[1]."/".$parte[0];
                // $pdf->Cell(100,5,'Fecha Convocatoria: '.$nueva_fecha.'',0,'L');

                $Y_Fields_Name_position = 55;
                $Y_Table_Position = 61;
                //////////////////////////////////////////////////////////////193
                //////////////////////////////////////////////////////////////190
                // Genero el encabezado de la tabla.
                $pdf->SetFillColor(232,232,232);
                $pdf->SetFont('Arial','B',8);
                $pdf->SetY($Y_Fields_Name_position);
                $pdf->SetX(10);
                $pdf->Cell(80,6,'Tecnico.',1,0,'C',1);
                $pdf->SetX(90);
                $pdf->Cell(80,6,'Cantidad',1,0,'C',1);
                $pdf->SetX(85);
               

                $pdf->SetFont('Arial','',8);
                $pdf->SetY($Y_Table_Position);
                
                foreach ($data as $key) {
                    
                    $pdf->Cell(80,6, $key['tecnico'] ,1,0,'C');
                    $pdf->Cell(80,6, $key['cantidad'] ,1,0,'C');
                    

                    $pdf->Ln();
                }
                $pdf->Output();
                break;            
            default:
                # code...
                break;
        }
    }
    public function getExportarinforme(){
            include("assets/phpexcel/PHPExcel.php");

            $tareas = Tasks::get();
            
            $fecha_in = date('Y-m-d', strtotime(Input::get('inicio')));
            $fecha_fin = date('Y-m-d', strtotime(Input::get('fin')));
            $nuevafecha = strtotime('+1 day', strtotime($fecha_fin));
            $fecha_fin = date('Y-m-j', $nuevafecha);

            

            $objPHPExcel = new PHPExcel();

            $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Tarea')
                    ->setCellValue('C1', 'Usuario')
                    ->setCellValue('D1', 'Prioridad')
                    ->setCellValue('E1', 'Progreso')
                    ->setCellValue('F1', 'Observacion')
                    ->setCellValue('G1', 'Fecha Inicio');
                
                // $objPHPExcel->setActiveSheetIndex(0)
                //         ->setCellValue('A' . 2,'TR')
                //         ->setCellValue('B' . 2, 'as')
                //         ->setCellValue('C' . 2, 'asd')
                //         ->setCellValue('D' . 2, 'asd')
                //         ->setCellValue('E' . 2, 'qw')
                //         ->setCellValue('F' . 2, 'th')
                //         ->setCellValue('G' . 2, 'gbmn');

            $j = 2;
            $cal = array();
            for ($i = 0; $i < count($tareas); $i++) {
                
                $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue('A' . $j,'TR'.str_pad($tareas[$i]->id, 6, "0", STR_PAD_LEFT))
                        ->setCellValue('B' . $j, $tareas[$i]->tasks)
                        ->setCellValue('C' . $j, $tareas[$i]->Users->name." ".$tareas[$i]->Users->last_name)
                        ->setCellValue('D' . $j, $tareas[$i]->priority)
                        ->setCellValue('E' . $j, $tareas[$i]->progress)                    
                        ->setCellValue('F' . $j, $tareas[$i]->description)
                        ->setCellValue('G' . $j, $tareas[$i]->start_date);
                        
                        

                $j++;
            }
            
             $objPHPExcel->getActiveSheet()->setTitle('Numero de ingresos');

            $objPHPExcel->setActiveSheetIndex(0);

            $date = date("d-m-Y");

            // Redirect output to a client's web browser (Excel5)
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="Reporte_' . $fecha_in . '_' . $fecha_fin . '.xls"');
            header('Cache-Control: max-age=0');

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save('php://output');
    }
    public function postNotnuevaactividad() {

        $users = DB::table('tasks_has_users')->where('tasks_id', Input::get('id_actividad'))->get();
        
        $bandera = 0;
        foreach ($users as $user) {
            
                if ($user->users_id == Auth::user()->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            
        }
        echo $bandera;
    }
    public function postNotavanceactividad() {

        $users = DB::table('tasks')->where('tasks.id', Input::get('id_actividad'))->get();
        
        $bandera = 0;
        foreach ($users as $user) {
            
                if ($user->users_id == Auth::user()->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            
        }
        echo $bandera;
    }
    public function getCambiarestado(){
        $estado_tarea       = Input::get('estado_tarea');
        $id_tarea           = Input::get('id_tarea');
        $calificacion_tarea = Input::get('calificacion_tarea');

        $tasks                      = Tasks::find($id_tarea);
        $tasks->tasks_statuses_id   = $estado_tarea;
        $tasks->rating              = $calificacion_tarea;
        
        if ($tasks->save()) {
            echo "1";
        }
    }
    public function getReasignartarea(){
        $status         = 0;
        $id_tarea       = Input::get('id_tarea');
        $responsable    = Input::get('responsable');
        $fecha_actual   = date('Y-m-d H:i:s');

        $tasks = new TasksComents;
        $tasks->comment     = "Actividad Reasignada";
        $tasks->date        = $fecha_actual;
        $tasks->tasks_id    = $id_tarea;
        $tasks->users_id    = Auth::user()->id;
        $tasks->save();
        $status = 0;


        // DB::update('update tasks_has_users set status  = ? where id = ?', array($status ,$id_asignacion));
        
        DB::table('tasks_has_users')->insert(array(
            array('users_id' => $responsable, 'tasks_id' => $id_tarea, 'date_sign' => $fecha_actual),
        ));

        echo "1";
    }
    public function getAgregaritematarea(){
        $id_tarea           = Input::get('id_tarea');
        $otro_nuevo_item    = Input::get('otro_nuevo_item');

        
        DB::table('tasks_items')->insert(array(
            array('item' => $otro_nuevo_item, 'status' => 1, 'required' => 1),
        
        ));

        $cons_item = TasksItems::get();
        $last_item = $cons_item->last()->id;

        DB::table('tasks_items_has_tasks')->insert(array(
            array('tasks_items_id' => $last_item, 'tasks_id' => $id_tarea),
        
        ));
        echo "1";  


    }

    //funcion para crear tareas automaticamente
    public function getCronactividades(){
        $fecha_actual = date('Y-m-d');

        // cosultmaos las tareas que no se han vencido
        // $tareas = DB::table('tasks')
        // ->where('end_date', '>=', $fecha_actual)
        // ->groupBy('tasks')    
        // ->orderBy('id', 'asc')
        // ->get();



        $sql = 'select * from (select * from tasks order by id desc)sub where end_date >= '.$fecha_actual.' group by tasks order by id asc';
        $tareas  = DB::select($sql);




        // recorremos las tareas
        foreach ($tareas as $key) {
                // echo $key->id." ".$key->tasks." - ".$key->start_date."<br>";
            // validamos si son diarias
            if ($key->tasks_periodicity_id == 1) {


                $tarea = new Tasks;

                $tarea->tasks               = $key->tasks;
                $tarea->description         = $key->description;
                $tarea->tasks_periodicity_id= $key->tasks_periodicity_id;
                $tarea->start_date          = $fecha_actual;
                $tarea->end_date            = $key->end_date;
                $tarea->email               = $key->email;
                $tarea->users_id            = $key->users_id;
                $tarea->tasks_statuses_id   = 1;
                $tarea->priority            = $key->priority;
                $tarea->tasks_categories_id = $key->tasks_categories_id;


                // si se guarda la tarea 
                if($tarea->save()){
                    // consultamos el id de la tarea recien creada
                    $cons = Tasks::Where("users_id", $key->users_id)->get();
                    $bandera = $cons->last()->id;
                     
                    // cosultamos los usuarios que tienen la tarea asignada
                    $responsable = DB::table('tasks_has_users')
                    ->where('tasks_id', $key->id)
                    ->get();
                    

                    //recorremos los usuarios
                    foreach ($responsable as $key2) {
                        
                        // guardamos los usuarios para la nueva tarea
                        DB::table('tasks_has_users')->insert(array(
                            array('users_id' => $key2->users_id, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                        
                        ));


                        $notification = new Notifications;

                        $notification->description = 'Le Asigno Una Nueva Actividad';
                        $notification->estate = '1';
                        $notification->type_notifications_id = '5';
                        $notification->user_id = $key2->users_id;
                        $notification->user_id1 = $key->users_id;
                        $notification->link = 'tareas/asignadas';

                        $notification->save();
                    }

                    // cosultamos los items de la tarea
                    $items = DB::table('tasks_items_has_tasks')
                    ->where('tasks_id', $key->id)
                    ->get();

                    if (count($items) != 0) {
                        // recorremos los items
                        foreach ($items as $key3) {
                            
                            // guardamos los items de la nueva tarea
                            DB::table('tasks_items_has_tasks')->insert(array(
                                array('tasks_items_id' => $key3->tasks_items_id, 'tasks_id' => $bandera),
                            
                            ));             

                            
                        }
                    }
                }else{
                    echo "Error al duplicar tarea ".$key->id."<br>";
                }

            // validamos si la tarea es semanal
            }elseif($key->tasks_periodicity_id == 2){
                $fecha_inicio=$key->start_date;
                $nuevafecha = date('Y-m-d', strtotime("$fecha_inicio + 7 day"));



                // si la fecha de proxima creacion es igual a la fecha actual creamos la tarea
                if ($nuevafecha == $fecha_actual) {
                    // echo $nuevafecha;

                    $tarea = new Tasks;

                    $tarea->tasks               = $key->tasks;
                    $tarea->description         = $key->description;
                    $tarea->tasks_periodicity_id= $key->tasks_periodicity_id;
                    $tarea->start_date          = $fecha_actual;
                    $tarea->end_date            = $key->end_date;
                    $tarea->email               = $key->email;
                    $tarea->users_id            = $key->users_id;
                    $tarea->tasks_statuses_id   = 1;
                    $tarea->priority            = $key->priority;
                    $tarea->tasks_categories_id = $key->tasks_categories_id;


                    // si se guarda la tarea 
                    if($tarea->save()){
                        // consultamos el id de la tarea recien creada
                        $cons = Tasks::Where("users_id", $key->users_id)->get();
                        $bandera = $cons->last()->id;
                         
                        // cosultamos los usuarios que tienen la tarea asignada
                        $responsable = DB::table('tasks_has_users')
                        ->where('tasks_id', $key->id)
                        ->get();
                        

                        //recorremos los usuarios
                        foreach ($responsable as $key2) {
                            
                            // guardamos los usuarios para la nueva tarea
                            DB::table('tasks_has_users')->insert(array(
                                array('users_id' => $key2->users_id, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                            
                            ));


                            $notification = new Notifications;

                            $notification->description = 'Le Asigno Una Nueva Actividad';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '5';
                            $notification->user_id = $key2->users_id;
                            $notification->user_id1 = $key->users_id;
                            $notification->link = 'tareas/asignadas';

                            $notification->save();
                        }

                        // cosultamos los items de la tarea
                        $items = DB::table('tasks_items_has_tasks')
                        ->where('tasks_id', $key->id)
                        ->get();

                        if (count($items) != 0) {
                            // recorremos los items
                            foreach ($items as $key3) {
                                
                                // guardamos los items de la nueva tarea
                                DB::table('tasks_items_has_tasks')->insert(array(
                                    array('tasks_items_id' => $key3->tasks_items_id, 'tasks_id' => $bandera),
                                
                                ));             

                                
                            }
                        }
                    }else{
                        echo "Error al duplicar tarea ".$key->id."<br>";
                    }
                    
                }
            
            }elseif($key->tasks_periodicity_id == 3){
                $fecha_inicio=$key->start_date;
                $nuevafecha = date('Y-m-d', strtotime("$fecha_inicio + 14 day"));

                

                // si la fecha de proxima creacion es igual a la fecha actual creamos la tarea
                if ($nuevafecha == $fecha_actual) {

                    // echo $nuevafecha;
                    $tarea = new Tasks;

                    $tarea->tasks               = $key->tasks;
                    $tarea->description         = $key->description;
                    $tarea->tasks_periodicity_id= $key->tasks_periodicity_id;
                    $tarea->start_date          = $fecha_actual;
                    $tarea->end_date            = $key->end_date;
                    $tarea->email               = $key->email;
                    $tarea->users_id            = $key->users_id;
                    $tarea->tasks_statuses_id   = 1;
                    $tarea->priority            = $key->priority;
                    $tarea->tasks_categories_id = $key->tasks_categories_id;


                    // si se guarda la tarea 
                    if($tarea->save()){
                        // consultamos el id de la tarea recien creada
                        $cons = Tasks::Where("users_id", $key->users_id)->get();
                        $bandera = $cons->last()->id;
                         
                        // cosultamos los usuarios que tienen la tarea asignada
                        $responsable = DB::table('tasks_has_users')
                        ->where('tasks_id', $key->id)
                        ->get();
                        

                        //recorremos los usuarios
                        foreach ($responsable as $key2) {
                            
                            // guardamos los usuarios para la nueva tarea
                            DB::table('tasks_has_users')->insert(array(
                                array('users_id' => $key2->users_id, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                            
                            ));


                            $notification = new Notifications;

                            $notification->description = 'Le Asigno Una Nueva Actividad';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '5';
                            $notification->user_id = $key2->users_id;
                            $notification->user_id1 = $key->users_id;
                            $notification->link = 'tareas/asignadas';

                            $notification->save();
                        }

                        // cosultamos los items de la tarea
                        $items = DB::table('tasks_items_has_tasks')
                        ->where('tasks_id', $key->id)
                        ->get();

                        if (count($items) != 0) {
                            // recorremos los items
                            foreach ($items as $key3) {
                                
                                // guardamos los items de la nueva tarea
                                DB::table('tasks_items_has_tasks')->insert(array(
                                    array('tasks_items_id' => $key3->tasks_items_id, 'tasks_id' => $bandera),
                                
                                ));             

                                
                            }
                        }
                    }else{
                        echo "Error al duplicar tarea ".$key->id."<br>";
                    }
                    
                }

            }elseif($key->tasks_periodicity_id == 4){
                $fecha_inicio=$key->start_date;
                $nuevafecha = date('Y-m-d', strtotime("$fecha_inicio + 1 month"));

                

                // si la fecha de proxima creacion es igual a la fecha actual creamos la tarea
                if ($nuevafecha == $fecha_actual) {

                    // echo $nuevafecha;
                    $tarea = new Tasks;

                    $tarea->tasks               = $key->tasks;
                    $tarea->description         = $key->description;
                    $tarea->tasks_periodicity_id= $key->tasks_periodicity_id;
                    $tarea->start_date          = $fecha_actual;
                    $tarea->end_date            = $key->end_date;
                    $tarea->email               = $key->email;
                    $tarea->users_id            = $key->users_id;
                    $tarea->tasks_statuses_id   = 1;
                    $tarea->priority            = $key->priority;
                    $tarea->tasks_categories_id = $key->tasks_categories_id;


                    // si se guarda la tarea 
                    if($tarea->save()){
                        // consultamos el id de la tarea recien creada
                        $cons = Tasks::Where("users_id", $key->users_id)->get();
                        $bandera = $cons->last()->id;
                         
                        // cosultamos los usuarios que tienen la tarea asignada
                        $responsable = DB::table('tasks_has_users')
                        ->where('tasks_id', $key->id)
                        ->get();
                        

                        //recorremos los usuarios
                        foreach ($responsable as $key2) {
                            
                            // guardamos los usuarios para la nueva tarea
                            DB::table('tasks_has_users')->insert(array(
                                array('users_id' => $key2->users_id, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                            
                            ));


                            $notification = new Notifications;

                            $notification->description = 'Le Asigno Una Nueva Actividad';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '5';
                            $notification->user_id = $key2->users_id;
                            $notification->user_id1 = $key->users_id;
                            $notification->link = 'tareas/asignadas';

                            $notification->save();
                        }

                        // cosultamos los items de la tarea
                        $items = DB::table('tasks_items_has_tasks')
                        ->where('tasks_id', $key->id)
                        ->get();

                        if (count($items) != 0) {
                            // recorremos los items
                            foreach ($items as $key3) {
                                
                                // guardamos los items de la nueva tarea
                                DB::table('tasks_items_has_tasks')->insert(array(
                                    array('tasks_items_id' => $key3->tasks_items_id, 'tasks_id' => $bandera),
                                
                                ));             

                                
                            }
                        }
                    }else{
                        echo "Error al duplicar tarea ".$key->id."<br>";
                    }
                    
                }

            }elseif($key->tasks_periodicity_id == 5){
                $fecha_inicio=$key->start_date;
                $nuevafecha = date('Y-m-d', strtotime("$fecha_inicio + 6 month"));

                

                    // echo $nuevafecha;
                // si la fecha de proxima creacion es igual a la fecha actual creamos la tarea
                if ($nuevafecha == $fecha_actual) {

                    $tarea = new Tasks;

                    $tarea->tasks               = $key->tasks;
                    $tarea->description         = $key->description;
                    $tarea->tasks_periodicity_id= $key->tasks_periodicity_id;
                    $tarea->start_date          = $fecha_actual;
                    $tarea->end_date            = $key->end_date;
                    $tarea->email               = $key->email;
                    $tarea->users_id            = $key->users_id;
                    $tarea->tasks_statuses_id   = 1;
                    $tarea->priority            = $key->priority;
                    $tarea->tasks_categories_id = $key->tasks_categories_id;


                    // si se guarda la tarea 
                    if($tarea->save()){
                        // consultamos el id de la tarea recien creada
                        $cons = Tasks::Where("users_id", $key->users_id)->get();
                        $bandera = $cons->last()->id;
                         
                        // cosultamos los usuarios que tienen la tarea asignada
                        $responsable = DB::table('tasks_has_users')
                        ->where('tasks_id', $key->id)
                        ->get();
                        

                        //recorremos los usuarios
                        foreach ($responsable as $key2) {
                            
                            // guardamos los usuarios para la nueva tarea
                            DB::table('tasks_has_users')->insert(array(
                                array('users_id' => $key2->users_id, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                            
                            ));


                            $notification = new Notifications;

                            $notification->description = 'Le Asigno Una Nueva Actividad';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '5';
                            $notification->user_id = $key2->users_id;
                            $notification->user_id1 = $key->users_id;
                            $notification->link = 'tareas/asignadas';

                            $notification->save();
                        }

                        // cosultamos los items de la tarea
                        $items = DB::table('tasks_items_has_tasks')
                        ->where('tasks_id', $key->id)
                        ->get();

                        if (count($items) != 0) {
                            // recorremos los items
                            foreach ($items as $key3) {
                                
                                // guardamos los items de la nueva tarea
                                DB::table('tasks_items_has_tasks')->insert(array(
                                    array('tasks_items_id' => $key3->tasks_items_id, 'tasks_id' => $bandera),
                                
                                ));             

                                
                            }
                        }
                    }else{
                        echo "Error al duplicar tarea ".$key->id."<br>";
                    }
                    
                }

            }elseif($key->tasks_periodicity_id == 6){
                $fecha_inicio=$key->start_date;
                $nuevafecha = date('Y-m-d', strtotime("$fecha_inicio + 12 month"));

                

                    // echo $nuevafecha;
                // si la fecha de proxima creacion es igual a la fecha actual creamos la tarea
                if ($nuevafecha == $fecha_actual) {

                    $tarea = new Tasks;

                    $tarea->tasks               = $key->tasks;
                    $tarea->description         = $key->description;
                    $tarea->tasks_periodicity_id= $key->tasks_periodicity_id;
                    $tarea->start_date          = $fecha_actual;
                    $tarea->end_date            = $key->end_date;
                    $tarea->email               = $key->email;
                    $tarea->users_id            = $key->users_id;
                    $tarea->tasks_statuses_id   = 1;
                    $tarea->priority            = $key->priority;
                    $tarea->tasks_categories_id = $key->tasks_categories_id;


                    // si se guarda la tarea 
                    if($tarea->save()){
                        // consultamos el id de la tarea recien creada
                        $cons = Tasks::Where("users_id", $key->users_id)->get();
                        $bandera = $cons->last()->id;
                         
                        // cosultamos los usuarios que tienen la tarea asignada
                        $responsable = DB::table('tasks_has_users')
                        ->where('tasks_id', $key->id)
                        ->get();
                        

                        //recorremos los usuarios
                        foreach ($responsable as $key2) {
                            
                            // guardamos los usuarios para la nueva tarea
                            DB::table('tasks_has_users')->insert(array(
                                array('users_id' => $key2->users_id, 'tasks_id' => $bandera, 'date_sign' => $fecha_actual),
                            
                            ));


                            $notification = new Notifications;

                            $notification->description = 'Le Asigno Una Nueva Actividad';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '5';
                            $notification->user_id = $key2->users_id;
                            $notification->user_id1 = $key->users_id;
                            $notification->link = 'tareas/asignadas';

                            $notification->save();
                        }

                        // cosultamos los items de la tarea
                        $items = DB::table('tasks_items_has_tasks')
                        ->where('tasks_id', $key->id)
                        ->get();

                        if (count($items) != 0) {
                            // recorremos los items
                            foreach ($items as $key3) {
                                
                                // guardamos los items de la nueva tarea
                                DB::table('tasks_items_has_tasks')->insert(array(
                                    array('tasks_items_id' => $key3->tasks_items_id, 'tasks_id' => $bandera),
                                
                                ));             

                                
                            }
                        }
                    }else{
                        echo "Error al duplicar tarea ".$key->id."<br>";
                    }
                    
                }

            }


        }



    }

    

}
?>