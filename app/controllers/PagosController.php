<?php
/**
 * 
 */
class PagosController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }

    

    //funcion para controlar los permisos de los usuarios
    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }

    public function getAprobar(){
        $user = Users::find(Auth::user()->id);

        $payments = Payments::where('payments_statuses_id', 1)
        ->get();


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']     = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
                 $data[$i]['shopping_quotation_id'] =  $payments[$i]->shopping_quotation_id;
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }


            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_aprobar_pagos')
                                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.aprobar')
                ->with('user', $user)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Aprobar Pagos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    
    //funcion para notificacion de causar pago
    public function getNotcausarpayment() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 37;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
    
     //funcion para notificacion de causar pago
        public function getNotverificarpayment() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 36;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
    
    //funcion para notificacion de verificar pago
        public function getNottesoreriapayment() {

        $user = Users::find(Auth::user()->id);
        $submenu_required = 38;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                    $bandera = $notificaciones->last()->id;
                }
            }
        }
        echo $bandera;
    }
    
    //funcion para notificacion de finalizar pago
        public function getNotfinalizarpayment() {
        
        $tipo = Input::get('tipo');
        $id= Input::get('id');        
        
        if(intval($tipo) !== 0){
                
                        $user = Users::find(Auth::user()->id);
                        $submenu_required = 26;
                        $bandera = 0;
                        foreach ($user->Profiles as $perfil) {
                            foreach ($perfil->submenus as $submenu) {
                                if ($submenu_required == $submenu->id) {
                                    $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                                    $bandera = $notificaciones->last()->id;
                                }
                            }
                        }
                        echo $bandera;                        
        
            }else{
                
                $bandera = 0;
                
                            $sql = "SELECT*FROM users JOIN payments ON (users.id = payments.users_id) WHERE payments.id = '".$id."' ";
                            $user_not = DB::select($sql);
                            
                            if(Auth::user()->id == $user_not[0]->users_id){
                            $notificaciones = Notifications::Where("user_id", Auth::user()->id)->get();
                            $bandera = $notificaciones->last()->id;
                            }                            
                            echo $bandera;                            
                            
            }

    }
    
    public function getAprobarhistorico(){
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();

        $codigo = Input::get('code');
        $status = Input::get('status');

        $payments = Payments::where('payments_statuses_id','<>', 1)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('payments_statuses_id', 'LIKE', '%' . $status . '%')
        ->orderBy('payments_statuses_id', 'desc')
        ->orderBy('id', 'desc')
        ->paginate(15);


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['status']     = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['comment']    = $payments[$i]->comment;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status_id']  = $payments[$i]->payments_statuses_id;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_aprobar_pagos_hist')
                                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.aprobarHist')
                ->with('user', $user)
                ->with('applicants', $applicants)
                ->with('pag', $payments)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Aprobar Pagos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    public function getIngresar(){
        $user = Users::find(Auth::user()->id);

        $concepts = PaymentsConcepts::get();
        $providers = Providers::get();
        $payments_methods = PaymentsMethods::get();

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {                        
            
            return View::make('dashboard.index')
            ->with('container', 'dashboard.pagos.ingresar_pagos')
            ->with('user', $user)
            ->with('concepts', $concepts)
            ->with('providers', $providers)
            ->with('payments_methods', $payments_methods)

            ->with('submenu_activo', 'Ingresar Pago')
            ->with('menu_activo', 'Pagos');
        }
    }
    public function getMispagos(){
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();

        $codigo = Input::get('code');
        $status = Input::get('status');

        $payments = Payments::where('payments_statuses_id','<>', 20)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('payments_statuses_id', 'LIKE', '%' . $status . '%')
        ->where('users_id', Auth::user()->id)
        ->orderBy('payments_statuses_id', 'desc')
        ->orderBy('id', 'desc')
        ->paginate(15);


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['status']     = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['comment']    = $payments[$i]->comment;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status_id']  = $payments[$i]->payments_statuses_id;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_aprobar_pagos_hist')
                                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.mis_pagos')
                ->with('user', $user)
                ->with('applicants', $applicants)
                ->with('pag', $payments)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Ingresar Pago')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    public function postCreatepayment() {

        if (Input::file('archivo')) {

            $my_id = Auth::user()->id;

            $filename = Input::file('archivo')->getClientOriginalName();
            
            $destinationPath = 'assets/files/facturas/';

            $concept    = Input::get('concept');
            $description= Input::get('description');
            $value      = Input::get('value');
            $n_factura      = Input::get('factura');
            $fecha_pago = Input::get('fecha_pago');
            $forma_pago = Input::get('forma_pago');
            $providers_id = Input::get('nit');

            if ($my_id == 1) {
                $status = "2";
            }else{
                $status = "1";
            }

            $uploadSuccess = Input::file('archivo')->move($destinationPath, $filename);

            if ($uploadSuccess) {

                 $new_payment = new Payments;

                 $new_payment->description = $description;
                 $new_payment->payments_statuses_id = $status;
                 $new_payment->n_factura = $n_factura;
                 $new_payment->payment_date = $fecha_pago;
                 $new_payment->payments_methods_id = $forma_pago;
                 
                 $new_payment->payments_concepts_id = $concept;
                 $new_payment->value = $value;
                 $new_payment->file = $destinationPath.$filename;
                 if ($providers_id != "") {
                    $new_payment->providers_id = $providers_id;
                 }
                 $new_payment->users_id = $my_id;

                

                $new_payment->save();

                $url = 'pagos/ingresar';
                echo "1";
            }
        } else {
            echo "2";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');            
        }
    }
    public function getAprobacion(){
        $my_id = Auth::user()->id;
        $id = e(Input::get('valorCaja1'));
        $status = e(Input::get('estado'));
        $comment = e(Input::get('observation'));
        $neto_paga = e(Input::get('neto_pagar'));
        $neto_pagar = str_replace(",", "", $neto_paga);
        $tipo_compra = e(Input::get('tipo_compra'));
        $documento = e(Input::get('documento'));
        $comment_user = e(Input::get('observation'));
        $devuelto = e(Input::get('devuelto'));
        $status_dev = e(Input::get('estado_debuelto'));
        $tipo = Input::get('tipo');
        $fuente = Input::get('fuente');
        

        if ($documento != "" ) {
            DB::update('update payments set accounting_document = ?  where id = ?', array($documento, $id));
            echo 1;
            exit();
        }


        
                if ($status == 3) {
                    $description_trace = "Rechazó un nuevo pago";
                    
                    if(intval($fuente)==1){
                            $user = Users::all();
                            $submenu_required = 36;
                            $bandera = 0;
                    
                            foreach ($user as $key) {
                                foreach ($key->Profiles as $perfil) {
                                    foreach ($perfil->submenus as $submenu) {
                                        if ($submenu_required == $submenu->id) {
                                            //echo $key->name." ".$key->last_name."<br>";
                                            $notification = new Notifications;

                                            $notification->description = 'Rechazo una solicitud de pago';
                                            $notification->estate = '1';
                                            $notification->type_notifications_id = '3';
                                            $notification->user_id = $key->id;
                                            $notification->user_id1 = Auth::user()->id;
                                            $notification->link = 'pagos/verificar';

                                            $notification->save();
                                        }
                                    }
                                }
                            }
                            
                    }elseif(intval($fuente)==2){
                        
                        $user = Users::all();
                            $submenu_required = 37;
                            $bandera = 0;
                    
                            foreach ($user as $key) {
                                foreach ($key->Profiles as $perfil) {
                                    foreach ($perfil->submenus as $submenu) {
                                        if ($submenu_required == $submenu->id) {
                                            //echo $key->name." ".$key->last_name."<br>";
                                            $notification = new Notifications;

                                            $notification->description = 'Rechazo una solicitud de pago';
                                            $notification->estate = '1';
                                            $notification->type_notifications_id = '3';
                                            $notification->user_id = $key->id;
                                            $notification->user_id1 = Auth::user()->id;
                                            $notification->link = 'pagos/verificar';

                                            $notification->save();
                                        }
                                    }
                                }
                            }
                        
                        
                        
                    }
                            
                }elseif ($status == 2) {
                    if ($devuelto == 1) {
                        $description_trace = "Devolvio un pago";
                        DB::update('update payments set payments_statuses_id = ?  where id = ?', array($status, $id));
                        $payment_traces  = new PaymentsTraces;

                        $payment_traces->description    = $description_trace;
                        $payment_traces->comment        = $comment_user;
                        $payment_traces->payments_id    = $id;
                        $payment_traces->users_id       = $my_id;

                        $payment_traces->save();
                        echo "3";
                        exit(); 
                    }else{
                        $description_trace = "Aprobó un nuevo pago";
                        
                        $user = Users::all();
            $submenu_required = 37;
            $bandera = 0;
            foreach ($user as $key) {
                foreach ($key->Profiles as $perfil) {
                    foreach ($perfil->submenus as $submenu) {
                        if ($submenu_required == $submenu->id) {
                            //echo $key->name." ".$key->last_name."<br>";
                            $notification = new Notifications;

                            $notification->description = 'Aprobo una solicitud de pago';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '3';
                            $notification->user_id = $key->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'pagos/gestionar';

                            $notification->save();
                        }
                    }
                }
            }
                    }

                }
                elseif ($status == 5) {
                    $description_trace = "Verifico un nuevo pago";
                    DB::update('update payments set payments_statuses_id = ?  where id = ?', array($status, $id));
                    $payment_traces  = new PaymentsTraces;

                    $payment_traces->description = $description_trace;
                    $payment_traces->payments_id = $id;
                    $payment_traces->users_id    = $my_id;
                    $payment_traces->comment     = $comment_user;
                    $payment_traces->save();
                    
                    
                    $user = Users::all();
            $submenu_required = 38;
            $bandera = 0;
            foreach ($user as $key) {
                foreach ($key->Profiles as $perfil) {
                    foreach ($perfil->submenus as $submenu) {
                        if ($submenu_required == $submenu->id) {
                            //echo $key->name." ".$key->last_name."<br>";
                            $notification = new Notifications;

                            $notification->description = 'Verifico una solicitud de pago';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '3';
                            $notification->user_id = $key->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'pagos/finalizar';

                            $notification->save();
                        }
                    }
                }
            }
            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
            return $user_not;

                    //echo "3";
                    exit();
                }
                elseif ($status == 4) {
                    $description_trace = "Causo un nuevo pago";
                    
                    $user = Users::all();
            $submenu_required = 36;
            $bandera = 0;
            foreach ($user as $key) {
                foreach ($key->Profiles as $perfil) {
                    foreach ($perfil->submenus as $submenu) {
                        if ($submenu_required == $submenu->id) {
                            //echo $key->name." ".$key->last_name."<br>";
                            $notification = new Notifications;

                            $notification->description = 'Causo una solicitud de Pago';
                            $notification->estate = '1';
                            $notification->type_notifications_id = '3';
                            $notification->user_id = $key->id;
                            $notification->user_id1 = Auth::user()->id;
                            $notification->link = 'pagos/verificar';

                            $notification->save();
                        }
                    }
                }
            }
                    
                }
                elseif ($status == 6) {
                    $description_trace = "Finalizo un nuevo pago";
                    
                    DB::update('update payments set payments_statuses_id = ?  where id = ?', array($status, $id));
                    $payment_traces  = new PaymentsTraces;

                    $payment_traces->description = $description_trace;
                    $payment_traces->payments_id = $id;
                    $payment_traces->users_id = $my_id;

                    $payment_traces->save();
                    
                     $user = Users::all();
                     $bandera = 0;
            if(intval($tipo) !== 0){
                
                            $submenu_required = 26;
                            foreach ($user as $key) {
                                foreach ($key->Profiles as $perfil) {
                                    foreach ($perfil->submenus as $submenu) {
                                        if ($submenu_required == $submenu->id) {
                                            //echo $key->name." ".$key->last_name."<br>";
                                            $notification = new Notifications;

                                            $notification->description = 'Finalizo una solicitud de pago';
                                            $notification->estate = '1';
                                            $notification->type_notifications_id = '3';
                                            $notification->user_id = $key->id;
                                            $notification->user_id1 = Auth::user()->id;
                                            $notification->link = 'pagos/ingresar';

                                            $notification->save();
                                        }
                                    }
                                }
                            }
                            
            }else{
                
                            $sql = "SELECT*FROM users JOIN payments ON (users.id = payments.users_id) WHERE payments.id = '".$id."'";
                            $user_not = DB::select($sql);
                            
                            $notification = new Notifications;

                                            $notification->description = 'Finalizo una solicitud de pago';
                                            $notification->estate = '1';
                                            $notification->type_notifications_id = '3';
                                            $notification->user_id = $user_not[0]->users_id;
                                            $notification->user_id1 = Auth::user()->id;
                                            $notification->link = 'pagos/mispagos';

                                            $notification->save();
            }
            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
            return $user_not;

                    //echo "3";
                    exit();

                }elseif ($status == 7) {

                    $description_trace = "Devolvio un nuevo pago";
                    $pago = DB::table('payments')->select('shopping_quotation_id')->where('id', $id)->get();

                    if ($pago[0]->shopping_quotation_id == "") {
                        $status = 3;
                        $description_trace = "Rechazó un nuevo pago";
                        
                    }

                    DB::update('update payments set payments_statuses_id = ?, status_dev = ?  where id = ?', array($status, $status_dev, $id));
                    $payment_traces  = new PaymentsTraces;

                    $payment_traces->description    = $description_trace;
                    $payment_traces->comment        = $comment_user;
                    $payment_traces->payments_id    = $id;
                    $payment_traces->users_id       = $my_id;

                    $payment_traces->save();
                    echo "3";
                    exit();

                }
                $payment_traces  = new PaymentsTraces;

                $payment_traces->description    = $description_trace;
                $payment_traces->comment        = $comment_user;
                $payment_traces->payments_id    = $id;
                $payment_traces->users_id       = $my_id;

                $payment_traces->save();
            
        
            DB::update('update payments set payments_statuses_id = ?, comment = ?, net_pay = ?, type_purchase = ?  where id = ?', array($status, $comment, $neto_pagar, $tipo_compra, $id));                                
            
            $sql = "SELECT users.id, users.name, users.last_name, users.img_min FROM users WHERE id = '".Auth::user()->id."'";
            $user_not = DB::select($sql);
            
            return $user_not;

    }
    public function getGestionar(){
        $user = Users::find(Auth::user()->id);

        $payments = Payments::where('payments_statuses_id', 2)
        ->get();


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']      = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;
            $data[$i]['provider']    = $payments[$i]->Providers->provider;
            $data[$i]['identificacion']    = $payments[$i]->Providers->nit;
            $data[$i]['metodo']    = $payments[$i]->PaymentsMethods->name;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_gestionar_pagos')
                ->with('user', $user)
                ->with('payments', $payments)
                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.gestionar')
                ->with('user', $user)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Causar Pagos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    public function getGestionarhist(){
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();

        $codigo = Input::get('code');
        $status = Input::get('status');

        $payments = Payments::where('payments_statuses_id', '<>', 2)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('payments_statuses_id', 'LIKE', '%' . $status . '%')
        ->paginate(15);


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['comment']    = $payments[$i]->comment;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']      = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_gestionar_pagos_hist')
                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.gestionarHist')
                ->with('user', $user)
                ->with('applicants', $applicants)
                ->with('pag', $payments)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Causar Pagos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    public function getVerificar(){
        $user = Users::find(Auth::user()->id);

        $payments = Payments::where('payments_statuses_id', 4)
        ->get();


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']      = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;
            $data[$i]['provider']    = $payments[$i]->Providers->provider;
            $data[$i]['identificacion']    = $payments[$i]->Providers->nit;
            $data[$i]['metodo']    = $payments[$i]->PaymentsMethods->name;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_verificar_pagos')
                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.verificar')
                ->with('user', $user)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Verificar Pagos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    public function getVerificarhist(){
        $user = Users::find(Auth::user()->id);

        $applicants = Users::all();

        $codigo = Input::get('code');
        $status = Input::get('status');

        $payments = Payments::where('payments_statuses_id','<>', 4)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('payments_statuses_id', 'LIKE', '%' . $status . '%')
        ->paginate(15);


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['comment']    = $payments[$i]->comment;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']      = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }
            
            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_verificar_pagos_hist')
                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.verificarHist')
                ->with('user', $user)
                ->with('applicants', $applicants)
                ->with('pag', $payments)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Verificar Pagos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }

    public function getFinalizar(){
        $user = Users::find(Auth::user()->id);

        $payments = Payments::where('payments_statuses_id', 5)
        ->orderBy('payment_date', 'ASC')
        ->get();

        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) {
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['metodo']    = $payments[$i]->PaymentsMethods->name;
            $data[$i]['provider']    = $payments[$i]->Providers->provider;
            $data[$i]['identificacion']    = $payments[$i]->Providers->nit;
            $data[$i]['value']      = $payments[$i]->net_pay;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']      = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{
                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_finalizar_pagos')
                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.finalizar')
                ->with('user', $user)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Tesoreria')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    
    public function getFinalizarhist(){
        $user = Users::find(Auth::user()->id);

        $applicants = Users::all();

        $codigo = Input::get('code');
        $status = Input::get('status');

        $payments = Payments::where('payments_statuses_id','<>', 5)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('payments_statuses_id', 'LIKE', '%' . $status . '%')
        ->paginate(15);


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['comment']    = $payments[$i]->comment;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status']      = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }
            $data[$i]['tipo_boton'] = 'btn-default';
            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6){
                $data[$i]['label'] = 'label-warning';
                if ($payments[$i]->accounting_document == ""){
                    $data[$i]['tipo_boton'] = 'btn-info';
                }
            }
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_finalizar_pagos_hist')
                ->with('pagos', $data)
                ->with('applicants', $applicants)
                ->with('pag', $payments);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.finalizarHist')
                ->with('user', $user)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('applicants', $applicants)
                ->with('pag', $payments)
                ->with('submenu_activo', 'Tesoreria')
                ->with('menu_activo', 'Pagos');
            }
        }

    }
    public function interval_date($init, $finish) {
        //formateamos las fechas a segundos tipo 1374998435
        $diferencia = strtotime($finish) - strtotime($init);

        //comprobamos el tiempo que ha pasado en segundos entre las dos fechas
        //floor devuelve el número entero anterior, si es 5.7 devuelve 5
        if ($diferencia < 60) {
            $tiempo = floor($diferencia) . " Seg";
        } else if ($diferencia > 60 && $diferencia < 3600) {
            $tiempo = floor($diferencia / 60) . ' Min';
        } else if ($diferencia > 3600 && $diferencia < 86400) {
            $tiempo = floor($diferencia / 3600) . " Horas";
        } else if ($diferencia > 86400 && $diferencia < 2592000) {
            $tiempo = floor($diferencia / 86400) . " Días";
        } else if ($diferencia > 2592000 && $diferencia < 31104000) {
            $tiempo = floor($diferencia / 2592000) . " Meses";
        } else if ($diferencia > 31104000) {
            $tiempo = floor($diferencia / 31104000) . " Años";
        } else {
            $tiempo = "Error";
        }
        return $tiempo;
    }
    public function getConsultarprov()
    {
        //get POST data
                $texto = Input::get('palabra');
                
                $sql = "SELECT * FROM providers WHERE provider LIKE '%".$texto."%' ORDER BY provider DESC";

                $datos = DB::select($sql);
                
                if($datos){
                    for($i=0;count($datos)>$i;$i++)
                        {
                        ?>
                        <a onclick="agregarNit('<?php echo $datos[$i]->nit; ?>', '<?php echo $datos[$i]->provider; ?>', '<?php echo $datos[$i]->id; ?>')" style="text-decoration:none;" >
                        <div class="display_box" align="left">
                        <div style="margin-right:6px;"><b><?php echo $datos[$i]->provider; ?></b></div></div>
                        </a>
                        <?php
                        }
                }else{
                    echo 0;
                }
                    
    }

    public function getConsultafactura()
    {
        
                $concept = Input::get('concept');
                
                $payments = Payments::where('payments_concepts_id', $concept)
                            ->get();

                

                if (count($payments) == "") {
                    if ($concept == 2) {
                        $numero = rand(10016,20000);
                    }elseif ($concept == 8) {
                        $numero = rand(30016,40000);
                    }elseif ($concept == 10) {
                        $numero = rand(60016,50000);
                    }elseif ($concept == 15) {
                        $numero = rand(90016,60000);
                    }
                }else{
                    $numero = $payments->last()->n_factura;
                }

                $numero = $numero+1;
                echo $numero;
                    
    }
    
    
    public function getExportarexcelprov() {
        
        $pagos = explode(",",$_GET["proveedores_excel"]);
        
        include("assets/phpexcel/PHPExcel.php");

        $objPHPExcel = new PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Indoamericana")
                ->setLastModifiedBy("Indoamericana")
                ->setTitle("Ingresos por usuario")
                ->setSubject("Indoamericana")
                ->setDescription("Indoamericana")
                ->setKeywords("Indoamericana")
                ->setCategory("Indoamericana");

        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'Identificacion')
                ->setCellValue('B1', 'Valor')
                ->setCellValue('C1', 'Nombre')
                ->setCellValue('D1', 'Banco')
                ->setCellValue('E1', 'Tipo de cuenta')
                ->setCellValue('F1', 'Cuenta') 
                ->setCellValue('G1', 'Inf. Adicional');

        $j = 2;
        for ($i = 0; $i < count($pagos); $i++) {
            
            $payments = Payments::where('id','=', $pagos[$i])
            ->get();
            


            $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $j, $payments[0]->Providers->nit)
                    ->setCellValue('B' . $j, $payments[0]->net_pay)
                    ->setCellValue('C' . $j, $payments[0]->Providers->provider)
                    ->setCellValue('D' . $j, $payments[0]->Providers->bank)
                    ->setCellValue('E' . $j, $payments[0]->Providers->account_type)
                    ->setCellValue('F' . $j, $payments[0]->Providers->account_number)
                    ->setCellValue('G' . $j, $payments[0]->description);

            $j++;
        }

        $objPHPExcel->getActiveSheet()->setTitle('Informacion Pagos');

        //$objPHPExcel->setActiveSheetIndex(0);

        $date = date("d-m-Y");

        // Redirect output to a client's web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Ingresos_' . $date . '.xls"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
    }

    public function postGestionpago() {
        $id = $_POST['valorCaja1'];


        
        $payments = Payments::where('id', $id)->get();
        
        $status = SupportStatus::all();
        $traces = PaymentsTraces::where('payments_id', $id)->where('comment', '<>', '')->get();
        $comprobante = PaymentsVouchers::get();

        


        if (Input::get('valorCaja2')) {
            return View::make('dashboard.pagos.detalle_de_pagos_validar')
            ->with('statuses', $status)
            ->with('comprobantes', $comprobante)
            ->with('traces', $traces)
            ->with('payments', $payments);
            
        }else{
            return View::make('dashboard.pagos.detalle_de_pagos')
            ->with('statuses', $status)
            ->with('comprobantes', $comprobante)
            ->with('traces', $traces)
            ->with('payments', $payments);
        }
    }
    public function postModalhistorico() {
        $id = $_POST['valorCaja1'];

        
        
        $payments = Payments::where('id', $id)->get();
        $traces = PaymentsTraces::where('payments_id', $id)->where('comment', '<>', '')->get();
        $status = SupportStatus::all();
        $comprobante = PaymentsVouchers::get();
        $payments_methods = PaymentsMethods::get();
        


        if (Input::get('valorCaja2')) {
            return View::make('dashboard.pagos.detalle_de_pagos_historico')
            ->with('statuses', $status)
            ->with('comprobantes', $comprobante)
            ->with('traces', $traces)
            ->with('payments_methods', $payments_methods)
            ->with('payments', $payments);
            
        
        }elseif (Input::get('valorCaja3')) {
            return View::make('dashboard.pagos.detalle_de_pagos_historico_finalizar')
            ->with('statuses', $status)
            ->with('comprobantes', $comprobante)
            ->with('traces', $traces)
            ->with('payments_methods', $payments_methods)
            ->with('payments', $payments)
            ->with('submenu_activo', 'Tesoreria');
        }else{
            return View::make('dashboard.pagos.detalle_de_pagos')
            ->with('statuses', $status)
            ->with('comprobantes', $comprobante)
            ->with('traces', $traces)
            ->with('payments_methods', $payments_methods)
            ->with('payments', $payments);
        }
    }
    public function getDevueltos(){
        $user = Users::find(Auth::user()->id);
        $applicants = Users::all();

        $codigo = Input::get('code');
        $status = Input::get('status');

        $payments = Payments::where('payments_statuses_id','=', 7)
        ->where('id', 'LIKE', '%' . $codigo . '%')
        ->where('payments_statuses_id', 'LIKE', '%' . $status . '%')
        
        ->orderBy('payments_statuses_id', 'desc')
        ->orderBy('id', 'desc')
        ->paginate(15);


        $data = array();
        $fecha_actual = date('Y-m-d H:i:s');
        for ($i=0; $i < count($payments); $i++) { 
            
            $fecha = date($payments[$i]->created_at);
            $fecha_close = date($payments[$i]->updated_at);

            $data[$i]['id']         = 'SP' . str_pad($payments[$i]->id, 6,'0', STR_PAD_LEFT);
            $data[$i]['hid']        = $payments[$i]->id;
            $data[$i]['description']= $payments[$i]->description;
            $data[$i]['status']     = $payments[$i]->PaymentsStatuses->name;
            $data[$i]['concept']    = $payments[$i]->PaymentsConcepts->acronym;
            $data[$i]['value']      = $payments[$i]->value;
            $data[$i]['comment']    = $payments[$i]->comment;
            $data[$i]['file']       = $payments[$i]->file;
            $data[$i]['time']       = $this->interval_date($fecha, $fecha_actual);
            $data[$i]['status_id']  = $payments[$i]->payments_statuses_id;
            $data[$i]['user']       = $payments[$i]->Users->name." ".$payments[$i]->Users->last_name;
            $data[$i]['f_pago']     = $payments[$i]->payment_date;

            if($payments[$i]->shopping_quotation_id == ""){
                 $data[$i]['id_compra'] = "display:none";
            }else{

                $data[$i]['id_compra']  = $payments[$i]->ShoppingQuotation->shoppings_id;
                
            }

            if ($payments[$i]->payments_statuses_id == 1)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 2)
                $data[$i]['label'] = 'label-info';
            else if ($payments[$i]->payments_statuses_id == 3)
                $data[$i]['label'] = 'label-danger';
            else if ($payments[$i]->payments_statuses_id == 4)
                $data[$i]['label'] = 'label-success';
            else if ($payments[$i]->payments_statuses_id == 5)
                $data[$i]['label'] = 'label-default';
            else if ($payments[$i]->payments_statuses_id == 6)
                $data[$i]['label'] = 'label-warning';
            else if ($payments[$i]->payments_statuses_id == 7)
                $data[$i]['label'] = 'label-warning';
        }

        if (count($user->profiles) == 0) {
            echo "el Usuario no tiene ningun perfil";
        } else {  

            if (isset($_GET['nombre'])) {
                return View::make('dashboard.pagos.tabla_aprobar_pagos_hist')
                                ->with('pagos', $data);
            } else {                      
            
                return View::make('dashboard.index')
                ->with('container', 'dashboard.pagos.mis_pagos')
                ->with('user', $user)
                ->with('applicants', $applicants)
                ->with('pag', $payments)
                ->with('payments', $payments)
                ->with('pagos', $data)
                ->with('submenu_activo', 'Pagos Devueltos')
                ->with('menu_activo', 'Pagos');
            }
        }

    }

    function postActualizardatos(){
        $my_id = Auth::user()->id;

        $fecha_pago = e(Input::get('fecha_pago'));
        $forma_pago = e(Input::get('forma_pago'));
        $id         = e(Input::get('id'));

        $pago = DB::table('payments')->select('status_dev')->where('id', $id)->get();

        $status_dev =  $pago[0]->status_dev;
        
        $ruta = 'assets/files/facturas/'; 
        foreach ($_FILES as $key) //Iteramos el arreglo de archivos
        {
            if($key['error'] == UPLOAD_ERR_OK )//Si el archivo se paso correctamente Ccontinuamos 
                {
                    $NombreOriginal = $id."_factura"."_".utf8_decode($key['name']);//Obtenemos el nombre original del archivos
                    $temporal = $key['tmp_name']; //Obtenemos la ruta Original del archivo
                    $Destino = $ruta.$NombreOriginal;   //Creamos una ruta de destino con la variable ruta y el nombre original del archivo 
                    $description_trace = 'Actualizó un pago';
                    move_uploaded_file($temporal, $Destino); //Movemos el archivo temporal a la ruta especificada

                    DB::update('update payments set payment_date = ?, payments_methods_id = ?, file = ?,payments_statuses_id = ? where id = ?', array($fecha_pago, $forma_pago, $Destino,$status_dev, $id));
                    $payment_traces  = new PaymentsTraces;

                    
                    $payment_traces->description    = $description_trace;
                    $payment_traces->comment        = 'Actualización de pago';
                    $payment_traces->payments_id    = $id;
                    $payment_traces->users_id       = $my_id;

                    $payment_traces->save();
                    
                    echo "1";
                }
        }
        
    }
    public function getEditardocumento(){
        $my_id = Auth::user()->id;
        $id = Input::get('id');
        $comentario = Input::get('observacion');

        $description_trace = 'Edito el documento contable';

        $payment_traces  = new PaymentsTraces;

        
        $payment_traces->description    = $description_trace;
        $payment_traces->comment        = $comentario;
        $payment_traces->payments_id    = $id;
        $payment_traces->users_id       = $my_id;

        $payment_traces->save();

        echo "1";

    }

    

}

?>
