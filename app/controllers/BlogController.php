<?php

/**
 * Controlador Intranet	
 */
class BlogController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth'); //bloqueo de acceso
    }
    
    public function formatoFecha($fecha) {

        $transf = strtotime($fecha);
        $dia = date("d", $transf);
        $mes = date("F", $transf);
        if ($mes == "January")
            $mes = "Enero";
        if ($mes == "February")
            $mes = "Febrero";
        if ($mes == "March")
            $mes = "Marzo";
        if ($mes == "April")
            $mes = "Abril";
        if ($mes == "May")
            $mes = "Mayo";
        if ($mes == "June")
            $mes = "Junio";
        if ($mes == "July")
            $mes = "Julio";
        if ($mes == "August")
            $mes = "Agosto";
        if ($mes == "September")
            $mes = "Setiembre";
        if ($mes == "October")
            $mes = "Octubre";
        if ($mes == "November")
            $mes = "Noviembre";
        if ($mes == "December")
            $mes = "Diciembre";
        $ano = date("Y", $transf);

        return $dia . " de " . $mes . " de " . $ano;
    }

    public function permission_control($menu_id) {
        $user = Users::find(Auth::user()->id);
        $submenu_required = $menu_id;
        $bandera = 0;
        foreach ($user->Profiles as $perfil) {
            foreach ($perfil->submenus as $submenu) {
                if ($submenu_required == $submenu->id) {
                    $bandera = "1";
                }
            }
        }
        return $bandera;
    }

    public function getBlog() {
        
        $permission = $this->permission_control("80");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Informacion General')
                            ->with('menu_activo', 'Intranet');
        }
        
        $blogs = DB::table('blogs')
                ->leftjoin('blog_views', 'blogs.id', '=', 'blog_views.blogs_id')
                ->select('blogs.id', 'blogs.title', 'blogs.description', 'blogs.article', 'blogs.image_1', 'blogs.image_2', 'blogs.users_id', 'blogs.views', 'blogs.public', 'blogs.created_at')
                ->where('blog_views.users_id', Auth::user()->id)
                ->orwhere('blogs.public', '1')
                ->orderby('id', 'DESC')
                ->paginate(5);
        
        $sql = "SELECT DISTINCT blog_tags.tag FROM blog_tags
               LEFT JOIN blogs ON (blogs.id = blog_tags.blogs_id) 
               LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id) 
               WHERE blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1";
        $tags_general = DB::select($sql);
        
        $sql = "SELECT blogs.id, blogs.title, blogs.description, blogs.article, blogs.image_1, blogs.image_2, blogs.users_id, blogs.views, blogs.public, blogs.created_at
                FROM blogs LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id) WHERE blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1 ORDER BY views DESC limit 5";
        $tops = DB::select($sql);
        
        //construimos la vista
        return View::make('dashboard.index')
                        ->with('blogs', $blogs)
                        ->with('pag', $blogs)
                        ->with('tops', $tops)
                        ->with('tags_general', $tags_general)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.blog.blog')
                        ->with('menu_activo', 'Blog')
                        ->with('submenu_activo', 'Blog');
    }

    public function getNuevopost() {
        
        $permission = $this->permission_control("81");
        if ($permission == 0) {
            return View::make('dashboard.index')
                            ->with('container', 'errors.access_denied_ad')
                            ->with('submenu_activo', 'Informacion General')
                            ->with('menu_activo', 'Intranet');
        }
        
        $categorias = CategoriesProfiles::All();
        $procesos = Processes::All();

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.blog.gestion_blog')
                        ->with('categorias', $categorias)
                        ->with('procesos', $procesos)
                        ->with('submenu_activo', 'Nuevo Post')
                        ->with('menu_activo', 'Blog');
    }
    
    public function getEditarblog() {

        $id = Input::get('id');
        $blog = Blogs::find($id);
        $sql = "SELECT blog_views.status, blog_views.required, blog_views.id as id_views, blog_views.updated_at, users.id, users.name, users.last_name, users.name2, users.last_name2 FROM blog_views
                JOIN users ON (users.id = blog_views.users_id)
                WHERE blogs_id = '".$id."'";
        $usuarios = DB::select($sql);
       
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.blog.editar_blog')
                        ->with('blog', $blog)
                        ->with('usuarios', $usuarios)
                        ->with('submenu_activo', 'Blog')
                        ->with('menu_activo', 'Blog');
    }
    
    public function postGuardareditarblog() {
        
        include "assets/plugins/class_image/plGeneral.fnc.php";
        include "assets/plugins/class_image/PhpThumbFactory.class.php";
        include "assets/plugins/class_image/ClassFile.class.php";

        $id = Input::get('id_post');
        $titulo = Input::get('titulo');
        $desc = Input::get('desc');
        $articulo = Input::get('articulo');
        $public = Input::get('public');
        
        DB::table('blogs')->where('id', $id)->update(array('title' => $titulo, 'description' => $desc, 'article' => $articulo, 'public' => $public));
        
        if (Input::hasFile('imagen_previa')) {
                
                $filename = $_FILES["imagen_previa"]["tmp_name"];
                $name = $_FILES["imagen_previa"]["name"];
                //$uploadSuccess = move_uploaded_file($filename, $destinationPath);
                
                $retorno = ClassFIle::UploadImagenFile("imagen_previa", "assets/img/blog/prev/", $name, $name, 390, 292);

                if ($retorno["Status"]=="Uploader"){
                    $destinationPath = 'assets/img/blog/prev/' . $name;
                     DB::table('blogs')->where('id', $id)->update(array('image_1' => $destinationPath));
                            } else {
                                echo "error";
                            }
            
        } else {
            echo "no";
        }
        
        if (Input::hasFile('imagen_post')) {
                        
                            $filename = $_FILES["imagen_post"]["tmp_name"];
                            $name = $_FILES["imagen_post"]["name"];
                            //$uploadSuccess = move_uploaded_file($filename, $destinationPath1);
                            
                            $retorno2 = ClassFIle::UploadImagenFile("imagen_post", "assets/img/blog/post/", $name, $name, 946, 381);

                            if ($retorno2["Status"]=="Uploader"){
                                
                                $destinationPath1 = 'assets/img/blog/post/' . $name;
                                
                                DB::table('blogs')->where('id', $id)->update(array('image_2' => $destinationPath1));
                            } else {
                                echo "error";
                            }                                                
        } else {
            echo "no";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');
        }
        
            $url = "http://192.168.0.18/indo/public/blog/blog";
            return Redirect::away($url);
    }
    
    public function getDetallepost() {

        $id = Input::get('id');
        $sql = "SELECT blogs.id, blogs.title, blogs.description, blogs.article, blogs.image_1, blogs.image_2, blogs.users_id, blogs.views, blogs.public, blogs.created_at
                FROM blogs LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id) WHERE blogs.id = '".$id."' AND (blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1) ORDER BY views DESC limit 5";
        $blog = DB::select($sql);
        if(!$blog){
            $url = "http://192.168.0.18/indo/public/blog";
            return Redirect::away($url);
        }
        
        $sql = "SELECT DISTINCT blog_tags.tag FROM blog_tags
               LEFT JOIN blogs ON (blogs.id = blog_tags.blogs_id)
               LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id)
               WHERE blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1";
        $tags_general = DB::select($sql);
                        
        $tags = BlogTags::where('blogs_id', $id)->get();
        $comentarios = BlogComments::where('blogs_id', $id)->get();
        
        $views = $blog[0]->views+1;
        
        $sql = "SELECT blogs.id, blogs.title, blogs.description, blogs.article, blogs.image_1, blogs.image_2, blogs.users_id, blogs.views, blogs.public, blogs.created_at
                FROM blogs LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id) WHERE blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1 ORDER BY views DESC limit 5";
        $tops = DB::select($sql);
        
        DB::table('blogs')->where('id', $id)->update(array('views' => $views));
        
        DB::table('blog_views')->where('blogs_id', $id)->where('users_id', Auth::user()->id)->update(array('status' => 1));
       
        return View::make('dashboard.index')
                        ->with('container', 'dashboard.blog.detalle_blog')
                        ->with('blog', $blog)
                        ->with('tags', $tags)
                        ->with('tops', $tops)
                        ->with('tags_general', $tags_general)
                        ->with('comentarios', $comentarios)
                        ->with('submenu_activo', 'Blog')
                        ->with('menu_activo', 'Blog');
    }
    
    public function getTag() {

        $tag = Input::get('tag');
        
        $blogs = DB::table('blogs')
                ->leftjoin('blog_views', 'blogs.id', '=', 'blog_views.blogs_id')
                ->leftjoin('blog_tags', 'blogs.id', '=', 'blog_tags.blogs_id')
                ->select('blogs.id', 'blogs.title', 'blogs.description', 'blogs.article', 'blogs.image_1', 'blogs.image_2', 'blogs.users_id', 'blogs.views', 'blogs.public', 'blogs.created_at')
                ->where('blog_tags.tag', 'LIKE', '%'.$tag.'%')
                ->where(function($query)
                {
                $query->where('blog_views.users_id', '=', Auth::user()->id)
                      ->orwhere('blogs.public', '=', '1');
                })
                ->orderby('id', 'DESC')
                ->paginate(5);
        
        $sql = "SELECT DISTINCT blog_tags.tag FROM blog_tags
               LEFT JOIN blogs ON (blogs.id = blog_tags.blogs_id)
               LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id)
               WHERE blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1";
        $tags_general = DB::select($sql);
        
        $sql = "SELECT blogs.id, blogs.title, blogs.description, blogs.article, blogs.image_1, blogs.image_2, blogs.users_id, blogs.views, blogs.public, blogs.created_at
                FROM blogs LEFT JOIN blog_views ON (blogs.id = blog_views.blogs_id) WHERE blog_views.users_id = '".Auth::user()->id."' OR blogs.public = 1 ORDER BY views DESC limit 5";
        $tops = DB::select($sql);
       
        return View::make('dashboard.index')
                        ->with('blogs', $blogs)
                        ->with('pag', $blogs)
                        ->with('tops', $tops)
                        ->with('tags_general', $tags_general)
                        ->with('content', 'container')
                        ->with('container', 'dashboard.blog.blog')
                        ->with('menu_activo', 'Blog')
                        ->with('submenu_activo', 'Blog');
    }
    
    public function getGuardarcomentario() {

        $comentario_user = Input::get('comentario_user');
        $id = Input::get('id');

                                $comment = new BlogComments;
                                $comment->comment = $comentario_user;
                                $comment->blogs_id = $id;
                                $comment->users_id = Auth::user()->id;
                                $comment->save();
                                
                echo '<div class="media">
                        <a href="#" class="pull-left">
                        <img alt="" src="../'.Auth::user()->img_min.'" class="media-object">
                        </a>
                        <div class="media-body">
                           <h4 class="media-heading">'.Auth::user()->name.' '.Auth::user()->last_name.'<span>Hace un momento</span></h4>
                           <p>'.$comentario_user.'</p>
                        </div>
                     </div>';

    }

    public function postGuardarpost() {
        
        include "assets/plugins/class_image/plGeneral.fnc.php";
        include "assets/plugins/class_image/PhpThumbFactory.class.php";
        include "assets/plugins/class_image/ClassFile.class.php";

        $titulo = Input::get('titulo');
        $desc = Input::get('desc');
        $articulo = Input::get('articulo');
        $tags = explode(",", Input::get('final_tags'));
        $public = Input::get('public');
        $dirigido = Input::get('dirigido');
        $requerido = Input::get('requerido');
        $proceso = Input::get('proceso');

        if (Input::hasFile('imagen_previa')) {
            
                $filename = $_FILES["imagen_previa"]["tmp_name"];
                $name = $_FILES["imagen_previa"]["name"];
                //$uploadSuccess = move_uploaded_file($filename, $destinationPath);
                
                $retorno = ClassFIle::UploadImagenFile("imagen_previa", "assets/img/blog/prev/", $name, $name, 390, 292);

                if ($retorno["Status"]=="Uploader"){
                    $destinationPath = 'assets/img/blog/prev/' . $name;

                    if (Input::hasFile('imagen_post')) {
                        
                            $filename = $_FILES["imagen_post"]["tmp_name"];
                            $name = $_FILES["imagen_post"]["name"];
                            //$uploadSuccess = move_uploaded_file($filename, $destinationPath1);
                            
                            $retorno2 = ClassFIle::UploadImagenFile("imagen_post", "assets/img/blog/post/", $name, $name, 946, 381);

                            if ($retorno2["Status"]=="Uploader"){
                                
                                $destinationPath1 = 'assets/img/blog/post/' . $name;

                                $img = new Blogs;
                                $img->title = $titulo;
                                $img->description = $desc;
                                $img->article = $articulo;
                                $img->image_1 = $destinationPath;
                                $img->image_2 = $destinationPath1;
                                $img->users_id = Auth::user()->id;
                                $img->public = $public;
                                
                                if($img->save()){
                                    
                                    $blogs = Blogs::All();
                                    $id_blog = $blogs->last()->id;
                                    
                                    
                                        if($public==0){
                                            if($dirigido==1){
                                                
                                                $sql = "SELECT users.id FROM users
                                                    JOIN profiles_users ON (users.id = profiles_users.users_id)
                                                    JOIN profiles ON (profiles.id = profiles_users.profiles_id)
                                                    JOIN categories_profiles ON (profiles.categories_profile_id = categories_profiles.id)
                                                    WHERE categories_profiles.id = '$dirigido' AND profiles.processes_id = '$proceso'";
                                                
                                            }else{
                                                
                                                $sql = "SELECT users.id FROM users
                                                    JOIN profiles_users ON (users.id = profiles_users.users_id)
                                                    JOIN profiles ON (profiles.id = profiles_users.profiles_id)
                                                    JOIN categories_profiles ON (profiles.categories_profile_id = categories_profiles.id)
                                                    WHERE categories_profiles.id = '$dirigido'";
                                                
                                            }
                                            $permisos = DB::select($sql);
                                            
                                                $blog_views = new BlogViews;
                                                $blog_views->blogs_id = $id_blog; 
                                                $blog_views->users_id = Auth::user()->id;
                                                $blog_views->required = $requerido;
                                                $blog_views->status = 1;
                                                $blog_views->save();

                                            foreach ($permisos as $permiso){
                                                
                                                if($permiso->id <> Auth::user()->id){
                                                    $blog_views = new BlogViews;
                                                    $blog_views->blogs_id = $id_blog; 
                                                    $blog_views->users_id = $permiso->id;
                                                    $blog_views->required = $requerido;
                                                    $blog_views->save();
                                                    if($requerido==1){
                                                        $not = new Notifications;
                                                        $not->description = "Publico un nuevo post de lectura obligatoria";
                                                        $not->estate = 1;
                                                        $not->type_notifications_id = 6;
                                                        $not->user_id = $permiso->id;
                                                        $not->user_id1 = Auth::user()->id;
                                                        $not->link = "blog/detallepost?id=".$id_blog;
                                                        $not->save();
                                                    }
                                                }
                                            }
                                        }
                                        
                                    foreach($tags as $tag){
                                    $blog_tags = new BlogTags;
                                    $blog_tags->tag = $tag;
                                    $blog_tags->blogs_id = $id_blog;
                                    $blog_tags->save();
                                    }
                                }
                                
                            } else {
                                echo "error";
                            }

                    } else {
                        echo "error";
                    }
                }
            

            $url = "http://192.168.0.18/indo/public/blog/blog";
            return Redirect::away($url);
        } else {
            echo "no";
            //return Redirect::to($url)->with('mensaje','¡ERROR!.');
        }

        return View::make('dashboard.index')
                        ->with('container', 'dashboard.blog.gestion_blog')
                        ->with('submenu_activo', 'Blog')
                        ->with('menu_activo', 'Nuevo Post');
    }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

}
?>