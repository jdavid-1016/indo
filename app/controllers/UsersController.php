<?php 	
/**
* 	
*/
class UsersController extends BaseController
{
	public function __construct()
	{
		$this->beforeFilter('auth'); //bloqueo de acceso
	}

	public function getIndex()
	{
		$my_id = Auth::user()->id;
		$level = Auth::user()->rol_id;

		//control permissions only access administrator (ad)
		if($level==1)
		{
			$users = DB::table('users')->where('rol_id','<>','1')->where('id','<>',$my_id)->get();
			return View::make('dashboard.index')
			->with('users',$users)
			->with('container', 'dashboard.users.users')
			->with('menu_activo', 'administration');
			//echo var_dump($users);
		}else{
			return View::make('dashboard.index')
			->with('container', 'errors.access_denied_ad')
			->with('menu_activo', 'administration');
			
		}
		
	}
	public function all_my_notifications()
	{
		$my_id = Auth::user()->id;
		

		$noti = Notifications::where('user_id' ,'=', Auth::user()->id)
		->orderBy('id', 'desc')
		->get();

		return View::make('dashboard.index')
		->with('container', 'dashboard.administration.notifications')
		->with('notifications', $noti)
		->with('submenu_activo', 'Mis solicitudes')
		->with('menu_activo', 'Help Desk');
		//echo var_dump($users);
		
		
	}
	public function Create()
	{
		
		$document = Input::get("document");
		$profile = Input::get("profile");

		$user = new Users;

		$user->name= Input::get('name');
		$user->name2= Input::get('name2');
		$user->last_name= Input::get('last_name');
		$user->last_name2= Input::get('last_name2');
		$user->email_institutional= Input::get('email_institutional');
		$user->email= Input::get('email');
		$user->cell_phone= Input::get('phone');	
		$user->address= Input::get('address');
		$user->phone= Input::get('phone');
		$user->type_document= Input::get('type_document');
		$user->document= Input::get('document');
		$user->city_id= Input::get('city_id');
		
		//guardamos
		$user->save();


		$last_user = Users::all();

		$id_user = $last_user->last()->id;

		DB::table('profiles_users')->insert(
		    array('users_id' => $id_user, 'profiles_id' => $profile )
		);


		//rediigimos a usuarios
		return Response::json(['success'=>true]);

	}

	public function create_user_new()
	{
		
		$document 	= Input::get("document");
		$profile 	= Input::get("profile_id");
		$extention 	= Input::get("extention");

		if (Input::get('name') == "" 
			or Input::get('last_name') == ""
			or Input::get('email_institutional') == ""
			or Input::get('document') == ""
			or Input::get('city_id')== ""
			or Input::get('process_id')== "" 
			or Input::get('profile_id')== "")  
		{
			echo "3";
			exit;
		}else{



			$user = new Users;

			$user->name 				= Input::get('name');
			$user->name2 				= Input::get('name2');
			$user->last_name 			= Input::get('last_name');
			$user->last_name2			= Input::get('last_name2');
			$user->email_institutional 	= Input::get('email_institutional');
			$user->email 				= Input::get('email');
			$user->cell_phone 			= Input::get('phone');	
			$user->address  			= Input::get('address');
			$user->phone 				= Input::get('phone');
			$user->type_document 		= Input::get('type_document');
			$user->document  			= Input::get('document');
			$user->city_id 				= Input::get('city_id');
			$user->img 					= "assets/img/profiles/default.jpg";
			$user->img_profile			= "assets/img/profile/default.jpg";
			$user->img_min 				= "assets/img/avatar_small/default.jpg";
			
			//guardamos
			
			if ($user->save()) {
				
				$last_user = Users::all();
				$id_user = $last_user->last()->id;
				DB::table('profiles_users')->insert(
				    array('users_id' => $id_user, 'profiles_id' => $profile )
				);

				DB::table('administratives')->insert(
				    array('extension' => $extention,'responsible' => "0", 'user_id' => $id_user )
				);
				echo 1;
			}else{
				echo 2;
			}
		}

	}
	
	public function getDelete($user_id)
	{
		//buscamos el usuario en la base de datos segun la id enviada
		$user = Users::find($user_id);
		//eliminamos y redirigimos
		$user->delete();
		return Redirect::to('usuarios')->with('status', 'ok_delete');
	}



	public function edit_user(){
		$id = Input::get('valorCaja1');

		$data = Users::find($id);

		//$data = Supports::find($id);


		$submenus = Submenus::all();
		$categories = SupportCategory::all();

		$technical = DB::table('users')
		->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
		->join('profiles', 'profiles.id', '=','profiles_users.profiles_id')
		->select('users.id', 'users.name', 'users.last_name')
		->where('profiles.processes_id', 7)
		->where('users.status', 1)
		->orderBy('name','asc')
		->get();

		

		return View::make('dashboard.users.users_edit')
		->with('submenus',$submenus)
		->with('categories',$categories)
		->with('technicals', $technical)
		->with('data',$data);
	}
	public function edit_user2(){
		$id = Input::get('valorCaja1');

		$data = Users::find($id);
		$processes = processes::get();
		$profiles = Profiles::get();

		//$data = Supports::find($id);


		$submenus = Submenus::all();
		$categories = SupportCategory::all();
		$cities = Cities::all();

		$technical = DB::table('users')
		->join('profiles_users', 'users.id', '=', 'profiles_users.users_id')
		->join('profiles', 'profiles.id', '=','profiles_users.profiles_id')
		->select('users.id', 'users.name', 'users.last_name')
		->where('profiles.processes_id', 7)
		->where('users.status', 1)
		->orderBy('name','asc')
		->get();

		

		return View::make('dashboard.users.users_edit2')
		->with('submenus',$submenus)
		->with('categories',$categories)
		->with('technicals', $technical)
		->with('processes', $processes)
		->with('profiles', $profiles)
		->with('cities', $cities)
		->with('data',$data);
	}

	public function exist_new_profile(){
		$profile = Input::get('name_new_profile');
		//echo $profile;

		$data = Profiles::where('profile', $profile)->get();

		if (count($data) == "" ) {
			echo "2";
		}else{
			echo "error";
		}
	}
	public function selected_profile_process(){
		$process = Input::get('process');
		//echo $profile;

		$data = Profiles::where('processes_id', $process)->get();

		foreach ($data as $key) {
			echo "<option value='$key->id'>$key->profile</option>";
		}
	}
	public function selected_submenu_profile(){
            $menus = "";
            $bandera ="";
            $cont=0;
            
            if(Input::get('profile')){
            	$profile = Input::get('profile');
				//echo $profile;                
				$data = Profiles::find($profile);

                return View::make('dashboard.users.multiselect')
                ->with('data',$data);
            }else{
            	$profile = Input::get('profile_add');
				//echo $profile;
				$submenus_profile = Profiles::find($profile);
                $submenus = Submenus::get();
                $sql = "";
	        	$h = 0;
                foreach ($submenus_profile->Submenus as $key) {
                    if($h==0){
	        			$sql .= "id <> '".$key->id."'";
	        		}else{
	        			$sql .= " and id <> '".$key->id."'";
	        		}
	        		$h++;
                }
                
	        	$sql2 = "SELECT * FROM submenus WHERE (".$sql.")";

	        	$data = DB::select($sql2);
                
                
                return View::make('dashboard.users.multiselect_add')
                ->with('data',$data);
            }                
	}

	public function selected_submenu_profile2(){
		$profile = Input::get('profile');
		//echo $profile;
		$data = Profiles::find($profile);
        return View::make('dashboard.users.users')
        ->with('data',$data);
	}
	public function form_createusers(){
		$user = Users::find(Auth::user()->id);
		$cities = Cities::get();
		$processes = processes::get();
		$submenus = Submenus::get();
		$menus = Menus::get();
		$profiles = Profiles::get();
		       
		
		return View::make('dashboard.users.form_createusers')

		->with('cities', $cities)
		->with('container', 'dashboard.users.users')
		->with('user', $user)
		->with('processes', $processes)
		->with('submenus', $submenus)
		->with('menus', $menus)
		->with('profiles', $profiles)
		->with('submenu_activo', 'Administrar Usuarios')
		->with('menu_activo', 'Sistema');
		
	
		
        return View::make('dashboard.users.form_createusers');
        
	}

        
    public function guardarPerfil() {
            $perfil = Input::get('perfil');
            $proceso_perfil = Input::get('proceso_perfil');
            $permisos = Input::get('permisos');
            /*$permisos = array(
              "permiso" => Input::get('permisos')
            );*/
            
            $profiles = new Profiles;

            $profiles->profile = $perfil;
            $profiles->categories_profile_id = 1;
            $profiles->processes_id = $proceso_perfil;

            $profiles->save();
            
            $bandera= Profiles::Where("processes_id", $proceso_perfil)->get();
            $bandera = $bandera->last()->id;
        
            
            foreach ($permisos as $per) {
                
                DB::table('profiles_submenus')->insert(
                    array('profiles_id' => $bandera, 'submenus_id' => $per)
                );                            
                
            }
            
        	echo 1;
    }
    
    public function eliminarPermisos() {
        $perfil = Input::get('perfil');            
        $permisos_e = Input::get('permisos_e');                                    
        
        foreach ($permisos_e as $per) {
            DB::table('profiles_submenus')->where('profiles_id', '=', $perfil)->where('submenus_id', '=', $per)->delete();                                
            
        }
            
        echo 1;
    }
    
    public function agregarPermisos() {
            $perfil = Input::get('perfil');            
            $permisos_a = Input::get('permisos_a');                                    
            
            foreach ($permisos_a as $per) {
                
                DB::table('profiles_submenus')->insert(
                    array('profiles_id' => $perfil, 'submenus_id' => $per)
                );     
                
            }
            
        	echo 1;
    }

    public function actualizarusuario(){

    	$id = Input::get('id_user');

    	$name = Input::get("name");
    	$name2 = Input::get("name2");
    	$last_name = Input::get("last_name");
    	$last_name2 = Input::get("last_name2");
    	$email_institutional = Input::get("email_institutional");
    	$email = Input::get("email");
    	$cell_phone = Input::get("cell_phone");
    	$phone = Input::get("phone");
    	$direccion = Input::get("direccion");
    	$type_document = Input::get("type_document");
    	$document = Input::get("document");
    	$city_id = Input::get("city_id");
    	$proceso_perfil = Input::get("proceso_perfil");
    	$profile_user = Input::get("profile_user");
    	$id_user = Input::get("id_user");
    	

		

		DB::update('update users set name = ?, name2 = ?,last_name = ?,last_name2 = ?,email_institutional = ?,email = ?,cell_phone = ?,address = ?,phone = ?,type_document = ?,document = ?,city_id  = ? where id = ?', array(
										$name,
										$name2,
										$last_name,
										$last_name2,
										$email_institutional,
										$email,
										$cell_phone,
										$direccion,
										$phone,
										$type_document,
										$document,
										$city_id,
										$id_user
									));


		echo "2";

    }
}

?>