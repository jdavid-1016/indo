<?php  
/**
* 
*/
class EquipmentItemMaintenances extends Eloquent
{
	
	    public function EquipmentTypeMaintenances(){
			return $this->belongsTo('EquipmentTypeMaintenances');
		}
		public function EquipmentMaintenances(){
		    return $this->belongsToMany('EquipmentMaintenances', 'equipment_maintenances_details', 'equipment_item_maintenances_id', 'equipment_maintenances_id');
		}
	

}
?>