<?php
/**
* 
*/
class Tasks extends Eloquent
{
	
	public function TasksCategories(){
		return $this->belongsTo('TasksCategories');
	}
	public function TasksStatuses(){
		return $this->belongsTo('TasksStatuses');
	}
	public function Users(){
		return $this->belongsTo('Users');
	}
	public function TasksPeriodicity(){
		return $this->belongsTo('TasksPeriodicity');
	}
	public function TasksComents(){
		return $this->hasMany('TasksComents');
	}

}
?>