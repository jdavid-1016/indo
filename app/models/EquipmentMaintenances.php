<?php  
/**
* 
*/
class EquipmentMaintenances extends Eloquent
{
	    public function Users(){
			return $this->belongsTo('users');
		}
		public function EquipmentTypeMaintenances(){
			return $this->belongsTo('EquipmentTypeMaintenances');
		}
		public function EquipmentMaintenancesStatuses(){
			return $this->belongsTo('EquipmentMaintenancesStatuses');
		}
		public function EquipmentMaintenancesDetails(){
			return $this->hasMany('EquipmentMaintenancesDetails');
		}
		public function EquipmentInventories(){
		    return $this->belongsToMany('EquipmentInventories', 'equipment_maintenances_details', 'equipment_maintenances_id', 'equipment_inventories_id');
		}
		public function EquipmentItemMaintenances(){
		    return $this->belongsToMany('EquipmentItemMaintenances', 'equipment_maintenances_details', 'equipment_maintenances_id', 'equipment_item_maintenances_id');
		}

}
?>