<?php  
/**
* 
*/
class Processes extends Eloquent
{
	public $timestamps = false;
	//relacion uno a muchos entre processes y requests
	public function requests(){
		return $this->hasMany('Requests');
	}
	//relacion uno a muchos entre processes y requests
	public function users(){
		return $this->hasMany('users');
	}
        
        //relacion uno a muchos entre processes y profiles
	public function profiles(){
		return $this->hasMany('profiles');
	}

}
?>