<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Users extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $fillable = array('name', 'name2' ,'last_name' ,'last_name2','password','email_institutional','email','cell_phone','phone','user','type_document','document','city_id');
	//public static $table = 'users';
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
        
    public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
        
        //relacion uno a muchos entre users y Administratives
	public function administratives(){
		return $this->hasMany('administratives');
	}

	//relacion uno a muchos entre users y requests
	public function pqrs(){
		return $this->hasMany('pqrs');
	}
        
        //relacion uno a muchos entre users y income_statistics
	public function IncomeStatistics(){
		return $this->hasMany('IncomeStatistics');
	}
                
	public function RecordFaqs(){
		return $this->hasMany('RecordFaqs');
	}

	public function processes(){
		return $this->hasMany('processes');
	}

	//relacion uno a muchos entre users y traceabilitys
	public function traceabilitys(){
		return $this->hasMany('traceabilitys');
	}
	public function PaymentsTraces(){
		return $this->hasMany('PaymentsTraces');
	}
    public function Supports(){
		return $this->hasMany('Supports');
	}
	public function Payments(){
		return $this->hasMany('Payments');
	}
	public function Shoppings(){
		return $this->hasMany('Shoppings');
	}
	public function ShoppingTraces(){
		return $this->hasMany('ShoppingTraces');
	}
	public function SupportTraces(){
		return $this->hasMany('SupportTraces');
	}

	//un usuario pertenece a un proceso
	public function City(){
		return $this->belongsTo('City');
	}
                
	public function BlogComments(){
		return $this->belongsTo('BlogComments');
	}
        
        //un usuario pertenece a un proceso
	public function Blogs(){
		return $this->belongsTo('Blogs');
	}

	public function DefaultText(){
		return $this->hasMany('DefaultText');
	}
	public function DefaultConfidential(){
		return $this->hasMany('DefaultConfidential');
	}

	//relación muchos a muchos entre usuarios y perfiles
	public function Profiles(){
		// return $this->belongsToMany('Profiles');
		return $this->belongsToMany('Profiles', 'profiles_users', 'users_id', 'profiles_id');
	}
	public function delete(){
		//eliminamos los request
		if (count($this->request)>0) {
			foreach ($$this->request as $request) {
				$request->delete();
			}
		}
		//eliminamos la información de la tabla menu_user con detach
		//que hace referencia al usuario
		if(count($this->menus)>0){
			$this->menus()->detach();
		}

		//eliminamos al usuario
		return parent::delete();  
	}

	 
	public function EquipmentInventories(){
	    return $this->belongsToMany('EquipmentInventories', 'equipment_inventories_users', 'users_id', 'equipment_inventories_id');
	}
	
	public function EquipmentMaintenances(){
		return $this->hasMany('EquipmentMaintenances');
	}
        
    public function AspirantsTraces(){
		return $this->hasMany('AspirantsTraces');
	}
	
    public function AspirantsSchoolTraces(){
		return $this->hasMany('AspirantsSchoolTraces');
	}
                
	public function CoursesStudents(){
		return $this->hasMany('CoursesStudents');
	}

	public function Tasks(){
		return $this->hasMany('Tasks');
	}
	public function TasksHasUsers(){
		return $this->hasMany('TasksHasUsers');
	}
	public function CoursesInstructors(){
		return $this->hasMany('CoursesInstructors');
	}





	public function Countries(){
		return $this->belongsTo('Countries');
	}
	public function GeneralMaritalStatus(){
		return $this->belongsTo('GeneralMaritalStatus');
	}
	public function GeneralBloodGroup(){
		return $this->belongsTo('generalBloodGroup');
	}
	public function GeneralGenres(){
		return $this->belongsTo('GeneralGenres');
	}
	public function TasksComents(){
		return $this->hasMany('TasksComents');
	}

	

}
