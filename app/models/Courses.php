<?php
/**
* 
*/
class Courses extends Eloquent
{
	
	public function CoursesInstructors(){
		return $this->belongsTo('CoursesInstructors');
	}
	public function CoursesInspectors(){
		return $this->belongsTo('CoursesInspectors');
	}
	public function CoursesCategories(){
		return $this->belongsTo('CoursesCategories');
	}
    public function CoursesUsers(){
	    return $this->belongsToMany('CoursesUsers', 'courses_courses_users', 'courses_id', 'courses_users_id');
	}
    public function CoursesSubjects(){
		return $this->hasMany('CoursesSubjects');
	}
        
    public function CoursesStudents(){
	    return $this->belongsToMany('CoursesStudents', 'courses_has_courses_students', 'courses_id', 'courses_students_id');
	}

}
?>