<?php 
/**
* 
*/
class Supports extends Eloquent
{
	
	public function SupportCategory(){
		return $this->belongsTo('SupportCategory');
	}

	public function SupportStatus(){
		return $this->belongsTo('SupportStatus');
	}
	public function Users(){
		return $this->belongsTo('Users');
	}
	public function SupportTraces(){
		return $this->hasMany('SupportTraces');
	}
}
?>