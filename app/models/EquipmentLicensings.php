<?php
/**
* 
*/
class EquipmentLicensings extends Eloquent
{
	
	public function EquipmentKeyLicenses(){
	    return $this->hasMany('EquipmentKeyLicenses');
	}
        
        public function EquipmentTypeLicenses(){
	    return $this->belongsTo('EquipmentTypeLicenses');
	}

}
?>