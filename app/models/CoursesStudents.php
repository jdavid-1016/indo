<?php  
/**
* 
*/
class CoursesStudents extends Eloquent
{    	
        
    public function Courses(){
	    return $this->belongsToMany('Courses', 'courses_has_courses_students', 'courses_students_id', 'courses_id');
	}
        
        
	public function Users(){
		return $this->belongsTo('Users');
	}

}
?>