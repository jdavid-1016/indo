<?php
/**
* 
*/
class AspirantsInformations extends Eloquent
{
		
	public function AspirantsStatusProspects(){
		return $this->belongsTo('AspirantsStatusProspects');
	}
        public function AspirantsProspects(){
		return $this->belongsTo('AspirantsProspects');
	}
        public function AspirantsPrograms(){
		return $this->belongsTo('AspirantsPrograms');
	}
        public function AspirantsStatusManagements(){
		return $this->belongsTo('AspirantsStatusManagements');
	}
        public function AspirantsTimes(){
		return $this->belongsTo('AspirantsTimes');
	}
        public function AspirantsPeriods(){
		return $this->belongsTo('AspirantsPeriods');
	}
        public function AspirantsTypeDocuments(){
		return $this->belongsTo('AspirantsTypeDocuments');
	}
        public function AspirantsMaritalStatuses(){
		return $this->belongsTo('AspirantsMaritalStatuses');
	}
        public function AspirantsGenders(){
		return $this->belongsTo('AspirantsGenders');
	}
        public function AspirantsCities(){
		return $this->belongsTo('AspirantsCities');
	}
        public function AspirantsBirthPlaces(){
		return $this->belongsTo('AspirantsBirthPlaces');
	}
        public function AspirantsLocations(){
		return $this->belongsTo('AspirantsLocations');
	}
        public function AspirantsSources(){
		return $this->belongsTo('AspirantsSources');
	}
        public function AspirantsTrainings(){
		return $this->belongsTo('AspirantsTrainings');
	}
        public function AspirantsResults(){
		return $this->hasMany('AspirantsResults');
	}
        public function AspirantsStatusResults(){
		return $this->belongsTo('AspirantsStatusResults');
	}
        public function AspirantsWorkings(){
		return $this->hasMany('AspirantsWorkings');
	}
        public function AspirantsFamilies(){
		return $this->hasMany('AspirantsFamilies');
	}
        public function AspirantsStudies(){
		return $this->hasMany('AspirantsStudies');
	}

}
?>