<?php
/**
* 
*/
class AspirantsTraces extends Eloquent
{
	public function AspirantsProspects(){
		return $this->belongsTo('AspirantsProspects');
	}
        
    public function AspirantsStatusProspects(){
		return $this->belongsTo('AspirantsStatusProspects');
	}
        
    public function Users(){
		return $this->belongsTo('Users');
	}

}
?>