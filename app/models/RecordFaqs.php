<?php  
/**
* 
*/
class RecordFaqs extends Eloquent
{
    protected $table = 'record_faqs';
    
    public function Users(){
		return $this->belongsTo('users');
	}
        
    public function SupportFaqs(){
		return $this->belongsTo('support_faqs');
	}

}
?>