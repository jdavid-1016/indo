<?php  
/**
* 
*/
class Pqrs extends Eloquent
{
	public $timestamps = false;
	//un requests pertenece a un usuario
	public function user(){
		return $this->belongsTo('User');
	}

	//un requests pertenece a un estado
	public function StateRequests(){
		return $this->belongsTo('StateRequests');
	}

	//un requests pertenece a un tipo de usuario
	public function TypeUsers(){
		return $this->belongsTo('TypeUsers');
	}

	//un requests pertenece a un tipo de requerimiento
	public function TypeRequests(){
		return $this->belongsTo('TypeRequests');
	}

	//un requests pertenece a un proceso
	public function process(){
		return $this->belongsTo('Processes');
	}

	//relacion uno a muchos entre requests y traceability
	public function Traceabilitys(){
		return $this->hasMany('Traceabilitys');
	}
}
?>