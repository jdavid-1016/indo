<?php  
/**
* 
*/
class CoursesInstructors extends Eloquent
{
    
    
	public function Courses(){
		return $this->hasMany('Courses');
	}
        
    public function CoursesSubjects(){
		return $this->hasMany('CoursesSubjects');
	}


	public function Users(){
		return $this->belongsTo('Users');
	}

}
?>