<?php  
/**
* 
*/
class Payments extends Eloquent
{
	
	
	public function PaymentsConcepts(){
		return $this->belongsTo('PaymentsConcepts');
	}
	public function PaymentsStatuses(){
		return $this->belongsTo('PaymentsStatuses');
	}
	public function Users(){
		return $this->belongsTo('Users');
	}

	public function ShoppingQuotation(){
		return $this->belongsTo('ShoppingQuotation');
	}
        
        public function Providers(){
		return $this->belongsTo('Providers');
	}
        
    public function PaymentsMethods(){
		return $this->belongsTo('PaymentsMethods');
	}
	public function PaymentsTraces(){
		return $this->belongsTo('PaymentsTraces');
	}

}
?>