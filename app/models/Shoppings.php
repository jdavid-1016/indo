<?php  
/**
* 
*/
class Shoppings extends Eloquent
{
	
	public function Users(){
		return $this->belongsTo('Users');
	}
	public function ShoppingStatuses(){
		return $this->belongsTo('ShoppingStatuses');
	}

	public function ShoppingDetails(){
		return $this->hasMany('ShoppingDetails');
	}

	public function ShoppingQuotation(){
		return $this->hasMany('ShoppingQuotation');
	}
	

}
?>
