<?php 
/**
* 
*/
class SupportCategory extends Eloquent
{
	
	public function Supports(){
		return $this->hasMany('Supports');
	}

	public function SupportFaqs(){
		return $this->hasMany('SupportFaqs');
	}
}

 ?>