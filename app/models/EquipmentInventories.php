<?php  
/**
* 
*/
class EquipmentInventories extends Eloquent
{
	public function Equipments(){
	    return $this->belongsToMany('Equipments', 'equipments_has_equipment_inventories', 'equipment_inventories_id', 'equipments_id');
	}

	public function EquipmentMaintenances(){
	    return $this->belongsToMany('EquipmentMaintenances', 'equipment_maintenances_details', 'equipment_inventories_id', 'equipment_maintenances_id');
	}

	public function EquipmentTypes(){
		return $this->belongsTo('EquipmentTypes');
	}
	public function EquipmentOffices(){
		return $this->belongsTo('EquipmentOffices');
	}
	public function EquipmentStatuses(){
		return $this->belongsTo('EquipmentStatuses');
	}

}
?>