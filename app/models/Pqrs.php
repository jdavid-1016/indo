<?php  
/**
* 
*/
class Pqrs extends Eloquent
{
	
	//un requests pertenece a un usuario
	public function Users(){
		return $this->belongsTo('Users');
	}

	//un requests pertenece a un estado
	public function PqrsStatuses(){
		return $this->belongsTo('PqrsStatuses');
	}

	//un requests pertenece a un tipo de usuario
	public function PqrsTypeUsers(){
		return $this->belongsTo('PqrsTypeUsers');
	}

	//un requests pertenece a un tipo de requerimiento
	public function PqrsTypes(){
		return $this->belongsTo('PqrsTypes');
	}

	//un requests pertenece a un proceso
	public function Processes(){
		return $this->belongsTo('Processes');
	}

	//relacion uno a muchos entre requests y traceability
	public function Traceabilitys(){
		return $this->hasMany('Traceabilitys');
	}
}
?>