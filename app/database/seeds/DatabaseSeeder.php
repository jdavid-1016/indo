<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UserTableSeeder');
		// $this->call('MenuTableSeeder');
		// $this->call('ProcessesTableSeeder');
		// $this->call('StateRequestsTableSeeder');
		// $this->call('TypeRequestsTableSeeder');
		// $this->call('TypeUsersTableSeeder');
	}

}
