$(function() {
	
	var root    = '../indoamericana/';

	var list 	= root + 'intranet/listaEmpleados/';
	var home 	= root + 'administrar/callHome/';
	var profile = root + 'user/';
	var addUser = root + 'user/addUSer';

	// función para controlar el progreso de una tarea

	function modifyProgress(delta) {
		var element = $('#progress');
		var progress = +element.val() + delta;
		element.val( isNaN(progress) ? 0 : progress < 0 ? 0 : progress > 100 ? 100 : progress );
	}

	$(document).on('click', '#plus, #minus', function(e){
	   modifyProgress(5 * (this.id === "plus" ? 1 : -1));
	});

	// función 

	$(document).on('click', '.expand', function(e){
		e.preventDefault();
		
		var item = $(this).attr('data-item');

		if( $( this ).hasClass( "plus" ) ) {
			$( this ).removeClass( "plus" );
			$( this ).addClass( "minus" );
			$('tr[data-item=' + item + ']').show( );
			$( this ).empty( );
			$( this ).append('<i class="icon-minus-sign" ></i>');
		}
		else if( $( this ).hasClass( "minus" ) ) {
			$( this ).removeClass( "minus" );	
			$( this ).addClass( "plus" );
			$('tr[data-item=' + item + ']').hide( );
			$( this ).empty( );
			$( this ).append('<i class="icon-plus-sign" ></i>');
		}

	});

	// función para actualizar el estado de las notificaciones de usuario

	$(document).on('click', '.notification', function(e){

		$.ajax({
	    
	        type 	 : "GET",
	        url  	 : "../user/updateNotification",
	        dataType : "json",
	    	success  : function(content) {
				if (content.estado == "success") 
	            	console.log("success");
	            else if (content.estado == "error") 
	            	console.log( content.estado + ' + ' );
	            if(content.redirect) {
		        	window.location = content.redirectURL;
		        }
            },
        	error: function(){ 
            	console.log('error del request ' + this.url ); 
            }
	     
	    });
    	
    	e.preventDefault();
    });

	// función para cargar recursos en los iframes para los documentos de proceso

	$(document).on('click', '#documentacion', function(e) { 

    	var head 	= $('iframe').contents().find('head');
    	
    	head.append($('<link/>', { rel: 'stylesheet', href: '/indoamericana/public/assets/css/style.css', type: 'text/css' }));
    	head.append($('<link/>', { rel: 'stylesheet', href: '/indoamericana/public/assets/css/custom.css', type: 'text/css' }));
    
        e.preventDefault();
    });

    // función para cargar el árbol de navegación de documentos

	$(document).on('click', '#documentacion', function(e) {
    	
    	var id 		= "#listdocument";
    	var process = parseInt($( this ).attr('dir'));                
    	
    	$('#listdocument').empty();
    	$('#docview').empty();

    	loadTree(process, id);
		e.preventDefault();

	});

	// función para cargar recursos en los iframes para las imagenes de proceso

	$(document).on('click', '#imagenes', function(e) { 

    	var head 	= $('iframe').contents().find('head');
    	
    	head.append($('<link/>', { rel: 'stylesheet', href: '/indoamericana/webroot/css/style.css', type: 'text/css' }));
    	head.append($('<link/>', { rel: 'stylesheet', href: '/indoamericana/webroot/css/custom.css', type: 'text/css' }));
    
        e.preventDefault();
    });

    // función para cargar el árbol de navegación de imagenes

	$(document).on('click', '#imagenes', function(e) {
    	
    	var id 		= "#listimage";
    	var process = parseInt($( this ).attr('dir'));
    	
    	$('#listimage').empty();
    	$('#docview').empty();

    	loadTree(process, id);
		e.preventDefault();

	});

    // función

	$(document).on("change", "#escalar", function(){
	  	if( $( this ).is( ":checked" ) ){
	  		$("#select_responsible").fadeIn();
	  	}
	  	else{
	  		$("#select_responsible").fadeOut();
	  		$("#responsible").val("-1");
	  	}
	});

	// función 

	$("#select_responsible").on("ready", function(){
  		$( this ).hide();
	});

    // función para controlar el css enlace activo en el menú principal

	$( document ).on( 'click', '.page-sidebar-menu li', function(e) {
   		console.log('click captured');
   		$( this ).addClass('active');
		$( this ).siblings().removeClass('active');
	});

	// función para permanecer en la misma página luego de ejecutar un formulario

    $(document).on('submit', 'form:not(#login-user, #nueva-solicitud, #register-user, #add-task, #add-comment, #update-pass, #formulario)', function (e){

    	var message = '';
    	
    	if ( $(this).attr('data-ms') == 'save' )
			message = 'Información guardada correctamente.';
        else
        	message = 'El formulario ha sido enviado.';
        
    	$.ajax({
			
			url		: $(this).attr('action'),
		    type	: $(this).attr('method'),
		    data	: $(this).serialize(),
		
		    success : function( html ) {
                        $( '.form-body' ).prepend( '<div id="log-statement" class="alert alert-success"><strong>Mensaje: ' + message + '</strong> </div>' );
                        $( '.modal-backdrop' ).fadeOut( 3200 );
                        $( '#ajax' ).hide( );
                        location.reload();
                    },
        
            error   : function(){
            			$( '.form-body' ).before( '<div id="log-statement" class="alert alert-danger"><strong>Mensaje: </strong> Algo falló enviando el formulario.</div>' );
                        //$( '#ajax' ).hide( );
                        //$( '.alert-danger' ).fadeOut( 3200 );
                    }
		});
	
		e.preventDefault();

	});

	// función para cargar los integrantes de un procedimiento

	$(document).on("change", "#process", function() {
    	
    	$.ajax({
	        
	        type 	 : "GET",
	        url  	 : "../intranet/getMembers/" + $( this ).val(),
	        dataType : "json",
	    	    
	        success  : function(content) {
	            
	            if (content.status == "success") 
	            {
	            	$("#members").find('option').remove();
	            	$.each(content.message, function(key, val) {
	            		$("#members").append(
	            			'<option value="' + val.id + '">' + val.name + ' '+ val.last_name + '</option>'
	            		);
	            	});
	            } 
	            else 
	                $("#error").html('<p>' + content.message + '</p>');
            }
	    });
    
	    return false;

	});

	// función para cargar el árbol de navegación de documentos

	function loadTree( raiz, list ) { 
		
		var down_ico  = '<i class="icon-download"></i>';

		$.ajax({
	        
	        type     : "GET",
	        url  	 : "/indoamericana/intranet/getFiles/" + raiz,
	        dataType : "json",
	            
	        success  : function(content) {
	            	
	            if (content.status == "success") 
	            {
	            	file_id = '#' + raiz;
	            	listed = $('<ul/>').appendTo( list );

	            	if ( content.data.length > 0)
	            	{
		            	$.each(content.data, function(key, val) {
	            			
	            			if(val.type == 1) 																		// Se valida el tipo de documento
	            			{ 
	        					my_icon  = '<i class="icon-folder-close"></i>'; 
	        					my_class = 'folder';
	        				}
		            		else 
	            			{ 
	            				my_icon  = '<i class="icon-file-text"></i>';
	            				my_class = 'file';
	            			}	

	            			//if(  val.extension != '.jpg' && val.extension != '.pdf' && val.extension != '.dir')
	            				//listed.append('<li dir ="' + ulr_down + '" id = "' + val.id + '" class = "' + my_class + '">' + my_icon + '<a href = "' + val.ubication + '">' + val.name.capitalize() + down_ico + '</a></li>');	
	            			//else
	            				listed.append('<li dir ="' + val.ubication + '" id = "' + val.id + '" class = "' + my_class + '">' + my_icon + val.name.capitalize() + '</li>');
	            		});
	            	}
	            	else
	            		listed.append('<li> (Directorio vacío) </li>');	            		
	            }
	            else
	            {
	            	console.log( content );
	            } 
	            $( list ).addClass('clicked');
	        },

	        error: function (xhr, ajaxOptions, thrownError) {
		        console.log(xhr.status);
		        console.log(thrownError);
	        }
	    });
	    
	    return false;

	}	

	// función para cargar el contenido de una carpeta en el árbol de navegación
    
    $(document).on('click', '.folder', function(event) {
    	
    	var f_open    = '<i class="icon-folder-open"></i>';
    	var f_close   = '<i class="icon-folder-close"></i>';
    	var fi_icon   = '<i class="icon-file-text"></i>';
    	var root      = parseInt($( this ).attr('id'));
    	var clase;

    	if(root)
    	{
    		id = "#" + root;
    		$( this ).children('i').remove();
    		$( this ).prepend(f_open);
    		
    		if( !($( this ).hasClass('clicked')) )
    			loadTree(root, id);	
		}

		$( this ).siblings().children().remove('ul, i');
    	
        $.each( $(this).siblings(), function() {
    		
    		clase =  $( this ).hasClass('file');
    		
    		if ( clase )
    			$( this ).prepend(fi_icon);    	
    		else 
    			$( this ).prepend(f_close);    
    		
		});

        $( this ).siblings().removeClass('clicked');	
    	$( this ).attr('id', root);
    	
	});

	//función para cargar un documento en el navegador de documentos

	$(document).on('click', '.file', function(e) { 

		root   = $( this ).attr('id');
		my_url = $( this ).attr('dir');

		if(root)
		{
			$('#docview embed').remove();
			$('#docview').append( '<embed id="doc-container" src="' + my_url +'">');
			
			$.ajax({
				type     : "GET", 
				url  	 : "/indoamericana/intranet/countClicksFiles/" + root,
				dataType : "json",

				success: 	function(content){
					if( content.message.response != 'success' )
						console.log('Se produjo un error en el proceso');				
				},

				error: function(){
					console.log('Se produjo un error en la petición');	
				}
			});
		}
		
	});

	// función para poner en mayúscula la primera letra de un palabra

	String.prototype.capitalize = function() {
    	return this.charAt(0).toUpperCase() + this.slice(1);
	}

	// función cargar en el formulario de edición de perfil el usuario seleccionado

    $(document).on('click', 'a[data-target=#ajax]', function(e){
	    var target = $(this).attr("href");
	    $("#ajax").load(target, function() {
	         $("#ajax").modal("show"); 
	    });
	    e.preventDefault();
	});

	$(document).on('click', 'a[data-target=#form_edit]', function(e){
	    var target = $(this).attr("href");
	    $("#form_edit").load(target, function() { 
	         $("#form_edit").modal("show"); 
	    });
	    e.preventDefault();
	});

	// función

	$(document).on("click", ".refresh",function(e){
		//console.log("reload");
		location.reload();
	});

	// función

	$(document).on('change', '#sel-category', function(){
		var id  = $( this ).val();
		//var dir = '../updateCategoryForm/';
		var obj = $('a[data-info=update]');
		var link = obj.attr('href');
		link = link.slice(0,-1);
		obj.attr('href', link + id); 
	}); 

});