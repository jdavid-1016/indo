jQuery(function($){
    $('.fecha_nuevoalum').mask('0000-00-00');
});

$( ".fecha_nuevoalum" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '-50y' });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Funciones para traer datos en el formulario de editar curso//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   function agregarCursoe(codigo, nombre, id){
       
       $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarcursose",//la url adonde se va a mandar la cadena a buscar
                data: {codigo: codigo},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    $("#displaycodigop").hide();
                    $("#displaynombrep").hide();
                    $('#coding').val(codigo);
                    $('#name_curso').val(html[0]["name_curso"]);
                    $('#open').val(html[0]["open"]);
                    $('#status').val(html[0]["status"]);
                    $('#start_date').val(html[0]["start_date"]);
                    $('#end_date').val(html[0]["end_date"]);
                    $('#courses_categories_id').val(html[0]["courses_categories_id"]);
                    $('#instructors_id').val(html[0]["instructors_id"]);
                }
        });
      
   }

// Focus = Changes the background color of input to yellow
function focusFunctioname() {
    $("#displaynombrep").hide('slow');
}

function blurFunctioname() {
    $("#displaynombrep").show('slow');
}

function focusFunctionCode() {
    $("#displaycodigop").hide('slow');
}

function blurFunctionCode() {
    $("#displaycodigosp").show('slow');
}

$("#name_curso").keyup(function() //se crea la funcion keyup
{
    var texto = $("#name_curso").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displaynombrep").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarnombredit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displaynombrep").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

$("#coding").keyup(function() //se crea la funcion keyup
{
    var texto = $("#coding").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displaycodigop").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarcodigoedit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displaycodigop").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Funciones para traer datos en el formulario de editar curso//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//funcion para guardar un nuevo curso
function guardarCourse() {
    var validacion = 0;
    var coding = $('#coding').val();
    var name_curso = $('#name_curso').val();
    var open = $('#open').val();
    var status = $('#status').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var courses_categories_id = $('#courses_categories_id').val();
    var cupo = $('#cupo').val();
    var time_intensity = $('#time_intensity').val();
    var schedule = $('#schedule').val();
    var ocsa = $('#ocsa').val();
    
    if(coding !="" && name_curso !="" && start_date !="" && end_date !="" && cupo !="" && time_intensity !="" && schedule !="" && ocsa !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarcurso",
                data: {coding:coding, name_curso:name_curso, open:open, status:status, start_date:start_date, 
                    end_date:end_date, courses_categories_id:courses_categories_id, cupo:cupo, time_intensity:time_intensity, schedule:schedule, ocsa:ocsa },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    $('#coding').val('');
                    $('#name_curso').val('');
                    $('#open').val('');
                    $('#status').val('');
                    $('#start_date').val('');
                    $('#end_date').val('');
                    $('#cupo').val('');
                    $('#courses_categories_id').val('');
                    $('#time_intensity').val('');
                    $('#schedule').val('');
                    $('#ocsa').val('');

                toastr.success('El nuevo curso se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar el Curso', 'Error');
            }
        }
}

//funcion para guardar un nuevo curso
function duplicarCurso(id) {
    var validacion = 0;
    var coding = $('#coding').val();
    var name_curso = $('#name_curso').val();
    var open = $('#open').val();
    var status = '1';
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var courses_categories_id = $('#courses_categories_id').val();
    
    if(coding !="" && name_curso !="" && start_date !="" && end_date !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "duplicarcurso",
                data: {id:id, coding:coding, name_curso:name_curso, open:open, status:status, start_date:start_date, 
                    end_date:end_date, courses_categories_id:courses_categories_id },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('El nuevo curso se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar el Curso', 'Error');
            }
        }
}

//funcion para guardar una nueva categoria
function guardarCategory() {
    var validacion = 0;
    var category = $('#category').val();
    
    if(category !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarcategory",
                data: {category:category },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    $('#category').val('');

                toastr.success('La nueva categoria se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar la categoria', 'Error');
            }
        }
}

function cargar_detalle_empresa(id){

    var html = $.ajax({
        type: "GET",
        url: "detalleempresa",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax3').html(html);

}
function cargar_seguimientos_empresa(id){

    var html = $.ajax({
        type: "GET",
        url: "seguimientosempresa",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax1').html(html);

}

function editar_empresa(id){
    var nombre = $('#nombre_edit').val();
    var contacto = $('#contacto_edit').val();
    var direccion = $('#direccion_edit').val();
    var telefono = $('#telefono_edit').val();
    var email = $('#email_edit').val();
    var observaciones = $('#observaciones_edit').val();
    var id = id;

    if(nombre == ""){
        toastr.error('El campo nombre esta vacio', 'Error');
        return;
    }
    if(contacto == ""){
        toastr.error('El campo contacto esta vacio', 'Error');
        return;
    }
    if(direccion == ""){
        toastr.error('El campo direccion esta vacio', 'Error');
        return;
    }
    if(telefono == ""){
        toastr.error('El campo telefono esta vacio', 'Error');
        return;
    }
    if(email == ""){
        toastr.error('El campo email esta vacio', 'Error');
        return;
    }
    if(observaciones == ""){
        toastr.error('El campo observaciones esta vacio', 'Error');
        return;
    }

    var html = $.ajax({
        type: "GET",
        url: "ingresarempresa",
        data: {
            id:id,
            nombre:nombre,
            contacto:contacto,
            direccion:direccion,
            telefono:telefono,
            email:email,
            observaciones:observaciones
        },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La Agenda se ha Editado correctamente', 'Edición de Agenda');
            $('#close_modal_detalle').click();
            $('#nombre_edit').val("");
            $('#contacto_edit').val("");
            $('#direccion_edit').val("");
            $('#telefono_edit').val("");
            $('#email_edit').val("");
            $('#observaciones_edit').val("");
            recargarTabla();
            
        } else {
            toastr.error('No ha sido posible editar la Agenda', 'Error');
        }

}