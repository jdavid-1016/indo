toastr.options = {
    "closeButton": true,
    "debug": false,
    "positionClass": "toast-bottom-right",
    "onclick": null,
    "showDuration": "5000",
    "hideDuration": "5000",
    "timeOut": "5000",
    "extendedTimeOut": "5000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}


// var host = "ws://192.168.0.152/laravelcei/public/js/server.php";
// var socket;


//funcion para actualizar la infromacion del usuario
function actualizar() {

    var celular = $("#celular").val();
    var telefono = $("#telefono").val();
    var email = $("#email").val();
    var direccion = $("#direccion").val();

    var html = $.ajax({
        type: "GET",
        url: "actualizarperfil",
        data: {celular: celular, telefono: telefono, email: email, direccion: direccion},
        async: false
    }).responseText;

    if (html == "1")
    {
        toastr.success('Sus datos se han actualizado correctamente', 'Actualizacion');
    }
    else
    {
        toastr.error('No ha sido posible actualizar sus datos', 'Error');
    }
}

function tiene_numeros(texto) {
    var numeros = "0123456789";
    for (i = 0; i < texto.length; i++) {
        if (numeros.indexOf(texto.charAt(i), 0) != -1) {
            return true;
        }
    }
    return false;
}

function tiene_letras(texto) {
    var letras = "abcdefghyjklmnñopqrstuvwxyz";
    texto = texto.toLowerCase();
    for (i = 0; i < texto.length; i++) {
        if (letras.indexOf(texto.charAt(i), 0) != -1) {
            return true;
        }
    }
    return false;
}
//funcion para cambiar la contraseña del usuario
function cambiarpass() {
    var nueva = $("#nueva").val();
    var renueva = $("#renueva").val();
    var actual = $("#actual").val();
    if (nueva == renueva) {
        if (nueva.length < 6) {
            toastr.warning('La nueva contraseña debe tener al menos 6 caracteres', 'Error');
            return;
        } else {
            if (!tiene_numeros(nueva) || !tiene_letras(nueva)) {
                toastr.warning('La nueva contraseña debe tener numeros y letras', 'Error');
                return;
            }
        }
        var html = $.ajax({
            type: "GET",
            url: "cambiarpass",
            data: {nueva: nueva, renueva: renueva, actual: actual},
            async: false
        }).responseText;

        if (html == "1") {
            $("#nueva").val("");
            $("#renueva").val("");
            $("#actual").val("");
            toastr.success('La contraseña ha sido actualizada correctamente', 'Actualizacion');

        } else if (html == "2") {
            toastr.warning('La contraseña actual es incorrecta, verifiquela e intente de nuevo', 'Error');

        } else {
            toastr.error('No ha sido posible actualizar su contraseña', 'Error');
        }
    } else {
        toastr.warning('las contraseñas no coinciden, intente nuevamente', 'Error');
    }
}
//funcion para actualizar las redes sociales del usuario
function redes() {

    var linked = $("#linked").val();
    var google = $("#google").val();
    var facebook = $("#facebook").val();
    var twitter = $("#twitter").val();
    var skype = $("#skype").val();

    var html = $.ajax({
        type: "GET",
        url: "redes",
        data: {linked: linked, google: google, facebook: facebook, twitter: twitter, skype: skype},
        async: false
    }).responseText;

    if (html == "1") {
        toastr.success('Sus redes sociales se han actualizado correctamente', 'Actualizacion');

    } else {
        toastr.error('No ha sido posible actualizar sus redes sociales', 'Error');
    }
}
//funcion para asignar soportes a un usuario
function asing_support() {

    var responsible = $("#responsible").val();
    var status = $("#status").val();
    var priority = $("#priority").val();
    var category = $("#category").val();
    var observation = $("#observation").val();
    var support_id = $("#support_id").val();



    $.ajax({
        type: "GET",
        url: "asing_support",
        data: {responsible: responsible, status: status, priority: priority, category: category, observation: observation, support_id: support_id}
    })
            .done(function (data) {
                if (data == "2") {
                    toastr.error('No ha sido posible asignar el soporte', 'Error');
                } else {

                    if (status == 3) {
                        var type_event = ["refuse_support", data["name"], data["last_name"], data["img_min"], support_id];
                        send(type_event);
                        toastr.success('Se actualizo el soporte correctamente', 'Actualizacion');
                    } else {
                        var type_event = ["asignsupport", data["name"], data["last_name"], data["img_min"], responsible];
                        send(type_event);
                        toastr.success('Se asigno el soporte correctamente', 'Actualizacion');
                    }
                    $("#close_modal").click();
                }
            });


}

function attend_support() {

    if ($("#closed").is(':checked')) {
        var status = 2;
    } else {
        var status = 4;
    }

    if ($("#escalar").is(':checked')) {
        var status = 5;
        var scale = 1;
        var user_asign = $("#responsible").val();
    } else {
        var scale = 0;
        var user_asign = "";
    }

    var descarga = $("#descarga").val();
    var subida = $("#subida").val();


    var observation = $("#observation").val();
    var support_id = $("#support_id").val();



    var html = $.ajax({
        type: "GET",
        url: "attend_support",
        data: {observation: observation, support_id: support_id, status: status, scale: scale, user_asign: user_asign, descarga: descarga, subida: subida },
        async: false
    }).done(function (datos) {
        if (datos == "2") {
            toastr.error('No se a podido actualizar el soporte', 'Error');
        } else {
            toastr.success('Se actualizo el soporte correctamente', 'Actualizacion');
            $("#close_modal").click();

            var cont = $("#5").html();
            cont = parseInt(cont);
            cont = cont - 1;
            $("#5").html(cont);
            if (cont == 0) {
                $("#5").css("display", "none");
            }

            if (status == 5) {
                var type_event = ["scaled_support", datos["name"], datos["last_name"], datos["img_min"], support_id, user_asign];
                send(type_event);
            } else {
                var type_event = ["acept_support", datos["name"], datos["last_name"], datos["img_min"], support_id, status];
                send(type_event);

            }
            recargarTablaresp();
        }
    });

}

function recargarTablaresp() {

    var html = $.ajax({
        type: "GET",
        url: "respond_supports",
        cache: false,
        data: {nombre: "hola"},
        async: false
    }).responseText;

    $('#table_respond_supports').html(html);

}
function cancel_support(id_support) {

    var html = $.ajax({
        type: "GET",
        url: "cancel_support",
        data: {support_id: id_support},
        async: false
    }).done(function (datos) {
        if (datos == "2") {
            toastr.error('No ha sido posible actualizar el soporte', 'Error');
        } else {
            toastr.success('Se actualizo el soporte correctamente', 'Actualizacion');
            var type_event = ["cancel_support", datos['name'], datos['last_name'], datos['img_min'], id_support];
            send(type_event);
            $("#" + id_support).css("display", "none");
        }
    });


}


// function load_more_notifications(){

//     var html = $.ajax({
//         type: "GET",
//         url: "load_more_notifications",
//         data: { more: "1"},
//         async: false
//     }).done(function( datos ) {

//     });


// }


function ratingSupport() {


    var question_1 = $('input:radio[name=question_1]:checked').val();
    var question_2 = $('input:radio[name=question_2]:checked').val();
    var question_3 = $('input:radio[name=question_3]:checked').val();
    var support_id = $("#support_id").val();
    var comment = $("#comment").val();



    opciones_1 = document.getElementsByName("question_1");
    opciones_2 = document.getElementsByName("question_2");
    opciones_3 = document.getElementsByName("question_3");

    var seleccionado1 = false;
    for (var i = 0; i < opciones_1.length; i++) {
        if (opciones_1[i].checked) {
            seleccionado1 = true;
            break;
        }
    }
    var seleccionado2 = false;
    for (var i = 0; i < opciones_2.length; i++) {
        if (opciones_2[i].checked) {
            seleccionado2 = true;
            break;
        }
    }
    var seleccionado3 = false;
    for (var i = 0; i < opciones_3.length; i++) {
        if (opciones_3[i].checked) {
            seleccionado3 = true;
            break;
        }
    }
    var max = 3;

    if (!seleccionado1 || !seleccionado2 || !seleccionado3) {
        toastr.error('Por favor califique el soporte', 'Error');
        return;
    } else if (comment == "") {
        toastr.error('El campo Observaciones está vacio', 'Error');
        return;
    } else if (comment.length < max ){
        toastr.error('Observacion no valida', 'Error');
        return;

    }else{

        
        var html = $.ajax({
            type: "GET",
            url: "submit_rating_support",
            data: {question_1: question_1, question_2: question_2, question_3: question_3, support_id: support_id, comment: comment},
            async: false
        }).responseText;

        if (html == "1") {
            toastr.success('La calificación fue enviada correctamente', 'Actualizacion');
            $("#close_modal").click();
            $("#cal" + support_id).html('<td class="td_center"><i class="icon-check"></i></td>');
        }
        else {

            toastr.error('No ha sido posible calificar el soporte', 'Error');
        }
    }
}

$("#resena").mouseenter(function (evento) {
    $('#botoneditar').toggle('slow');
});

$("#resena").mouseleave(function (e) {
    $('#botoneditar').toggle('slow');
});


function funcion() {

    $('.escalar').each(function () {
        var checkbox = $(this);
        if (checkbox.is(':checked') == true) {
            $('#select_responsible').toggle('slow');
        } else {
            $('#select_responsible').toggle('slow');
        }
    });

}



function guardarResena(id) {
    var editor = CKEDITOR.instances['editor'].getData();
    var html = $.ajax({
        type: "GET",
        url: "../resena",
        data: {id: id, editor: editor},
        async: false
    }).responseText;

    if (html == "1") {
        $("#descriptionresena").html(editor);
        $(".close").click();
        toastr.success('Reseña actualizada correctamente', 'Actualizacion');

    } else {
        toastr.error('No ha sido posible actualizar la reseña', 'Error');
    }
}

function crearDir(id, proceso) {
    var nombre = $("#carpeta").val();

    $.ajax({
        type: "GET",
        url: "../crearDir",
        data: {id: id, nombre: nombre, proceso: proceso},
    })
            .done(function (data) {
                if (data == "2") {
                    toastr.error('No ha sido posible crear la carpeta', 'Error');
                } else {

                    $(".close").click();
                    toastr.success('carpeta creada correctamente', 'Nueva carpeta');
                    $('#carpetas').append(new Option(data["name"], data["id"], true, true));
                }
            });

}

function desactive_notification(valorCaja1) {
    var parametros = {
        "valorCaja1": valorCaja1
    };
    var ruta = "";
    if ($("#fancyurl").val() == "1") {
        ruta = "../";
    }
    $.ajax({
        data: parametros,
        url: ruta + 'desactive_notification',
        type: 'get',
        success: function (response) {
            if (response == "1") {
                var dir = $('#' + valorCaja1).attr('href');
                window.location = ruta + dir;
            } else {
                alert("error");
            }
            //$("#ajax").html(response);
        }
    });
}

function edit_data_user() {

    var profile = $("#profile").val();
    var status = $("#status").val();
    var priority = $("#priority").val();
    var category = $("#category").val();
    var observation = $("#observation").val();
    var support_id = $("#support_id").val();


    $.ajax({
        type: "GET",
        url: "asing_support",
        data: {responsible: responsible, status: status, priority: priority, category: category, observation: observation, support_id: support_id}
    })
            .done(function (data) {
                if (data == "2") {
                    toastr.error('No ha sido posible asignar el soporte', 'Error');
                } else {
                    toastr.success('Se asigno el soporte correctamente', 'Actualizacion');
                    $("#close_modal").click();
                    var type_event = ["asignsupport", data["name"], data["last_name"], data["img_min"], responsible];
                    send(type_event);

                }
            });
}


function create_new_user() {
    var parametros = {
        "name": $("#name").val(),
        "name2": $("#name2").val(),
        "last_name": $("#last_name").val(),
        "last_name2": $("#last_name2").val(),
        "email_institutional": $("#email_institutional").val(),
        "email": $("#email").val(),
        "cell_phone": $("#cell_phone").val(),
        "address": $("#address").val(),
        "phone": $("#phone").val(),
        // "img"             : $("#img").val(),
        // "img_profile"     : $("#img_profile").val(),
        // "img_min"         : $("#img_min").val(),
        "type_document": $("#type_document").val(),
        "document": $("#document").val(),
        "city_id": $("#city_id").val(),
        "process_id": $("#create_user_process").val(),
        "extention": $("#extention").val(),
        "profile_id": $("#create_user_profile").val()

    };

    $.ajax({
        data: parametros,
        url: 'create_user_new',
        cache: false,
        type: 'get',
        success: function (response) {
            if (response == "1") {
                toastr.success('Se creo el usuario correctamente', 'Nuevo Usuario');
            }
            if (response == "3") {
                toastr.error('No ha sido posible crear el usuario', 'Error');
            }
        }
    });
}

function exportar(id) {
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();

    switch (id) {
        case 1:

            var dir = "exportar?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;

        case 2:

            var dir = "exportarfiles?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;

        case 3:

            var dir = "exportarprocess?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
    }
    window.open(dir);
}

function exportar_help_desk() {
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();
    var dir = "exportar_help_desk?inicio=" + fecha_inicio + "&fin=" + fecha_fin;
    window.open(dir);
}

function likeImage(id) {
    var cont = $("#img-" + id).find("i").html();
    cont = parseInt(cont);
    cont = cont + 1;

    $.ajax({
        type: "GET",
        url: "../likeImage",
        data: {id: id, cont: cont, }
    })
            .done(function (data) {
                if (data == "1") {
                    $("#img-" + id).find("i").removeClass("icon-thumbs-up-alt");
                    $("#img-" + id).find("i").addClass("icon-thumbs-up");
                    $("#img-" + id).find("i").html(" " + cont);
                    $("#img-" + id).addClass("disabled");

                } else {
                    toastr.error('No ha sido posible guardar su Like', 'Error');
                }
            });
}



$('#name_new_profile').keyup(function () {
    var max = 8;

    if ($("#name_new_profile").val().length > max) {
        var name_new_profile = $("#name_new_profile").val();

        var exist = $.ajax({
            type: "GET",
            url: "exist_new_profile",
            data: "name_new_profile=" + name_new_profile,
            async: false


        }).responseText;

        if (exist == 2) {
            $("#name_new_profile").css("border-color", "#468847");
            $("#create_new_profile").removeClass("disabled");
        } else {
            $("#name_new_profile").css("border-color", "#b94a48");
            $("#create_new_profile").addClass("disabled");
        }
    }
});


$('#edit_processes').change(function (event) {
    var process = $("#edit_processes").find(':selected').val();

    $("#profiles_cahge").load('selected_profile_process?process=' + process);

    //$("#profile_css").css("display", "block");
});

$('#edit_process').change(function (event) {
    var process = $("#edit_process").find(':selected').val();

    $("#profile").load('selected_profile_process?process=' + process);

    //$("#profile_css").css("display", "block");
});

$('#profiles_cahge').change(function (event) {
    var profile = $("#profiles_cahge").find(':selected').val();

    $("#permisos_user").load('selected_submenu_profile?profile=' + profile);

});

$('#add_processes').change(function (event) {
    var process = $("#add_processes").find(':selected').val();

    $("#profiles_change_add").load('selected_profile_process?process=' + process);

});

$('#profiles_change_add').change(function (event) {
    var profile = $("#profiles_change_add").find(':selected').val();

    $("#permisos_user_add").load('selected_submenu_profile?profile_add=' + profile);

});

function buscar_proceso() {
    var profile = $("#create_user_process").find(':selected').val();

    $("#create_user_profile").load('selected_profile_process?process=' + profile);

}

window.addEventListener('focus', function () {
    document.title = 'Indoamericana';
});

//ingresamos en base de datos el login del usuario
$.ajax({
    type: "GET",
    url: "/indo/public/actIngresos",
    data: {cont: "1"}
})
        .done(function (data) {
            //alert(data);
        });

//funcion para guardar una nueva FAQ
function guardarfaq() {
    var categoria = $("#categoria").find(':selected').val();
    var pregunta = $("#pregunta").val();
    var descripcion = CKEDITOR.instances['descripcion'].getData();

    var html = $.ajax({
        type: "post",
        url: "guardarfaq",
        data: {categoria: categoria, pregunta: pregunta, descripcion: descripcion},
        async: false
    }).responseText;

    if (parseInt(html) == 1) {
        document.getElementById("form").reset();
        CKEDITOR.instances['descripcion'].setData("");
        toastr.success('La nueva FAQ se ha guardado correctamente', 'Nueva FAQ');

    } else {
        toastr.error('No ha sido posible guardar la nueva FAQ', 'Error');
    }
}

//funcion para guardar un nuevo Proveedor
function guardarprov() {
    var validacion = 0;
    var nit = $("#nit").val();
    var branch_office = $("#branch_office").val();
    var branch_office_usa = $("#branch_office_usa").val();    
    var provider = $("#provider").val();
    var contact = $("#contact").val();
    var address = $("#address").val();
    var phone1 = $("#phone1").val();
    var phone2 = $("#phone2").val();
    var phone3 = $("#phone3").val();
    var phone4 = $("#phone4").val();
    var fax = $("#fax").val();
    var air_section = $("#air_section").val();
    var email = $("#email").val();
    var genders_id = $("#genders_id").val();
    var birth = $("#birth").val();
    var digit_of_verification = $("#digit_of_verification").val();
    var tax_ids_id = $("#tax_ids_id").val();
    var country_code = $("#country_code").val();
    var city_code = $("#city_code").val();
    var tax_classifications_id = $("#tax_classifications_id").val();
    var economic_activity = $("#economic_activity").val();
    var legal_person_types_id = $("#legal_person_types_id").val();
    var observations = $("#observations").val();
    var withholding_agent = $("#withholding_agent").val();
    var beneficiary_reteiva = $("#beneficiary_reteiva").val();
    var agent_reteica = $("#agent_reteica").val();
    var declarant = $("#declarant").val();
    var uses_retention = $("#uses_retention").val();
    var status = $("#status").val();
    var uses_social_reason = $("#uses_social_reason").val();
    var ean_code = $("#ean_code").val();
    var first_name = $("#first_name").val();
    var middle_name = $("#middle_name").val();
    var last_name = $("#last_name").val();
    var last_name2 = $("#last_name2").val();
    var method_of_payment = $("#method_of_payment").val();
    var rating = $("#rating").val();
    var discount_percentage = $("#discount_percentage").val();
    var payment_period = $("#payment_period").val();
    var optimistic_days = $("#optimistic_days").val();
    var pessimistic_days = $("#pessimistic_days").val();
    var foreign_identification = $("#foreign_identification").val();
    var tax_identification_code = $("#tax_identification_code").val();
    var company_type = $("#company_type").val();
    var authorizes_lenders_report = $("#authorizes_lenders_report").val();
    var differential_rate_reteiva_shopping = $("#differential_rate_reteiva_shopping").val();
    var differential_rate_reteiva_sales = $("#differential_rate_reteiva_sales").val();
    var payments_methods_id = $("#payments_methods_id").val();
    var bank = $("#bank").val();
    var account_type = $("#account_type").val();
    var account_number = $("#account_number").val();
    
    
    if(nit !="" && provider !="" && contact !="" && phone1 !="" && tax_ids_id !="1" && tax_classifications_id !="1" && payments_methods_id !="1"){
                if(payments_methods_id==3){
                    if(bank=="0" || account_type=="0" || account_number=="0"){
                       toastr.error('Debe llenar los campos para *banco, *tipo de cuenta y *numero de cuenta', 'Error');
                    }else{
                        validacion = 1;
                    }
                }else{
                  validacion = 1;
                }
                
            }else{
                toastr.error('Todos los campos con * son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarprov",
                data: {nit:nit, branch_office:branch_office, branch_office_usa:branch_office_usa, provider:provider, contact:contact, address:address, phone1:phone1, 
                       phone2:phone2, phone3:phone3, phone4:phone4, fax:fax, air_section:air_section, email:email, genders_id:genders_id, birth:birth, 
                       digit_of_verification:digit_of_verification, tax_ids_id:tax_ids_id, country_code:country_code, city_code:city_code, tax_classifications_id:tax_classifications_id, 
                       economic_activity:economic_activity, legal_person_types_id:legal_person_types_id, observations:observations, withholding_agent:withholding_agent, 
                       beneficiary_reteiva:beneficiary_reteiva, agent_reteica:agent_reteica, declarant:declarant, uses_retention:uses_retention, status:status, 
                       uses_social_reason:uses_social_reason, ean_code:ean_code, first_name:first_name, middle_name:middle_name, last_name:last_name, last_name2:last_name2, 
                       method_of_payment:method_of_payment, rating:rating, discount_percentage:discount_percentage, payment_period:payment_period, optimistic_days:optimistic_days, 
                       pessimistic_days:pessimistic_days, foreign_identification:foreign_identification, tax_identification_code:tax_identification_code, company_type:company_type, 
                       authorizes_lenders_report:authorizes_lenders_report, differential_rate_reteiva_shopping:differential_rate_reteiva_shopping, differential_rate_reteiva_sales:differential_rate_reteiva_sales, 
                       payments_methods_id:payments_methods_id, bank:bank, account_type:account_type, account_number:account_number },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                document.getElementById("form").reset();
                toastr.success('El nuevo Proveedor se ha guardado correctamente', 'Nuevo Proveedor');

            } else {
                toastr.error('No ha sido posible guardar el Proveedor', 'Error');
            }
        }
}




function create_new_payment() {
    var parametros = {
        "concept":      $("#concept").val(),
        "value":        $("#value").val(),
        "description":  $("#description").val(),
        "file":         $("#file").val(),
    };

    $.ajax({
        data: parametros,
        url: 'createpayment',
        cache: false,
        type: 'get',
        success: function (response) {
            if (response == "1") {
                toastr.success('Se Envió la solicitud de pago correctamente', 'Nuevo pago');
                $("#description").val("");
                $("#value").val("");
                $("#concept").val("");
                $("#file").val("");
            }
            if (response == "3") {
                toastr.error('No ha sido posible crear la solicitud', 'Error');
            }
        }
    });
}

function exportarShoppings(id) {
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();

    switch (id) {
        case 1:

            var dir = "exportarstatuses?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;

        case 2:

            var dir = "exportarfiles?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;

        case 3:

            var dir = "exportarprocess?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 4:

            var dir = "exportarprocess?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 5:

            var dir = "exportarprocess?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 6:

            var dir = "exportarprocess?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 7:

            var dir = "exportarprocess?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
    }
    window.open(dir);
}

function exportarMensual() {
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();

    var dir = "exportarmensual?inicio=" + fecha_inicio + "&fin=" + fecha_fin;
    window.open(dir);
}

function exportarAspirantes(id) {
    var fecha_inicio = $('input:text[name=daterangepicker_start]').val();
    var fecha_fin = $('input:text[name=daterangepicker_end]').val();    

    switch (id) {
        case 1:

            var dir = "exportarseguimientos?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;

        case 2:

            var dir = "exportarprogramas?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;

        case 3:

            var dir = "exportarcampanas?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 4:

            var dir = "exportarfuentes?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 5:

            var dir = "exportarestados?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 6:

            var dir = "exportarjornadas?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 7:

            var dir = "exportargeneros?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 8:

            var dir = "exportarciudad?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
            
        case 9:

            var dir = "exportarlocalidad?inicio=" + fecha_inicio + "&fin=" + fecha_fin;

            break;
    }
    window.open(dir);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Funciones para traer datos en el formulario de editar proveedor//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   function agregarProvp(nit, nombre, id){
       
       $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarprovp",//la url adonde se va a mandar la cadena a buscar
                data: {nit: nit},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    $("#displayprovp").hide();
                    $("#displaynitp").hide();
                    $('#nit').val(nit);
                    $('#branch_office').val(html[0]["branch_office"]);
                    $('#branch_office_usa').val(html[0]["branch_office_usa"]);
                    $('#provider').val(html[0]["provider"]);
                    $('#contact').val(html[0]["contact"]);
                    $('#address').val(html[0]["address"]);
                    $('#phone1').val(html[0]["phone1"]);
                    $('#phone2').val(html[0]["phone2"]);
                    $('#phone3').val(html[0]["phone3"]);
                    $('#phone4').val(html[0]["phone4"]);
                    $('#fax').val(html[0]["fax"]);
                    $('#air_section').val(html[0]["air_section"]);
                    $('#email').val(html[0]["email"]);
                    $('#genders_id').val(html[0]["genders_id"]);
                    $('#birth').val(html[0]["birth"]);
                    $('#digit_of_verification').val(html[0]["digit_of_verification"]);
                    $('#tax_ids_id').val(html[0]["tax_ids_id"]);
                    $('#country_code').val(html[0]["country_code"]);
                    $('#city_code').val(html[0]["city_code"]);
                    $('#tax_classifications_id').val(html[0]["tax_classifications_id"]);
                    $('#economic_activity').val(html[0]["economic_activity"]);
                    $('#legal_person_types_id').val(html[0]["legal_person_types_id"]);
                    $('#observations').val(html[0]["observations"]);
                    $('#withholding_agent').val(html[0]["withholding_agent"]);
                    $('#beneficiary_reteiva').val(html[0]["beneficiary_reteiva"]);
                    $('#agent_reteica').val(html[0]["agent_reteica"]);
                    $('#declarant').val(html[0]["declarant"]);
                    $('#uses_retention').val(html[0]["uses_retention"]);
                    $('#status').val(html[0]["status"]);
                    $('#uses_social_reason').val(html[0]["uses_social_reason"]);
                    $('#ean_code').val(html[0]["ean_code"]);
                    $('#first_name').val(html[0]["first_name"]);
                    $('#middle_name').val(html[0]["middle_name"]);
                    $('#last_name').val(html[0]["last_name"]);
                    $('#last_name2').val(html[0]["last_name2"]);
                    $('#method_of_payment').val(html[0]["method_of_payment"]);
                    $('#rating').val(html[0]["rating"]);
                    $('#discount_percentage').val(html[0]["discount_percentage"]);
                    $('#payment_period').val(html[0]["payment_period"]);
                    $('#optimistic_days').val(html[0]["optimistic_days"]);
                    $('#pessimistic_days').val(html[0]["pessimistic_days"]);
                    $('#foreign_identification').val(html[0]["foreign_identification"]);
                    $('#tax_identification_code').val(html[0]["tax_identification_code"]);
                    $('#company_type').val(html[0]["company_type"]);
                    $('#authorizes_lenders_report').val(html[0]["authorizes_lenders_report"]);
                    $('#differential_rate_reteiva_shopping').val(html[0]["differential_rate_reteiva_shopping"]);
                    $('#differential_rate_reteiva_sales').val(html[0]["differential_rate_reteiva_sales"]);
                    $('#payments_methods_id').val(html[0]["payments_methods_id"]);
                    $('#bank').val(html[0]["bank"]);
                    $('#account_type').val(html[0]["account_type"]);                    
                    $('#account_number').val(html[0]["account_number"]);
                }
        });
      
   }

// Focus = Changes the background color of input to yellow
function focusFunctionp() {
    $("#displayprovp").hide('slow');
}

function blurFunctionp() {
    $("#displayprovp").show('slow');
}

function focusFunctionNitp() {
    $("#displaynitp").hide('slow');
}

function blurFunctionNitp() {
    $("#displaynitp").show('slow');
}

$("#provider").keyup(function() //se crea la funcion keyup
{
    var texto = $("#provider").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displayprovp").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarprovedit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displayprovp").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

$("#nit").keyup(function() //se crea la funcioin keyup
{
    var texto = $("#nit").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displaynitp").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarnitedit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displaynitp").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Funciones para traer datos en el formulario de editar proveedor//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////