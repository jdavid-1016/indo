jQuery(function($){
    $('.fecha_required').mask('0000-00-00');
    $('.number').mask('9999999999999');
    $('.money').mask('000,000,000,000,000', {reverse: true});
});


$( ".fecha_required" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });

var items = new Array();
var posicion = 0;
var t = 0;
var categorias = "";

//consultamos en base de datos las categorias
   $.ajax({
        type: "GET",
        url: "../compras/categorias",
        data: { cont:"1"}
        })
        .done(function( data ) {
        categorias = data;
    });

function ValidarItems(fecha, descripcion, cantidad, referencia, precio) {

    if (fecha != "" && descripcion != "" && cantidad != "" && referencia != "" && precio != "") {
        return true;
    } else {
        return false;
    }

}

function GuardarItem(t) {

    if (items.length == 10) {
        toastr.error('Solo puede agregar un maximo de 10 Items', 'Error');
        return;
    }

    var fecha = $("#fecha").val();
    var categoria = $("#categorias").find(':selected').html();
    var categoriaval = $("#categorias").find(':selected').val();
    var descripcion = $("#descripcion").val();
    var cantidad = $("#cantidad").val();
    var referencia = $("#referencia").val();
    var precio = $("#precio").val();

    var validacion = ValidarItems(fecha, descripcion, cantidad, referencia, precio);

    if (validacion) {

        items.push(new Array(fecha, descripcion, cantidad, referencia, precio, categoriaval));

        posicion = items.length - 1;
        
        $("#cancelar"+t).remove();

        $("#bodytable").append('<tr id="posicion' + posicion + '">\n\
                                    <td>' + fecha + '</td>\n\\n\
                                    <td>' + categoria + '</td>\n\
                                    <td>' + descripcion + '</td>\n\
                                    <td>' + cantidad + '</td>\n\
                                    <td>' + referencia + '</td>\n\
                                    <td>' + precio + '</td>\n\
                                    <td><button type="button" onclick="BorrarItem(' + posicion + ')" class="btn btn-danger">Eliminar</button></td>\n\
                                </tr>');

        document.getElementById("formcompras").reset();

    } else {
        toastr.error('Todos los campos son obligatorios.', 'Error');
    }

}

function BorrarItem(pos) {

    items.splice(pos, 1);

    $("#posicion" + pos).remove();

}

function NuevoItem(){
    
    t = items.length+1;
    
    $("#bodytable").prepend('<tr  id="cancelar'+t+'">\n\
                                    <td><input type="text" name="fecha" id="fecha" class="form-control input-small fecha_required" placeholder="yyyy-mm-dd" value=""></td>\n\\n\
                                    <td>'+categorias+'</td>\n\
                                    <td><textarea id="descripcion" name="descripcion" rows="3" class="form-control" ></textarea></td>\n\
                                    <td><input type="text" name="cantidad" id="cantidad" class="form-control input-small number" value=""></td>\n\
                                    <td><input type="text" name="referencia" id="referencia" class="form-control input-small" value=""></td>\n\
                                    <td><input type="text" name="precio" id="precio" class="form-control input-small money" value="0"></td>\n\
                                    <td><button type="button" onclick="CancelarItem('+t+')" class="btn btn-danger">Cancelar</button>\n\
                                        <button type="button" onclick="GuardarItem('+t+')" class="btn btn-success">Guardar</button></td></tr>');
    
    $( ".fecha_required" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });
    
}

function CancelarItem(t){
    
    $("#cancelar"+t).remove();
    
}

function GuardarNuevaCompra() {

    var justificacion = $("#justificacion").val();

    if (items.length > 0) {

        if (justificacion != "") {

            $.ajax({
                type: "GET",
                url: "nuevacompra",
                data: {justificacion: justificacion, items: items }
            }).done(function (data) {
                if (data == "0") {
                    toastr.error('No ha sido posible guadar su nueva solicitud', 'Error');
                } else {
                    
                    $("#justificacion").val(" ");
                    $("#bodytable").html(" ");
                    items = new Array();
                    posicion = 0;
                    t = 0;
                    toastr.success('Su nueva solicitud de compra ha sido guardada.', 'Nueva Solicitud');
                    if(data!=1){
                        var type_event = ["newshopping", data[0]["name"],data[0]["last_name"],data[0]["img_min"], data[1]["id"]];
                        send(type_event);
                    }
                }                                    
            });

        } else {
            toastr.error('La justificacion de la compra es obligatoria.', 'Error');
        }
        
    } else {
        toastr.error('Debe ingresar al menos un item.', 'Error');
    }

}