var permisos = new Array();
var permisos_e = new Array();
var permisos_a = new Array();
var permisos_eq = new Array();

function guardarperfil() {
    var perfil = $("#name_new_profile").val();
    var proceso_perfil = $("#proceso_perfil").val();    
    if (perfil !== "") {
        if (permisos.length > 0) {
            $.ajax({
                type: "GET",
                url: "guardarPerfil",
                data: {perfil: perfil, proceso_perfil: proceso_perfil, permisos: permisos, }
            }).done(function (data) {
                        if (data == "1") {
                                toastr.success('El nuevo perfil ha sido guardado', 'Nuevo Perfil');
                                $("#close_modal_perfil").click();
                        } else {
                            toastr.error('No ha sido posible guardar el nuevo perfil', 'Error');
                        }
                    });
        } else {
            toastr.error('Debe asignar algun permiso al nuevo perfil', 'Error');
        }
    } else {
        $("#name_new_profile").css("border-color", "#b94a48");
        toastr.error('El campo Nombre de perfil está vacio', 'Error');
    }

}

function eliminarPermisos() {
    var perfil = $("#profiles_cahge").find(':selected').val();
    if (perfil !== "") {
        if (permisos_e.length > 0) {
            $.ajax({
                type: "GET",
                url: "eliminarPermisos",
                data: {perfil: perfil, permisos_e: permisos_e, }
            }).done(function (data) {
                        if (data == "1") {
                                toastr.success('Los cambios se han guardado correctamente', 'Eliminar Permisos');
                                $("#close_modal_perfil_e").click();
                        } else {
                            toastr.error('No ha sido posible editar el perfil', 'Error');
                        }
                    });
        } else {
            toastr.error('Debe eliminar algun permiso del perfil', 'Error');
        }
    } else {
        toastr.error('Debe seleccionar un perfil', 'Error');
    }

}

function agregarPermisos() {
    var perfil = $("#profiles_change_add").find(':selected').val();
    if (perfil !== "") {
        if (permisos_a.length > 0) {
            $.ajax({
                type: "GET",
                url: "agregarPermisos",
                data: {perfil: perfil, permisos_a: permisos_a, }
            }).done(function (data) {
                        if (data == "1") {
                                toastr.success('Los cambios se han guardado correctamente', 'Agregar Permisos');
                                $("#close_modal_perfil_a").click();
                        } else {
                            toastr.error('No ha sido posible editar perfil', 'Error');
                        }
                    });
        } else {
            toastr.error('Debe agregar algun permiso al perfil', 'Error');
        }
    } else {
        toastr.error('Debe seleccionar un perfil', 'Error');
    }

}
function agregarMantenimientos(id) {
    var comentario = $("#observation_edit").val();
    if (comentario !== "") {
        if (permisos_a.length > 0) {
            $.ajax({
                type: "GET",
                url: "agregarmantenimientos",
                data: {comentario: comentario, permisos_a: permisos_a,id_equipo:id }
            }).done(function (data) {
                        if (data == "1") {
                                toastr.success('Los cambios se han guardado correctamente', 'Agregar Permisos');
                                $("#close_modal_perfil_a").click();
                        } else {
                            toastr.error('No ha sido posible editar perfil', 'Error');
                        }
                    });
        } else {
            toastr.error('Debe agregar algun Mantenimiento', 'Error');
        }
    } else {
        toastr.error('El campo comentario esta Vacio.', 'Error');
    }

}
function agregar_equipos_al_grupo() {
    var grupo = $("#id_grupo_asign").val();
    if (grupo !== "") {
        if (permisos_eq.length > 0) {
            $.ajax({
                type: "GET",
                url: "agregarequiposalgrupo",
                data: {grupo: grupo, permisos_eq: permisos_eq }
            }).done(function (data) {
                        if (data == "1") {
                                toastr.success('Los cambios se han guardado correctamente', 'Agregar Equipos');
                                $("#close_modal_perfil_a").click();
                        } else {
                            toastr.error('No ha sido posible agregar los Equipos', 'Error');
                        }
                    });
        } else {
            toastr.error('Debe agregar algun Equipo', 'Error');
        }
    } else {
        toastr.error('El campo grupo esta Vacio.', 'Error');
    }

}
// Put your application scripts here
(function ($) {

    $(function () {
        $('#aloha').multiSelect({
            keepOrder: true
        });
        $('.multiselect').multiSelect({});

        $('.searchable').multiSelect({
            selectableHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='try \"12\"'>",
            selectionHeader: "<input type='text' class='search-input' autocomplete='off' placeholder='try \"4\"'>",
            afterInit: function (ms) {
                var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function (e) {
                            if (e.which === 40) {
                                that.$selectableUl.focus();
                                return false;
                            }
                        });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function (e) {
                            if (e.which == 40) {
                                that.$selectionUl.focus();
                                return false;
                            }
                        });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });


        $('#keep-order').multiSelect({
            keepOrder: true
        });

        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });


        $('#custom-headers').multiSelect({
            selectableHeader: "<div class='custom-header'>Selectable item</div>",
            selectionHeader: "<div class='custom-header'>Selection items</div>",
            selectableFooter: "<div class='custom-header'>Selectable Footer</div>",
            selectionFooter: "<div class='custom-header'>Selection Footer</div>"
        });

        $('#callbacks').multiSelect({
            afterSelect: function (values) {
                permisos.push(parseInt(values));
                //alert("Select value: "+values);
            },
            afterDeselect: function (values) {
                var j = 0;
                for (j; permisos.length > j; j++) {
                    if (parseInt(permisos[j]) == parseInt(values)) {
                        permisos.splice(j, 1);
                    }
                }
                //alert("Deselect value: "+values);
            }
        });
        
        $('#callbacks2').multiSelect({
            afterSelect: function (values) {
                permisos.push(parseInt(values));
                //alert("Select value: "+values);
            },
            afterDeselect: function (values) {
                var j = 0;
                for (j; permisos.length > j; j++) {
                    if (parseInt(permisos[j]) == parseInt(values)) {
                        permisos.splice(j, 1);
                    }
                }
                //alert("Deselect value: "+values);
            }
        });
        
        $('#callbacks3').multiSelect({
            afterSelect: function (values) {
                permisos_e.push(parseInt(values));
                //alert(permisos_e);
            },
            afterDeselect: function (values) {
                var j = 0;
                for (j; permisos_e.length > j; j++) {
                    if (parseInt(permisos_e[j]) == parseInt(values)) {
                        permisos_e.splice(j, 1);
                    }
                }
                //alert(permisos_e);
            }
        });
        
        $('#callbacks4').multiSelect({
            afterSelect: function (values) {
                permisos_a.push(parseInt(values));
                //alert(permisos_e);
            },
            afterDeselect: function (values) {
                var j = 0;
                for (j; permisos_a.length > j; j++) {
                    if (parseInt(permisos_a[j]) == parseInt(values)) {
                        permisos_a.splice(j, 1);
                    }
                }
                //alert(permisos_e);
            }
        });
        $('#callbacks5').multiSelect({
            afterSelect: function (values) {
                permisos_a.push(parseInt(values));
                //alert(permisos_e);
            },
            afterDeselect: function (values) {
                var j = 0;
                for (j; permisos_a.length > j; j++) {
                    if (parseInt(permisos_a[j]) == parseInt(values)) {
                        permisos_a.splice(j, 1);
                    }
                }
                //alert(permisos_e);
            }
        });
        $('#callbacks6').multiSelect({
            afterSelect: function (values) {
                permisos_eq.push(parseInt(values));
                //alert(permisos_e);
            },
            afterDeselect: function (values) {
                var j = 0;
                for (j; permisos_eq.length > j; j++) {
                    if (parseInt(permisos_eq[j]) == parseInt(values)) {
                        permisos_eq.splice(j, 1);
                    }
                }
                //alert(permisos_e);
            }
        });

        $('#refresh').on('click', function () {
            $('#public-methods').multiSelect('refresh');
            return false;
        });

        $('#public-methods').multiSelect({});

        $('#select-all').click(function () {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function () {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });

        var arr = [];

        for (var i = 0; i < 100; i++) {
            arr[i] = 'elem_' + (i + 1);
        }

        $('#select-100').click(function () {
            $('#public-methods').multiSelect('select', arr);
            return false;
        });
        $('#deselect-100').click(function () {
            $('#public-methods').multiSelect('deselect', arr);
            return false;
        });

        $('#add-option').on('click', function () {
            $('#public-methods').multiSelect('addOption', {value: 42, text: 'test 42', index: 0});
            return false;
        });

    });
})(jQuery);
