$(function () {
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////Funciones para documentacion de los procesos/////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // función para cargar el contenido de una carpeta en el árbol de navegación

    $(document).on('click', '.folder', function (event) {

        var root = parseInt($(this).attr('id'));
        dir = $(this).attr('dir');

        if (root)
        {
            id = "#" + root;

            if (!($(this).hasClass('clicked'))) {

                var html = $.ajax({
                    type: "GET",
                    url: "../cargardocumentos",
                    data: {id: root},
                    async: false
                }).responseText;

                $("#ad" + dir).append(html);
                $(this).addClass('clicked');
                $(this).children('i').removeClass('icon-folder-close');
                $(this).children('i').addClass('icon-folder-open');

            } else {
                html = "";
                $("#ad" + dir).html(html);
                $(this).removeClass('clicked');
                $(this).children('i').removeClass('icon-folder-open');
                $(this).children('i').addClass('icon-folder-close');
            }

        }

    });

//función para cargar un documento en el navegador de documentos

    $(document).on('click', '.file', function (e) {

        var root = $(this).attr('id');
        var my_url = $(this).attr('dir');
        var extension = (my_url.substring(my_url.lastIndexOf("."))).toLowerCase();
        
        if (root)
        {
            
            var html = $.ajax({
                    type: "GET",
                    url: "../estadocumentos",
                    data: {id: root},
                    async: false
                }).responseText;
            
            
            if (extension == '.pdf' || extension == '.jpg' || extension == '.png') {
                $('#docview embed').remove();
                $('#docview').append('<embed id="doc-container" src="' + my_url + '">');
            }else{
                $('#docview embed').remove();
                window.open(my_url,'','width=600,height=400,left=50,top=50,toolbar=yes');
            }
        }

    });
    
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   //////////////////////////////////////////////////////////Fin funciones para documentacion de los procesos////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
   
   ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   //////////////////////////////////////////////////////////Funciones para documentacion de los aspirantes////////////////////////////////////////////////////////////////
   /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////            
    
    // función para cargar el contenido de una carpeta en el árbol de navegación

    $(document).on('click', '.folderasp', function (event) {

        var root = parseInt($(this).attr('id'));
        dir = $(this).attr('dir');

        if (root)
        {
            id = "#" + root;

            if (!($(this).hasClass('clicked'))) {

                var html = $.ajax({
                    type: "GET",
                    url: "cargardocumentos",
                    data: {id: root},
                    async: false
                }).responseText;

                $("#ad" + dir).append(html);
                $(this).addClass('clicked');
                $(this).children('i').removeClass('icon-folder-close');
                $(this).children('i').addClass('icon-folder-open');

            } else {
                html = "";
                $("#ad" + dir).html(html);
                $(this).removeClass('clicked');
                $(this).children('i').removeClass('icon-folder-open');
                $(this).children('i').addClass('icon-folder-close');
            }

        }

    });

//función para cargar un documento en el navegador de documentos

    $(document).on('click', '.fileasp', function (e) {

        var root = $(this).attr('id');
        var my_url = $(this).attr('dir');
        var extension = (my_url.substring(my_url.lastIndexOf("."))).toLowerCase();
        
        if (root)
        {
            
            if (extension == '.pdf' || extension == '.jpg' || extension == '.png') {
                $('#docview embed').remove();
                $('#docview').append('<embed id="doc-container" src="' + my_url + '">');
            }else{
                $('#docview embed').remove();
                window.open(my_url,'','width=600,height=400,left=50,top=50,toolbar=yes');
            }
        }

    });
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
   //////////////////////////////////////////////////////////Fin funciones para documentacion de los procesos////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////             

});