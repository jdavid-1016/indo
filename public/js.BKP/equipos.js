function desactivar_tipo(id, status){
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id: id, status: status}
    })
    .done(function(data) {
        if (data != 2) {
            toastr.success('Se Gestiono correctamente el Tipo de Mantenimiento', 'Desactivo Tipo de Mantenimiento');
            
            $("#tabla_tipo_equipos").html(data);
            
        }else{
            toastr.error('No ha sido posible Gestionar el Tipo de Mantenimiento', 'Error');
        }
    });
}
function detalle_mantenimientos(id){
    
    $.post('frommantenimientos', {
              id:id
            } ,

            function(response){
                $('#basic').html(response);
                //var datos = JSON.parse(response);
            });
}
function Ingresar_tipo_mantenimiento(){
    var tipo_equipo = $('#item_mant').val();
    var tipo_mant = $('#tipo_mant').val();

    if (tipo_equipo == "") {
        toastr.error('El campo Item Mantenimiento está vacio.', 'Error');
        return;
    }
    if (tipo_mant == "") {
        toastr.error('El campo Tipo  está vacio.', 'Error');
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_equipo: tipo_equipo, tipo_mant:tipo_mant}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Mantenimiento', 'Nuevo Tipo de Mantenimiento');
            $("#item_mant").val("");
            $("#tipo_mant").val("");
            $("#tabla_tipo_equipos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Mantenimiento', 'Error');
        }
    });
}

function cargar_detalle_prospecto2(id){

    if (tipo_equipo == "") {
        toastr.error('El campo Item Mantenimiento está vacio.', 'Error');
        return;
    }
    if (tipo_mant == "") {
        toastr.error('El campo Tipo  está vacio.', 'Error');
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_equipo: tipo_equipo, tipo_mant:tipo_mant}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Mantenimiento', 'Nuevo Tipo de Mantenimiento');
            $("#item_mant").val("");
            $("#tipo_mant").val("");
            $("#tabla_tipo_equipos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Mantenimiento', 'Error');
        }
    });
}

function crear_tipo_equipo(){
    var item = $("#tipo_equipo").val();
    if (item == "") {
        toastr.error("El campo Tipo Equipo esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_equipo: item, status:1}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Equipo', 'Nuevo Tipo de Equipo');
            $("#tipo_equipo").val("");
            
            $("#tabla_tipo_equipos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Equipo', 'Error');
        }
    });
}
function crear_tipo_mantenimiento(){
    var item = $("#tipo_mantenimiento").val();
    if (item == "") {
        toastr.error("El campo Tipo Mantenimiento esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_mantenimiento: item, status:1}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Mantenimiento', 'Nuevo Tipo de Mantenimiento');
            $("#tipo_mantenimiento").val("");
            
            $("#tabla_tipo_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Mantenimiento', 'Error');
        }
    });
}
function crear_grupo_equipos(){
    var grupo = $("#nombre_grupo").val();
    if (grupo == "") {
        toastr.error("El campo Nombre del Grupo esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creargrupoequipos",
            data: { grupo: grupo, status:1}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Grupo de Equipos', 'Nuevo Grupo de Equipo');
            $("#nombre_grupo").val("");
            
            $("#tabla_grupo_mantenimiento").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Grupo de Equipos', 'Error');
        }
    });
}
function agregar_equipo_a_grupo(){
    var equipo = $("#id_equipo_asing").val();
    var grupo = $("#id_grupo_asign").val();


    if (equipo == "") {
        toastr.error("El campo Equipo está vacio.", "Error");
        return;
    }
    if (grupo == "") {
        toastr.error("El campo Grupo esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "agregarequipoagrupo",
            data: { grupo: grupo, equipo:equipo}
    })
    .done(function(data) {
        if (data == 1) {
            toastr.success('Se Agrego correctamente el el Equipo al Grupo', 'Nuevo Asignación de Equipo');
            $("#id_equipo_asing").val("");
            $("#id_grupo_asign").val("");
            $('.select2-search-choice-close').click();
            // $("#tabla_grupo_mantenimiento").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Equipo al Grupo', 'Error');
        }
    });
}
function crear_item_mantenimiento(){
    var item = $("#item_mantenimiento").val();
    var tipo = $("#item_tipo_mantenimiento").val();
    var requerido = $("#requerido_mantenimiento").val();
    if (item == "") {
        toastr.error("El campo Item Mantenimiento esta vacio.", "Error");
        return;
    }
    if (tipo == "") {
        toastr.error("El campo Tipo Mantenimiento esta vacio.", "Error");
        return;
    }
    if (requerido == "") {
        toastr.error("El campo Requerido esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { item_mantenimiento: item, tipo:tipo, requerido:requerido}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Item de Mantenimiento', 'Nuevo Item de Mantenimiento');
            $("#item_mantenimiento").val("");
            $("#item_tipo_mantenimiento").val("");
            $("#requerido_mantenimiento").val("");
            
            $("#tabla_item_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Item de Mantenimiento', 'Error');
        }
    });
}
function  gestionar_item_mantenimientos(item,status){

    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id_item: item, status:status}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se Modifico correctamente el Item de Mantenimiento', 'Gestion Item de Mantenimiento');
            $("#item_mantenimiento").val("");
            $("#item_tipo_mantenimiento").val("");
            $("#requerido_mantenimiento").val("");
            
            $("#tabla_item_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible Modificar el  Item de Mantenimiento', 'Error');
        }
    });

}
function  gestionar_tipo_mantenimientos(item,status){

    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id_tipo_mant: item, status:status}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se Modifico correctamente el Tipo de Mantenimiento', 'Gestion Tipo de Mantenimiento');
            $("#item_mantenimiento").val("");
            $("#item_tipo_mantenimiento").val("");
            $("#requerido_mantenimiento").val("");
            
            $("#tabla_tipo_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible Modificar el  Tipo de Mantenimiento', 'Error');
        }
    });

}