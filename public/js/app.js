function TodoCtrl($scope, $http){    
	$http.get("todos").success(function(todos){
		$scope.todos = todos;
	});

	$http.get("cityes").success(function(cityes){
		$scope.cityes = cityes;
	});

	$scope.remaining = function(){
		var count = 0;

		angular.forEach($scope.todos, function(todo){
			count += todo.done ? 0 : 1;
		});
		return count;
	}

	$scope.addNew = function(){
		var todo = {
			name: $scope.name,
			name2: $scope.name2,
			last_name: $scope.last_name,
			last_name2: $scope.last_name2, 
			email_institutional: $scope.email_institutional,
			email: $scope.email,
			cell_phone: $scope.cell_phone,
			phone: $scope.phone,
			type_document: $scope.type_document,
			document: $scope.document,
			city_id: $scope.city_id,
			address: $scope.address,
			profile: $scope.profile_user
		}

		

		$scope.todotxt = "";

				$http.post("todos", todo).success(function(data){
		                    //		$scope.todos.push(todo);
		                    var type_event = ["newuser", todo.name, todo.last_name, todo.phone, todo.email, todo.document];                    
		                        send(type_event);
					$('.close').click();
				}).error(function(data){
					//alert('no se guardo')
				});
	}
                
}

function help_deskCtrl($scope, $http){
	$http.get("categories").success(function(categories){
		$scope.categories = categories;
	});
	$http.get("allSupports").success(function(supports){
		$scope.supports = supports;
	});
	$http.get("supportsPending").success(function(supports){
		$scope.supportsPending = supports;
	});
	$scope.ordenarPor = function(orden){
		$scope.ordenSeleccionado = orden;
	};
	$http.get("my_support_asing").success(function(supports){
		$scope.supports_asing = supports;
	});
	$scope.newSupport = function(){
		
		var description = $("#description").val();
		var category = $("#category").val();
		var priority = $("#priority").val();


		if (description == "") {
			toastr.warning('El campo descripcion esta vacio', 'Error');
			return;
		}
		if (category == "") {
			toastr.warning('El campo categoria esta vacio', 'Error');
			return;
		}
		if (priority == "") {
			toastr.warning('El campo prioridad esta vacio', 'Error');
			return;
		}
		
		if ($scope.description == "") {
			toastr.warning('El campo descripcion esta vacio', 'Error');

		}
		if ($scope.priority == "") {
			toastr.warning('El campo prioridad esta vacio', 'Error');
		}
		if ($scope.category == "") {
			toastr.warning('El campo categoria esta vacio', 'Error');
		}
		var support = {
			description: $scope.description,
			priority: $scope.priority,
			category: $scope.category		
		}


		if ($scope.description == "" || $scope.priority == "" || $scope.category == "") {
			//alert('todo esta vacio');
		}
		else{
			

		

		$scope.description = "";
		$scope.category = "";
		$scope.priority = "";

		$http.post("newSupport", support).success(function(data){		    
                    var type_event = ["newsupport", data["name"],data["last_name"], data["img_min"]];
		         send( type_event);
                    
                    var parametros = {
                        "description" : support.description,
                        "category" : support.category,
                    };
                       
                      $.ajax({
                        data: parametros,                        
                        url: "support_faq",
                        type: "GET",
                        beforeSend: function(){
                            $('#support_faq').html('<img width="50px" src="assets/img/cargando.gif">');
                        },
                        success: function(html_faq){
                            setTimeout(
                                    function(){
                                       $('#support_faq').html(html_faq);
                                       $('#admin_suppotrs').html('<div class="note note-warning"><h4 class="block">Atención:</h4><p>No puede Ingresar mas solicitudes hasta que no sean cerradas todas las solicitudes pendientes. <a class="btn btn-success btn-xs" href="my_supports_pending"> <i class="icon-ok"></i>Ver Mis Solicitudes Pendientes</a></p></div>');                                       
                                    }, 2000);
                        }
                    });
                    
                    toastr.success('Su solicitud se ha ingresado correctamente', 'Nueva Solicitud');
		});

		}
		

	}
	$scope.ratingSupport = function(){
		var support = {
			question_1: $scope.question_1,
			question_2: $scope.question_2,
			question_3: $scope.question_3,	
			support_id:	$scope.support_id 
		}

		

		$http.post("submit_rating_support", support).success(function(data){
                    toastr.success('Se ha enviado la calificación correctamente', 'calificación');
		});
		

	}
}

function FunCtrl($scope, $http){
	$http.get("funcionarios").success(function(funcionarios){
		$scope.funcionarios = funcionarios;
	});
}

function ManCtrl($scope, $http){
	$http.post("mantenimientosequipos?var=1").success(function(mantenimientosequipos){
		$scope.mantenimientosequipos = mantenimientosequipos;
	});
}