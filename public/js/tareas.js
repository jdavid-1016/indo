    var items = new Array();
    var items_items = new Array();
    var items_contador = new Array();
    var tareas_posicion = 0;
    var items_posicion = 0;
    
    function crear_tarea(){
        var tarea = $("#tarea").val();
        var tarea_desc = $("#tarea_desc").val();
        var tarea_per = $("#tarea_per").val();
        var tarea_fecha_ini = $("#tarea_fecha_ini").val();

        var responsable = $("#responsable").val();
        var prioridad = $("#prioridad").val();
        var categoria = $("#categoria").val();

        var tarea_fecha_venci   = $("#tarea_fecha_venci").val();
        

        if ($("#noti_correo").attr('checked')) {
            var noti_correo = 1;
            
        }else{
            var noti_correo = 0;
        }

        if(tarea == ""){
            toastr.error('El campo Tarea no puede estar vacio.', 'Error');
            $("#tarea").focus();
            return;
        }
        if(tarea_desc == ""){
            toastr.error('El campo Descripción no puede estar vacio.', 'Error');
            $("#tarea_desc").focus();
            return;
        }
        if(tarea_per == ""){
            toastr.error('El campo Periodicidad no puede estar vacio.', 'Error');
            $("#tarea_per").focus();
            return;
        }
        if(tarea_fecha_ini == ""){
            toastr.error('El campo Fecha de Inicio no puede estar vacio.', 'Error');
            $("#tarea_fecha_ini").focus();
            return;
        }
        if(tarea_fecha_venci == ""){
            toastr.error('El campo Fecha de Vencimiento no puede estar vacio.', 'Error');
            $("#tarea_fecha_venci").focus();
            return;
        }
        if(items.length == ""){
            toastr.error('Debe asignar la tarea a por lo menos un Usuario.', 'Error');
            $("#btn_most_users").click();
            return;
        }
        
        if(prioridad == ""){
            toastr.error('El campo Prioridad no puede estar vacio.', 'Error');
            $("#prioridad").focus();
            return;
        }
        if(categoria == ""){
            toastr.error('El campo Categoria no puede estar vacio.', 'Error');
            $("#categoria").focus();
            return;
        }
        

        $.ajax({
                type: "GET",
                url:  "ingresartarea",
                data: { tarea:tarea,
                        tarea_desc:tarea_desc,
                        tarea_per:tarea_per,
                        tarea_fecha_ini:tarea_fecha_ini,
                        responsable:items,
                        items:items_items,
                        prioridad:prioridad,
                        tarea_fecha_venci:tarea_fecha_venci,
                        noti_correo:noti_correo,
                        categoria:categoria
                }
        })
        .done(function(data) {
            if (data != 2) {
                toastr.success('Creo correctamente la tarea', 'Nueva tarea');
                var type_event = ["nuevaactividad", data[0]["name"],data[0]["last_name"], data[0]["img_min"], data[1]];
                send( type_event);


                $("#tarea").val("");
                $("#tarea_desc").val("");
                $("#tarea_per").val("");
                $("#tarea_fecha_ini").val("");
                $("#tarea_fecha_venci").val("");
                $("#responsable").val("");
                $("#prioridad").val("");
                
                 
            }else{
                toastr.error('No ha sido posible crear la tarea', 'Error');
            }
        });
    }
    function tareas_guardar_usuario(){

        var responsable = $("#responsable").val();

        $.ajax({
                type: "GET",
                url:  "consultausuario",
                data: { 
                    responsable:responsable
                }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.error('No ha sido posible cargar el usuario', 'Error');
            }else{
                items.push(responsable);
                tareas_posicion = items.length - 1;

                console.log(items.toString());
                console.log(tareas_posicion);


                $("#tareas_usuarios").append(data);
                $("#responsable").val('');
                

            }
        });

    }
    function tareas_guardar_item(){


        var item1 = $("#item1").val();
        if (item1 == "") {
            toastr.error('El campo Item no puede estar vacio.', 'Error');
            $("#item1").focus();
            return;
        }

        
        var aleatorio = Math.round(Math.random()*100);
            
        items_items.push(item1);
        items_contador.push(aleatorio);
        items_posicion = items_items.length - 1;

        console.log(items_items.toString());
        console.log(items_posicion);

        var text = "<tr id='id_item_"+aleatorio+"'><td>"+item1+"</td><td><a class='btn' onclick='tareas_eliminar_item("+aleatorio+")'><i class='icon-remove'></a></td></tr>";

                $("#tareas_items").append(text);
                $("#item1").val('');
    }
    function eliminar_usuario(posicion, id ){
        $("#id_usuario_"+posicion).hide('slow');
        for (var i = 0; i < items.length; i++) {

            if (items[i] == id) {
                items.splice(i,1);
                console.log('eliminado');
            }
            
        };
        
        console.log(items.toString());
        // $("#id_usuario_"+id).hide('hide');
    }
    function tareas_eliminar_item(posicion, id ){
        $("#id_item_"+posicion).hide('slow');

        for (var i = 0; i < items_contador.length; i++) {

            if (items_contador[i] == posicion) {
                items_items.splice(i,1);
                items_contador.splice(i,1);
                console.log('eliminado');
            }
            
        };
        
        console.log(items_items.toString());
        // $("#id_usuario_"+id).hide('hide');
    }
    function cargar_responsables(){
        var process = $("#proceso").val();

        $("#responsable").load('cargarresponsable?id_proceso=' + process);
        
    }
    function cargar_responsables2(){
        var process = $("#proceso").val();

        $("#responsable2").load('cargarresponsable?id_proceso=' + process);
        
    }
    function cambiar_estado(id){
        var estado_tarea = $("#estado_tarea").val();
        var calificacion_tarea = $("#calificacion_tarea").val();

        if (estado_tarea == 2) {
            if (calificacion_tarea == "") {
                toastr.error('El campo Calificacion de la Actividad no puede estar vacio', 'Error');
                return;
            }
        }


        if (estado_tarea == "") {
            toastr.error('El campo Estado de la Solicitud no puede estar vacio', 'Error');
            return;
        }

        $.ajax({
                type: "GET",
                url:  "cambiarestado",
                data: { 
                    estado_tarea:estado_tarea, id_tarea:id, calificacion_tarea:calificacion_tarea
                }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se han guardado los cambios correctamente', 'Error');
                $("#estado_tarea").val('');
                actualizar_tabla_tareas_admin();
                $("#close_modal").click();
            }else{
                toastr.error('No ha sido posible cambiar el estado', 'Error');

                

            }
        });

    }
    function actualizar_tabla_tareas_admin(){

        var html2 = $.ajax({
            type: "GET",
            url: "ver",
            cache: false,
            data: {filtro: 1},
            async: false
        }).responseText;

        $('#tabla_tareas').html(html2);

    }
    function actualizar_tabla_mis_tareas(id){
        var html2 = $.ajax({
            type: "GET",
            url: "asignadas",
            cache: false,
            data: {filtro: 1},
            async: false
        }).responseText;

        $('#tabla_mis_tareas').html(html2);
    }
    function validar_gestion(){
        var id_gestion = $("#id_gestion").val();
        if (id_gestion == 1) {
            $("#btn_atender_solicitud").html('Atender Actividad');
            $("#escalar_actividad").hide('slow');
            $("#rechazar_actividad").hide('slow');
        }
        if (id_gestion == 2) {
            $("#btn_atender_solicitud").html('Rechazar Actividad');
            $("#rechazar_actividad").show('show');
            $("#escalar_actividad").hide('slow');
        }
        if (id_gestion == 3) {
            $("#btn_atender_solicitud").html('Escalar Actividad');
            $("#escalar_actividad").show('show');
            $("#rechazar_actividad").hide('slow');
        }
    } 
    function cargar_responsables(){
        var process = $("#proceso").val();

        $("#responsable").load('cargarresponsable?id_proceso=' + process);
        
    }
    function atender_tarea(id_tarea, id_asignacion){
        var comentario_rechazado = $("#comentario_rechazado").val();
        var responsable = $("#responsable2").val();
        var id_gestion = $("#id_gestion").val();
        
        if (id_gestion == 2) {
            if (comentario_rechazado == "") {
                toastr.error('El campo Comentario no puede estar vacio.', 'Error');
                $("#comentario_rechazado").focus();
                return;
            }
            $("#close_modal").click();
        }
        if (id_gestion == 3) {
            if (responsable == "") {
                toastr.error('El campo Usuario no puede estar vacio.', 'Error');
                $("#responsable2").focus();
                return;
                
            }
                $("#close_modal").click();

        }

        $.ajax({
            type: "post",
            url:  "confirmatend",
            data: { id_tarea: id_tarea, id_asignacion:id_asignacion, comentario_rechazado:comentario_rechazado, responsable:responsable, id_gestion:id_gestion }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Gestiono correctamente la Actividad', 'Gestión de Actividad');
                actualizar_tabla_mis_tareas();
                
            }else{
                toastr.error('No se ha podido gestionar la Actividad', 'Error');
            }

        });
    }
    function reasignar_tarea(id){
        var responsable = $("#responsable2").val();

        $.ajax({
            type: "get",
            url:  "reasignartarea",
            data: { id_tarea: id, responsable:responsable }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Gestiono correctamente la Actividad', 'Gestión de Actividad');
                $("#close_modal").click();
                
            }else{
                toastr.error('No se ha podido gestionar la Actividad', 'Error');
            }

        });
    }
    function activar_campo(id){

        
        if($("#item_"+id).is(':checked')) {  
            $("#texto_item_"+id).show('show');
        }else{
            $("#texto_item_"+id).hide('slow');
        }
    }
    function progreso(signo){
        var progress = parseFloat($("#progress").val());
        var total = progress;

        if (signo == 1) {
            if (progress != 100) {
                total = progress+5;
            }
        }else{
            if (progress != 0) {
                total = progress-5;
            }
            
        }

        $("#progress").val(total);
    }
    function activar_calificacion(){
        var estado_activ = $("#estado_tarea").val();
        if (estado_activ == 2) {
            $("#campo_calificacion").show('show');
        }else{
            $("#campo_calificacion").hide('slow'); 
            
        }

    }
    function agregar_nuevo_item(id_tarea){
        // console.log(id_tarea);
        // return;

        
        var otro_nuevo_item = $("#otro_nuevo_item").val();

        $.ajax({
            type: "get",
            url:  "agregaritematarea",
            data: { otro_nuevo_item:otro_nuevo_item, id_tarea:id_tarea }
        })
        .done(function(data) {
            if (data == 1) {
                toastr.success('Se Agrego correctamente el nuevo item.', 'Nuevo Item');
                
                cargar_detalle_tarea(id_tarea, 1);
                
            }else{
                toastr.error('No se ha podido Agregar el Item', 'Error');
            }

        });
    }

    
