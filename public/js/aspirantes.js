$( ".fecha_required_asp" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '-50y' });
$( ".fecha_required_time" ).datepicker({ dateFormat: "yy-mm-dd h:m:s", minDate: '-50y' });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////Funciones para gestionar los nuevos prospectos//////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function removeAdmin(tipo, id){              
           
            var r = confirm("¿ Esta seguro que desea eliminar el Item?");
                         if (r == false) {
                             return;
                         }else{

                                $.ajax({
                                  //metodo ajax
                                       type: "GET",//aqui puede  ser get o post
                                       url: "removeadmin",//la url adonde se va a mandar la cadena a buscar
                                       data: {tipo:tipo, id: id},
                                       cache: false,
                                       success: function(html)//funcion que se activa al recibir un dato
                                       {
                                           
                                            switch (tipo)
                                            {

                                                    case "1":
                                                        
                                                        $("#admin_metodo_"+id).remove();

                                                    break;

                                                    case "2":
                                                        
                                                        $("#admin_campana_"+id).remove();

                                                    break;
                                                    
                                                    case "3":
                                                        
                                                        $("#admin_fuente_"+id).remove();

                                                    break;
                                                    
                                                    case "4":
                                                        
                                                        $("#admin_estado_"+id).remove();

                                                    break;
                                                    
                                                    case "5":
                                                        
                                                        $("#admin_documento_"+id).remove();

                                                    break;
                                                    
                                                    case "6":
                                                        
                                                        $("#admin_convocatoria_"+id).remove();

                                                    break;
                                                    
                                                    case "7":
                                                        
                                                        $("#admin_salon_"+id).remove();

                                                    break;
                                                    
                                                    case "8":
                                                        
                                                        $("#admin_asig_"+id).remove();

                                                    break;
                                            }

                                           toastr.success('El registro se elimino correctamente', 'Cambios Guardados');
                                       }
                                });

                        }

   }

function guardarCambiosAspirantes(id){
       
       var name = $('#name').val();
       var last_name = $('#last_name').val();
       var email = $('#email').val();
       var phone = $('#phone').val();
       var phone2 = $('#phone2').val();
       var document = $('#document').val();
       var convocations = $('#convocatorias').val();
       var city = $('#city').val();
       var status = $('#prospectos_status').val();
       var programs = $('#programs').val();
       var observation_edit = $('#observation_edit').val();
       
       if(observation_edit==""){
           
            var r = confirm("¿ Desea continuar sin escribir un comentario ?");
                         if (r == false) {
                             return;
                         }
                         
       }
              
       $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "guardarcambios",//la url adonde se va a mandar la cadena a buscar
                data: {id:id, name: name, last_name: last_name, email:email, phone:phone, phone2:phone2, document:document, city:city, status:status, programs:programs, convocations:convocations, observation_edit:observation_edit},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    $('#observation_edit').val("");
                    toastr.success('Cambios guardados correctamente', 'Cambios Guardados');
                }
        });
      
   }
   

//funcion para guardar un nuevo aspirante
function enviarNuevoAspirante(id) {
        
       var name = $('#name').val();
       var last_name = $('#last_name').val();
       var email = $('#email').val();
       var phone = $('#phone').val();
       var phone2 = $('#phone2').val();
       var document = $('#document').val();
       var city = $('#city').val();
       var status = $('#prospectos_status').val();
       var programs = $('#programs').val();
       var observation_edit = $('#observation_edit').val();
       
    var validacion = 0;
    
    if(name !="" && last_name !="" && email !="" && phone !="" && city !="" && document !=""){
                                    
                validacion = 1;
            }else{
                toastr.error('Para enviar el formulario de registro debe llenar todos los campos', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "generarformulario",
                data: {id:id, name:name, last_name:last_name, email:email, phone:phone, phone2:phone2, document:document, city:city, status:status, programs:programs, observation_edit:observation_edit },
                async: false
            }).responseText;

            if (html == "1") {
                
                toastr.success('El formulario de registro se ha enviado correctamente', 'Envio Formulario');

            } else {
                toastr.error('No ha sido posible enviar el formulario', 'Error');
            }
        }
}

//
function guardarNuevosAspirantes(id){
       
       var bandera = 0;
       var name = $('#name_n').val();
       var last_name = $('#last_name_n').val();
       var email = $('#email_n').val();
       var phone = $('#phone_n').val();
       var phone2 = $('#phone2_n').val();
       var city = $('#city_n').val();
       var document = $('#document_n').val();
       var fuentes = $('#fuentes_n').val();
       var campaign = $('#campaign_n').val();
       var method = $('#method_n').val();
       var programs = $('#programs_n').val();
       
       if(name!= "" && last_name!= "" && email!= "" && phone!= "" && city!= ""){
           bandera = 1;
       }
       
       if(bandera==1){
              
                $.ajax({//metodo ajax
                         type: "GET",//aqui puede  ser get o post
                         url: "nuevoaspirante",//la url adonde se va a mandar la cadena a buscar
                         data: {name: name, last_name: last_name, email:email, phone:phone, phone2:phone2, city:city, document:document, fuentes:fuentes, campaign:campaign, method:method, programs:programs},
                         cache: false,
                         success: function(html)//funcion que se activa al recibir un dato
                         {
                             $('#name_n').val("");
                             $('#last_name_n').val("");
                             $('#email_n').val("");
                             $('#phone_n').val("");
                             $('#phone2_n').val("");
                             $('#city_n').val("");
                             $('#document_n').val("");
                             $('#fuentes_n').val("1");
                             $('#campaign_n').val("1");
                             $('#method_n').val("1");
                             $('#programs_n').val("1");
                             recargarTabla();
                             $('.close').click();

                             toastr.success('Se ha ingresado el nuevo aspirante correctamente', 'Nuevo Aspirante');
                         }
                 });
                 
        }else{
            toastr.error('Los campos Nombre, Email, Telefono y Ciudad son obligatorios', 'Error');
        }
      
   }
   
   $("#email_n").keyup(function(){
                    
                    var max = 10;
                    
                    if($("#email_n").val().length>max)
                    {
                        var email = $("#email_n").val();
                        var html = $.ajax({
                                type: "GET",
                                url: "comprobaremail",
                                data: "email=" + email,
                                async: false
                             }).responseText;

                        if(html == "2")
                        {
                            $("#botonguardarnuevo").removeClass( "disabled" );
                        }
                        else
                        {
                            $("#botonguardarnuevo").addClass( "disabled" );
                            toastr.error('El Email ingresado ya se encuentra registrado', 'Error');
                        }

                    }

                });
                
                $("#phone_n").keyup(function(){
                    
                    var max = 5;
                    
                    if($("#phone_n").val().length>max)
                    {
                        var phone = $("#phone_n").val();
                        var html = $.ajax({
                                type: "GET",
                                url: "comprobarphone",
                                data: "phone=" + phone,
                                async: false
                             }).responseText;

                        if(html == "2")
                        {
                            $("#botonguardarnuevo").removeClass( "disabled" );
                        }
                        else
                        {
                            $("#botonguardarnuevo").addClass( "disabled" );
                            toastr.error('El Telefono ingresado ya se encuentra registrado', 'Error');
                        }

                    }

                });
                
                $("#document_n").keyup(function(){
                    
                    var max = 5;
                    
                    if($("#document_n").val().length>max)
                    {
                        var document = $("#document_n").val();
                        var html = $.ajax({
                                type: "GET",
                                url: "comprobardocumento",
                                data: "document=" + document,
                                async: false
                             }).responseText;

                        if(html == "2")
                        {
                            $("#botonguardarnuevo").removeClass( "disabled" );
                        }
                        else
                        {
                            $("#botonguardarnuevo").addClass( "disabled" );
                            toastr.error('El Documento ingresado ya se encuentra registrado', 'Error');
                        }

                    }

                });
                
                //funcion para guardar un nuevo aspirante
function enviaremailnuevo(tipo, id) {    
    
       var email = $('#email').val();      
              
    var validacion = 0;    
    if(email !=""){

                validacion = 1;
            }else{
                toastr.error('Para enviar el email debe ingresar el email', 'Error');
            }

        if(validacion == 1){
                                    
            var html = $.ajax({
                type: "GET",
                url: "enviaremailnuevo",
                data: {id:id, tipo:tipo, email:email },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
            $('#botones-correos').show("show");
            $('#img-loading-correos').hide("slow");
                toastr.success('El correo se ha enviado correctamente', 'Mensaje enviado');

            } else {
                $('#botones-correos').show("show");
            $('#img-loading-correos').hide("slow");
                toastr.error('No ha sido posible enviar el correo', 'Error');
            }
        }
}

function validaremailnuevo(tipo, id) {
    
       var email = $('#email').val();
              
    var validacion = 0;    
    if(email !=""){

                validacion = 1;
            }else{
                toastr.error('Para enviar el email debe ingresar el email', 'Error');
            }

        if(validacion == 1){
            
            $('#botones-correos').hide("slow");
            $('#img-loading-correos').show("show");
            setTimeout ('enviaremailnuevo('+tipo+','+id+')', 1000);
        }
}

function guardarNextDate(id){
       
       var next_date = $('#next_fecha').val();
       var observation = $('#next_observation').val();
       
       if(observation=="" || next_date==""){
           
            toastr.error('Todos los campos son obligatorios', 'Error');
                             return;
                         
                         
       }
              
       $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "guardarnextdate",//la url adonde se va a mandar la cadena a buscar
                data: {id:id, observation: observation, next_date:next_date},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    $('#next_fecha').val("");
                    $('#next_observation').val("");
                    toastr.success('Se ha programado el seguimiento correctamente', 'Seguimiento Guardado');
                }
        });
      
   }

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////Funciones para gestionar los nuevos prospectos//////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function crear_metodo() {
    var metodo = $('#item_metodo').val();
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: { metodo:metodo},
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('El nuevo Método se ha guardado correctamente', 'Nuevo Método');
            $('#close_modal_metodo').click();
            $('#item_metodo').val('');
        } else {
            toastr.error('No ha sido posible guardar el nuevo Método', 'Error');
        }
    
}
function crear_campania() {
    var campania = $('#item_campania').val();
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {campania:campania },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La nueva Campaña se ha guardado correctamente', 'Nueva Campaña');
            $('#close_modal_campania').click();
            $('#item_campania').val('');
        } else {
            toastr.error('No ha sido posible guardar la nueva Campaña', 'Error');
        }
    
}
function crear_fuente() {
    var fuente = $('#item_fuente').val();
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {fuente:fuente },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La nueva Fuente se ha guardado correctamente', 'Nueva Fuente');
            $('#close_modal_fuente').click();
            $('#item_fuente').val('');
        } else {
            toastr.error('No ha sido posible guardar la nueva Fuente', 'Error');
        }
    
}
function crear_estado() {
    var estado = $('#item_estado').val();
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {estado:estado },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('El nuevo Estado se ha guardado correctamente', 'Nuevo Estado');
            $('#close_modal_estado').click();
            $('#item_estado').val('');
        } else {
            toastr.error('No ha sido posible guardar el Estado', 'Error');
        }
    
}
function crear_documento() {
    var documento = $('#item_documento').val();
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {documento:documento },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('El nuevo Documento se ha guardado correctamente', 'Nuevo Documento');
            $('#close_modal_estado').click();
            $('#item_documento').val('');
        } else {
            toastr.error('No ha sido posible guardar el Estado', 'Error');
        }
    
}
function crear_convocatoria() {
    var convocation = $('#convocation').val();
    var date_convocation = $('#date_convocation').val();
    var period_c = $('#period_c').val();
    
    if(convocation =="" || date_convocation =="" || period_c ==0){
        toastr.error('Todos los campos son obligatorios', 'Error');
        return;
    }
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {convocation:convocation, date:date_convocation, period_c:period_c },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La nueva convocatoria se ha guardado correctamente', 'Nueva Convocatoria');
            $('#convocation').val('');
            $('#date_convocation').val('');
            $('#period_c').val('');
        } else {
            toastr.error('No ha sido posible guardar la convocatoria', 'Error');
        }
    
}
function crear_salon() {
    var classroom = $('#classroom').val();
    var quota = $('#quota').val();
    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {classroom:classroom, quota:quota},
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('El nuevo salon se ha guardado correctamente', 'Nuevo Salon');
            $('#classroom').val('');
            $('#quota').val('');
        } else {
            toastr.error('No ha sido posible guardar el salon', 'Error');
        }
    
}

function crear_asignacion() {
    var convocation_a = $('#convocation_a').val();
    var classroom_a = $('#classroom_a').val();
    var type_a = $('#type_a').val();
    var time_a = $('#time_a').val();
    
    if(convocation_a ==0 || classroom_a ==0 || type_a ==0 || time_a ==""){
        toastr.error('Todos los campos son obligatorios', 'Error');
        return;
    }    
    var html = $.ajax({
        type: "GET",
        url: "ingresaritems",
        data: {convocation_a:convocation_a, classroom_a:classroom_a, type_a:type_a, time_a:time_a},
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La nueva asignacion se ha guardado correctamente', 'Nueva Asignacion');
            $('#convocation_a').val('');    
            $('#classroom_a').val('');
            $('#type_a').val('');
            $('#time_a').val('');
        } else {
            toastr.error('No ha sido posible guardar la asignacion', 'Error');
        }
    
}

function cargar_programas_asignacion(id){

    var html = $.ajax({
        type: "GET",
        url: "programasasignacion",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax1').html(html);

}

function crear_agenda(){
    var nombre = $('#nombre').val();
    var contacto = $('#contacto').val();
    var direccion = $('#direccion').val();
    var telefono = $('#telefono').val();
    var email = $('#email').val();
    var feria = $('#feria').val();
    var rector = $('#rector').val();
    var observaciones = $('#observaciones').val();


    if(nombre == ""){
        toastr.error('El campo nombre esta vacio', 'Error');
        return;
    }
    if(contacto == ""){
        toastr.error('El campo contacto esta vacio', 'Error');
        return;
    }
    if(direccion == ""){
        toastr.error('El campo direccion esta vacio', 'Error');
        return;
    }
    if(telefono == ""){
        toastr.error('El campo telefono esta vacio', 'Error');
        return;
    }
    if(email == ""){
        toastr.error('El campo email esta vacio', 'Error');
        return;
    }
    if(feria == ""){
        toastr.error('El campo feria esta vacio', 'Error');
        return;
    }
    if(rector == ""){
        toastr.error('El campo rector esta vacio', 'Error');
        return;
    }
    if(observaciones == ""){
        toastr.error('El campo observaciones esta vacio', 'Error');
        return;
    }

    var html = $.ajax({
        type: "GET",
        url: "ingresaragenda",
        data: {
            nombre:nombre,
            contacto:contacto,
            direccion:direccion,
            telefono:telefono,
            email:email,
            feria:feria,
            rector:rector,
            observaciones:observaciones
        },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La nueva Agenda se ha guardado correctamente', 'Nueva Agenda');
            $('#close_modal').click();
            $('#nombre').val("");
            $('#contacto').val("");
            $('#direccion').val("");
            $('#telefono').val("");
            $('#email').val("");
            $('#feria').val("");
            $('#rector').val("");
            $('#observaciones').val("");
            recargarTabla();
            
        } else {
            toastr.error('No ha sido posible guardar la nueva Agenda', 'Error');
        }


}
function editar_agenda(id){
    var nombre = $('#nombre_edit').val();
    var contacto = $('#contacto_edit').val();
    var direccion = $('#direccion_edit').val();
    var telefono = $('#telefono_edit').val();
    var email = $('#email_edit').val();
    var feria = $('#feria_edit').val();
    var rector = $('#rector_edit').val();
    var observaciones = $('#observaciones_edit').val();
    var id = id;

    if(nombre == ""){
        toastr.error('El campo nombre esta vacio', 'Error');
        return;
    }
    if(contacto == ""){
        toastr.error('El campo contacto esta vacio', 'Error');
        return;
    }
    if(direccion == ""){
        toastr.error('El campo direccion esta vacio', 'Error');
        return;
    }
    if(telefono == ""){
        toastr.error('El campo telefono esta vacio', 'Error');
        return;
    }
    if(email == ""){
        toastr.error('El campo email esta vacio', 'Error');
        return;
    }
    if(feria == ""){
        toastr.error('El campo feria esta vacio', 'Error');
        return;
    }
    if(rector == ""){
        toastr.error('El campo rector esta vacio', 'Error');
        return;
    }
    if(observaciones == ""){
        toastr.error('El campo observaciones esta vacio', 'Error');
        return;
    }

    var html = $.ajax({
        type: "GET",
        url: "ingresaragenda",
        data: {
            id:id,
            nombre:nombre,
            contacto:contacto,
            direccion:direccion,
            telefono:telefono,
            email:email,
            feria:feria,
            rector:rector,
            observaciones:observaciones
        },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La Agenda se ha Editado correctamente', 'Edición de Agenda');
            $('#close_modal_detalle').click();
            $('#nombre_edit').val("");
            $('#contacto_edit').val("");
            $('#direccion_edit').val("");
            $('#telefono_edit').val("");
            $('#email_edit').val("");
            $('#feria_edit').val("");
            $('#rector_edit').val("");
            $('#observaciones_edit').val("");
            recargarTabla();
            
        } else {
            toastr.error('No ha sido posible editar la Agenda', 'Error');
        }


}
function cargar_detalle_agenda(id){


    var html = $.ajax({
        type: "GET",
        url: "detalleagenda",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax3').html(html);



}
function cargar_seguimientos_agenda(id){


    var html = $.ajax({
        type: "GET",
        url: "seguimientosagenda",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax1').html(html);

}
function cargar_seguimientos_feria(id){


    var html = $.ajax({
        type: "GET",
        url: "seguimientosferias",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax1').html(html);

}
function crearDirasp() {
    var nombre = $("#carpeta").val();

    $.ajax({
        type: "GET",
        url: "creardir",
        data: {nombre: nombre},
    })
            .done(function (data) {
                if (data == "2") {
                    toastr.error('No ha sido posible crear la carpeta', 'Error');
                } else {

                    $(".close").click();
                    toastr.success('carpeta creada correctamente', 'Nueva carpeta');
                    $('#carpetas').append(new Option(data["name"], data["id"], true, true));
                }
            });

}

$('#convocation_a').change(function (event) {
    
    var convocatoria = $("#convocation_a").find(':selected').val();        
    
    $.ajax({
        type: "GET",
        url: "refreshconvocatorias",
        data: {convocatoria: convocatoria},
        
    }).done(function (data) {
        
                if (data == "1") {
                    $('#table-asignaciones').html("");
                } else {
                    $('#table-asignaciones').html(data);
                }
            });
});

var id_actual = 0;
function cargar_detalle_prospecto(id) {
       
       id_actual = id;
            
      var parametros = {
         "id": id
      };
      $.ajax({
         data: parametros,
         url:  'detalleprospecto',
         type: 'get',

         success: function(response){
              
            $("#ajax2").html(response);
            var type_event = ["statusaspviews", id, "1"];
                send(type_event);
            
         }
      });

   }
   function cargar_detalle_inscrito(id) {
      
      id_actual = id;
      
      var parametros = {
         "id": id
         
      };
      $.ajax({
         data: parametros,
         url:  'detalleinscrito',
         type: 'get',

         success: function(response){
              
            $("#ajax2").html(response);
            var type_event = ["statusaspviews", id, "1"];
            send(type_event);
         }
      });

   }
   function cargar_detalle_inscrito2(id) {
      
      id_actual = id;
      
      var parametros = {
         "id": id
         
      };
      $.ajax({
         data: parametros,
         url:  'detalleinscrito2',
         type: 'get',

         success: function(response){
              
            $("#ajax2").html(response);
            var type_event = ["statusaspviews", id, "1"];
            send(type_event);
         }
      });

   }
function actualizarAspview(){
    
    var parametros = {
         "id": id_actual
         
      };
      $.ajax({
         data: parametros,
         url:  'actualizaraspview',
         type: 'get',

         success: function(response){
              
            var type_event = ["statusaspviews", id_actual, "0"];
            send(type_event);
         }
      });
             
   }