var texto_global_equipos  = "";
function desactivar_tipo(id, status){
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id: id, status: status}
    })
    .done(function(data) {
        if (data != 2) {
            toastr.success('Se Gestiono correctamente el Tipo de Mantenimiento', 'Desactivo Tipo de Mantenimiento');
            
            $("#tabla_tipo_equipos").html(data);
            
        }else{
            toastr.error('No ha sido posible Gestionar el Tipo de Mantenimiento', 'Error');
        }
    });
}
function detalle_mantenimientos(id){
    
    $.post('frommantenimientos', {
              id:id
            } ,

            function(response){
                $('#basic').html(response);
                //var datos = JSON.parse(response);
            });
}
function Ingresar_tipo_mantenimiento(){
    var tipo_equipo = $('#item_mant').val();
    var tipo_mant = $('#tipo_mant').val();

    if (tipo_equipo == "") {
        toastr.error('El campo Item Mantenimiento está vacio.', 'Error');
        return;
    }
    if (tipo_mant == "") {
        toastr.error('El campo Tipo  está vacio.', 'Error');
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_equipo: tipo_equipo, tipo_mant:tipo_mant}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Mantenimiento', 'Nuevo Tipo de Mantenimiento');
            $("#item_mant").val("");
            $("#tipo_mant").val("");
            $("#tabla_tipo_equipos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Mantenimiento', 'Error');
        }
    });
}

function cargar_detalle_prospecto2(id){

    if (tipo_equipo == "") {
        toastr.error('El campo Item Mantenimiento está vacio.', 'Error');
        return;
    }
    if (tipo_mant == "") {
        toastr.error('El campo Tipo  está vacio.', 'Error');
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_equipo: tipo_equipo, tipo_mant:tipo_mant}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Mantenimiento', 'Nuevo Tipo de Mantenimiento');
            $("#item_mant").val("");
            $("#tipo_mant").val("");
            $("#tabla_tipo_equipos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Mantenimiento', 'Error');
        }
    });
}

function crear_tipo_equipo(){
    var item = $("#tipo_equipo").val();
    if (item == "") {
        toastr.error("El campo Tipo Equipo esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_equipo: item, status:1}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Equipo', 'Nuevo Tipo de Equipo');
            $("#tipo_equipo").val("");
            
            $("#tabla_tipo_equipos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Equipo', 'Error');
        }
    });
}
function crear_tipo_mantenimiento(){
    var item = $("#tipo_mantenimiento").val();
    if (item == "") {
        toastr.error("El campo Tipo Mantenimiento esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_mantenimiento: item, status:1}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Tipo de Mantenimiento', 'Nuevo Tipo de Mantenimiento');
            $("#tipo_mantenimiento").val("");
            
            $("#tabla_tipo_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Tipo de Mantenimiento', 'Error');
        }
    });
}
function crear_grupo_equipos(){
    var grupo = $("#nombre_grupo").val();
    if (grupo == "") {
        toastr.error("El campo Nombre del Grupo esta vacio.", "Error");
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creargrupoequipos",
            data: { grupo: grupo, status:1}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Grupo de Equipos', 'Nuevo Grupo de Equipo');
            $("#nombre_grupo").val("");
            
            $("#tabla_grupo_mantenimiento").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Grupo de Equipos', 'Error');
        }
    });
}
function eliminar_grupo(id, status){
    $.ajax({
            type: "GET",
            url:  "creargrupoequipos",
            data: { desactivar_id: id, status: status}
    })
    .done(function(data) {
        if (data != 2) {
            toastr.success('Se Gestiono correctamente el Grupo', 'Gestion de Grupo');
            
            $("#tabla_grupo_mantenimiento").html(data);
            
        }else{
            toastr.error('No ha sido posible Gestionar el Grupo', 'Error');
        }
    }); 
}
function editar_grupo(id){
    var texto = $("#texto_"+id).val();
    if (texto_global_equipos == texto) {
        return;
    }
    $.ajax({
            type: "GET",
            url:  "creargrupoequipos",
            data: { texto_editar: id, texto: texto}
    })
    .done(function(data) {
        if (data == 1) {
            toastr.success('Se Edito correctamente el Grupo', 'Gestion de Grupo');
            
            $("#id_grupo_"+id).addClass('has-success');
            
        }else{
            toastr.error('No ha sido posible Editar el Grupo', 'Error');
        }
    }); 
}
function eliminar_mantenimiento(id, status){
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id: id, status: status}
    })
    .done(function(data) {
        if (data != 2) {
            toastr.success('Se Gestiono correctamente el Tipo de Mantenimiento', 'Gestion de Tipo de Mantenimiento');
            
            $("#tabla_tipo_mantenimientos").html(data);
            
        }else{
            toastr.error('No ha sido posible Gestionar el Tipo de Mantenimiento', 'Error');
        }
    }); 
}

function capturar_valor(id){
    texto_global_equipos = $("#texto_"+id).val();
}
function editar_mantenimiento(id){
    var texto = $("#texto_"+id).val();

    if (texto_global_equipos == texto) {
        return;
    }

    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { texto_editar: id, texto: texto}
    })
    .done(function(data) {
        if (data == 1) {
            toastr.success('Se Edito correctamente el Tipo de Mantenimiento', 'Gestion de Tipo de Mantenimiento');
            
            $("#id_mantenimiento_"+id).addClass('has-success');
            
        }else{
            toastr.error('No ha sido posible Editar el Tipo de Mantenimiento', 'Error');
        }
    }); 
}
function mostrar_inactivos_grupos(){
    var status = "1";
    if ($("#check_grupos").prop( "checked" ) == true) {
        var status = "2";
    }
    
    $.ajax({
            type: "GET",
            url:  "creargrupoequipos",
            data: { estado_grupos:status}
    })
    .done(function(data) {
        if (data != 1) {
            $("#tabla_grupo_mantenimiento").html(data);
            
            // $("#id_grupo_"+id).addClass('has-success');
            
        }else{
            toastr.error('No ha sido posible Editar el Grupo', 'Error');
        }
    }); 
}
function mostrar_inactivos_mantenimientos(){
    var status = "1";
    if ($("#check_mantenimientos").prop( "checked" ) == true) {
        var status = "2";
    }
    
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { estado_mantenimientos:status}
    })
    .done(function(data) {
        if (data != 1) {
            $("#tabla_tipo_mantenimientos").html(data);
            
            // $("#id_grupo_"+id).addClass('has-success');
            
        }else{
            toastr.error('No ha sido posible Editar el Grupo', 'Error');
        }
    }); 
}

function crear_item_mantenimiento(){
    var item = $("#item_mantenimiento").val();
    var tipo = $("#item_tipo_mantenimiento").val();
    
    if (item == "") {
        toastr.error("El campo Item Mantenimiento esta vacio.", "Error");
        return;
    }
    if (tipo == "") {
        toastr.error("El campo Tipo Mantenimiento esta vacio.", "Error");
        return;
    }
    
    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { item_mantenimiento: item, tipo:tipo}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el Item de Mantenimiento', 'Nuevo Item de Mantenimiento');
            $("#item_mantenimiento").val("");
            $("#item_tipo_mantenimiento").val("");
            $("#requerido_mantenimiento").val("");
            
            $("#tabla_item_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible ingresar el  Item de Mantenimiento', 'Error');
        }
    });
}
function gestionar_item_mantenimientos(item,status){

    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id_item: item, status:status}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se Modifico correctamente el Item de Mantenimiento', 'Gestion Item de Mantenimiento');
            $("#item_mantenimiento").val("");
            $("#item_tipo_mantenimiento").val("");
            $("#requerido_mantenimiento").val("");
            
            $("#tabla_item_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible Modificar el  Item de Mantenimiento', 'Error');
        }
    });

}
function gestionar_tipo_mantenimientos(item,status){

    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { desactivar_id_tipo_mant: item, status:status}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se Modifico correctamente el Tipo de Mantenimiento', 'Gestion Tipo de Mantenimiento');
            $("#item_mantenimiento").val("");
            $("#item_tipo_mantenimiento").val("");
            $("#requerido_mantenimiento").val("");
            
            $("#tabla_tipo_mantenimientos").html(data);
        }else{
            toastr.error('No ha sido posible Modificar el  Tipo de Mantenimiento', 'Error');
        }
    });

}
function cargar_items_de_namt(){
    tipo_mant = $("#item_tipo_mantenimiento").val();

    $.ajax({
            type: "GET",
            url:  "creartipomantenimiento",
            data: { tipo_mant: tipo_mant}
    })
    .done(function(data) {
        if (data != 2) {
            // toastr.success('Se Gestiono correctamente el Tipo de Mantenimiento', 'Gestion de Tipo de Mantenimiento');
            
            $("#tabla_item_mantenimientos").html(data);
            
        }else{
            // toastr.error('No ha sido posible Gestionar el Tipo de Mantenimiento', 'Error');
        }
    }); 
}
function eliminar_items_mant(id, status, tipo){
    $.ajax({
            type: "GET",
            url:  "gestionaritemmant",
            data: { desactivar_id: id, status: status, tipo:tipo }
    })
    .done(function(data) {
        if (data != 2) {
            toastr.success('Se Gestiono correctamente el Grupo', 'Gestion de Grupo');
            
            $("#tabla_item_mantenimientos").html(data);
            
        }else{
            toastr.error('No ha sido posible Gestionar el Grupo', 'Error');
        }
    }); 
}
function editar_items_mant(id, tipo){
    var texto = $("#texto_"+id).val();

    if (texto_global_equipos == texto) {
        return;
    }
    
    $.ajax({
            type: "GET",
            url:  "gestionaritemmant",
            data: { texto_editar: id, texto: texto, tipo:tipo}
    })
    .done(function(data) {
        if (data == 1) {
            toastr.success('Se Edito correctamente el Grupo', 'Gestion de Grupo');
            
            $("#id_mant_"+id).addClass('has-success');
            
        }else{
            toastr.error('No ha sido posible Editar el Grupo', 'Error');
        }
    }); 
}
function nuevo_item_mantenimiento(){
    var texto = $("#campo_nuevo_item").val();
    var campo_requerido = $("#campo_requerido").val();


    $.ajax({
            type: "GET",
            url:  "nuevoitemmant",
            data: { texto: texto, campo_requerido:campo_requerido}
    })
    .done(function(data) {
        if (data != 1) {
            toastr.success('Se ingreso correctamente el nuevo Item', 'Nuevo Item');
            $("#campo_nuevo_item").val("");
            $("#campo_requerido").val("");

            $("#item_mantenimiento").html(data);
            
        }else{
            toastr.error('No ha sido posible ingresar el Item', 'Error');
        }
    }); 
}
function mostrar_form_agregar_grupo(id){
    if (id == 1) {
        $("#btn_mostrar_form").attr('onclick', 'mostrar_form_agregar_grupo(2)');
        $("#btn_mostrar_form").html('<i class="icon-remove"></i> Cancelar');
        $("#tabla_tipos_mantenimientos").hide('show');
        $("#tabla_form_asign_grupos").show('slow');

    }else{
        $("#btn_mostrar_form").attr('onclick', 'mostrar_form_agregar_grupo(1)');
        $("#btn_mostrar_form").html('<i class="icon-ok"></i> Asignar a un Grupo');
        $("#tabla_tipos_mantenimientos").show('show');
        $("#tabla_form_asign_grupos").hide('slow');        
    }
}