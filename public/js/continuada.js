jQuery(function($){
    $('.fecha_nuevoalum').mask('0000-00-00');
   
});

$( ".fecha_nuevoalum" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '-50y' });

// $( ".fecha_required" ).datepicker({ dateFormat: "yy-mm-dd", minDate: '+0d' });

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Funciones para traer datos en el formulario de editar curso//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

   function agregarCursoe(codigo, nombre, id){
       
       $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarcursose",//la url adonde se va a mandar la cadena a buscar
                data: {codigo: codigo},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    $("#displaycodigop").hide();
                    $("#displaynombrep").hide();
                    $('#coding').val(codigo);
                    $('#name_curso').val(html[0]["name_curso"]);
                    $('#open').val(html[0]["open"]);
                    $('#status').val(html[0]["status"]);
                    $('#start_date').val(html[0]["start_date"]);
                    $('#end_date').val(html[0]["end_date"]);
                    $('#courses_categories_id').val(html[0]["courses_categories_id"]);
                    $('#instructors_id').val(html[0]["instructors_id"]);
                }
        });
      
   }

// Focus = Changes the background color of input to yellow
function focusFunctioname() {
    $("#displaynombrep").hide('slow');
}

function blurFunctioname() {
    $("#displaynombrep").show('slow');
}

function focusFunctionCode() {
    $("#displaycodigop").hide('slow');
}

function blurFunctionCode() {
    $("#displaycodigosp").show('slow');
}

$("#name_curso").keyup(function() //se crea la funcion keyup
{
    var texto = $("#name_curso").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displaynombrep").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarnombredit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displaynombrep").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

$("#coding").keyup(function() //se crea la funcion keyup
{
    var texto = $("#coding").val();//se recupera el valor de la caja de texto y se guarda en la variable texto

    if(texto=='' || texto.length<2)//si no tiene ningun valor la caja de texto no realiza ninguna accion
    {
        $("#displaycodigop").hide();
    }
    else
    {
        $.ajax({//metodo ajax
                type: "GET",//aqui puede  ser get o post
                url: "consultarcodigoedit",//la url adonde se va a mandar la cadena a buscar
                data: {texto: texto},
                cache: false,
                success: function(html)//funcion que se activa al recibir un dato
                {
                    if(html != 0){
                        $("#displaycodigop").html(html).show();// funcion jquery que muestra el div con identificador display, como formato html, tambien puede ser .text
                    }
                }
        });
    }return false;
});

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////Funciones para traer datos en el formulario de editar curso//////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//funcion para guardar un nuevo curso
function EditarInstructor(idu,idi) {
    var validacion = 0;

                            var nombre1 = $('#nombre1').val();
                            var nombre2 = $('#nombre2').val();
                            var apellido1 = $('#apellido1').val();
                            var apellido2 = $('#apellido2').val();
                            var tipo_identificacion = $('#tipo_identificacion').val();
                            var n_documento = $('#n_documento').val();
                            var lugar_nacimiento = $('#lugar_nacimiento').val();
                            var fecha_nacimiento = $('#fecha_nacimiento').val();
                            var estado_civil = $('#estado_civil').val();
                            var genero = $('#genero').val();
                            var grupo_sanguineo = $('#grupo_sanguineo').val();
                            var direccion = $('#direccion').val();
                            var telefono = $('#telefono').val();
                            var celular = $('#celular').val();
                            var email = $('#email').val();
                            var ciudad_residencia = $('#ciudad_residencia').val();
                            var numero_hijos = $('#numero_hijos').val();    

                            if(nombre1 !="" && nombre2 !="" && apellido1 !="" && apellido2 !="" && tipo_identificacion !="" && n_documento !="" && lugar_nacimiento !="" && fecha_nacimiento !="" 
                               && estado_civil !="" && genero !="" && grupo_sanguineo !="" && direccion !="" && telefono !="" && celular !="" && email !="" && ciudad_residencia !="" && numero_hijos !=""){
                                       validacion = 1;
                                    }else{
                                        toastr.error('Todos los campos son obligatorios', 'Error');
                                    }

                                if(validacion == 1){
                                    var html = $.ajax({
                                        type: "GET",
                                        url: "editarinstructor",
                                        data: {idu:idu, idi:idi, nombre1:nombre1, nombre2:nombre2, apellido1:apellido1, apellido2:apellido2, tipo_identificacion:tipo_identificacion, 
                                            n_documento:n_documento, lugar_nacimiento:lugar_nacimiento, fecha_nacimiento:fecha_nacimiento, estado_civil:estado_civil, genero:genero, 
                                            grupo_sanguineo:grupo_sanguineo, direccion:direccion, telefono:telefono, celular:celular, email:email, ciudad_residencia:ciudad_residencia, numero_hijos:numero_hijos },
                                        async: false
                                    }).responseText;

                                    if (parseInt(html) == 1) {

                                        toastr.success('Cambios guardados correctamente', 'Actualizacion');

                                    } else {
                                        toastr.error('No ha sido posible guardar los cambios', 'Error');
                                    }
                                }

}

//funcion para cambiar la contraseña del instructor
function CambiarPassIns(idu) {
    var nueva = $("#nueva").val();
    var renueva = $("#renueva").val();
    if (nueva == renueva) {
        if (nueva.length < 6) {
            toastr.warning('La nueva contraseña debe tener al menos 6 caracteres', 'Error');
            return;
        } else {
            if (!tiene_numeros(nueva) || !tiene_letras(nueva)) {
                toastr.warning('La nueva contraseña debe tener numeros y letras', 'Error');
                return;
            }
        }
        var html = $.ajax({
            type: "GET",
            url: "cambiarpassinst",
            data: {idu:idu, nueva: nueva, renueva: renueva},
            async: false
        }).responseText;

        if (html == "1") {
            $("#nueva").val("");
            $("#renueva").val("");
            toastr.success('La contraseña ha sido actualizada correctamente', 'Actualizacion');

        } else {
            toastr.error('No ha sido posible actualizar su contraseña', 'Error');
        }
    } else {
        toastr.warning('las contraseñas no coinciden, intente nuevamente', 'Error');
    }
}

//funcion para cambiar la contraseña del estudiante
function CambiarPassEst(idu) {
    var nueva = $("#nueva").val();
    var renueva = $("#renueva").val();
    if (nueva == renueva) {
        if (nueva.length < 6) {
            toastr.warning('La nueva contraseña debe tener al menos 6 caracteres', 'Error');
            return;
        } else {
            if (!tiene_numeros(nueva) || !tiene_letras(nueva)) {
                toastr.warning('La nueva contraseña debe tener numeros y letras', 'Error');
                return;
            }
        }
        var html = $.ajax({
            type: "GET",
            url: "cambiarpassest",
            data: {idu:idu, nueva: nueva, renueva: renueva},
            async: false
        }).responseText;

        if (html == "1") {
            $("#nueva").val("");
            $("#renueva").val("");
            toastr.success('La contraseña ha sido actualizada correctamente', 'Actualizacion');

        } else {
            toastr.error('No ha sido posible actualizar su contraseña', 'Error');
        }
    } else {
        toastr.warning('las contraseñas no coinciden, intente nuevamente', 'Error');
    }
}

//funcion para guardar un nuevo curso
function guardarCourse() {
    var validacion = 0;
    var coding = $('#coding').val();
    var name_curso = $('#name_curso').val();
    var open = $('#open').val();
    var status = $('#status').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    var courses_categories_id = $('#courses_categories_id').val();
    var cupo = $('#cupo').val();
    var time_intensity = $('#time_intensity').val();
    var schedule = $('#schedule').val();
    var ocsa = $('#ocsa').val();
    var inspector = $('#inspector').val();
    var fecha_teorica = $('#fecha_teorica').val();
    var fecha_practica = $('#fecha_practica').val();
    var inst_teorica = $('#inst_teorica').val();
    var inst_practica = $('#inst_practica').val();
    
    if(coding !="" && name_curso !="" && start_date !="" && end_date !="" && cupo !="" && time_intensity !="" && schedule !="" && ocsa !="" && inspector !="" && fecha_teorica !="" && fecha_practica !="" && inst_teorica !="" && inst_practica !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarcurso",
                data: {coding:coding, name_curso:name_curso, open:open, status:status, start_date:start_date, 
                    end_date:end_date, courses_categories_id:courses_categories_id, cupo:cupo, time_intensity:time_intensity, schedule:schedule, 
                    ocsa:ocsa, inspector:inspector, fecha_teorica:fecha_teorica, fecha_practica:fecha_practica, inst_teorica:inst_teorica, inst_practica:inst_practica },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    $('#coding').val('');
                    $('#name_curso').val('');
                    $('#open').val('');
                    $('#status').val('');
                    $('#start_date').val('');
                    $('#end_date').val('');
                    $('#cupo').val('');
                    $('#courses_categories_id').val('');
                    $('#time_intensity').val('');
                    $('#schedule').val('');
                    $('#ocsa').val('');
                    $('#fecha_teorica').val('');
                    $('#fecha_practica').val('');
                    $('#inst_teorica').val('');
                    $('#inst_practica').val('');

                toastr.success('El nuevo curso se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar el Curso', 'Error');
            }
        }
}

//funcion para guardar un nuevo curso
function duplicarCurso(id) {
    var validacion = 0;
    var name_curso = $('#name_curso').val();
    var courses_categories_id = $('#courses_categories_id').val();
    var time_intensity = $('#time_intensity').val();
    var schedule = $('#schedule').val();
    var open = $('#open').val();
    var inspector = $('#inspector').val();    
    var inst_teorica = $('#inst_teorica').val();
    var fecha_teorica = $('#fecha_teorica').val();
    var inst_practica = $('#inst_practica').val();
    var fecha_practica = $('#fecha_practica').val();
    var start_date = $('#start_date').val();
    var end_date = $('#end_date').val();
    
    if(name_curso !="" && start_date !="" && end_date !="" && time_intensity !="" && schedule !="" && schedule !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "duplicarcurso",
                data: {id:id, name_curso:name_curso, courses_categories_id:courses_categories_id, time_intensity:time_intensity, schedule:schedule, 
                    open:open, inspector:inspector, inst_teorica:inst_teorica, fecha_teorica:fecha_teorica, inst_practica:inst_practica, fecha_practica:fecha_practica, 
                    start_date:start_date, end_date:end_date },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('El nuevo curso se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar el Curso', 'Error');
            }
        }
}

//funcion para guardar una nueva categoria
function guardarCategory() {
    var validacion = 0;
    var category = $('#category').val();
    
    if(category !=""){
               validacion = 1;
            }else{
                toastr.error('Todos los campos con son obligatorios', 'Error');
            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "guardarcategory",
                data: {category:category },
                async: false
            }).responseText;

            if (parseInt(html) == 1) {
                
                    $('#category').val('');

                toastr.success('La nueva categoria se ha guardado correctamente', 'Nuevo Curso');

            } else {
                toastr.error('No ha sido posible guardar la categoria', 'Error');
            }
        }
}

function cargar_detalle_empresa(id){

    var html = $.ajax({
        type: "GET",
        url: "detalleempresa",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax3').html(html);

}
function cargar_seguimientos_empresa(id){

    var html = $.ajax({
        type: "GET",
        url: "seguimientosempresa",
        data: {id:id},
        async: false
    }).responseText;

    $('#ajax1').html(html);

}

function editar_empresa(id){
    var nombre = $('#nombre_edit').val();
    var contacto = $('#contacto_edit').val();
    var direccion = $('#direccion_edit').val();
    var telefono = $('#telefono_edit').val();
    var email = $('#email_edit').val();
    var observaciones = $('#observaciones_edit').val();
    var id = id;

    if(nombre == ""){
        toastr.error('El campo nombre esta vacio', 'Error');
        return;
    }
    if(contacto == ""){
        toastr.error('El campo contacto esta vacio', 'Error');
        return;
    }
    if(direccion == ""){
        toastr.error('El campo direccion esta vacio', 'Error');
        return;
    }
    if(telefono == ""){
        toastr.error('El campo telefono esta vacio', 'Error');
        return;
    }
    if(email == ""){
        toastr.error('El campo email esta vacio', 'Error');
        return;
    }
    if(observaciones == ""){
        toastr.error('El campo observaciones esta vacio', 'Error');
        return;
    }

    var html = $.ajax({
        type: "GET",
        url: "ingresarempresa",
        data: {
            id:id,
            nombre:nombre,
            contacto:contacto,
            direccion:direccion,
            telefono:telefono,
            email:email,
            observaciones:observaciones
        },
        async: false
    }).responseText;
        if (parseInt(html) == 1) {
            toastr.success('La Agenda se ha Editado correctamente', 'Edición de Agenda');
            $('#close_modal_detalle').click();
            $('#nombre_edit').val("");
            $('#contacto_edit').val("");
            $('#direccion_edit').val("");
            $('#telefono_edit').val("");
            $('#email_edit').val("");
            $('#observaciones_edit').val("");
            recargarTabla();
            
        } else {
            toastr.error('No ha sido posible editar la Agenda', 'Error');
        }

}

//funcion para guardar evaluacion de entrada
function EvaluacionEntrada(idalumno, idcurso) {
    var validacion = 1;
    var pregunta1 = $('#pregunta1').val();
    var pregunta2 = $('#pregunta2').val();
    var pregunta3 = $('#pregunta3').val();
    var pregunta4 = $('#pregunta4').val();
    var pregunta5 = $('#pregunta5').val();
    var pregunta23 = $('#pregunta23').val();
    var pregunta24 = $('#pregunta24').val();
    
//    if(pregunta1 !="" && pregunta2 !="" && pregunta3 !="" && pregunta4 !="" && pregunta5){
//               validacion = 1;
//            }else{
//                toastr.error('Todas las preguntas son obligatorias', 'Error');
//            }

        if(validacion == 1){
            var html = $.ajax({
                type: "GET",
                url: "evaluacionentrada",
                data: {idalumno:idalumno, idcurso:idcurso, pregunta1:pregunta1, pregunta2:pregunta2, pregunta3:pregunta3, pregunta4:pregunta4, 
                    pregunta5:pregunta5, pregunta23:pregunta23, pregunta24:pregunta24},
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('La evaluacion se ha guardado correctamente', 'Cambios guardados');

            } else {
                toastr.error('No ha sido posible guardar la evaluacion', 'Error');
            }
        }
}

//funcion para guardar un nuevo curso
function eliminar_materia(id) {    
        
            var html = $.ajax({
                type: "GET",
                url: "eliminarmateria",
                data: {id:id},
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('El instructor se ha eliminado correctamente', 'Instructor eliminado');
                
                $('#table-inst-'+id).hide('slow');

            } else {
                
                toastr.error('No ha sido posible eliminar el instructor', 'Error');
                
            }
            
}

function eliminar_asignacion_est(id) {    
        
            var html = $.ajax({
                type: "GET",
                url: "eliminarasigancionest",
                data: {id:id},
                async: false
            }).responseText;

            if (parseInt(html) == 1) {

                toastr.success('El estudiante se ha eliminado correctamente', 'Estudiante eliminado');
                
                $('#table-est-'+id).hide('slow');

            } else {
                
                toastr.error('No ha sido posible eliminar el estudiante', 'Error');
                
            }
            
}
var texto_global_respuestas_entrada  = "";
function capturar_valor_respuesta_entrada(id, id_pregunta){
    
    texto_global_respuestas_entrada = $("#pregunta"+id_pregunta).val();

}
function editar_respuesta_entrada(id, id_pregunta){
    var texto = $("#pregunta"+id_pregunta).val();
    if (texto_global_respuestas_entrada == texto) {
        return;
    }
    $.ajax({
            type: "GET",
            url:  "editarrespuestaentrada",
            data: { texto_editar: id, texto: texto}
    })
    .done(function(data) {
        if (data == 1) {
            toastr.success('Se Edito correctamente la respuesta', 'Edición de Respuesta');
            
            $("#pregunta"+id_pregunta).addClass('has-success');
            
        }else{
            toastr.error('No ha sido posible Editar la Respuesta', 'Error');
        }
    }); 
}