$('#tags_2').tagEditor({
    initialTags: [],
    delimiter: ', ',
    placeholder: 'Ingrese las etiquetas ...'
});

function finaltags(){
    
    var final_tags = $('#tags_2').tagEditor('getTags')[0].tags;    
    $('#final_tags').val(final_tags);
    
    var titulo = $('#titulo').val();
    var desc = CKEDITOR.instances['desc'].getData();
    var articulo = CKEDITOR.instances['articulo'].getData();
    var final_tags = $('#final_tags').val();
    var imagen_previa = $('#imagen_previa').val();
    var imagen_post = $('#imagen_post').val();
    
    if(titulo == "" || desc=="" || articulo=="" || final_tags=="" || imagen_previa=="" || imagen_post==""){
        toastr.error('Todos los campos son obligatorios', 'Error');
    }else{
        $('#form_blog').submit();
    }
        
    
}

$('#public').change(function (event) {
    var estado = $("#public").find(':selected').val();
    var dirigido = $("#dirigido").find(':selected').val();
    
    if(estado==1){
        $(".id_dirigido").hide();
        $(".proceso").hide();
    }else{       
        $(".id_dirigido").show();
        if(dirigido==1){
        $(".proceso").show();
    }
    }

});

$('#dirigido').change(function (event) {
    var dirigido = $("#dirigido").find(':selected').val();
    
    if(dirigido==1){
        $(".proceso").show();
    }else{
        $(".proceso").hide();
    }

});